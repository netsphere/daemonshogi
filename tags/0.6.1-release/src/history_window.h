/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HISTORY_WINDOW_H__
#define HISTORY_WINDOW_H__ 1

#include "si/dte.h"
#include <vector>
#include <utility>

struct Canvas;

/** (現在注目している) 棋譜 */
class History
{
  Canvas* canvas;

  /** 棋譜ウィンドウ */
  GtkWidget* window;

public:
  /** 現在の表示棋譜. 手と消費時間のペア. ゲームモードで使用。 */
  std::vector< std::pair<DTe, int> > focus_line;

  /** 表示棋譜の現在表示行. 棋譜の途中を指すことがある. 0 で開始局面 */
  int cur_pos;

public:
  History(Canvas* canvas_): canvas(canvas_), window(NULL) {}

  void show_window();

  void hide_window();

  void sync(const Record& record, int n, const DTe& te);

  void clear();

  void append_move(const DBoard& bo, const DTe& te, int elapsed_sec);

  void delete_move( int n );

  void delete_last();

  void highlight_cur_pos();

  void select_nth(int new_pos);

 private:
  void refresh_list();
};


void alt_moves_dialog_update();

void alt_moves_dialog_hide();

#endif
