/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _DBOARD_H_
#define _DBOARD_H_

#include <stdio.h>
#include "dte.h"
#include "si.h"


/** 盤の外の値 */
#define DWALL (0x20)


typedef BOARD DBoard; // for compatibility


/*********************************************************/
/* function prototypes */
/*********************************************************/


void    daemon_dboard_set_board (DBoard *bo,
				 int x,
				 int y,
                                 PieceKind p);

PieceKind daemon_dboard_get_board (const DBoard* bo,
                                   int x, int y);

void    daemon_dboard_set_piece (DBoard *bo,
				 int next,
				 PieceKind p,
				 int n);

void    daemon_dboard_add_piece (DBoard *bo,
				 int next,
				 PieceKind p);

void    daemon_dboard_dec_piece (DBoard *bo,
				 int next,
				 PieceKind p);

int     daemon_dboard_get_piece (const DBoard* bo,
				 int next,
				 PieceKind p);

void    daemon_dboard_set_mate  (DBoard *bo);
void    daemon_dboard_set_rl_reversal(DBoard *bo);
void    daemon_dboard_set_order_reversal(DBoard *bo);
void    daemon_dboard_flip      (DBoard *bo);
void    daemon_dboard_set_all_to_pbox(DBoard *bo);
void    daemon_dboard_set_for_edit(DBoard *bo);

void    daemon_dboard_copy      (const DBoard* src, DBoard *dest);

void    daemon_dboard_set_pbox  (DBoard* bo, PieceKind p, int n);
int     daemon_dboard_get_pbox  (const DBoard* bo, PieceKind p);

void    daemon_dboard_add_pbox  (DBoard *bo, int p);
void    daemon_dboard_dec_pbox  (DBoard *bo, int p);


#endif /* _DBOARD_H_ */
