/* -*- coding:utf-8 -*-
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dte.h"
#include "board-misc.h"
#include "si.h"
using namespace std;


DTe* daemon_dte_new() 
{
  DTe* te;

  te = (DTe*) malloc(sizeof(DTe));
  if (te == NULL) {
    printf("No enough memory in daemon_dte_new().");
    abort();
  }

  return te;
}


void daemon_dte_free(DTe* te) 
{
  assert(te != NULL);
  free(te);
}


/** デバッグ用の表現 */
void daemon_dte_output(const DTe* te, FILE* out) 
{
  assert(te != NULL);
  assert(out != NULL);

  if ( !te->fm ) {
    fprintf(out, "(%d,%d) %d uti\n", te->to & 0xF, te->to >> 4, te->uti);
  } 
  else {
    fprintf(out, "(%d,%d)->(%d,%d)%s\n",
	    te->fm & 0xF, te->fm >> 4,
	    te->to & 0xF, te->to >> 4,
	    te->nari ? " nari" : "");
  }
}


/** 局面を利用した文字列化. 主にデバッグ用 */
string DTe::to_str( const BOARD& bo ) const
{
  if ( special ) {
    switch (special) {
    case RESIGN:
      return "%TORYO";
    case CHUDAN:
      return "#CHUDAN";
    case KACHI:
      return "%KACHI";
    case JISHOGI:
      return "#JISHOGI";
    case ILLEGAL:
      return "#ILLEGAL_MOVE";
    case TIME_UP:
      return "#TIME_UP";
    case SENNICHITE:
      return "#SENNICHITE";
    default:
      abort();
    }
  }

  // 通常の指し手
  char buf[100];
  if ( !fm ) {
    sprintf(buf, "%d%d%s*",
	    to & 0xf, to >> 4, CSA_PIECE_NAME[uti]);
  }
  else {
    sprintf(buf, "%d%d%s%s(%d%d)",
	    to & 0xf, to >> 4,
	    CSA_PIECE_NAME[bo.board[fm] & 0xf],
	    (nari ? "+" : ""),
	    fm & 0xf, fm >> 4);
  }

  return buf;
}


/** 
 * 手を作る. 
 * daemon_record_load_csa_te() を参考に。 
 */
void dte_parse_move( const BOARD* bo, const string& line, DTe* net_te, 
                     int* elapsed_time )
{
  assert( net_te );
  assert( elapsed_time );

  int idx = 0;

  if ( !line.find("%TORYO") ) {
    net_te->clear();
    net_te->special = DTe::RESIGN;
    idx += strlen("%TORYO");
  }
  else if ( !line.find("%KACHI") ) {
    net_te->clear();
    net_te->special = DTe::KACHI;
    idx += strlen("%KACHI");
  }
  else {
    // 通常の指し手
    assert( line[0] == '+' || line[0] == '-' );

    char koma_str[3];
    PieceKind p;

    net_te->clear();
    net_te->special = DTe::NORMAL_MOVE;
    // xyが逆
    Xy fm = ((line[2] - 0x30) << 4) + line[1] - 0x30;
    Xy to = ((line[4] - 0x30) << 4) + line[3] - 0x30;

    sscanf(line.c_str() + 5, "%2s", koma_str );
    for (p = 1; p <= 0xf; p++) {
      if (strcmp(koma_str, CSA_PIECE_NAME[p]) == 0)
	break;
    }
    if (p > 0xf)
      throw 0;

    if ( fm == 0 ) {
      // 駒打ち
      if (to < 0x11 || to > 0x99)
	throw 0;
      net_te->to = to; net_te->uti = p;
    }
    else {
      // 移動
      PieceKind pf = bo->board[fm] & 0xf;

      net_te->fm = fm;
      net_te->to = to;
      net_te->nari = pf <= 8 && p >= 9 ? true : false;
      net_te->tori = bo->board[to];
    }

    idx += 7;
  }

  // 消費時間
  *elapsed_time = 1; // 0 はない
  sscanf( line.c_str() + idx, ",T%d", elapsed_time );

  printf( "%s: parsed = %s, %d\n", 
	  __func__, net_te->to_str(*bo).c_str(), *elapsed_time );
}


////////////////////////////////////////////////////////////////////////
// ソケット/パイプ経由で送信


/** 0終端の文字列を送信 */
void conn_send_str(int server_fd, const char* line)
{
  printf("send: %s", line); // DEBUG

  if (server_fd >= 0) {
    int ret = write(server_fd, line, strlen(line));
    if (ret < 0) {
      printf("%s: socket write error.\n", __func__);
      abort();
    }
  }
}


/** 
 * 指し手を送信.
 * daemon_record_output_csa_te() を参考に。 
 */
int conn_send_move( int server_fd, const BOARD* board, const DTe& te )
{
  if (te.special == DTe::RESIGN) {
    conn_send_str(server_fd, "%TORYO\n");
    return 1;
  }

  // 通常の指し手
  char buf[100];
  if ( !te.fm ) {
    // 打ち
    sprintf(buf, "%c%d%d%d%d%s\n",
	    !board->next ? '+' : '-',
	    0, 0, 
	    te.to & 0xf, te.to >> 4,
	    CSA_PIECE_NAME[te.uti]);
  }
  else {
    // 移動
    PieceKind p = daemon_dboard_get_board(board, te.fm & 0xf, te.fm >> 4);
    if ( te.nari ) p += 8;

    sprintf(buf, "%c%d%d%d%d%s\n",
	    !board->next ? '+' : '-',
	    te.fm & 0xf, te.fm >> 4,
	    te.to & 0xf, te.to >> 4,
	    CSA_PIECE_NAME[p & 0xf]);
  }
  conn_send_str(server_fd, buf);

  return 1;
}


static DTe pending_move;

/** サーバから受信した行を解析する */
INPUTSTATUS conn_queue_front_move( list<string>& net_queue, const BOARD* bo,
                                   DTe* te, int* elapsed_time )
{
  while (true) {
    string line = net_queue.front();
    net_queue.pop_front();

  if (line == "#JISHOGI") {
    // プロトコルv1.1.2では「日本将棋連盟ルール」とだけ書いている.
    // 24点法だと思うが、そうすると引き分けの場合がある
    //
    // http://www.computer-shogi.org/wcsc20/rule.html
    // によれば, プロトコルを無視して, 27点法によるみたい
    //   → 引き分けはない
    assert( pending_move.special == DTe::KACHI );
  }
  else if (line == "#SENNICHITE") {
    // 連続王手でない千日手. 指してもよいがそこで終了. これだけ扱いが違う
    // クライアントでも千日手チェックするか, 非同期で受信しないといけない
    //   +6978KI,T2
    //   #SENNICHITE
    //   #DRAW
    assert( pending_move.special == DTe::SENNICHITE );
  }
  else if (line == "#OUTE_SENNICHITE") {
    // 連続王手の千日手
    // 受け方が指したときに成立することがある
    // これもクライアントでのチェックが必要
    //   -8493OU,T2
    //   #OUTE_SENNICHITE
    //   #WIN
    assert( pending_move.special == DTe::ILLEGAL ||
	    pending_move.special == DTe::SENNICHITE );
  }
  else if (line == "#ILLEGAL_MOVE") {
    // これもクライアントでのチェックが必要
    //   +0031FU,T1
    //   #ILLEGAL_MOVE
    //   #WIN
    assert( pending_move.special == DTe::ILLEGAL || 
	    pending_move.special == DTe::KACHI );

    pending_move.special = DTe::ILLEGAL; // 不正な入玉勝利宣言
  }
  else if (line == "#TIME_UP") {
    // 時間切れ. 1行目で判断できる
    // #TIME_UP
    // #WIN
    pending_move.special = DTe::TIME_UP;
  }
  else if (line == "#CHUDAN") {
    // サーバからの中断指示
    // クライアントからの中断要請もできるが, プロトコルv1.1.2では何が起こるか不明
    // gmSetGameStatus( &g_game, SI_CHUDAN_REMOTE );
    return SI_CHUDAN_REMOTE;
  }
  else if ( line == "#RESIGN" ) {
    assert( pending_move.special == DTe::RESIGN );
  }
  else if ( line == "#WIN" || line == "#LOSE" || line == "#DRAW" ) {
    assert( pending_move.special );

    if ( pending_move.special == DTe::TIME_UP ) {
      // gmSetGameStatus( &g_game, SI_TIMEOUT );
      printf("%s: time out!!\n", __func__); // DEBUG
      return SI_TIMEOUT;
    }
    else {
      *te = pending_move;
      return SI_NORMAL;
    }
  }
  else {
    // 通常手, 投了, 勝利宣言
    DTe net_te;
    dte_parse_move( bo, line, &net_te, elapsed_time );
    bool forbidden = false;

      if (net_te.special == DTe::RESIGN) {
	// 勝ったとき. #RESIGN, #WINは無視してよい
	//   %TORYO.T4
	//   #RESIGN
	//   #WIN
	pending_move = net_te;
      }
      else if (net_te.special == DTe::KACHI) {
	// 勝利宣言. クライアントでチェックするか2行目を待つか
	// shogi-server では #DRAW を返すことはないようだが?
	//   %KACHI,T8
	//   #JISHOGI
	//   #WIN
	// または
	//   %KACHI,T8
	//   #ILLEGAL_MOVE
	//   #WIN
	pending_move = net_te;
      }
      else if ( !te_is_valid(bo, net_te.pack()) ) {
	pending_move = net_te;
	pending_move.special = DTe::ILLEGAL;
	// #ILLEGAL_MOVE を待つ
      }
      else if ( te_is_sennichite(bo, net_te.pack(), &forbidden) ) {
	pending_move = net_te;
	pending_move.special = forbidden ? DTe::ILLEGAL : DTe::SENNICHITE;
	// #OUTE_SENNICHITE または #SENNICHITE を待つ
      }
      else {
	*te = net_te;
	return SI_NORMAL;
      }
    }
  } // of while
}
