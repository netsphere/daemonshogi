/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Sprite class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/sprite.h,v $
 * $Id: sprite.h,v 1.1.1.1 2005/12/09 09:03:07 tokita Exp $
 */

#ifndef _SPRITE_H_
#define _SPRITE_H_

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

  
typedef enum {
  KOMA_SPRITE = 0,
  ETC_SPRITE = 1
} SPRITE_TYPE;


/** スプライトクラス.
 * \todo Z座標.
 * \todo 駒情報は追い出す
 */
typedef struct {
  /** スプライトの種類 */
  SPRITE_TYPE type;

  /**
   * sprite の元画像. このスプライトでは画像を所有しない.
   * (src_x, src_y)-(width, height) を転送する。
   */
  GdkPixmap* pixmap;

  /** 元画像の透過マスク */
  GdkBitmap* mask;

  /** sprite の表示元画像でのX位置 */
  gint src_x;
  /** sprite の表示元画像でのY位置 */
  gint src_y;

  /** sprite の横のドット数 */
  gint width;
  /** sprite の縦のドット数 */
  gint height;
  
  /** sprite のX位置 */	
  gint x;
  /** sprite のY位置 */	
  gint y;
  /** ドラッグ前のX位置 */
  gint from_x;
  /** ドラッグ前のY位置 */
  gint from_y;
  /** 表示するか？のフラグ。TRUEならば表示する。 */
  gboolean visible;
  /** 移動できるかのフラグ。TRUEならば移動できる。 */
  gboolean move;
  /** 駒種類 */
  gint koma;
} Sprite;

/*********************************************************/
/* function prototypes */
/*********************************************************/

Sprite*    daemon_sprite_new( GdkPixmap* pixmap,
                              GdkBitmap* bitmap,
                              int src_x, int src_y,
                              gint width,
                              gint height);

SPRITE_TYPE daemon_sprite_get_type  (Sprite* sprite);

gint       daemon_sprite_get_width  (const Sprite* sprite);
gint       daemon_sprite_get_height (const Sprite* sprite);

gint       daemon_sprite_get_x      (const Sprite* sprite);
gint       daemon_sprite_get_y      (const Sprite* sprite);

// gint       daemon_sprite_get_src_x  (Sprite* sprite);
// gint       daemon_sprite_get_src_y  (Sprite* sprite);

void daemon_sprite_set_pixmap( Sprite* sprite, GdkPixmap *pixmap, GdkBitmap* mask,
                               int src_x, int src_y, int width, int height );

GdkPixmap* daemon_sprite_get_pixmap (Sprite* sprite);
gboolean   daemon_sprite_isvisible  (Sprite* sprite);
void       daemon_sprite_set_type   (Sprite* sprite,
				     SPRITE_TYPE type);

void       daemon_sprite_set_xy( Sprite* sprite, int x, int y );

gboolean   daemon_sprite_on         (Sprite* sprite,
				     int x,
				     int y);
void       daemon_sprite_set_mask   (Sprite* sprite,
				     GdkBitmap* mask);
GdkBitmap* daemon_sprite_get_mask   (Sprite* sprite);
void       daemon_sprite_set_visible(Sprite* sprite,
				     gboolean bool_);
void       daemon_sprite_set_koma   (Sprite* sprite,
				     gint koma);
gint       daemon_sprite_get_koma   (Sprite* sprite);
gboolean   daemon_sprite_ismove     (Sprite* sprite);
void       daemon_sprite_set_move   (Sprite* sprite, gboolean move);
void       daemon_sprite_set_from_x (Sprite* sprite,
				     gint x);
void       daemon_sprite_set_from_y (Sprite* sprite,
				     gint y);
gint       daemon_sprite_get_from_x (Sprite* sprite);
gint       daemon_sprite_get_from_y (Sprite* sprite);

void daemon_sprite_draw(Sprite* sprite, GdkPixmap* drawable, GdkGC* gc);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _SPRITE_H_ */
