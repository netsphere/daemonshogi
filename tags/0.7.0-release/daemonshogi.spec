
# specの書き方は
# http://docs.fedoraproject.org/developers-guide/ch-rpm-building.html

%define name    daemonshogi
%define version 0.7.0
%define release 1.fc16


################################################
# introduction

Name:        %name
Summary:     Daemonshogi is a GTK+ based, simple shogi (Japanese chess) program.
Summary(ja): デーモン将棋はGTK+を使ったシンプルな将棋プログラムです。
Version:     %version
Release:     %release
License:     GPLv3+
Group:       Amusements/Games
URL:         http://www.nslabs.jp/daemonshogi/
Requires:    gtk3 >= 3.0.12
Requires:    tokyocabinet
#Requires:    bzip2-libs
#Requires:    mpich2 >= 1.2.0
Requires:    boost-serialization >= 1.32.0

Source:        %{name}-%{version}.tar.gz
BuildRequires: gettext
BuildRequires: gtk3-devel >= 3.0.12
BuildRequires: tokyocabinet-devel
BuildRequires: cppunit-devel
#BuildRequires: mpich2-devel >= 1.2.0
BuildRequires: boost-devel >= 1.32.0
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root

%description
Daemonshogi is a GTK+ based, simple shogi (Japanese chess) program.
 * easy to play
 * pretty look by GTK+
 * thinking engine is so weak

%description -l ja
デーモン将棋はGTK+を使ったシンプルな将棋プログラムです。
 * シンプルな操作
 * GTK+を使った美しいインターフェイス
 * 思考エンジンは弱い


################################################
%prep
%setup -q


################################################
%build
%configure
make


################################################
%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}


################################################
%clean
rm -rf %{buildroot}


################################################
%files
%defattr(-, root, root)
%{_bindir}/*
%{_datadir}/*
%doc README README.ja COPYING ChangeLog NEWS AUTHORS INSTALL


################################################
%changelog
* Mon Dec 12 2005 Masahiko Tokita <BQB04357@nifty.ne.jp>
- version 0.1.14
- Fixed for Redhat 9 / Fedora Core 2 / Fedore Core 4

* Fri Dec  9 2005 Masahiko Tokita <BQB04357@nifty.ne.jp>
- Fixed for Vine linux 3.2

* Thu Sep 13 2002 Masahiko Tokita <BQB04357@nifty.ne.jp>
- Cleaned up a bit

* Thu Sep 12 2002 Masahiko Tokita <BQB04357@nifty.ne.jp>
- First try at an RPM
