/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// #define GTK_DISABLE_DEPRECATED 1
// #define GDK_DISABLE_DEPRECATED 1
// #define G_DISABLE_DEPRECATED 1
#define _XOPEN_SOURCE 700 // SUSv4
#include <config.h>

#include <pthread.h>
#include <gtk/gtk.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h> // open()
#include <algorithm>
G_BEGIN_DECLS
#include "callbacks.h"
#include "interface.h"
#include "support.h"
G_END_DECLS
#include "canvas.h"
#include "conn.h"
#include "history_window.h"
using namespace std;


/** メニュー -> ファイル -> 棋譜ファイルを開く */
G_MODULE_EXPORT void
on_file_open_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  GtkWidget* dialog;
  GtkFileFilter* filter;

  dialog = gtk_file_chooser_dialog_new(_("fileselection"),
                                       GTK_WINDOW(g_canvas->window),
                                       GTK_FILE_CHOOSER_ACTION_OPEN,
                                       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                       GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                                       NULL);
  filter = gtk_file_filter_new();
  gtk_file_filter_set_name( filter, 
                            _("Game record files (.csa .pcl .kif .ki2)"));
  gtk_file_filter_add_pattern(filter, "*.csa");
  gtk_file_filter_add_pattern(filter, "*.pcl");
  gtk_file_filter_add_pattern(filter, "*.kif");
  gtk_file_filter_add_pattern(filter, "*.ki2");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);

  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
    char* filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    daemon_kiffile_load_ok_button_pressed(filename);
    g_free(filename);
  }
  gtk_widget_destroy(dialog);
}


G_MODULE_EXPORT void
on_file_exit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  exit( 0 );
}


/** daemonshogi について */
G_MODULE_EXPORT void
on_help_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  GtkWidget* about = create_about_dialog();
  gtk_dialog_run(GTK_DIALOG(about));
  gtk_widget_hide(about);
}


G_MODULE_EXPORT gboolean
on_window_delete_event                 (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  return FALSE;
}


G_MODULE_EXPORT void
on_window_destroy( GtkWidget* widget, gpointer user_data )
{
  exit(0);
}


G_MODULE_EXPORT gboolean
on_drawingarea_motion_notify_event     (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data)
{
  daemon_canvas_motion_notify(g_canvas, event);

  return FALSE;
}


G_MODULE_EXPORT gboolean
on_drawingarea_button_press_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  daemon_canvas_button_press(g_canvas, event);

  return FALSE;
}


G_MODULE_EXPORT gboolean
on_drawingarea_button_release_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  daemon_canvas_button_release(g_canvas, event);

  return FALSE;
}


static GtkWidget* create_overwrite_confirmation_dialog(
                    const char* filename, const char* folder)
{
  GtkWidget* msg_dlg = gtk_message_dialog_new( 
	     GTK_WINDOW(g_canvas->window),
	     GtkDialogFlags(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
	     GTK_MESSAGE_QUESTION,
             GTK_BUTTONS_CANCEL,
	     _("A file \"%s\" already exists.\nDo you want to replace it?"),
	     filename );

  // 置換ボタン
  GtkWidget* button = gtk_button_new_with_mnemonic( _("_Replace") );
  gtk_button_set_image( GTK_BUTTON(button),
			gtk_image_new_from_stock(GTK_STOCK_SAVE_AS,
						 GTK_ICON_SIZE_BUTTON) );
  gtk_widget_show(button);
  gtk_dialog_add_action_widget( GTK_DIALOG(msg_dlg), button,
				GTK_RESPONSE_ACCEPT );

  return msg_dlg;
}


/** ファイル -> 別名で保存 */
G_MODULE_EXPORT void
on_file_saveas_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  GtkWidget* dialog;
  GtkFileFilter *csa_filter, *kif_filter;

  dialog = gtk_file_chooser_dialog_new(_("Save game record file"),
                                       GTK_WINDOW(g_canvas->window),
                                       GTK_FILE_CHOOSER_ACTION_SAVE,
                                       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                       GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                       NULL);

  kif_filter = gtk_file_filter_new();
  gtk_file_filter_set_name(kif_filter, _("Kakinoki format (*.kif)") );
  gtk_file_filter_add_pattern(kif_filter, "*.kif");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), kif_filter);

  csa_filter = gtk_file_filter_new();
  gtk_file_filter_set_name(csa_filter, _("CSA format (*.csa)") );
  gtk_file_filter_add_pattern(csa_filter, "*.csa");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), csa_filter);

  FileWriter writer;

 l1:
  if ( gtk_dialog_run(GTK_DIALOG (dialog)) != GTK_RESPONSE_ACCEPT ) {
    gtk_widget_destroy(dialog);
    return;
  }

  GtkFileFilter* filter;
  Record::FileType ft;

  filter = gtk_file_chooser_get_filter(GTK_FILE_CHOOSER(dialog));
  if (filter == csa_filter)
    ft = Record::D_FORMAT_CSA;
  else if (filter == kif_filter)
    ft = Record::D_FORMAT_KIF;
  else {
    abort();
  }

  // 拡張子を補う
  char* filename_ = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
  string filename(filename_);
  g_free(filename_); filename_ = NULL;

  if ( filename.find(".") == string::npos ) {
    if (ft == Record::D_FORMAT_CSA)
      filename += ".csa";
    else
      filename += ".kif";
  }
  
  int fd = open(filename.c_str(), O_WRONLY | O_CREAT | O_EXCL, 0600 );
  if ( fd >= 0 )
    writer.fdopen(fd);
  else {
    if ( errno == EEXIST ) {
      // ファイルがあった
      gchar* folder = gtk_file_chooser_get_current_folder(
						  GTK_FILE_CHOOSER(dialog) );

      GtkWidget* msg_dlg = create_overwrite_confirmation_dialog(
              					filename.c_str(), folder);
      g_free(folder);

      int r = gtk_dialog_run(GTK_DIALOG(msg_dlg));
      gtk_widget_destroy(msg_dlg);
      if ( r != GTK_RESPONSE_ACCEPT )
	goto l1;

      if ( daemon_filewriter_open(&writer, filename.c_str()) < 0 ) {
	daemon_messagebox(g_canvas, _("Something is wrong with the file name."), 
			  GTK_MESSAGE_ERROR );
	goto l1;
      }
    }
    else {
      daemon_messagebox(g_canvas, _("Something is wrong with the file name."), 
			  GTK_MESSAGE_ERROR );
      goto l1;
    }
  }

  gtk_widget_destroy(dialog);
  daemon_kiffile_save_ok_button_pressed( filename, &writer, ft );
}


G_MODULE_EXPORT void
on_edit_startedit_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  D_CANVAS_MODE mode;

  mode = daemon_canvas_get_mode(g_canvas);

  if (mode != D_CANVAS_MODE_EDIT) {
    int ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("Start edit mode?"));
    if (ret  == GTK_RESPONSE_OK) {
      daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
    }
  }
}


/** 表示 -> 小さい */
G_MODULE_EXPORT void 
on_view_small_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (g_canvas)
    daemon_canvas_change_size(g_canvas, D_CANVAS_SIZE_SMALL);
}


G_MODULE_EXPORT void
on_view_middle_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (g_canvas)
    daemon_canvas_change_size(g_canvas, D_CANVAS_SIZE_MIDDLE);
}


G_MODULE_EXPORT void
on_view_big_activate                        (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (g_canvas)
    daemon_canvas_change_size(g_canvas, D_CANVAS_SIZE_BIG);
}


/** ウィンドウの露出。転送するだけ。 */
G_MODULE_EXPORT gboolean
on_drawingarea_draw( GtkWidget* widget, cairo_t* cr, gpointer user_data )
{
  cairo_set_source_surface(cr, daemon_canvas_get_pixmap(g_canvas), 0, 0);
  int width = gtk_widget_get_allocated_width( widget );
  int height = gtk_widget_get_allocated_height( widget );
  cairo_rectangle( cr, 0, 0, width, height );
  cairo_fill( cr );

  return FALSE;
}


G_MODULE_EXPORT gboolean
on_drawingarea_leave_notify_event      (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data)
{
  daemon_canvas_leave_notify(g_canvas, widget, event, user_data);

  return FALSE;
}


extern NetGameInfo game_info;


/** 
 * ゲーム開始ダイアログ.
 * 各オプションメニューの初期値を設定する
 */
G_MODULE_EXPORT void 
on_dialog_game_realize             (GtkWidget       *widget,
                                        gpointer         user_data)
{
  GtkWidget* w;

  for (int i = 0; i < 2; i++) {
    w = lookup_widget(widget, i == 0 ? "optionmenu_sente" : "optionmenu_gote");

    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(w), _("HUMAN"));
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(w), _("LEVEL1"));
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(w), _("LEVEL2"));
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(w), _("LEVEL3"));
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(w), _("BONANZA"));

    gtk_combo_box_set_active(GTK_COMBO_BOX(w), 0);
  }

  // 手合
  w = lookup_widget(widget, "optionmenu_teai");
  gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(w), _("HIRATE"));
  gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(w), _("Just current board"));
  gtk_combo_box_set_active(GTK_COMBO_BOX(w), 0);
}


static ConnectThreadArg worker;
static guint idle_id = 0;
GtkWidget* connect_status_dialog = NULL;
static pthread_t thread_id;

static void connect_status_set_text(const char* txt)
{
  if (!connect_status_dialog)
    return;

  gtk_label_set_text(
         GTK_LABEL(lookup_widget(connect_status_dialog, "message")), txt);
}


/** 接続できたかどうか */
static gboolean on_connecting_idle(gpointer data)
{
  if (worker.status_queue.size() > 0) {
    string status;
    while (worker.status_queue.size() > 0) {
      status = worker.status_queue.front(); // back() して clear() だと取りこぼす
      worker.status_queue.pop_front();
    }
    connect_status_set_text(status.c_str());
  }

  if (!worker.result)
    return TRUE; // 引き続き呼び出してもらう.

  if (worker.result < 0) {
    // 失敗
    disconnect_server();
    daemon_messagebox(g_canvas, _("connection failed"), GTK_MESSAGE_ERROR);
    g_signal_emit_by_name(
		  G_OBJECT(lookup_widget(g_canvas->window, "connect_server")),
		  "activate", NULL);
    return FALSE;
  }

  // 接続に成功
  connect_status_set_text(_("connected."));

  g_signal_connect_after(G_OBJECT(g_canvas->window), "notify::startgame",
			 daemon_game_start_via_network, NULL);
  // ログインする
  csa_login_and_wait_socket( &worker );

  return FALSE; // アイドル待ちは終了
}


extern void add_watch_socket();

/**
 * ネットワーク経由でのゲーム開始.
 * on_socket_read() から呼び出される
 */
void daemon_game_start_via_network()
{
  printf("%s enter.\n", __func__);

  // 二重に呼び出されることがある
  if (g_canvas->mode != D_CANVAS_MODE_PRE_GAME) {
    printf("mode is %d. return.\n", g_canvas->mode);
    return;
  }

  PLAYERTYPE playertype[2];

  // watchしなおす
  add_watch_socket();

  // TODO: 平手以外
  bo_reset_to_hirate(&g_canvas->board);

  if (game_info.my_turn == '+') {
    playertype[0] = worker.player_type;
    playertype[1] = SI_NETWORK_1;

    if (g_canvas->front != 0)
      on_view_flip_activate(NULL, NULL);
  }
  else {
    playertype[0] = SI_NETWORK_1;
    playertype[1] = worker.player_type;

    if (g_canvas->front == 0)
      on_view_flip_activate(NULL, NULL);
  }

  daemon_game_setup(playertype[0], game_info.player_name[0].c_str(),
                    playertype[1], game_info.player_name[1].c_str(),
		    game_info.total_time );

  // main loop
  daemon_game_main_loop();
}


extern PLAYERTYPE get_player_type(int combo_index);
// int network_game_start = 0;

/** メニュー: ゲーム -> CSAサーバに接続 */
G_MODULE_EXPORT void 
on_game_remote_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (worker.hostname == "") {
    worker.hostname = "localhost";
    worker.port = "4081";
  }

  GtkWidget* dialog;

  dialog = create_remote_dialog();
  GtkEntry* server_host_entry = GTK_ENTRY(lookup_widget(dialog, "server_host"));
  GtkEntry* server_port_entry = GTK_ENTRY(lookup_widget(dialog, "server_port"));
  GtkEntry* username_entry = GTK_ENTRY(lookup_widget(dialog, "username"));
  GtkEntry* password_entry = GTK_ENTRY(lookup_widget(dialog, "password"));
  GtkCheckButton* accept_immediately = GTK_CHECK_BUTTON(
				lookup_widget(dialog, "accept_immediately"));
  assert( server_host_entry && server_port_entry && username_entry && 
	  password_entry && accept_immediately );

  gtk_entry_set_text(server_host_entry, worker.hostname.c_str());
  gtk_entry_set_text(server_port_entry, worker.port.c_str());
  gtk_entry_set_text(username_entry, worker.username.c_str());
  gtk_entry_set_text(password_entry, worker.password.c_str());
  gtk_toggle_button_set_active(
           GTK_TOGGLE_BUTTON(accept_immediately), worker.accept_immediately);
  while (1) {
    if (gtk_dialog_run(GTK_DIALOG(dialog)) != GTK_RESPONSE_OK) {
      gtk_widget_hide(dialog);
      return;
    }

    worker.hostname = gtk_entry_get_text(server_host_entry);
    worker.port = gtk_entry_get_text(server_port_entry);
    worker.username = gtk_entry_get_text(username_entry);
    worker.password = gtk_entry_get_text(password_entry);
    GtkWidget* w = lookup_widget(dialog, "playertype_combobox");
    worker.player_type = get_player_type(
			     gtk_combo_box_get_active(GTK_COMBO_BOX(w)));
    worker.accept_immediately = gtk_toggle_button_get_active(
				     GTK_TOGGLE_BUTTON(accept_immediately));

    // TODO: 設定ファイルへの保存

    // TODO: エラーチェックをしっかりと
    string err;
    if (worker.hostname.size() == 0)
      err += "ホスト名を入力してください。\n";
    if ( worker.port.size() == 0 || atoi(worker.port.c_str()) == 0 )
      err += "ポート番号を入力してください。\n";
    if (worker.username.size() == 0)
      err += "ユーザ名を入力してください。\n";
    if (worker.password.size() == 0)
      err += "パスワードを入力してください。\n";

    if (err != "") {
      gtk_label_set_text(GTK_LABEL(lookup_widget(dialog, "error_message")), 
			 err.c_str());
    }
    else
      break;
  }

  gtk_widget_hide(dialog);

  if (!connect_status_dialog)
    connect_status_dialog = create_connecting_dialog();
  gtk_window_set_modal(GTK_WINDOW(connect_status_dialog), TRUE);
  gtk_widget_show_all(connect_status_dialog);

  // 接続は子スレッドでおこなう
  worker.result = 0;
  // worker.to_stop = 0;
  if (pthread_create(&thread_id, NULL, csa_connect_thread_main, &worker)) {
    daemon_messagebox(g_canvas, "internal error: thread create failed.", 
		      GTK_MESSAGE_ERROR);
    return;
  }
  pthread_detach(thread_id);

  // TODO: こんな感じでパイプを通したほうがいいのかもしらんが
  // http://d.hatena.ne.jp/viver/20081015/p1
  idle_id = g_idle_add(on_connecting_idle, NULL);
}


G_MODULE_EXPORT gboolean
on_bookwindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  assert(g_canvas);

  history_window_menuitem_set_active(g_canvas, false);
  gtk_widget_hide(widget);

  return TRUE; // stop other handlers
}


G_MODULE_EXPORT void
on_booklist_cursor_changed             (GtkTreeView     *treeview,
                                        gpointer         user_data)
{
  GtkTreePath* path = NULL;
  const gint* idx;

  gtk_tree_view_get_cursor(treeview, &path, NULL);
  assert(path);
  idx = gtk_tree_path_get_indices(path);
  assert(idx);

  daemon_canvas_bookwindow_click(treeview, *idx);

  gtk_tree_path_free(path);
}


// メニュー -> 表示 -> 棋譜ウィンドウ
G_MODULE_EXPORT void 
on_view_bookwindow_activate(GtkMenuItem* menu_item,
				 gpointer user_data) 
{
  if (!bookwindow_item_is_actived(g_canvas)) {
    // 表示済
    printf("%s: when active.\n", __func__); // DEBUG
    g_canvas->history->hide_window();
  }
  else
    g_canvas->history->show_window();
}


/** 初期局面まで戻す */
G_MODULE_EXPORT void 
on_go_first_activate(GtkBin* menuitem, gpointer user_data)
{
  printf("%s: cur_pos = %d\n", __func__, g_canvas->history->cur_pos); // DEBUG
  
  Canvas* canvas = g_canvas;

  if (canvas->mode != D_CANVAS_MODE_BOOK) 
    return;

  while (canvas->history->cur_pos > 0) {
    DTe te = canvas->history->focus_line[--canvas->history->cur_pos].first;
    // assert( !teCmpTE(&te, &canvas->board.mi.te[canvas->board.mi.count - 1]) );
    boBack_mate( &canvas->board );
  }

  canvas->history->select_nth(canvas->history->cur_pos);

  // 再描画
  canvas_redraw(canvas);
  
  daemon_canvas_set_kif_sensitive(canvas);
}


/** 1手戻す */
G_MODULE_EXPORT void 
on_go_back_activate(GtkBin* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;

  if (canvas->mode != D_CANVAS_MODE_BOOK)
    return;

  if (canvas->history->cur_pos == 0)
    return;

  DTe te = canvas->history->focus_line[--canvas->history->cur_pos].first;
  // assert( !teCmpTE(&te, &canvas->board.mi.te[canvas->board.mi.count - 1]) );
  boBack_mate( &canvas->board );

  // 手を選択
  canvas->history->select_nth(canvas->history->cur_pos);

  // 再描画
  canvas_redraw(canvas);

  daemon_canvas_set_kif_sensitive(canvas);
}


/** 1手進める */
G_MODULE_EXPORT void 
on_go_next_activate(GtkBin* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;

  if (canvas->mode != D_CANVAS_MODE_BOOK)
    return;

  if (canvas->history->cur_pos >= canvas->history->focus_line.size())
    return;

  DTe te = canvas->history->focus_line[canvas->history->cur_pos].first;
  if ( te.special != DTe::NORMAL_MOVE && te.special != DTe::SENNICHITE )
    return;
  boMove_mate( &canvas->board, te.pack() );
  canvas->history->cur_pos++;

  // 棋譜ウィンドウ更新
  canvas->history->select_nth(canvas->history->cur_pos);

  // 再描画
  canvas_redraw(canvas);

  daemon_canvas_set_kif_sensitive(canvas);
}


G_MODULE_EXPORT void 
on_go_last_activate(GtkBin* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;

  if (canvas->mode != D_CANVAS_MODE_BOOK)
    return;

  while (canvas->history->cur_pos < canvas->history->focus_line.size()) {
    DTe te = canvas->history->focus_line[canvas->history->cur_pos].first;
    if ( te.special != DTe::NORMAL_MOVE && te.special != DTe::SENNICHITE )
      break;
    boMove_mate( &canvas->board, te.pack() );
    canvas->history->cur_pos++;
  }

  canvas->history->select_nth(canvas->history->cur_pos);

  // 再描画
  canvas_redraw(canvas);

  daemon_canvas_set_kif_sensitive(canvas);
}


/** 表示 -> 上下を反転 (回転) */
G_MODULE_EXPORT void 
on_view_flip_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  GtkWidget* flip_menu;
  Canvas* canvas = g_canvas;

  canvas->front = canvas->front == SENTE ? GOTE : SENTE;

  flip_menu = lookup_widget(canvas->window, "flip");
  assert(flip_menu);
  gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM(flip_menu),
				  canvas->front != SENTE );

  // 再描画
  canvas_redraw(canvas);
  daemon_canvas_update_statusbar(canvas);
}


struct MateParam {
  int max_depth;
  int node_count;
};


static void daemon_game_mate_start( MateParam* param, int tesuki, 
				    const char* title )
{
  char buf[100];

  if ( !daemon_game_mate_setup() ) {
    daemon_messagebox( g_canvas, _("No good board."), GTK_MESSAGE_ERROR );
    return;
  }

  GtkWidget* dialog = create_dialog_mate();
  gtk_window_set_title( GTK_WINDOW(dialog), title );

  GtkWidget* max_depth = lookup_widget(dialog, "max_depth_entry" );
  assert(max_depth);
  GtkWidget* node_count = lookup_widget(dialog, "node_count_entry" );
  assert(node_count);

  sprintf(buf, "%d", param->max_depth);
  gtk_entry_set_text(GTK_ENTRY(max_depth), buf);
  sprintf(buf, "%d", param->node_count );
  gtk_entry_set_text(GTK_ENTRY(node_count), buf );

  bool r = false;
  while ( gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK ) {
    int d = atoi( gtk_entry_get_text(GTK_ENTRY(max_depth)) );
    int n = atoi( gtk_entry_get_text(GTK_ENTRY(node_count)) );
    if (d < 1 || n < 100) {
      daemon_messagebox( g_canvas, _("parameter is invalid."), 
			 GTK_MESSAGE_ERROR );
      continue;
    }

    // ok
    param->max_depth = d;
    param->node_count = n;
    r = true;
    break;
  }

  gtk_widget_hide(dialog);

  if (r)
    daemon_game_mate_main_loop(tesuki, param->max_depth, param->node_count);
}


static MateParam solve_tsume_shogi = {61, 20000};
static MateParam solve_hisshi = {11, 2000};

/** ゲーム -> 詰め将棋 */
G_MODULE_EXPORT void 
on_game_checkmate_activate( GtkMenuItem* menuitem, gpointer user_data )
{
  daemon_game_mate_start( &solve_tsume_shogi, 0, _("Solve Tsume Shogi") );
}


G_MODULE_EXPORT void 
on_edit_set_hirate_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  assert( daemon_canvas_get_mode(g_canvas) == D_CANVAS_MODE_EDIT );

  gint ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("Set hirate?"));
  if (GTK_RESPONSE_OK != ret) 
    return;

  bo_reset_to_hirate(&g_canvas->board);
  // daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
  canvas_redraw(g_canvas);
}


G_MODULE_EXPORT void 
on_edit_set_mate_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  assert( daemon_canvas_get_mode(g_canvas) == D_CANVAS_MODE_EDIT );

  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("Set for check mate?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_set_mate(&g_canvas->board);
  // daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
  canvas_redraw(g_canvas);
}


G_MODULE_EXPORT void 
on_edit_rl_reversal_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  assert( daemon_canvas_get_mode(g_canvas) == D_CANVAS_MODE_EDIT );

  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("right/left reversal?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_set_rl_reversal(&g_canvas->board);
  // daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
  canvas_redraw(g_canvas);
}


G_MODULE_EXPORT void 
on_edit_order_reversal_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  assert( daemon_canvas_get_mode(g_canvas) == D_CANVAS_MODE_EDIT );

  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("order reversal?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_set_order_reversal(&g_canvas->board);
  // daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
  canvas_redraw(g_canvas);
}


G_MODULE_EXPORT void 
on_edit_flip_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  assert( daemon_canvas_get_mode(g_canvas) == D_CANVAS_MODE_EDIT );

  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("flip white/black board?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_flip(&g_canvas->board);
  // daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
  canvas_redraw(g_canvas);
}


G_MODULE_EXPORT void 
on_edit_all_koma_to_pbox_activate(GtkMenuItem* menuitem, 
				       gpointer user_data)
{
  assert( daemon_canvas_get_mode(g_canvas) == D_CANVAS_MODE_EDIT );

  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("All pieces to the piece box?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_set_all_to_pbox(&g_canvas->board);
  // daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
  canvas_redraw(g_canvas);
}


/** メニュー -> Game -> Play Game... */
G_MODULE_EXPORT void 
on_game_new_game_activate(GtkBin* menuitem, gpointer user_data)
{
  GtkWidget* dialog = create_dialog_game();
  int r = 0;

  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
    r = daemon_game_new_game_ok(dialog);

  gtk_widget_hide(dialog);

  if (r)
    daemon_game_main_loop();
}


/** メニュー -> 中断 */
G_MODULE_EXPORT void 
on_game_stop_activate(GtkBin* menuitem, gpointer user_data)
{
  if (daemon_canvas_get_mode(g_canvas) != D_CANVAS_MODE_GAME)
    return;

  gmSetGameStatus( &g_game, SI_CHUDAN_LOCAL );
}


/** 待った */
G_MODULE_EXPORT void 
on_game_matta_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;
  int i;

  if (daemon_canvas_get_mode(canvas) != D_CANVAS_MODE_GAME)
    return;

  if (canvas->board.mseq.count < 1)
    return;

  // 棋譜を最大で2手削除する
  for (i = 0; i < 2 && canvas->board.mseq.count > 0; i++) {
    // 千日手チェックを削除
    bo_dec_repetition( &canvas->board );

    // 局面を戻す
    boBack( &canvas->board );
    daemon_dmoveinfo_pop(canvas->record->mi);
    canvas->history->delete_last();
  }

  // 再描画
  canvas_redraw(canvas);
}


/** 投了 */
G_MODULE_EXPORT void 
on_game_giveup_activate(GtkBin* menuitem, gpointer user_data)
{
  if (daemon_canvas_get_mode(g_canvas) != D_CANVAS_MODE_GAME)
    return;

  g_canvas->gui_te = DTe(DTe::RESIGN, 0, 0);
  // gmSetGameStatus(&g_game, SI_TORYO);
}


G_MODULE_EXPORT void
on_view_move_property_activate         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menuitem))) {
    if (!g_canvas->move_property_dialog)
      g_canvas->move_property_dialog = create_alt_moves_window();
    gtk_widget_show_all(g_canvas->move_property_dialog);
  }
  else 
    gtk_widget_hide(g_canvas->move_property_dialog);
}


/** 変化手順ダイアログの行を更新 */
static void alt_moves_update_row( GtkWidget* alt_moves_dialog,
                                  GtkTreePath* path,
                                  int te_count, const MoveEdge& move )
{
  GtkTreeView* next_moves = GTK_TREE_VIEW(
	  lookup_widget(alt_moves_dialog, "next_moves"));
  assert( next_moves );
  GtkListStore* model = GTK_LIST_STORE( gtk_tree_view_get_model(next_moves) );
  assert(model);

  GtkTreeIter list_iter;
  gtk_tree_model_get_iter( GTK_TREE_MODEL(model), &list_iter, path );

  char buf[100];
  create_te_string(&g_canvas->board, te_count + 1, &move.te, buf);
  gtk_list_store_set(model, &list_iter,
		     0, buf,
                     1, ANNOT_STR[ move.annotation ],
                     2, move.te_comment.c_str(),
		     -1);

}


static void on_alt_moves_annot_changed ( GtkCellRendererCombo *combo,
                             gchar                *path_string,
                             GtkTreeIter          *new_iter,
                             gpointer              user_data)
{
  printf( "%s: enter: path = '%s', iter_stamp = %d\n",
	  __func__, path_string, new_iter->stamp ); // DEBUG

  GtkTreeModel* annot_model = NULL;
  g_object_get( G_OBJECT(combo), "model", &annot_model, NULL );
  GtkTreePath* path = gtk_tree_model_get_path(annot_model, new_iter);
  int annot_idx = *gtk_tree_path_get_indices(path);
  gtk_tree_path_free(path);

  printf("annot_idx = %d\n", annot_idx); // DEBUG

  // 局面を得る
  KyokumenNode* n = g_canvas->record->mi;
  int te_count;
  for (te_count = 0; te_count < g_canvas->history->cur_pos; te_count++) 
    n = n->lookup_child(g_canvas->history->focus_line[te_count].first);

  // 値を更新
  path = gtk_tree_path_new_from_string(path_string);
  int list_idx = *gtk_tree_path_get_indices(path);
  n->moves[list_idx].annotation = (Annotation) annot_idx;

  alt_moves_update_row( g_canvas->move_property_dialog,
                        path,
                        te_count, n->moves[list_idx] );

  gtk_tree_path_free(path);
}


/** 前後の空白を削除. boostの代替 */
string str_trim(const string& s)
{
  string::size_type i;
  int j;
  for (i = 0; i < s.size(); i++) {
    if ( !isspace(s[i]) )
      break;
  }
  for (j = s.size() - 1; j > i; j--) {
    if ( !isspace(s[j]) )
      break;
  }
  return string(s, i, j - i + 1);
}


static void on_alt_moves_comment_editing_started( GtkCellRenderer* renderer,
						  GtkCellEditable* editable,
						  gchar* path_string,
						  gpointer user_data )
{
  GtkWidget* dialog = create_comment_dialog();
  GtkWidget* comment_textview = lookup_widget(dialog, "comment_textview");
  assert( comment_textview );

  // 局面を得る
  KyokumenNode* n = g_canvas->record->mi;
  int te_count;
  for (te_count = 0; te_count < g_canvas->history->cur_pos; te_count++) 
    n = n->lookup_child(g_canvas->history->focus_line[te_count].first);

  // 編集開始
  GtkTreePath* path = gtk_tree_path_new_from_string(path_string);
  int list_idx = *gtk_tree_path_get_indices(path);

  GtkTextBuffer* buffer =
    gtk_text_view_get_buffer( GTK_TEXT_VIEW(comment_textview) );
  gtk_text_buffer_set_text( buffer, n->moves[list_idx].te_comment.c_str(), -1 );

  if ( gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK ) {
    GtkTextIter start, end;
    gtk_text_buffer_get_bounds( buffer, &start, &end );
    char* ss = gtk_text_buffer_get_text( buffer, &start, &end, FALSE );

    n->moves[list_idx].te_comment = str_trim(ss);
    g_free(ss);

    // 変化ウィンドウを更新
    alt_moves_update_row( g_canvas->move_property_dialog,
			  path,
			  te_count, n->moves[list_idx] );
  }

  gtk_widget_hide(dialog);
  gtk_tree_path_free(path);

  gtk_widget_destroy( GTK_WIDGET(editable) );
}


/** 変化選択ダイアログの構築 */
G_MODULE_EXPORT void 
on_alt_moves_window_realize( GtkWidget* widget, gpointer user_data )
{
  GtkCellRenderer* renderer;
  GtkTreeViewColumn* column;

  // printf("%s: called.\n", __func__); // DEBUG

  GtkWidget* next_moves = lookup_widget(widget, "next_moves");
  assert(next_moves);

  GtkListStore* model = gtk_list_store_new(3,
                               G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
  gtk_tree_view_set_model(GTK_TREE_VIEW(next_moves), GTK_TREE_MODEL(model));

  // 列1: 指し手
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(
    _("Move"), // title
    renderer,  // cell
    "text", 0, // レンダラの"text"をカラム0に
    NULL);
  assert(column);
  gtk_tree_view_append_column( GTK_TREE_VIEW(next_moves), column );

  // 列2: 評価
  GtkListStore* annot_model = gtk_list_store_new( 1, G_TYPE_STRING );

  for (int i = 0; ANNOT_STR[i]; i++) {
    GtkTreeIter iter;
    gtk_list_store_append(annot_model, &iter);
    gtk_list_store_set(annot_model, &iter, 0, ANNOT_STR[i], -1);
  }
  renderer = gtk_cell_renderer_combo_new();
  g_object_set(G_OBJECT(renderer), 
	       "model", annot_model, 
	       "editable", TRUE,  // GtkCellRendererText prop.
               "text-column", 0, // 選択肢の文字列の列
               "has-entry", FALSE,
               NULL);
  g_signal_connect(G_OBJECT(renderer), "changed", 
		   G_CALLBACK(on_alt_moves_annot_changed), NULL);

  gtk_tree_view_insert_column_with_attributes( 
		        GTK_TREE_VIEW(next_moves),
			-1,
		        _("Annot"),
			renderer,
                        "text", 1,
                        NULL );

  // 列3: コメント
  renderer = gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(renderer), 
               "editable", TRUE, 
	       "wrap-mode", PANGO_WRAP_CHAR,
	       NULL);
  g_signal_connect( G_OBJECT(renderer), "editing-started",
		    G_CALLBACK(on_alt_moves_comment_editing_started), NULL );
  column = gtk_tree_view_column_new_with_attributes( _("Comment"),
                                                     renderer,
                                                     "text", 2,
                                                     NULL );
  gtk_tree_view_append_column(GTK_TREE_VIEW(next_moves), column);
  gtk_tree_view_column_set_resizable(column, TRUE);

  // 次の手を表示
  alt_moves_dialog_update();
}


/** 変化選択ダイアログで次の手を選択 */
G_MODULE_EXPORT void
on_next_moves_cursor_changed           (GtkTreeView     *treeview,
                                        gpointer         user_data)
{
  printf("%s: called.\n", __func__); // DEBUG

  // 何番目か
  GtkTreePath* path = NULL;
  gtk_tree_view_get_cursor(treeview, &path, NULL);
  assert(path);
  const gint* idx = gtk_tree_path_get_indices(path);
  assert(idx);

  // まず現在の局面までたどって
  KyokumenNode* n = g_canvas->record->mi;
  for (int i = 0; i < g_canvas->history->cur_pos; i++)
    n = n->lookup_child( g_canvas->history->focus_line[i].first );

  DTe selected_te = n->moves[*idx].te;

  gtk_tree_path_free( path );
  path = NULL;
  
  // 次の一手を変更するか
  if (g_canvas->history->focus_line[g_canvas->history->cur_pos].first == 
      selected_te)
    return;

  printf("change next move.\n"); // DEBUG

  // 注目する手順を更新
  g_canvas->history->sync( *g_canvas->record, g_canvas->history->cur_pos, 
			   selected_te );
  g_canvas->history->highlight_cur_pos();
}


G_MODULE_EXPORT gboolean
on_alt_moves_window_delete_event   (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  GtkWidget* menuitem = lookup_widget(g_canvas->window, "move_property");
  assert(menuitem);

  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(menuitem), FALSE);
  gtk_widget_hide(widget);

  return TRUE; // stop other handlers
}


G_MODULE_EXPORT void
on_bonanza_setting_button_activate     (GtkButton       *button,
                                        gpointer         user_data)
{

}


/** 接続中ダイアログ -> cancel button */
G_MODULE_EXPORT void
on_connecting_cancelbutton_activate    (GtkButton       *button,
                                        gpointer         user_data)
{
  assert(0); // TODO: ワーカースレッドが動いている場合？
  // worker.to_stop = 1;

  // gtk_widget_destroy( connect_status_dialog );
  // connect_status_dialog = NULL;
  // gtk_widget_hide( connect_status_dialog );

  disconnect_server();
}


/** 主ウィンドウ */
G_MODULE_EXPORT void
on_window_realize( GtkWidget* widget, gpointer user_data )
{
  canvas_build(g_canvas);
}


G_MODULE_EXPORT void
on_remote_dialog_realize               (GtkWidget       *widget,
                                        gpointer         user_data)
{
  GtkWidget* w = lookup_widget(widget, "playertype_combobox");
  assert(w);

  gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(w), _("HUMAN"));
  gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(w), _("LEVEL1"));
  gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(w), _("LEVEL2"));
  gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(w), _("LEVEL3"));
  gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(w), _("BONANZA"));
  gtk_combo_box_set_active(GTK_COMBO_BOX(w), 0);
}


extern NetGameInfo game_info;

/** サーバからの対局提案ダイアログ */
G_MODULE_EXPORT void
on_network_game_dialog_realize         (GtkWidget       *widget,
                                        gpointer         user_data)
{
  int oppo = game_info.my_turn == '+' ? 1 : 0;
  gtk_label_set_text( GTK_LABEL(lookup_widget(widget, "opponent_name")),
                      game_info.player_name[oppo].c_str() );
  gtk_label_set_text( GTK_LABEL(lookup_widget(widget, "teban")),
          game_info.my_turn == '+' ? _("SENTE (BLACK)") : _("GOTE (WHITE)") );
  gtk_label_set_text( GTK_LABEL(lookup_widget(widget, "handicap")), 
		      _("HIRATE"));
  gtk_label_set_text( GTK_LABEL(lookup_widget(widget, "total_time")),
		 game_info.total_time >= 60 * 60 ?
   		       str_format(_("%d hours %d minutes"), 
				  game_info.total_time / (60 * 60), 
				  (game_info.total_time / 60) % 60).c_str() :
		       str_format(_("%d minutes"), 
				  (game_info.total_time / 60) % 60).c_str() );
}


/** ゲーム -> 必至 */
G_MODULE_EXPORT void 
on_game_solve_hisshi_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  daemon_game_mate_start( &solve_hisshi, 1, _("Solve Hisshi") );
}


/** 変化手順の削除 */
G_MODULE_EXPORT void
on_alt_moves_delete_button_activate( GtkButton* button, gpointer user_data )
{
  KyokumenNode* n = g_canvas->record->mi;
  int te_count;
  for( te_count = 0; te_count < g_canvas->history->cur_pos; te_count++ )
    n = n->lookup_child( g_canvas->history->focus_line[te_count].first );

  // どの指し手か
  GtkTreeView* next_moves = GTK_TREE_VIEW(
        	 lookup_widget(g_canvas->move_property_dialog, "next_moves"));
  assert( next_moves );

  // cursor ではなく, selection を使う
  GtkTreeSelection* sel = gtk_tree_view_get_selection( next_moves );
  if ( !sel )
    return;
  GList* rows = gtk_tree_selection_get_selected_rows( sel, NULL );
  if ( !rows || !g_list_length(rows) ) {
    g_list_free_full( rows, (GDestroyNotify) gtk_tree_path_free );
    return;
  }

  if ( daemon_canvas_yes_no_dialog(g_canvas, _("Are you sure?")) ==
                                                        GTK_RESPONSE_YES ) {
    GtkTreeModel* model = gtk_tree_view_get_model( next_moves );
    GtkTreePath* path = (GtkTreePath*) g_list_nth_data(rows, 0);
    int idx = *gtk_tree_path_get_indices( path );

    n->remove_child( n->moves[idx].te );

    GtkTreeIter iter;
    gboolean r = gtk_tree_model_iter_nth_child( model, &iter, NULL, idx );
    assert( r );
    gtk_list_store_remove( GTK_LIST_STORE(model), &iter );

    if ( n->moves.size() > 0 ) {
      // 注目する手順を更新
      idx = min( idx, int(n->moves.size() - 1) );
      g_canvas->history->sync( *g_canvas->record, g_canvas->history->cur_pos,
			       n->moves[idx].te );
    }
    else 
      g_canvas->history->delete_move( te_count );

    g_canvas->history->highlight_cur_pos();
  }

  g_list_free_full( rows, (GDestroyNotify) gtk_tree_path_free );
}


static GtkWidget* game_property_dialog = NULL;
static GtkWidget* popup = NULL;

G_MODULE_EXPORT void
on_file_properties_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  Record* record = g_canvas->record;

  if (!game_property_dialog)
    game_property_dialog = create_game_property_dialog();

  GtkWidget* date_entry = lookup_widget(game_property_dialog, "date_entry");
  GtkWidget* title_entry = lookup_widget(game_property_dialog, "title_entry");
  GtkWidget* source_entry = lookup_widget(game_property_dialog, "source_entry");
  GtkWidget* player_name1_entry = lookup_widget(game_property_dialog, 
                                         "player_name1_entry");
  GtkWidget* player_name2_entry = lookup_widget(game_property_dialog, 
                                         "player_name2_entry");
  GtkWidget* game_comment = lookup_widget( game_property_dialog, 
                                         "game_comment" );
  assert( date_entry && title_entry && source_entry && player_name1_entry &&
	  player_name2_entry && game_comment );

  gtk_entry_set_text( GTK_ENTRY(date_entry), 
		      get_datetime_string(record->start_time).c_str() );
  gtk_entry_set_text( GTK_ENTRY(title_entry), record->title.c_str() );
  gtk_entry_set_text( GTK_ENTRY(source_entry), record->source.c_str() );
  gtk_entry_set_text( GTK_ENTRY(player_name1_entry), 
		      record->player_name[0].c_str() );
  gtk_entry_set_text( GTK_ENTRY(player_name2_entry), 
		      record->player_name[1].c_str() );
  GtkTextBuffer* buffer =
                   gtk_text_view_get_buffer( GTK_TEXT_VIEW(game_comment) );
  gtk_text_buffer_set_text( buffer, record->game_comment.c_str(), -1 );

  int r;
  while ( (r = gtk_dialog_run(GTK_DIALOG(game_property_dialog))),
	  (r == GTK_RESPONSE_OK || r == GTK_RESPONSE_APPLY) ) {
    printf("response = %d\n", r);

    DateTime t = string_to_datetime( 
                                 gtk_entry_get_text(GTK_ENTRY(date_entry)) );
    if (t.year < 0) {
      daemon_messagebox(g_canvas, _("Date is invalid."), GTK_MESSAGE_ERROR);
      continue;
    }

    record->start_time = t;
    record->title = gtk_entry_get_text(GTK_ENTRY(title_entry));
    record->source = gtk_entry_get_text(GTK_ENTRY(source_entry));
    record->player_name[0] = gtk_entry_get_text(GTK_ENTRY(player_name1_entry));
    record->player_name[1] = gtk_entry_get_text(GTK_ENTRY(player_name2_entry));

    GtkTextIter start, end;
    gtk_text_buffer_get_bounds( buffer, &start, &end );
    char* ss = gtk_text_buffer_get_text( buffer, &start, &end, FALSE );
    record->game_comment = str_trim(ss);
    g_free( ss );

    // プレイヤー名を書き直し
    canvas_redraw(g_canvas);

    if ( r == GTK_RESPONSE_OK )
      break;
  }

  // gtk_widget_destroy(game_property_dialog);
  // game_property_dialog = NULL;
  gtk_widget_hide( game_property_dialog );
}


G_MODULE_EXPORT void
on_game_property_dialog_calendar_button_activate
                                        (GtkButton       *button,
                                        gpointer         user_data)
{
  // printf("%s: enter.\n", __func__);
  // daemon_messagebox(g_canvas, "enter", GTK_MESSAGE_INFO); // DEBUG

  if (!popup)
    popup = create_calendar_popup();

  if ( gtk_dialog_run(GTK_DIALOG(popup)) == GTK_RESPONSE_OK ) {
    DateTime m;
    memset(&m, 0, sizeof(m));

    guint year, mon, day;
    gtk_calendar_get_date( GTK_CALENDAR(lookup_widget(popup, "calendar1")), 
			   &year, &mon, &day );
    // printf("calendar's = %d, %d, %d\n", year, mon, day); // DEBUG
    m.year = year; m.month = mon + 1; m.day = day;

    gtk_entry_set_text( 
            GTK_ENTRY(lookup_widget(game_property_dialog, "date_entry")), 
            get_datetime_string(m).c_str() );
  }

  // gtk_widget_destroy(popup);
  // popup = NULL;
  gtk_widget_hide(popup);
}


G_MODULE_EXPORT void
on_calendar_popup_day_selected_double_click
                                        (GtkCalendar     *calendar,
                                        gpointer         user_data)
{
  gtk_dialog_response( GTK_DIALOG(popup), GTK_RESPONSE_OK );
}


G_MODULE_EXPORT void
on_file_save_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  Record* record = g_canvas->record;

  if ( record && record->filename != "" ) {
    FileWriter writer;
    daemon_filewriter_open( &writer, record->filename.c_str() );
    if ( writer.stat == D_FILEWRITER_ERROR ) {
      daemon_messagebox(g_canvas, _("Failed to open file."), GTK_MESSAGE_ERROR);
      return;
    }

    daemon_kiffile_save_ok_button_pressed( record->filename,
					   &writer, record->filetype );
  }
  else 
    on_file_saveas_activate( menuitem, user_data );
}

