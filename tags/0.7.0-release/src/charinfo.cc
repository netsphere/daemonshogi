/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "charinfo.h"
#include "daemon.xpm"


/** デフォルトのデータで CharInfo を生成する。 */
CharInfo* daemon_charinfo_new(GtkWidget* window) {
  CharInfo* info;
  
  info = (CharInfo*) malloc(sizeof(CharInfo));
  if (info == NULL) {
    printf("No enough memory in daemon_charinfo_new");
    abort();
  }
  
  daemon_charinfo_init(info);
  
  return info;
}
  
/** 画像ファイルと台詞ファイルを読み込んで CharInfo を生成する。 */
CharInfo* dameon_charinfo_new_from_file(GtkWidget* window,
					gchar* image_file,
					gchar* words_file) {
  CharInfo* info;

  info = (CharInfo*) malloc(sizeof(CharInfo));
  if (info == NULL) {
    printf("No enough memory in daemon_charinfo_new");
    abort();
  }
  
  return info;
}


/** info を初期化する */
void daemon_charinfo_init(CharInfo* info) 
{
  memset(info, 0, sizeof(CharInfo));
}


/**
 * 勝敗の種類に併せて画像を返す。
 * 
 */
GdkPixbuf* daemon_charinfo_get_image( CharInfo* info, MATCH_STATE s ) 
{
  return info->pixmap;
}

  
/**
 * 勝敗の種類にあわせて台詞をランダムに返す。
 * ただし last_word を見て前回と同じ台詞は返さないようにする。
 */
const gchar* daemon_charinfo_get_word(CharInfo* info, MATCH_STATE s) {
  if (info->words[s][0][0] == '\0' &&
      info->words[s][0][0] == '\0' &&
      info->words[s][0][0] == '\0' &&
      info->words[s][0][0] == '\0') {
    /* 該当の台詞が一つもない */
    return NULL;
  }
  
  return NULL;
}


/** デフォルトデータで CharInfo を生成した返す。 */
CharInfo* daemon_charinfo_get_daemon(GtkWidget* window) 
{
  CharInfo* info;
  // GdkPixbuf* im;
  gint w, h;
  
  info = daemon_charinfo_new(window);

  strncpy(info->char_name, "daemon", sizeof(info->char_name));
  
  /* 画像を読む */
  info->pixmap = gdk_pixbuf_new_from_xpm_data( daemon_xpm );
  if ( !info->pixmap ) {
    printf("No enough memory in daemon_charinfo_get_daemon(). im is NULL\n");
    abort();
  }
  // gdk_pixbuf_render_pixmap_and_mask(im, &info->pixmap, NULL, 0);
  // g_object_unref(im);

  strncpy(info->words[0][0], "は、しまった！", sizeof(info->words[0][0]));
  strncpy(info->words[1][0], "勝負はこれからだよ！", sizeof(info->words[1][0]));
  strncpy(info->words[2][0], "将棋はミスした方が負けるのだよ！", sizeof(info->words[2][0]));
  strncpy(info->words[2][1], "はっはっはっ、完勝！", sizeof(info->words[2][1]));
  strncpy(info->words[2][2], "え、もう終り？", sizeof(info->words[2][2]));
  strncpy(info->words[2][3], "悲しいけどコレ将棋なのよね", sizeof(info->words[2][3]));
  strncpy(info->words[3][0], "拾わせてもらったよ", sizeof(info->words[3][0]));
  strncpy(info->words[3][1], "いやー、惜しかったね", sizeof(info->words[3][1]));
  strncpy(info->words[4][0], "認めたくないものだな、若さゆえの（以下略）", sizeof(info->words[4][0]));
  strncpy(info->words[4][1], "えええっ！（ガーン）", sizeof(info->words[4][1]));
  strncpy(info->words[4][2], "読みの深さが戦力の決定的な差でないことを教えてやる。", sizeof(info->words[4][2]));
  strncpy(info->words[5][0], "今日はこのぐらいで勘弁してやるよ(涙", sizeof(info->words[5][0]));
  strncpy(info->words[5][1], "。。。ありません", sizeof(info->words[5][1]));
  
  return info;
}


