/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * 汎用のスプライトクラス
 */

#include <gtk/gtk.h>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include "sprite.h"
#include "misc.h"
#include <si/si.h>

using namespace std;


/**
 * Sprite を生成してそのポインタを返す。
 * @param pixmap 元画像
 * @param mask 透過マスク
 * @param src_x 元画像でのX座標
 * @param src_y 元画像でのY座標
 * @param width  スプライトの横サイズ
 * @param height スプライトの縦サイズ
 */
Sprite* daemon_sprite_new( cairo_surface_t* pixmap,
                           // GdkBitmap* mask,
                           int src_x, int src_y,
                           gint width,
                           gint height )
{
  Sprite* sprite;

  sprite = (Sprite*) malloc(sizeof(Sprite));
  if ( !sprite ) {
    si_abort("No enough memory in daemon_sprite_new()");
  }

  daemon_sprite_set_pixmap( sprite, pixmap, /*mask,*/ 
			                       src_x, src_y, width, height );

  sprite->x       = 0;
  sprite->y       = 0;
  sprite->visible = FALSE;
  // sprite->koma    = 0;
  sprite->scale_size_x = sprite->scale_size_y = 1.0;

  return sprite;
}


/**
 * widthを返す。
 * @param sprite 対象のsprite
 * @return width
 */
int daemon_sprite_get_width( const Sprite* sprite )
{
  return sprite->src_width;
}


/**
 * heightを返す。
 * @param sprite 対象のsprite
 * @return height
 */
int daemon_sprite_get_height( const Sprite* sprite )
{
  return sprite->src_height;
}


/**
 * x座標を返す。
 * @param sprite 対象のsprite
 * @return x
 */
gint daemon_sprite_get_x(const Sprite* sprite)
{
  return sprite->x;
}


/**
 * y座標を返す。
 * @param sprite 対象のsprite
 * @return y
 */
gint daemon_sprite_get_y(const Sprite* sprite)
{
  return sprite->y;
}


/**
 * src_xを返す。
 * @param sprite 対象のsprite
 * @return x
 */
int daemon_sprite_get_src_x( const Sprite* sprite ) 
{
  // gint koma;

  g_assert(sprite != NULL);
/*  
  if (sprite->type == KOMA_SPRITE) {
    koma = daemon_sprite_get_koma(sprite);
    g_assert(1 <= koma && koma <= 0x1F);
    return src_xy[koma][0] * sprite->width / 40;
  } else {
    return 0;
  }
*/
  return sprite->src_x;
}


/**
 * src_yを返す。
 * @param sprite 対象のsprite
 * @return y
 */
int daemon_sprite_get_src_y( const Sprite* sprite ) 
{
  // gint koma;
  
  g_assert(sprite != NULL);
/*  
  if (sprite->type == KOMA_SPRITE) {
    koma = daemon_sprite_get_koma(sprite);
    g_assert(1 <= koma && koma <= 0x1F);
    return src_xy[koma][1] * sprite->height / 40;
  } else {
    return 0;
  }
*/
  return sprite->src_y;
}


void daemon_sprite_set_src_xy( Sprite* sprite, int src_x, int src_y )
{
  assert( sprite );

  sprite->src_x = src_x;
  sprite->src_y = src_y;
}


/**
 * 座標をセットする。
 * @param sprite 対象のsprite
 * @param x   x座標 (ドット単位)
 * @param y   y座標
 */
void daemon_sprite_set_xy( Sprite* sprite, int x, int y ) 
{
  assert( sprite );

  sprite->x = x;
  sprite->y = y;
}


/**
 * 元画像をセットしなおす.
 * @param sprite 対象のsprite
 * @param pixmap セットするpixmap
 * @param mask   透過マスク
 * @param src_x  元画像のX座標
 * @param src_y  元画像のY座標
 * @param width  スプライトの幅
 * @param height スプライトの高さ
 */
void daemon_sprite_set_pixmap( Sprite* sprite,
                               cairo_surface_t* pixmap, // GdkBitmap* mask,
                               int src_x, int src_y, int width, int height )
{
  assert(sprite);
  assert(pixmap);
  // assert(mask);

  sprite->pixmap = pixmap;
  // sprite->mask    = mask;
  sprite->src_x   = src_x;
  sprite->src_y   = src_y;
  sprite->src_width   = width;
  sprite->src_height  = height;
}


/**
 * pixmapを返す。
 * @param sprite 対象のsprite
 * @return pixmap
 */
cairo_surface_t* daemon_sprite_get_pixmap( Sprite* sprite ) 
{
  return sprite->pixmap;
}


/**
 * 表示するかのフラグを返す。TRUEならば表示する。
 * @param sprite 対象のsprite
 * @return 表示するかのフラグ
 */
gboolean daemon_sprite_isvisible( const Sprite* sprite ) 
{
  return sprite->visible;
}


/**
 * 表示するかのフラグをセットする。
 * @param sprite 対象のsprite
 * @param bool_ 表示するかのフラグ
 */
void daemon_sprite_set_visible(Sprite* sprite, gboolean bool_) 
{
  sprite->visible = bool_;
}


/** 移動できるかのフラグを返す。*/
gboolean daemon_sprite_ismovable( const Sprite* sprite ) 
{
  return sprite->movable;
}


/** 移動できるかのフラグをセットする */
void daemon_sprite_set_movable(Sprite* sprite, gboolean move) 
{
  sprite->movable = move;
}


/** ドラッグ前のX位置をセットする */
void daemon_sprite_set_drag_from( Sprite* sprite, int x, int y )
{
  sprite->from_x = x;
  sprite->from_y = y;
}


/** ドラッグ前のX位置を返す */
gint daemon_sprite_get_from_x(Sprite* sprite) 
{
  return sprite->from_x;
}


/** ドラッグ前のY位置を返す */
gint daemon_sprite_get_from_y(Sprite* sprite) 
{
  return sprite->from_y;
}


void daemon_sprite_draw_all( Sprite* sprites[], cairo_surface_t* drawable )
{
  for (int i = 0; i < SPRITEMAX; i++) {
    Sprite* sprite = sprites[i];
    if ( !sprite )
      continue;
    if ( !daemon_sprite_isvisible(sprite) )
      continue;

    daemon_sprite_draw( sprite, drawable );
  }
}


/**
 * 指定領域内に掛かるスプライトを再描画.
 * 指定領域より広い範囲を再描画することがある
 */
void daemon_sprite_draw_overlap( Sprite* sprites[], const GdkRectangle* rect_,
				 cairo_surface_t* drawable )
{
  GdkRectangle rect, s_rect;
  Sprite* sprite;
  gint    i;

  assert( rect_ );

  rect = *rect_;

  for (i = 0; i < SPRITEMAX; i++) {
    sprite = sprites[i];
    if ( !sprite )
      continue;
    if ( !daemon_sprite_isvisible(sprite) )
      continue;

    s_rect.x      = daemon_sprite_get_x(sprite);
    s_rect.y      = daemon_sprite_get_y(sprite);
    s_rect.width  = daemon_sprite_get_width(sprite) * sprite->scale_size_x;
    s_rect.height = daemon_sprite_get_height(sprite) * sprite->scale_size_y;
    if ( is_on_rectangle(&rect, &s_rect) ) {
      daemon_sprite_draw( sprite, drawable );
      // 領域を広げる
      int x2 = max(rect.x + rect.width, s_rect.x + s_rect.width);
      int y2 = max(rect.y + rect.height, s_rect.y + s_rect.height);
      rect.x = min(rect.x, s_rect.x);
      rect.y = min(rect.y, s_rect.y);
      rect.width = x2 - rect.x;
      rect.height = y2 - rect.y;
    }
  } 
}


/** スプライトを drawable に描画する */
void daemon_sprite_draw( Sprite* sprite, cairo_surface_t* drawable )
{
  int src_x, src_y;

  assert(sprite);
  assert(drawable);
  // assert(gc);

  if ( !sprite->visible )
    return;

  src_x = daemon_sprite_get_src_x(sprite);
  src_y = daemon_sprite_get_src_y(sprite);

  // gdk_draw_drawable(drawable, gc, sprite->pixmap, src_x, src_y, 
  //		    sprite->x, sprite->y, sprite->width, sprite->height);
  cairo_t* cr = cairo_create( drawable ); // dest
  cairo_scale( cr, sprite->scale_size_x, sprite->scale_size_y );
  cairo_set_source_surface( cr, 
			    sprite->pixmap, 
			    sprite->x / sprite->scale_size_x - src_x, 
			    sprite->y / sprite->scale_size_y - src_y );
  cairo_rectangle( cr, sprite->x / sprite->scale_size_x, 
 	               sprite->y / sprite->scale_size_y, 
		       sprite->src_width, sprite->src_height );
  cairo_fill( cr );
  cairo_destroy( cr );
}


/**
 * 指定の点がスプライトの上にあるか。
 * @param sprites  スプライトの配列
 * @param pt       点
 * @return 指定の点が乗っている最初のスプライトの番号。なければ -1.
 */
int daemon_sprite_point_on_sprite( Sprite* sprites[], int pt_x, int pt_y )
{
  const Sprite* sprite;
  gint x, y, width, height;
  gint i;

  for (i = 0; i < SPRITEMAX; i++) {
    sprite = sprites[i];
    if ( !sprite )
      continue;
    // if ( !daemon_sprite_ismovable(sprite) )
    // continue;
    if ( !daemon_sprite_isvisible(sprite) )
      continue;

    x      = daemon_sprite_get_x(sprite);
    y      = daemon_sprite_get_y(sprite);
    width  = daemon_sprite_get_width(sprite) * sprite->scale_size_x;
    height = daemon_sprite_get_height(sprite) * sprite->scale_size_y;

    // スプライト上に点がある
    if ( pt_x >= x && pt_x <= x + width && pt_y >= y && pt_y <= y + height )
      return i;
  }

  return -1;
}


void daemon_sprite_set_scale( Sprite* sprite, double xscale, double yscale )
{
  assert( sprite );
  sprite->scale_size_x = xscale;
  sprite->scale_size_y = yscale;
}

