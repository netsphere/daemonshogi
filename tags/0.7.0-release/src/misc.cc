/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// #define GTK_DISABLE_DEPRECATED 1
// #define GDK_DISABLE_DEPRECATED 1
// #define G_DISABLE_DEPRECATED 1

#include <gtk/gtk.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdexcept>
#include "misc.h"
#include "canvas.h"
using namespace std;



/** 画像を拡大する */
cairo_surface_t* load_pixmap_from_imlib_image_scaled( cairo_format_t format,
                      const GdkPixbuf* im, int width, int height ) 
{
  // GdkPixbuf *im2;
  cairo_surface_t* pixmap = cairo_image_surface_create( format, width, height );
  assert( pixmap );

  cairo_t* cr = cairo_create( pixmap ); // dest
  cairo_scale( cr, ((double) width) / gdk_pixbuf_get_width(im), 
   	           ((double) height) / gdk_pixbuf_get_height(im) );
  gdk_cairo_set_source_pixbuf( cr, im, 0, 0 );
  cairo_rectangle( cr, 0, 0, width, height );
  cairo_fill( cr );
  cairo_destroy( cr );

  return pixmap;
}


/**
 * tv と now の時間差が DOUBLECLICKTIMEOUT 以内の場合は TRUE を返す。
 * それ以外は FALSE を返す。
 * @param tv  前にクリックされた時刻
 * @param now 今の時刻
 * @return 時間差が DOUBLECLICKTIMEOUT 以内の場合は TRUE
 */
gboolean check_timeval(const struct timeval* tv, const struct timeval* now) 
{
  if (2 <= now->tv_sec - tv->tv_sec) {
    /* ２秒以上差がある */
    return FALSE;
  }
  
  if (0 < now->tv_sec - tv->tv_sec) {
    if ((now->tv_usec + 1000000) - tv->tv_usec > DOUBLECLICKTIMEOUT * 1000) {
      return FALSE;
    } else {
      // tv->tv_sec = 0L;
      // tv->tv_usec = 0L;
      return TRUE;
    }
  } else {
    if (now->tv_usec - tv->tv_usec > DOUBLECLICKTIMEOUT * 1000) {
      return FALSE;
    } else {
      // tv->tv_sec = 0L;
      // tv->tv_usec = 0L;
      return TRUE;
    }
  }
}

/**
 * a と b が重なりあっているか調べる。
 * @param a 
 * @param b
 * @return 重なっていれば TRUE 、そうでなければ FALSE を返す。
 */
gboolean is_on_rectangle( const GdkRectangle* a, const GdkRectangle* b ) 
{
  if (a->x <= b->x && b->x <= a->x + a->width &&
      a->y <= b->y && b->y <= a->y + a->height) {
    return TRUE;
  }
  
  if (b->x <= a->x && a->x <= b->x + b->width &&
      a->y <= b->y && b->y <= a->y + a->height) {
    return TRUE;
  }
  
  if (b->x <= a->x && a->x <= b->x + b->width &&
      b->y <= a->y && a->y <= b->y + b->height) {
    return TRUE;
  }

  if (a->x <= b->x && b->x <= a->x + a->width &&
      b->y <= a->y && a->y <= b->y + b->height) {
    return TRUE;
  }

  return FALSE;
}


gboolean daemon_on_rectangle(GdkRectangle *rect, GdkPoint *point) 
{
  g_assert(rect != NULL);
  g_assert(point != NULL);

  if (rect->x <= point->x &&
      rect->y <= point->y &&
      point->x <= rect->x + rect->width &&
      point->y <= rect->y + rect->height) {
    return TRUE;
  }

  return FALSE;
}


/** メッセージボックスを表示 */
void daemon_messagebox(Canvas* canvas, const char* s, GtkMessageType mes_type)
{
  GtkWidget *messagebox;

  messagebox = gtk_message_dialog_new(GTK_WINDOW(canvas->window),
           GtkDialogFlags(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
                                      mes_type,
                                      GTK_BUTTONS_CLOSE,
                                      "%s",
                                      s);
  gtk_dialog_run(GTK_DIALOG(messagebox));
  gtk_widget_destroy(messagebox);
}


/** OK, Cancel ボタンのダイアログ */
gint daemon_canvas_ok_cancel_dialog(Canvas* canvas, const char* message) 
{
  GtkWidget *dialog;
  gint reply;

  dialog = gtk_message_dialog_new(GTK_WINDOW(canvas->window),
           GtkDialogFlags(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
				  GTK_MESSAGE_QUESTION,
				  GTK_BUTTONS_OK_CANCEL,
				  "%s", 
				  message);
  reply = gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);

  return reply;
}


/** ヘルパー: Yes, No ダイアログ */
int daemon_canvas_yes_no_dialog(Canvas* canvas, const char* message) 
{
  GtkWidget *dialog;
  gint reply;

  dialog = gtk_message_dialog_new(GTK_WINDOW(canvas->window),
           GtkDialogFlags(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
				  GTK_MESSAGE_QUESTION,
				  GTK_BUTTONS_YES_NO,
				  "%s",
				  message);
  reply = gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);

  return reply;
}
