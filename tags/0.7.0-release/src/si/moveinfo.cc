/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "si.h"


/**
 * 新しくメモリを確保してMOVEINFOのポインタを返す。
 * @return MOVEINFOのポインタ
 */
MOVEINFO* newMOVEINFO()
{
  MOVEINFO *mi;
  mi = (MOVEINFO *) malloc(sizeof(MOVEINFO));
  if (mi == NULL) {
    si_abort("No enough memory. In moveinfo.c#newMOVEFINO()");
  }
  mi->count = 0;

  return mi;
}


/**
 * mi のメモリを解放する。
 * @param mi 対象のMOVEINFO
 */
void freeMOVEINFO(MOVEINFO* mi)
{
  assert(mi != NULL);
  free( mi );
} 


/** 
 * xy1からxy2に向かう方向.
 * @return xy1 == xy2 のときまたは8方向以外のとき 0. 桂馬のときは常に0
 */
XyDiff norm_direc(Xy xy1, Xy xy2) // __attribute__((const))
{
  if (xy1 == xy2)
    return 0;

  int const x1 = (xy1 & 0xf);
  int const y1 = (xy1 >> 4);
  int const x2 = (xy2 & 0xf);
  int const y2 = (xy2 >> 4);

  if (x1 == x2)
    return y2 > y1 ? +16 : -16;

  if (y1 == y2)
    return x1 < x2 ? +1 : -1;

  if (x1 < x2) {
    if (x2 - x1 == y2 - y1)
      return +17;
    else if (x2 - x1 == -(y2 - y1))
      return -15;
    else
      return 0;
  }
  else { // x1 > x2
    if (x2 - x1 == y2 - y1)
      return -17;
    else if (x2 - x1 == -(y2 - y1))
      return +15;
    else
      return 0;
  }
}


#ifndef USE_PIN
/**
 * 局面 bo で指し手 te を指したときに、自玉に相手方の (長い) 利きが
 * 掛かってしまうか
 * @param bo 対象のBOARD
 * @param te 指そうとする手
 * @param king_xy 自玉の場所
 * @param next 手を指そうとするプレイヤ
 * @return 王手がなければ 0 を返す。あれば 1 以上を返す。
 */
static int is_sunuki(const BOARD* bo, const TE& te, Xy const king_xy)
{
  assert(te_from(te) >= 0x11 && te_from(te) <= 0x99);

  BOARD* tmp_bo;
  int flag;
  int i;
  int const next = bo->next;

  if (!king_xy) // 詰将棋で自玉がない
    return 0;

  int to_king_diff = norm_direc(te_from(te), king_xy);
  if (!to_king_diff)
    return 0; // 玉と軸が違う

  int may_pinned = 0;
  for (i = 0; i < bo->long_attack[1 - next][te_from(te)].count; i++) {
    if ( norm_direc(bo->long_attack[1 - next][te_from(te)].from[i], te_from(te)) == 
	 to_king_diff ) {
      may_pinned = to_king_diff;
      break;
    }
  }
  if (!may_pinned)
    return 0;

  if ( norm_direc(te_from(te), te_to(te)) == may_pinned || 
       norm_direc(te_from(te), te_to(te)) == -may_pinned )
    return 0;
  
  // 実際に動かしてみる
  // TODO: 本当は利きだけ更新すればいい。

  tmp_bo = bo_dup(bo);
  boMove_mate( tmp_bo, te );

  flag = bo_is_checked(tmp_bo, next);

  freeBOARD(tmp_bo);

  return flag;
}
#endif // !USE_PIN


/**
 * 王が動く指し手を生成する
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param king_xy 王の位置
 * @param next 受け側の手番
 */
void mi_append_king_moves(const BOARD* bo, MOVEINFO* mi, Xy king_xy, int next) 
{
  static const XyDiff tolist[] = {
    -17, -16, -15,
    -1, 1, 
    15, 16, 17,
  };
  int i, j;

  for (i = 0; i < 8; i++) {
    Xy const to = king_xy + tolist[i];
    PieceKind const p = bo->board[ to ];

    if ( p == WALL )
      continue;
    if ( p && getTeban(p) == next )
      continue;

    if ( bo_attack_count(bo, 1 - next, to) )
      continue;

    // 長い利きが当たっているとき、引く手はダメ
    for (j = 0; j < bo->long_attack[1 - next][king_xy].count; j++) {
      int ng_direc = norm_direc( 
                       bo->long_attack[1 - next][king_xy].from[j], king_xy );
      if ( ng_direc == to - king_xy )
	goto NEXT;
    }
    miAdd( mi, te_make_move(king_xy, to, false) );
  NEXT: ;
  }
}


/**
 * MOVEINFO に指し手を追加する。
 * 歩、香、飛、角の成らずには STUPID フラグを付ける
 * 玉以外の駒を移動する手は必ずこの関数を通じて追加すること。(pinチェック)
 * @param bo 現在の局面
 * @param mi 対象のmi
 * @param te_ 追加するte
 */
void mi_add_with_pin_check( MOVEINFO* mi, const BOARD& bo, const TE& te_ )
{
  PieceKind pie;

  assert(mi != NULL);
  assert(1 <= (te_from(te_) >> 4) && (te_from(te_) >> 4) <= 9);
  assert(1 <= (te_from(te_) & 0xF) && (te_from(te_) & 0xF) <= 9);
  assert(1 <= (te_to(te_) >> 4) && (te_to(te_) >> 4) <= 9);
  assert(1 <= (te_to(te_) & 0xF) && (te_to(te_) & 0xF) <= 9);
  assert( bo.board[te_from(te_)] && getTeban(bo.board[te_from(te_)]) == bo.next );

  TE te = te_;
  te_set_hint( &te, 0 );

  assert( (bo.board[te_from(te)] & 0xf) != OH );
/*
  if ( (bo.board[te_from(te)] & 0xf) == OH ) {
    append_a_king_move( &bo, mi, te );
    return;
  }
*/

  // pinされていないか
#ifdef USE_PIN
  if ( bo.pinned[te_->fm] ) {
    if (bo.pinned[te_->fm] != norm_direc(te_->fm, te_->to) &&
	bo.pinned[te_->fm] != -norm_direc(te_->fm, te_->to) )
      return;
  }
#else
  if ( is_sunuki(&bo, te, bo.king_xy[bo.next]) )
    return;
#endif

  pie = boGetPiece( &bo, te_from(te)) & 0xF;
  if ( bo.next == GOTE ) {
    if ( !(te_from(te) >= 0x71 || te_to(te) >= 0x71) ) {
      if (!te_promote(te)) miAdd(mi, te);
      return;
    }

    // 成り強制か、stupidか
    switch (pie) {
    case FU:
      if (te_to(te) >= 0x91) {
        if (te_promote(te)) miAdd(mi, te);
      }
      else {
        if (te_promote(te)) 
          miAdd(mi, te);
	else {
          te_add_hint(&te, TE_STUPID); miAdd(mi, te);
	}
      }
      break;
    case KYO:
      if (te_to(te) >= 0x91) {
        if (te_promote(te)) miAdd(mi, te);
      }
      else if (te_to(te) >= 0x81) {  // 2段目のときのみ
	if (te_promote(te))
          miAdd(mi, te);
	else {
          te_add_hint(&te, TE_STUPID); miAdd(mi, te);
	}
      }
      else
        miAdd(mi, te);
      break;
    case KAKU: case HISHA:
      if (te_promote(te)) 
        miAdd(mi, te);
      else {
        te_add_hint(&te, TE_STUPID); miAdd(mi, te);
      }
      break;
    case KEI:
      if (te_to(te) >= 0x81) {
        if (te_promote(te)) miAdd(mi, te);
      }
      else 
        miAdd(mi, te);
      break;
    case GIN:
      miAdd(mi, te);
      break;
    default: // 金、成り駒
      if (!te_promote(te)) miAdd(mi, te);
      break;
    }
  }
  else { // 先手
    if ( !(te_from(te) <= 0x39 || te_to(te) <= 0x39) ) {
      if (!te_promote(te)) miAdd(mi, te);
      return;
    }

    switch (pie) {
    case FU:
      if (te_to(te) <= 0x19) {
        if (te_promote(te)) miAdd(mi, te);
      }
      else {
        if (te_promote(te))
          miAdd(mi, te);
	else {
          te_add_hint(&te, TE_STUPID); miAdd(mi, te);
	}
      }
      break;
    case KYO:
      if (te_to(te) <= 0x19) {
        if (te_promote(te)) miAdd(mi, te);
      }
      else if (te_to(te) <= 0x29) {  // 2段目のときのみ
        if (te_promote(te))
          miAdd(mi, te);
        else {
          te_add_hint(&te, TE_STUPID); miAdd(mi, te);
	}
      }
      else
        miAdd(mi, te);
      break;
    case KAKU: case HISHA:
      if (te_promote(te))
        miAdd(mi, te);
      else {
        te_add_hint(&te, TE_STUPID); miAdd(mi, te);
      }
      break;
    case KEI:
      if (te_to(te) <= 0x29) {
        if (te_promote(te)) miAdd(mi, te);
      }
      else
        miAdd(mi, te);
      break;
    case GIN:
      miAdd(mi, te);
      break;
    default: // 金、成り駒
      if (!te_promote(te)) miAdd(mi, te);
      break;
    }
  }
}


/**
 * MOVEINFO に指し手を追加する。成らずと成りの両方。
 * 歩、香、飛、角の成らずには STUPID フラグを付ける
 * 玉以外の駒を移動する手は必ずこの関数を通じて追加すること。(pinチェック)
 *
 * @param board 現在の局面
 * @param mi 対象のmi
 * @param te_ 追加するte
 */
void miAddWithNari( MOVEINFO* mi, const BOARD& board, Xy from, Xy to )
{
  assert(1 <= (from >> 4) && (from >> 4) <= 9);
  assert(1 <= (from & 0xF) && (from & 0xF) <= 9);
  // assert(1 <= (to >> 4) && (to >> 4) <= 9);
  // assert(1 <= (to & 0xF) && (to & 0xF) <= 9);
  assert( board.board[from] && getTeban(board.board[from]) == board.next );
  assert( (board.board[from] & 0xf) != OH );

  PieceKind const c = board.board[to];
  if ( c == WALL || (c && getTeban(c) == getTeban(board.board[from])) )
    return;

  TE te = te_make_move(from, to, false);
  PieceKind pie;

  // pinされていないか
#ifdef USE_PIN
  if ( board.pinned[te_->fm] ) {
    if (board.pinned[te_->fm] != norm_direc(te_->fm, te_->to) &&
	board.pinned[te_->fm] != -norm_direc(te_->fm, te_->to) )
      return;
  }
#else
  if ( is_sunuki(&board, te, board.king_xy[board.next]) )
    return;
#endif

  pie = boGetPiece(&board, from) & 0xF;
  if ( board.next == GOTE ) {
    switch (pie) {
    case FU:
      if (to >= 0x91) {
        te_set_promote(&te, true); miAdd(mi, te);
      }
      else if (to >= 0x71) {
        te_set_promote(&te, true); miAdd(mi, te);
        te_set_promote(&te, false); te_add_hint(&te, TE_STUPID); miAdd(mi, te);
      }
      else {
        te_set_promote(&te, false); miAdd(mi, te);
      }
      break;
    case KYO:
      if (to >= 0x91) {
        te_set_promote(&te, true); miAdd(mi, te);
      }
      else if (to >= 0x81) {  // 2段目のときのみ
        te_set_promote(&te, true); miAdd(mi, te);
        te_set_promote(&te, false); te_add_hint(&te, TE_STUPID); miAdd(mi, te);
      }
      else if (to >= 0x71) {
        te_set_promote(&te, true); miAdd(mi, te);
        te_set_promote(&te, false); miAdd(mi, te);
      }
      else {
        te_set_promote(&te, false); miAdd(mi, te);
      }
      break;
    case KAKU: case HISHA:
      if ( 0x70 <= from || 0x70 <= to ) {
        te_set_promote(&te, true); miAdd(mi, te);
        te_set_promote(&te, false); te_add_hint(&te, TE_STUPID); miAdd(mi, te);
      }
      else {
        te_set_promote(&te, false); miAdd(mi, te);
      }
      break;
    case KEI:
      if (to >= 0x81) {
        te_set_promote(&te, true); miAdd(mi, te);
      }
      else if (to >= 0x71) {
        te_set_promote(&te, true); miAdd(mi, te);
        te_set_promote(&te, false); miAdd(mi, te);
      }
      else {
        te_set_promote(&te, false); miAdd(mi, te);
      }
      break;
    case GIN:
      if (from >= 0x71 || to >= 0x71) {
        te_set_promote(&te, true); miAdd(mi, te);
      }
      te_set_promote(&te, false); miAdd(mi, te);
      break;
    default: // 金、玉、成り駒
      te_set_promote(&te, false); miAdd(mi, te);
      break;
    }
  }
  else { // 先手
    switch (pie) {
    case FU:
      if (to <= 0x19) {
        te_set_promote(&te, true); miAdd(mi, te);
      }
      else if (to <= 0x39) {
        te_set_promote(&te, true); miAdd(mi, te);
        te_set_promote(&te, false); te_add_hint(&te, TE_STUPID); miAdd(mi, te);
      }
      else {
        te_set_promote(&te, false); miAdd(mi, te);
      }
      break;
    case KYO:
      if (to <= 0x19) {
        te_set_promote(&te, true); miAdd(mi, te);
      }
      else if (to <= 0x29) {  // 2段目のときのみ
        te_set_promote(&te, true); miAdd(mi, te);
        te_set_promote(&te, false); te_add_hint(&te, TE_STUPID); miAdd(mi, te);
      }
      else if (to <= 0x39) {
        te_set_promote(&te, true); miAdd(mi, te);
        te_set_promote(&te, false); miAdd(mi, te);
      }
      else {
        te_set_promote(&te, false); miAdd(mi, te);
      }
      break;
    case KAKU: case HISHA:
      if (from <= 0x39 || to <= 0x39) {
        te_set_promote(&te, true); miAdd(mi, te);
        te_set_promote(&te, false); te_add_hint(&te, TE_STUPID); miAdd(mi, te);
      }
      else {
        te_set_promote(&te, false); miAdd(mi, te);
      }
      break;
    case KEI:
      if (to <= 0x29) {
        te_set_promote(&te, true); miAdd(mi, te);
      }
      else if (to <= 0x39) {
        te_set_promote(&te, true); miAdd(mi, te);
        te_set_promote(&te, false); miAdd(mi, te);
      }
      else {
        te_set_promote(&te, false); miAdd(mi, te);
      }
      break;
    case GIN:
      if (from <= 0x39 || to <= 0x39) {
        te_set_promote(&te, true); miAdd(mi, te);
      }
      te_set_promote(&te, false); miAdd(mi, te);
      break;
    default: // 金、玉、成り駒
      te_set_promote(&te, false); miAdd(mi, te);
      break;
    }
  }
}


/**
 * 手の番号を指定してmiからteを削除して間が空いたら詰める。
 * @param mi 対象のMOVEINFO
 * @param num 削除する手の番号
 */
void miRemoveTE2( MOVEINFO* mi, int num ) 
{
#ifdef DEBUG
  assert( mi != NULL );
  assert( 0 <= num && num <= mi->count );
#endif /* DEBUG */
  
  if ( mi->count - 1 == num ) {
    mi->count--;
    return;
  }
      
  mi->te[ num ] = mi->te[ mi->count - 1 ];
  mi->count--;
  return;
}


/**
 * miから重複する手を削除する。削除するときは先に登録された手を残す。
 * @param mi 対象のMOVEINFO
 */ 
void miRemoveDuplicationTE(MOVEINFO* mi) 
{
  int i, j;

#ifdef DEBUG
  assert( mi != NULL );
#endif /* DEBUG */

  for ( i=0; i<mi->count; i++ ) {
    for ( j=i+1; j<mi->count; ) {
      if ( ! teCmpTE( mi->te[ i ], mi->te[ j ] ) ) {
	miRemoveTE2( mi, j );
      } else {
	j++;
      }
    }
  }
}


/**
 * src の内容を dst へコピーする。
 * @param src コピー元
 * @param dest コピー先
 */
void miCopy(MOVEINFO* dest, const MOVEINFO* src)
{
  assert(dest != NULL);
  assert(src != NULL);
  // assert(0 <= src->count && src->count <= MOVEINFOMAX);

  memcpy(dest, src, sizeof(MOVEINFO));
}


/** 手の一覧を表示する */
void mi_print( const MOVEINFO* mi, const BOARD& bo )
{
  assert(mi);

  for (int i = 0; i < mi->count; i++)
    printf( "%s ", te_str(mi->te[i], bo).c_str() );

  printf("\n");
}


/** @return 見つかったとき true */
bool mi_includes( const MOVEINFO* mi, const TE& te ) 
{
  for (int i = 0; i < mi->count; i++) {
    if ( !teCmpTE(mi->te[i], te) )
      return true;
  }
  return false;
}
