/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iconv.h>
#include "filewriter.h"
#include "si.h"


/** コンストラクタ */
FileWriter::FileWriter(): 
  out(NULL), stat(D_FILEWRITER_SUCCESSFUL), cd((iconv_t) -1)
{
  daemon_filewriter_set_outcode( this, "CP932");
}


/** オブジェクトの生成 */
FileWriter* daemon_filewriter_new() 
{
  FileWriter* writer = new FileWriter();
  if (writer == NULL) {
    si_abort("No enough memory in daemon_filewriter_new().\n");
  }

  return writer;
}


int FileWriter::fdopen(int fd)
{
  out = ::fdopen(fd, "w");
  assert( out );
  return 1;
}


/** デストラクタ */
FileWriter::~FileWriter() 
{
  if ( out ) {
    daemon_filewriter_close(this);
    out = NULL;
  }
}


/** オブジェクトの解放 */
void daemon_filewriter_free(FileWriter* writer) 
{
  if (writer)
    delete writer;
}


/** 
 * 書き込み用にファイルを開く.
 * @return 失敗したとき -1
 */
int daemon_filewriter_open(FileWriter* writer, const char* filename) 
{
  assert(writer != NULL);
  assert(filename != NULL);

  writer->out = fopen( filename, "w");
  if (writer->out == NULL) {
    writer->stat = D_FILEWRITER_ERROR;
    return -1;
  }

  return 0;
}


/** ファイルを閉じる */
void daemon_filewriter_close(FileWriter* writer) 
{
  assert(writer != NULL);

  iconv_close(writer->cd);
  fclose(writer->out);
  writer->out = NULL;
}


/** 文字列を出力する */
void daemon_filewriter_put(FileWriter* writer, const char* src) 
{
  size_t isize;
  size_t osize;
  char *outbuf, *result;
  size_t r;

  assert(writer != NULL);
  assert(src != NULL);

  if ( writer->cd == (iconv_t) -1 ) {
    writer->cd = iconv_open(writer->outcode.c_str(), "UTF-8");
    if (writer->cd == (iconv_t) -1) {
      printf("%s: iconv_open() error.\n", __func__); // DEBUG
      fclose(writer->out);
      writer->out = NULL;
      writer->stat = D_FILEWRITER_ERROR;
      return;
    }
  }

  isize = strlen(src);
  osize = isize * 3;
  const char* inbuf = src;
  result = outbuf = (char*) malloc(osize);
  memset(result, 0, osize);

  r = iconv(writer->cd, const_cast<char**>(&inbuf), &isize, &outbuf, &osize);
  if (r == (size_t) -1) {
    fprintf(stderr, "%s: iconv() error.\n", __func__);
    iconv(writer->cd, NULL, 0, NULL, 0); // reset
  }
  *outbuf = '\0';

  fprintf(writer->out, "%s", result);
  free(result);
}


/** 出力コードをセットする。 */
void daemon_filewriter_set_outcode(FileWriter* writer, const char* outcode) 
{
  writer->outcode = outcode;
}


/** ファイル読み込み状況を返す */
D_FILEWRITER_STAT daemon_filewriter_get_stat(const FileWriter* writer) 
{
  return writer->stat;
}
