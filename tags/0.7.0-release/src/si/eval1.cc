/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// 静的評価関数

#include "si-base.h"
#if EVAL_VERSION == 1

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <algorithm>
#include <fstream>
#include "si.h"
#include "si-think.h"
#include "picojson.h"
using namespace std;


static NPC g_npc;


static Xy rotate_if( Xy const xy, int side ) __attribute__((const));
static Xy rotate_if( Xy const xy, int side ) 
{
  if (side == 0)
    return xy;
  else 
    return ((10 - (xy >> 4)) << 4) + 10 - (xy & 0xf);
}


/**
 * 盤面の駒の種類位置、持駒の数から盤面評価点数を計算して返す。
 * @param bo 対象のBOARD
 * @param next 計算する手番. boの手番と同じとは限らない
 * @return 盤面評価点数
 */
int grade(const BOARD* bo, int next)
{
  int point;
  int i, j;

  point = 0;

  // 盤上の駒
  for (i = 1; i <= 81; i++) {
    PieceKind const p = boGetPiece( bo, arr_xy[i] );
    if ( p && getTeban(p) == next )
      point += add_grade( bo, arr_xy[i], next );
  }

  // 持ち駒
  for (i = 1; i <= 7; i++) {
    for (j = 0; j < bo->inhand[next][i]; j++)
      point += score_inhand(bo, i, next);
  }

  /* 王の安全度の評価 */
  // point -= add_oh_safe_grade(bo, next);

  return point;
}


/**
 * xy の位置の駒の点数
 * @param bo   対象のBOARD
 * @param xy   対象の駒の位置
 * @param next bo->board[xy] のプレイヤーと同じ
 */
int add_grade( const BOARD* bo, Xy xy, int next )
{
  assert( xy >= 0x11 && xy <= 0x99 );
  assert( bo->board[xy] != 0 );
  assert( bo->board[xy] != WALL );
#ifndef NDEBUG
  if ( getTeban(bo->board[xy]) != next ) {
    printBOARD( bo );
    printf("xy = %x, next = %d\n", xy, next );
    abort();
  }
#endif // !NDEBUG

  int kx, ky;
  int jx, jy;
  int x, y;
  // int piece_p; 
  int score;
  
  /* 自玉の位置 */
  Xy const king_xy = bo->king_xy[next];
  kx = king_xy & 0xF;
  ky = king_xy >> 4;

  /* 相手玉の位置 */
  Xy oppo_king_xy = bo->king_xy[1 - next];
  jx = oppo_king_xy & 0xF;
  jy = oppo_king_xy >> 4;
  if ( !king_xy || !oppo_king_xy )
    return 0;

  x = xy & 0xF;
  y = xy >> 4;

  PieceKind const p = boGetPiece(bo, xy) & 0xf;
  
  if ( p == OH )
    return 0; // 玉は別途評価
    // return g_npc.king_xy_points[ rotate_if(xy, next) ] * 100;

  if ( is_long_piece(p) ) {
    /* 飛車、角の場合は位置によるポイント増減はしない */
    score = g_npc.base_piece_points[ p ] * 100;
  }
  else {
    int v, w;

    // 自玉との距離
    v = abs(kx - x);
    w = ( next == SENTE ) ? ky - y : y - ky;
    score = g_npc.base_piece_points[p] * g_npc.defense_relative_mod[8 + w][v];

    // 相手玉との距離
    v = abs(jx - x);
    w = ( next == SENTE ) ? y - jy : jy - y;
    score = score * g_npc.offense_relative_mod[8 + w][v] / 100;

    if ( p == FU )
      score += g_npc.fu_position_bonus * (next == SENTE ? 9 - y : y - 1);
  }

#if 0
  if ( (p & 0x7) == HISHA ) {
    /* 「王飛接近するべからず」の格言通り王と飛車が近い場合は減点する */
    int hd = max( abs(kx - x), abs(ky - y) );
    if ( hd <= 2 ) {
      score -= (3 - hd) * OH_HISYA_POINT;
    }
  }
#endif // 0

  /* 駒の動ける数を評価に加える */
  // movables は boMove() で正しく更新できないので、think_eval() で計算する
  // score += bo_piece_kiki_count(bo, n) * NUM_OF_MOVE_POINT;

  return score;
}


/**
 * 持ち駒 piece 1枚の得点。
 * 増やすときはboを変更した後で呼び出すこと。減らすときはboを変更する前。
 * \todo 歩切れの評価など
 *
 * @param bo 対象のBOARD
 * @param piece 取った駒
 * @param next 盤面評価点数を増減する手番. boの手番と同じとは限らない
 */
int score_inhand(const BOARD* bo, PieceKind piece, int next ) 
{
  assert( 1 <= piece && piece <= 7 );

  return g_npc.inhand_points[piece] * 100;
}


/** 
 * 盤上の駒の得点 (の合計) を得る
 */
static int32_t onboard_piece_value(const BOARD* bo )
{
#if 0
  unsigned char tbl[16 * 10];

  memset(tbl, 0, sizeof tbl);
  // 自玉の近傍
  update_mask( tbl, bo, bo->king_xy[bo->next], DEFE );
  // 相手玉
  update_mask( tbl, bo, bo->king_xy[1 - bo->next], OFFE );
#endif // 0

  int32_t score = 0;

  // 盤上の駒
  for (int i = 1; i <= 81; i++) {
    Xy const xy = arr_xy[i];
    PieceKind const pie = bo->board[xy];

    if ( !pie )
      continue;

    // 駒の基礎的な価値
    // update_onboard_piece_value( /* tbl, */ bo, xy, score );

    // 大駒の可動性を加える.
    if ( is_long_piece(pie) ) {
      if ( getTeban(pie) == bo->next ) {
	score += bo->piece_kiki_count[xy] * 
	              g_npc.attack_count_bonus[pie & 0xf];
      }
      else {
	score -= bo->piece_kiki_count[xy] * 
                      g_npc.attack_count_bonus[pie & 0xf];
      }
    }

    // 相手駒への当たり. これは盤面全体を対象にしていい
    if ( getTeban(pie) != bo->next ) {
      if ( bo_attack_count(bo, bo->next, xy) )
	score += g_npc.base_piece_points[pie & 0x7] * g_npc.atari_mod;
    }
    else {
      if ( bo_attack_count(bo, 1 - bo->next, xy) )
	score -= g_npc.base_piece_points[pie & 0x7] * g_npc.atari_mod;
    }
  }

  return score;
}


#ifdef USE_NPC
static void update_king_safe(const BOARD* bo, PhiDelta* score)
{
  int yd, xd;

  // 相手玉の周りの利き制圧度: phi
  Xy const oppo_king_xy = bo->king_xy[1 - bo->next];
  for (yd = -3; yd <= +3; yd++) {
    for (xd = -3; xd <= +3; xd++) {
      int const x = (oppo_king_xy & 0xf) + xd;
      int const y = (oppo_king_xy >> 4) + yd;
      if (x < 1 || x > 9 || y < 1 || y > 9)
	continue;

      Xy const xy = (y << 4) + x;
      int v = abs( (oppo_king_xy & 0xf) - x );
      int w = bo->next == SENTE ? y - (oppo_king_xy >> 4) : 
                                  (oppo_king_xy >> 4) - y;
      score->phi += ( bo_attack_count(bo, bo->next, xy) - 
                      bo_attack_count(bo, 1 - bo->next, xy) ) * 
               100 * g_npc.control_around_oppo_king[w + 8][v];
    }
  }

  // 相手玉の自由度
  score->phi -= g_npc.king_movables[ think_count_king_movable2(bo, 1 - bo->next) ];

  // 自玉の周りの利き制圧度: delta
  Xy const my_king_xy = bo->king_xy[bo->next];
  for (yd = -3; yd <= +3; yd++) {
    for (xd = -3; xd <= +3; xd++) {
      int const x = (my_king_xy & 0xf) + xd;
      int const y = (my_king_xy >> 4) + yd;
      if (x < 1 || x > 9 || y < 1 || y > 9)
	continue;

      Xy const xy = (y << 4) + x;
      int v = abs( (my_king_xy & 0xf) - x );
      int w = bo->next == SENTE ? (my_king_xy >> 4) - y : 
                                  y - (my_king_xy >> 4);
      score->delta -= ( bo_attack_count(bo, bo->next, xy) - 
                        bo_attack_count(bo, 1 - bo->next, xy) ) *
                   100 * g_npc.control_around_oppo_king[w + 8][v];
    }
  }

  // 自玉の自由度
  score->delta -= g_npc.king_movables[ think_count_king_movable2(bo, bo->next) ];
}
#endif // USE_NPC


/**
 * bo の手番から見た評価値.
 * phi,delta = (+VAL_INF, -VAL_INF) で勝ち
 */
int32_t think_eval(const BOARD* bo)
{
  PhiDelta score; // TODO: phi, deltaではない

  // 基礎点数. phi, delta 両方に影響
  // bo->point[] は盤上の駒の基礎点数と持ち駒。 
  score.phi = ( onboard_piece_value(bo) +
		(bo->point[bo->next] - bo->point[1 - bo->next]) ) / 2;
  score.delta = -score.phi;

#if 0
  // 持ち駒は両方に加算
  int i;
  for (i = 1; i <= 7; i++) {
    score->phi += g_npc.inhand_points[i] * 100 * bo->inhand[bo->next][i];
    // score->delta -= g_npc.inhand_points[i] * 100 * bo->inhand[bo->next][i];

    score->phi -= g_npc.inhand_points[i] * 100 * bo->inhand[1 - bo->next][i];
    // score->delta += g_npc.inhand_points[i] * 100 * bo->inhand[1 - bo->next][i];
  }    
#endif // 0

  // phi, delta のどちらかだけに影響するもの

  // 自玉
  score.delta -= g_npc.king_xy_points[ rotate_if(bo->king_xy[bo->next], bo->next) ] * 100;

  // 相手玉
  score.phi -= g_npc.king_xy_points[ rotate_if(bo->king_xy[1 - bo->next], 1 - bo->next) ] * 100;

  // 玉の周りの安全度 (危険度)
  update_king_safe(bo, &score);

  return score.phi - score.delta;
}


template <typename T>
const picojson::value& json_map_value(const picojson::value& v, const string& key)
{
  const picojson::value& r = v.get(key);
  if ( !r.is<T>() )
    throw 1;
  return r;
}


/** 
 * 初期化
 * @return エラーがあったとき false 
 */
bool think_init( const string& filename )
{
  ifstream ifs( filename.c_str() );
  if ( ifs.fail() ) {
    // fprintf(stderr, "Cannot open file: %s\n", filename.c_str() );
    return false;
  }

  picojson::value root;
  string err = picojson::parse(root, ifs);
  if (err != "") {
    cout << err << endl;
    return false;
  }

#ifdef USE_NPC
  // 構造体に設定する
  try {
    int i, j;
    picojson::value v;

    v = json_map_value<picojson::array>(root, "base_piece_points");
    for (i = 0; i < 16; i++)
      g_npc.base_piece_points[i] = v.get(i).get<double>();

    v = json_map_value<picojson::array>(root, "offense_relative_mod");
    for (i = 0; i < 17; i++) {
      for (j = 0; j < 9; j++)
	g_npc.offense_relative_mod[i][j] = v.get(i).get(j).get<double>();
    }

    v = json_map_value<picojson::array>(root, "defense_relative_mod");
    for (i = 0; i < 17; i++) {
      for (j = 0; j < 9; j++)
	g_npc.defense_relative_mod[i][j] = v.get(i).get(j).get<double>();
    }

    v = json_map_value<picojson::array>(root, "inhand_points");
    for (i = 0; i < 8; i++)
      g_npc.inhand_points[i] = v.get(i).get<double>();

    v = json_map_value<picojson::array>(root, "king_xy_points");
    for (i = 0; i < 16 * 10; i++) 
      g_npc.king_xy_points[i] = v.get(i).get<double>();

    v = json_map_value<double>(root, "fu_position_bonus");
    g_npc.fu_position_bonus = v.get<double>();

    v = json_map_value<picojson::array>(root, "attack_count_bonus");
    for (i = 0; i < 16; i++)
      g_npc.attack_count_bonus[i] = v.get(i).get<double>();

    v = json_map_value<double>(root, "atari_mod");
    g_npc.atari_mod = v.get<double>();

    v = json_map_value<picojson::array>(root, "control_around_oppo_king");
    for (i = 0; i < 17; i++) {
      for (j = 0; j < 9; j++)
	g_npc.control_around_oppo_king[i][j] = v.get(i).get(j).get<double>();
    }

    v = json_map_value<picojson::array>(root, "king_movables");
    for (i = 0; i < 25; i++) 
      g_npc.king_movables[i] = v.get(i).get<double>();
  }
  catch (int& ) {
    fprintf(stderr, "Type mismatch.\n");
    return false;
  }
#endif // USE_NPC

  return true;
}

#endif // EVAL_VERSION == 1
