/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <tcutil.h>  // TCMAP
#include <tchdb.h>
#include <stddef.h> // offsetof
#include "si.h"
#include "si-think.h"
#include <iostream>
using namespace std;


// #define HASH_ON_FILE 1
#undef HASH_ON_FILE


#define TTABLE_COUNT 6

/**
 * 置換表 transposition table. [0], [1] は詰め将棋用、[2], [3] は必至探索用
 * オンメモリは LRUアルゴリズムで表から追い出す.
 * TCMAPは末尾に追加される。先頭を追い出すだけでいい。
 *
 * \todo 分散化するときに Tokyo Tyrantなどに移行
 * http://alpha.mixi.co.jp/blog/?p=166
 */
static TCMAP* ttable_mate[ TTABLE_COUNT ] = {NULL, NULL, NULL, NULL, NULL, NULL};

#ifdef HASH_ON_FILE
static TCHDB* ttable = NULL;
#else
/** 指し将棋用 */
static TCMAP* ttable = NULL;
#endif 


/** 1レコード48バイトとして、データで30Mバイト. */
#define TTABLE_ENT_COUNT 625000


/**
 * ハッシュテーブル用にメモリを確保する。
 */
void newHASH()
{
#ifndef NDEBUG
  assert( (sizeof(EntVariant) % 8) == 0 );
  assert( sizeof(EntVariant) == HASH_ENT_SIZE );
  assert( (sizeof(HASH) % 8) == 0 );

  if ( sizeof(HASH) != HASH_HEADER_SIZE + HASH_ENT_SIZE ||
       offsetof(HASH, ent) != HASH_HEADER_SIZE ) {
    printf( "entry size: expected = %d, actual = %ld\n",
            HASH_HEADER_SIZE + HASH_ENT_SIZE, sizeof(HASH) );
    printf( "entry header size: expected = %d, actual = %ld\n",
            HASH_HEADER_SIZE, offsetof(HASH, ent) );
    abort();
  }
#endif // !NDEBUG

  if (ttable)
    freeHASH();

  for (int i = 0; i < TTABLE_COUNT; i++) 
    ttable_mate[ i ] = tcmapnew();

#ifdef HASH_ON_FILE
  ttable = tchdbnew();
  bool r = tchdbsetcache( ttable, TTABLE_ENT_COUNT );
  assert( r );
  r = tchdbopen(ttable, "/tmp/daemonshogi.bin", HDBOWRITER | HDBOCREAT );
  assert( r );
#else
  ttable = tcmapnew();
#endif
}


/** 
 * ハッシュテーブルを解放する。
 */
void freeHASH()
{
  if (ttable) {
    for (int i = 0; i < TTABLE_COUNT; i++) {
      tcmapdel( ttable_mate[i] );
      ttable_mate[ i ] = NULL;
    }

#ifdef HASH_ON_FILE
    tchdbclose(ttable);
    tchdbdel(ttable);
#else
    tcmapdel(ttable);
#endif
    ttable = NULL;
  }
}


/**
 * BOARD からハッシュ値を生成する。持ち駒は考慮しない。
 * @param bo 対象のBOARD
 * @return uint64_t ハッシュ値 (64bit)
 */
uint64_t hsGetHashKey(const BOARD* bo)
{
  assert( bo != NULL );

  uint64_t key = 0;
  int i;

  for ( i = 1; i <= 81; i++ ) {
    // if ( bo->board[arr_xy[i]] )
    key ^= hashval[bo->board[ arr_xy[ i ] ]][ arr_xy[ i ] ];
  }

  return bo->next == SENTE ? key : ~key;
}


/**
 * HASHに持駒情報をセットする。
 * @param hs 対象のエントリ
 * @param bo 対象のBOARD
 */
static void hsSetPiece(EntVariant* hs, const BOARD* bo)
{
  hs->packed_inhand[ SENTE ] =
    bo->inhand[ SENTE ][ 1 ] +   // 18ヶ ... 5ビット
    (bo->inhand[ SENTE ][ 2 ] << 5) +
    (bo->inhand[ SENTE ][ 3 ] << 8) +
    (bo->inhand[ SENTE ][ 4 ] << 11) +
    (bo->inhand[ SENTE ][ 5 ] << 14) +
    (bo->inhand[ SENTE ][ 6 ] << 17) +
    (bo->inhand[ SENTE ][ 7 ] << 19);

  hs->packed_inhand[ GOTE ] =
    bo->inhand[ GOTE ][ 1 ] +
    (bo->inhand[ GOTE ][ 2 ] << 5) +
    (bo->inhand[ GOTE ][ 3 ] << 8) +
    (bo->inhand[ GOTE ][ 4 ] << 11) +
    (bo->inhand[ GOTE ][ 5 ] << 14) +
    (bo->inhand[ GOTE ][ 6 ] << 17) +
    (bo->inhand[ GOTE ][ 7 ] << 19);
}


#ifdef USE_PROOF_PIECES
/** 呼び出した側で boSetToBOARD() を呼び出すこと */
static void unpack_inhand(const EntVariant* hs, BOARD* bo)
{
  int32_t p = hs->packed_inhand[0];
  int32_t q = hs->packed_inhand[1];

  bo->inhand[ SENTE ][ FU ] = (p & 0x1f);
  bo->inhand[ SENTE ][ KYO ] = ((p >> 5) & 0x7);
  bo->inhand[ SENTE ][ KEI ] = ((p >> 8) & 0x7);
  bo->inhand[ SENTE ][ GIN ] = ((p >> 11) & 0x7);
  bo->inhand[ SENTE ][ KIN ] = ((p >> 14) & 0x7);
  bo->inhand[ SENTE ][ KAKU ] = ((p >> 17) & 0x3);
  bo->inhand[ SENTE ][ HISHA ] = ((p >> 19) & 0x3);

  bo->inhand[ GOTE ][ FU ] = (q & 0x1f);
  bo->inhand[ GOTE ][ KYO ] = ((q >> 5) & 0x7);
  bo->inhand[ GOTE ][ KEI ] = ((q >> 8) & 0x7);
  bo->inhand[ GOTE ][ GIN ] = ((q >> 11) & 0x7);
  bo->inhand[ GOTE ][ KIN ] = ((q >> 14) & 0x7);
  bo->inhand[ GOTE ][ KAKU ] = ((q >> 17) & 0x3);
  bo->inhand[ GOTE ][ HISHA ] = ((q >> 19) & 0x3);
}
#endif // USE_PROOF_PIECES


static EntVariant make_v(const BOARD* bo, int32_t phi, int32_t delta,
                         int te_count, const TE& te)
{
  EntVariant v;

  hsSetPiece(&v, bo);
  v.phi = phi; v.delta = delta;
  v.te_count = te_count;
  v.best_move = te_pack(te);

  return v;
}


/**
 * 持駒を比較する。
 * @param hs 対象のHASH
 * @param bo 対象のBOARD
 * @return 持駒が同じなら 0 を返す。相違がある場合は 1 を返す。
 */
static int hsCmpPiece(const EntVariant* hs, const BOARD* bo)
{
  int next = bo->next;
  int oppo = 1 - bo->next;
  int32_t p = hs->packed_inhand[ next ];
  int32_t q = hs->packed_inhand[ oppo ];

  if ( bo->inhand[ next ][ FU ] != (p & 0x1f) ||
       bo->inhand[ next ][ KYO ] != ((p >> 5) & 0x7) ||
       bo->inhand[ next ][ KEI ] != ((p >> 8) & 0x7) ||
       bo->inhand[ next ][ GIN ] != ((p >> 11) & 0x7) ||
       bo->inhand[ next ][ KIN ] != ((p >> 14) & 0x7) ||
       bo->inhand[ next ][ KAKU ] != ((p >> 17) & 0x3) ||
       bo->inhand[ next ][ HISHA ] != ((p >> 19) & 0x3) ||

       bo->inhand[ oppo ][ FU ] != (q & 0x1f) ||
       bo->inhand[ oppo ][ KYO ] != ((q >> 5) & 0x7) ||
       bo->inhand[ oppo ][ KEI ] != ((q >> 8) & 0x7) ||
       bo->inhand[ oppo ][ GIN ] != ((q >> 11) & 0x7) ||
       bo->inhand[ oppo ][ KIN ] != ((q >> 14) & 0x7) ||
       bo->inhand[ oppo ][ KAKU ] != ((q >> 17) & 0x3) ||
       bo->inhand[ oppo ][ HISHA ] != ((q >> 19) & 0x3) ) {
    return 1;
  }

  return 0;
}


/**
 * 盤面の優越関係を調べる。
 * 手番側の持ち駒が同じか多く、かつ相手側の持ち駒が同じか少ないとき
 * 優越しているという。
 * ある局面で詰みがあるとき、その局面を優越している局面でも詰む。
 *
 * @param hs 対象のHASH
 * @param bo 対象のBOARD
 *
 * @return hsでの持ち駒がboでの持ち駒を優越しているとき > 0, 逆のとき < 0, 
 *         どちらも優越していないとき0
 */
static int hsSuperiorBOARD(const EntVariant* hs, const BOARD* bo)
{
  int next = bo->next;
  int oppo = 1 - bo->next;
  int32_t p = hs->packed_inhand[ next ];
  int32_t q = hs->packed_inhand[ oppo ];

  // 駒箱に駒があることがあるので、両sideを調べる。
  if ( bo->inhand[ next ][ FU ] >= (p & 0x1f) &&
       bo->inhand[ next ][ KYO ] >= ((p >> 5) & 0x7) &&
       bo->inhand[ next ][ KEI ] >= ((p >> 8) & 0x7) &&
       bo->inhand[ next ][ GIN ] >= ((p >> 11) & 0x7) &&
       bo->inhand[ next ][ KIN ] >= ((p >> 14) & 0x7) &&
       bo->inhand[ next ][ KAKU ] >= ((p >> 17) & 0x3) &&
       bo->inhand[ next ][ HISHA ] >= ((p >> 19) & 0x3) &&

       bo->inhand[ oppo ][ FU ] <= (q & 0x1f) &&
       bo->inhand[ oppo ][ KYO ] <= ((q >> 5) & 0x7) &&
       bo->inhand[ oppo ][ KEI ] <= ((q >> 8) & 0x7) &&
       bo->inhand[ oppo ][ GIN ] <= ((q >> 11) & 0x7) &&
       bo->inhand[ oppo ][ KIN ] <= ((q >> 14) & 0x7) &&
       bo->inhand[ oppo ][ KAKU ] <= ((q >> 17) & 0x3) &&
       bo->inhand[ oppo ][ HISHA ] <= ((q >> 19) & 0x3) ) {
    return -1;
  }
  else if ( bo->inhand[ next ][ FU ] <= (p & 0x1f) &&
            bo->inhand[ next ][ KYO ] <= ((p >> 5) & 0x7) &&
            bo->inhand[ next ][ KEI ] <= ((p >> 8) & 0x7) &&
            bo->inhand[ next ][ GIN ] <= ((p >> 11) & 0x7) &&
            bo->inhand[ next ][ KIN ] <= ((p >> 14) & 0x7) &&
            bo->inhand[ next ][ KAKU ] <= ((p >> 17) & 0x3) &&
            bo->inhand[ next ][ HISHA ] <= ((p >> 19) & 0x3) &&

            bo->inhand[ oppo ][ FU ] >= (q & 0x1f) &&
            bo->inhand[ oppo ][ KYO ] >= ((q >> 5) & 0x7) &&
            bo->inhand[ oppo ][ KEI ] >= ((q >> 8) & 0x7) &&
            bo->inhand[ oppo ][ GIN ] >= ((q >> 11) & 0x7) &&
            bo->inhand[ oppo ][ KIN ] >= ((q >> 14) & 0x7) &&
            bo->inhand[ oppo ][ KAKU ] >= ((q >> 17) & 0x3) &&
            bo->inhand[ oppo ][ HISHA ] >= ((q >> 19) & 0x3) ) {
    return 1;
  }

  return 0;
}


static const HASH* hs_find_entry(int tag, const BOARD* bo, int* size)
{
  return static_cast<const HASH*>(
    tcmapget(ttable_mate[tag], &bo->key, sizeof(bo->key), size) );
}


#ifdef USE_PROOF_PIECES
/**
 * 攻め方の持ち駒最小で勝ちの局面にする。
 * 呼び出した側で boSetToBOARD() すること
 * @param offense_side 攻め方プレイヤー
 * @param bo 局面. 更新する
 *
 * @return 優越している局面があったとき true
 */
bool hs_minimize_proof_pieces(int offense_side, BOARD* bo)
{
  bool ret = false;

  int size = 0;
  const HASH* cntr = hs_find_entry(offense_side, bo, &size);
  if (!cntr)
    return false;

  int i;
  for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
    const EntVariant* ent = cntr->ent + i;

    if (bo->next == offense_side) {
      if ( ent->phi == 0 && ent->delta >= +VAL_INF ) {
        if (hsSuperiorBOARD(ent, bo) < 0) {
          unpack_inhand(ent, bo);
          ret = true;
        }
      }
    }
    else {
      if ( ent->phi >= +VAL_INF && ent->delta == 0 ) {
        if ( hsSuperiorBOARD(ent, bo) > 0 ) {
          unpack_inhand(ent, bo);
          ret = true;
        }
      }
    }
  }

  return ret;
}
#endif // USE_PROOF_PIECES


#ifdef NDEBUG
  #define assert_tt_put(bo, best_move) (static_cast<void>(0))
#else
void assert_tt_put(const BOARD* bo, const TE& best_move)
{
  assert(bo);

  if ( te_to(best_move) && te_is_put(best_move) ) {
    if (bo->inhand[bo->next][te_pie(best_move) & 0x7] <= 0) {
      printBOARD(bo);
      printf("add move = %s\n", te_str(best_move, *bo).c_str() );
      si_abort("inhand is %d\n", bo->inhand[bo->next][te_pie(best_move) & 0x7]);
    }
  }
}
#endif // !NDEBUG


/**
 * 局面を置換表に登録する。詰め将棋用。
 *
 * @param tag 攻め方とレベル
 * @param bo 対象のBOARD
 * @param phi   攻め方ノードでは証明数.
 * @param delta 攻め方ノードでは反証数. (phi, delta) = (0, +INF) で勝ち
 * @param te_count 手数. 深さではない.
 * @param best_move 最善手. 投了はNULL
 */
void hsPutBOARD( int tag, const BOARD* bo, 
                 int32_t phi, int32_t delta, int rest_depth,
                 const TE& best_move )
{
  assert_tt_put(bo, best_move);

  int size = 0;
  // エンディアンは考慮しない。
  // TODO: エンディアンの異なるノードが参加するときは要修正
  const HASH* old_ent = hs_find_entry(tag, bo, &size);

  if (!old_ent) {
    HASH n;
    n.key = bo->key;
    n.ent[0] = make_v(bo, phi, delta, rest_depth, best_move);
    tcmapput( ttable_mate[tag], &bo->key, sizeof(bo->key), 
	      &n, sizeof(HASH) );
    return;
  }

  HASH* new_ent = static_cast<HASH*>( malloc(size + HASH_ENT_SIZE) );
  new_ent->key = bo->key;

  int count = 0;
  bool to_add = true;
  int i;

  if ( phi == 0 && delta >= VAL_INF ) {
    // 勝ち. 攻め方の持ち駒がより多い情報は捨ててよい
    for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
      const EntVariant* ent = old_ent->ent + i;

      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) > 0 ) {
        // より少ない持駒で詰んでいる. 持ち駒ベクトルが零点に近い
        // -> 既存のは削除してよい
        // skip
      }
      else if ( hsSuperiorBOARD(ent, bo) < 0 && 
		(ent->phi == 0 && ent->delta >= VAL_INF) ) {
        // 既存のも詰み. 新たに追加する必要なし
        to_add = false;
        new_ent->ent[count++] = *ent;
      }
      else {
        // ベクトルが違う. 両方生き
        new_ent->ent[count++] = *ent;
      }
    }
  }
  else if ( phi >= VAL_INF && delta == 0 ) {
    // 負け. 持ち駒がより少ない不詰み情報は捨ててよい
    for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
      const EntVariant* ent = old_ent->ent + i;

      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) < 0 ) {
        // skip
      }
      else if ( hsSuperiorBOARD(ent, bo) > 0 && 
		(ent->phi >= VAL_INF && ent->delta == 0) ) {
        // 既存のも不詰み
        to_add = false;
        new_ent->ent[count++] = *ent;
      }
      else
        new_ent->ent[count++] = *ent;
    }
  }
  else {
    // 探索途中.
    for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
      const EntVariant* ent = old_ent->ent + i;

      if ( ent->phi == 0 && ent->delta >= VAL_INF ) {
        // すでに勝ちが登録されている
        if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) < 0 ) {
          to_add = false;
          new_ent->ent[count++] = *ent;
        }
        else
          new_ent->ent[count++] = *ent;
      }
      else if (ent->phi >= VAL_INF && ent->delta == 0) {
        if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) > 0 ) {
          to_add = false;
          new_ent->ent[count++] = *ent;
        }
        else
          new_ent->ent[count++] = *ent;
      }
      else {
        if ( /*ent->te_count < te_count ||*/ !hsCmpPiece(ent, bo) ) {
          // 深さで判定するのは不味い. 深いところから戻りながら更新していく
          // 既存のは捨てる. skip
        }
        else
          new_ent->ent[count++] = *ent;
      }
    }
  }

  // 追加すべきときだけ実際に書き込む
  if (to_add) {
    new_ent->ent[count++] = make_v(bo, phi, delta, rest_depth, best_move);
    tcmapput( ttable_mate[tag], &bo->key, sizeof(bo->key),
              new_ent, HASH_HEADER_SIZE + HASH_ENT_SIZE * count );

    tcmapmove( ttable_mate[tag], &bo->key, sizeof(bo->key), false );

    if ( tcmaprnum(ttable_mate[tag]) > TTABLE_ENT_COUNT )
      tcmapcutfront(ttable_mate[tag], TTABLE_ENT_COUNT / 3);
  }
  else
    tcmapmove( ttable_mate[tag], &bo->key, sizeof(bo->key), false );

  free(new_ent);
}


#ifdef NDEBUG
  #define assert_post_tt_get(bo, best_move) (static_cast<void>(0))
#else
void assert_post_tt_get(const BOARD* bo, const TE& best_move) 
{
  if ( te_to(best_move) && te_is_put(best_move) ) {
    if (bo->inhand[bo->next][te_pie(best_move) & 0x7] <= 0) {
      printBOARD(bo);
      printf("get best move = %s\n", te_str(best_move, *bo).c_str() );
      si_abort("inhand is %d\n", bo->inhand[bo->next][te_pie(best_move) & 0x7]);
    }
  }
}
#endif // !NDEBUG


/**
 * 局面の得点 (score) を取り出す。
 *
 * @param tag 攻め方とレベル
 * @param bo 対象のBOARD
 * @param phi 攻め方ノードで証明数.
 * @param delta 攻め方ノードで反証数.
 * @param te 最善手. NULLでもよい
 * @param rest_depth 探索深さ。逃れの場合だけ考慮する。
 *
 * @return 盤面が見つかったら 1 、見つからなかったら 0 を返す。
 */
int hsGetBOARD( int tag, const BOARD* bo, 
                int32_t* phi, int32_t* delta, TE* te, int rest_depth )
{
  assert( phi && delta );

  int size = 0;
  const HASH* cntr = hs_find_entry(tag, bo, &size);
  if (!cntr)
    return 0;

  int i;
  for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
    const EntVariant* ent = cntr->ent + i;

    if ( ent->phi == 0 || ent->delta >= VAL_MAY_MATCH ) {
      // 勝ち？

      // 逃れの場合だけ特別扱い
      if (ent->delta < VAL_INF && ent->te_count < rest_depth )
	continue;

      // 手番プレイヤーについて現局面 (bo) のほうが優越してれば勝ち
      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) < 0 ) {
	*phi = ent->phi; *delta = ent->delta;
        if (te) {
          *te = te_unpack(ent->best_move);
	  assert_post_tt_get(bo, *te);
	}
	return 1;
      }
      continue;
    }
    else if ( ent->phi >= VAL_MAY_MATCH || ent->delta == 0 ) {
      if ( ent->phi < VAL_INF && ent->te_count < rest_depth )
	continue;

      // 負け
      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) > 0 ) {
	*phi = ent->phi; *delta = ent->delta;
        if (te) {
          *te = te_unpack(ent->best_move);

	  // 優越関係によっては指せないことがある
	  if ( te_is_put(*te) && bo->inhand[bo->next][te_pie(*te) & 0x7] <= 0)
	    te_clear(te);

	  assert_post_tt_get(bo, *te);
	}
	return 1;
      }
      continue;
    }
    else {
#if 0
      if (ent->te_count < te_count)
        continue;
#endif // 0

      if ( !hsCmpPiece(ent, bo) ) {
	*phi = ent->phi; *delta = ent->delta;
        if (te) {
          *te = te_unpack(ent->best_move);
	  assert_post_tt_get(bo, *te);
	}
	return 1;
      }
    }
  } // of for

  // 見付からなかった
  return 0;
}


/**
 * min/max検索用にBOARDをハッシュテーブルに登録する。
 * phi,delta = (+VAL_INF, -VAL_INF) で勝ち
 *
 * @param bo 対象のBOARD
 * @param phi 盤面の評価点数
 * @param delta 局面の評価点数
 * @param te_count 手数. 深さではない
 * @param best_move 最善手. 投了のときはNULL
 */
void hsPutBOARDMinMax( const BOARD* bo, int32_t phi, int32_t delta,
		       int te_count, 
                       const TE& best_move )
{
  assert_tt_put(bo, best_move);

  int size = 0;
#ifdef HASH_ON_FILE
  HASH* old_ent = static_cast<HASH*>(
		     tchdbget(ttable, &bo->key, sizeof(bo->key), &size) );
#else
  const HASH* old_ent = static_cast<const HASH*>(
                	 tcmapget(ttable, &bo->key, sizeof(bo->key), &size) );
#endif

  if (!old_ent) {
    HASH n;
    n.key = bo->key;
    n.ent[0] = make_v(bo, phi, delta, te_count, best_move);
#ifdef HASH_ON_FILE
    tchdbput( ttable, &bo->key, sizeof(bo->key), &n, sizeof(HASH) );
#else
    tcmapput( ttable, &bo->key, sizeof(bo->key), &n, sizeof(HASH) );
#endif
    return;
  }

  HASH* new_ent = static_cast<HASH*>( malloc(size + HASH_ENT_SIZE) );
  new_ent->key = bo->key;

  int count = 0;
  bool to_add = true;
  int i;

  if ( phi >= +VAL_INF || delta <= -VAL_INF ) {
    // 勝ち
    for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
      const EntVariant* ent = old_ent->ent + i;

      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) > 0 ) {
        // より少ない持ち駒
        // skip
      }
      else if ( hsSuperiorBOARD(ent, bo) < 0 && 
		(ent->phi >= +VAL_INF || ent->delta <= -VAL_INF) ) {
        // 既存のも勝ち
        to_add = false;
        new_ent->ent[count++] = *ent;
      }
      else
        new_ent->ent[count++] = *ent;
    }
  }
  else if ( phi <= -VAL_INF || delta >= +VAL_INF ) {
    // 負け
    for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
      const EntVariant* ent = old_ent->ent + i;

      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) < 0 ) {
        // skip
      }
      else if ( hsSuperiorBOARD(ent, bo) > 0 && 
		(ent->phi <= -VAL_INF || ent->delta >= +VAL_INF) ) {
        to_add = false;
        new_ent->ent[count++] = *ent;
      }
      else
        new_ent->ent[count++] = *ent;
    }
  }
  else {
    // 探索途中
    for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
      const EntVariant* ent = old_ent->ent + i;

      if ( ent->phi >= +VAL_INF || ent->delta <= -VAL_INF ) {
        // 既存のエントリが勝ち
        if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) < 0 ) {
          to_add = false;
          new_ent->ent[count++] = *ent;
        }
        else
          new_ent->ent[count++] = *ent;
      }
      else if ( ent->phi <= -VAL_INF || ent->delta >= +VAL_INF ) {
        if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) > 0 ) {
          to_add = false;
          new_ent->ent[count++] = *ent;
        }
        else
          new_ent->ent[count++] = *ent;
      }
      else {
        if ( ent->te_count < te_count || !hsCmpPiece(ent, bo) ) {
          // skip
        }
        else
          new_ent->ent[count++] = *ent;
      }
    } // of for
  }

  if (to_add) {
    new_ent->ent[count++] = make_v(bo, phi, delta, te_count, best_move);
#ifdef HASH_ON_FILE
    tchdbput( ttable, &bo->key, sizeof(bo->key), new_ent,
              HASH_HEADER_SIZE + HASH_ENT_SIZE * count );
#else
    tcmapput( ttable, &bo->key, sizeof(bo->key), new_ent, 
	      HASH_HEADER_SIZE + HASH_ENT_SIZE * count );

    tcmapmove( ttable, &bo->key, sizeof(bo->key), false );

    if ( tcmaprnum(ttable) > TTABLE_ENT_COUNT )
      tcmapcutfront(ttable, TTABLE_ENT_COUNT / 3);
#endif
  }
  else {
#ifndef HASH_ON_FILE
    // 移動
    tcmapmove( ttable, &bo->key, sizeof(bo->key), false );
#endif // !HASH_ON_FILE
  }

#ifdef HASH_ON_FILE
  free( old_ent );
#endif
  free(new_ent);
}


/**
 * 盤面の得点 (score) を取りだす。min/max検索用。
 * @param bo 対象のBOARD
 * @param score 得点を格納する変数のポインタ
 * @param te 見つかった指し手を格納する先. NULLのときは格納しない
 * @param te_count 手数. 深さではない
 *
 * @return 盤面が見つかったら 1 、見つからなかったら 0 を返す。
 */
int hsGetBOARDMinMax( const BOARD* bo, PhiDelta* score, TE* te, int te_count )
{
  assert( bo != NULL );
  assert( score );

  int size = 0;
#ifdef HASH_ON_FILE
  HASH* cntr = static_cast<HASH*>(
		     tchdbget(ttable, &bo->key, sizeof(bo->key), &size) );
#else
  const HASH* cntr = static_cast<const HASH*>(
   		      tcmapget(ttable, &bo->key, sizeof(bo->key), &size) );
#endif

  if (!cntr)
    return 0;

  int i;
  for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
    const EntVariant* ent = cntr->ent + i;

    if ( ent->phi >= +VAL_INF || ent->delta <= -VAL_INF ) {
      // 勝ち.
      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) < 0 ) {
        score->phi = ent->phi; score->delta = ent->delta;
        if (te) {
          *te = te_unpack(ent->best_move);
	  assert_post_tt_get(bo, *te);
	}
#ifdef HASH_ON_FILE
	free( cntr );
#endif
        return 1;
      }
    }
    else if ( ent->phi <= -VAL_INF || ent->delta >= +VAL_INF ) {
      // 負け
      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) > 0 ) {
        score->phi = ent->phi; score->delta = ent->delta;
        if (te) {
          *te = te_unpack(ent->best_move);

	  // 優越関係によっては指せないことがある
	  if ( te_is_put(*te) && bo->inhand[bo->next][te_pie(*te) & 0x7] <= 0)
	    te_clear(te);

	  assert_post_tt_get(bo, *te);
	}
#ifdef HASH_ON_FILE
	free( cntr );
#endif
        return 1;
      }
    }
    else {
#if 0
      if (ent->te_count < te_count)
        continue;
#endif // !USE_ANY_MOVE

      if ( !hsCmpPiece(ent, bo) ) {
        score->phi = ent->phi; score->delta = ent->delta;
        if (te) {
          *te = te_unpack(ent->best_move);
	  assert_post_tt_get(bo, *te);
	}
#ifdef HASH_ON_FILE
	free( cntr );
#endif
        return 1;
      }
    }
  } // of for

#ifdef HASH_ON_FILE
  free( cntr );
#endif

  // 見付からなかった
  return 0;
}


/**
 * XYの位置のハッシュ値を足す。
 * @param bo 対象のBOARD
 * @param xy 駒の位置
 */
void bo_add_hash_value(BOARD* bo, Xy xy)
{
  PieceKind p;
  
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( 1 <= (xy & 0xF) && (xy & 0xF) <= 9 );

  p = boGetPiece(bo, xy);

  bo->key ^= hashval[0][xy] ^ hashval[ p ][ xy ];
  bo->hash_all ^= hashval[0][xy] ^ hashval[p][xy];
}


/**
 * XYの位置のハッシュ値を外す。
 * @param bo 対象のBOARD
 * @param xy 駒の位置
 */
void bo_sub_hash_value(BOARD* bo, Xy xy)
{
  PieceKind p;
  
#ifdef DEBUG
  assert(bo != NULL);
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( 1 <= (xy & 0xF) && (xy & 0xF) <= 9 );
#endif /* DEBUG */
  
  p = boGetPiece(bo, xy);
  
  bo->key ^= hashval[0][xy] ^ hashval[ p ][ xy ];
  bo->hash_all ^= hashval[0][xy] ^ hashval[p][xy];
}


/** 使用中のエントリの個数を得る */
int hs_entry_count(int tag)
{
  if (tag < 0) {
#ifdef HASH_ON_FILE
    return tchdbrnum( ttable );
#else
    return tcmaprnum(ttable);
#endif
  }
  else if (tag < TTABLE_COUNT)
    return tcmaprnum( ttable_mate[tag] );
  else {
    abort();
  }
}


/** 詰め手順を得る */
void hs_get_best_sequence( int level, const BOARD* bo, MOVEINFO2* seq )
{
  assert( level >= -1 && level <= 2 );

  seq->count = 0;

  int32_t phi, delta;
  TE te_ret;

  int const offense_side = bo->next;
  BOARD* bo_tmp = bo_dup(bo);

  while ( true && seq->count < 100 ) {
    int r;
    te_clear(&te_ret);
    if (level >= 0)
      r = hsGetBOARD(level * 2 + offense_side, bo_tmp,
                     &phi, &delta, &te_ret, 0);
    else {
      PhiDelta dmy;
      r = hsGetBOARDMinMax(bo_tmp, &dmy, &te_ret, 0);
    }

    if (!r)
      break;

    mi2Add( seq, te_ret); // 投了も登録する
    if (!te_to(te_ret) )
      break;

    boMove_mate(bo_tmp, te_ret);
  }
  freeBOARD(bo_tmp);
}


