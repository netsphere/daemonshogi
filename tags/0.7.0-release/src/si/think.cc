/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "si-base.h"

#include <stdio.h>
#include <assert.h>
#include "si.h"
#include "si-think.h"
#include <algorithm>
#include <string.h>
#include <emmintrin.h> // SSE2
using namespace std;


/** 
 * side による target の周りへの利きの合計.
 * bo->next と side は同じとは限らない
 */
int think_count_attack24(const BOARD* bo, int side, Xy target )
{
  assert( target >= 0x11 && target <= 0x99);

  int r = 0;

  for (int i = 0; i < 24; i++) {
    Xy const xy = target + arr_round_to24[i];
    if (xy < 0x11 || xy > 0x99)
      continue;
    if (bo->board[xy] == WALL)
      continue;
    r += bo_attack_count(bo, side, xy);
  }

  return r;
}


/**
 * sideの自玉が行ける範囲を数え上げる.
 * 2手で行ける範囲
 */
int think_count_king_movable2(const BOARD* bo, int side)
{
  Xy const king_xy = bo->king_xy[ side ];
  assert( king_xy != 0 );

  int r[69];
  memset( r, 0, 69 * sizeof(int) );

  Xy xy;
  for (int i = 0; i < 8; i++) {
    for (int ii = 0; ii < bo->long_attack[1 - side][king_xy].count; ii++) {
      // 引くのはダメ
      if (norm_direc(bo->long_attack[1 - side][king_xy].from[ii], king_xy)
	  == arr_round_to[i]) {
	goto next;
      }
    }

    xy = king_xy + arr_round_to[i];
    if (bo->board[xy] == WALL)
      continue;
    if (bo->board[xy] && getTeban(bo->board[xy]) == side )
      continue;
    if (bo_attack_count(bo, 1 - side, xy))
      continue;

    for (int j = 0; j < 8; j++) {
      Xy xy2 = xy + arr_round_to[j];
      if (xy2 < 0x11 || xy > 0x99)
        continue;
      if (bo->board[xy2] == WALL)
        continue;
      if (bo->board[xy2] && getTeban(bo->board[xy2]) == side )
        continue;
      if (bo_attack_count(bo, 1 - side, xy2))
        continue;

      r[xy2 - king_xy + 34] = 1;
    }
  next: ;
  }

  r[34] = 0;
  return count(r, r + 69, 1);
}


#if EVAL_VERSION == 2


/////////////////////////////////////////////////////////////////
// 静的評価関数の作り直し

// ごく単純なパーセプトロン -> 隠れ層がない
// TODO: 隠れ層があるニューラルネットワーク (バックプロパゲーション) はどうすれば
//       うまく学習できるか

// ■盤上の駒
// (1) 盤上の駒の価値 (駒)
//     駒がない、というのも価値があるかもしれない  ... どんな意味？
//     -> (2)

// (2) 盤上の各座標の駒の価値 (駒, xy)  
//     -> すべてのxyでの値が同じ => (1) になる
//     特に玉は意味がありそう

// (3) 盤上の玉からの相対位置の駒の価値 (駒, 玉xy - xy)
//     -> すべての (玉xy - xy) での値が同じ => (1) になる
//        (2) と同じにはならない
//     -> (6)

// (4) 盤上の各座標の駒2ヶの価値 (駒1, 駒1のxy, 駒2, 駒2のxy)
//     駒1 = 歩のとき、駒2 = 歩..龍
//     駒1 = 香のとき、駒2 = 香..龍   ～ 組み合わせの数は掛け算ではない
//     -> (駒2, 駒2のxy) での値が同じ => (2) になる
//     これは意味がなさそう. 玉との関係が重要では？

// (5) (4) を相対位置にすると相互の(利きの)関係になる
//     (駒1, 駒2, xy1 - xy2)
//     意味があるかもしれないし、あまりないかも. どうかな？？？

// (6) 玉を含む3駒の相対位置
//     (駒1, 玉xy - xy1, 駒2, 玉xy - xy2)
//     (3) を含む
//     これは意味がありそう

// ★採用::
//     (2), (6)自玉, 相手玉について
//     どちらも駒の価値を含む. 符号が違うとき offsetが必要

// ■持ち駒
// 歩の枚数が多いときのカーブ（マイナス）を付けるかどうか

// ■玉の自由度
// 2手あるいは3手で行ける範囲

// ■玉周辺の利き
// 自玉、相手玉の近傍24ヶ所
// (7) 制圧度 (自利き - 相手利き)
//     これは意味がありそう

// (8) 相手利きの有無
//     玉が狭くなる。意味がありそうだが、玉の自由度と近そう
//     壁駒が不味い、というのはこれでは把握できない. 
//     単純な玉との相対位置 (6) でも把握できない. -> 玉の自由度のほうが良さそう

// ■脅威
// 自飛車、角の利き先に自駒があると、その先に染み出す. 開き王手とか
// 角を二つ (斜めに) 並べると強い

// ■当たり  ... 駒取りの先行指標, 自駒に紐を付ける
//     玉の近傍は制圧度 (7) があるから、こちらでの区別は不要か？

// ■大駒の可動性
//     当たりと近そう


/**
 * si の座標を csa shogi の座標に変換するテーブル。
 */ 
static const int si2csa_table[] = {
   0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,
   0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 0, 0, 0, 0, 0, 0,
   0, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0, 0, 0, 0, 0, 0,
   0, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 0, 0, 0, 0, 0,
   0, 28, 29, 30, 31, 32, 33, 34, 35, 36, 0, 0, 0, 0, 0, 0,
   0, 37, 38, 39, 40, 41, 42, 43, 44, 45, 0, 0, 0, 0, 0, 0,
   0, 46, 47, 48, 49, 50, 51, 52, 53, 54, 0, 0, 0, 0, 0, 0,
   0, 55, 56, 57, 58, 59, 60, 61, 62, 63, 0, 0, 0, 0, 0, 0,
   0, 64, 65, 66, 67, 68, 69, 70, 71, 72, 0, 0, 0, 0, 0, 0,
   0, 73, 74, 75, 76, 77, 78, 79, 80, 81, 0, 0, 0, 0, 0, 0,
};


/** 駒番号を詰める */
static const int pie2pidx[] = {
  -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, -1, 12, 13,
  -1, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, 26, 27,
};


/** 
 * 盤上の各座標での各駒の数 (駒, xy)
 * 14 x 81
 */
static void xvec_piece_xy(int16_t* xvec, const BOARD* bo)
{
  for (int i = 1; i <= 81; i++) {
    Xy const xy = arr_xy[i];
    PieceKind const pie = bo->board[xy];

    if ( pie ) {
      if ( !getTeban(pie) )
	xvec[ pie2pidx[pie & 0xf] * 81 + (i - 1) ]++; // 先手
      else
	xvec[ pie2pidx[pie & 0xf] * 81 + (81 - i) ]--;
    }
  }
}


/**
 * 持ち駒
 * 7
 */
static void xvec_inhand(int16_t* xvec, const BOARD* bo)
{
  for (int i = 1; i <= 7; i++) {
    xvec[ i - 1 ] = bo->inhand[0][i] - bo->inhand[1][i];
  }
}


/** 
 * 相手玉の自由度 (2手)
 * 1
 */
static void xvec_king_movable(int side, int16_t* xvec, const BOARD* bo)
{
  if (!side) 
    xvec[ 0 ] = think_count_king_movable2(bo, 1);
  else
    xvec[ 0 ] = -think_count_king_movable2(bo, 0);
}


/**
 * 相手玉周辺の制圧度 (周囲3)
 * 7 x 7
 */
static void xvec_kiki_oppo_king(int side, int16_t* xvec, const BOARD* bo)
{
  if (!side) {
    // 後手玉
    int const kx = bo->king_xy[1] & 0xf;
    int const ky = bo->king_xy[1] >> 4;

    for (int i = 1; i <= 81; i++) {
      Xy const xy = arr_xy[i];
      int const x = xy & 0xf;
      int const y = xy >> 4;

      // 後手玉の近傍
      if (x >= kx - 3 && x <= kx + 3 && y >= ky - 3 && y <= ky + 3) 
	xvec[ (kx - x + 3) + (ky - y + 3) * 7 ] +=
	          bo_attack_count(bo, 0, xy) - bo_attack_count(bo, 1, xy);
    }
  }
  else {
    // 先手玉
    int const jx = bo->king_xy[0] & 0xf;
    int const jy = bo->king_xy[0] >> 4;

    for (int i = 1; i <= 81; i++) {
      Xy const xy = arr_xy[i];
      int const x = xy & 0xf;
      int const y = xy >> 4;

      // 先手玉の近傍
      if (x >= jx - 3 && x <= jx + 3 && y >= jy - 3 && y <= jy + 3) 
	xvec[ (x - jx + 3) + (y - jy + 3) * 7 ] += 
          	  bo_attack_count(bo, 0, xy) - bo_attack_count(bo, 1, xy);
    }
  }
}


#if 0
/**
 * 自玉周辺への利き
 * 7 x 7
 */
static void xvec_kiki_my_king(int16_t* xvec, const BOARD* bo)
{
  // 先手玉
  int const jx = bo->king_xy[0] & 0xf;
  int const jy = bo->king_xy[0] >> 4;

  // 後手玉
  int const kx = bo->king_xy[1] & 0xf;
  int const ky = bo->king_xy[1] >> 4;

  for (int i = 1; i <= 81; i++) {
    Xy const xy = arr_xy[i];
    int const x = xy & 0xf;
    int const y = xy >> 4;

    // 先手玉の近傍
    if (x >= jx - 3 && x <= jx + 3 && y >= jy - 3 && y <= jy + 3)
      xvec[ (x - jx + 3) + (y - jy + 3) * 7 ] += bo_attack_count(bo, 0, xy);

    // 後手玉の近傍
    if (x >= kx - 3 && x <= kx + 3 && y >= ky - 3 && y <= ky + 3) 
      xvec[ (kx - x + 3) + (ky - y + 3) * 7 ] -= bo_attack_count(bo, 1, xy);
  }
}
#endif // 0


/** 
 * 相手玉への脅威
 * 7 x 7
 */
static void xvec_threats_around_king(int side, int16_t* xvec, const BOARD* bo)
{
  if (!side) {
    // 後手玉
    int const kx = bo->king_xy[1] & 0xf;
    int const ky = bo->king_xy[1] >> 4;

    for (int i = 1; i <= 81; i++) {
      Xy const xy = arr_xy[i];
      int const x = xy & 0xf;
      int const y = xy >> 4;

      if ( !bo->board[xy] )
	continue;

      // 先手からの利きを止めているか
      int ii;
      for (ii = 0; ii < bo->long_attack[0][xy].count; ii++) {
	XyDiff d = norm_direc(bo->long_attack[0][xy].from[ii], xy);
	Xy vw = xy;
	while (1) {
	  vw += d;
	  if (bo->board[vw] == WALL)
	    break;

	  // 後手玉の近傍
	  if (x >= kx - 3 && x <= kx + 3 && y >= ky - 3 && y <= ky + 3)
	    xvec[ (kx - x + 3) + (ky - y + 3) * 7 ]++;

	  if (bo->board[vw])
	    break;
	}
      }
    }
  }
  else {
    // 先手玉
    int const jx = bo->king_xy[0] & 0xf;
    int const jy = bo->king_xy[0] >> 4;

    for (int i = 1; i <= 81; i++) {
      Xy const xy = arr_xy[i];
      int const x = xy & 0xf;
      int const y = xy >> 4;

      if ( !bo->board[xy] )
	continue;

      int ii;
      for (ii = 0; ii < bo->long_attack[1][xy].count; ii++) {
	XyDiff d = norm_direc(bo->long_attack[1][xy].from[ii], xy);
	Xy vw = xy;
	while (1) {
	  vw += d;
	  if (bo->board[vw] == WALL)
	    break;

	  // 先手玉の近傍
	  if (x >= jx - 3 && x <= jx + 3 && y >= jy - 3 && y <= jy + 3) 
	    xvec[ (x - jx + 3) + (y - jy + 3) * 7 ]--;

	  if (bo->board[vw])
	    break;
	}
      }
    }
  }
}


/**
 * 当たりのある (有無) 駒
 * 28
 */
static void xvec_atari(int16_t* xvec, const BOARD* bo)
{
  for (int i = 1; i <= 81; i++) {
    Xy const xy = arr_xy[i];
    PieceKind const pie = bo->board[xy];

    if (pie) {
      // 先手からの利き
      if ( bo_attack_count(bo, 0, xy) )
	xvec[ pie2pidx[pie] ]++;

      // 後手からの利き
      if ( bo_attack_count(bo, 1, xy) )
	xvec[ pie2pidx[pie ^ 0x10] ]--;
    }
  }
}


/**
 * 駒の可動性 (1手)
 * 14
 */
static void xvec_movable1(int16_t* xvec, const BOARD* bo)
{
  for (int i = 1; i <= 81; i++) {
    Xy const xy = arr_xy[i];
    PieceKind const pie = bo->board[xy];

    if (pie) {
      if ( !getTeban(pie) ) 
	xvec[ pie2pidx[pie] ] += bo->piece_kiki_count[xy];
      else
	xvec[ pie2pidx[pie & 0xf] ] -= bo->piece_kiki_count[xy];
    }
  }
}


struct XVecFn {
  int count;
  void (*fn)(int16_t* xvec, const BOARD* bo);
};

/** 先手、後手の区別があるもの */
struct XVecFn2 {
  int count;
  void (*fn)(int side, int16_t* xvec, const BOARD* bo);
};


void eval_set_xvec(const BOARD* bo, int16_t* xvec)
{
  static const XVecFn fn[] = {
    { 14 * 81, xvec_piece_xy },
    { 7, xvec_inhand },
    { 28, xvec_atari },
    { 14, xvec_movable1 },
    { -1, NULL },
  };

  // 先手、後手の区別があるもの
  static const XVecFn2 fn2[] = {
    { 1, xvec_king_movable },
    { 7 * 7, xvec_kiki_oppo_king },
    // { 7 * 7, xvec_kiki_my_king },
    { 7 * 7, xvec_threats_around_king },
    { -1, NULL },
  };

  memset( xvec, 0, sizeof(int16_t) * XVEC_SIZE );

  int p = 0;
  int i;
  for (i = 0; fn[i].fn; i++) {
    fn[i].fn(xvec + p, bo);
    p += fn[i].count;
  }
  for (int j = 0; j < 2; j++) {
    for (i = 0; fn2[i].fn; i++) {
      fn2[i].fn(j, xvec + p, bo);
      p += fn2[i].count;
    }
  }
  assert(p == XVEC_SIZE);
}


/** 重み. 符号付き16ビットの範囲を超えないように調整すること */
static int16_t weight[HIDDEN_SIZE][XVEC_ROUND] __attribute__((aligned(16)));
static int16_t hidden_weight[HIDDEN_SIZE + 1] __attribute__((aligned(16)));


/** 先手から見た評価値 */
static int32_t eval2(const BOARD* bo)
{
  int16_t xvec[XVEC_ROUND] __attribute__((aligned(16)));
  int16_t hidden[HIDDEN_SIZE + 1] __attribute__((aligned(16)));
  int i, j;

  eval_set_xvec(bo, xvec + 1);
  xvec[0] = 1;
  for (i = 0; i < (8 - ((XVEC_SIZE + 1) % 8)) % 8; i++)
    xvec[XVEC_SIZE + 1 + i] = 0;

  // 入力層 -> 中間層
  for (j = 0; j < HIDDEN_SIZE; j++) {
    __m128i in_ = _mm_set_epi32(0, 0, 0, 0);
    
    for ( i = 0; i < XVEC_SIZE + 1; i += 8 ) {
      __m128i a = _mm_madd_epi16( 
			 _mm_load_si128((__m128i*) (weight[j] + i)),
			 _mm_load_si128((__m128i*) (xvec + i)) );
      in_ = _mm_add_epi32(in_, a); // 効率悪い
    }
    int32_t in[4] __attribute__((aligned(16)));
    _mm_store_si128( (__m128i*) in, in_ );
    hidden[j + 1] = 1000.0 * sigmoid(in[0] + in[1] + in[2] + in[3], 1e-4);
  }
  hidden[0] = 1000;

  // 中間層 -> 出力層
  __m128i out_ = _mm_set_epi32(0, 0, 0, 0);
  for ( i = 0; i < HIDDEN_SIZE + 1; i += 8 ) {
    __m128i a = _mm_madd_epi16(
		       _mm_load_si128((__m128i*) (hidden_weight + i)),
		       _mm_load_si128((__m128i*) (hidden + i)) );
    out_ = _mm_add_epi32(out_, a);
  }
  int32_t out[4] __attribute__((aligned(16)));
  _mm_store_si128( (__m128i*) out, out_ );
/*
  // DEBUG
  int32_t out2 = 0;
  for ( i = 0; i < HIDDEN_SIZE + 1; i++ )
    out2 += hidden_weight[i] * hidden[i];
  assert( out[0] + out[1] + out[2] + out[3] == out2 );
*/
  return sigmoid(out[0] + out[1] + out[2] + out[3], 1e-7) * 100000000.0;
}


/** 
 * bo の手番から見た評価値
 */
int32_t think_eval(const BOARD* bo)
{
  return !bo->next ? eval2(bo) : -eval2(bo);
}


/** 乗じた値が int16_t の範囲に入っていること */
#define WEIGHT_SCALE 10000 

static bool eval_weight_load( const string& filename )
{
  FILE* fp = fopen(filename.c_str(), "r");
  if (!fp)
    return false;

  int i, j;

  for (j = 0; j < HIDDEN_SIZE; j++) {
    for ( i = 0; i < XVEC_SIZE + 1; i++ ) {
      float w;
      fscanf(fp, "%f\n", &w );
      weight[j][i] = w * WEIGHT_SCALE;
    }
  }
  for (i = 0; i < HIDDEN_SIZE + 1; i++) {
    float h;
    fscanf(fp, "%f\n", &h );
    hidden_weight[i] = h * WEIGHT_SCALE;
  }

  fclose(fp);
  return true;
}


bool think_init( const string& filename )
{
  if ( !eval_weight_load(filename) ) {
    si_abort("failed to load weight file.\n");
  }

#if 0
  // DEBUG
  int i, j;
  for (j = 0; j < HIDDEN_SIZE; j++) {
    for (i = 0; i < XVEC_SIZE + 1; i++) 
      weight[j][i] = (rand() % 60000) - 30000;
  }

  for (i = 0; i < HIDDEN_SIZE + 1; i++)
    hidden_weight[i] = (rand() % 60000) - 30000;
#endif

  return true;
}


#if 0
要素数が多すぎる

static const int Cp[] = {
  -1, 
  0, 31, 61, 90, 118, 145, 171, 196, 220, 243, 265, 286, 306, 325, 343, 360,
  376, 391, 405, 418, 430, 441, 451, 460, 468, 475, 481, 486, 490, 493, 495, 
};

static const int Cv[] = {
  0, 17, 33, 
};

/** 
 * 玉と玉以外の駒2ヶの位置関係 (駒1, xy1 - 自玉xy, 駒2, xy2 - 自玉xy)
 * 3駒には相手玉も含めない
 * 32 x (9 x 17) x ?? x (17 x 17)
 */
static void xvec_3pieces_xy(int16_t* xvec, const BOARD* bo)
{
  for (int i = 1; i <= 81; i++) {
    Xy const xy1 = arr_xy[i];
    PieceKind const p1 = bo->board[xy1];

    if ( !p1 || (p1 & 0xf) == OH )
      continue;

    for (int j = i + 1; j <= 81; j++) {
      Xy const xy2 = arr_xy[j];
      PieceKind const p2 = bo->board[xy2];

      if ( !p2 || (p2 & 0xf) == OH )
	continue;

      // 先手玉との関係
      int v1 = (xy1 & 0xf) - (bo->king_xy[0] & 0xf);
      int w1 = (xy1 >> 4) - (bo->king_xy[0] >> 4);
      int v2 = (xy2 & 0xf) - (bo->king_xy[0] & 0xf);
      int w2 = (xy2 >> 4) - (bo->king_xy[0] >> 4);

      if ( p1 <= p2 ) {
	if ( v1 >= 0 ) {
	  xvec[ p1 * (2601 * Cp[p1] * Cv[v1]) + 
		(v1 + (w1 + 8) * 9) * (Cp[p1] * 17 * Cv[v1]) + 
		(p2 - p1) * (17 * Cv[v1]) + 
		(w2 + 8) * Cv[v1] +
		(v2 + 8 - v1) ]++;
	}
	else {
	  // 左右反転
	  xvec[ p1 * (44217 * Cp[p1]) +
		(-v1 + (w1 + 8) * 9) * (Cp[p1] * 289) +
		(p2 - p1) * 289 +
		(-v2 + 8) + (w2 + 8) * 17 ]++;
	}
      }
      else {
	// 駒を入れ替え
	assert(0);
      }
    }
  }
}
#endif // 0


#endif // EVAL_VERSION == 2
