/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include "si.h"
#include "si-think.h"
#include <algorithm>

#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>

using namespace std;


/* -------------------------------------------------------- */
/* BOARDの初期化関係関数 */
/* -------------------------------------------------------- */


/**
 * 新しくメモリを確保してBOARDのポインタを返す。初期化付き。
 * @return BOARDのポインタ
 */
BOARD* newBOARDwithInit()
{
  BOARD* b = new BOARD();
  if (b == NULL) {
    si_abort("No enough memory in board.cc:%s\n", __func__ );
  }

  bo_init(b);
  
  return b;
}


/**
 * bo のメモリを解放する。
 * @param bo 対象のBOARD
 */
void freeBOARD(BOARD* bo) 
{
  if ( bo )
    delete bo;
}


static const int piece_max[] = {
  0, 18, 4, 4, 4, 4, 2, 2, 2,
};


/** 
 * 盤面を初期化する。
 * bo 対象のBOARD
 */
void bo_init(BOARD* bo) 
{
  assert( bo != NULL );

  int i, j;

  // 盤の外に WALL (0x20) で壁を作る  
  for (i = -16; i < 16 * 12; i++)
    bo->board[i] = WALL;

  for (i = 0; i < 2; i++) {
    bo->king_xy[i] = 0;

    for (j = 0; j < 8; j++)
      bo->inhand[i][j] = 0;
  }
  
  for (i = 1; i <= 9; i++) {
    for (j = 1; j <= 9; j++) {
      Xy const sq = (i << 4) + j;
      
      // 中は 0 で初期化
      bo->board[sq] = 0;
      bo->piece_kiki_count[sq] = 0;

      // 利きをクリア
#ifdef USE_PIN
      bo->pinned[sq] = 0;
#endif
      bo->short_attack[0][sq].count = 0;
      bo->short_attack[1][sq].count = 0;
      bo->long_attack[0][sq].count = 0;
      bo->long_attack[1][sq].count = 0;
    }
  }

#if EVAL_VERSION == 1  
  bo->point[0] = 0;
  bo->point[1] = 0;
#endif // EVAL_VERSION == 1

  // 駒はすべて駒箱に
  for (i = 0; i < 9; i++)
    bo->pbox[i] = piece_max[i];
  
  bo->mseq.count = 0;
  for (i = 0; i < MOVEINFO2MAX; i++) {
    bo->tori[ i ] = 0;
    // bo->used_time[i] = 0;
  }
  bo->reappearing_times.clear();

  bo->next = 0;
}


/**
 * 対局用に利き情報や消費時間を初期化する。
 * この関数を呼び出す前に、盤上や持ち駒を設定しておく。
 * @param bo 対象のBOARD
 */
void boPlayInit(BOARD* bo) 
{
  boSetToBOARD(bo);
  
  bo->mseq.count = 0;
  bo->reappearing_times.clear();
}


/* -------------------------------------------------------- */
/* BOARDの初期化関係関数 ここまで*/
/* -------------------------------------------------------- */


/**
 * 駒箱から駒を取り出す。
 * @param pb 対象の駒コード
 * @param p 駒種類
 */
static void pbPop( BOARD* bo, PieceKind p )
{
  assert( p >= 1 && p <= 0x1f );

  /* oh */
  if (p == 0x08 || p == 0x18 ) {
    bo->pbox[ 8 ]--;

    assert( bo->pbox[8] >= 0 );
    return;
  }

  bo->pbox[p & 7]--;
  assert( bo->pbox[p & 7] >= 0 );
}


/**
 * 長い攻撃以外の利きを付ける.
 * @param bo 対象のBOARD
 * @param xy 場所
 * @param piece 駒種. 成り、プレイヤーを含む
 */
static void movetoNormal2(BOARD* bo, Xy const xy, PieceKind piece)
{
  assert( bo->board[xy] );
  assert( bo->board[xy] == piece );

  int side = ((piece & 0x10) >> 4);
  int i;

  for (i = 0; i < normal_to_count[piece]; i++) {
    PieceKind c;
    Xy vw;

    vw = xy + normal_to[piece][i];
    c = bo->board[vw];
    if (c == WALL)
      continue;

    bo->piece_kiki_count[ xy ]++; // 壁以外はすべて効かす
    bo->short_attack[side][vw].from[ bo->short_attack[side][vw].count++ ] = xy;
  }
}


/**
 * 長い攻撃による利きを付ける.
 * @param bo 対象のBOARD
 * @param xy 駒の座標
 */
static void bo_add_long_attack(BOARD* bo, Xy xy)
{
  assert( bo->board[xy] );

  int i;
  int const side = getTeban(bo->board[xy]);
  PieceKind piece;
  
  piece = bo->board[ xy ];

  for (i = 0; i < remote_to_count[piece]; i++) {
    Xy vw = xy;
    while (1) {
      vw += remote_to[piece][i];

      if ( bo->board[ vw ] == WALL )
        break;

      bo->piece_kiki_count[ xy ]++;
      bo->long_attack[side][vw].from[ bo->long_attack[side][vw].count++ ] = xy;

      if ( bo->board[ vw ] ) {
#ifdef USE_PIN
        if ( getTeban(bo->board[vw]) != side ) {
          // 相手の駒 ... pin情報を更新
          Xy vw2 = vw;
          do {
	    vw2 += remote_to[piece][i];
	  } while (!bo->board[vw2]);

          if ( bo->board[vw2] == OH + ((1 - side) << 4) ) 
            bo->pinned[vw] = remote_to[piece][i];
        }
#endif
        break;
      }
    }
  }
}


/**
 * xyにある駒の利き情報 kiki, toBoard3 を更新する.
 * @param bo 対象のBOARD
 * @param xy 駒の場所
 */
static void bo_movetoKiki(BOARD* bo, Xy xy)
{
  int next;

  assert( 1 <= (xy & 0x0f) && (xy & 0x0f) <= 9);
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert(bo->next == 0 || bo->next == 1);
  assert(bo->board[xy] != 0);

  bo->piece_kiki_count[ xy ] = 0;
  
  PieceKind const piece = bo->board[ xy ];
  next = (0x10 & piece) ? GOTE : SENTE;

  movetoNormal2(bo, xy, piece);

  if ( is_long_piece(piece) )
    bo_add_long_attack(bo, xy);
}


/**
 * board, inhand に基づいて盤面すべての利き情報を設定する。
 * (盤面, 持ち駒, 手番) を更新した後で呼び出すこと。
 * 手順 (miメンバ) は初期化しない
 * @param bo BOARD
 */
void boSetToBOARD(BOARD* bo) 
{
  int i;

  assert( bo != NULL );

  // 駒箱を初期化
  for (i = 0; i < 9; i++)
    bo->pbox[i] = piece_max[i];

  bo->king_xy[0] = 0;
  bo->king_xy[1] = 0;

  for (i = 0; i <= 9; i++) {
    bo->fu_exists[0][i] = false;
    bo->fu_exists[1][i] = false;
  }

  // 利きを初期化
  for ( i = 1; i <= 81; i++ ) {
    Xy const xy = arr_xy[i];

    bo->short_attack[ 0 ][ xy ].count = 0;
    bo->short_attack[ 1 ][ xy ].count = 0;
    bo->long_attack[ 0 ][ xy ].count = 0;
    bo->long_attack[ 1 ][ xy ].count = 0;

    bo->piece_kiki_count[ xy ] = 0;
#ifdef USE_PIN
    bo->pinned[ arr_xy[i] ] = 0;
#endif
  }

  // 利きなどを更新
  for ( i = 1; i <= 81; i++ ) {
    Xy const fm = arr_xy[i];
    PieceKind const pie = boGetPiece(bo, fm);
    if ( !pie )
      continue;

    pbPop( bo, pie );

    if (pie == OH)
      bo->king_xy[0] = fm;
    else if (pie == OH + 0x10)
      bo->king_xy[1] = fm;
    else if (pie == FU)
      bo->fu_exists[0][fm & 0xf] = true;
    else if (pie == FU + 0x10)
      bo->fu_exists[1][fm & 0xf] = true;

    bo_movetoKiki(bo, fm); // 利きを更新
  }

  bo->key = hsGetHashKey(bo);

  int j, k;
  uint64_t inhand_hash = 0;
  for (i = 0; i < 2; i++) {
    for (j = 1; j <= 7; j++) {
      // 個数分だけ編み込む. 0のときは bo->key = bo->hash_all
      for (k = 0; k < bo->inhand[i][j]; k++) {
	pbPop( bo, j );
        inhand_hash ^= hashseed_inhand[i][j][k + 1];
      }
    }
  }

  //  if (bo->next) // 手番 = 後手
  //    bo->hash_all = bo->key ^ ~ inhand_hash;
  //  else
    bo->hash_all = bo->key ^ inhand_hash;

#if EVAL_VERSION == 1
  // 得点を設定
  if ( bo->king_xy[0] && bo->king_xy[1] ) {
    bo->point[0] = grade(bo, 0);
    bo->point[1] = grade(bo, 1);
  }
#endif // EVAL_VERSION == 1
}


/**
 * BOARDのコピーを作成する。
 */
BOARD* bo_dup(const BOARD* src) 
{
  BOARD* dest = new BOARD(*src);
  return dest;
}


/**
 * 持ち駒を増やす。
 * @param bo BOARD
 * @param p 持駒にする駒種類. 成り/不成り、プレイヤー付き
 */
static void inc_inhand(BOARD* bo, PieceKind p )
{
  assert( bo != NULL );
  assert( 1<=p && p<=0x1F );
  assert( p != 8 && p != 0x18 );

  int count = ++bo->inhand[bo->next][p & 0x07];

#ifdef USE_HASH  
  //  if (bo->next) 
  //    bo->hash_all ^= ~ hashseed_inhand[ bo->next][p & 7][ count ];
  //  else
    bo->hash_all ^= hashseed_inhand[ bo->next][p & 7][ count ];    
#endif
}


/**
 * 持ち駒を減らす.
 */
static void dec_inhand(BOARD* bo, PieceKind p)
{
#ifdef USE_HASH
  //  if (bo->next)
  //    bo->hash_all ^= ~ hashseed_inhand[ bo->next][p & 7][ bo->inhand[bo->next][p & 7]];
  //  else
    bo->hash_all ^= hashseed_inhand[ bo->next][p & 7][ bo->inhand[bo->next][p & 7]];
#endif

  bo->inhand[bo->next][p & 7]--;
}


/** 駒を置き、周りからの利きを更新しない */
static void put_piece(BOARD* bo, Xy xy, PieceKind pie )
{
  assert(xy >= 0x11 && xy <= 0x99);
  assert( pie >= 1 && pie <= 0x1f );

  bo->board[xy] = pie;

  switch ( pie ) {
  case FU:
    bo->fu_exists[0][xy & 0xf] = true;
    break;
  case FU + 0x10:
    bo->fu_exists[1][xy & 0xf] = true;
    break;
  case OH:
    bo->king_xy[0] = xy;
    break;
  case OH + 0x10:
    bo->king_xy[1] = xy;
    break;
  default:
    break;
  }

  bo_movetoKiki(bo, xy); // xyからの利きを付ける

  /* ハッシュ値に te->to の位置の駒の値を足す */
  bo_add_hash_value(bo, xy);
}


/** 
 * 配列から一致するものを削除する.
 * 末尾の要素で詰める
 */
static void ary_erase( Xy* begin, Xy* end, Xy value)
{
  Xy* p;
  for (p = begin; p != end; p++) {
    if (*p == value) {
      *p = *(end - 1);
      *(end - 1) = 0;
      return;
    }
  }
}


/** 駒を置き、利きを更新する */
static void put_piece_with_kiki( BOARD* bo, Xy xy, PieceKind pie )
{
  assert(xy >= 0x11 && xy <= 0x99);

  int i;
  int side;

  put_piece( bo, xy, pie );

  for (side = 0; side < 2; side++) {
    for (i = 0; i < bo->long_attack[side][xy].count; i++) {
      Xy vw = xy;
      Xy const attack_from = bo->long_attack[side][xy].from[i];
      XyDiff const direc = norm_direc(attack_from, xy);
      do {
        vw += direc;
        if (bo->board[vw] == WALL)
          break;

        ary_erase( bo->long_attack[side][vw].from,
               bo->long_attack[side][vw].from + bo->long_attack[side][vw].count,
               attack_from );
        bo->long_attack[side][vw].count--;
        bo->piece_kiki_count[attack_from]--;

        if (bo->board[vw]) {
#ifdef USE_PIN
          ... ;
#endif
          break;
        }
      } while (true);
    }
  }
}


/** 短い利きを取り除く*/
static void bo_remove_short_attack( BOARD* bo, Xy const xy )
{
  assert( bo->board[xy] );

  PieceKind piece = bo->board[xy];
  int side = getTeban(bo->board[xy]);
  int i;

  for (i = 0; i < normal_to_count[piece]; i++) {
    Xy vw;

    vw = xy + normal_to[piece][i];
    if ( bo->board[vw] == WALL )
      continue;

    ary_erase( bo->short_attack[side][vw].from,
           bo->short_attack[side][vw].from + bo->short_attack[side][vw].count,
           xy );
    bo->short_attack[side][vw].count--;

    bo->piece_kiki_count[ xy ]--;
  }
}


/** 長い利きを取り除く */
static void bo_remove_long_attack(BOARD* bo, Xy xy, PieceKind piece)
{
  assert( bo->board[xy] );

  int const side = getTeban(bo->board[xy]);
  int i;

  for (i = 0; i < remote_to_count[piece]; i++) {
    Xy vw = xy;
    while (1) {
      vw += remote_to[piece][i];

      if ( bo->board[ vw ] == WALL )
        break;

      ary_erase( bo->long_attack[side][vw].from,
             bo->long_attack[side][vw].from + bo->long_attack[side][vw].count,
             xy );
      bo->long_attack[side][vw].count--;
      bo->piece_kiki_count[ xy ]--;

      if (bo->board[vw]) {
#ifdef USE_PIN
        if ( bo->pinned[vw] == remote_to[piece][i] )
          bo->pinned[vw] = 0;
#endif
        break;
      }
    }
  }
}


/**
 * xy にある駒の利き (短いのも長いのも) を取り除く。
 * @param bo BOARD
 * @param xy 座標
 */
static void boPopToBoard(BOARD* bo, Xy xy) 
{
  int next;
  
  assert( 1 <= (xy&0x0f) && (xy&0x0f) <= 9 );
  assert( 1 <= (xy>>4) && (xy>>4) <= 9 );
  assert( bo->board[ xy ] != 0 );
  
  next = getTeban(bo->board[ xy ]);

  bo_remove_short_attack( bo, xy );

  if ( is_long_piece(bo->board[xy]) )
    bo_remove_long_attack( bo, xy, bo->board[xy] );

  assert( bo->piece_kiki_count[xy] == 0 );
}


/** 
 * 駒をbo->board[] から取り除き、周りからの利きを更新しない.
 */
static void remove_piece(BOARD* bo, Xy xy)
{
  /* ハッシュ値に te->fm の位置の駒の値を引いておく */
  bo_sub_hash_value(bo, xy);

  boPopToBoard(bo, xy); // xyからの利きを除く

  switch ( bo->board[xy] ) {
  case FU:
    bo->fu_exists[0][xy & 0xf] = false;
    break;
  case FU + 0x10:
    bo->fu_exists[1][xy & 0xf] = false;
    break;
  default:
    break;
  }

  bo->board[xy] = 0;
}


/** 駒を取り除き、利きを更新する */
static void remove_piece_with_kiki(BOARD* bo, Xy xy)
{
  assert(bo->board[xy]);
  assert(xy >= 0x11 && xy <= 0x99);

  int i;
  int side;

  remove_piece( bo, xy );

  for (side = 0; side < 2; side++) {
    for (i = 0; i < bo->long_attack[side][xy].count; i++) {
      Xy vw = xy;
      Xy const attack_from = bo->long_attack[side][xy].from[i];
      XyDiff const direc = norm_direc(attack_from, xy);
      do {
        vw += direc;
        if (bo->board[vw] == WALL)
          break;

        bo->long_attack[side][vw].from[ bo->long_attack[side][vw].count++ ] = attack_from;
        bo->piece_kiki_count[attack_from]++;

        if (bo->board[vw]) {
#ifdef USE_PIN
          ... ;
#endif
          break;
        }
      } while (true);
    }
  }
}


#ifdef NDEBUG
  #define assert_move(bo, te)  (static_cast<void>(0))
#else
static void assert_move(BOARD* bo, const TE& te)
{
#if 0
  // とても重い
  uint64_t orig_key = bo->key;
  uint64_t orig_hash_all = bo->hash_all;
  boSetToBOARD(bo);
  assert(orig_key == bo->key);
  assert(orig_hash_all == bo->hash_all);
#endif // 0

  // 指し手の妥当性
  assert( 1 <= (te_to(te) & 0x0f) && (te_to(te) & 0x0f) <=9 );
  assert( 1 <= (te_to(te) >> 4) && (te_to(te) >> 4) <=9 );
  if ( !te_is_put(te) ) {
    assert( 1 <= (te_from(te) & 0x0f) && (te_from(te) & 0x0f) <= 9 );
    assert( 1 <= (te_from(te) >> 4) && (te_from(te) >> 4) <=9 );

    if ( bo->board[te_from(te)] == 0 ) {
      printBOARD( bo );
      printf("bad move = %s\n", te_str(te, *bo).c_str() );
      si_abort("missing from's piece" );
    }

    // 移動先に自分の駒があってはいけない
    if ( bo->board[te_to(te)] ) {
      if ( getTeban(bo->board[te_from(te)]) == GOTE )
	assert( getTeban(bo->board[te_to(te)]) == SENTE );
      else
	assert( getTeban(bo->board[te_to(te)]) == GOTE );
    }

    /* 成れないのに成りにしていないか調べる */
    if ( te_promote(te) ) {
      assert( (bo->board[te_from(te)] & 0x0F) <= 7 );
    }
  }
  else {
    assert( te_pie(te) >= 1 && te_pie(te) <= 7 );
    if ( bo->inhand[ bo->next ][te_pie(te)] <= 0 ) {
      printBOARD(bo);
      printf("bad move = %s\n", te_str(te, *bo).c_str());
      si_abort("cannot put as inhand <= 0");
    }

    assert( te_from(te) == 0);
    assert( bo->board[te_to(te)] == 0 );

    assert( te_promote(te) == 0);
  }
}
#endif // !NDEBUG


/** 
 * 手番を入れ替える. 手数などは変更しない。
 * パスしたいときはこの関数ではなく bo_pass() を呼び出すこと
 * @param bo 対象のBOARD
 */
static void boToggleNext(BOARD* bo)
{
  assert( bo != NULL );
  
  bo->next = (bo->next == 0);
  bo->key = ~ bo->key;
  bo->hash_all = ~ bo->hash_all;
}


#ifdef USE_PIN
/**
 * 玉が動いたときに、相手方からのpinを更新する
 * @param bo 局面
 * @param side pin攻撃を更新する側
 */
static void bo_update_pin(BOARD* bo, int king_side, Xy from, Xy to)
{
  int i;
  
  // pinを外す
  for (i = 0; i < 8; i++) {
    Xy xy = from;
    do {
      xy += arr_round_to[i];
      if (bo->board[xy] == WALL)
        break;
      if (bo->board[xy]) {
        if ( getTeban(bo->board[xy]) == king_side )
          bo->pinned[xy] = 0;
        break;
      }
    } while (true);
  }

  // 新しくpinを付ける
  for (i = 0; i < 8; i++) {
    Xy xy = to;
    do {
      xy += arr_round_to[i];
      if (bo->board[xy] == WALL)
        break;
      if (bo->board[xy]) {
        if ( getTeban(bo->board[xy]) == king_side ) {
          // 先まで延ばす
          for (int j = 0; j < bo->long_attack[1 - king_side][xy].count; j++) {
            Xy attack_from = bo->long_attack[1 - king_side][xy].from[j];
            if ( norm_direc(attack_from, xy) == -arr_round_to[i] )
              bo->pinned[xy] = -arr_round_to[i];
          }
        }
        break;
      }
    } while (true);
  }
}
#endif // USE_PIN


#ifdef NDEBUG
  #define assert_post_move(bo, te) (static_cast<void>(0))
#else
static void assert_post_move(BOARD* bo, const TE& te)
{
#if 0
  // とても重い
  uint64_t orig_key = bo->key;
  uint64_t orig_hash_all = bo->hash_all;
  boSetToBOARD(bo);
  assert(orig_key == bo->key);
  assert(orig_hash_all == bo->hash_all);
#endif // 0

  if ( bo_is_checked(bo, 1 - bo->next) ) {
    printf("internal error: still checked.\n");

    boBack_mate(bo); // 不正な手は戻す
    printBOARD(bo);
    printf("hash = %" PRIx64 ", %" PRIx64 "\n", bo->key, bo->hash_all);
    printf("illegal move = %s\n", te_str(te, *bo).c_str() );

    // 初手からの指し手を表示
    BOARD* bo_tmp = bo_dup(bo);
    while (bo_tmp->mseq.count > 0)
      boBack_mate(bo_tmp);

    printBOARD(bo_tmp);
    printf("seq = ");
    mi2_print_sequence( &bo->mseq, *bo_tmp );
    freeBOARD(bo_tmp);

    abort();
  }
}
#endif // !NDEBUG


/*
 * BOARD上の駒を動かす。
 * @param bo BOARD
 * @param te 移動情報
 */
int boMove( BOARD* bo, const TE& te )
{
  PieceKind c;
  PieceKind p = 0;
  int flg_tori;

  // printf("\n%s: in: point = (%d, %d)\n", __func__, bo->point[0], bo->point[1]);

  assert_move(bo, te);
  
  flg_tori = 0;
  int const next = bo->next;

  // 手順を記録
  mi2Add( &bo->mseq, te ); 

  // 王の安全度の評価を盤面評価に足す (戻す)
  // bo->point[SENTE] += add_oh_safe_grade(bo, SENTE);
  // bo->point[GOTE] += add_oh_safe_grade(bo, GOTE);
  
  if ( !te_is_put(te) ) {
    /* 駒を移動する */

    p = bo->board[ te_from(te) ];
    assert( p && getTeban(p) == bo->next );

    // 移動元.
    // printf("from:%d ", -add_grade(bo, te_from(te), next));
#if EVAL_VERSION == 1
    bo->point[next] -= add_grade(bo, te_from(te), next );
#endif // EVAL_VERSION == 1
    remove_piece_with_kiki(bo, te_from(te));

    c = bo->board[ te_to(te) ];
    if ( c != 0 ) {
      /* 移動先に駒があった */
      
      // 持ち駒に加える
      inc_inhand(bo, c );
      // printf("inhand:%d ", +score_inhand(bo, c & 7, next));
#if EVAL_VERSION == 1
      bo->point[next] += score_inhand( bo, c & 7, next );
#endif // EVAL_VERSION == 1

      // 盤上から駒を取り除く
      // printf("remove_to(op):%d ", -add_grade(bo, te_to(te), (next == 0)));
#if EVAL_VERSION == 1
      bo->point[1 - next] -= add_grade(bo, te_to(te), (next == 0));
#endif // EVAL_VERSION == 1
      remove_piece(bo, te_to(te));

      /* 取られる駒を覚えておく */
      bo->tori[ bo->mseq.count - 1 ] = c;
      flg_tori = 1;
    } 
    else {
      bo->tori[ bo->mseq.count - 1 ] = 0;
    }

    // 移動先
    if ( te_promote(te) )
      p += 8;

    if (flg_tori)
      put_piece(bo, te_to(te), p );
    else
      put_piece_with_kiki(bo, te_to(te), p );
    // printf("to:%d ", add_grade(bo, te_to(te), next));
#if EVAL_VERSION == 1
    bo->point[next] += add_grade(bo, te_to(te), next );
#endif // EVAL_VERSION == 1
  } 
  else {
    // 駒打ちの場合
    
    // 持ち駒を減らす
    // printf("dec_inhand:%d ", -score_inhand(bo, te_pie(te), next));
#if EVAL_VERSION == 1
    bo->point[next] -= score_inhand( bo, te_pie(te), next );
#endif // EVAL_VERSION == 1
    dec_inhand(bo, te_pie(te));

    // 駒を置く
    put_piece_with_kiki(bo, te_to(te), te_pie(te) + (next << 4) );
    // printf("put:%d ", add_grade(bo, te_to(te), next));
#if EVAL_VERSION == 1
    bo->point[next] += add_grade(bo, te_to(te), next);
#endif // EVAL_VERSION == 1
    
    bo->tori[ bo->mseq.count - 1 ] = 0;
  }

  boToggleNext(bo);

#if EVAL_VERSION == 1
  if ((p & 0xf) == OH) {
    bo->point[0] = grade(bo, SENTE);
    bo->point[1] = grade(bo, GOTE);
#ifdef USE_PIN
    bo_update_pin(bo, 1 - bo->next, te_from(te), te_to(te) );
#endif
  }
#endif // EVAL_VERSION == 1

  if ( bo_is_checked(bo, bo->next) )
    te_add_hint( &bo->mseq.te[bo->mseq.count - 1], TE_CHECK );

  assert_post_move(bo, te);

  // printf("\n%s: out: point = (%d, %d)\n", __func__, bo->point[0], bo->point[1]);

  return flg_tori;
}


#ifdef NDEBUG
  #define assert_back(expr) (static_cast<void>(0))
#else
static void assert_back(const BOARD* bo)
{
  assert( bo != NULL );
  assert( bo->mseq.count > 0 );
}
#endif // !NDEBUG


/**
 * 一手戻す。
 * @param bo 対象のBOARD
 */
void boBack( BOARD* bo ) 
{
  assert_back(bo);

  PieceKind p = 0;
  PieceKind cap;

  boToggleNext(bo);
#if EVAL_VERSION == 1
  int next = bo->next;
#endif // EVAL_VERSION == 1

  const TE& te = bo->mseq.te[ --bo->mseq.count ];
  cap = bo->tori[ bo->mseq.count ];

  if (!te_to(te)) // pass手
    return;

  if ( !te_is_put(te) ) {
    p = bo->board[ te_to(te) ];

    /* 盤面評価点数から te_to(te) の位置の駒の点数を引く */
    // printf("to:%d ", -add_grade(bo, te_to(te), next));
#if EVAL_VERSION == 1
    bo->point[next] -= add_grade(bo, te_to(te), next );
#endif // EVAL_VERSION == 1

    if (cap) {
      // 駒を取っていた場合
      remove_piece(bo, te_to(te));

      // 持ち駒から減らす
      // printf("inhand:%d ", -score_inhand(bo, cap & 7, next));
#if EVAL_VERSION == 1
      bo->point[next] -= score_inhand(bo, cap & 7, next);
#endif // EVAL_VERSION == 1

      dec_inhand(bo, cap);

      // 駒を戻す
      put_piece(bo, te_to(te), cap);
      // printf("restore(op):%d ", add_grade(bo, te_to(te), (next == 0)));
#if EVAL_VERSION == 1
      bo->point[1 - next] += add_grade(bo, te_to(te), (next == 0));
#endif // EVAL_VERSION == 1
    }
    else
      remove_piece_with_kiki(bo, te_to(te));

    // 移動元
    if ( te_promote(te) )
      p &= 0x17;
    put_piece_with_kiki(bo, te_from(te), p );
    // printf("from:%d ", add_grade(bo, te_from(te), next));
#if EVAL_VERSION == 1
    bo->point[next] += add_grade(bo, te_from(te), next);
#endif // EVAL_VERSION == 1
  } 
  else {
    /* 駒打ちだった場合 */

    // 取り除く
    // printf("remove:%d ", -add_grade(bo, te_to(te), next));
#if EVAL_VERSION == 1
    bo->point[next] -= add_grade(bo, te_to(te), next);
#endif // EVAL_VERSION == 1
    remove_piece_with_kiki(bo, te_to(te));

    // 持ち駒に戻す
    inc_inhand(bo, te_pie(te) );
    // printf("to_inhand:%d ", +score_inhand(bo, te_pie(te), next));
#if EVAL_VERSION == 1
    bo->point[next] += score_inhand(bo, te_pie(te), next);
#endif // EVAL_VERSION == 1
  }

#if EVAL_VERSION == 1  
  if ( (p & 0xf) == OH ) {
    bo->point[0] = grade(bo, SENTE);
    bo->point[1] = grade(bo, GOTE);
#ifdef USE_PIN
    bo_update_pin(bo, bo->next, te_to(te), te_from(te) );
#endif
  }
#endif // EVAL_VERSION == 1

  // printf("\n%s: out: point = (%d, %d)\n", __func__, bo->point[0], bo->point[1]);
}


/*
 * BOARD上の駒を動かす。
 * @param bo BOARD
 * @param te 移動情報
 * @param next 手番
 */
void boMove_mate(BOARD* bo, const TE& te)
{
  int c;
  PieceKind p = 0;
  int flg_tori;

  assert_move(bo, te);
  
  flg_tori = 0;

  mi2Add( &bo->mseq, te );
  
  if ( !te_is_put(te) ) {
    /* 駒を移動する */

    p = bo->board[ te_from(te) ];

    // 移動元
    remove_piece_with_kiki(bo, te_from(te));

    c = bo->board[ te_to(te) ];
    if ( c != 0 ) {
      /* 移動先に駒があった */
      
      // 持ち駒に加える
      inc_inhand(bo, c );

      remove_piece(bo, te_to(te));

      /* 取られる駒を覚えておく */
      bo->tori[ bo->mseq.count - 1 ] = c;
      flg_tori = 1;
    } 
    else  {
      bo->tori[ bo->mseq.count - 1 ] = 0;
      flg_tori = 0;
    }

    // 移動先    
    if (te_promote(te))
      p += 8;

    if (flg_tori)
      put_piece(bo, te_to(te), p );
    else
      put_piece_with_kiki(bo, te_to(te), p );
  } 
  else {
    /* 駒打ちの場合 */
    dec_inhand(bo, te_pie(te));
    put_piece_with_kiki(bo, te_to(te), te_pie(te) + (bo->next << 4) );

    bo->tori[ bo->mseq.count - 1 ] = 0;
  }

  boToggleNext(bo);

#ifdef USE_PIN
  if ( (p & 0xf) == OH )
    bo_update_pin(bo, 1 - bo->next, te_from(te), te_to(te) );
#endif
  
  if ( bo_is_checked(bo, bo->next) )
    te_add_hint( &bo->mseq.te[bo->mseq.count - 1], TE_CHECK );

#ifdef USE_PIN
#if 1
  // 打ち歩詰めは実際に動かして確認する
  // TODO: 動作確認できたら削除すること
  assert_post_move(bo, te);
#endif
#endif // USE_PIN
}


/**
 * 一手戻す.
 * @param bo 対象のBOARD
 */
void boBack_mate( BOARD* bo )
{
  assert_back(bo);

  const TE& te = bo->mseq.te[ --bo->mseq.count ];
  PieceKind cap = bo->tori[ bo->mseq.count ];
  PieceKind p = 0;

  boToggleNext(bo);

  if (!te_to(te)) // pass手
    return;

  if ( !te_is_put(te) ) {
    p = bo->board[ te_to(te) ];

    if ( cap ) {
      /* 駒を取っていた場合 */

      remove_piece(bo, te_to(te));

      dec_inhand(bo, cap);
      put_piece(bo, te_to(te), cap );
    }
    else
      remove_piece_with_kiki(bo, te_to(te));

    // 移動元
    if ( te_promote(te) ) 
      p &= 0x17;
    put_piece_with_kiki(bo, te_from(te), p );
  } 
  else {
    /* 駒打ちだった場合 */
#ifndef NDEBUG
    assert( 1 <= te_pie(te) && te_pie(te) <= 7 );
    assert( bo->board[ te_to(te) ] != 0 );
    assert( bo->board[ te_to(te) ] != WALL );
#endif /* DEBUG */

    remove_piece_with_kiki(bo, te_to(te));
    inc_inhand(bo, te_pie(te) );
  }

#ifdef USE_PIN
  if ( (p & 0xf) == OH )
    bo_update_pin(bo, bo->next, te_to(te), te_from(te) );
#endif
}


/**
 * bo1 と bo2 を比較する。
 *
 * @param bo1 対象のBOARD
 * @param bo2 対象のBOARD
 * @return 等しければ0を、違いがあれば0以外を返す
 */
int boCmp(const BOARD* bo1, const BOARD* bo2) 
{
#ifdef USE_HASH
  if ( bo1->hash_all != bo2->hash_all )
    return 1;
#endif

  if ( memcmp( bo1->board, bo2->board, sizeof( bo1->board ) ) )
    return 1;

  if ( memcmp( bo1->inhand[0], bo2->inhand[0], sizeof( bo1->inhand[0] ) ) )
    return 1;

  if ( memcmp( bo1->inhand[1], bo2->inhand[1], sizeof( bo1->inhand[1] ) ) )
    return 1;

  return 0;
}


/**
 * 使っていない駒を詰将棋用に受け側の持駒としてセットする。
 * @param bo 対象のBOARD
 */
void boSetTsumeShogiPiece(BOARD* bo)
{
  int i;

  boSetToBOARD(bo);

  for ( i = 1; i <= 7; i++ ) {
    bo->inhand[ (bo->next == 0) ][ i ] += bo->pbox[i];
    bo->pbox[i] = 0;
    // piece_max[ i ] - bo->pb.count[ i ] - bo->inhand[ bo->next ][ i ];
  }

  boSetToBOARD(bo);
}


/** パスする. 必ず boBack_mate() で戻すこと */
void bo_pass( BOARD* bo )
{
  TE te;
  te_clear(&te);

  mi2Add( &bo->mseq, te );
  bo->tori[bo->mseq.count - 1] = 0;

  boToggleNext(bo);
}


static int bo_is_correct_mate(const BOARD* bo, Xy* oh_xy)
{
  int piece[9]; // 駒種ごとの駒の数
  Xy xy;
  PieceKind p;
  int i;
  int x, y, flg;

  assert(bo != NULL);

  oh_xy[0] = 0;
  oh_xy[1] = 0;
  for (i = 0; i < 9; i++) {
    piece[i] = 0;
  }

  for (y = 1; y <= 9; y++) {
    for (x = 1; x <= 9; x++) {
      xy = (y << 4) + x;

      p = boGetPiece(bo, xy);
      if (p == 0)
	continue;

      // 動かせない駒はないか
      if ( (p == FU && y <= 1) ||
	   (p == KYO && y <= 1) ||
	   (p == KEI && y <= 2) ) {
	printf("cannot move\n");
	return 0;
      }
      if ( (p == FU + 0x10 && y >= 9) ||
	   (p == KYO + 0x10 && y >= 9) ||
	   (p == KEI + 0x10 && y >= 8) ) {
	printf("cannot move\n");
	return 0;
      }

      // 玉は高々1枚ずつ
      if (p == OH) {
	if (oh_xy[0]) {
	  printf("king only 1\n");
	  return 0;
	}
	oh_xy[0] = xy;
	piece[8]++;
      }
      else if (p == OH + 0x10) {
	if (oh_xy[1]) {
	  printf("king only 1\n");
	  return 0;
	}
	oh_xy[1] = xy;
	piece[8]++;
      }
      else
	piece[p & 7]++;
    }
  }

  // 二歩
  for (x=1; x <= 9; x++) {
    flg = 0;
    for (y=1; y <= 9; y++) {
      p = boGetPiece(bo, (y << 4) + x);
      if (p == FU) {
	if (flg == 1) {
	  printf("nifu\n");
	  return 0;
	}
	flg = 1;
      }
    }
  }

  for (x=1; x <= 9; x++) {
    flg = 0;
    for (y=1; y <= 9; y++) {
      p = boGetPiece(bo, (y << 4) + x);
      if (p == FU + 0x10) {
	if (flg == 1) {
	  printf("nifu\n");
	  return 0;
	}
	flg = 1;
      }
    }
  }

  /* 持ち駒の数を調べる */
  for (i=1; i<=7; i++) {
    piece[i] += bo->inhand[0][i];
    piece[i] += bo->inhand[1][i];
  }

  /* 駒全部の数を数える */
  for (i = 1; i <= 8; i++) {
    if (piece_max[i] < piece[i]) {
      printf("pieces too many\n");
      return 0;
    }
  }
/*
  // 受け玉
  if (bo->next == SENTE) {
    if (oh_xy[1] == 0) {
      printf("uke-king nothing\n");
      return 0;
    }
  } 
  else {
    if (oh_xy[0] == 0) {
      printf("uke-king nothing\n");
      return 0;
    }
  }
*/  
  if (bo->next == SENTE) {
    // 後手に王手が掛かっていてはいけない
    if ( bo_is_checked(bo, GOTE) ) {
      printf("king can capture\n");
      return 0;
    }
  }
  else {
    if ( bo_is_checked(bo, SENTE) ) {
      printf("king can capture\n");
      return 0;
    }
  }

  return 1;
}


/**
 * 対局用に正当な盤面か調べる。
 * @param bo 対象のBOARD
 * @return 1 ならば正当。0 ならば不正。
 */
int boCheckJustGame(const BOARD* bo)
{
  Xy oh_xy[2];
  if ( !bo_is_correct_mate(bo, oh_xy) )
    return 0;

  if (oh_xy[0] == 0)
    return 0;

  if (oh_xy[1] == 0)
    return 0;

  return 1;
}


/**
 * 詰将棋用に正当な盤面か調べる。
 * @param bo 対象のBOARD
 * @return 1 ならば正当。0 ならば不正。
 */
int boCheckJustMate(const BOARD* bo)
{
  Xy oh_xy[2];
  return bo_is_correct_mate(bo, oh_xy);
}


/** 
 * 盤面をコンソールに出力.
 * \todo front = 1 のときの表示
 */
void printBOARD(const BOARD* brd, int front)
{
  assert( brd != NULL );

  int x, y;

  for (y = 1; y <= 9; y++) {
    printf("P%d", y);
    for (x = 9; x >= 1; x--) {
      PieceKind p = boGetPiece(brd, (y << 4) + x);
      if (!p)
        printf(" * ");
      else
        printf("%c%s", (getTeban(p) == 0 ? '+' : '-'), CSA_PIECE_NAME[p & 0xf]);
    }
    printf("\n");
  }

  // 持ち駒
  int i, side;
  for (side = 0; side < 2; side++) {
    if ( *max_element(brd->inhand[side] + 1, brd->inhand[side] + 8) > 0) {
      // 読みやすいように
      printf("'%c ", side == 0 ? '+' : '-');
      for (x = 1; x <= 7; x++) {
        if ( brd->inhand[side][x] )
          printf("%s%d ", CSA_PIECE_NAME[x], brd->inhand[side][x]);
      }
      printf("\n");

      // CSA形式
      printf("P%c", side == 0 ? '+' : '-');
      for (x = 1; x <= 7; x++) {
        for (i = 0; i < brd->inhand[ side ][x]; i++)
          printf("00%s", CSA_PIECE_NAME[x]);
      }
      printf("\n");
    }
  }
  printf( "%c\n", brd->next == 0 ? '+' : '-' );
#ifndef NDEBUG
  printf( "'key = %" PRIx64 ", all = %" PRIx64 "\n", brd->key, brd->hash_all );
#endif // !NDEBUG
}


static const char* fix_map = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

/** 盤面を固定長の文字列にする */
string bo_to_fixed_str( const BOARD* bo )
{
  char s[104];

  // 盤上
  for (int y = 1; y <= 9; y++) {
    for (int x = 1; x <= 9; x++) 
      s[ (y - 1) * 10 + x - 1 ] = fix_map[ bo->board[(y << 4) + x] ];
    s[ (y - 1) * 10 + 9 ] = '/';
  }

  // 持ち駒
  for (int j = 0; j < 2; j++) {
    for (int i = 1; i <= 7; i++)
      s[ 89 + j * 7 + (i - 1) ] = 'a' + bo->inhand[j][i];
  }

  // 手番
  s[ 103 ] = !bo->next ? '+' : '-';

  return string(s, 104);
}


/** 固定長の文字列から復元する */
bool bo_set_by_fixed_str( BOARD* bo, const string& str )
{
  for (int y = 1; y <= 9; y++) {
    for (int x = 1; x <= 9; x++) {
      const char* p = strchr( fix_map, str[(y - 1) * 10 + x - 1] );
      if (!p)
	return false;
      bo->board[(y << 4) + x] = p - fix_map;
    }
  }

  // 持ち駒
  for (int j = 0; j < 2; j++) {
    for (int i = 1; i <= 7; i++)
      bo->inhand[j][i] = str[89 + j * 7 + (i - 1)] - 'a';
  }

  bo->next = str[103] == '+' ? 0 : 1;

  boSetToBOARD(bo);
  return true;
}
