/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <gtk/gtk.h>


gboolean
on_drawingarea_motion_notify_event     (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data);

gboolean
on_drawingarea_button_press_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_drawingarea_button_release_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_drawingarea_draw( GtkWidget* widget, cairo_t* cr, gpointer user_data );

gboolean
on_drawingarea_leave_notify_event      (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

void
on_edit_flip_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_all_koma_to_pbox_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data);


////////////////////////////////////////////////////////////////////////

void
on_game_remote_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_connect_cancel_button_clicked       (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_bookwindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_disconnect_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_booklist_cursor_changed             (GtkTreeView     *treeview,
                                        gpointer         user_data);

void
on_booklist_cursor_changed             (GtkTreeView     *treeview,
                                        gpointer         user_data);

void
on_file_open_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_file_saveas_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_file_exit_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_game_new_game_activate              (GtkBin* menuitem,
                                        gpointer         user_data);

void
on_game_stop_activate                  (GtkBin* menuitem,
                                        gpointer         user_data);


void
on_game_matta_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_game_giveup_activate                (GtkBin* menuitem,
                                        gpointer         user_data);

void
on_game_restart_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_game_checkmate_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_view_small_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_view_middle_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_view_big_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_view_flip_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_view_bookwindow_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_go_first_activate                   (GtkBin* menuitem,
                                        gpointer         user_data);

void
on_go_back_activate                    (GtkBin* menuitem,
                                        gpointer         user_data);


void
on_go_next_activate                    (GtkBin* menuitem,
                                        gpointer         user_data);

void
on_go_last_activate                    (GtkBin* menuitem,
                                        gpointer         user_data);

void
on_help_about_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

gboolean
on_window_delete_event                 (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void on_window_destroy( GtkWidget* widget,
                                        gpointer         user_data);

void
on_edit_startedit_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_edit_set_hirate_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_edit_set_mate_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_edit_rl_reversal_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_edit_order_reversal_activate        (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_edit_all_koma_to_pbox_activate      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_view_move_property_activate         (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_next_moves_cursor_changed           (GtkTreeView     *treeview,
                                        gpointer         user_data);

void
on_bonanza_setting_button_activate     (GtkButton       *button,
                                        gpointer         user_data);

void
on_connecting_cancelbutton_activate    (GtkButton       *button,
                                        gpointer         user_data);

void on_window_realize( GtkWidget* widget,
                                        gpointer         user_data);

void
on_remote_dialog_realize               (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_network_game_dialog_realize         (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_game_solve_hisshi_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_alt_moves_delete_button_activate    (GtkButton       *button,
                                        gpointer         user_data);

void
on_alt_moves_window_realize            (GtkWidget       *widget,
                                        gpointer         user_data);

gboolean
on_alt_moves_window_delete_event       (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_file_properties_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_game_property_dialog_calendar_button_activate
                                        (GtkButton       *button,
                                        gpointer         user_data);

void
on_calendar_popup_day_selected_double_click
                                        (GtkCalendar     *calendar,
                                        gpointer         user_data);

void
on_file_save_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void 
on_dialog_game_realize             (GtkWidget       *widget,
				    gpointer         user_data);
