/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// 通常探索のテスト

#include <si/si.h>
#include "tests.h"

using namespace std;
using namespace CppUnit;

class SearchTest: public TestCase
{
  CPPUNIT_TEST_SUITE( SearchTest );
  CPPUNIT_TEST( test_1 );
  CPPUNIT_TEST_SUITE_END();

public:
  virtual void setUp()
  {
    newHASH();
  }

  virtual void tearDown()
  {
    freeHASH();
  }

  /** 通常探索 */
  void test_1()
  {
    BOARD* bo = newBOARDwithInit();
    bo_reset_to_hirate(bo);

    TE te;
    bfs_minmax(bo, 61, &te);
    printf("best = %s\n", te_str(te, *bo).c_str() );

    MOVEINFO2 seq;
    seq.count = 0;
    hs_get_best_sequence(-1, bo, &seq);
    mi2_print_sequence(&seq, *bo);

    freeBOARD( bo );
  }

  /** 局面を進めたり戻したりする速度 */
  void test_2()
  {
    BOARD* bo = newBOARDwithInit();
    bo_reset_to_hirate(bo);

    MOVEINFO mi1;
    make_moves( bo, &mi1 );
    for (int i = 0; i < mi1.count; i++) {
      
    }

    freeBOARD( bo );
  }
};
CPPUNIT_TEST_SUITE_REGISTRATION( SearchTest );

int main()
{
  CPPUNIT_ASSERT_EQUAL( true, 
       think_init("../../ai-config/00-only-piece-value.json") );

  TextTestRunner runner;
  TestFactoryRegistry& registry = TestFactoryRegistry::getRegistry();
  runner.addTest( registry.makeTest() );
  return !runner.run("", false);
}
