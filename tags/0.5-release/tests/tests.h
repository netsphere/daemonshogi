
/** daemonshogi 単体テスト */

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TextTestRunner.h>

inline std::ostream& operator << (std::ostream& ost, std::pair<int, int> x) {
  ost << '(' << x.first << ',' << x.second << ')';
  return ost;
}


CPPUNIT_NS_BEGIN
template<>
struct assertion_traits<TE>
{
  static bool equal( const TE& x, const TE& y )
  {
    return !teCmpTE(x, y);
  }

  static std::string toString( const TE& x )
  {
    OStringStream ost;
    te_print(x, ost);
    return ost.str();
  }
};
CPPUNIT_NS_END
