/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// 詰め将棋のテスト

#include <si/si.h>
#include <si/ui.h>
#include "tests.h"
#include <tcutil.h> // TCMAP
#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>

using namespace std;
using namespace CppUnit;


/** 詰将棋のテスト */
class MateTest: public TestCase 
{
  CPPUNIT_TEST_SUITE( MateTest );
  CPPUNIT_TEST( test_3 );
  CPPUNIT_TEST( test_4 );
  CPPUNIT_TEST( test_5 );
  CPPUNIT_TEST( test_6 );

  // まだ解けない
  // CPPUNIT_TEST( test_7 );
  CPPUNIT_TEST_SUITE_END();

public:
  virtual void setUp() 
  {
    newHASH();
  }

  virtual void tearDown() 
  {
    freeHASH();
  }

  /** 詰め将棋 */
  void test_3()
  {
    // http://www.geocities.co.jp/Playtown/6157/3-5te/sannte.html
    BOARD* bo = newBOARDwithInit();
    bo_set_by_text( bo,
                    "P1 *  *  *  *  *  *  * -OU-KY\n"
                    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  *  *  * +TO * -FU\n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00GI\n"
                    "+\n" );

    boSetTsumeShogiPiece(bo);

    // 手の生成
    TE line[] = {
      te_make_put(0x22, GIN),
      te_make_move(0x12, 0x21, false),
      te_make_move(0x22, 0x11, true),
    };
    int i;
    for (i = 0; i < 3; i++) 
      boMove_mate(bo, line[i]);
    MOVEINFO mi;
    uke(bo, &mi);
    CPPUNIT_ASSERT( mi.count != 0 );

    for (i = 0; i < 3; i++)
      boBack_mate(bo);

    // hashが衝突しないか
    uint64_t h[100];
    h[0]= bo->hash_all;
    printf("hash = %" PRIx64 "\n", h[0]);

    TE line2[] = {
      te_make_put(0x22, GIN),
      te_make_move(0x12, 0x21, false),
      te_make_move(0x33, 0x32, false),
    };
    for (i = 0; i < 3; i++) {
      boMove_mate( bo, line2[i] );
      h[i + 1] = bo->hash_all;
      printf("hash = %" PRIx64 "\n", h[i + 1]);
      for (int j = 0; j <= i; j++) {
        CPPUNIT_ASSERT( h[i + 1] != h[j] );
      }
    }
    CPPUNIT_ASSERT( h[1] == ~(h[0] ^ hashval[GIN][0x22] ^ hashseed_inhand[0][GIN][1]) );
    CPPUNIT_ASSERT( h[2] == ((~h[1]) ^ hashval[OH+0x10][0x12] ^ hashval[OH+0x10][0x21]) );
    CPPUNIT_ASSERT( h[3] == ~(h[2] ^ hashval[TOKIN][0x33] ^ hashval[TOKIN][0x32]) );
    for (i = 0; i < 3; i++) {
      boBack_mate(bo);
      CPPUNIT_ASSERT( bo->hash_all == h[2 - i] );
    }

    // df-pnのテスト
    TE te_ret;
    int node_limit = 5000;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, dfpn_mate(bo, 5, &te_ret, &node_limit) );
    CPPUNIT_ASSERT_EQUAL( te_make_put(0x23, GIN), te_ret );

    // 置換表の使われ方
    CPPUNIT_ASSERT( hs_entry_count(0) != 0 );
    CPPUNIT_ASSERT_EQUAL( 0, hs_entry_count(1) );
    CPPUNIT_ASSERT_EQUAL( 0, hs_entry_count(2) );

    freeBOARD(bo);
  }

  /** 詰め将棋: 5手詰め */
  void test_4()
  {
    // http://www.geocities.co.jp/Playtown/6157/3-5te/gotedume.html
    BOARD* bo = newBOARDwithInit();
    bo_set_by_text( bo,
                    "P1 *  *  *  *  *  *  * -KE * \n"
                    "P2 *  *  *  *  *  *  *  * -OU\n"
		    "P3 *  *  *  *  *  * +UM-FU * \n"
		    "P4 *  *  *  *  *  *  *  * -FU\n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00GI00KE\n"
                    "+\n" );

    boSetTsumeShogiPiece(bo);

    TE te_ret;
    int node_limit = 5000;
    CPPUNIT_ASSERT( dfpn_mate(bo, 3, &te_ret, &node_limit) != CHECK_MATE );

    // ハッシュをクリアしないと同じ局面での探索失敗
    newHASH();
    
    node_limit = 5000;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, dfpn_mate(bo, 5, &te_ret, &node_limit) );
    CPPUNIT_ASSERT_EQUAL( te_make_put(0x42, KEI), te_ret );

    freeBOARD( bo );
  }


  /** 双玉、長手数 */
  void test_5()
  {
    // http://members.at.infoseek.co.jp/donotmind/dabun36.html
    BOARD* bo = newBOARDwithInit();
    bo_set_by_text( bo,
                    "P1 *  *  *  *  *  *  *  * -OU\n"
                    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  *  *  * +OU *  * \n"
		    "P4 *  *  *  *  *  * +KE+KY-FU\n"
		    "P5 *  *  *  *  *  * -RY * -UM\n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00KA00KE00KE00KE\n"
                    "+\n" );

    boSetTsumeShogiPiece(bo);

    TE te_ret;
    int node_limit = 5000;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, dfpn_mate(bo, 19, &te_ret, &node_limit) );
    CPPUNIT_ASSERT_EQUAL( te_make_put(0x32, KEI), te_ret );

    printf("%s:%d: ", __func__, __LINE__);
    MOVEINFO2 seq;
    seq.count = 0;
    hs_get_best_sequence(0, bo, &seq);
    mi2_print_sequence(&seq, *bo);

    newHASH();

    // http://www.ne.jp/asahi/tetsu/toybox/kato/tchu.htm
    // No.16
    bo_set_by_text( bo,
                    "P1 *  *  *  *  *  *  *  *  * \n"
                    "P2 *  *  *  *  *  * +KY+KY * \n"
		    "P3 *  *  *  *  * +GI+GI+KE+KI\n"
		    "P4 *  *  *  *  *  * +GI+KE+KI\n"
		    "P5 *  *  *  *  *  * +KI+KE * \n"
		    "P6 *  *  *  *  *  * +KY *  * \n"
		    "P7 *  *  *  *  *  *  *  * +KA\n"
		    "P8 *  *  *  *  * +KY+HI+HI+KA\n"
		    "P9 *  *  *  * +GI-OU * +KE+KI\n"
                    "+\n" );

    boSetTsumeShogiPiece(bo);

    node_limit = 5000;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, dfpn_mate(bo, 41, &te_ret, &node_limit) );
    CPPUNIT_ASSERT_EQUAL( te_make_move(0x83, 0x93, false), te_ret );

    seq.count = 0;
    hs_get_best_sequence(0, bo, &seq);
    mi2_print_sequence(&seq, *bo);

    freeBOARD( bo );
  }


  /** 合い駒, 証明駒 */
  void test_6()
  {
    BOARD* bo = newBOARDwithInit();
    bo_set_by_text( bo,
                    "P1+HI-FU *  *  *  *  *  * -OU\n"
                    "P2 *  *  *  *  *  *  *  *  * \n"
                    "P3 *  *  *  *  *  *  *  * -GI\n"
                    "P4 *  *  *  *  *  *  *  *  * \n"
                    "P5 *  *  *  *  *  *  *  *  * \n"
                    "P6 *  *  *  *  *  *  *  *  * \n"
                    "P7 *  *  *  *  *  *  *  * +FU\n"
                    "P8 *  *  *  *  *  *  *  *  * \n"
                    "P9 *  *  *  *  *  *  * +KY * \n"
                    "+\n" );
    boSetTsumeShogiPiece(bo);

    BOARD* bo2 = bo_dup(bo);
    CPPUNIT_ASSERT( bo->key == bo2->key );
    CPPUNIT_ASSERT( bo->hash_all == bo2->hash_all );

    // 2手進んだところ
    BOARD* bo3 = newBOARDwithInit();
    bo_set_by_text( bo3,
                    "P1 * +RY *  *  *  *  * -FU-OU\n"
                    "P2 *  *  *  *  *  *  *  *  * \n"
                    "P3 *  *  *  *  *  *  *  * -GI\n"
                    "P4 *  *  *  *  *  *  *  *  * \n"
                    "P5 *  *  *  *  *  *  *  *  * \n"
                    "P6 *  *  *  *  *  *  *  *  * \n"
                    "P7 *  *  *  *  *  *  *  * +FU\n"
                    "P8 *  *  *  *  *  *  *  *  * \n"
                    "P9 *  *  *  *  *  *  * +KY * \n"
                    "+\n" );
    boSetToBOARD(bo3);
    printf("hash: %" PRIx64 "\n", bo3->key);

    // ハッシュがおかしい？
    boMove_mate(bo, te_make_move(0x19, 0x18, true));
    uint64_t h2 = ~(bo2->key ^ hashval[HISHA][0x19] ^ hashval[FU+0x10][0x18] ^ hashval[RYU][0x18]);
    CPPUNIT_ASSERT( bo->key == h2 );

    boMove_mate(bo, te_make_put(0x12, FU));
    uint64_t h3 = (~h2) ^ hashval[FU+0x10][0x12];
    CPPUNIT_ASSERT( bo3->key == h3 );
    CPPUNIT_ASSERT( bo3->key == bo->key );

    boMove_mate(bo, te_make_move(0x18, 0x12, false) );
    CPPUNIT_ASSERT( bo->key == ~(h3 ^ hashval[RYU][0x18] ^ hashval[FU+0x10][0x12] ^ hashval[RYU][0x12]) );

    // 詰め探索
    TE te_ret;
    int node_limit = 5000;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, dfpn_mate(bo2, 41, &te_ret, &node_limit) );
    
    MOVEINFO2 seq;
    seq.count = 0;
    hs_get_best_sequence(0, bo2, &seq);
    mi2_print_sequence(&seq, *bo2);

    CPPUNIT_ASSERT_EQUAL( te_make_move(0x19, 0x18, true), te_ret );

    freeBOARD(bo);
    freeBOARD(bo2);
    freeBOARD(bo3);
  }

  // http://www.ne.jp/asahi/tetsu/toybox/kenkyu/ragyoku.htm
  void test_7()
  {
    // 2009.10.8 解けない. 手を展開する順が悪い. 枝刈りが足りない

    printf("%s: enter\n", __func__);

    BOARD* bo = newBOARDwithInit();
    bo_set_by_text( bo,
                    "P1 *  *  *  *  *  *  *  * -OU\n"
                    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  *  *  *  *  *  * \n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00KA00KA00GI00GI00GI00KE\n"
		    "P+00FU00FU00FU00FU00FU\n"
                    "+\n" );
    boSetTsumeShogiPiece(bo);
    printBOARD(bo);

    TE te_ret;
    int node_limit = 80000;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, dfpn_mate(bo, 41, &te_ret, &node_limit) );

    freeBOARD(bo);
  }
};
CPPUNIT_TEST_SUITE_REGISTRATION( MateTest );


int main()
{
  TextTestRunner runner;
  TestFactoryRegistry& registry = TestFactoryRegistry::getRegistry();
  runner.addTest( registry.makeTest() );
  return !runner.run("", false);
}


/*
  詰め将棋の問題

  http://ameblo.jp/professionalhearts/entry-10005160178.html
  via http://d.hatena.ne.jp/mkomiya/20080703/1215088245
 */
