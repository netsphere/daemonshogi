/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// 乱数プレイで詰めろを探す

#include <si/si.h>
#include <si/ui.h>
#include <stdlib.h>
#include <list>
using namespace std;


extern int think_count_king_movables(const BOARD* bo, int side);

static void stat_node_sub(FILE* fp, const BOARD* bo)
{
  // 簡単な実験では
  // sum( (each val - x) * y ) > 0 で詰めろあり
  // x: -12	-9	-20	-33	3	-3	29	33	-9	6	15	-38
  // y: -295	-139	373	334	-356	69	-82	400	368	54	-200	-148

  // 持ち駒
  for (int i = 1; i <= 7; i++)
    fprintf(fp, "%d\t", bo->inhand[bo->next][i]);

  // 相手玉の周りへの利き
  fprintf(fp, "%d\t", think_count_attack8(bo, bo->next, bo->king_xy[1 - bo->next]) );

  // 相手玉の周り24への利き
  fprintf(fp, "%d\t", think_count_attack24(bo, bo->next, bo->king_xy[1 - bo->next]) );

  // 受け方の利き
  fprintf(fp, "%d\t", think_count_attack8(bo, 1 - bo->next, bo->king_xy[1 - bo->next]) );
  
  // 受け方の周り24への利き
  fprintf(fp, "%d\t", think_count_attack24(bo, 1 - bo->next, bo->king_xy[1 - bo->next]) );

  // 受け方の玉の自由度
  fprintf(fp, "%d\t", think_count_king_movables(bo, 1 - bo->next) );
}


/** 詰めろが掛かる局面 (2手スキ) の状況を出力 */
void stat_node( BOARD* bo )
{
  printBOARD(bo);
  printf("move = %s\n", te_str(bo->mseq.te[bo->mseq.count], *bo).c_str() );

  FILE* fp = fopen("tsumero_data.txt", "a");

  // 詰めろあり
  stat_node_sub(fp, bo);
  fprintf(fp, "1\n");

  boBack(bo);
  boBack(bo);

  stat_node_sub(fp, bo);
  fprintf(fp, "0\n");
}


/** 
 * 詰ます手か、詰みがないときはランダムな手を返す
 * @return 手がない (投了)     false
 */
bool mate_or_random_move( BOARD* bo, TE* te_ret )
{
  int node_limit = 2000; // magic
  if ( dfpn_mate(bo, 5, te_ret, &node_limit) == CHECK_MATE )
    return true;

  MOVEINFO mi;
  make_moves( bo, &mi );

  if ( !mi.count )
    return false;
  
  do {
    *te_ret = mi.te[ random() % mi.count ];
  } while ( !te_is_valid(bo, *te_ret) );

  return true;
}


int playout( const BOARD* start_board )
{
  BOARD* bo = bo_dup( start_board );

  int winner = -1;

  while ( bo->mseq.count < 200 ) {
    assert( bo->next == (bo->mseq.count % 2) );

    TE te;
    bool r = mate_or_random_move(bo, &te);
    if (!r) {
      winner = 1 - bo->next;
      break;
    }

    boMove_mate( bo, te );

#if 0
    MOVEINFO mi;
    make_moves(bo, &mi);

    // 詰めろが掛かるか
    for (int i = 0; i < mi.count; i++) {
      // まず指してみる
      boMove( bo, mi.te[i] );

      // 詰めろが掛かった？
      bo_pass(bo);

      TE te_ret;
      int node_limit = 5000; // magic. 深め
      if ( dfpn_mate(bo, 31, &te_ret, &node_limit) == CHECK_MATE ) {
        // 詰めろ.
        boBack_mate(bo); // パスの手
        boBack(bo);
        stat_node(bo);

        goto endgame;
      }

      // 詰めろではない
      boBack_mate(bo); // パスの手
      boBack(bo);
    }
#endif // 0
  }
  
  freeBOARD( bo );
  return winner;
}


// extern GAME g_game;


typedef pair<TE, int> CountPair;

struct SortByCount {
  bool operator ()(const CountPair& x, const CountPair& y) const {
    return x.second > y.second;
  }
};

int main()
{
  int i, j;

  srandom( time(NULL) );
  // srandom( 1 );

  newHASH();

  BOARD* bo = newBOARDwithInit();
  bo_reset_to_hirate(bo);

  list<CountPair> counts;

  MOVEINFO mi;
  // make_moves( bo, &mi );
  mi.count = 1;
  mi.te[0] = te_make_move(0x77, 0x67, false);

  for (i = 0; i < mi.count; i++ ) {
    boMove_mate( bo, mi.te[i] );

    int count = 0;
    for (j = 0; j < 1000; j++) {
      int winner = playout( bo );
      count += winner == 0 ? 2 : (winner == -1 ? 1 : 0);
    }

    boBack_mate( bo );

    printf("%s = %d\n", te_str(mi.te[i], *bo).c_str(), count );

    counts.push_back( CountPair(mi.te[i], count) );
  }

  counts.sort( SortByCount() );
  list<CountPair>::const_iterator it;
  for (it = counts.begin(); it != counts.end(); it++) {
    printf("%s = %d, ", te_str(it->first, *bo).c_str(), it->second);
  }
  printf("\n");

  freeBOARD(bo);
  freeHASH();

  return 0;
}
