/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#define GTK_DISABLE_DEPRECATED 1
#define GDK_DISABLE_DEPRECATED 1
#define G_DISABLE_DEPRECATED 1
#include <config.h>

#include <gtk/gtk.h>

G_BEGIN_DECLS
#include "interface.h"
#include "support.h"
G_END_DECLS

#include "canvas.h"
#include "misc.h"
#include "si/si.h"
#include "si/ui.h"
#include <string>
#include <stdlib.h>
using namespace std;


/** global application instance */
Canvas* g_canvas = NULL;

GtkBuilder* interface_builder = NULL;


static void usage()
{
  fprintf( stderr, "\n" );
  fprintf( stderr, "Daemonshogi %s\n", VERSION );
  fprintf( stderr, "  Copyright (C) Masahiko Tokita    2002-2005,2009\n"
           "  Copyright (C) Hisashi Horikawa   2008-2010\n"
	   "\n"
	   "USAGE\n"
	   "    daemonshogi -c <AI config file>\n" );
}


/* timeout <= 0 の場合は時間制限なし  */
void pending_loop( bool may_block )
{
  if (may_block) {
    do {
      gtk_main_iteration();
    } while (gtk_events_pending());
  }
  else {
    while (gtk_events_pending())
      gtk_main_iteration();
  }
}


int main( int argc, char* argv[] )
{
#ifdef ENABLE_NLS
  bindtextdomain(GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
  textdomain(GETTEXT_PACKAGE);
#endif

  gtk_set_locale();
  gtk_init(&argc, &argv);

  // add_pixmap_directory( PACKAGE_DATA_DIR "/" PACKAGE );

  if (!g_thread_supported())
    g_thread_init(NULL);

  // 思考ファイル
  string think_config_file = PACKAGE_DATA_DIR "/" PACKAGE "/01.json";
  int ch;
  while( (ch = getopt(argc, argv, "c:")) != -1 ) {
    switch (ch) {
    case 'c':
      think_config_file = optarg;
      break;
    default:
      usage();
      exit(1);
      break;
    }
  }

  if ( !think_init(think_config_file) ) {
    printf( "error: ai-config file '%s' not found.\n", 
	    think_config_file.c_str() );
    usage();
    exit(1);
  }
  
  // 定跡が使えるか
  if ( !book_init(PACKAGE_DATA_DIR "/" PACKAGE "/daemonshogi-book.bin", 
		  false) ) {
    printf("warning: book file '%s' not found.\n", 
	               PACKAGE_DATA_DIR "/" PACKAGE "/daemonshogi-book.bin");
    printf("         book disabled.\n");
  }

  // UIの構築
  GError* error = NULL;
  interface_builder = gtk_builder_new();
  if ( !gtk_builder_add_from_file(interface_builder, 
		  PACKAGE_DATA_DIR "/" PACKAGE "/daemonshogi.gtkbuilder",
				  &error) ) {
    g_warning("Couldn't load builder file: %s", error->message);
    g_error_free(error);
    exit(1);
  }
  gtk_builder_connect_signals( interface_builder, NULL );

  // 主ウィンドウを開く
  GtkWidget* window = GTK_WIDGET( 
                        gtk_builder_get_object(interface_builder, "window") );
  g_canvas = daemon_canvas_new(window);

  set_pending_function(pending_loop);

  gtk_widget_show_all(window);
  gtk_main();

  daemon_canvas_free(g_canvas);
  book_free();
  return 0;
}

