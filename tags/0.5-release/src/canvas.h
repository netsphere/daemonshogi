/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _CANVAS_H_
#define _CANVAS_H_

#include <gtk/gtk.h>
#include <sys/time.h>
#include <assert.h>
#include <vector>
#include "si/si.h"
#include "si/dte.h"
#include "si/board-misc.h"
#include "si/record.h"
#include "sprite.h"
#include "conn.h"
#include "history_window.h"


typedef enum {
  /** 待機モード */
  // D_CANVAS_MODE_WAIT,
  
  /** 棋譜モード */
  D_CANVAS_MODE_BOOK,

  /** (局面) 編集モード */
  D_CANVAS_MODE_EDIT,

  /** 対局モード */
  D_CANVAS_MODE_GAME,
  
  /** 詰将棋モード */
  // D_CANVAS_MODE_MATE,
} D_CANVAS_MODE;


/* canvas をサイズを表す定数 */
typedef enum {
  /* 最小のサイズ */
  D_CANVAS_SIZE_SMALL  = 1,
  /* 中のサイズ */
  D_CANVAS_SIZE_MIDDLE = 2,
  /* 最大のサイズ */
  D_CANVAS_SIZE_BIG    = 3,
} D_CANVAS_SIZE;


enum D_CANVAS_AREA {
  /** 該当エリアなし */
  D_CANVAS_AREA_NONE,
  /** 左駒台 */
  D_CANVAS_AREA_LEFT_KOMADAI,
  /** 右駒台 */
  D_CANVAS_AREA_RIGHT_KOMADAI,
  /** 駒箱 */
  D_CANVAS_AREA_KOMABAKO,
  /** 盤 */
  D_CANVAS_AREA_BOARD,
};


#define D_SPRITE_KOMA_WIDTH  40
#define D_SPRITE_KOMA_HEIGHT 40
#define D_CANCAS_SIZE_SMALL_X  640
#define D_CANCAS_SIZE_SMALL_Y  380
#define D_CANCAS_SIZE_MIDDLE_X 800
#define D_CANCAS_SIZE_MIDDLE_Y 475
#define D_CANCAS_SIZE_BIG_X    1024
#define D_CANCAS_SIZE_BIG_Y    608

#define SPRITEMAX 41


/** 子プロセス情報 */
struct ChildProcess 
{
  /** プロセスID */
  pid_t pid;

  /** 書き込み用パイプ */
  int write_fd;

  /** 読み込み用パイプ */
  int read_fd;

  /** 構造体を初期化 */
  void clear() {
    pid = -1; 
    write_fd = -1; read_fd = -1;
  }

  /** 子プロセスを終了させる.
    \todo impl.
   */
  void kill() {
    assert(0); // TODO: impl
  }
};


/** アプリケーションクラス */
struct Canvas 
{
  /** 主window */
  GtkWidget* window;

  /** 棋譜ウィンドウ */
  History* history;

  /** 変化手順ウィンドウ */
  GtkWidget* move_property_dialog;

  /** canvasの横のドット数 */
  gint width;
  /** canvasの縦のドット数 */
  gint height;
  /** 描画するドローイングエリア */
  GtkWidget *drawingarea;
  /** ダブルバッファリングのためのpixmap */
  GdkPixmap *pixmap[3];
  /** ドラッグ中に使う作業用 pixmap */
  GdkPixmap *tmp_pixmap;
  /** 背景のpixmap */
  GdkPixmap *back[3];
  /** 駒の画像 */
  GdkPixmap *koma[3];
  /** 駒のマスク */
  GdkBitmap *mask[3];
  /** 背景の元の画像 */
  GdkPixbuf *back_src;
  /** 駒の元の画像 */
  GdkPixbuf *koma_src;
  /** pixmapのgc */
  GdkGC *gc;

  /** 駒スプライト */
  Sprite *sprite[SPRITEMAX];

  /** 前回クリックした時刻 */
  struct timeval tv;
  /** ドラッグ中か？のフラグ */
  gboolean drag;
  /** ドラッグ中のスプライドの番号 */
  gint drag_no;
  /** スプライトとマウスカーソルの座標の差分 */
  GdkPoint diff;

  /** 現在の局面 */
  BOARD board;

  /** モード */
  D_CANVAS_MODE mode;

  /** canvas のサイズを表す定数 */
  D_CANVAS_SIZE size;

  /** record. 棋譜 */
  Record* record;

  /** 表示する手前側の手番 */
  TEBAN front;

  /** 思考中か？のフラグ */
  gboolean flg_thinking;

  /** 手合い */
  D_TEAI teai;

  /** 対局者 */
  PLAYERTYPE playertype[2];

  /** GUIで入力する手 */
  DTe gui_te;

  /** 子プロセス情報 */
  ChildProcess child_process[2];

  guint timeout_id;
  int elapsed;

  Canvas(GtkWidget* window_): window(window_) { }

  ~Canvas() {
    if (record) {
      daemon_record_free(record);
      record = NULL;
    }

    if (history) {
      delete history;
      history = NULL;
    }
  }

  void update_window_title();
};


/** アプリケーションインスタンス */
extern Canvas* g_canvas;


Canvas* daemon_canvas_new           (GtkWidget *window);

// void    daemon_canvas_init          (Canvas *canvas, GtkWidget *window);

void    daemon_canvas_free          (Canvas *canvas);
void    daemon_canvas_init_sprite   (Canvas *canvas);
void    daemon_canvas_dispose_sprite(Canvas *canvas);

/* set_* , get_* */
GtkWidget*    daemon_canvas_get_window    (Canvas *canvas);
GdkGC*        daemon_canvas_get_gc        (Canvas *canvas);
GdkPixmap*    daemon_canvas_get_pixmap    (Canvas *canvas);
GdkPixmap*    daemon_canvas_get_back      (Canvas *canvas);
GtkWidget*    daemon_canvas_get_drawinarea(Canvas *canvas);
// gboolean      daemon_canvas_isdrag        (Canvas *canvas);
void          daemon_canvas_set_drag      (Canvas *canvas,
					   gboolean drag);
gint          daemon_canvas_get_dragno    (Canvas *canvas);
void          daemon_canvas_set_dragno    (Canvas *canvas,
					   gint no);
GdkPoint      daemon_canvas_get_diff      (Canvas *canvas);
void          daemon_canvas_set_diff      (Canvas *canvas,
					   gint x,
					   gint y);

D_CANVAS_MODE daemon_canvas_get_mode      (const Canvas* canvas);

void          daemon_canvas_set_size      (Canvas* canvas,
					   D_CANVAS_SIZE size);
Sprite*       daemon_canvas_get_sprite    (Canvas* canvas, gint no);
void          daemon_canvas_set_front     (Canvas *canvas, TEBAN next);
TEBAN       daemon_canvas_get_front     (const Canvas* canvas);

/* event */

void       daemon_canvas_motion_notify              (Canvas *canvas, GdkEventMotion  *event);
void       daemon_canvas_button_press               (Canvas *canvas, GdkEventButton  *event);
void       daemon_canvas_button_release             (Canvas *canvas, GdkEventButton  *event);

void       daemon_canvas_bookwindow_activate        (Canvas *canvas,
						     GtkMenuItem *menuitem,
						     gpointer user_data);
void       daemon_canvas_bookwindow_click (GtkTreeView* treeview,
					   int nth);

void       daemon_canvas_bookwindow_destroy         (Canvas *canvas,
						     GtkObject *object,
						     gpointer user_data);
void       daemon_canvas_change_size                (Canvas *canvas, D_CANVAS_SIZE size);

void       daemon_canvas_change_mode                (Canvas *canvas, D_CANVAS_MODE mode);
void       daemon_canvas_change_size_sprite         (Canvas *canvas, D_CANVAS_SIZE size);

// gboolean daemon_canvas_timeout ( gpointer data );

void       daemon_kiffile_load_ok_button_pressed    (const char* filename);

void daemon_kiffile_save_ok_button_pressed( const std::string& filename,
                                            FileWriter* writer,
                                            Record::FileType filetype );

void       daemon_canvas_leave_notify               (Canvas *canvas,
						     GtkWidget *widget,
						     GdkEventCrossing *event,
						     gpointer user_data);

#if 0
void       daemon_canvas_kif_first                  (Canvas *canvas);
void       daemon_canvas_kif_back                   (Canvas *canvas);
void       daemon_canvas_kif_next                   (Canvas *canvas);
void       daemon_canvas_kif_last                   (Canvas *canvas);
#endif // 0

void       daemon_canvas_set_kif_sensitive          (Canvas *canvas);
void       daemon_canvas_set_sensitive              (Canvas *canvas);

#if 0
void       daemon_canvas_set_mate_activate          (Canvas *canvas);
void       daemon_canvas_set_rl_reversal_activate   (Canvas *canvas);
void       daemon_canvas_set_order_reversal_activate(Canvas *canvas);
void       daemon_canvas_set_edit_flip_activate     (Canvas *canvas);
void       daemon_canvas_all_koma_to_pbox_activate  (Canvas *canvas);
#endif // 0

int        daemon_canvas_ok_cancel_dialog           (Canvas* canvas,
						     const char* message);
int        daemon_canvas_yes_no_dialog              (Canvas* canvas,
						     const char* message);

void       daemon_canvas_mate_think_cancel();


///////////////////////////////////////////////////////////////////

int daemon_game_new_game_ok(GtkWidget* dialog_game);

void daemon_game_start_via_network();

void daemon_game_setup( PLAYERTYPE playertype1, const char* playername1,
		        PLAYERTYPE playertype2, const char* playername2 );

void daemon_game_main_loop();

bool daemon_game_mate_setup();

void daemon_game_mate_main_loop(int tesuki, int max_depth, int node_count);

#if 0
void       daemon_canvas_stop_activate              (Canvas *canvas);
void       daemon_canvas_restart_activate           (Canvas *canvas);
void       daemon_canvas_give_up_activate           (Canvas *canvas);
#endif // 0

void       daemon_canvas_update_statusbar           (Canvas *canvas);
void       daemon_canvas_update_statusbar_te        (Canvas* canvas,
						     const DTe* te);

/* draw */

void daemon_canvas_draw_back                       (Canvas *canvas);
void daemon_canvas_draw_back_update                (Canvas *canvas,
						    GdkRectangle *rect);
void daemon_canvas_draw_back_update_from_tmp_pixmap(Canvas *canvas,
						    GdkRectangle *rect);
void daemon_canvas_draw_time                       (Canvas *canvas);

void canvas_redraw(Canvas* canvas);
void canvas_build(Canvas* canvas);

/* etc */
void          daemon_canvas_create_back_pixmap(Canvas *canvas);
gint          daemon_canvas_on_sprite         (Canvas *canvas,
					       GdkEventButton *event);
gboolean      daemon_canvas_sprite_on_board   (Canvas *canvas,
					       Sprite *sprite);
gboolean      daemon_canvas_is_doubleclick    (Canvas* canvas);
void          daemon_canvas_create_tmp_pixmap (Canvas* canvas);
GtkWidget*    daemon_create_fileselection     (void);
GtkWidget*    daemon_create_fileselection_save(void);
void          daemon_messagebox   (Canvas* canvas, 
				   const char* s,
				   GtkMessageType mes_type);

gint          daemon_canvas_rate_xy           (const Canvas* canvas,
					       gint value);
// gint          daemon_canvas_rerate_xy         (Canvas* canvas,
//					       gint value);
D_CANVAS_AREA daemon_canvas_on_drag_area      (Canvas *canvas,
					       gint x, gint y,
					       gint *dest_x,
					       gint *dest_y);
// GtkWidget*    daemon_create_dialog_game       (void);

// INPUTSTATUS daemon_input_next_gui_human_impl( const BOARD *bo,
//					       TE *te);

gint          count_pbox                      (DBoard *board);
void          create_te_string                (const DBoard* board,
					       int no,
					       const DTe* te,
					       char* buf);
void          create_te_string2               (const DBoard* board,
					       int no,
					       const DTe* te,
					       char* buf);


bool bookwindow_item_is_actived(Canvas* canvas);
void history_window_menuitem_set_active(Canvas* canvas, bool a);
void history_window_show(Canvas* canvas);

extern GAME g_game;


#endif /* _CANVAS_H_ */

