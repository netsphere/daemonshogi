/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// TCP/IP library.

// リファレンス
// http://msdn.microsoft.com/en-us/library/ms740673(VS.85).aspx


#define GTK_DISABLE_DEPRECATED 1
#define GDK_DISABLE_DEPRECATED 1
#define G_DISABLE_DEPRECATED 1

#include <config.h>

#ifdef _WINDOWS
  #define WIN32_LEAN_AND_MEAN
  #define _WIN32_WINNT  0x0501
  #include <winsock2.h>
  #include <ws2tcpip.h>
#else  // UNIX
  #include <sys/socket.h>
  #include <netdb.h>
  #include <unistd.h>
  #include <errno.h>
  #define closesocket close
  #define WSAGetLastError() errno
  #define WSACleanup() 
#endif

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <string>
#include <list>
using namespace std;

G_BEGIN_DECLS
#include "support.h"
#include "interface.h"
G_END_DECLS

#include "conn.h"
#include "canvas.h"
#include "si/ui.h"


/** 行に分けた受信データ */
static RecvBuffer recv_buffer;

static int wait_state = 0;
static gint io_watch = 0;
static int server_fd = -1;
// static guint idle_id = 0;

/** 受信した指し手 */
struct NetTe 
{
  /** 指し手. 投了, 千日手なども */
  DTe te;

  /** 消費秒数 */
  int sec;

  NetTe(): sec(0) { }
};

typedef list<NetTe> NetTeList;
static NetTeList net_te_list;

extern GtkWidget* connect_status_dialog;

/** サーバとの接続を切断。 */
void disconnect_server()
{
  gtk_widget_hide(connect_status_dialog);

  if (server_fd < 0)
    return;

  recv_buffer.clear();
  wait_state = 0;
  net_te_list.clear();

  g_source_remove(io_watch);
  io_watch = 0;

  closesocket(server_fd);
  server_fd = -1;

  // daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_BOOK);
}


/** 0終端の文字列を送信 */
static void csa_send(const char* line)
{
  printf("send: %s", line); // DEBUG
  int ret = write(server_fd, line, strlen(line));
  if (ret < 0) {
    printf("%s: socket write error.\n", __func__);
    exit(1);
  }
}


static string username;

NetGameInfo game_info;


/**
 * 持ち時間をパース.
 */
static bool parse_time()
{
  while ( recv_buffer.recv_list.size() > 0 ) {
    string line = recv_buffer.recv_list.front();
    recv_buffer.recv_list.pop_front();

    if (line.find("END Time") == 0) {
      wait_state = 2;
      return true;
    }
    
    string name, value;
    int pos;

    if ( (pos = line.find(":")) > 0 ) {
      name = line.substr(0, pos);
      value = line.substr(pos + 1);
    }
    else
      name = "";

    if (name == "Time_Unit") {
      if ( value != "1sec" ) {
	si_abort( "not support except 'one second.' value = %s\n", 
		  value.c_str() );
      }
      // TODO: impl.
    }
    else if (name == "Total_Time")
      game_info.total_time = atoi(value.c_str());
    else if (name == "Byoyomi")
      ;
    else if (name == "Least_Time_Per_Move")
      ;
  }

  return false; // 引き続き呼び出してもらう
}


/**
 * CSAサーバからゲーム提案を受信する.
 * http://www.computer-shogi.org/protocol/tcp_ip_server_112.html
 * @return キューを消化したときは true
 */
static bool state_2_read()
{
  assert(g_canvas);
  printf("%s\n", __func__); // DEBUG

  bool finished = false;

  while ( recv_buffer.recv_list.size() > 0 ) {
    string line = recv_buffer.recv_list.front();
    recv_buffer.recv_list.pop_front();

    string name, value;
    int pos;

    if ( (pos = line.find(":")) > 0 ) {
      name = line.substr(0, pos);
      value = line.substr(pos + 1);
    }
    else
      name = "";

    if (name == "Game_ID")
      game_info.game_id = value;
    else if (name == "Name+") 
      game_info.player_name[0] = value;
    else if (name == "Name-") 
      game_info.player_name[1] = value;
    else if (name == "Your_Turn") {
      game_info.my_turn = value[0];
      assert( game_info.my_turn == '+' || game_info.my_turn == '-' );
    }
    else if ( line.find("BEGIN Time") == 0) {
      wait_state = 21;
      return false;
    }
    else if ( line.find("BEGIN Position") == 0) {
      // TODO: impl.
    }
    else if ( line.find("END Game_Summary") == 0) {
      finished = true;
      break;
    }
  }

  if (!finished) {
    printf("data partial.\n"); // DEBUG
    return false; // まだ全部届いていない
  }

  printf("receive summary.\n"); // DEBUG
  return true;
}


/** ゲーム開始 or 対戦相手からreject */
static bool state_3_read()
{
  printf("%s: line = %s\n", __func__, 
	 recv_buffer.recv_list.front().c_str()); // DEBUG

  string line = recv_buffer.recv_list.front(); 
  recv_buffer.recv_list.pop_front();
  if (line.find("START:", 0) == string::npos) {
    // reject ?
    // network_log_append_message("reject game.\n");
    // network_game_start = -1;
    return false;
  }

  // network_game_start = 1;
  wait_state = 4;
  return true;
}


const char csa_koma_str[][3] = {
  "",   "FU", "KY", "KE", "GI", "KI", "KA", "HI",
  "OU", "TO", "NY", "NK", "NG", "",   "UM", "RY",
};

/** 手を作る. daemon_record_load_csa_te() を参考に。 */
static void parse_te( const string& line, NetTe* net_te )
{
  assert( net_te );

  int idx = 0;

  if ( !line.find("%TORYO") ) {
    net_te->te.clear();
    net_te->te.special = DTe::RESIGN;
    idx += strlen("%TORYO");
  }
  else if ( !line.find("%KACHI") ) {
    net_te->te.clear();
    net_te->te.special = DTe::KACHI;
    idx += strlen("%KACHI");
  }
  else {
    // 通常の指し手
    assert( line[0] == '+' || line[0] == '-' );

    char koma_str[3];
    PieceKind p;

    net_te->te.clear();
    net_te->te.special = DTe::NORMAL_MOVE;
    // xyが逆
    Xy fm = ((line[2] - 0x30) << 4) + line[1] - 0x30;
    Xy to = ((line[4] - 0x30) << 4) + line[3] - 0x30;

    sscanf(line.c_str() + 5, "%2s", koma_str );
    for (p = 1; p < 0xf; p++) {
      if (strcmp(koma_str, csa_koma_str[p]) == 0)
	break;
    }

    if ( fm == 0 ) {
      // 駒打ち
      net_te->te.to = to; net_te->te.uti = p;
    }
    else {
      // 移動
      PieceKind pf = daemon_dboard_get_board(&g_canvas->board, 
				     fm & 0xf, 
				     fm >> 4) & 0xf;
      net_te->te.fm = fm;
      net_te->te.to = to;
      net_te->te.nari = pf <= 8 && p >= 9 ? true : false;
      net_te->te.tori = g_canvas->board.board[to];
    }

    idx += 7;
  }

  // 消費時間
  net_te->sec = 1; // 0 はない
  sscanf( line.c_str() + idx, ",T%d", &net_te->sec );

  printf("%s: parsed = %s, %d\n", 
	 __func__, net_te->te.to_str(g_canvas->board).c_str(), net_te->sec);
}


static NetTe pending_move;

/** 相手の手または自分の手番で時間切れを受信 */
static bool state_4_read()
{
  printf("%s: front = '%s'\n", __func__, 
	 recv_buffer.recv_list.front().c_str()); // DEBUG

  string line = recv_buffer.recv_list.front();
  recv_buffer.recv_list.pop_front();

  NetTe net_te;

  if (line == "#JISHOGI") {
    // プロトコルv1.1.2では「日本将棋連盟ルール」とだけ書いている.
    // 24点法だと思うが、そうすると引き分けの場合がある
    //
    // http://www.computer-shogi.org/wcsc20/rule.html
    // によれば, プロトコルを無視して, 27点法によるみたい
    //   → 引き分けはない
    assert( pending_move.te.special == DTe::KACHI );
  }
  else if (line == "#SENNICHITE") {
    // 連続王手でない千日手. 指してもよいがそこで終了. これだけ扱いが違う
    // クライアントでも千日手チェックするか, 非同期で受信しないといけない
    //   +6978KI,T2
    //   #SENNICHITE
    //   #DRAW
    assert( pending_move.te.special == DTe::SENNICHITE );
  }
  else if (line == "#OUTE_SENNICHITE") {
    // 連続王手の千日手
    // これもクライアントでのチェックが必要
    //   -8493OU,T2
    //   #OUTE_SENNICHITE
    //   #WIN
    assert( pending_move.te.special == DTe::ILLEGAL );
  }
  else if (line == "#ILLEGAL_MOVE") {
    // これもクライアントでのチェックが必要
    //   +0031FU,T1
    //   #ILLEGAL_MOVE
    //   #WIN
    assert( pending_move.te.special == DTe::ILLEGAL || 
	    pending_move.te.special == DTe::KACHI );

    pending_move.te.special = DTe::ILLEGAL; // 不正な入玉勝利宣言
  }
  else if (line == "#TIME_UP") {
    // 時間切れ. 1行目で判断できる
    // #TIME_UP
    // #WIN
    pending_move.te.special = DTe::TIME_OUT;
  }
  else if (line == "#CHUDAN") {
    // サーバからの中断指示
    // クライアントからの中断要請もできるが, プロトコルv1.1.2では何が起こるか不明
    gmSetGameStatus( &g_game, SI_CHUDAN_REMOTE );
  }
  else if ( line == "#RESIGN" ) {
    assert( pending_move.te.special == DTe::RESIGN );
  }
  else if ( line == "#WIN" || line == "#LOSE" || line == "#DRAW" ) {
    assert( pending_move.te.special );

    if ( pending_move.te.special == DTe::TIME_OUT )
      gmSetGameStatus( &g_game, SI_TIMEOUT );
    else {
      net_te_list.push_back( pending_move );
      printf("%s: add net_te = %s\n", __func__, 
	   pending_move.te.to_str(g_canvas->board).c_str() ); // DEBUG
    }
  }
  else {
    // 通常手, 投了, 勝利宣言
    parse_te( line, &net_te );
    bool forbidden = false;
    const BOARD* bo = &g_canvas->board;

    if (net_te.te.special == DTe::RESIGN) {
      // 勝ったとき. #RESIGN, #WINは無視してよい
      //   %TORYO.T4
      //   #RESIGN
      //   #WIN
      pending_move = net_te;
    }
    else if (net_te.te.special == DTe::KACHI) {
      // 勝利宣言. クライアントでチェックするか2行目を待つか
      // shogi-server では #DRAW を返すことはないようだが?
      //   %KACHI,T8
      //   #JISHOGI
      //   #WIN
      // または
      //   %KACHI,T8
      //   #ILLEGAL_MOVE
      //   #WIN
      pending_move = net_te;
    }
    else if ( !te_is_valid(bo, net_te.te.pack()) ) {
      pending_move = net_te;
      pending_move.te.special = DTe::ILLEGAL;
      // #ILLEGAL_MOVE を待つ
    }
    else if ( te_is_sennichite(bo, net_te.te.pack(), &forbidden) ) {
      pending_move = net_te;
      pending_move.te.special = forbidden ? DTe::ILLEGAL : DTe::SENNICHITE;
      // #OUTE_SENNICHITE または #SENNICHITE を待つ
    }
    else {
      net_te.te.special = DTe::NORMAL_MOVE;
      net_te_list.push_back(net_te);
    }
  }

  return true;
}


/** 
 * ネットワーク経由の手を格納.
 * ブロックする 
 */
INPUTSTATUS daemon_input_next_network_impl( const BOARD* bo, 
					    DTe* te, int* elapsed_time )
{
  // 手が設定されるまで待つ
  while (net_te_list.size() == 0) {
    // 時間切れ, 中断
    if ( gmGetGameStatus(&g_game) != 0 )
      return gmGetGameStatus(&g_game);

    processing_pending( true );
  }

  NetTe net_te = net_te_list.front();
  net_te_list.pop_front();

  *te = net_te.te;
  *elapsed_time = net_te.sec;
  return SI_NORMAL;
}


/** ログイン待ち */
static void state_1_read()
{
  string s = "LOGIN:" + username + " OK";
  if ( recv_buffer.recv_list.front() == s) {
    // network_log_append_message(_("%s login ok.\n"), arg->username.c_str());
    // arg->status_queue.push_back(_("login ok."));
    recv_buffer.recv_list.pop_front();
    wait_state = 2;
  }
  else {
    disconnect_server();
    daemon_messagebox(g_canvas, _("login failed."), GTK_MESSAGE_ERROR);
    g_signal_emit_by_name(
		  G_OBJECT(lookup_widget(g_canvas->window, "connect_server")),
		  "activate", NULL);
  }
}


#if 0
struct SocketHook {
  bool (*on_game_proposal)();
  
  void (*on_game_start)();
 
};
#endif // 0


/** 
 * ソケットのコールバック関数.
 * 行を切り出して、recv_list に追加する. 
 */
static gboolean on_socket_read( GIOChannel* source,
                                GIOCondition condition,
                                gpointer data )
{
  if ( !recv_buffer.read_from_server(server_fd) ) {
    // network_log_append_message(_("server shutdown or an error occured.\n"));
    disconnect_server();
    return FALSE;
  }

  GtkWidget* dialog;

  while ( recv_buffer.recv_list.size() > 0) {
    switch (wait_state) {
    case 1:
      // ログイン待ち
      state_1_read();
      break;
    case 2:
      // ゲーム情報待ち
      if (!state_2_read()) {
	if (wait_state == 2)
	  return TRUE; // データが一部届いていない. 届くまで待つ
	break;
      }

      gtk_widget_hide(connect_status_dialog);

      // 受けるか拒否するかだけ
      dialog = create_network_game_dialog();
      if ( gtk_dialog_run(GTK_DIALOG(dialog)) != GTK_RESPONSE_OK ) {
        csa_send_logout();
        disconnect_server();
	gtk_widget_hide(dialog);
        return FALSE;
      }
      gtk_widget_hide(dialog);

      csa_send("AGREE\n");
      wait_state = 3;
      break;
    case 21:
      // ゲーム情報 -> 持ち時間
      if ( !parse_time() )
	return TRUE; // 一部届いていない
      break;
    case 3:
      // ゲーム開始待ち
      if (state_3_read()) {
	g_signal_emit_by_name(G_OBJECT(g_canvas->window), 
			      "notify::startgame", NULL);
	return FALSE; // watchをいったん終了
      }
      else {
        // 対戦相手からreject
        csa_send_logout();
        disconnect_server(); 
        daemon_messagebox(g_canvas, _("reject from the opponent"), 
			  GTK_MESSAGE_INFO);
        return FALSE;
      }
      break;
    case 4:
      // 相手の手または自分の手番で時間切れ
      state_4_read();
      break;
    default:
      printf("wait_state = %d\n", wait_state); // DEBUG
      abort();
      break;
    }
  }
  return TRUE; // 引き続き呼び出してもらう
}


/** 
 * WinSockの初期化 
 * @return 成功したとき1 
 */
static int init_socket()
{
#ifdef _WINDOWS
  WSADATA wsaData;
  int r = ::WSAStartup(MAKEWORD(2, 2), &wsaData);
  if (r != 0) {
    printf("WSAStartup() failed: %d\n", r);
    exit(1);
  }
#endif
  return 1;
}


static int socket_inited = 0;

/** 
 * サーバに接続する。
 * @param hostname IPv4 or IPv6 ホスト名
 * @param service  ポート番号の文字列
 * @return 成功したら1
 */
int connect_to_server(const string& hostname, const string& service)
{
  struct addrinfo hints;
  struct addrinfo* res = NULL;
  struct addrinfo* ai;

  if (!socket_inited) {
    socket_inited = init_socket();
    if (!socket_inited)
      return 0;
  }

  // ホスト情報を得る
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;   // IPv4/IPv6両対応
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = NI_NUMERICSERV; // AI_NUMERICSERV // serviceはポート番号に限る

  int r = getaddrinfo(hostname.c_str(), service.c_str(), &hints, &res);
  if (r != 0) {
    printf("getaddrinfo() failed: %s\n", gai_strerror(r));
    return 0;
  }

  // 接続する
  int sockfd = -1;
  for (ai = res; ai; ai = ai->ai_next) {
    sockfd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
    if (sockfd < 0) {
      printf("Error at socket(): %d\n", WSAGetLastError());
      break; // 致命的エラー
    }
    if (::connect(sockfd, ai->ai_addr, ai->ai_addrlen) < 0) {
      closesocket(sockfd);
      sockfd = -1;
      continue; // 別のアドレスを試す
    }
    // ok
    break;
  }
  freeaddrinfo(res);

  server_fd = sockfd;
  return sockfd > 0;
}


///////////////////////////////////////////////////////////////////
// CSAプロトコル


/**
 * ログイン要求を送信する.
 * @return   1 成功 
 */
static int csa_send_login(const string& username, const string& password)
{
  string buf = "LOGIN " + username + " " + password + "\n";
  csa_send(buf.c_str());
  return 1;
}


/**
 * ログアウト要求を送信する
 */
void csa_send_logout()
{
  csa_send("LOGOUT\n");

// TODO:  "LOGOUT:completed" を待つ
}


int csa_send_chudan_request()
{
  csa_send("%CHUDAN\n");
  return 1;
}


/** CSAサーバに接続してログインする. ワーカースレッドとして呼び出す */
void* csa_connect_thread_main(void* arg_)
{
  ConnectThreadArg* arg = static_cast<ConnectThreadArg*>(arg_);

  // network_log_append_message("connect to %s:%s\n",
  //                           arg->hostname.c_str(), arg->port.c_str());
  arg->status_queue.push_back(_("connect..."));

  if (!connect_to_server(arg->hostname, arg->port)) {
    arg->result = -1;
    return NULL;
  }

  arg->result = +1;
  return NULL;
}


/** ソケットを待つコールバック関数を登録する */
void add_watch_socket()
{
  GIOChannel* channel = g_io_channel_unix_new(server_fd);
  io_watch = g_io_add_watch( channel, 
                             GIOCondition( G_IO_IN | G_IO_HUP | G_IO_ERR ),
                             on_socket_read,
                             NULL );
  assert( io_watch > 0 );
  g_io_channel_unref( channel );

  // printf("io_watch = %d\n", io_watch); // DEBUG
}


void csa_login_and_wait_socket(const char* username_, const char* password)
{
  add_watch_socket();
  wait_state = 1;  // サーバからのログイン許可を待つ

  // 引き続き、ログインする。
  // network_log_append_message("LOGIN %s\n", arg->username.c_str());
  username = username_;
  csa_send_login(username_, password);
}


/** 
 * 指し手を送信.
 * daemon_record_output_csa_te() を参考に。 
 */
int csa_send_move(const BOARD* board, int is_sente, const DTe& te)
{
  if (te.special == DTe::RESIGN) {
    csa_send("%TORYO\n");
    return 1;
  }

  // 通常の指し手
  char buf[100];
  if ( !te.fm ) {
    // 打ち
    sprintf(buf, "%c%d%d%d%d%s\n",
	    is_sente ? '+' : '-',
	    0, 0, 
	    te.to & 0xf, te.to >> 4,
	    csa_koma_str[te.uti]);
  }
  else {
    // 移動
    PieceKind p = daemon_dboard_get_board(board, te.fm & 0xf, te.fm >> 4);
    if ( te.nari ) p += 8;

    sprintf(buf, "%c%d%d%d%d%s\n",
	    is_sente ? '+' : '-',
	    te.fm & 0xf, te.fm >> 4,
	    te.to & 0xf, te.to >> 4,
	    csa_koma_str[p & 0xf]);
  }
  csa_send(buf);

  return 1;
}


/**
 * 手を送信した結果 (経過秒数など) を得る.
 * ブロックする。
 */
INPUTSTATUS csa_wait_for_result( DTe* te, int* elapsed_time )
{
  printf("%s\n", __func__); // DEBUG

  while (net_te_list.size() == 0)
    processing_pending( true );

  NetTe net_te = net_te_list.front();
  net_te_list.pop_front();

  // 千日手のことがある
  *te = net_te.te;
  *elapsed_time = net_te.sec;
  printf("my time = %d\n", *elapsed_time); // DEBUG

  // きわどいタイミングで時間切れのことがある
  return gmGetGameStatus(&g_game);
}
