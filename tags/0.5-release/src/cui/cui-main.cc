/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#define G_DISABLE_DEPRECATED 1
#define _XOPEN_SOURCE 700 // SUSv4
#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <si/si.h>
#include <si/ui.h>
#include <glib.h>
#include <string>
#include <vector>
using namespace std;


static void cui_usage()
{
  fprintf( stderr, "Daemonshogi %s\n", VERSION );
  fprintf( stderr, "  Copyright (C) Masahiko Tokita    2002-2005,2009\n"
           "  Copyright (C) Hisashi Horikawa   2008-2010\n"
	   "\n"
	   "USAGE\n"
	   "    daemonshogi-cui [options ...]\n"
           "OPTIONS\n"
           "        -b file   read board file.\n"
	   "        -n        Flip board.\n"
	   "        -h        Print help. This output.\n" );
}


extern GAME g_game;
static RecvBuffer recv_buffer;
static gint io_watch = 0;
static int wait_state = 0;

void shutdown_io_watch()
{
  recv_buffer.clear();
  wait_state = 0;

  g_source_remove(io_watch);
  io_watch = 0;
}


void print_prompt()
{
  printf("> ");
  fflush(stdout);
}


extern int flgDisplayMode;

INPUTSTATUS si_input_next_human(const BOARD* bo, DTe* te, int* elapsed_time );

/**
 * コマンド入力を受け付ける.
 * \todo コマンドは何かに準拠する？
 */
static void state_1()
{
  string cmd = recv_buffer.recv_list.front();
  recv_buffer.recv_list.pop_front();

  string name, value;
  string::size_type p;

  if ( (p = cmd.find(" ")) != string::npos ) {
    name = string(cmd, 0, p);
    value = string(cmd, p + 1);
  }
  else {
    name = cmd;
    value = "";
  }

  if ( name == "load" ) {
    // TODO: impl
  }
  else if ( name == "quit") {
    exit(0);
  }
  else if ( name == "newgame" ) {
    vector<string> pt;
    str_split(&pt, value, ' ');

    if ( (pt[0] != "human" && pt[0] != "ai") ||
	 (pt[1] != "human" && pt[1] != "ai") ) {
      printf("  invalid player type.\n");
      print_prompt();
      return;
    }

    if ( pt[0] == "ai" && pt[1] == "ai" ) {
      printf("  cannot play AI vs AI.\n");
      print_prompt();
      return;
    }

    for (int i = 0; i < 2; i++) {
      gmSetInputFunc( &g_game, i,
          pt[i] == "human" ? si_input_next_human : si_input_next_computer );
    }

    wait_state = 2;
    return;
  }
  else if ( name == "flip" )
    flgDisplayMode = 1 - flgDisplayMode;
  else if ( name == "help" )
    putHelp();
  else {
    printf("  invalid command.\n");
  }

  print_prompt();
}


/** 
 * 指し手、投了、待ったを受け付ける 
 * 
 * 指し手::
 *     xyxypi
 *         移動前の位置、移動後の位置、移動後の駒名
 *         ex) 3324NG ... 2四銀成
 *     00xypi
 *         駒を打つ場合
 *     %toryo
 *     %chudan
 *     %matta
 */
static void state_2()
{
}


static gboolean on_stdin_read( GIOChannel* source,
			       GIOCondition condition,
			       gpointer data )
{
  if ( !recv_buffer.read_from_server(STDIN_FILENO) ) {
    shutdown_io_watch();
    return FALSE;
  }

  while ( recv_buffer.recv_list.size() > 0 ) {
    switch (wait_state) {
    case 1:
      // コマンド入力
      state_1();
      break;
    case 2:
      // ゲーム中
      state_2();
      break;
    default:
      assert(0);
      break;
    }
  }

  return TRUE; // 引き続き呼び出してもらう
}


static void add_watch_stdin()
{
  wait_state = 1;

  GIOChannel* channel = g_io_channel_unix_new( STDIN_FILENO );
  io_watch = g_io_add_watch( channel,
			     GIOCondition(G_IO_IN | G_IO_HUP | G_IO_ERR),
			     on_stdin_read,
			     NULL );
  assert( io_watch > 0 );
  g_io_channel_unref( channel );
}


static void cui_pending_loop( bool may_block )
{
  if ( may_block ) {
    do {
      g_main_context_iteration( NULL, TRUE );
    } while ( g_main_context_pending(NULL) );
  }
  else {
    while ( g_main_context_pending(NULL) )
      g_main_context_iteration(NULL, FALSE);
  }
}


int main(int argc, char* argv[])
{
  srand( time(NULL) );
  newHASH();

  int c;

  BOARD* bo = newBOARDwithInit();
  bo_reset_to_hirate( bo );

  flgDisplayMode = 0;

  while ( (c = getopt(argc, argv, "b:hn")) != -1 ) {
    switch ( c ) {
    case 'b':
      if ( !bo_load_file(bo, optarg) ) {
	fprintf( stderr, "File not found or illegal board.\n" );
	exit( EXIT_FAILURE );
      }	
      break;
    case 'n':
      flgDisplayMode = 1;
      break;
    case 'h':
    default:
      cui_usage();
      exit(EXIT_FAILURE);
      break;
    }
  }
  
  initGAME(&g_game);

  /* ゲーム開始 */
  // play( bo, &g_game);

  GMainLoop* loop = g_main_loop_new(NULL, FALSE);

  set_pending_function( cui_pending_loop );
  add_watch_stdin();

  printf("# Welcome to Daemonshogi.\n");
  printf("# To start game, newgame (ai|human) (ai|human)\n");
  print_prompt();

  // メインループ開始
  g_main_loop_run(loop);

  g_main_loop_unref(loop);
  freeHASH();

  return 0;
}
