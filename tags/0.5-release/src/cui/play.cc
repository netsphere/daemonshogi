/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <si/si.h>
#include <si/ui.h>


static InputCallbackFunc si_input_next[2];


/**
 * ステータスが SI_TORYO だった時の処理。
 * @param bo 対象のBOARD
 */
void play_toryo(BOARD *bo, GAME *gm) {
#ifdef USE_SERIAL
  if (si_input_next[(bo->next == 0)] == si_input_next_serialport ) {
    caSend(&(gm->ca[(bo->next == 0)]), "%TORYO\n");
    caClose(&(gm->ca[(bo->next == 0)]));
  }
#endif /* USE_SERIAL */
#ifdef DEBUG
  printf("sente : %d\n", bo->point[ 0 ]);
  printf("gote  : %d\n", bo->point[ 1 ]);
#endif /* DEBUG */
  printf("%s win.(SI_TORYO)\n", bo->next == GOTE ? "sente" : "gote");
}

/**
 * ステータスが SI_WIN だった時の処理。
 * @param bo 対象のBOARD
 */
void play_win(BOARD *bo, GAME *gm) {
#ifdef DEBUG
  printf("sente : %d\n", bo->point[ 0 ]);
  printf("gote  : %d\n", bo->point[ 1 ]);
#endif /* DEBUG */
  printf("%s win.(SI_WIN)\n", bo->next == SENTE ? "sente" : "gote");
}

/**
 * ステータスが SI_CHUDAN だった時の処理。
 * @param bo 対象のBOARD
 */
void play_chudan(BOARD *bo, GAME *gm) {
  printf("CHUDAN by %s\n", bo->next == SENTE ? "SENTE" : "GOTE");
}

#if 0
/**
 * ステータスが SI_CHUDAN_RECV だった時の処理。
 * @param bo 対象のBOARD
 */
void play_chudan_recv(BOARD *bo, GAME *gm) {
  printf("CHUDAN by %s\n", bo->next == SENTE ? "GOTE" : "SENTE");
}


/**
 * ステータスが SI_NGDATA だった時の処理。
 * @param bo 対象のBOARD
 */
void play_ngdata(BOARD *bo, GAME *gm) {
  printf("NGDATA by %s\n", bo->next == SENTE ? "SENTE" : "GOTE");
}
#endif // 0


/** コマンドモードのヘルプ */
void putHelp() 
{
  printf("# COMMAND\n"
	 "  quit         Exit this program.\n"
	 "  newgame (ai|human) (ai|human)\n"
	 "               Start new game.\n"
	 "  flip         Flip board.\n"
	 "  load file    Load CSA board.\n"
	 "  help         Print this message.\n"
         "\n"
         "# MOVE\n"
	 "  7776FU     move on board.\n"
         "             To promote, set piece name Nari-koma.\n"
         "  0055KA     put piece.\n"
	 "\n"
	 "  - piece name\n"
         "        FU (Fu)          TO (Tokin)\n"
	 "        KY (Kyosha)      NY (Nari-kyo)\n"
	 "        KE (Keima)       NK (Nari-kei)\n"
	 "        GI (Gin)         NG (Nari-gin)\n"
	 "        KI (Kin)\n"
	 "        KA (Kaku)        UM (Uma)\n"
	 "        HI (Hisha)       RY (Ryu)\n"
	 "        OU (Gyoku)\n");
}


extern const char* CSA_PIECE_NAME[];


static void putCmdHelp()
{
  printf("- command\n");
  printf("\n");
  printf("6978 move (6,9) to (7,8)\n");
  printf("34u1 koma uti (6,9) fu\n");
  printf("\n");
  printf("         fu    1 , kyo  2\n");
  printf("         kei   3 , gin  4\n");
  printf("         kin   5 , kaku 5\n");
  printf("         hisya 7\n");
  printf("\n");
  printf("toryo or make : Lost.\n");
  printf("quit   : Exit this program.\n");
  printf("disp   : Change to display mode.\n");
  printf("board  : Display board.\n");
  printf("\n");
}


static void fputBOARD(FILE* out, int mode, const BOARD* bo)
{
  int i, j;
  int c;

#ifdef DEBUG
  assert( out != NULL && bo != NULL );
  assert( mode == 0 || mode == 1 );
#endif /* DEBUG */

  fprintf(out, "/Board\n");
  for (i = 1; i <= 9; i++) {
    for (j = 9; 0<j; j--) {
      c = boGetPiece( bo, (i << 4) + j );
      if (c) {
	if (mode)
	  fprintf(out, "%c%X ", (c & 0x10) ? 'v' : '^', c & 0x0F);
	else
	  fprintf(out, "%02X ", c);
      } else {
	if (mode)
	  fprintf(out, " . ");
	else
	  fprintf(out, "00 ");
      }
    }
    fprintf(out, "\n");
  }
  fprintf(out, "/Piece\n");
  fprintf(out, "%d %d %d %d %d %d %02d\n",
	 bo->inhand[0][7],
	 bo->inhand[0][6],
	 bo->inhand[0][5],
	 bo->inhand[0][4],
	 bo->inhand[0][3],
	 bo->inhand[0][2],
	 bo->inhand[0][1]);
  fprintf(out, "%d %d %d %d %d %d %02d\n",
	 bo->inhand[1][7],
	 bo->inhand[1][6],
	 bo->inhand[1][5],
	 bo->inhand[1][4],
	 bo->inhand[1][3],
	 bo->inhand[1][2],
	 bo->inhand[1][1]);
  fprintf(out, "/Next move\n");
  if (mode)
    fprintf(out, "%s\n", bo->next ? "gote" : "sente");
  else
    fprintf(out, "%d\n", bo->next);
  
#ifdef DEBUG
  printf("key : %08LX\n", bo->key);
#endif /* DEBUG */
  
  return;
}


/**
 * ゲーム用にBOARDを表示する。
 * 消費時間の表示も行う。
 * @param bo 対象のBOARD
 * @param gm 対象のGAME
 */
void putBOARDforGAME(const BOARD* bo, const GAME* gm)
{
  char buf[BUFSIZ];
  extern int flgDisplayMode;

#ifdef DEBUG
  assert( flgDisplayMode == 0 || flgDisplayMode == 1 );
  assert( bo != NULL );
  assert( gm != NULL );
#endif /* DEBUG */
  
  if ( flgDisplayMode )
    fputBOARD(stdout, 0, bo);
  else 
    fputBOARD(stdout, 1, bo);
  
  printf("SENTE TIME - ");
  time2string(buf, bo->total_time[SENTE]);
  printf("%s/", buf);
  time2string(buf, bo->limit_time[SENTE]);
  printf("%s\n", buf);
  
  printf("GOTE TIME  - ");
  time2string(buf, bo->total_time[GOTE]);
  printf("%s/", buf);
  time2string(buf, bo->limit_time[GOTE]);
  printf("%s\n", buf);
}


/**
 * 対局 main loop
 */
void play(BOARD *main_bo, GAME *gm) 
{
#if 0
  char send_buf[ BUFSIZ ];
  DTe te;
  extern int THINK_DEEP_MAX;
  INPUTSTATUS ret;
#ifdef DEBUG
  extern long count_minmax;
#endif /* DEBUG */
    
  printf("Welcome si shogi.\n");
  printf("---\n");
  printBOARD( main_bo );
  printf("\n");

  boPlayInit(main_bo);
  
  si_input_next[SENTE] = gmGetInputNextPlayer(gm, SENTE);
  si_input_next[GOTE]  = gmGetInputNextPlayer(gm, GOTE);

  while ( true ) {
    THINK_DEEP_MAX = gmGetComputerLevel(gm, main_bo->next);
    /* 現在の時間を記録する */
    // boSetNowTime(main_bo);

    int elapsed_time = 0;
    ret = (*si_input_next[main_bo->next])(main_bo, &te, &elapsed_time );

    /* 消費時間を記録する */
    // boSetUsedTime(main_bo);
#ifdef DEBUG
    printf("used_time : %ld\n", main_bo->used_time[main_bo->mi.count]);
#endif /* DEBUG */

    /* ステータスの判定処理 */
    // if ( ret == SI_TORYO) {
    // play_toryo(main_bo, gm);
    // break;
    // } 
    // else if ( ret == SI_WIN ) {
    // play_win(main_bo, gm);
    // break;
    // } 
    // else 
    if ( ret == SI_CHUDAN_LOCAL ) {
      play_chudan(main_bo, gm);
      break;
    } 
    // else if ( ret == SI_CHUDAN_RECV ) {
    //  play_chudan_recv(main_bo, gm);
    //  break;
    // } 
    // else if ( ret == SI_NGDATA ) {
    // play_ngdata(main_bo, gm);
    // break;
    // } 
    else {
      /* 仕様上ありえない分岐 */
      assert( 0 );
    }
    // printTE( &te );
    printf("%s\n", te_str(te, *main_bo).c_str() );

    boMove( main_bo, te );

    putBOARDforGAME( main_bo, gm );
#ifdef DEBUG
    printf("count_minmax : %ld\n", count_minmax);
#endif /* DEBUG */
  }
  
  // CSA_output_auto(main_bo);
#endif // 0
}


static void putBOARD(const BOARD* brd)
{
  extern int flgDisplayMode;

#ifdef DEBUG
  assert( brd != NULL );
#endif /* DEBUG */
  
  if ( flgDisplayMode )
    fputBOARD(stdout, 0, brd);
  else 
    fputBOARD(stdout, 1, brd);
}


/**
 * 画面から文字ベースで次の一手を入力し te に格納する。
 * @param bo 対象のBOARD
 * @param tte TE
 *
 * @return GAMESTAT
 */
INPUTSTATUS si_input_next_human(const BOARD* bo, DTe* tte, int* /*dmy*/ ) 
{
#if 0
  char buf[BUFSIZ];
  TE te;

  while ( true ) {
    printf("$ ");
    fgets(buf, BUFSIZ - 1, stdin);
    if ( buf[0] == '\n' )
      continue;

    if ( strncmp( buf, "matta", 5 ) == 0 ) {
      if ( 0 < bo->mi.count ) {
        boBack( bo );
        boBack( bo );
	putBOARD( bo );
      }
    } 
    else if ( strncmp( buf, "help", 4 ) == 0 ) {
      putCmdHelp();
    } 
    else if ( strncmp( buf, "disp", 4 ) == 0 ) {
      extern int flgDisplayMode;
      flgDisplayMode = (1 - flgDisplayMode);
    } 
    else if ( strncmp( buf, "toryo", 4 ) == 0 ) {
      *tte = DTe(DTe::RESIGN, 0, 0);
      return SI_NORMAL;
    }
      // } else if ( strncmp( buf, "make", 4 ) == 0 ) {
      // return SI_TORYO;
    } 
  else if ( strncmp( buf, "quit", 4 ) == 0 ) {
    return SI_CHUDAN_LOCAL;
  } 
  else if ( strncmp( buf, "board", 5 ) == 0 ) {
      putBOARD(bo);
    } else if ( isdigit(buf[0]) && isdigit(buf[1]) &&
	        isdigit(buf[2]) && isdigit(buf[3]) ) {
      te = te_make_move(
			((buf[1]-'0') << 4) + buf[0]-'0',
			((buf[3]-'0') << 4) + buf[2]-'0',
			( buf[4] == 'n' ) );
      if ( te_is_valid(bo, te ) ) {
	*tte = DTe( DTe::NORMAL_MOVE, te, bo->board[te_to(te)] );
	break;
      } else {
	printf("No good move\n");
      }
    } else if ( isdigit(buf[0]) && isdigit(buf[1]) &&
	        buf[2] == 'u' && isdigit(buf[3]) ) {
      te = te_make_put(
		       ((buf[1]-'0') << 4) + buf[0]-'0',
		       buf[3] - '0' );
      if ( te_is_valid(bo, te ) ) {
	*tte = DTe( DTe::NORMAL_MOVE, te, bo->board[te_to(te)] );
	break;
      } else {
	printf("No good move\n");
      }
    } else {
      printf("Unknown command : %s", buf);
    }
  }
#endif // 0

  return SI_NORMAL;
}
