/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"


const int normal_to_count[] = {
  /* dummy */
  0,

  /* sente */
  1, 0, 2, 5, 6, 0, 0, 8,
  6, 6, 6, 6, 0, 4, 4, 0,

  /* gote */
  1, 0, 2, 5, 6, 0, 0, 8,
  6, 6, 6, 6, 0, 4, 4, 0,
};


/** 長い攻撃でない移動可能先 */
const XyDiff normal_to[][8] = {
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dummy */

  /* sente */
  { -16,   0,   0,   0,   0,   0,   0,   0, }, /* fu */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kyo */
  { -33, -31,   0,   0,   0,   0,   0,   0, }, /* kei */
  { -16, -17,  15,  17, -15,   0,   0,   0, }, /* gin */
  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* kin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kaku */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* hisya */
  { -16, -17,  -1,  15,  16,  17,   1, -15, }, /* oh */

  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* to */
  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* narikyo */
  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* narikei */
  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* narigin */
  {   0,   0,   0,   0,   0,   0,   0,   0, },
  { -16,  -1,  16,   1,   0,   0,   0,   0, }, /* uma */
  { -17,  15,  17, -15,   0,   0,   0,   0, }, /* ryu */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dummy */

  /* gote */
  {  16,   0,   0,   0,   0,   0,   0,   0, }, /* fu */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kyo */
  {  31,  33,   0,   0,   0,   0,   0,   0, }, /* kei */
  { -17,  15,  16,  17, -15,   0,   0,   0, }, /* gin */
  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* kin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kaku */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* hisya */
  { -16, -17,  -1,  15,  16,  17,   1, -15, }, /* oh */

  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* to */
  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* narikyo */
  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* narikei */
  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* narigin */
  {   0,   0,   0,   0,   0,   0,   0,   0, },
  { -16,  -1,  16,   1,   0,   0,   0,   0, }, /* uma */
  { -17,  15,  17, -15,   0,   0,   0,   0, }, /* ryu */
};


#if 0
/**
 * 長い攻撃ではない指し手を生成する。飛・角などの短い攻撃も。
 * @param bo 対象のBOARD
 * @param xy 場所
 * @param piece 駒
 * @param mi 結果を格納するMOVEINFO
 */
static void append_short_onboard_moves(const BOARD* bo, Xy xy, PieceKind piece, 
                                       MOVEINFO* mi)
{
  int i;

  int const next = (piece & 0x10) == 0x10;

  for (i = 0; i < normal_to_count[piece]; i++) {
    Xy const vw = xy + normal_to[piece][i];

#ifdef DEBUG
    assert( -1 <= (vw >> 4) && (vw >> 4) <= 11 );
    assert( 0 <= (vw & 0xF) && (vw & 0xF) <= 10 );
#endif /* DEBUG */

    PieceKind const c = boGetPiece(bo, vw);
    if (c == WALL)
      continue;

    if ( c ) { /* 移動先に駒がある場合 */
      if ( getTeban(c) == next )
	continue;
    }

    miAddWithNari( mi, *bo, xy, vw );
  }
}
#endif // 0


/** remoteOhte() でも利用する */
const int remote_to_count[] = {
  0,
  0, 1, 0, 0, 0, 4, 4, 0,
  0, 0, 0, 0, 0, 4, 4, 0,
  0, 1, 0, 0, 0, 4, 4, 0,
  0, 0, 0, 0, 0, 4, 4, 0,
};

const XyDiff remote_to[][8] = {
  { 0, 0, 0, 0, 0, 0, 0, 0, }, /* dummy */

  /* sente */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* fu */
  { -16,   0,   0,   0,   0,   0,   0,   0, }, /* kyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kei */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* gin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kin */
  {  17,  15, -15, -17,   0,   0,   0,   0, }, /* kaku */
  {   1,  -1, -16,  16,   0,   0,   0,   0, }, /* hisya */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* oh */

  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* to */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narikyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narikei */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narigin */
  {   0,   0,   0,   0,   0,   0,   0,   0, },
  {  17,  15, -15, -17,   0,   0,   0,   0, }, /* uma */
  {   1,  -1, -16,  16,   0,   0,   0,   0, }, /* ryu */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dummy */

  /* gote */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* fu */
  {  16,   0,   0,   0,   0,   0,   0,   0, }, /* kyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kei */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* gin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kin */
  {  17,  15, -15, -17,   0,   0,   0,   0, }, /* kaku */
  {   1,  -1, -16,  16,   0,   0,   0,   0, }, /* hisya */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* oh */

  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* to */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narikyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narikei */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narigin */
  {   0,   0,   0,   0,   0,   0,   0,   0, },
  {  17,  15, -15, -17,   0,   0,   0,   0, }, /* uma */
  {   1,  -1, -16,  16,   0,   0,   0,   0, }, /* ryu */
};


/**
 * 長い攻撃の指し手を生成する
 * @param mi 結果を格納するMOVEINFO
 * @param bo 対象のBOARD
 * @param xy 駒の座標
 * @param next 駒の持ち主
 */
static void movetoLong(MOVEINFO* mi, const BOARD* bo, Xy const xy, int next)
{
  assert( 1 <= (xy & 0xf) && (xy & 0xf) <= 9 );
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( bo->board[xy] != 0 );
  assert( getTeban(bo->board[xy]) == bo->next );

  int i;
  PieceKind piece = boGetPiece(bo, xy);
  // PieceKind p = piece & 0x0F;

  for (i = 0; i < remote_to_count[piece]; i++) {
#ifdef USE_PIN
    // こちらでもpinされていないか確認しておく
    if (bo->pinned[xy]) {
      if (bo->pinned[xy] != remote_to[piece][i] &&
           bo->pinned[xy] != -remote_to[piece][i])
        continue;
    }
#endif

    bool flag = false;
    Xy vw = xy;
    while (1) {
      vw += remote_to[piece][i];

      PieceKind const c = boGetPiece(bo, vw);
      if (c == WALL)
        break;

      if (c) {
        // 自分の駒があるとダメ
	if ( getTeban(c) == next )
	  break;
        flag = true;
      }

      // te.to = vw;
      miAddWithNari( mi, *bo, xy, vw );

      if (flag)
        break;
    }
  }
}


/**
 * xy にある駒の動ける場所 (指し手) を探してmiに追加する。
 * @param bo 対象のBOARD
 * @param xy 駒の場所
 * @param mi 結果を格納するMOVEINFO
 */
void mi_append_moveto(const BOARD* bo, Xy xy, MOVEINFO* mi)
{
  assert( 1 <= (xy & 0x0f) && (xy & 0x0f) <= 9 );
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( bo->board[xy] != 0 );
  assert( getTeban(bo->board[xy]) == bo->next );

  PieceKind const piece = bo->board[ xy ];
#if 0
  if ( (piece & 0xf) == OH )
    mi_append_king_moves( bo, mi, xy, bo->next );
  else
    append_short_onboard_moves( bo, xy, piece, mi );

  if ( is_long_piece(piece) )
    movetoLong(mi, bo, xy, bo->next);
#else
  // 展開してみる   2009.12 多少だが効果あり
  switch (piece) {
  case FU:
    miAddWithNari( mi, *bo, xy, xy - 16 );
    break;
  case KYO: case KYO + 0x10:
  case KAKU: case KAKU + 0x10:
  case HISHA: case HISHA + 0x10:
    movetoLong( mi, bo, xy, bo->next );
    break;
  case KEI:
    miAddWithNari( mi, *bo, xy, xy - 33 );
    miAddWithNari( mi, *bo, xy, xy - 31 );
    break;
  case GIN:
    miAddWithNari( mi, *bo, xy, xy - 16 );
    miAddWithNari( mi, *bo, xy, xy - 17 );
    miAddWithNari( mi, *bo, xy, xy + 15 );
    miAddWithNari( mi, *bo, xy, xy + 17 );
    miAddWithNari( mi, *bo, xy, xy - 15 );
    break;
  case KIN:
  case TOKIN: case NARIKYO: case NARIKEI: case NARIGIN:
    miAddWithNari( mi, *bo, xy, xy - 16 );
    miAddWithNari( mi, *bo, xy, xy - 17 );
    miAddWithNari( mi, *bo, xy, xy - 1 );
    miAddWithNari( mi, *bo, xy, xy + 16 );
    miAddWithNari( mi, *bo, xy, xy + 1 );
    miAddWithNari( mi, *bo, xy, xy - 15 );
    break;
  case OH:
  case OH + 0x10:
    mi_append_king_moves( bo, mi, xy, bo->next );
    break;
  case UMA:
  case UMA + 0x10:
    miAddWithNari( mi, *bo, xy, xy - 16 );
    miAddWithNari( mi, *bo, xy, xy - 1 );
    miAddWithNari( mi, *bo, xy, xy + 16 );
    miAddWithNari( mi, *bo, xy, xy + 1 );
    movetoLong( mi, bo, xy, bo->next );
    break;
  case RYU:
  case RYU + 0x10:
    miAddWithNari( mi, *bo, xy, xy - 17 );
    miAddWithNari( mi, *bo, xy, xy + 15 );
    miAddWithNari( mi, *bo, xy, xy + 17 );
    miAddWithNari( mi, *bo, xy, xy - 15 );
    movetoLong( mi, bo, xy, bo->next );
    break;
  case FU + 0x10:
    miAddWithNari( mi, *bo, xy, xy + 16 );
    break;
  case KEI + 0x10:
    miAddWithNari( mi, *bo, xy, xy + 33 );
    miAddWithNari( mi, *bo, xy, xy + 31 );
    break;
  case GIN + 0x10:
    miAddWithNari( mi, *bo, xy, xy + 16 );
    miAddWithNari( mi, *bo, xy, xy + 17 );
    miAddWithNari( mi, *bo, xy, xy - 15 );
    miAddWithNari( mi, *bo, xy, xy - 17 );
    miAddWithNari( mi, *bo, xy, xy + 15 );
    break;
  case KIN + 0x10:
  case TOKIN + 0x10: case NARIKYO + 0x10: case NARIKEI + 0x10: case NARIGIN + 0x10:
    miAddWithNari( mi, *bo, xy, xy + 16 );
    miAddWithNari( mi, *bo, xy, xy + 17 );
    miAddWithNari( mi, *bo, xy, xy + 1 );
    miAddWithNari( mi, *bo, xy, xy - 16 );
    miAddWithNari( mi, *bo, xy, xy - 1 );
    miAddWithNari( mi, *bo, xy, xy + 15 );
    break;
  default:
    abort();
  }
#endif
}


/**
 * 指し手が打ち歩詰めになるか
 * @param bo 歩を打つ前の局面
 * @param te 歩を打つ手
 * @return 打ち歩詰めのときtrue
 */
static bool te_is_uchifuzume(const BOARD* bo, const TE& te)
{
  int defense;
  Xy defense_king;
  int i;

  if (bo->next) {
    // 後手が攻め
    defense = 0;
    defense_king = te_to(te) + 16;
  }
  else {
    // 先手が攻め
    defense = 1;
    defense_king = te_to(te) - 16;
  }

  // 歩に攻め方の利きが付いてなければ玉で取れる
  if ( bo_attack_count(bo, 1 - defense, te_to(te)) == 0 )
    return false;

  // 受け玉の周りに攻撃側の利きのないところがあればok
  for (i = 0; i < 8; i++) {
    Xy sq = defense_king + normal_to[ OH + (defense << 4) ][i];
    if (bo->board[sq] == WALL)
      continue;

    if ( (!bo->board[sq] || getTeban(bo->board[sq]) != defense) && 
         bo_attack_count(bo, 1 - defense, sq) == 0 )
      return false;
  }

  // 受け側の駒で取れるか (短いの)
  for (i = 0; i < bo->short_attack[defense][te_to(te)].count; i++) {
    Xy defense_from = bo->short_attack[defense][te_to(te)].from[i];
    if ( (bo->board[defense_from] & 0xf) == OH )
      continue;
#ifdef USE_PIN
    // pinされているときは決して取れない
    if (!bo->pinned[defense_from])
      return false;
#else
    // これは厳密ではない。
    if (bo->long_attack[1 - defense][defense_from].count == 0)
      return false;
#endif // USE_PIN
  }

  // 受け側の駒で取れるか (長いの)
  for (i = 0; i < bo->long_attack[defense][te_to(te)].count; i++) {
    Xy defense_from = bo->long_attack[defense][te_to(te)].from[i];

#ifdef USE_PIN
    // pinされているときは決して取れない
    if (!bo->pinned[defense_from])
      return false;
#else
    // これは厳密ではない。
    if (bo->long_attack[1 - defense][defense_from].count == 0)
      return false;
#endif // USE_PIN
  }

#ifdef USE_PIN
  // 歩で遮られることはない -> 受けなし
  if ( !bo->long_attack[1 - defense][te_to(te) - 1].count &&
       !bo->long_attack[1 - defense][te_to(te) + 1].count )
    return true;
#endif

  // 歩を打つことで攻め方の長い攻撃が遮られることがある
  // 実際に動かしてみる
  BOARD* tmp_bo = bo_dup(bo);
  boMove_mate( tmp_bo, te );

  MOVEINFO tmp_mi;
  make_moves(tmp_bo, &tmp_mi);
  bool ret = tmp_mi.count == 0;  // 詰み

  freeBOARD(tmp_bo);

  return ret;
}


/**
 * ルール上正しい指し手かチェックする。
 * canvas.cc から呼び出される. この関数は重い
 *
 * @param bo 対象のBOARD
 * @param te 調べる一手
 * @return 正しければ true を返す。正しくなければ false を返す。
 */
bool te_is_valid( const BOARD* bo, const TE& te)
{
  MOVEINFO mi;

  make_moves(bo, &mi);  // 打ち歩詰めが含まれる
  if ( !mi_includes(&mi, te) )
    return false;

  if ( te_is_put(te) && te_pie(te) == FU ) {
    if ( (bo->next == SENTE && te_to(te) == bo->king_xy[GOTE] + 16) ||
	 (bo->next == GOTE && te_to(te) == bo->king_xy[SENTE] - 16) ) {
      if ( te_is_uchifuzume(bo, te) )
	return false;
    }
  }

  return true;
}


/**
 * ある駒を打つ手を生成.
 * utiOhte() から呼び出される. 打ち歩詰めになったりするときは追加しない。
 * @param bo 局面
 * @param mi 追加するMOVEINFO
 * @param pie 駒種
 * @param xy 座標. 0x11-0x99まで
 */
void append_a_put_move( const BOARD* bo, MOVEINFO* mi, PieceKind pie, Xy xy )
{
  TE te = te_make_put(xy, pie);

  if (bo->next == 0) {
    // 先手
    switch (pie) {
    case FU:
      if (checkFuSente(bo, xy & 0xf) || xy <= 0x19)
        return;
#if 0
      // 打ち歩詰め？
      if (bo->board[xy - 16] == (OH + 0x10)) {
        if (is_uchifuzume(bo, te))
          return;
      }
#endif // 0
      break;
    case KYO:
      if (xy <= 0x19)
        return;
      break;
    case KEI:
      if (xy <= 0x29)
        return;
      break;
    default:
      break;
    }
  }
  else {
    // 後手
    switch (pie) {
    case FU:
      if (checkFuGote(bo, xy & 0xf) || xy >= 0x91)
        return;
#if 0
      if (bo->board[xy + 16] == OH) {
        if (is_uchifuzume(bo, te))
          return;
      }
#endif // 0
      break;
    case KYO:
      if (xy >= 0x91)
        return;
      break;
    case KEI:
      if (xy >= 0x81)
        return;
      break;
    default:
      break;
    }
  }

  miAdd(mi, te);
}


/**
 * ある場所に打つ手をすべて生成
 * 2009.11 ループの内部でこの関数を呼び出すと遅い. 
 */
void mi_append_put_moves(const BOARD* bo, Xy xy, MOVEINFO* mi)
{
  assert( xy >= 0x11 && xy <= 0x99 );
  assert( !bo->board[xy] );

  int i;
  for (i = 1; i <= 7; i++) {
    if ( !bo->inhand[bo->next][i] )
      continue;

    append_a_put_move(bo, mi, i, xy);
  }
}


/**
 * 局面で着手可能な指し手を生成する
 * 打ち歩詰めの手も生成する
 *
 * @param bo 局面
 * @param mi 手一覧
 */
void make_moves(const BOARD* bo, MOVEINFO* mi)
{
  int i;
  PieceKind inhand_pie[8];

  // 王手を掛けられているか
  if ( bo_is_checked(bo, bo->next) ) {
    uke( bo, mi );
    return;
  }

  mi->count = 0;

  // あらかじめ持ち駒セットを作る. 2009.11 効果あり
  int p = 0;
  for (i = 1; i <= 7; i++) {
    if ( bo->inhand[bo->next][i] > 0 )
      inhand_pie[p++] = i;
  }
  inhand_pie[p] = 0;

  for (i = 1; i <= 81; i++) {
    if ( bo->board[arr_xy[i]] ) {
      // xyから移動する手
      if ( getTeban(bo->board[arr_xy[i]]) == bo->next ) 
	mi_append_moveto(bo, arr_xy[i], mi);
    }
    else {
      // 打つ手
      for (p = 0; inhand_pie[p]; p++)
	append_a_put_move( bo, mi, inhand_pie[p], arr_xy[i] );
    }
  }
}
