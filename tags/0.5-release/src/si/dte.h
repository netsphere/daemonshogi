/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _DTE_H_
#define _DTE_H_

#include <stdint.h> // int16_t
#include <string>
#include "si-base.h"


struct BOARD;

/**
 * 指し手.
 */
struct DTe
{
  enum Special {
    /** (合法な) 指し手 */
    NORMAL_MOVE = 0,

    /** 投了 */
    RESIGN = 2,

    /** 中断. 棋譜ツリーには保存しない */
    CHUDAN = 4,

    /** 入玉し, 24点法/27点法での勝ち */
    KACHI = 6,  

    /** 持将棋. 合意による引き分け. 24点法で両者24点以上 */
    JISHOGI = 7,

    /** 不正な手 */
    ILLEGAL = 8,

    /** 時間切れ */
    TIME_OUT = 9,

    /** 千日手で引き分け */
    SENNICHITE = 10,
  };

  /** 移動元の場所. 符号あり */
  Xy fm;

  /** 移動先の場所 */
  Xy to;

  /** 成り (promote) ならば true, それ以外は false をセットする */
  bool nari;

  /** 駒打ち (put) ならば打ち込む駒種、それ以外は 0 をセットする */
  PieceKind uti;

  /** 取った駒. undoに必要. */
  PieceKind tori;

  /** 投了など */
  Special special;

  DTe() { clear(); }

  DTe(Special special_, const TE& te, PieceKind tori_): 
    fm(te_from(te)), to(te_to(te)), nari(te_promote(te)), uti(te_pie(te)),
    tori(tori_), special(special_)
  {    
  }

  void clear() {
    fm = 0; to = 0; nari = false; uti = 0; tori = 0; 
    special = NORMAL_MOVE;
  }

  bool operator == (const DTe& x) const {
    if (special != NORMAL_MOVE || x.special != NORMAL_MOVE)
      return special == x.special;
    else {
      return to == x.to && fm == x.fm && nari == x.nari && uti == x.uti &&
             tori == x.tori;
    }
  }

  bool operator != (const DTe& x) const {
    return !(*this == x);
  }

  TE pack() const {
#ifdef USE_TE32
    return (((uint16_t) fm) << 24) + (((uint16_t) to) << 16) + 
      (nari ? 0x8000 : 0) + (uti << 10);
#else
    TE r;
    r.fm = fm; r.to = to; r.nari = nari; r.uti = uti; r.hint = 0;
    return r;
#endif
  }

  std::string to_str( const BOARD& bo );
};


/*********************************************************/
/* function prototypes */
/*********************************************************/

DTe* daemon_dte_new   (void);
void daemon_dte_free  (DTe* te);
void daemon_dte_output(const DTe* te, FILE* out);

#endif /* _DTE_H_ */
