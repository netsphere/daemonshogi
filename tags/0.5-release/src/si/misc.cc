/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdarg.h>
#include "si.h"
#include <string>
#include <unistd.h>
using namespace std;


/**
 * 致命的エラーが発生したときに呼ばれる(動作に必要なメモリが確保でき
 * なかった等)。
 * @param fmt エラー内容を表す文字列
 */
void si_abort(const char* fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  // printf("si_abort: ");
  vprintf(fmt, ap);
  va_end(ap);

  abort();
}


/**
 * buf に時間を表す文字列を格納する。
 * 00:00:00 の形式で表示する。内容は 時:分:秒。
 * ただし t に100 時間以上の時間が格納されている場合 99 時間として表示する。
 * buf には BUFSIZ 以上のメモリが確保されれていることを想定している。
 * @param buf 格納先のポインタ
 * @param t 表示する時間(秒)
 */
void time2string(char *buf, time_t t) {
  int hour;
  int minute;
  int second;

#ifdef DEBUG
  assert( buf != NULL );
#endif /* DEBUG */
  
  hour = t / 3600;
  if ( 99 < hour ) {
    hour = 99;
  }

  minute = (t % 3600) / 60;
  if ( 59 < minute ) {
    minute = 59;
  }
  
  second = t % 60;

  sprintf(buf, "%02d:%02d:%02d", hour, minute, second);
}


/**
 * 先手の駒ではない場合 1 を返す。
 * 
 * @param p 駒種類
 * @return 
 */
int notSentePiece(int p) {
#ifdef DEBUG
  assert( p != WALL );
#endif /* DEBUG */

  if ( p == 0 )
    return 1;
  
  if ( (p & 0x10) == 0x10 )
    return 1;
  
  return 0;
}

/**
 * 後手の駒ではない場合 1 を返す。
 * 
 * @param p 駒種類
 * @return 
 */
int notGotePiece( int p ) {
#ifdef DEBUG
  assert( p != WALL );
#endif /* DEBUG */

  if ( p == 0 )
    return 1;
  
  if ( (p & 0x10) == 0x10 )
    return 0;
  
  return 1;
}


/**
 * サーバから読み込んでバッファに貯める
 * @return エラーまたは接続が切れたとき 0
 */
int RecvBuffer::read_from_server( int server_fd )
{
  char buf[1000];
  int r = ::read( server_fd, buf, sizeof(buf) - 1 );
  if (r < 0) {
    // エラー発生
    printf("%s: socket recv error.\n", __func__); // DEBUG
    return 0;
  }
  else if (r == 0) // 接続先が閉じた
    return 0;

  buf[r] = '\0';
  sock_buffer += buf;

  // 行を切り出す. 末尾の改行は削除する
  string::size_type idx;
  while ((idx = sock_buffer.find("\n", 0)) != string::npos) {
    string line = string(sock_buffer, 0, idx);
    printf("recv: %s\n", line.c_str()); // DEBUG
    recv_list.push_back(line);
    sock_buffer.erase(0, idx + 1);
  }

  return 1;
}
