/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <list>
#include "dte.h"
#include "movetree.h"
#include "board-misc.h"
#include "record.h"
#include "filereader.h"
#include "filewriter.h"
// #include "misc.h"
using namespace std;


static const char* csa_koma_str[] = {
  "",   "FU", "KY", "KE", "GI", "KI", "KA", "HI",
  "OU", "TO", "NY", "NK", "NG", "",   "UM", "RY",
};

static void csa_output_seq_sub(DBoard* bo, const KyokumenNode* node,
			      int te_count,
			      FileWriter& writer)
{
  char buf[80];
  int i;

  for (i = 0; i < node->moves.size(); i++) {
    if ( i != 0 ) {
      sprintf(buf, "'手数目:%d\n", te_count + 1);
      writer << buf;
    }

    const MoveEdge mv = node->moves[i];
    DTe te = mv.te;

    if (te.special) {
      // 投了など
      switch (te.special) {
      case DTe::RESIGN:
	sprintf(buf, "%s\n", "%TORYO");
	break;
      case DTe::KACHI:
	sprintf(buf, "%s\n", "%KACHI");
	break;
      case DTe::CHUDAN:
	sprintf(buf, "%s\n", "%CHUDAN");
	break;
      case DTe::JISHOGI:
	sprintf(buf, "%s\n", "%JISHOGI");
	break;
      case DTe::ILLEGAL:
	sprintf(buf, "%s\n", "%ILLEGAL_MOVE");
	break;
      case DTe::TIME_OUT:
	sprintf(buf, "%s\n", "%TIME_UP");
	break;
      case DTe::SENNICHITE:
	break; // 特殊
      default:
	abort(); // ない
      }

      if (te.special != DTe::SENNICHITE) {
	writer << buf;
	continue;
      }
    }

    // 通常の指し手
    if ( !te.fm ) {
      /* 駒打ち */
      sprintf(buf, "%c%d%d%d%d%s\n",
	      (bo->next == SENTE) ? '+' : '-',
	      0, 0,
	      te.to & 0xF, te.to >> 4,
	      csa_koma_str[te.uti]);
    } 
    else {
      /* 駒移動 */
      PieceKind p = daemon_dboard_get_board(bo, te.fm & 0xF, te.fm >> 4);
      p += te.nari ? 0x08 : 0;
      sprintf(buf, "%c%d%d%d%d%s\n",
	      (bo->next == SENTE) ? '+' : '-',
	      te.fm & 0xF, te.fm >> 4,
	      te.to & 0xF, te.to >> 4,
	      csa_koma_str[p & 0xF]);
    }
    writer << buf;
    sprintf(buf, "T%d\n", mv.time);
    writer << buf;
    
    if ( !te.special ) {
      boMove_mate(bo, te.pack() );
      csa_output_seq_sub(bo, node->lookup_child(te), te_count + 1, writer);
      boBack_mate(bo);
    }
    else {
      assert( te.special == DTe::SENNICHITE );
      writer << "%SENNICHITE\n";
    }
  } // of for
}


/**
 * 棋譜 (手順木) を出力する.
 */
static void daemon_record_output_csa_te(const Record* record, 
					FileWriter* writer) 
{
  // DBoard* bo = daemon_dboard_new();
  // daemon_dboard_copy(&(record->first_board), bo);
  BOARD* bo = bo_dup( &record->first_board );

  daemon_filewriter_put(writer, "'  指し手\n");
  csa_output_seq_sub(bo, record->mi, 0, *writer);

  // daemon_dboard_free(bo);
  freeBOARD( bo );
}


/** CSA形式でファイル filename に出力する。 */
int daemon_record_output_csa(Record* record, FileWriter* writer )
{
  return daemon_record_output_csa_code(record, writer, "CP932");
}


/** 持ち駒出力 */
static void daemon_record_output_csa_piece(const Record* record, 
					   FileWriter* writer) 
{
  const DBoard* board;
  int p, j, i;
  char buf[BUFSIZ];

  board = &(record->first_board);

  daemon_filewriter_put(writer, "'  先手持駒\n");
  for (j=7; 1 <= j; j--) {
    p = daemon_dboard_get_piece(board, SENTE, j);
    for (i=0; i<p; i++) {
      sprintf(buf, "P+00%s\n", csa_koma_str[j]);
      daemon_filewriter_put(writer, buf);
    }    
  }

  daemon_filewriter_put(writer, "'  後手持駒\n");
  for (j=7; 1 <= j; j--) {
    p = daemon_dboard_get_piece(board, GOTE, j);
    for (i=0; i<p; i++) {
      sprintf(buf, "P-00%s\n", csa_koma_str[j]);
      daemon_filewriter_put(writer, buf);
    }
  }
}


static void daemon_record_output_csa_board(const Record* record, 
					   FileWriter* writer) 
{
  const DBoard* bo;
  int p;
  int x, y;
  char buf[BUFSIZ];

  bo = &(record->first_board);

  for (y=1; y <= 9; y++) {
    sprintf(buf, "P%d", y);
    daemon_filewriter_put(writer, buf);
    for (x=9; 1 <= x; x--) {
      p = daemon_dboard_get_board(bo, x, y);
      if (p) {
	sprintf(buf, "%c%s", (p & 0x10) ? '-' : '+', csa_koma_str[p & 0x0F]);
	daemon_filewriter_put(writer, buf);
      } else {
	daemon_filewriter_put(writer, " * ");
      }
    }
    daemon_filewriter_put(writer, "\n");
  }
}


/** CSA形式でファイル filename に出力する。 */
static int daemon_record_output_csa_p(const Record* record, FileWriter& writer) 
{
  assert(record != NULL);

  writer << "V2.2\n";

  daemon_filewriter_put(&writer, "'  対局者名\n");
  writer << "N+" << 
    (record->player_name[0] != "" ? record->player_name[0] : "") << "\n";

  writer << "N-" <<
    (record->player_name[1] != "" ? record->player_name[1] : "") << "\n";

  daemon_filewriter_put(&writer, "'  盤面\n");
  daemon_record_output_csa_board(record, &writer);
  daemon_record_output_csa_piece(record, &writer);
  daemon_filewriter_put(&writer, "'  手番\n");
  daemon_filewriter_put(&writer, record->first_board.next ? "-\n" : "+\n");
  daemon_record_output_csa_te(record, &writer);

  return 0;
}


/**
 * CSA形式でファイル filename に出力する。
 * @param record 棋譜
 * @param writer 出力ストリーム
 * @param code 文字コード
 */
int daemon_record_output_csa_code(Record* record, 
				  FileWriter* writer,
				  const char* code) 
{
  assert(record != NULL);
  assert( writer != NULL);
  assert(code);

  int r;
  
  // writer = daemon_filewriter_new();
  daemon_filewriter_set_outcode(writer, code);
/*
  r = daemon_filewriter_open(writer, filename);
  if (r == -1) 
    return -1;
*/
  if (string(code) == "CP932")
    daemon_filewriter_put(writer, "' -*- coding:shift_jis -*-\n");

  r = daemon_record_output_csa_p(record, *writer);
  // daemon_filewriter_close(writer);

  return r;
}


/**
 * 初期盤面を読み込んで record->first_board を更新する. 次のようなフォーマット
<pre>
P1-KY-KE-GI-KI-OU-KI-GI-KE-KY
P2 * -HI *  *  *  *  * -KA * 
P3-FU-FU-FU-FU-FU-FU-FU-FU-FU
P4 *  *  *  *  *  *  *  *  * 
P5 *  *  *  *  *  *  *  *  * 
P6 *  *  *  *  *  *  *  *  * 
P7+FU+FU+FU+FU+FU+FU+FU+FU+FU
P8 * +KA *  *  *  *  * +HI * 
P9+KY+KE+GI+KI+OU+KI+GI+KE+KY
P+00GI00GI
P-00FU00FU00FU00FU00KA       # 00 はセパレータ。個数ではない。
+                            # 手番
</pre>
 */
static void daemon_record_load_csa_board(Record* record, FileReader* reader) 
{
  const char* s;

  BOARD* bo = newBOARDwithInit();

  while ((s = daemon_filereader_skip_comment(reader, '\'')) != NULL) {
    int error = 0;
    bo_update_board_by_csa_line(bo, s, &error);

    switch (error) {
    case 1:
      record->set_error("inhand separator error");
      goto ex;
    case 2:
      record->set_error("inhand pieces too much");
      goto ex;
    case 3:
      record->set_error("piece side error");
      goto ex;
    case 4:
      record->set_error("piece kind error");
      goto ex;
    case 5:
      record->set_error("unknown line type");
      goto ex;
    default:
      break;
    }

    if (*s == '+' || *s == '-')
      break;
  }

  record->first_board = *bo;
  boSetToBOARD( &record->first_board );
ex:
  freeBOARD(bo);
}


/** 
 * 経過時間 (opt.) を読み込む
 * 古い形式では次の行、新しい形式 (V2?) では同じ行
 */
static int csa_parse_time(FileReader* reader, const char* s)
{
  int tim = 0;
  if (*s == ',') {
    s++;
    sscanf(s, "T%d", &tim);
  }
  else { 
    s = daemon_filereader_skip_comment(reader, '#');
    if (s == NULL) { // ファイル末尾
/*
      daemon_dboard_free(board);
      record->loadstat = D_LOAD_SUCCESS;
*/
      return 0;
    }
    if (*s != 'T')
      daemon_filereader_push(reader, s);
    else
      sscanf(s, "T%d", &tim);
  }

  return tim;
}


/** 棋譜を読み込む */
static void daemon_record_load_csa_te(Record* record, FileReader* reader) 
{
  int p, p2;
  const char* s;

  static const char* tesume = "'手数目:";

  printf("%s: start.\n", __func__); // DEBUG

  BOARD* board = bo_dup( &record->first_board );
  printBOARD( board ); // DEBUG

  KyokumenNode* current = record->mi;
  int pos = 0;

  while ((s = daemon_filereader_skip_comment(reader, '#')) != NULL) {
    DTe te;
    int tim = 0;

    // printf("read: '%s'\n", s); // DEBUG
    switch (*s) {
    case '+':
    case '-':
      /* 指す手 */
      te.fm = ((s[2] - 0x30) << 4) + s[1] - 0x30;
      te.to = ((s[4] - 0x30) << 4) + s[3] - 0x30;
      s += 5;
      for (p=1; p<=0xF; p++) {
	if (strncmp(s, csa_koma_str[p], 2) == 0)
	  break;
      }
      if (0xF < p) {
	/* 仕様上ありえない */
	record->set_error("piece kind error");
	// daemon_dboard_free(board);
	freeBOARD( board );
	return;
      }
      if (te.fm == 0) { // 駒打ちの場合
	if (te.to < 0x11 || 0x99 < te.to) {
	  /* 仕様上ありえない */
	  record->set_error("put sq error");
	  // daemon_dboard_free(board);
	  freeBOARD( board );
	  return;
	}
	te.uti = p;
	te.nari = 0;
	te.tori = 0;
	te.special = DTe::NORMAL_MOVE;
      } 
      else { // 駒移動
	te.uti = 0;
	te.special = DTe::NORMAL_MOVE;

	if (te.fm < 0x11 || 0x99 < te.fm ||
	    te.to < 0x11 || 0x99 < te.to) {
	  /* 仕様上ありえない */
	  record->set_error("move sq error");
	  // daemon_dboard_free(board);
	  freeBOARD( board );
	  return;
	}
	if (p == 0x05 || p == 0x8 || p == 0x15 || p == 0x18) {
	  /* 金、王の場合は成りはない */
	  te.nari = 0;
	} 
	else {
	  /* 金、王以外で移動元が不成り、移動先が成りなら、成りとする */
	  p2 = daemon_dboard_get_board(board, te.fm & 0xF, te.fm >> 4);
	  if (0x9 <= p && (p2 & 0xF) < 0x9) {
	    te.nari = 1;
	  } else {
	    te.nari = 0;
	  }
	}
	te.tori = board->board[te.to];
      }

      s += 2;
      tim = csa_parse_time(reader, s);

      // 手を追加
      current = current->add_child(te, tim, "")->node;
      pos++;

      // daemon_dboard_move(board, &te);
      boMove_mate(board, te.pack() );
      break;
    case '%':
      te.fm = 0; te.to = 0; te.uti = 0; te.nari = 0; te.tori = 0;

      if (strncmp(s, "%TORYO", 6) == 0) {
	te.special = DTe::RESIGN;
	s += 6;
	tim = csa_parse_time(reader, s);
      }
      else if (strncmp(s, "%KACHI", 6) == 0) 
	te.special = DTe::KACHI;
      else if (strncmp(s, "%CHUDAN", 7) == 0)
	te.special = DTe::CHUDAN;
      else {
	assert(0); // TODO:不正な手、千日手の場合は？
#if 0
      else if (strncmp(s, "%MATTA", 6) == 0) {
	record->matchstat = D_MATTA;
      } 
      else if (strncmp(s, "%SENNICHITE", 11) == 0) {
	record->matchstat = D_SENNICHITE;
      } 
      else if (strncmp(s, "%JISHOGI", 8) == 0) {
	record->matchstat = D_JISHOGI;
      } 
      else if (strncmp(s, "%TSUMI", 6) == 0) {
	record->matchstat = D_TSUMI;
      } 
      else if (strncmp(s, "%FUZUMI", 7) == 0) {
	record->matchstat = D_FUZUMI;
      } else if (strncmp(s, "%ERROR", 6) == 0) {
	record->matchstat = D_ERROR;
      } 
#endif
      }
      current = current->add_child(te, tim, "")->node;
      pos++;
      break;
    case '\'':
      // コメント or 分岐
      if (!string(s).find(tesume)) {
	int to_back = -1;
	sscanf(s + strlen(tesume), "%d", &to_back);
	
	// 巻き戻す 
	while (pos >= to_back) {
	  // daemon_dboard_back(board, current->move_to_back);
	  boBack_mate(board);
	  current = current->prev;
	  pos--;
	}
      }
      break;
    case '\0':
      // 空行
      break;
    default:
      /* ありえない分岐 */
      assert(0);
    }
  }

  record->loadstat = D_LOAD_SUCCESS;
  freeBOARD( board );
}


/** 
 * CSA形式のファイルを読み込んで Record を生成して返す。
 */
static void daemon_record_load_csa_p(Record* record, FileReader* reader) 
{
  char* s;

  assert(record != NULL);
  assert(reader != NULL);

  bo_reset_to_hirate( &record->first_board );

  // 初期局面
  while ((s = daemon_filereader_skip_comment(reader, '\'')) != NULL) {
    if (strncmp(s, "V2", 2) == 0) {
      // バージョン番号
    }
    else if (strncmp(s, "N+", 2) == 0) {
      /* 先手対局者名 */
      record->player_name[0] = s + 2;
    }
    else if (strncmp(s, "N-", 2) == 0) {
      /* 後手対局者名 */
      record->player_name[1] = s + 2;
    }
    else if (strncmp(s, "$EVENT:", 7) == 0) {
      // ゲーム名
    }
    else if (strncmp(s, "$START_TIME:", 12) == 0) {
      // 開始実時間
    }
    // else if (strncmp(s, "PI", 2) == 0) {
    //   /* 初期配置 */
    // }
    else if (strncmp(s, "P1", 2) == 0) {
      // 初期配置の盤面. first_boardを更新する
      daemon_filereader_push(reader, s);
      daemon_record_load_csa_board(record, reader);
      break;
    }
    else {
      assert(0); // DEBUG
    }
  }

  // 棋譜
  daemon_record_load_csa_te(record, reader);

#ifndef NDEBUG
  KyokumenNode* p = record->mi;
  while (p && p->moves.size() > 0) {
    daemon_dte_output(&p->moves.begin()->te, stdout);
    p = p->moves.begin()->node;
  }
#endif // NDEBUG
}


/** CSA形式のファイルを読み込む */
Record* daemon_record_load_csa( const char* filename )
{
  return daemon_record_load_csa_code( filename, "CP932" );
}


/**
 * CSA形式のファイルを読み込む. 文字コードを指定できる。
 * @param filename ファイル名
 * @param encoding 文字コード
 * @return 生成したrecord. エラーの場合でも生成して返す
 */
Record* daemon_record_load_csa_code( const char* filename, 
                                     const char* encoding )
{
  assert(filename != NULL);
  assert(encoding != NULL);

  FileReader* reader = daemon_filereader_new();
  daemon_filereader_set_incode(reader, encoding);
  daemon_filereader_open(reader, filename);

  Record* record = daemon_record_new();

  if (daemon_filereader_get_stat(reader) == D_FILEREADER_STAT_ERROR) {
    /* 読み込みに失敗した */
    daemon_filereader_free(reader);
    record->set_error("failed to open file");
    return record;
  }

  daemon_record_load_csa_p(record, reader);

  daemon_filereader_close(reader);
  daemon_filereader_free(reader);

  if (record->loadstat == D_LOAD_SUCCESS) {
    // 成功
    record->filename = filename;
    record->filetype = Record::D_FORMAT_CSA;
    record->loadstat = D_LOAD_SUCCESS;
    record->changed = false;
  }
  return record;
}
