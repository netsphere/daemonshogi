/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <string>
#include <assert.h>
#include "si.h"
#include "ui.h"
using namespace std;


#define READ_FILE_ERROR (1)

extern const char* CSA_PIECE_NAME[];


/**
 * 読み込んだ1行から局面を更新する。
 * @param bo 局面
 * @param s 1行
 * @param error エラーがあったときにエラーを知らせる。NULLでもよい。
 */
void bo_update_board_by_csa_line(BOARD* bo, const char* s, int* error)
{
  assert(s);

  int side;
  int i, y;

  if (*s == '+') {
    bo->next = SENTE;
    return;
  }
  else if (*s == '-') {
    bo->next = GOTE;
    return;
  }

  s++;
  if (*s == '+' || *s == '-') {
      /* 持ち駒 */
      side = *s == '+' ? 0 : 1;
      s++;

      while (*s != '\0') {
	if (*s != '0' || *(s + 1) != '0') {
	  /* 仕様上ありえない */
          if (error)
            *error = 1; // record->set_error("mochigoma separator error");
	  return;
	}
	s += 2;
	for (i = 1; i <= 7; i++) {
          if (strncmp(s, CSA_PIECE_NAME[i], 2) == 0)
	    break;
	}
	if (i > 7) {
	  /* 仕様上ありえない */
          if (error)
            *error = 2; // record->set_error("mochigoma too much");
	  return;
	}
        bo->inhand[side][i]++;
	s += 2;
      }
    }
    else if (*s >= '1' && *s <= '9') {
      // 盤面
#if 0
      if (y != *s - 0x30) {
	/* 仕様上ありえない */
        if (record)
          record->set_error("board lineno error");
	return;
      }
#endif // 0
      y = *s - 0x30;
      s++;

      for (int x = 9; x >= 1; x--) {
	if (*s == ' ') {
          bo->board[(y << 4) + x] = 0;
	  s += 3;
	  continue;
	}
	else if (*s == '+')
	  side = 0; // 先手
	else if (*s == '-')
	  side = 1; // 後手
	else {
          if (error)
            *error = 3; // record->set_error("piece side error");
	  return;
	}
	s++;

	for (i = 1; i <= 15; i++) {
          if (strncmp(s, CSA_PIECE_NAME[i], 2) == 0)
	    break;
	}
	if (i > 15) {
	  /* 仕様上ありえない */
          if (error)
            *error = 4; // record->set_error("piece kind error");
	  return;
	}
        bo->board[(y << 4) + x] = i + side * 0x10;
	s += 2;
      }
      // y++;
    }
    else {
      // 持ち駒でも盤面でもない
      if (error)
        *error = 5; // record->set_error("unknown line type");
      return;
    }
}


/**
 * 文字列から局面をセットする
 * @return 成功はtrue
 */
bool bo_set_by_text(BOARD* bo, const char* s)
{
  string text = s;
  int idx = 0;
  string::size_type epos;
  int error = 0;

  bo_init(bo);

  // 持ち駒を初期化
  for (int i = 0; i <= 7; i++) {
    bo->inhand[0][i] = 0;
    bo->inhand[1][i] = 0;
  }

  while ( (epos = text.find("\n", idx)) != string::npos ) {
    string line(text, idx, epos - idx);
    if (line[0] == '\'') {
      idx = epos + 1;
      continue;
    }

    bo_update_board_by_csa_line(bo, line.c_str(), &error);
    if (error)
      return false;

    idx = epos + 1;
  }

  // 枚数が多すぎないか
  if (!boCheckJustMate(bo)) {
    printf("%s: board error!!!!!\n", __func__); // DEBUG
    return false;
  }

  // 利き情報を更新
  boSetToBOARD(bo);

  return true;
}


/** 
 * CSAファイルから局面を読み込む
 * @return 成功は true
 */
bool bo_load_file( BOARD* bo, const char* filename )
{
  FILE* fp = fopen(filename, "r");
  if (!fp)
    return false;

  char buf[2000];
  int r = fread(buf, 1, sizeof(buf) - 1, fp);
  buf[r] = '\0';

  fclose(fp);

  if ( !bo_set_by_text(bo, buf) )
    return false;

  // 利きを更新
  boSetToBOARD(bo);

  return true;
}
