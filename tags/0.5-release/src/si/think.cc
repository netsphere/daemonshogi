/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <assert.h>
#include "si.h"
#include "si-think.h"
#include "ui.h"
#include <algorithm>
#include <string.h>
#include "taboo_list.h"
#include "picojson.h"
#include <fstream>
using namespace std;


extern int THINK_DEEP_MAX;

NPC g_npc;


/** 
 * side による target の周りへの利きの合計.
 * bo->next と side は同じとは限らない
 */
int think_count_attack24(const BOARD* bo, int side, Xy target )
{
  assert( target >= 0x11 && target <= 0x99);

  int r = 0;

  for (int i = 0; i < 24; i++) {
    Xy const xy = target + arr_round_to24[i];
    if (xy < 0x11 || xy > 0x99)
      continue;
    if (bo->board[xy] == WALL)
      continue;
    r += bo_attack_count(bo, side, xy);
  }

  return r;
}


/**
 * side による target の周りへの利きの合計
 * bo->next と side は同じとは限らない
 */
int think_count_attack8( const BOARD* bo, int side, Xy target )
{
  assert( target >= 0x11 && target <= 0x99);

  int r = 0;

  for (int i = 0; i < 8; i++) {
    Xy const xy = target + arr_round_to[i];
    if (bo->board[xy] == WALL)
      continue;
    r += bo_attack_count(bo, side, xy);
  }

  return r;
}


/**
 * sideの自玉が行ける範囲を数え上げる.
 * とりあえず、2手で行ける範囲
 */
int think_count_king_movables(const BOARD* bo, int side)
{
  Xy king_xy = bo->king_xy[ side ];
  assert( king_xy != 0 );

  int r[69];
  memset( r, 0, 69 * sizeof(int) );

  Xy xy;
  for (int i = 0; i < 8; i++) {
    xy = king_xy + arr_round_to[i];
    if (bo->board[xy] == WALL)
      continue;
    if (bo->board[xy] && getTeban(bo->board[xy]) == side )
      continue;
    if (bo_attack_count(bo, 1 - side, xy))
      continue;

    for (int j = 0; j < 8; j++) {
      Xy xy2 = xy + arr_round_to[j];
      if (xy2 < 0x11 || xy > 0x99)
        continue;
      if (bo->board[xy2] == WALL)
        continue;
      if (bo->board[xy2] && getTeban(bo->board[xy2]) == side )
        continue;
      if (bo_attack_count(bo, 1 - side, xy2))
        continue;

      r[xy2 - king_xy + 34] = 1;
    }
  }

  r[34] = 0;
  return count(r, r + 69, 1);
}


#define DEFE 1
#define OFFE 2


#if 0
/** 攻め、受けに参加している駒を探す */
static void update_mask( unsigned char tbl[], const BOARD* bo, Xy const target, 
	                 unsigned char mask )
{
  // TODO: 広い？
  for (int yd = -3; yd <= +3; yd++) {
    for (int xd = -3; xd <= +3; xd++) {
      int const x = (target & 0xf) + xd;
      int const y = (target >> 4) + yd;
      if (x < 1 || x > 9 || y < 1 || y > 9)
	continue;

      Xy const xy = (y << 4) + x;
      int i;
      // 自分の駒による攻撃
      for (i = 0; i < bo->short_attack[bo->next][xy].count; i++)
	tbl[ bo->short_attack[bo->next][xy].from[i] ] |= mask;
      for (i = 0; i < bo->long_attack[bo->next][xy].count; i++)
	tbl[ bo->long_attack[bo->next][xy].from[i] ] |= mask;

      // 相手方による攻撃
      for (i = 0; i < bo->short_attack[1 - bo->next][xy].count; i++)
	tbl[ bo->short_attack[1 - bo->next][xy].from[i] ] |= mask;
      for (i = 0; i < bo->long_attack[1 - bo->next][xy].count; i++)
	tbl[ bo->long_attack[1 - bo->next][xy].from[i] ] |= mask;
    }
  }
}


/** 
 * 盤上の駒で、bo の手番から見た得点を更新する
 * score->phi のみを設定
 */
static void update_onboard_piece_value( // const unsigned char* tbl, 
				     const BOARD* bo, 
				     // Xy const xy, 
				     PhiDelta* score )
{
  // assert( xy >= 0x11 && xy <= 0x99 );
  // assert( bo->board[xy] != 0 && bo->board[xy] != WALL );

  Xy const king_xy = bo->king_xy[ bo->next ];
  Xy const oppo_king_xy = bo->king_xy[ 1 - bo->next ];

  if ( !king_xy || !oppo_king_xy )
    return;

  // 自玉の位置
  int const kx = king_xy & 0xf; 
  int const ky = king_xy >> 4;

  // 相手玉の位置
  int const jx = oppo_king_xy & 0xf;
  int const jy = oppo_king_xy >> 4;

  for ( int i = 1; i <= 81; i++ ) {
    Xy const xy = arr_xy[i];
    PieceKind const pie = bo->board[ xy ];
    PieceKind const p = pie & 0xf;

    if ( !pie || p == OH ) 
      continue; // 玉は別途評価
    
    int const x = xy & 0xf;
    int const y = xy >> 4;
    int v, w;

    if ( getTeban(pie) == bo->next ) {
      // 自分の駒
      if ( is_long_piece(p) ) {
	score->phi += g_npc.base_piece_points[p] * 100;
      }
      else {
	v = abs(jx - x);
	w = bo->next == SENTE ? y - jy : jy - y;
	score->phi += g_npc.base_piece_points[p] * g_npc.offense_relative_mod[8 + w][v];
      }

      if ( p == FU ) {
	score->phi += g_npc.fu_position_bonus * (bo->next == SENTE ? 9 - y : y - 1);
      }
    }
    else {
      // 相手の駒
      w = bo->next == SENTE ? jy - y : y - jy;
      if ( is_long_piece(p) ) {
	score->phi -= g_npc.base_piece_points[p] * 100;
      }
      else {
	score->phi -= g_npc.base_piece_points[p] * g_npc.defense_relative_mod[8 + w][v];
      }
    }
  }

  // 自玉の近傍. delta
  if ( (tbl[xy] & DEFE) != 0 ) {
    v = abs(kx - x);
    if ( getTeban(bo->board[xy]) == bo->next ) {
      // 自分の駒
      w = bo->next == SENTE ? y - ky : ky - y;
      if ( is_long_piece(p) ) {
	score->delta -= g_npc.base_piece_points[p] * 100;
      }
      else {
	score->delta -= g_npc.base_piece_points[p] * g_npc.defense_relative_mod[8 + w][v];
      }
    }
    else {
      // 相手の駒
      w = bo->next == SENTE ? ky - y : y - ky;
      if ( is_long_piece(p) ) {
	score->delta += g_npc.base_piece_points[p] * 100;
      }
      else {
	score->delta += g_npc.base_piece_points[p] * g_npc.offense_relative_mod[8 + w][v];
      }

      if ( p == FU ) {
	score->delta += g_npc.fu_position_bonus * (bo->next == SENTE ? y - 1 : 9 - y);
      }
    }
  }
}
#endif // 0


/** 
 * 盤上の駒の得点 (の合計) を得る
 */
static int32_t onboard_piece_value(const BOARD* bo )
{
/*
  unsigned char tbl[16 * 10];

  memset(tbl, 0, sizeof tbl);
  // 自玉の近傍
  update_mask( tbl, bo, bo->king_xy[bo->next], DEFE );
  // 相手玉
  update_mask( tbl, bo, bo->king_xy[1 - bo->next], OFFE );
*/

  int32_t score = 0;

  // 盤上の駒
  for (int i = 1; i <= 81; i++) {
    Xy const xy = arr_xy[i];
    PieceKind const pie = bo->board[xy];

    if ( !pie )
      continue;

    // 駒の基礎的な価値
    // update_onboard_piece_value( /* tbl, */ bo, xy, score );

    // 大駒の可動性を加える.
    if ( is_long_piece(pie) ) {
      if ( getTeban(pie) == bo->next ) {
	score += bo->piece_kiki_count[xy] * 
	              g_npc.attack_count_bonus[pie & 0xf];
      }
      else {
	score -= bo->piece_kiki_count[xy] * 
                      g_npc.attack_count_bonus[pie & 0xf];
      }
    }

    // 相手駒への当たり. これは盤面全体を対象にしていい
    if ( getTeban(pie) != bo->next ) {
      if ( bo_attack_count(bo, bo->next, xy) )
	score += g_npc.base_piece_points[pie & 0x7] * g_npc.atari_mod;
    }
    else {
      if ( bo_attack_count(bo, 1 - bo->next, xy) )
	score -= g_npc.base_piece_points[pie & 0x7] * g_npc.atari_mod;
    }
  }

  return score;
}


#ifdef USE_NPC
static void update_king_safe(const BOARD* bo, PhiDelta* score)
{
  int yd, xd;

  // 相手玉の周りの利き制圧度: phi
  Xy const oppo_king_xy = bo->king_xy[1 - bo->next];
  for (yd = -3; yd <= +3; yd++) {
    for (xd = -3; xd <= +3; xd++) {
      int const x = (oppo_king_xy & 0xf) + xd;
      int const y = (oppo_king_xy >> 4) + yd;
      if (x < 1 || x > 9 || y < 1 || y > 9)
	continue;

      Xy const xy = (y << 4) + x;
      int v = abs( (oppo_king_xy & 0xf) - x );
      int w = bo->next == SENTE ? y - (oppo_king_xy >> 4) : 
                                  (oppo_king_xy >> 4) - y;
      score->phi += ( bo_attack_count(bo, bo->next, xy) - 
                      bo_attack_count(bo, 1 - bo->next, xy) ) * 
               100 * g_npc.control_around_oppo_king[w + 8][v];
    }
  }

  // 相手玉の自由度
  score->phi -= g_npc.king_movables[ think_count_king_movables(bo, 1 - bo->next) ];

  // 自玉の周りの利き制圧度: delta
  Xy const my_king_xy = bo->king_xy[bo->next];
  for (yd = -3; yd <= +3; yd++) {
    for (xd = -3; xd <= +3; xd++) {
      int const x = (my_king_xy & 0xf) + xd;
      int const y = (my_king_xy >> 4) + yd;
      if (x < 1 || x > 9 || y < 1 || y > 9)
	continue;

      Xy const xy = (y << 4) + x;
      int v = abs( (my_king_xy & 0xf) - x );
      int w = bo->next == SENTE ? (my_king_xy >> 4) - y : 
                                  y - (my_king_xy >> 4);
      score->delta -= ( bo_attack_count(bo, bo->next, xy) - 
                        bo_attack_count(bo, 1 - bo->next, xy) ) *
                   100 * g_npc.control_around_oppo_king[w + 8][v];
    }
  }

  // 自玉の自由度
  score->delta -= g_npc.king_movables[ think_count_king_movables(bo, bo->next) ];
}
#endif // USE_NPC


/**
 * bo の手番から見た評価値.
 * phi,delta = (+VAL_INF, -VAL_INF) で勝ち
 */
void think_eval(const BOARD* bo, PhiDelta* score) 
{
  // 基礎点数. phi, delta 両方に影響
  // bo->point[] は盤上の駒の基礎点数と持ち駒。 
  score->phi = onboard_piece_value( bo ) +
               (bo->point[bo->next] - bo->point[1 - bo->next]);
  score->delta = -score->phi;

#if 0
  // 持ち駒は両方に加算
  int i;
  for (i = 1; i <= 7; i++) {
    score->phi += g_npc.inhand_points[i] * 100 * bo->inhand[bo->next][i];
    // score->delta -= g_npc.inhand_points[i] * 100 * bo->inhand[bo->next][i];

    score->phi -= g_npc.inhand_points[i] * 100 * bo->inhand[1 - bo->next][i];
    // score->delta += g_npc.inhand_points[i] * 100 * bo->inhand[1 - bo->next][i];
  }    
#endif // 0

  // phi, delta のどちらかだけに影響するもの

  // 自玉
  score->delta -= g_npc.king_xy_points[ rotate_if(bo->king_xy[bo->next], bo->next) ] * 100;

  // 相手玉
  score->phi -= g_npc.king_xy_points[ rotate_if(bo->king_xy[1 - bo->next], 1 - bo->next) ] * 100;

  // 玉の周りの安全度 (危険度)
  update_king_safe(bo, score);
}


template <typename T>
const picojson::value& json_map_value(const picojson::value& v, const string& key)
{
  const picojson::value& r = v.get(key);
  if ( !r.is<T>() )
    throw 1;
  return r;
}


/** 
 * 初期化
 * @return エラーがあったとき false 
 */
bool think_init( const string& filename )
{
  ifstream ifs( filename.c_str() );
  if ( ifs.fail() ) {
    // fprintf(stderr, "Cannot open file: %s\n", filename.c_str() );
    return false;
  }

  picojson::value root;
  string err = picojson::parse(root, ifs);
  if (err != "") {
    cout << err << endl;
    return false;
  }

#ifdef USE_NPC
  // 構造体に設定する
  try {
    int i, j;
    picojson::value v;

    v = json_map_value<picojson::array>(root, "base_piece_points");
    for (i = 0; i < 16; i++)
      g_npc.base_piece_points[i] = v.get(i).get<double>();

    v = json_map_value<picojson::array>(root, "offense_relative_mod");
    for (i = 0; i < 17; i++) {
      for (j = 0; j < 9; j++)
	g_npc.offense_relative_mod[i][j] = v.get(i).get(j).get<double>();
    }

    v = json_map_value<picojson::array>(root, "defense_relative_mod");
    for (i = 0; i < 17; i++) {
      for (j = 0; j < 9; j++)
	g_npc.defense_relative_mod[i][j] = v.get(i).get(j).get<double>();
    }

    v = json_map_value<picojson::array>(root, "inhand_points");
    for (i = 0; i < 8; i++)
      g_npc.inhand_points[i] = v.get(i).get<double>();

    v = json_map_value<picojson::array>(root, "king_xy_points");
    for (i = 0; i < 16 * 10; i++) 
      g_npc.king_xy_points[i] = v.get(i).get<double>();

    v = json_map_value<double>(root, "fu_position_bonus");
    g_npc.fu_position_bonus = v.get<double>();

    v = json_map_value<picojson::array>(root, "attack_count_bonus");
    for (i = 0; i < 16; i++)
      g_npc.attack_count_bonus[i] = v.get(i).get<double>();

    v = json_map_value<double>(root, "atari_mod");
    g_npc.atari_mod = v.get<double>();

    v = json_map_value<picojson::array>(root, "control_around_oppo_king");
    for (i = 0; i < 17; i++) {
      for (j = 0; j < 9; j++)
	g_npc.control_around_oppo_king[i][j] = v.get(i).get(j).get<double>();
    }

    v = json_map_value<picojson::array>(root, "king_movables");
    for (i = 0; i < 25; i++) 
      g_npc.king_movables[i] = v.get(i).get<double>();
  }
  catch (int& ) {
    fprintf(stderr, "Type mismatch.\n");
    return false;
  }
#endif // USE_NPC

  return true;
}
