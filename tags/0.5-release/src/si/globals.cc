/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "si.h"
#include "si-think.h"


/* -------------------------------------------------------- */
/* 他のファイル内の関数から更新される変数 */

int flgDisplayMode = 0;

/** 詰将棋の読みの深さの最大 */
int MATE_DEEP_MAX = 0;

/** 指し手探索での読みの深さの最大 */
int THINK_DEEP_MAX = 0;

long count_hash_hit = 0;

long count_minmax = 0;

long G_COUNT_ALPHA_CUT;
long G_COUNT_BETA_CUT;

/** f_min() 関数内でハッシュに登録された回数 */
long G_F_MIN_PUT_HASH;
/** f_max() 関数内でハッシュに登録された回数 */
long G_F_MAX_PUT_HASH;

/** GAME */
GAME g_game;


/* -------------------------------------------------------- */
/* ここから静的なデータ */

/** 
 * 盤上の位置の配列.
 * 2009.11 二重ループするより速い
 */
const Xy arr_xy[] = {
  0,
  0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
  0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
  0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
  0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49,
  0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59,
  0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69,
  0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79,
  0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
  0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99,
};

/**
 * 周り８方向への移動データ。
 * -16 : 上
 * -15 : 右斜め上
 *  -1 : 右
 *  15 : 右斜め下
 *  16 : 下
 *  17 : 左斜め下
 *   1 : 左
 * -17 : 左斜め上
 */
const XyDiff arr_round_to[] = {
  -16, -17, -1, 15, 16, 17, 1, -15,
};

/** 周り２マス分の移動データ */
const XyDiff arr_round_to24[] = {
  -34, -33, -32, -31, -30, 
  -18, -17, -16, -15, -14,
   -2,  -1,        1,   2,
   14,  15,  16,  17,  18, 
   30,  31,  32,  33,  34,
};


/** 先手陣の領域 */
int arr_sentejin[] = {
  0,
  0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99,
  0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
  0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79,
};

/** 後手陣の領域 */
int arr_gotejin[] = {
  0,
  0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
  0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
  0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
};
