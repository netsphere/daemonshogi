/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <assert.h>
#include "si.h"
#include "si-think.h"
#include "ui.h"
#include "taboo_list.h"
#include <algorithm>

#define DEBUGLEVEL 0

#if DEBUGLEVEL >= 3
#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>
#endif
using namespace std;


#ifdef USE_GUI
extern GAME g_game;
#endif


/**
 * 置換表から探す
 * @return 見つかったときtrue
 */
static bool lookup_hash( BOARD* bo,
                         int const te_count, int rest_depth,
			 TE* best_move,
			 PhiDelta* score )
{
  // ここが詰め将棋と共通化できない
  if ( hsGetBOARDMinMax(bo, score, best_move, te_count) )
    return true;

  think_eval(bo, score);
  return false;
}


struct SearchParam
{
  int te_count;
  int start_depth;
};


/** 置換表に勝ちを登録 */
struct PutWin {
  int te_count;
  PutWin(int te_count_): te_count(te_count_) {}
  void operator () (const BOARD* bo, const TE& move) const {
    hsPutBOARDMinMax(bo, +VAL_INF, -VAL_INF, te_count, move);
  }
};


/** 
 * 手を指す. 悪手のときは指さない
 * @return 悪手のとき, 指さずに false を返す
 */
static bool do_move( const SearchParam& param,
                     int rest_depth,
                     BOARD* node,
		     const TsumeroNogare& tsumero_nogare,
                     MOVEINFO* const mi, int index,
		     PhiDelta* child_score )
{
  if ( (te_hint(mi->te[index]) & TE_STUPID) != 0 )
    return false;

  bool is_checked = bo_is_checked(node, node->next);
  int atkcnt = think_count_attack24(node, node->next, 
				    node->king_xy[1 - node->next]);

  boMove( node, mi->te[index] );

  // 千日手？ とりあえず1回目でもループは避ける
  if ( bo_repetition_count(node) >= 1 ) {
    boBack( node );
    return false;
  }

  TE tt_move;
  bool r = lookup_hash( node, param.te_count, rest_depth,
                        &tt_move, 
			child_score );
  if (r) {
    if ( !teCmpTE(mi->te[index], tt_move) )
      te_set_hint( &mi->te[index], te_hint(tt_move) );

    return true;
  }

  if (is_checked) {
    // 王手の受け
    return true; 
  }
  else if ( bo_is_checked(node, node->next) ) {
    // 王手
    return true;
  }

  if ( (te_hint(node->mseq.te[node->mseq.count - 2]) & TE_TSUMERO) != 0) {
    // 詰めろを掛けられていたとき
    if ( !tsumero_nogare.promise(node) ) {
      // 判定が軽いときは置換表に登録するまでもない
      // hsPutBOARDMinMax(node, +VAL_INF, -VAL_INF, param.te_count, TE_NUL );
      boBack(node);
      return false;
    }

    // 受けになっているか
    TE mate_te;
    int mate_node_limit = GAME_MATE_NODE_LIMIT;
    if ( dfpn_mate(node, 31, &mate_te, &mate_node_limit) == CHECK_MATE ) {
      // 受けになっていない. 負け
      hsPutBOARDMinMax(node, +VAL_INF, -VAL_INF, param.te_count, TE_NUL );

      if ( te_is_put(mi->te[index]) ) {
        // ほかの打つ手はどうか
        mate_set_muda_uchi( node->next, 1, PutWin(param.te_count),
                            node, mi, index + 1 );
      }
      boBack(node);
      return false;
    }
  }

  // こちらから詰めろ (または詰めろ逃れの詰めろ) が掛かるか
  // ルートノードのときのみ調べる. TODO: より深いノードでも調べる
  if ( param.start_depth == rest_depth ) {
    bool promise = false;

    // - 取る手
    if ( node->tori[node->mseq.count - 1] )
      promise = true;

    // - 利きを入れる手
    else if ( think_count_attack24(node, 1 - node->next, 
				 node->king_xy[node->next]) > atkcnt )
      promise = true;

    if ( promise ) {
      // 詰めろ？
      bo_pass(node);
      int mate_node_limit = GAME_MATE_NODE_LIMIT;
      TE mate_te;
      if ( dfpn_mate(node, 31, &mate_te, &mate_node_limit) == CHECK_MATE ) {
        // 詰めろを掛けたときは仮評価を足して、優先的に読むように
	// child_score->phi -= 1000 * 100;
        child_score->delta += 1000 * 100; // TODO: NPC へ移動
        te_add_hint( &mi->te[index], TE_TSUMERO );
      }
      boBack_mate(node); // パスを戻す
      return true;
    }

    // 2手指してみる
    bo_pass( node );

    MOVEINFO mi;
    make_moves( node, &mi );

    child_score->phi = +VAL_INF; child_score->delta = -VAL_INF;
    for (int i = 0; i < mi.count; i++) {
      boMove( node, mi.te[i] );

      PhiDelta c;
      think_eval( node, &c );
      if ( (c.delta - c.phi) > (child_score->delta - child_score->phi) ) {
	child_score->phi = c.phi; child_score->delta = c.delta;
      }

      boBack( node );
    }
    boBack_mate( node ); // パスを戻す
  }

  return true;
}


/**
 * 手を展開し、最善手を選ぶ
 * 子ノードのうち delta 最小を選ぶ
 * 勝ち = (+VAL_INF, -VAL_INF), 負け = (-VAL_INF, +VAL_INF)
 * 一通り展開するので、move ordering は効果なし
 */
static void expand_and_best_move( const SearchParam& param,
                                  BOARD* node,
                                  int rest_depth, int rest_node,
                                  PhiDelta* best_child,
				  // int32_t* child_phi_min,
                                  PhiDelta* second_child,
                                  TE* best_move )
{
  // 手を展開する
  MOVEINFO mi;
  make_moves(node, &mi);

  best_child->delta = -VAL_INF; best_child->phi = +VAL_INF; // worst value
  // *child_phi_min = +VAL_INF;

  if (!mi.count) {
    // 打ち歩詰め. ノード側の勝ち
    if ( node->mseq.count >= 1 && 
	 te_is_put(node->mseq.te[node->mseq.count - 1]) &&
	 te_pie(node->mseq.te[node->mseq.count - 1]) == FU ) {
      best_child->phi = -VAL_INF; best_child->delta = +VAL_INF;
      te_clear(best_move);
      return;
    }

    // 負け
    te_clear(best_move);
    return;
  }

  *second_child = *best_child;
  const TE* may_best = mi.te; // 手がある限り何か選ぶ

#if DEBUGLEVEL >= 3
  printf("depth %d: ", rest_depth);
#endif

  // 手を指す前の状態. 相手から詰めろを掛けられていたときのチェック用
  TsumeroNogare tsumero_nogare;
  tsumero_nogare.before_move( node, 1 - node->next );

  for (int i = 0; i < mi.count; i++) {
    PhiDelta child_score;  // child nodeにとってよい局面 -> phi大, delta小

    if ( !do_move(param, rest_depth, node, tsumero_nogare,
		  &mi, i, &child_score) )
      continue;

    if ( (child_score.delta - child_score.phi) > 
	 (best_child->delta - best_child->phi) ) {
      // bestを更新
      *second_child = *best_child;
      *best_child = child_score;
      may_best = mi.te + i;

      // 選択した手のphi
      // *child_phi_min = child_score.phi;
    }
    else if ( (child_score.delta - child_score.phi) > 
	      (second_child->delta - second_child->phi) ) {
      // 2位のみ更新
      *second_child = child_score;
    }

    // TODO: これでいいかどうか -> 選択しなかった手の値になることがある. 不味そう
    // *child_phi_min = min(*child_phi_min, child_score.phi);

    boBack(node);

#if DEBUGLEVEL >= 3
    printf("%s=(%d,%d), ", te_str(mi.te[i], *node).c_str(),
           child_score.phi, child_score.delta);
#endif

    if ( child_score.phi <= -VAL_MAY_MATCH || 
         child_score.delta >= +VAL_MAY_MATCH ) {
      break;  // nodeの手番側の勝ち確定
    }
  }

#if DEBUGLEVEL >= 3
  printf("best = %s\n", te_str(*may_best, *node).c_str());
#endif
  *best_move = *may_best;
}


/**
 * 探索する
 * 必ず一度はノードを展開する
 */
static void bfs_search( const SearchParam& param,
			BOARD* node,
                        const PhiDelta& threshold,
                        int depth, int* node_limit )
{
#ifndef NDEBUG
  uint64_t const node_hash = node->hash_all;
#endif // !NDEBUG

#if DEBUGLEVEL >= 3
  printf("%d: enter: threshold=(%d, %d), %d\n",
         depth, threshold.phi, threshold.delta, *node_limit);
#endif

  TE best_move, prev_move;
  PhiDelta prev_threshold = make_phi_delta(0, 0);
  te_clear(&best_move);
  te_clear(&prev_move);

#ifdef USE_GUI
  if ( !(*node_limit % 700) ) {
    /* GUIのペンディングされたイベントを処理する */
    processing_pending( false );
  }
#endif /* USE_GUI */

  if ( param.start_depth == depth ||
       (node->mseq.count >= 1 && 
        (te_hint(node->mseq.te[node->mseq.count - 1]) & TE_TSUMERO) != 0) ) {
    // ルートノードか、詰めろを掛けられていた. 勝ちがあるか
    int mate_node_limit = GAME_MATE_NODE_LIMIT;
    if ( dfpn_mate(node, 31, &best_move, &mate_node_limit) == CHECK_MATE ) {
      hsPutBOARDMinMax( node, +VAL_INF, -VAL_INF, param.te_count, best_move );
      (*node_limit)--;
      return;
    }
  }

  if ( node->mseq.count >= 2 && 
       (te_hint(node->mseq.te[node->mseq.count - 2]) & TE_TSUMERO) != 0 ) {
    // 詰めろを掛けた次の手で、詰め将棋を読んでみる
    int mate_node_limit = GAME_MATE_NODE_LIMIT;
    if ( dfpn_mate(node, 31, &best_move, &mate_node_limit) == CHECK_MATE ) {
      hsPutBOARDMinMax( node, +VAL_INF, -VAL_INF, param.te_count, best_move );
      (*node_limit)--;
      return;
    }
  }

  // サイクル回避
  bo_inc_repetition(node);

  PhiDelta best_child;
  // int32_t child_phi_min;

  while (true) {
    PhiDelta second_child;

    te_clear(&best_move);

    // ノードを展開して最善手を得る
    expand_and_best_move( param, node,
                          depth, *node_limit,
                          &best_child,
			  // &child_phi_min,
                          &second_child,
                          &best_move );

    // 閾値を超えた？
    if ( (best_child.delta - best_child.phi) <= threshold.phi || 
	 -(best_child.delta - best_child.phi) <= threshold.delta )
      break;

    // 候補手が一つでもあれば必ず設定されるはず
    assert( te_to(best_move) );

#ifdef USE_GUI
    /* 中断や時間切れが入ってないか調べる */
    if ( gmGetGameStatus(&g_game) != 0 )
      break;
#endif // USE_GUI

    // 探索ノードを使い果たした
    if ( depth <= 1 || *node_limit < 0 )
      break;

    PhiDelta threshold_child;

    // このしきい値を超える (下回る) -> delta cutが起こる
    threshold_child.phi = threshold.delta;

    // このしきい値を超える (下回る) -> 2位の方が有望
    threshold_child.delta = max( threshold.phi, 
				 (second_child.delta - second_child.phi) - 1 );

    // 先端 ?
    if ( !teCmpTE(prev_move, best_move) && prev_threshold == threshold_child ) {
#if 0
      printf( "%d: loop!! threshold=(%d, %d), score=(%d,%d)\n",
	      depth, threshold.phi, threshold.delta, 
	      best_child.delta, child_phi_min ); // DEBUG
#endif // 0
      break;
    }

    boMove(node, best_move);

    bfs_search( param, node, threshold_child,
                depth - 1, node_limit );

    boBack(node);

    prev_move = best_move;
    prev_threshold = threshold_child;
  }

  assert( node_hash == node->hash_all );
  hsPutBOARDMinMax( node, best_child.delta, best_child.phi /*child_phi_min*/, 
		    param.te_count, best_move );

#if DEBUGLEVEL >= 3
    printf("%d: put %" PRIx64 "-> (%d,%d)\n",
           depth, node->key, best_child.delta, child_phi_min );
#endif

  (*node_limit)--;

  // タブーリストから除く
  bo_dec_repetition(node);
}


/**
 * 通常探索をおこなう
 * @param bo 現在の局面
 * @param depth 探索する最大の深さ
 * @param best_move 最善手を返す. 手がない（投了）のときはクリアする
 *
 * @return SI_NORMAL
 */
INPUTSTATUS bfs_minmax( BOARD* bo, int depth, TE* best_move )
{
  assert(best_move);

  int r;
  PhiDelta score;

  // 結論が出ているか？
  r = hsGetBOARDMinMax( bo, &score, best_move, 0 );
  if (r) {
    // 結論が出ている場合のみ利用
    if ( score.phi >= +VAL_INF && score.delta <= -VAL_INF ) {
      assert( te_to(*best_move) >= 0x11 && te_to(*best_move) <= 0x99);
      return SI_NORMAL;
    }
    else if ( score.phi <= -VAL_INF && score.delta >= +VAL_INF ) {
      te_clear(best_move);
      return SI_NORMAL;
    }
  }

  SearchParam param;
  param.te_count = bo->mseq.count;
  param.start_depth = depth;
  int node_limit = 25000; // magic
  bfs_search( param, bo, 
	      make_phi_delta(-VAL_MAY_MATCH, -VAL_MAY_MATCH),
              depth, &node_limit );

  // 中断または時間切れ
  if ( gmGetGameStatus(&g_game) != 0 )
    return gmGetGameStatus(&g_game);

  r = hsGetBOARDMinMax( bo, &score, best_move, 0 );
  if (!r) {
    si_abort("internal error: not found result\n");
  }

  if ( score.phi <= -VAL_MAY_MATCH && score.delta >= +VAL_MAY_MATCH ) {
    te_clear(best_move);
    return SI_NORMAL;
  }

  assert( te_to(*best_move) >= 0x11 && te_to(*best_move) <= 0x99);
  return SI_NORMAL;
}
