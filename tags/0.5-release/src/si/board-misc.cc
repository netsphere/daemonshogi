/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "board-misc.h"


/** (x, y)に駒 p をセットする。 */
void daemon_dboard_set_board(DBoard* bo, int x, int y, PieceKind p) 
{
  assert(bo != NULL);
  assert(1 <= x && x <= 9);
  assert(1 <= y && y <= 9);
  assert(0 <= p && p <= 0x1F);

  bo->board[(y << 4) + x] = p;
}


/** (x, y)の駒を返す。 */
PieceKind daemon_dboard_get_board(const DBoard* bo, int x, int y) 
{
  assert(bo != NULL);
  assert(1 <= x && x <= 9);
  assert(1 <= y && y <= 9);

  return bo->board[(y << 4) + x];
}


/** next 手番の持ち駒 p の枚数をセットする。 */
void daemon_dboard_set_piece(DBoard* bo, int next, PieceKind p, int n) 
{
  bo->inhand[next][p & 7] = n;
}


/** next 手番の持ち駒 p の枚数を一枚プラスする。 */
void daemon_dboard_add_piece(DBoard* bo, int next, PieceKind p) 
{
  bo->inhand[next][p & 7]++;
}


/** next 手番の持ち駒 p の枚数を一枚減らす。 */
void daemon_dboard_dec_piece(DBoard* bo, int next, PieceKind p) 
{
  bo->inhand[next][p & 7]--;
  assert( bo->inhand[next][p & 7] >= 0 );
}


/**
 * 持ち駒 p の枚数を返す。
 * @param bo 局面
 * @param next 手番
 * @param p 駒種
 */
int daemon_dboard_get_piece(const DBoard* bo, int next, PieceKind p) 
{
  return bo->inhand[next][p & 7];
}


/** 平手の盤面をセットする */
void bo_reset_to_hirate( BOARD* bo )
{
  // 平手の配置パターン
  static const int hirate[] = {
    0x12, 0x13, 0x14, 0x15, 0x18, 0x15, 0x14, 0x13, 0x12,
    0x00, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x17, 0x00,
    0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00,
    0x02, 0x03, 0x04, 0x05, 0x08, 0x05, 0x04, 0x03, 0x02,
  };
  int x, y;
  
  bo_init(bo);
  
  for (y = 0; y < 9; y++) {
    for (x = 0; x < 9; x++) {
      daemon_dboard_set_board(bo, x + 1, y + 1, hirate[y * 9 + x]);
    }
  }

  // 利きを更新
  boSetToBOARD( bo );
}


/** 詰将棋用に盤面をセットする */
void daemon_dboard_set_mate(DBoard* board) 
{
  bo_init(board);

  daemon_dboard_set_board(board, 5, 1, 0x18);
  boSetTsumeShogiPiece( board );
}


void daemon_dboard_set_rl_reversal(DBoard *board) 
{
  int x, y, p;

  // tmp = daemon_dboard_new();
  // daemon_dboard_copy(board, tmp);
  BOARD* tmp = bo_dup(board);

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      p = daemon_dboard_get_board(tmp, x, y);
      daemon_dboard_set_board(board, 10 - x, y, p);
    }
  }

  // daemon_dboard_free(tmp);
  freeBOARD( tmp );
}


void daemon_dboard_set_order_reversal(DBoard* board) 
{
  int x, y;
  PieceKind p;

  // tmp = daemon_dboard_new();
  // daemon_dboard_copy(board, tmp);
  BOARD* tmp = bo_dup(board);

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      p = daemon_dboard_get_board(tmp, x, y);
      if (p != 0) {
	p ^= 0x10;
      }
      daemon_dboard_set_board(board, x, 10 - y, p);
    }
  }

  // daemon_dboard_free(tmp);
  freeBOARD( tmp );
}


/** 先手後手の盤面を入れ換える */
void daemon_dboard_flip(DBoard* board) 
{
  int x, y, i;
  PieceKind p;

  // tmp = daemon_dboard_new();
  // daemon_dboard_copy(board, tmp);
  BOARD* tmp = bo_dup(board);

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      p = daemon_dboard_get_board(tmp, x, y);
      if (p != 0) {
	p ^= 0x10;
      }
      daemon_dboard_set_board(board, 10 - x, 10 - y, p);
    }
  }

  for (i=1; i<=7; i++) {
    p = daemon_dboard_get_piece(tmp, SENTE, i);
    daemon_dboard_set_piece(board, GOTE, i, p);
    p = daemon_dboard_get_piece(tmp, GOTE, i);
    daemon_dboard_set_piece(board, SENTE, i, p);
  }

  // daemon_dboard_free(tmp);
  freeBOARD( tmp );
}


static const int piece_max[] = {
  0, 18, 4, 4, 4, 4, 2, 2, 2,
};

/** 全ての駒を駒箱へ移動する */
void daemon_dboard_set_all_to_pbox(DBoard* board) 
{
  int x, y, i;

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      daemon_dboard_set_board(board, x, y, 0);
    }
  }

  for (i=1; i<=7; i++) {
    daemon_dboard_set_piece(board, GOTE, i, 0);
    daemon_dboard_set_piece(board, SENTE, i, 0);
  }

  for (i=1; i<=8; i++) {
    daemon_dboard_set_pbox(board, i, piece_max[i]);
  }
}


/** 編集用に使っていない駒を pbox に格納する */
void daemon_dboard_set_for_edit(DBoard *board) 
{
  int piece[9];
  int x, y, p;
  int i;
  
  for (i=0; i<9; i++) {
    piece[i] = 0;
  }

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      p = daemon_dboard_get_board(board, x, y);
      if ((p & 0xF) == 8) {
	p = 8;
      } else {
	p &= 7;
      }
      piece[p]++;
    }
  }

  for (i=1; i<=7; i++) {
    p = daemon_dboard_get_piece(board, SENTE, i);
    piece[i] += p;
    p = daemon_dboard_get_piece(board, GOTE, i);
    piece[i] += p;
  }

  for (i=1; i<=8; i++) {
    daemon_dboard_set_pbox(board, i, piece_max[i] - piece[i]);
  }
}


/** DBoard の中見を src から dest にコピーする */
void daemon_dboard_copy(const DBoard* src, DBoard *dest) 
{
  *dest = *src;
}


void daemon_dboard_set_pbox(DBoard *bo, PieceKind p, int n) 
{
  assert(bo != NULL);
  assert(1 <= p && p <= 8);
  assert(0 <= n && n <= 18);

  bo->pbox[p] = n;
}


int daemon_dboard_get_pbox(const DBoard* bo, PieceKind p) 
{
  assert(bo != NULL);
  assert(1 <= p && p <= 8);

  return bo->pbox[p];
}

void daemon_dboard_add_pbox(DBoard *bo, int p) {
  assert(bo != NULL);
  assert(1 <= p && p <= 8);

  bo->pbox[p]++;
}

void daemon_dboard_dec_pbox(DBoard *bo, int p) {
  assert(bo != NULL);
  assert(1 <= p && p <= 8);

  bo->pbox[p]--;
}
