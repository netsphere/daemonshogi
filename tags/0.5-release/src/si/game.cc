/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "si.h"
#include "ui.h"
#ifdef USE_SERIAL
#include "comaccess.h"
#endif /* USE_SERIAL */


/**
 * 初期化付きでGAMEを生成する。
 * @return GAMEのポインタ
 */
GAME* newGAME() 
{
  GAME *game;

  game = (GAME *) malloc(sizeof(GAME));
  if (game == NULL) {
    si_abort("No enough memory. In moveinfo.c#newGAME()");
  }

  initGAME(game);

  return game;
}


/**
 * game を初期化する。
 * @param gm 対象のGAME
 */
void initGAME(GAME* gm) 
{
  assert(gm != NULL);

  // gm->teai = HIRATE;
  gm->si_input_next[SENTE] = NULL;
  gm->si_input_next[GOTE] = NULL;

  gm->computer_level[SENTE] = 5;
  gm->computer_level[GOTE]  = 5;
#ifdef USE_SERIAL
  initCOMACCESS(&(gm->ca[SENTE]));
  initCOMACCESS(&(gm->ca[GOTE]));
#endif /* USE_SERIAL */
  gm->flg_run_out_to_lost = 1;
  gm->game_status = SI_NORMAL;
  // gm->chudan = NULL;
}

/**
 * game のメモリを解放する。
 * @param game 対象のGAME
 */
void freeGAME(GAME *game) {
#ifdef DEBUG
  assert(game != NULL);
#endif /* DEBUG */
  
  free( game );
}

/**
 * 次の手を入力する関数へのポインタを返す。
 * @param gm 対象のGAME
 * @param next 次の手番
 * @return 次の手を入力する関数へのポインタ
 */ 
InputCallbackFunc gmGetInputNextPlayer(const GAME* gm, int next)
{
  return gm->si_input_next[next];
}


#ifdef USE_SERIAL
/**
 * シリアルポートのデバイルファイルの名前を返す。
 * @param gm 対象のGAME
 * @param next 次の手番
 * @return 次の手を入力する関数へのポインタ
 */ 
char *gmGetDeviceName(GAME *gm, int next)
{
  return gm->ca[next].DEVICENAME;
}
#endif /* USE_SERIAL */


/**
 * コンピューターのレベルを返す。
 * @param gm 対象のGAME
 * @param next 次の手番
 * @return コンピューターのレベル
 */ 
int gmGetComputerLevel(GAME *gm, int next) {
  return gm->computer_level[next];
}

/**
 * 対戦を行うプレイヤーを設定する。
 * 
 * @param gm 対象GAME
 * @param next 設定する手番
 * @param fn コールバック関数
 */ 
void gmSetInputFunc(GAME* gm, int next, InputCallbackFunc fn) 
{
  assert(gm != NULL);
  assert(next == SENTE || next == GOTE); 
  assert( fn );

  gm->si_input_next[next] = fn;
}


#ifdef USE_SERIAL
/**
 * デバイス名をセットする。
 * @param gm 対象のGAME
 * @param next セットする手番 
 * @param s デバイス名
 */
void gmSetDeviceName(GAME *gm, int next, char *s)
{
  caSetDEVICENAME(&(gm->ca[next]), s);

}
#endif /* USE_SERIAL */


/**
 * ゲームのステータスをセットする。
 * @param gm 対象のGAME
 * @param status ゲームのステータス
 */
void gmSetGameStatus(GAME* gm, INPUTSTATUS status) 
{
  assert( gm != NULL);
  gm->game_status = status;
}


#if 0
/**
 * 中断フラグへのポインタをセットする。
 * @param gm 対象のGAME
 * @param chudan 中断フラグへのポインタ
 */ 
void gmSetChudan(GAME *gm, int *chudan) {
  gm->chudan = chudan;
}

/**
 * 中断フラグへのポインタを返す。
 * @param gm 対象のGAME
 * @return 中断フラグへのポインタ
 */ 
int *gmGetChudan(GAME *gm) {
  return gm->chudan;
}


/**
 * 中断フラグの値を返す。
 * @param gm 対象のGAME
 * @return 中断フラグの値
 */ 
int gmGetChudanFlg(GAME *gm) {
  if (gm->chudan == NULL) {
    return 0;
  }
  
  return *gm->chudan;
}
#endif // 0
