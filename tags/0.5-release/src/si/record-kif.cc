/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <list>
#include <stdexcept>
#include "dte.h"
#include "movetree.h"
#include "board-misc.h"
#include "record.h"
#include "filereader.h"
#include "filewriter.h"
// #include "misc.h"
using namespace std;


static void daemon_record_create_time_string(char *buf, time_t t, time_t total) 
{
  time_t min, sec;
  time_t total_hour, total_min, total_sec;

  min = t / 60;
  sec = t % 60;
  total_hour = total / 3600;
  total = total % 3600;
  total_min  = total / 60;
  total_sec  = total % 60;

  sprintf(buf, "(%2d:%02d/%02d:%02d:%02d)",
	  (int)min,
	  (int)sec,
	  (int)total_hour,
	  (int)total_min,
	  (int)total_sec);
}


static const char* knum_data[] = {
  "",  "一", "二", "三", "四", "五", "六", "七", "八", "九",
};

static const char* anum_data[] = {
  "",  "１", "２", "３", "４", "５", "６", "７", "８", "９", 
};

static const char* kif_koma_data[] = {
  "",   "歩", "香", "桂", "銀", "金", "角", "飛",
  "玉", "と", "杏", "圭", "全", "",   "馬", "龍",
};


/** 持ち駒を出力 */
static void daemon_record_output_kif_piece(const DBoard* board, 
				    FileWriter* writer, 
				    TEBAN next) 
{
  int count;
  int i;

  if (next == SENTE) {
    daemon_filewriter_put(writer, "先手の持駒：");
  } else {
    daemon_filewriter_put(writer, "後手の持駒：");
  }

  for (i=1; i<=7; i++) {
    if (daemon_dboard_get_piece(board, next, i) != 0) 
      break;
  }
  if (i == 8) {
    /* 持ち駒なし */
    daemon_filewriter_put(writer, "なし");
  }

  for (i=7; 1 <= i; i--) {
    count = daemon_dboard_get_piece(board, next, i);
    if (count == 0) {
      continue;
    }
    daemon_filewriter_put(writer, kif_koma_data[i]);
    if (count == 1) {
      daemon_filewriter_put(writer, "　");
      continue;
    }
    if (10 <= count) {
      daemon_filewriter_put(writer, "十");
      count -= 10;
      if (count == 0) {
	daemon_filewriter_put(writer, "　");
	continue;
      }
    }
    daemon_filewriter_put(writer, knum_data[count]);
    daemon_filewriter_put(writer, "　");
  }

  daemon_filewriter_put(writer, "\n");
}


static const char* long_koma_data[] = {
  "",   "歩", "香",   "桂",   "銀",   "金", "角", "飛",
  "玉", "と", "成香", "成桂", "成銀", "",   "馬", "龍",
};


struct ElapsedTime {
  time_t time[2];
};


static void kif_output_seq_sub(DBoard* board, 
			      int te_count, 
			      ElapsedTime total_time,
			      Xy const last_xy,
			      const KyokumenNode* node,
			      FileWriter* writer)
{
  KyokumenNode::Moves::size_type i;
  char buf[BUFSIZ];
  // int back_count = 0;
  char time_buf[BUFSIZ];

  if ( !node->moves.size() ) {
    daemon_record_create_time_string(time_buf, 0, 
				     total_time.time[board->next]);
    sprintf(buf, "%4d 中断         %s\n", te_count + 1, time_buf);
    daemon_filewriter_put(writer, buf);
    sprintf(buf, "まで%d手で中断\n", te_count);
    daemon_filewriter_put(writer, buf);

    return;
  }

  for (i = 0; i < node->moves.size(); i++) {
    if ( i ) {
      sprintf(buf, "\n変化：%d手\n", te_count + 1 );
      daemon_filewriter_put(writer, buf);
    }

    const MoveEdge& mv = node->moves[i];
    DTe const te = mv.te; 

    if (te.special) {
      switch (te.special) {
      case DTe::RESIGN:
        sprintf(buf, "%4d 投了        ", te_count + 1);
	break;
      case DTe::KACHI:
	assert(0); // 不明
	break;
      default:
	assert(0);
      }
    }
    else if ( !te.fm ) {
      sprintf(buf, "%4d %s%s%s打    ",
	      te_count + 1,
	      anum_data[te.to & 0xF],
	      knum_data[te.to >> 4],
	      long_koma_data[te.uti]);
    } 
    else {
      // 動かす手
      PieceKind p = daemon_dboard_get_board(board, te.fm & 0xF, te.fm >> 4);
      p &= 0xF;
      assert( p );
      if (last_xy == te.to) {
	sprintf(buf, "%4d 同　%s%s(%d%d)%s",
		te_count + 1,
		long_koma_data[p],
		te.nari ? "成" : "",
		te.fm & 0xF,
		te.fm >> 4,
		te.nari ? "" : "  " );
      } 
      else {
	sprintf(buf, "%4d %s%s%s%s(%d%d)%s",
		te_count + 1,
		anum_data[te.to & 0xF],
		knum_data[te.to >> 4],
		long_koma_data[p],
		te.nari ? "成" : "",
		te.fm & 0xF, 
		te.fm >> 4,
		te.nari ? "" : "  " );
      }
    }
    total_time.time[board->next] += mv.time;
    daemon_record_create_time_string(time_buf, mv.time, 
				     total_time.time[board->next]);
    sprintf( buf + strlen(buf), " %s%s\n", time_buf,
	     node->moves.size() > i + 1 ? "+" : "" );
    daemon_filewriter_put(writer, buf);

    // コメント行
    if ( mv.te_comment != "" || mv.annotation ) {
      vector<string> c;

      if ( mv.annotation == ANNOT_POOR )
	*writer << "*[?]";
      else if ( mv.annotation == ANNOT_QUESTIONABLE )
	*writer << "*[?!]";
      else 
	*writer << "*";

      str_split(&c, mv.te_comment, '\n');
      for ( vector<string>::size_type  ii = 0; ii < c.size(); ii++ ) {
	if (ii)
	  *writer << "*";
	*writer << c[ii] << "\n";
      }
    }

    if ( te.special ) {
      sprintf( buf, "まで%d手で%sの勝ち\n", 
	       te_count, (board->next == SENTE ?  "後手" : "先手") );
      daemon_filewriter_put(writer, buf);
    }
    else {
      // 子ノードを先に出力する
      KyokumenNode* child = node->lookup_child(te);
      // daemon_dboard_move(board, &te);
      boMove_mate(board, te.pack() );
      kif_output_seq_sub(board, te_count + 1, 
				      total_time,
				      te.to,
				      child, writer);
      // daemon_dboard_back(board, &te);
      boBack_mate(board);
      total_time.time[board->next] -= mv.time;
    }
  } // of for
}


static void output_kif_board(const DBoard* board, FileWriter* writer)
{
  int x, y;
  int p;

  daemon_record_output_kif_piece(board, writer, GOTE);
  daemon_filewriter_put(writer, "  ９ ８ ７ ６ ５ ４ ３ ２ １\n");
  daemon_filewriter_put(writer, "+---------------------------+\n");
  for (y=1; y<=9; y++) {
    daemon_filewriter_put(writer, "|");
    for (x=9; 1 <= x; x--) {
      p = daemon_dboard_get_board(board, x, y);
      if (p == 0) {
	daemon_filewriter_put(writer, " ・");
      } else {
	if (p < 0x10) {
	  daemon_filewriter_put(writer, " ");
	} else {
	  daemon_filewriter_put(writer, "v");
	}
	daemon_filewriter_put(writer, kif_koma_data[p & 0xF]);
      }
    }
    daemon_filewriter_put(writer, "|");
    daemon_filewriter_put(writer, knum_data[y]);
    daemon_filewriter_put(writer, "\n");
  }
  daemon_filewriter_put(writer, "+---------------------------+\n");
  daemon_record_output_kif_piece(board, writer, SENTE);
  if (board->next == SENTE) {
    daemon_filewriter_put(writer, "先手番\n");
  } else {
    daemon_filewriter_put(writer, "後手番\n");
  }
}


/**
 * @return 成功 0
 */
static int daemon_record_output_kif_p(Record* record, FileWriter& writer) 
{
  BOARD* board = bo_dup( &record->first_board );

  daemon_filewriter_put(&writer, "# ---- daemonshogi kif file ----\n");
  writer << "# ファイル名：" << record->filename << "\n";
  if ( record->start_time.year != 0 )
    writer << "開始日時：" << get_datetime_string(record->start_time) << "\n";
  if ( record->title != "" )
    writer << "表題：" << record->title << "\n";
  if ( record->source != "" )
    writer << "掲載：" << record->source << "\n";

  // 初期局面
  BOARD* tmp_bo = newBOARDwithInit();
  bo_reset_to_hirate( tmp_bo );
  if (*tmp_bo == *board)
    daemon_filewriter_put(&writer, "手合割：平手\n");
  else
    output_kif_board(board, &writer);
  freeBOARD( tmp_bo );

  writer << "先手：" << 
    (record->player_name[0] != "" ? record->player_name[0] : "") << "\n";

  writer << "後手：" <<
    (record->player_name[1] != "" ? record->player_name[1] : "") << "\n";

  daemon_filewriter_put(&writer, "手数----指手---------消費時間--\n");

  ElapsedTime total_time;
  total_time.time[0] = 0;
  total_time.time[1] = 0;
  Xy last_xy = 0;

  kif_output_seq_sub(board, 0, total_time, last_xy, record->mi, &writer);

  freeBOARD( board );
  return 0;
}


/** 
 * 柿木 KIF形式でファイル filename に出力する。
 * @return 成功 0
 */
int daemon_record_output_kif(Record* record, FileWriter* writer )
{
  return daemon_record_output_kif_code(record, writer, "CP932");
}


/**
 * 柿木 KIF形式でファイル filename に出力する。
 * @param record 棋譜データ
 * @param writer 出力ストリーム
 * @param code 文字コード
 */
int daemon_record_output_kif_code(Record* record, 
				  FileWriter* writer,
				  const char* code) 
{
  assert(record != NULL);
  assert( writer != NULL);
  assert(code);

  daemon_filewriter_set_outcode(writer, code);
/*
  r = daemon_filewriter_open(writer, filename);
  if (r == -1)
    return -1;
*/
  return daemon_record_output_kif_p(record, *writer);
  // daemon_filewriter_close(writer);
}


struct RecordLoadError: public runtime_error
{
  RecordLoadError(const string& s): runtime_error(s) {}
};


/** 持ち駒の解析 */
static void daemon_record_load_kif_piece( BOARD* board,
					 const char* s, 
					 TEBAN next) 
{
  int p, i, num;

  // 1が空白
  static const char* num_data[] = {
    "",  "一", "二", "三", "四", "五", "六", "七", "八", "九",
  };

  if (strstr(s, "なし") == s)
    return; // 持ち駒なし

  while (*s != '\0' && *s != 0xd && *s != 0xa) {
    if ( *s == ' ') {
      s++; continue;
    }
    if (strstr(s, "　") == s) {
      s += strlen("　"); continue;
    }

    for (p=1; p<=7; p++) {
      if (strncmp(kif_koma_data[p], s, strlen(kif_koma_data[p])) == 0)
	break;
    }
    if (7 < p) {
      /* 仕様上ありえない */
      throw RecordLoadError("inhand piece kind error");
    }
    s += strlen(kif_koma_data[p]);

    // 枚数
    if (*s == '\0' || *s == '\r' || *s == '\n') 
      num = 1;
    else if (strncmp("　", s, strlen("　")) == 0) {
      s += strlen("　");
      num = 1;
    } 
    else {
      num = 0;
      if (strncmp(s, "十", strlen("十")) == 0) {
	// 10～18枚
	s += strlen("十");
	num = 10;
      }

      for (i=1; i<=9; i++) {
	if (strncmp(num_data[i], s, strlen(num_data[i])) == 0) {
	  s += strlen(num_data[i]);
	  break;
	}
      }
      if (9 < i) {
	/* 仕様上ありえない */
	throw RecordLoadError("inhand too much");
      }
      num += i;
    }
    daemon_dboard_set_piece( board,next, p, num);
  }
}


/** KIF形式のファイルの盤面データを読み込む */
static void daemon_record_load_kif_board( BOARD* board, FileReader* reader) 
{
  char *s;
  int x, y, p;
  int next;

  for (y=1; (s=daemon_filereader_skip_comment(reader, '#')) != NULL && y<=9; 
       y++) {
    s++;
    for (x=9; 1 <= x; x--) {
      next = (*s == 'v') ? GOTE : SENTE;
      s++;
      if (strncmp("・", s, strlen("・")) == 0) {
	s += strlen("・");
	continue;
      }
      for (p=1; p<=0xF; p++) {
	if (strncmp(kif_koma_data[p], s, strlen(kif_koma_data[p])) == 0)
	  break;
      }
      if (0xF < p) {
	/* 仕様上ありえない */
	throw RecordLoadError("onboard piece kind error");
      }
      if (next == GOTE)
	p += 0x10;
      daemon_dboard_set_board( board, x, y, p);
      s += strlen(kif_koma_data[p]);
    }
  }  
}


static const char* TEBAN_MARKS[][3] = { 
  {"▲", NULL}, { "△", "▽", NULL }
};


/** 
 * KIF形式のファイルのヘッダー部分を読み込む.
 * @return エラー発生のときfalse 
 */
static bool daemon_record_load_kif_header(Record* record, FileReader* reader) 
{
  char* s;
  bool board_read = false;
  string name, value;

  while ((s = daemon_filereader_skip_comment(reader, '#')) != NULL) {
    const char* p;
    if ((p = strstr(s, "：")) != NULL) {
      name = string(s, p - s);
      value = string(p + strlen("："));
    }
    else
      name = "";

    if (name == "対局日" || name == "開始日時") {
      record->start_time = string_to_datetime(value);
      if (record->start_time.year < 0)
	throw RecordLoadError("invalid date");
    } 
    else if ( name == "表題" )
      record->title = value;
    else if ( name == "掲載" )
      record->source = value;
    else if (name == "手合割") {
      if (value == "平手")
	record->teai = D_HIRATE;
      else {
	throw RecordLoadError("not support except Hirate");
	// record->teai = HANDICAP_ETC;
      }
    } 
    else if (name == "後手の持駒" || name == "下手の持駒") {
      daemon_record_load_kif_piece( &record->first_board, 
				   s + name.length() + strlen("："), GOTE);
    }
    else if (strstr(s, "+---------------------------+") == s) {
      daemon_record_load_kif_board( &record->first_board, reader);
      board_read = true;
    }
    else if (name == "先手の持駒" || name == "上手の持駒") {
      daemon_record_load_kif_piece( &record->first_board, 
				   s + name.length() + strlen("："), SENTE);
    }
    else if (strstr(s, "先手番") == s)
      record->first_board.next = SENTE;
    else if (strstr(s, "後手番") == s)
      record->first_board.next = GOTE;
    else if (name == "先手") {
      char* p = s + strlen("先手：");
      daemon_filereader_return2zero(p);
      record->player_name[0] = p;
    } 
    else if (name == "後手") {
      char* p = s + strlen("後手：");
      daemon_filereader_return2zero(p);
      record->player_name[1] = p;
    } 
    else if ( *s == '*' ) {
      if ( record->game_comment == "" )
	record->game_comment = s + 1;
      else
	record->game_comment += string("\n") + (s + 1);
    }
    else if (strstr(s, "手数----指手---------消費時間--") == s)
      break;
    else {
      // KI2
      for (int j = 0; j < 2; j++) {
	for (int i = 0; TEBAN_MARKS[j][i]; i++) {
	  if (strstr(s, TEBAN_MARKS[j][i]) == s) {
	    daemon_filereader_push(reader, s);
	    goto end_header;
	  }
	}
      }
    }
  }  

end_header:
  if (!board_read)
    bo_reset_to_hirate(&record->first_board);
  else 
    boSetToBOARD(&record->first_board);

  return true;
}


struct Modifier {
  const char* v;
  XyDiff xydiff[7];
};

/** 
 * 先手が指すときの to に対する相対位置. ものすごい複雑 
 * http://www.shogi.or.jp/faq/kihuhyouki.html
 */
static const Modifier modif[] = {
  { "右", {-1, -17, +15, +31, 0} },
  { "左", {+1, -15, +17, +33, 0} },
  { "上", {+15, +16, +17, 0} },
  { "引", {-15, -16, -17, 0} },
  { "寄", {-1, +1, 0} },
  { "直", {+16, 0} },
  { "行", {+15, +16, +17, 0} },
  { NULL, {0} }
};

/** @return 読み込んだバイト数 */
static int parse_move( const char* s, DTe* te, const DTe& last_te, 
		       Record* record, const BOARD* bo )
{
  const char* const ss = s;
  int x, y;
  PieceKind p;

  if (strstr(s, "詰み") == s) {
    throw 0; // TODO:手じゃない
  } 

  if (strstr(s, "投了") == s) {
    te->special = DTe::RESIGN;
    return strlen("投了");
  } 
  else if (strstr(s, "中断") == s) {
    te->special = DTe::CHUDAN;
    return strlen("中断");
  }

  // 通常の指し手
  if (strstr(s, "同") == s) {
    // printf("last = %s\n", te_to_str(&last->te, bo).c_str() ); // DEBUG
    x = last_te.to & 0xf;
    y = (last_te.to >> 4);

    s += strlen("同");
    while ( *s == ' ' ) s++; 
    while ( strstr(s, "　") == s ) s += strlen("　");
  } 
  else {
    for (x = 1; x <= 9; x++) {
      if (strncmp(anum_data[x], s, strlen(anum_data[x])) == 0)
	break;
    }
    if (9 < x) {
      /* 仕様上ありえない */
      throw RecordLoadError("to-sq(x) of move error");
    }
    s += strlen(anum_data[x]);
    for (y = 1; y <= 9; y++) {
      if (strncmp(knum_data[y], s, strlen(knum_data[y])) == 0) 
	break;
    }
    if (9 < y) {
      /* 仕様上ありえない */
      throw RecordLoadError("to-sq(y) of move error");
    }
    s += strlen(knum_data[y]);
  }
  te->to = (y << 4) + x;

  for (p = 1; p <= 0xF; p++) {
    if ( *long_koma_data[p] != '\0' && 
	 strncmp(long_koma_data[p], s, strlen(long_koma_data[p])) == 0)
      break;
  }
  if ( 0xF < p ) {
    /* 仕様上ありえない */
    throw RecordLoadError("piece kind of move error");
  }
  s += strlen(long_koma_data[p]);

  // 候補を挙げて、削っていく
  MOVEINFO mi;
  int i;
  make_moves( bo, &mi );
  for ( i = 0; i < mi.count; ) {
    if ( te_to(mi.te[i]) != te->to ||
	 (te_is_put(mi.te[i]) ? te_pie(mi.te[i]) : 
                                (bo->board[te_from(mi.te[i])] & 0xf)) != p ) 
      miRemoveTE2(&mi, i);
    else 
      i++;
  }

  while ( !(*s == ' ' || !*s || *s == '\n' || *s == '\r' || *s == '(') ) {
    if ( strstr(s, "▲") == s || strstr(s, "△") == s || strstr(s, "▽") == s )
      break;

    if (strstr(s, "打") == s) {
      te->uti = p;
      for (i = 0; i < mi.count; ) { 
	if ( !te_is_put(mi.te[i]) )
	  miRemoveTE2(&mi, i);
	else 
	  i++;
      }
      s += strlen("打");
    } 
    else if (strstr(s, "不成") == s) {
      te->nari = false;
      for (i = 0; i < mi.count; ) { 
	if ( te_is_put(mi.te[i]) || te_promote(mi.te[i]) )
	  miRemoveTE2(&mi, i);
	else 
	  i++;
      }
      s += strlen("不成");
    }
    else if (strstr(s, "成") == s) {
      te->nari = true;
      for (i = 0; i < mi.count; ) { 
	if ( te_is_put(mi.te[i]) || !te_promote(mi.te[i]) )
	  miRemoveTE2(&mi, i);
	else 
	  i++;
      }
      s += strlen("成");
    } 
    else {
      bool hit = false;
      for (int j = 0; modif[j].v; j++) {
	if ( strstr(s, modif[j].v) != s )
	  continue;

	for ( int k = 0; k < mi.count; ) {
	  if ( te_is_put(mi.te[k]) ) {
	    miRemoveTE2(&mi, k);
	    continue;
	  }
	  for (int l = 0; modif[j].xydiff[l]; l++) {
	    if (bo->next == SENTE) {
	      XyDiff direc = bo->board[te_from(mi.te[k])] == KEI ?
		                 te_to(mi.te[k]) - te_from(mi.te[k]) :
		                 norm_direc(te_from(mi.te[k]), te_to(mi.te[k]));
	      if ( direc == -modif[j].xydiff[l] )
		goto found;
	    }
	    else {
	      XyDiff direc = bo->board[te_from(mi.te[k])] == KEI + 0x10 ?
                        	te_to(mi.te[k]) - te_from(mi.te[k]) :
                                norm_direc(te_from(mi.te[k]), te_to(mi.te[k]));
	      if ( direc == modif[j].xydiff[l] )
		goto found;
	    }
	  }
	  miRemoveTE2(&mi, k);
	  continue;
	found:
	  k++;
	}
	s += strlen(modif[j].v);
	hit = true;
      }

      if (!hit) {
#if 0 // NDEBUG
	printBOARD(bo);
	printf("unknown modifier: %s\n", ss);
	mi_print(&mi, *bo);
	assert(0);
#endif // !NDEBUG
	throw RecordLoadError("unknown modifier");
      }
    }
  }

  if ( *s == '(' ) {
    // fromを明記
    s++;
    if (!te->uti)
      te->fm = ((*(s + 1) - 0x30) << 4) + (*s - 0x30);

    s += 3;
  }
  else {
    if ( mi.count == 2 && te_is_put(mi.te[0]) && !te_is_put(mi.te[1]) )
      *te = DTe(DTe::NORMAL_MOVE, mi.te[1], 0);
    else if ( mi.count == 2 && !te_is_put(mi.te[0]) && te_is_put(mi.te[1]) )
      *te = DTe(DTe::NORMAL_MOVE, mi.te[0], 0);
    else {
      if ( mi.count != 1 ) {
#if 0 // NDEBUG
	printBOARD(bo);
	printf("ambiguous = %s\n", ss);
	mi_print(&mi, *bo);
	assert(0);
#endif // !NDEBUG
	throw RecordLoadError("ambiguous move");
      }
      *te = DTe( DTe::NORMAL_MOVE, mi.te[0], bo->board[te_to(mi.te[0])] );
    }
  }
  // printf("from = %x, to = %x, s = %s\n", te.fm, te.to, s); // DEBUG

  return s - ss;
}


/** 
 * 棋譜を読み込む.
 *
 * KIF形式::
 * <pre>
 *    1 ２六歩(27)   ( 0:03/00:00:03)
 *    2 ８四歩(83)   ( 0:12/00:00:12)
 * </pre>
 *
 * KI2形式::
 * <pre>
 * △３二金    ▲７八金    △１四歩    ▲４八銀
 * </pre>
 *
 * @param pos 次に読む手が何手目か
 */
static void daemon_record_load_kif_te(Record* record, FileReader* reader,
				      KyokumenNode* current, 
				      const DBoard* first_board,
			      int pos, KyokumenNode::Moves::iterator last ) 
{
  const char* s;
  list<int> alt_nodes;

  BOARD bo( *first_board );

  while ((s = daemon_filereader_next(reader)) != NULL) {
    DTe te;
    time_t t = 0;
    te.fm = 0; te.to = 0; te.nari = 0; te.uti = 0; te.tori = 0;
    te.special = DTe::NORMAL_MOVE;

    if (strstr(s, "まで") == s) {
      // printf("%d: line end.\n", __LINE__); // DEBUG

      // KI2では"投了"が出力されない
      // 出力されることもある？
      s += strlen("まで");
      while ( *s && *s >= '0' && *s <= '9' ) s++;

      DTe e;
      e.fm = 0; e.to = 0; e.nari = 0; e.uti = 0; e.tori = 0;
      if ( strstr(s, "手で先手の勝ち") == s ||
	   strstr(s, "手で後手の勝ち") == s ) {
	if ( !last->te.special ) {
	  e.special = DTe::RESIGN;
	  current->add_child( e, 0, "" );
	}
      }
      else if ( strstr(s, "手で持将棋") == s ) {
	if ( !last->te.special ) {
	  e.special = DTe::JISHOGI; // 持将棋で引き分け
	  current->add_child( e, 0, "" );
	}
      }
      else if ( strstr(s, "手で千日手") == s ) {
	if ( !last->te.special ) {
	  e.special = DTe::SENNICHITE;
	  current->add_child( e, 0, "" );
	}
      }
      else if ( strstr(s, "手で中断") == s ) {
      }
      else {
	// ほかに '手で後手の反則勝ち' など
#if 0
	si_abort("unknown ending: %s\n", s);
#endif // !NDEBUG
	throw RecordLoadError("unknown ending"); // TODO: impl.
      }

      // 変化がある？
      // KI2形式は読まないと分からない
      if ( !daemon_filereader_next(reader) ) // 空行
	break;

      if ( !(s = daemon_filereader_next(reader)) )
	break;
      if ( strstr(s, "変化：") != s)
	throw RecordLoadError("expect Henka");

      int n = -1;
      sscanf(s, "変化：%d", &n);
      if ( n <= 0 )
	throw RecordLoadError("missing alt count");
      if ( alt_nodes.size() > 0 ) {
	if ( n != alt_nodes.back() )
	  throw RecordLoadError("illegal alt count");
	alt_nodes.pop_back();
      }

      while (pos > n) {
	if ( !current->move_to_back->special )
	  boBack_mate( &bo );
	pos--;
	current = current->prev;
	last = current->prev->find_edge( *current->move_to_back );
	assert( last != current->prev->moves.end() );
	// printf("last = %s\n", te_to_str(&last->te, bo).c_str() );
      }
      // 再帰する
      daemon_record_load_kif_te(record, reader, current, &bo, pos, last);
      break;
    }
    else if (*s == '*') {
      // コメント. 複数行のことがある
      if (last->te_comment == "") {
	if ( string(s + 1).find("[?]") == 0 ) {
	  last->annotation = ANNOT_POOR;
	  last->te_comment = s + 4;
	}
	else if ( string(s + 1).find("[?!]") == 0 ) {
	  last->annotation = ANNOT_QUESTIONABLE;
	  last->te_comment = s + 5;
	}
	else
	  last->te_comment = s + 1;
      }
      else
	last->te_comment = last->te_comment + "\n" + (s + 1);
      continue;
    }
    else if (*s == '&') {
      // 取り込まない
      continue;
    }

    // 指し手
    while ( *s == ' ' ) s++;

    int te_cnt = -1;
    if ( sscanf(s, "%d", &te_cnt) == 1 ) {
      // KIF
      if ( te_cnt != pos )
	throw RecordLoadError("illegal move count");
      while ( *s >= '0' && *s <= '9' ) s++;
      while ( *s == ' ' ) s++;
      
      s += parse_move( s, &te, last->te, record, &bo );
      while ( *s == ' ' ) s++;

      int min, sec;
      sscanf(s, "(%d:%d/%*d:%*d:%*d)", &min, &sec );
      t = min * 60 + sec;
      s += 16;

      if ( *s == '+') {
	alt_nodes.push_back(pos);
	s++;
      }

      if ( !te.special || te.special == DTe::RESIGN ) {
	te.tori = bo.board[te.to];
	last = current->add_child(te, t, "");
	current = last->node;

	if ( !te.special )
	  boMove_mate( &bo, te.pack() );
	pos++;
      }
    }
    else {
      // KI2
      while ( !(!*s || *s == '\r' || *s == '\n') ) {
	int i, j;
	int teban = -1;
	for (j = 0; j < 2; j++) {
	  for (i = 0; TEBAN_MARKS[j][i]; i++) {
	    if ( strstr(s, TEBAN_MARKS[j][i]) == s ) {
	      teban = j;
	      goto found;
	    }
	  }
	}
      found:
	if ( teban != ((pos - 1) % 2) )
	  throw RecordLoadError("illegal side");
	s += strlen( TEBAN_MARKS[j][i] );
	s += parse_move( s, &te, last->te, record, &bo );

	if ( !te.special || te.special == DTe::RESIGN ) {
	  te.tori = bo.board[te.to];
	  last = current->add_child(te, 0, "");
	  current = last->node;

	  if ( !te.special )
	    boMove_mate( &bo, te.pack() );
	  pos++;
	}

	while ( *s == ' ' ) s++;
      }
    }
  } // of while
}


/** KIF形式のファイルを読み込んで Record を生成して返す。 */
Record* daemon_record_load_kif( const char* filename )
{
  return daemon_record_load_kif_code( filename, "CP932" );
}


/** 
 * KIF形式のファイルを読み込んで Record を生成して返す。
 * @return 生成したRecord. エラーの場合でも生成して返す
 */
Record* daemon_record_load_kif_code( const char* filename, 
				     const char* encoding )
{
  assert(filename != NULL);
  assert(encoding != NULL);

  FileReader* reader = daemon_filereader_new();
  daemon_filereader_set_incode(reader, encoding);
  daemon_filereader_open(reader, filename);

  Record* record = daemon_record_new();

  if (daemon_filereader_get_stat(reader) == D_FILEREADER_STAT_ERROR) {
    /* 読み込みに失敗した */
    daemon_filereader_free(reader);

    record->set_error("failed to open file");
    return record;
  }

  try {
    daemon_record_load_kif_header(record, reader);
    daemon_record_load_kif_te(record, reader, record->mi, 
			    &record->first_board, 1, record->mi->moves.end() );
  }
  catch (RecordLoadError& e) {
    record->set_error( e.what() ); // TODO: lineno
    return record;
  }

  // 成功
  daemon_filereader_close(reader);
  daemon_filereader_free(reader);

  record->filename = filename;
  record->filetype = Record::D_FORMAT_KIF;
  record->loadstat = D_LOAD_SUCCESS;
  record->changed = false;

  return record;
}
