/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <config.h>
#include <tchdb.h>
#include <dirent.h>
#include "record.h"
#include <string>
using namespace std;


/** 
 * 定跡ファイルをなめる
 */
template <typename U>
static void update_book( const BOARD* first_board, 
			 int te_cnt,
			 const KyokumenNode* node, 
			 const U& updater )
{
  BOARD* bo = bo_dup(first_board);

  for ( ; node && node->moves.size() > 0; te_cnt++ ) {
    // 定跡ファイルに登録
    KyokumenNode::Moves::size_type i;
    for ( i = 0; i < node->moves.size(); i++ ) {
      const MoveEdge& mv = node->moves[i];

      if ( mv.te.special ) // 投了とか
	continue;

      updater( bo, te_cnt, mv );
    }

    // 分岐. 主ライン以外のみ再帰する
    for ( i = 1; i < node->moves.size(); i++ ) {
      if ( node->moves[i].te.special )
	continue;
      boMove_mate( bo, node->moves[i].te.pack() );
      update_book( bo, te_cnt + 1, node->moves[i].node, updater );
      boBack_mate( bo );
    }

    if ( node->moves[0].te.special )
      break;
    // 主ライン
    boMove_mate( bo, node->moves[0].te.pack() );
    node = node->moves[0].node;
  }

  freeBOARD( bo );
}


/** 
 * 主ラインの勝者を調べる
 * @return 先手 0, 後手 0, 引き分け -1
 */
static int get_winner( const Record* record )
{
  int winner = -1;
  int te_count;

  KyokumenNode* mi = record->mi;
  for ( te_count = 0; mi && mi->moves.size() > 0; te_count++ ) {
    if ( mi->moves[0].te.special ) {
      if (mi->moves[0].te.special == DTe::RESIGN ) {
	winner = (te_count + 1) % 2;
	printf("te_count = %d, winner = %d\n", te_count, winner); // DEBUG

      }
      break;
    }
    mi = mi->lookup_child( mi->moves[0].te );
  }

  return winner;
}


template <typename U>
static bool update_all( const char* dirname, U updater )
{
  DIR* d = opendir( dirname );
  if ( !d ) {
    printf("opendir() error.\n");
    return false;
  }

  int i = 0;
  const struct dirent* e;
  while ( (e = readdir(d)) != NULL ) {
    if (string(e->d_name) == "." || string(e->d_name) == "..")
      continue;

    i++;
    printf("file: %d: %s\n", i, e->d_name);

    Record* record = daemon_record_load( 
                            (string(dirname) + "/" + e->d_name).c_str() );
    if ( record->loadstat != D_LOAD_SUCCESS ) {
      printf( "load error: %s\n", record->strerror.c_str() );
      daemon_record_free(record);
      continue;
    }

    updater.set_winner( get_winner(record) );
    update_book( &record->first_board, 0, record->mi, updater );
    daemon_record_free(record);
  }

  closedir( d );
  return true;
}


/**
 * Tier2: 手の良し悪しが検討されてないもの. 悪手を含むことがある
 */
struct Tier2 
{
  int winner;

  void set_winner(int w) { winner = w; }

  /**
   * @param bo     現在の局面
   * @param te_cnt 手数. 初手 = 0
   * @param mv     登録すべき手
   */
  void operator () ( const BOARD* bo, int te_cnt, const MoveEdge& mv ) const 
  {
    // 引き分けは両方登録する
    if ( winner >= 0 && winner != (te_cnt % 2) )
      return;

    int count = 0;
    BookEnt* ent = book_get( bo->hash_all, &count );

    bool found = false;
    for (int i = 0; i < count; i++) {
      if ( !teCmpTE(ent[i].book_move, mv.te.pack()) ) {
	found = true;
	break;
      }
    }

    if ( !found ) {
      // 追加する
      count++;
      ent = static_cast<BookEnt*>(realloc(ent, sizeof(BookEnt) * count) );
      ent[count - 1].book_move = mv.te.pack();
      ent[count - 1].weight = 1;

      // printf("%d %s, ", te_cnt, te_str(mv.te.pack(), *bo).c_str() ); // DEBUG
      book_put( bo->hash_all, ent, count );
    }

    free(ent);
  }
};


/**
 * Tier1: 悪手にはフラグが付いている, または定跡として編集されたもの
 */
struct Tier1 {
  void operator () ( const BOARD* bo, BookEnt* ent, int count, 
		     const MoveEdge& mv ) const
  {
    BookEnt* new_ent = static_cast<BookEnt*>( 
			     malloc(sizeof(BookEnt) * (count + 1)) );
    int new_count = 0;

    bool to_update = false;
    for (int i = 0; i < count ; i++) {
      if ( !teCmpTE(ent[i].book_move, mv.te.pack() ) ) {
	if ( ent[i].weight > 10 ) {
	  // 登録済み
	  free(ent);
	  free(new_ent);
	  return;
	}

	new_ent[new_count] = ent[i];
	new_ent[new_count++].weight = 11; // magic
	to_update = true;
      }
      else {
	// tier2の手は削除
	if ( ent[i].weight > 10 ) {
	}
      }
    }

    free(ent);
  }
};


int main() 
{
  book_init( "/tmp/daemonshogi-book.bin", true );

  update_all("/opt/src/shogi-book/tier2", Tier2() ); // TODO: 場所
  // update_all("/opt/src/shogi-book/tier1", Tier1() );

  book_free();

  return 0;
}

