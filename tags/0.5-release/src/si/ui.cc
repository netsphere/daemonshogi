/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "si.h"
#include "si-think.h"
#include "ui.h"


static void (*pending_function)( bool may_block ) = NULL;


#ifdef USE_PIECE_NO
void putPBOX(PBOX *pb) {
  fputPBOX(stdout, pb);
}


void fputPBOX(FILE *out, PBOX *pb) {
  static const char* name[] = {
    "dummy", "fu", "kyo", "kei", "gin", "kin", "kaku", "hisya", "oh",
  };
  int i, j;
  
#ifdef DEBUG
  assert( out != NULL );
  assert( pb != NULL );
#endif /* DEBUG */

  for ( i=1; i<9; i++ ) {
    fprintf( out, "%-5s : %2d   ", name[i], pb->count[i] );
    for ( j=0; j<pb->count[i]; j++ ) {
      fprintf( out, "%2d, ", pb->number[i][j] );
    }
    fprintf( out, "\n" );
  }
}
#endif // 0


/**
 * ペンディングされたイベントを処理する関数へのポインタをセットする。
 * @param f ペンディングされたイベントを処理する関数へのポインタ
 */
void set_pending_function( void (*f)(bool may_block) ) 
{
  pending_function = f;
}

/**
 * GUI のペンディングされたイベントを処理する。
 */
void processing_pending( bool may_block ) 
{
  if ( pending_function == NULL )
    return;
  
  (*pending_function)( may_block );
}

/* -------------------------------------------------------- */
/* 手の入力関数 */
/* -------------------------------------------------------- */


extern GAME g_game;

/**
 * プログラムの一手を te に格納する。
 * @param bo_ 対象のBOARD
 * @param best_move 指し手 (出力)
 *
 * @return GAMESTAT
 */
INPUTSTATUS si_input_next_computer( const BOARD* bo_, 
				    DTe* best_move, int* /*dmy */ )
{
  INPUTSTATUS ret;
  TE te_ret;
#ifndef NDEBUG
  int side = bo_->next;
  uint64_t hash = bo_->hash_all;
#endif

  BOARD bo = BOARD(*bo_);

  if ( jouseki(&bo, &te_ret) ) {
    // 定跡があった
    *best_move = DTe( DTe::NORMAL_MOVE, te_ret, bo.board[te_to(te_ret)] );
    return SI_NORMAL;
  }

  // 詰めろを掛けられているか
  if ( bo.mseq.count >= 1 && !bo_is_checked(&bo, bo.next) ) {
    bo_pass(&bo);
    int node_limit = GAME_MATE_NODE_LIMIT;
    if ( dfpn_mate(&bo, 31, &te_ret, &node_limit) == CHECK_MATE ) {
      // 詰めろ
      te_add_hint(&bo.mseq.te[bo.mseq.count - 2], TE_TSUMERO);
    }
    boBack_mate(&bo);
  }

  // GUIで中断, 時間切れ?
  if ( gmGetGameStatus(&g_game) != 0 )
    return gmGetGameStatus(&g_game);

  // 通常探索
  ret = bfs_minmax(&bo, 23, &te_ret);
  if ( ret == SI_NORMAL ) {
    if ( te_to(te_ret) )
      *best_move = DTe( DTe::NORMAL_MOVE, te_ret, bo.board[te_to(te_ret)] );
    else
      *best_move = DTe( DTe::RESIGN, 0, 0 );
  }

  assert(side == bo.next);
  assert(hash == bo.hash_all);
  return ret;
}
