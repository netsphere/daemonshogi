/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <algorithm>
#include "si.h"
#include "si-think.h"
#include "ui.h"
using namespace std;


/**
 * 盤面の駒の種類位置、持駒の数から盤面評価点数を計算して返す。
 * @param bo 対象のBOARD
 * @param next 計算する手番. boの手番と同じとは限らない
 * @return 盤面評価点数
 */
int grade(const BOARD* bo, int next)
{
  int point;
  int i, j;

  point = 0;

  // 盤上の駒
  for (i = 1; i <= 81; i++) {
    PieceKind const p = boGetPiece( bo, arr_xy[i] );
    if ( p && getTeban(p) == next )
      point += add_grade( bo, arr_xy[i], next );
  }

  // 持ち駒
  for (i = 1; i <= 7; i++) {
    for (j = 0; j < bo->inhand[next][i]; j++)
      point += score_inhand(bo, i, next);
  }

  /* 王の安全度の評価 */
  // point -= add_oh_safe_grade(bo, next);

  return point;
}


/**
 * xy の位置の駒の点数
 * @param bo   対象のBOARD
 * @param xy   対象の駒の位置
 * @param next bo->board[xy] のプレイヤーと同じ
 */
int add_grade( const BOARD* bo, Xy xy, int next )
{
  assert( xy >= 0x11 && xy <= 0x99 );
  assert( bo->board[xy] != 0 );
  assert( bo->board[xy] != WALL );
#ifndef NDEBUG
  if ( getTeban(bo->board[xy]) != next ) {
    printBOARD( bo );
    printf("xy = %x, next = %d\n", xy, next );
    abort();
  }
#endif // !NDEBUG

  int kx, ky;
  int jx, jy;
  int x, y;
  // int piece_p; 
  int score;
  
  /* 自玉の位置 */
  Xy const king_xy = bo->king_xy[next];
  kx = king_xy & 0xF;
  ky = king_xy >> 4;

  /* 相手玉の位置 */
  Xy oppo_king_xy = bo->king_xy[1 - next];
  jx = oppo_king_xy & 0xF;
  jy = oppo_king_xy >> 4;
  if ( !king_xy || !oppo_king_xy )
    return 0;

  x = xy & 0xF;
  y = xy >> 4;

  PieceKind const p = boGetPiece(bo, xy) & 0xf;
  
  if ( p == OH )
    return 0; // 玉は別途評価
    // return g_npc.king_xy_points[ rotate_if(xy, next) ] * 100;

  if ( is_long_piece(p) ) {
    /* 飛車、角の場合は位置によるポイント増減はしない */
    score = g_npc.base_piece_points[ p ] * 100;
  }
  else {
    int v, w;

    // 自玉との距離
    v = abs(kx - x);
    w = ( next == SENTE ) ? ky - y : y - ky;
    score = g_npc.base_piece_points[p] * g_npc.defense_relative_mod[8 + w][v];

    // 相手玉との距離
    v = abs(jx - x);
    w = ( next == SENTE ) ? y - jy : jy - y;
    score = score * g_npc.offense_relative_mod[8 + w][v] / 100;

    if ( p == FU )
      score += g_npc.fu_position_bonus * (next == SENTE ? 9 - y : y - 1);
  }

#if 0
  if ( (p & 0x7) == HISHA ) {
    /* 「王飛接近するべからず」の格言通り王と飛車が近い場合は減点する */
    int hd = max( abs(kx - x), abs(ky - y) );
    if ( hd <= 2 ) {
      score -= (3 - hd) * OH_HISYA_POINT;
    }
  }
#endif // 0

  /* 駒の動ける数を評価に加える */
  // movables は boMove() で正しく更新できないので、think_eval() で計算する
  // score += bo_piece_kiki_count(bo, n) * NUM_OF_MOVE_POINT;

  return score;
}


/**
 * 持ち駒 piece 1枚の得点。
 * 増やすときはboを変更した後で呼び出すこと。減らすときはboを変更する前。
 * \todo 歩切れの評価など
 *
 * @param bo 対象のBOARD
 * @param piece 取った駒
 * @param next 盤面評価点数を増減する手番. boの手番と同じとは限らない
 */
int score_inhand(const BOARD* bo, PieceKind piece, int next ) 
{
  assert( 1 <= piece && piece <= 7 );

  return g_npc.inhand_points[piece] * 100;
}
