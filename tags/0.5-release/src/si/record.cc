/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <list>
#include "dte.h"
#include "movetree.h"
#include "board-misc.h"
#include "record.h"
#include "filereader.h"
#include "filewriter.h"
// #include "misc.h"
using namespace std;


/** Record を初期化 */
static void daemon_record_init(Record* record) 
{
  assert(record != NULL);

  memset( &record->start_time, 0, sizeof(record->start_time) );
  record->title = "";
  record->source = "";
  record->player_name[0] = "";
  record->player_name[1] = "";
  record->game_comment = "";
  record->teai = HANDICAP_ETC; // 不明
  bo_init( &record->first_board );

  if (record->mi)
    delete record->mi;
  record->mi = new KyokumenNode(NULL, NULL);

  record->filename = "";
  record->filetype = Record::D_FORMAT_UNKNOWN;
  record->set_error("not open");

  record->changed = false;
}


Record::Record(): mi(NULL)
{
  daemon_record_init(this);
}


/**
 * コンストラクタ.
 */
Record* daemon_record_new() 
{
  Record* record = new Record();
  if (record == NULL) {
    printf("No enough memory in daemon_record_new().");
    abort();
  }

  return record;
}


/** デストラクタ */
void daemon_record_free(Record* record) 
{
  if (record)
    delete record;
}


/** ファイル filename のフォーマットを調査して返す。 */
static Record::FileType daemon_record_research_format(const char* filename) 
{
  char *s;
  FileReader *reader;
  Record::FileType format;
  int i;
  
  assert(filename != NULL);

  reader = daemon_filereader_new();
  if (reader == NULL) {
    return Record::D_FORMAT_ERROR;
  }
  if (daemon_filereader_open(reader, filename) != 0) {
    return Record::D_FORMAT_ERROR;
  }
  if (reader == NULL) {
    printf("No enough memory in daemon_record_reserach_format().\n");
    abort();
  }

  format = Record::D_FORMAT_UNKNOWN;

  for (i=0; i<256 && (s=daemon_filereader_next(reader)) != NULL; i++) {
    if (strncmp(s, "N+", 2) == 0 || strncmp(s, "P1", 2) == 0) {
      format = Record::D_FORMAT_CSA;
      break;
    } 
    else if ( strstr(s, "先手：") == s || strstr(s, "開始日時：") == s || 
	      strstr(s, "終了日時：") == s || strstr(s, "対局日：") == s ) {
      format = Record::D_FORMAT_KIF;
      break;
    } 
  }

  daemon_filereader_close(reader);
  daemon_filereader_free(reader);

  return format;
}


Record* daemon_record_load_code( const char* filename, const char* encoding )
{
  Record* record = NULL;

  Record::FileType recformat;
  recformat = daemon_record_research_format(filename);
  switch (recformat)
  {
  case Record::D_FORMAT_CSA:
    return daemon_record_load_csa_code( filename, encoding );
  case Record::D_FORMAT_KIF:
    return daemon_record_load_kif_code( filename, encoding );
  case Record::D_FORMAT_ERROR:
    record = daemon_record_new();
    record->set_error("error when open file");
    return record;
  case Record::D_FORMAT_UNKNOWN:
    record = daemon_record_new();
    record->set_error("unknown format");
    return record;
  default:
    abort();
  }
}


/**
 * 棋譜ファイル filename を読み込んで Record を生成する。
 * ファイル形式は自動で認識する。
 * @return 生成した Record. エラーが発生したときも生成して返す.
 */
Record* daemon_record_load( const char* filename )
{
  assert(filename != NULL);
  return daemon_record_load_code( filename, "CP932" );
}


D_LOADSTAT daemon_record_get_loadstat(const Record* record) 
{
  assert(record != NULL);
  return record->loadstat;
}


string daemon_record_get_player(const Record* record,
			      TEBAN next) {
  return record->player_name[next];
}


void daemon_record_set_player(Record* record,
			      TEBAN next,
			      const char* s) 
{
  assert(record != NULL);
  assert(next == SENTE || next == GOTE);
  assert(s != NULL);

  record->player_name[next] = s;
}


/** 日付または時刻を文字列にする. 分まで */
string get_datetime_string(const DateTime& t)
{
  if (t.year == 0)
    return "";

  char buf[1000];
  if (t.hour == 0 && t.minute == 0 && t.second == 0)
    sprintf( buf, "%04d/%02d/%02d", t.year, t.month, t.day );
  else {
    sprintf( buf, "%04d/%02d/%02d %02d:%02d", 
	     t.year, t.month, t.day, t.hour, t.minute );
  }

  return buf;
}


/** @return 適切ではないとき year = -1 */
DateTime string_to_datetime(const string& s)
{
  DateTime r;
  memset(&r, 0, sizeof(r));

  if ( s == "" )
    return r;

  int ssr = sscanf( s.c_str(), "%d/%d/%d %d:%d:%d", 
                  &r.year, &r.month, &r.day, &r.hour, &r.minute, &r.second );
  printf("sscanf = %d\n", ssr); // DEBUG
  if (ssr != 3 && ssr != 5 && ssr != 6) {
    r.year = -1;
    return r;
  }

  return r;
}
