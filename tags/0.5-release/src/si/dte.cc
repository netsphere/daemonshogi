/* -*- coding:utf-8 -*-
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "dte.h"
#include "board-misc.h"
#include "si.h"
using namespace std;


DTe* daemon_dte_new(void) {
  DTe* te;

  te = (DTe*) malloc(sizeof(DTe));
  if (te == NULL) {
    printf("No enough memory in daemon_dte_new().");
    abort();
  }

  return te;
}

void daemon_dte_free(DTe* te) {
  assert(te != NULL);
  free(te);
}


/** デバッグ用の表現 */
void daemon_dte_output(const DTe* te, FILE* out) 
{
  assert(te != NULL);
  assert(out != NULL);

  if ( !te->fm ) {
    fprintf(out, "(%d,%d) %d uti\n", te->to & 0xF, te->to >> 4, te->uti);
  } 
  else {
    fprintf(out, "(%d,%d)->(%d,%d)%s\n",
	    te->fm & 0xF, te->fm >> 4,
	    te->to & 0xF, te->to >> 4,
	    te->nari ? " nari" : "");
  }
}


extern const char* CSA_PIECE_NAME[];

/** 局面を利用した文字列化. 主にデバッグ用 */
string DTe::to_str( const BOARD& bo )
{
  if ( special ) {
    switch (special) {
    case RESIGN:
      return "%TORYO";
    case CHUDAN:
      return "#CHUDAN";
    case KACHI:
      return "%KACHI";
    case JISHOGI:
      return "#JISHOGI";
    case ILLEGAL:
      return "#ILLEGAL_MOVE";
    case TIME_OUT:
      return "#TIME_UP";
    case SENNICHITE:
      return "#SENNICHITE";
    default:
      abort();
    }
  }

  // 通常の指し手
  char buf[100];
  if ( !fm ) {
    sprintf(buf, "%d%d%s*",
	    to & 0xf, to >> 4, CSA_PIECE_NAME[uti]);
  }
  else {
    sprintf(buf, "%d%d%s%s(%d%d)",
	    to & 0xf, to >> 4,
	    CSA_PIECE_NAME[bo.board[fm] & 0xf],
	    (nari ? "+" : ""),
	    fm & 0xf, fm >> 4);
  }

  return buf;
}
