/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Sprite class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/sprite.c,v $
 * $Id: sprite.c,v 1.1.1.1 2005/12/09 09:03:07 tokita Exp $
 */

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "sprite.h"
#include "misc.h"
#include <si/si.h>


static gint src_xy[][2] = {
    { -1,  -1}, /* なし */
    {  0,   0}, /* 先手歩   0x01 */
    { 40,   0}, /* 先手香車 0x02 */
    { 80,   0}, /* 先手桂馬 0x03 */
    {120,   0}, /* 先手銀   0x04 */
    {160,   0}, /* 先手金   0x05 */
    {200,   0}, /* 先手角   0x06 */
    {240,   0}, /* 先手飛車 0x07 */
    {280,   0}, /* 先手王   0x08 */
    {  0,  40}, /* 先手と   0x09 */
    { 40,  40}, /* 先手成香 0x0A */
    { 80,  40}, /* 先手成桂 0x0B */
    {120,  40}, /* 先手成銀 0x0C */
    {280,  40}, /* 先手玉   0x0D */
    {200,  40}, /* 先手馬   0x0E */
    {240,  40}, /* 先手竜   0x0F */
    { -1,  -1}, /* 先手竜   0x10 */
    {  0,  80}, /* 後手歩   0x11 */
    { 40,  80}, /* 後手香車 0x12 */
    { 80,  80}, /* 後手桂馬 0x13 */
    {120,  80}, /* 後手銀   0x14 */
    {160,  80}, /* 後手金   0x15 */
    {200,  80}, /* 後手角   0x16 */
    {240,  80}, /* 後手飛車 0x17 */
    {280,  80}, /* 後手王   0x18 */
    {  0, 120}, /* 後手と   0x19 */
    { 40, 120}, /* 後手成香 0x1A */
    { 80, 120}, /* 後手成桂 0x1B */
    {120, 120}, /* 後手成銀 0x1C */
    {280, 120}, /* 後手玉   0x1D */
    {200, 120}, /* 後手馬   0x1E */
    {240, 120}, /* 後手竜   0x1F */
};


/**
 * Sprite を生成してそのポインタを返す。
 * @param pixmap 元画像
 * @param mask 透過マスク
 * @param src_x 元画像でのX座標
 * @param src_y 元画像でのY座標
 * @param width  スプライトの横サイズ
 * @param height スプライトの縦サイズ
 */
Sprite* daemon_sprite_new( GdkPixmap* pixmap,
                           GdkBitmap* mask,
                           int src_x, int src_y,
                           gint width,
                           gint height )
{
  Sprite* sprite;

  sprite = (Sprite*)malloc(sizeof(Sprite));
  if (sprite == NULL) {
    si_abort("No enough memory in daemon_sprite_new()");
  }

  daemon_sprite_set_pixmap( sprite, pixmap, mask, src_x, src_y, width, height );

  sprite->x       = 0;
  sprite->y       = 0;
  sprite->visible = FALSE;
  sprite->koma    = 0;

  return sprite;
}


/**
 * widthを返す。
 * @param sprite 対象のsprite
 * @return width
 */
gint daemon_sprite_get_width(const Sprite* sprite)
{
  return sprite->width;
}


/**
 * heightを返す。
 * @param sprite 対象のsprite
 * @return height
 */
gint daemon_sprite_get_height(const Sprite* sprite)
{
  return sprite->height;
}


/**
 * x座標を返す。
 * @param sprite 対象のsprite
 * @return x
 */
gint daemon_sprite_get_x(const Sprite* sprite)
{
  return sprite->x;
}


/**
 * y座標を返す。
 * @param sprite 対象のsprite
 * @return y
 */
gint daemon_sprite_get_y(const Sprite* sprite)
{
  return sprite->y;
}


/**
 * src_xを返す。
 * @param sprite 対象のsprite
 * @return x
 */
static gint daemon_sprite_get_src_x(Sprite* sprite) 
{
  gint koma;

  g_assert(sprite != NULL);
  
  if (sprite->type == KOMA_SPRITE) {
    koma = daemon_sprite_get_koma(sprite);
    g_assert(1 <= koma && koma <= 0x1F);
    return src_xy[koma][0] * sprite->width / 40;
  } else {
    return 0;
  }
}


/**
 * src_yを返す。
 * @param sprite 対象のsprite
 * @return y
 */
static gint daemon_sprite_get_src_y(Sprite* sprite) 
{
  gint koma;
  
  g_assert(sprite != NULL);
  
  if (sprite->type == KOMA_SPRITE) {
    koma = daemon_sprite_get_koma(sprite);
    g_assert(1 <= koma && koma <= 0x1F);
    return src_xy[koma][1] * sprite->height / 40;
  } else {
    return 0;
  }
}


/**
 * 座標をセットする。
 * @param sprite 対象のsprite
 * @param x   x座標 (ドット単位)
 * @param y   y座標
 */
void daemon_sprite_set_xy( Sprite* sprite, int x, int y ) 
{
  sprite->x = x;
  sprite->y = y;
}


/**
 * 元画像をセットしなおす.
 * @param sprite 対象のsprite
 * @param pixmap セットするpixmap
 * @param mask   透過マスク
 * @param src_x  元画像のX座標
 * @param src_y  元画像のY座標
 * @param width  スプライトの幅
 * @param height スプライトの高さ
 */
void daemon_sprite_set_pixmap( Sprite* sprite,
                               GdkPixmap* pixmap, GdkBitmap* mask,
                               int src_x, int src_y, int width, int height )
{
  assert(sprite);
  assert(pixmap);
  assert(mask);

  sprite->pixmap = pixmap;
  sprite->mask    = mask;
  sprite->src_x   = src_x;
  sprite->src_y   = src_y;
  sprite->width   = width;
  sprite->height  = height;
}


/**
 * pixmapを返す。
 * @param sprite 対象のsprite
 * @return pixmap
 */
GdkPixmap* daemon_sprite_get_pixmap(Sprite* sprite) {
  return sprite->pixmap;
}

/**
 * 表示するかのフラグを返す。TRUEならば表示する。
 * @param sprite 対象のsprite
 * @return 表示するかのフラグ
 */
gboolean daemon_sprite_isvisible(Sprite* sprite) {
  return sprite->visible;
}

/**
 * 表示するかのフラグをセットする。
 * @param sprite 対象のsprite
 * @param bool_ 表示するかのフラグ
 */
void daemon_sprite_set_visible(Sprite* sprite, gboolean bool_) 
{
  sprite->visible = bool_;
}


/** 駒種類をセットする */
void daemon_sprite_set_koma(Sprite* sprite, gint koma) {
  g_assert(sprite != NULL);
  g_assert(1 <= koma && koma <= 0x1F);
  sprite->koma = koma;
}

/** 駒種類を返す */
gint daemon_sprite_get_koma(Sprite* sprite) {
  g_assert(sprite != NULL);
  return sprite->koma;
}

/** スプライトの種類を返す */
SPRITE_TYPE daemon_sprite_get_type(Sprite* sprite) {
  return sprite->type;
}

/** スプライトの種類をセットする */
void daemon_sprite_set_type(Sprite* sprite, SPRITE_TYPE type) {
  sprite->type = type;
}

/** 移動できるかのフラグを返す。*/
gboolean daemon_sprite_ismove(Sprite* sprite) {
  return sprite->move;
}

/** 移動できるかのフラグをセットする */
void daemon_sprite_set_move(Sprite* sprite, gboolean move) {
  sprite->move = move;
}

/** ドラッグ前のX位置をセットする */
void daemon_sprite_set_from_x(Sprite* sprite, gint x) {
  sprite->from_x = x;
}

/** ドラッグ前のY位置をセットする */
void daemon_sprite_set_from_y(Sprite* sprite, gint y) {
  sprite->from_y = y;
}

/** ドラッグ前のX位置を返す */
gint daemon_sprite_get_from_x(Sprite* sprite) {
  return sprite->from_x;
}

/** ドラッグ前のY位置を返す */
gint daemon_sprite_get_from_y(Sprite* sprite) {
  return sprite->from_y;
}


/** スプライトを描画する */
void daemon_sprite_draw(Sprite* sprite, GdkPixmap* drawable, GdkGC* gc)
{
  int src_x, src_y;

  assert(sprite);
  assert(drawable);
  assert(gc);

  if (!sprite->visible)
    return;

  src_x = daemon_sprite_get_src_x(sprite);
  src_y = daemon_sprite_get_src_y(sprite);

  if (sprite->type == KOMA_SPRITE) {
    // 駒の場合
    gdk_gc_set_clip_mask(gc, sprite->mask);
    gdk_gc_set_clip_origin(gc, sprite->x - src_x, sprite->y - src_y);
  }
  else {
    // 駒以外
    GdkRectangle rect;

    rect.x = 0;
    rect.y = 0;
    rect.width = sprite->width;
    rect.height = sprite->height;

    gdk_gc_set_clip_rectangle(gc, &rect);
    gdk_gc_set_clip_origin(gc, sprite->x, sprite->y);
  }

  gdk_draw_drawable(drawable, gc, sprite->pixmap, src_x, src_y, 
		    sprite->x, sprite->y, sprite->width, sprite->height);
}
