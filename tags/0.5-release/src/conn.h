/* -*- mode:c++ -*-
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// TCP/IP library.


#ifndef DAEMON_CONN_H
#define DAEMON_CONN_H

#include "si/si.h"
#include "si/board-misc.h"
#include "misc.h"
#include <list>
#include <string>


void network_log_append_message(const char* format, ...);

// int connect_to_server(const char* hostname, const char* service);
void disconnect_server();

// int csa_send_login(const char* username, const char* password);
int csa_send_move(const DBoard* board, int is_sente, const DTe& te);
//int csa_send_toryo();

int csa_send_chudan_request();

INPUTSTATUS csa_wait_for_result( DTe* te, int* elapsed_time );

INPUTSTATUS daemon_input_next_network_impl( const BOARD* bo, 
					    DTe* te, int* elapsed_time );


/** スレッド間でやりとりするデータ */
struct ConnectThreadArg
{
  std::string hostname;
  std::string port;
  std::string username;
  std::string password;
  PLAYERTYPE player_type;

  /** ワーカー -> 主スレッド */
  std::list<std::string> status_queue;

  /** 結果. +1で正常終了、-1でエラー */
  int result;
};

void* csa_connect_thread_main(void* arg);
void csa_login_and_wait_socket(const char* username, const char* password);

void csa_send_logout();


/** CSAサーバから受信するゲームデータ */
struct NetGameInfo
{
  std::string game_id;
  std::string player_name[2];
  char my_turn;
  int total_time;
};


#endif
