/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


extern GtkBuilder* interface_builder;


static inline GtkWidget* create_bookwindow() {
  return GTK_WIDGET( gtk_builder_get_object(interface_builder, "history_window") );
}

static inline GtkWidget* create_dialog_mate() {
  return GTK_WIDGET( gtk_builder_get_object(interface_builder, 
					    "dialog_mate") );
}

static inline GtkWidget* create_dialog_game() {
  return GTK_WIDGET( gtk_builder_get_object(interface_builder, 
					    "dialog_game") );
}

static inline GtkWidget* create_remote_dialog() {
  return GTK_WIDGET( gtk_builder_get_object(interface_builder, 
					    "remote_dialog") );
}

static inline GtkWidget* create_about_dialog() {
  return GTK_WIDGET( gtk_builder_get_object(interface_builder, 
					    "about_dialog") );
}

static inline GtkWidget* create_connecting_dialog() {
  return GTK_WIDGET( gtk_builder_get_object(interface_builder, 
					    "connecting_dialog") );
}

static inline GtkWidget* create_network_game_dialog() {
  return GTK_WIDGET( gtk_builder_get_object(interface_builder, 
					    "network_game_dialog") );
}

static inline GtkWidget* create_alt_moves_window() {
  return GTK_WIDGET( gtk_builder_get_object(interface_builder, 
					    "alt_moves_window") );
}

static inline GtkWidget* create_calendar_popup() {
  return GTK_WIDGET( gtk_builder_get_object(interface_builder, 
					    "calendar_popup") );
}

static inline GtkWidget* create_comment_dialog() {
  return GTK_WIDGET( gtk_builder_get_object(interface_builder, 
					    "comment_dialog") );
}

static inline GtkWidget* create_game_property_dialog() {
  return GTK_WIDGET( gtk_builder_get_object(interface_builder, 
					    "game_property_dialog") );
}
