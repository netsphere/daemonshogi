/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _RECORD_H_
#define _RECORD_H_

#include "dte.h"
#include "movetree.h"
#include "board-misc.h"
#include "filewriter.h"
#include <string>


/** 対局者の名前の最大文字数 */
#define MAXPLAYERNAME 256

/** 手合いを表す定数 */
enum D_TEAI {
  /** 平手 */
  D_HIRATE = 0,

  /** 2: 香落ち */
  D_KYOOCHI = 1,

  /** 3: 角落ち */
  D_KAKUOCHI = 2,

  /** 4: 飛車落ち */
  D_HISYAOCHI = 3,

  /** 角香落ち */
  D_KAKUKYOOCHI = 4,

  /** 5: 飛香落ち */
  D_HISYAKYOOCHI = 5,

  /** 6-7: 二枚落ち */
  D_NIMAIOCHI = 6,

  /** 四枚落ち */
  D_YONMAIOCHI = 7,
  /** 六枚落ち */
  D_ROKUMAIOCHI = 8,
  /** 八枚落ち */
  D_HACHIMAIOCHI = 9,

  HANDICAP_ETC = 10
};


enum D_LOADSTAT {
  /** 読み込み成功 */
  D_LOAD_SUCCESS = 0,
  /** 読み込み失敗 */
  D_LOAD_ERROR = 1,
};


/** 
 * 日付と時刻. 開始日時は UNIX Epoch (1970-01-01) より古い場合がある
 * GLibの GDate は時刻を含まない.
 */
struct DateTime {
  int year, month, day;
  int hour, minute, second;
};


/** 
 * ゲーム情報を表す.
 * 変化を含むので、勝敗は一意に決まらない 
 */
struct Record 
{
  /** 
   * 棋譜ファイルフォーマット.
   * daemon_record_research_format() が返す 
   */
  enum FileType {
    D_FORMAT_ERROR = 0,
    D_FORMAT_UNKNOWN = 1,
    D_FORMAT_CSA = 2,
    D_FORMAT_KIF = 3,
  };

  /** 開始日時. UNIX Epoch (1970-01-01) より古い場合がある */
  DateTime start_time;

  /** 表題 */
  std::string title;

  /** 出典 (掲載) */
  std::string source;

  /** 対局者の名前 */
  std::string player_name[2];

  /** コメント */
  std::string game_comment;

  /** 手合い */
  D_TEAI teai;

  /** 初期配置図 */
  BOARD first_board;

  /** 指し手の内容 (変化を含む). */
  KyokumenNode* mi;

  /** 棋譜のファイル名 */
  std::string filename;

  /** ファイル形式 */
  FileType filetype;

  /** 読み込み状況 */
  D_LOADSTAT loadstat;

  /** ファイル読み込み中に発生したエラー */
  std::string strerror;

  /** 変更された */
  bool changed;

  /** コンストラクタ */
  Record();

  /** デストラクタ */
  ~Record() { delete mi; }

  /** 状態をエラー状態にする。 */
  void set_error(const char* message) {
    loadstat = D_LOAD_ERROR;
    strerror = message;
  }
};


/*********************************************************/
/* function prototypes */
/*********************************************************/

Record*     daemon_record_new            (void);
void        daemon_record_free           (Record *record);

int daemon_record_output_csa     ( Record* record,
				   FileWriter* writer );

int daemon_record_output_csa_code(Record* record,
				  FileWriter* writer,
				  const char* code);

int daemon_record_output_kif     ( Record* record,
				   FileWriter* writer );

int daemon_record_output_kif_code(Record* record,
				  FileWriter* writer,
				  const char* code);

Record* daemon_record_load( const char* filename );

/** 文字コード指定するバージョン */
Record* daemon_record_load_code( const char* filename, 
			         const char* encoding );

Record* daemon_record_load_csa( const char* filename );

/** CSA形式. 文字コードを指定 */
Record* daemon_record_load_csa_code( const char* filename, 
				     const char* encoding );

Record* daemon_record_load_kif( const char* filename );

/** KIF形式. 文字コードを指定 */
Record* daemon_record_load_kif_code( const char* filename, 
				     const char* encoding );

D_LOADSTAT  daemon_record_get_loadstat   (const Record* record);

void        daemon_record_set_player     (Record *record,
					  TEBAN next,
					  const char* s);

std::string get_datetime_string(const DateTime& t);

DateTime string_to_datetime(const std::string& s);

#endif /* _RECORD_H_ */
