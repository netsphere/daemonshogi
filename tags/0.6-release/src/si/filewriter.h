/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _FILE_WRITER_H_
#define _FILE_WRITER_H_

#include <iconv.h>
#include <string>


typedef enum {
  /** 読み込み成功 */
  D_FILEWRITER_SUCCESSFUL = 0,
  /** 読み込み失敗 */
  D_FILEWRITER_ERROR = 1,
} D_FILEWRITER_STAT;


struct FileWriter;

void              daemon_filewriter_put        (FileWriter* writer,
						const char* s);

/** 
 * ファイルへの出力と文字コード変換をおこなう.
 * 内部コードはUTF-8固定。
 */
struct FileWriter
{
  /** 出力文字コード */
  std::string outcode;

  /** 出力用 FILE* */
  FILE* out;

  /** ファイル読み込み状況 */
  D_FILEWRITER_STAT stat;

  /** iconv で使うディスクリプタ */
  iconv_t cd;

  FileWriter();
  ~FileWriter();

  int fdopen(int fd);

  FileWriter& operator << (const std::string& s) 
  {
    daemon_filewriter_put(this, s.c_str());
    return *this;
  }

private:
  FileWriter(const FileWriter& );
  FileWriter& operator = (const FileWriter& );
};


FileWriter*       daemon_filewriter_new();

void              daemon_filewriter_free       (FileWriter* writer);

int               daemon_filewriter_open       (FileWriter* writer,
						const char* filename);

void              daemon_filewriter_close      (FileWriter* writer);

/** 出力文字コードを設定する */
void              daemon_filewriter_set_outcode(FileWriter* writer,
						const char* outcode);

D_FILEWRITER_STAT daemon_filewriter_get_stat   (const FileWriter* writer);


#endif /* _FILE_READER_H_ */
