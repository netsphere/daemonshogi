// Sikou.cpp : DLL 用の初期化処理の定義を行います。
//

// #include "stdafx.h"
// #include "Sikou.h"

#include "si.h"
#include "ui.h"
#include "wrapper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	メモ!
//
//		この DLL が MFC DLL に対して動的にリンクされる場合、
//		MFC 内で呼び出されるこの DLL からエクスポートされた
//		どの関数も関数の最初に追加される AFX_MANAGE_STATE 
//		マクロを含んでいなければなりません。
//
//		例:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// 通常関数の本体はこの位置にあります
//		}
//
//		このマクロが各関数に含まれていること、MFC 内の
//		どの呼び出しより優先することは非常に重要です。
//		これは関数内の最初のステートメントでなければな
//		らないことを意味します、コンストラクタが MFC 
//		DLL 内への呼び出しを行う可能性があるので、オブ
//		ジェクト変数の宣言よりも前でなければなりません。
//
//		詳細については MFC テクニカル ノート 33 および
//		58 を参照してください。
//


BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
  switch ( fdwReason ) {
  case DLL_PROCESS_ATTACH:
  case DLL_THREAD_ATTACH:
  case DLL_THREAD_DETACH:
  case DLL_PROCESS_DETACH:
    break;
  default:
    assert(0);
  }
}


/** si shogi へのラッパー */
CSAShogiWrapper *wrapper;

/******************/
/* タスクチェンジ */	// 思考中にこのルーチンを呼ぶとタスクチェンジが行われ、右クリック等のオペレーションが可能になります。
/******************/	// 但し、多く入れるとその分、思考が遅くなります。
static void dispatch()
{
	MSG		Message;

	if(::PeekMessage(&Message,NULL,0,0,PM_REMOVE))
	{
		::TranslateMessage(&Message);
		::DispatchMessage (&Message);
	}
}

/********************************************************************************************************/
/* 中断フラグアドレス　中断フラグが０以外のときは、思考を中断してください。                             */
/*                     特に、負のときはそのプログラムが終了したときですから、ただちに終了してください。 */
/********************************************************************************************************/
static int*	chudan_flag;

/****************************************************************************/
/* 思考初期化ルーチン  対局開始のときに呼ばれます。                         */
/*                     メモリ取得等必要があれば、ここにコーディングします。 */
/*                     中断フラグのアドレスをここで取得します。             */
/****************************************************************************/

extern "C" __declspec(dllexport) void sikou_ini(int* c)
{
    chudan_flag = c;

    wrapper = si_wrap_new();
    si_wrap_sikou_ini(wrapper, c);
    si_wrap_set_dispatch_func(wrapper, dispatch);
}

/********************************************************/
/* 思考ルーチン　ここに思考をコーディングしてください。 */
/********************************************************/

extern "C" __declspec(dllexport) int sikou(int tesu, unsigned char kifu[][2],
					   int* timer_sec, int* i_moto,
					   int* i_saki, int* i_naru)
{
    return si_wrap_sikou(wrapper,
			 tesu,
			 kifu,
			 timer_sec,
			 i_moto,
			 i_saki,
			 i_naru);
}

/*************************************************************/
/* 思考終了ルーチン 対局終了したときに呼ばれます。           */
/*                  メモリ解放の必要がここに入れてください。 */
/*************************************************************/

extern "C" __declspec(dllexport) void sikou_end()
{
    si_wrap_sikou_end(wrapper);
}
