/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _CHARINFO_H_
#define _CHARINFO_H_

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/** 対局状態を表す定数 */
typedef enum {
  /**
   * 優勢の状態で盤面評価が逆転されたとき(ただし評価点の15%以上の差が
   * 付いたと
   */
  GYAKUTEN = 0,
  /**
   * 劣勢の状態で盤面評価が逆転したとき(ただし評価点の15%以上の差が
   * 付いたとき
   */
  GYAKUTEN2 = 1,
  /** 完全勝利。反則勝ち、時間切れ勝ちを含む */
  PERFECT_WIN = 2,
  /** 僅差勝利 */
  NARROW_MARGIN = 3,
  /** 僅差負け */
  NARROW_DEFEAT = 4,
  /** 完全負け */
  PERFECT_DEFEAT = 5,
  /** 通常 */
  NORMAL = 6
} MATCH_STATE;


/** キャラクターの情報をまとめて扱うクラス */
typedef struct {
  /** キャラクターの名前 */
  gchar char_name[256];
  /** キャラクターの画像 */
  GdkPixmap* pixmap;
  /** キャラクターの台詞 */
  gchar words[6][4][256];
  /** 前回の台詞の番号 */
  gint last_word;
  /** キャラクターの強さ */
  gint level;
} CharInfo;
  

/*********************************************************/
/* function prototypes */
/*********************************************************/

CharInfo*    daemon_charinfo_new          (GtkWidget* window);
CharInfo*    dameon_charinfo_new_from_file(GtkWidget* window,
					   gchar* image_file,
					   gchar* words_file);
void         daemon_charinfo_init         (CharInfo* info);
GdkPixmap*   daemon_charinfo_get_image    (CharInfo* info,
					   MATCH_STATE s);
const gchar* daemon_charinfo_get_word     (CharInfo* info,
					   MATCH_STATE s);
CharInfo*    daemon_charinfo_get_daemon   (GtkWidget* window);
  

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _CHARINFO_H_ */
