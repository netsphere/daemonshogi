/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <memory>
#include <stdio.h>
#include <si/si.h>
#include "tests.h"
#include <sys/time.h>

using namespace std;
using namespace CppUnit;


/** 手の生成のテスト */
class GenMovesTest: public TestCase
{
  CPPUNIT_TEST_SUITE( GenMovesTest );
  CPPUNIT_TEST( test_1 );
  CPPUNIT_TEST( test_1a );
  CPPUNIT_TEST( test_1b );
  CPPUNIT_TEST( test_1c );
  CPPUNIT_TEST( test_1d );
  CPPUNIT_TEST( test_2 );
  CPPUNIT_TEST( test_3 );
  CPPUNIT_TEST( test_4 );
  CPPUNIT_TEST( test_4b );
  CPPUNIT_TEST( test_5 );
  CPPUNIT_TEST( test_6 );
  CPPUNIT_TEST( test_7 );
  CPPUNIT_TEST( test_8 );
  CPPUNIT_TEST( test_9 );
  CPPUNIT_TEST_SUITE_END();

  BOARD* bo;

public:
  virtual void setUp() {
    bo = newBOARDwithInit();
  }

  virtual void tearDown() {
    freeBOARD(bo);
  }

  
  /** 合法手の生成 */
  void test_1()
  {
    printf("GenMovesTest::%s: begin\n", __func__);

    MOVEINFO mi;

    // 平手から
    bo_reset_to_hirate(bo);

    make_moves(bo, &mi);
    CPPUNIT_ASSERT_EQUAL( 30, mi.count );

    boMove( bo, te_make_move(0x77, 0x67, false) );
    boMove( bo, te_make_move(0x33, 0x43, false) );

    // 成ると不成
    make_moves(bo, &mi);
    CPPUNIT_ASSERT_EQUAL( 30 + 1 + 6 + 2, mi.count );
  }

  /** 合法手: 打つ手 */
  void test_1a()
  {
    printf("GenMovesTest::%s: begin\n", __func__);

    bo_set_by_text( bo,
		    "P1 *  *  *  * -OU *  *  * +TO\n"
		    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  *  *  *  *  *  * \n"
		    "P4 *  *  *  *  *  * -KI *  * \n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 * +FU *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  * +OU *  *  *  * \n"
		    "P+00FU00KE00KY\n"
		    "+\n" );
    MOVEINFO mi;
    make_moves( bo, &mi );
    CPPUNIT_ASSERT_EQUAL( 5 + 1 + 2 + // 玉, 盤上の歩
			  (8 * (9 - 1) - 2) + // 二歩を避ける
			  (7 * 9 - 3) + (8 * 9 - 3),
			  mi.count );
  }


  /** 合法手: pinされていると動けない */
  void test_1b()
  {
    printf("GenMovesTest::%s: begin\n", __func__);

    bo_set_by_text( bo,
		    "P1 *  *  *  *  *  *  *  * -OU\n"
		    "P2 *  *  *  *  *  *  * -KI-KI\n"
		    "P3 *  *  *  *  *  *  *  *  * \n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  * +HI\n"
		    "P+00KA\n"
		    "+\n" );

    // 打ってもpin発生
    boMove_mate( bo, te_make_put(0x55, KAKU) );

    MOVEINFO mi;
    make_moves( bo, &mi );
    CPPUNIT_ASSERT_EQUAL( 1 + 1 + 1, mi.count );

    boMove_mate( bo, te_make_move(0x21, 0x31, false) ); // 13金

    boMove_mate( bo, te_make_move(0x91, 0x92, false) ); // 29飛

    // ラインから外れれば自由に動ける
    make_moves( bo, &mi );
    CPPUNIT_ASSERT_EQUAL( 2 + 1 + 4, mi.count );
  }


  /** pinチェック */
  void test_1c()
  {
    bo_set_by_text( bo,
                    "P1 *  *  *  *  *  *  *  * -OU\n"
		    "P2 *  *  *  *  *  *  * -KI-KI\n"
		    "P3 *  *  *  *  *  *  *  *  * \n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  *  *  *  *  *  * +HI * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  * +HI\n"
		    "P+00KA\n"
                    "+\n" );

    bo_pass(bo);

    // 玉が新たなラインに入る
    boMove_mate( bo, te_make_move(0x11, 0x12, false) );
    bo_pass(bo);

    MOVEINFO mi;
    make_moves(bo, &mi);
    printf("%s: ", __func__);
    mi_print(&mi, *bo);
    CPPUNIT_ASSERT_EQUAL( 3 + 1 + 3, mi.count );
  }


  /** pinチェック: uke() */
  void test_1d()
  {
    bo_set_by_text( bo,
"P1 *  *  *  *  *  *  *  *  * \n"
"P2 *  *  *  *  *  * -FU *  * \n"
"P3 *  *  *  *  *  *  *  * -OU\n"
"P4 *  *  *  *  *  *  *  *  * \n"
"P5 *  *  *  *  *  *  * -KI * \n"
"P6 *  *  *  *  *  *  *  *  * \n"
"P7 *  *  *  *  *  *  *  *  * \n"
"P8 *  *  *  *  *  *  *  *  * \n"
"P9 *  *  *  *  *  *  *  *  * \n"
"P+00KY00KI\n"
"P-00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00KY00KY00KY00KE00KE00KE00GI00GI00GI00KI00KI00KA00KA00HI00HI\n"
"+\n" );

    boMove_mate( bo, te_make_put(0x51, KYO) );
    boMove_mate( bo, te_make_put(0x41, GIN) );
    boMove_mate( bo, te_make_put(0x32, KIN) );

    MOVEINFO mi;
    uke(bo, &mi);
    CPPUNIT_ASSERT_EQUAL( 1, mi.count );
    CPPUNIT_ASSERT( mi_includes(&mi, te_make_move(0x31, 0x32, false)) );

    bo_set_by_text( bo,
"P1 *  *  *  *  * -KE *  * -KE\n"
"P2 *  *  *  *  * +KA *  *  * \n"
"P3 *  * -OU+GI-GI * +RY *  * \n"
"P4 *  *  *  *  *  *  *  *  * \n"
"P5 *  *  *  *  *  *  *  * -RY\n"
"P6 *  *  *  *  *  *  *  *  * \n"
"P7 *  *  *  *  *  *  * +KA * \n"
"P8 *  *  *  *  *  *  * +FU * \n"
"P9 *  *  *  *  *  *  *  *  * \n"
"P+00FU00FU\n"
"P-00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00KY00KY00KY00KY00KE00KE00GI00GI00KI00KI00KI00KI\n"
"+\n" );

#ifdef USE_PIN
    CPPUNIT_ASSERT_EQUAL( (XyDiff) 0, bo->pinned[0x35] );
#endif
    boMove_mate( bo, te_make_move(0x36, 0x26, false) );

#ifdef USE_PIN
    CPPUNIT_ASSERT_EQUAL( (XyDiff) +1, bo->pinned[0x35] );
#endif

    uke(bo, &mi);
    CPPUNIT_ASSERT_EQUAL( 6, mi.count );
  }


  /** 王手: pinされてる場合 */
  void test_2()
  {
    printf("GenMovesTest::%s: begin\n", __func__);
    MOVEINFO mi;

    bo_set_by_text( bo, 
		    "P1 *  *  *  * -OU *  *  *  * \n"
		    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  * -RY *  *  *  * \n"
		    "P4 * -UM *  *  *  *  *  *  * \n"
		    "P5 *  * +KE * +KA *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  * +OU *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
		    "+\n" );

    ohte(bo, &mi);
    mi_print(&mi, *bo);
    CPPUNIT_ASSERT_EQUAL( 0, mi.count );
  }


  /** 王手を受ける: 長い攻撃では玉は引けない */
  void test_3()
  {
    bo_set_by_text( bo,
		    "P1 *  *  *  * -OU *  *  *  * \n"
		    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  *  *  *  *  *  * \n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  * +OU *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
		    "P-00KA\n"
		    "-\n" );

    boMove_mate(bo, te_make_put(0x95, KAKU) );

    MOVEINFO mi;
    uke(bo, &mi );
    CPPUNIT_ASSERT_EQUAL( 7, mi.count );
    // 取る手
    CPPUNIT_ASSERT( mi_includes(&mi, te_make_move(0x86, 0x95, false)) );
  }

  /** 王手と受けを生成 */
  void test_4()
  {
    printf("%s: begin\n", __func__);

    bo_reset_to_hirate( bo );

    MOVEINFO mi;
    // TE te;

    ohte(bo, &mi);
    mi_print(&mi, *bo);
    CPPUNIT_ASSERT_EQUAL( 0, mi.count );

    boMove(bo, te_make_move(0x77, 0x67, false) );
    CPPUNIT_ASSERT(bo->next == GOTE);

    boMove(bo, te_make_move(0x38, 0x48, false) );

    ohte(bo, &mi);
    CPPUNIT_ASSERT(mi.count == 2); // 不成と成る
    CPPUNIT_ASSERT( te_to(mi.te[0]) == 0x33);

    boMove(bo, mi.te[0] );
    CPPUNIT_ASSERT(bo->next == GOTE);

    uke(bo, &mi );
    mi_print(&mi, *bo);
    CPPUNIT_ASSERT_EQUAL( 7, mi.count );
  }

  /** 香車・桂による王手: 不成限定 */
  void test_4b()
  {
    bo_set_by_text( bo,
		    "P1 *  *  *  *  * -KI *  * -OU\n"
		    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  *  *  *  *  * -KI\n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  * +HI * +KE+KE * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  * +OU *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  * +KY\n"
		    "+\n" );
    MOVEINFO mi;
    ohte(bo, &mi);
    CPPUNIT_ASSERT_EQUAL(2, mi.count);

    CPPUNIT_ASSERT( mi_includes(&mi, te_make_move(0x53, 0x32, false)) );
  }

  /** 開き王手: 動かす駒と王手する駒が違う */
  void test_5()
  {
    bo_set_by_text( bo,
		    "P1 *  *  *  *  * -KI *  * -OU\n"
		    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  *  *  *  *  *  * \n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  * +HI *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  * +OU *  *  *  *  * \n"
		    "P9+KA *  *  *  *  *  *  *  * \n"
		    "+\n" );

    boMove_mate(bo, te_make_move(0x55, 0x15, true) );

    MOVEINFO mi;
    make_moves( bo, &mi );

    CPPUNIT_ASSERT_EQUAL( 2, mi.count );
  }

  /** 打ち歩詰め */
  void test_6()
  {
    // 打ち歩詰めではない
    bo_set_by_text( bo,
		    "P1 *  *  *  *  * -KI *  *  * \n"
		    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  *  *  *  *  *  * \n"
		    "P4 *  *  * -KI-FU-KI *  *  * \n"
		    "P5 *  *  * -KI-OU-FU *  *  * \n"
		    "P6 *  *  *  *  *  *  *  * +HI\n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  * +OU *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
		    "P+00FU\n"
		    "+\n" );

    MOVEINFO mi;
    ohte( bo, &mi );
    CPPUNIT_ASSERT_EQUAL(2, mi.count); // 歩を打ってもいい

    boMove_mate(bo, te_make_put(0x65, FU) );

    uke(bo, &mi );
    CPPUNIT_ASSERT_EQUAL(2, mi.count);

    // 打ち歩詰め
    bo_set_by_text( bo,
		    "P1 *  *  *  *  * -KI *  *  * \n"
		    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  *  *  *  *  *  * \n"
		    "P4 *  *  * -KI-FU-KI *  *  * \n"
		    "P5+HI *  * -KI-OU-FU *  *  * \n"
		    "P6 *  *  * -FU *  *  *  * +HI\n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  * +OU *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
		    "P+00FU\n"
		    "+\n" );

    CPPUNIT_ASSERT_EQUAL(1, bo->long_attack[0][0x66].count);
    boMove_mate(bo, te_make_put(0x65, FU) ); // 打ってみる
    CPPUNIT_ASSERT_EQUAL(0, bo->long_attack[0][0x66].count);
    CPPUNIT_ASSERT_EQUAL(1, bo->long_attack[0][0x65].count);

    make_moves(bo, &mi);
    CPPUNIT_ASSERT_EQUAL(0, mi.count);
    boBack_mate(bo );
    CPPUNIT_ASSERT_EQUAL(1, bo->long_attack[0][0x66].count);
    CPPUNIT_ASSERT_EQUAL(1, bo->long_attack[0][0x65].count);

    ohte( bo, &mi );
    printf("%s: %d\n", __func__, __LINE__);
    mi_print(&mi, *bo);
    CPPUNIT_ASSERT_EQUAL(2, mi.count); // 打ち歩詰めは生成しない

    // 合法手の生成でも打ち歩詰めの手は生成しない
    make_moves(bo, &mi);
    CPPUNIT_ASSERT_EQUAL( 7 + 16 + 14 + (72 - 10 - 1), mi.count );

    // 手は妥当ではない
    CPPUNIT_ASSERT_EQUAL( false, te_is_valid(bo, te_make_put(0x65, FU)) );

    newHASH();

    // 詰めることはできない
    TE te_ret;
    int node_limit = 5000;
    CPPUNIT_ASSERT( dfpn_mate(bo, 3, &te_ret, &node_limit) != CHECK_MATE );

    // 通常探索でも同じ
    bfs_minmax(bo, 3, &te_ret);
    CPPUNIT_ASSERT( teCmpTE(te_ret, te_make_put(0x65, FU)) );
  }

  /** 逆王手 */
  void test_7()
  {
    bo_set_by_text( bo,
		    "P1 *  *  *  *  * -KI * -KE-OU\n"
		    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  *  *  *  *  *  * \n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  * +HI *  * +OU * \n"
		    "P6 *  *  *  * +HI *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
		    "P+00KI\n"
		    "P-00KI\n"
		    "+\n" );
    MOVEINFO mi;
    ohte( bo, &mi );
    CPPUNIT_ASSERT_EQUAL( 1 + 2, mi.count );

    boMove_mate( bo, te_make_move( 0x65, 0x61, false ) );

    make_moves( bo, &mi );
    CPPUNIT_ASSERT_EQUAL( 1 + 1 + 4, mi.count );

    // 逆王手
    boMove_mate( bo, te_make_move(0x12, 0x31, false) );
    ohte( bo, &mi );
    printf("%s: %d\n", __func__, __LINE__);
    mi_print(&mi, *bo);
    CPPUNIT_ASSERT_EQUAL( 2, mi.count ); // 飛車で取る手のみ
  }

  /** ダブる？ */
  void test_8()
  {
    bo_set_by_text( bo,
"P1 *  *  *  *  *  *  *  *  * \n"
"P2 *  *  *  *  *  * +KY+KY * \n"
"P3 *  *  *  *  * +GI+GI+KE+KI\n"
"P4 *  *  *  *  *  * +GI+KE+KI\n"
"P5 *  *  *  *  *  * +KI+KE * \n"
"P6 *  *  *  *  *  * +KY *  * \n"
"P7 *  *  *  *  *  *  *  * +KA\n"
"P8 *  *  *  *  * +KY * +HI+KA\n"
"P9 *  *  *  * +GI * -OU+KE+KI\n"
"P-00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00FU00HI\n"
"+\n" );
    MOVEINFO mi;
    ohte(bo, &mi);
    CPPUNIT_ASSERT_EQUAL(3, mi.count);
  }


  /** 王手がおかしい？ */
  void test_9()
  {
    bo_set_by_text( bo,
"P1 *  *  *  *  *  *  *  *  * \n"
"P2 *  *  *  *  *  * +KY+KY * \n"
"P3 *  *  *  *  * +GI+GI+KE+KI\n"
"P4 *  *  *  *  *  * +GI+KE+KI\n"
"P5 *  *  *  *  *  * +KI+KE * \n"
"P6 *  *  *  *  *  * +KY *  * \n"
"P7 *  *  *  *  *  *  *  * +KA\n"
"P8 *  *  *  * -OU+KY+HI+HI+KA\n"
"P9 *  *  *  *  *  *  * +KE+KI\n"
"+\n" );
    CPPUNIT_ASSERT_EQUAL( 5, bo->piece_kiki_count[0x84]);
    CPPUNIT_ASSERT_EQUAL( 1, bo->long_attack[SENTE][0x34].count );
    CPPUNIT_ASSERT_EQUAL( 0, bo->long_attack[SENTE][0x85].count );

    CPPUNIT_ASSERT_EQUAL( 5, bo->piece_kiki_count[0x83]);
    CPPUNIT_ASSERT_EQUAL( 1, bo->long_attack[SENTE][0x84].count );

    CPPUNIT_ASSERT_EQUAL( 6, bo->piece_kiki_count[0x82]);
    CPPUNIT_ASSERT_EQUAL( 1, bo->long_attack[SENTE][0x83].count );

    MOVEINFO mi;
    ohte(bo, &mi);
    mi_print(&mi, *bo);
    CPPUNIT_ASSERT_EQUAL(4, mi.count);
  }

};
CPPUNIT_TEST_SUITE_REGISTRATION( GenMovesTest );
