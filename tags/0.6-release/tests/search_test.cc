/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// 通常探索のテスト

#include <si/si.h>
#include <si/si-think.h>
#include "tests.h"

#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>

using namespace std;
using namespace CppUnit;


/** それぞれの手の得点 */
void print_each_score( BOARD* bo )
{
  MOVEINFO mi;
  make_moves(bo, &mi);
  for (int i = 0; i < mi.count; i++) {
    printf("%s=", te_str(mi.te[i], *bo).c_str() );
    boMove(bo, mi.te[i]);
    PhiDelta score;
    if ( !hsGetBOARDMinMax(bo, &score, NULL, 0) )
      printf("N/A, ");
    else
      printf("(%d,%d), ", score.phi, score.delta);

    boBack(bo);
  }
  printf("\n");
}


bool score_after_move(BOARD* bo, TE te, PhiDelta* score)
{
  bool ret;
  boMove(bo, te);
  ret = hsGetBOARDMinMax(bo, score, NULL, 0);
  boBack(bo);
  return ret;
}


class SearchTest: public TestCase
{
  CPPUNIT_TEST_SUITE( SearchTest );
  // CPPUNIT_TEST( test_1a );
  CPPUNIT_TEST( test_2 );
  CPPUNIT_TEST( test_3 );
  CPPUNIT_TEST_SUITE_END();

  BOARD* bo;

public:
  virtual void setUp()
  {
    newHASH();
    bo = newBOARDwithInit();
  }

  virtual void tearDown()
  {
    freeBOARD(bo);
    freeHASH();
  }

#if 0
  /** ごく浅い探索 */
  void test_1a()
  {
    // http://navy.ap.teacup.com/nobuo90/1058.html
    bo_set_by_text(bo, 
		   "P1-KY *  *  *  *  * -GI-OU-KY\n"
		   "P2 *  * -HI+GI *  *  *  *  * \n"
		   "P3 *  *  *  * +TO *  * -FU-FU\n"
		   "P4 * -FU * -KA * +KE *  *  * \n"
		   "P5-FU+FU+FU *  *  * +FU *  * \n"
		   "P6 *  * +KI-FU-KI *  * +FU * \n"
		   "P7+FU *  *  *  * +FU * +GI+FU\n"
		   "P8 * +OU+KI+KA *  * +HI *  * \n"
		   "P9+KY+KE *  *  *  *  *  * +KY\n"
		   "P+00KE00FU00FU00FU\n"
		   "P-00KI00GI00KE00FU00FU\n"
		   "+\n" );

    TE te;
    bfs_minmax(bo, 2, &te);
    printf("best = %s\n", te_str(te, *bo).c_str() );
    // 正解は46角
  }
#endif // 0


  /** 
   * 負ける手 (45KE) を指してしまう
   * 43KY+ 43GI 33KA 41OU 42KI で詰み
   */
  void test_2()
  {
    printf("%s start.\n", __func__);

    bo_set_by_text(bo, 
		   "P1-KY-KE *  *  *  *  * -RY-KY\n"
		   "P2 *  *  * -GI-GI-OU *  *  * \n"
		   "P3 * +UM-FU-FU-FU-KA-KE * -FU\n"
		   "P4-FU * +FU *  * +KY+FU *  * \n"
		   "P5 *  *  *  *  *  * -FU *  * \n"
		   "P6 * +GI-KE *  * -FU *  *  * \n"
		   "P7+KY+FU * +FU+FU *  *  * +FU\n"
		   "P8 *  *  *  * +KI+FU *  *  * \n"
		   "P9 * +KI * +OU+HI *  *  *  * \n"
		   "P+00KI00KI00GI\n"
		   "P+00FU00FU00FU00FU\n"
		   "P-00KE\n"
		   "-\n" );
    TE te;
    bfs_minmax(bo, 5, &te);

    printf("score: ");
    print_each_score(bo);

    PhiDelta score;
    // 45KE, 25KEは頓死
    CPPUNIT_ASSERT( score_after_move(bo, te_make_move(0x33, 0x54, false), 
				     &score) );
    CPPUNIT_ASSERT_EQUAL( make_phi_delta(+VAL_INF, -VAL_INF), score );
    CPPUNIT_ASSERT( score_after_move(bo, te_make_move(0x33, 0x52, false), 
				     &score) );
    CPPUNIT_ASSERT_EQUAL( make_phi_delta(+VAL_INF, -VAL_INF), score );
  }


  void test_3()
  {
    printf("%s start.\n", __func__);

    // 先手が21FU+としたところ.
    // この後、21HIとしてしまう。33TO 31OU 42KI で詰み
    bo_set_by_text(bo, 
		   "P1-KY-KE *  * -HI *  * +TO-KY\n"
		   "P2 *  *  * -GI * -OU *  *  * \n"
		   "P3 * -FU-FU-FU-KI-FU * +TO-FU\n"
		   "P4 *  *  *  * -FU-KA-GI *  * \n"
		   "P5-FU *  *  *  *  *  *  *  * \n"
		   "P6+FU *  *  * +FU * +FU *  * \n"
		   "P7 * +FU+FU+FU * +FU *  * +FU\n"
		   "P8 * +KA+KI * +KI *  * +HI * \n"
		   "P9+KY+KE+GI+OU *  *  * +KE+KY\n"
		   "P+00KI00KE00FU\n"
		   "P-00GI\n"
		   "-\n" );

    TE te;

    boMove(bo, te_make_move(0x15, 0x12, false));

    // 詰んでない
    int node_limit = 300000;
    int depth = 51;
    CPPUNIT_ASSERT_EQUAL( NOT_CHECK_MATE, dfpn_mate(bo, depth, &te, &node_limit) );

    MOVEINFO mi;
    ohte(bo, &mi);
    for (int i = 0; i < mi.count; i++) {
      printf("%s=", te_str(mi.te[i], *bo).c_str() );
      boMove_mate(bo, mi.te[i]);
      int32_t phi, delta;
      if (!hsGetBOARD(0, bo, &phi, &delta, &te, depth - 1))
	printf("N/A, ");
      else
	printf("(%d,%d), ", phi, delta);

      boBack_mate(bo);
    }
    printf("\n");

    boMove(bo, te_make_move(0x32, 0x33, false));

    uke(bo, &mi);
    for (int i = 0; i < mi.count; i++) {
      printf("%s=", te_str(mi.te[i], *bo).c_str() );
      boMove_mate(bo, mi.te[i]);
      int32_t phi, delta;
      if (!hsGetBOARD(0, bo, &phi, &delta, &te, depth - 2))
	printf("N/A, ");
      else
	printf("(%d,%d), ", phi, delta);

      boBack_mate(bo);
    }
    printf("\n");
  }

};
CPPUNIT_TEST_SUITE_REGISTRATION( SearchTest );

int main()
{
#if EVAL_VERSION == 1
  string think_config_file = "../../ai-config/01.json";
#else
  string think_config_file = "../../ai-config/02ai.txt";
#endif 

  CPPUNIT_ASSERT_EQUAL( true, think_init(think_config_file) );

  TextTestRunner runner;
  TestFactoryRegistry& registry = TestFactoryRegistry::getRegistry();
  runner.addTest( registry.makeTest() );
  return !runner.run("", false);
}
