
class SpeedTest: public TestCase
{
  CPPUNIT_TEST_SUITE( SpeedTest );
  CPPUNIT_TEST( test_5 );
  CPPUNIT_TEST( test_10 );
  CPPUNIT_TEST_SUITE_END();

  void sub5(int depth, BOARD* bo)
  {
    if (depth <= 0)
      return;

    MOVEINFO mi;
    make_moves(bo, &mi);
    for (int j = 0; j < mi.count; j++) {
#if 1 // 2009.9.24現在 こちらが速い
      boMove_mate(bo, mi.te[j]);
      sub5(depth - 1, bo);
      boBack_mate(bo);
#else
      BOARD* bo2 = bo_dup(bo);
      boMove_mate(bo2, mi.te[j]);
      sub5(depth - 1, bo2);
      freeBOARD(bo2);
#endif
    }
  }

  /** 局面を戻すのとコピーするのはどちらが速いか */
  void test_5() 
  {
    sub5(5, bo);
  }

  /** 
   * 手の生成の速度
   * http://d.hatena.ne.jp/ak11/20091107#p1
   */
  void test_10()
  {
    struct timeval tv1, tv2;
    gettimeofday( &tv1, NULL );

    bo_set_by_text( bo,
"P1-KY *  *  *  *  *  * -KE-KY\n"
"P2 *  *  *  *  * +TO * -KI-OU\n"
"P3 *  * -KE-FU * +GI *  *  * \n"
"P4-FU * -FU *  *  *  * +FU-FU\n"
"P5 *  *  * +FU *  * +GI-FU * \n"
"P6 * +FU+FU-KA *  * +FU * +FU\n"
"P7+FU *  *  *  *  * +KI+GI * \n"
"P8+HI *  *  *  *  *  *  *  * \n"
"P9+KY+KE *  *  *  * -KA+OU+KY\n"
"P+00HI00KI\n"
"P-00KI00GI00KE\n"
"P-00FU00FU00FU00FU00FU\n"
"-\n" );
    int COUNT = 1000000 * 3;
    MOVEINFO mi;
    for (int i = 0; i < COUNT; i++) {
      make_moves(bo, &mi);
      // miRemoveDuplicationTE(&mi);
    }

    gettimeofday( &tv2, NULL );

    CPPUNIT_ASSERT_EQUAL( 207, mi.count ); // 角の不成もカウント
    printf("time = (%ld, %ld)\n", tv2.tv_sec - tv1.tv_sec, 
	   tv2.tv_usec - tv1.tv_usec );
  }
};
CPPUNIT_TEST_SUITE_REGISTRATION( SpeedTest );


int main()
{
  TextTestRunner runner;
  TestFactoryRegistry& registry = TestFactoryRegistry::getRegistry();
  runner.addTest( registry.makeTest() );
  return !runner.run("", false);
}
