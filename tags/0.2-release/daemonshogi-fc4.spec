%define name	daemonshogi
%define version	0.1.4
%define RELEASE	1tkfc4
%define rel	%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}
%define prefix /usr
%define sysconfdir /etc

Summary:     Daemonshogi is a GTK+ based, Simple shogi(japanese chess) program.
Summary(ja): デーモン将棋はGTK+を使ったシンプルな将棋プログラムです。
Name:        %name
Version:     %version
Release:     %rel
License:     GPL,LGPL
Group:       Amusements/Games
Source:      http://daemonshogi.sourceforge.jp/%{name}-%{version}.tar.gz
BuildRoot:   %{_tmppath}/%{name}-%{version}-root
Docdir:      %{prefix}/help
URL:         http://daemonshogi.sourceforge.jp/

Requires:    gtk+ >= 1.2.9
Requires:    gnome-libs >= 1.2.13
Requires:    libglade >= 0.16
Requires:    imlib

%description
Daemonshogi is a GTK+ based, Simple shogi(japanese chess) program.

%description -l ja
デーモン将棋はGTK+を使ったシンプルな将棋プログラムです。

%prep
%setup

%build
%ifarch alpha
   CFLAGS="$RPM_OPT_FLAGS" LDFLAGS="-s" ./configure --host=alpha-redhat-linux\
	--prefix=%{prefix} 
%else
   CFLAGS="$RPM_OPT_FLAGS" LDFLAGS="-s" ./configure \
	--prefix=%{prefix} 
%endif
make

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)

%doc README COPYING ChangeLog NEWS TODO AUTHORS INSTALL THANKS FAQ
%{prefix}/bin/%{name}
%{prefix}/share/*

%changelog
* Mon Dec 12 2005 Masahiko Tokita <BQB04357@nifty.ne.jp>
- version 0.1.14
- Fixed for Redhat 9 / Fedora Core 2 / Fedore Core 4

* Fri Dec  9 2005 Masahiko Tokita <BQB04357@nifty.ne.jp>
- Fixed for Vine linux 3.2

* Thu Sep 13 2002 Masahiko Tokita <BQB04357@nifty.ne.jp>
- Cleaned up a bit

* Thu Sep 12 2002 Masahiko Tokita <BQB04357@nifty.ne.jp>
- First try at an RPM
