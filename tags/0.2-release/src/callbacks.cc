/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#define GTK_DISABLE_DEPRECATED 1

#include <config.h>
#include <gtk/gtk.h>
#include <assert.h>
#include <stdlib.h>

G_BEGIN_DECLS
#include "callbacks.h"
#include "interface.h"
#include "support.h"
G_END_DECLS
#include "canvas.h"
#include "conn.h"
#include "history_window.h"


/** メニュー -> ファイル -> 棋譜ファイルを開く */
void
on_file_open_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  GtkWidget* dialog;
  GtkFileFilter* filter;

  dialog = gtk_file_chooser_dialog_new(_("fileselection"),
                                       GTK_WINDOW(g_canvas->window),
                                       GTK_FILE_CHOOSER_ACTION_OPEN,
                                       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                       GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                                       NULL);
  filter = gtk_file_filter_new();
  gtk_file_filter_set_name(filter, _("CSA format, KIF format"));
  gtk_file_filter_add_pattern(filter, "*.csa");
  gtk_file_filter_add_pattern(filter, "*.kif");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);

  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
    char* filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    daemon_kiffile_load_ok_button_pressed(filename);
    g_free(filename);
  }
  gtk_widget_destroy(dialog);
}


void
on_file_exit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  exit(0);
}


/** ...について */
void
on_help_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  GtkWidget* about = create_about_dialog();
  gtk_dialog_run(GTK_DIALOG(about));
  gtk_widget_destroy(about);
}


gboolean
on_window_delete_event                 (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  return FALSE;
}


void
on_window_destroy                      (GtkObject       *object,
                                        gpointer         user_data)
{
  exit(0);
}


gboolean
on_drawingarea_motion_notify_event     (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data)
{
  daemon_canvas_motion_notify(g_canvas, event);

  return FALSE;
}


gboolean
on_drawingarea_button_press_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  daemon_canvas_button_press(g_canvas, event);

  return FALSE;
}


gboolean
on_drawingarea_button_release_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  daemon_canvas_button_release(g_canvas, event);

  return FALSE;
}


void
on_file_saveas_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  GtkWidget* dialog;
  GtkFileFilter *csa_filter, *kif_filter;

  dialog = gtk_file_chooser_dialog_new(_("save book file"),
                                       GTK_WINDOW(g_canvas->window),
                                       GTK_FILE_CHOOSER_ACTION_SAVE,
                                       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                       GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                       NULL);

  csa_filter = gtk_file_filter_new();
  gtk_file_filter_set_name(csa_filter, "CSA format");
  gtk_file_filter_add_pattern(csa_filter, "*.csa");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), csa_filter);

  kif_filter = gtk_file_filter_new();
  gtk_file_filter_set_name(kif_filter, "KIF format");
  gtk_file_filter_add_pattern(kif_filter, "*.kif");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), kif_filter);

  gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);

  if (gtk_dialog_run(GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
    char* filename;
    GtkFileFilter* filter;
    enum KifuFileType ft;

    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    filter = gtk_file_chooser_get_filter(GTK_FILE_CHOOSER(dialog));
    if (filter == csa_filter)
      ft = CSA_FORMAT;
    else if (filter == kif_filter)
      ft = KIF_FORMAT;
    else
      assert(0);
    
    daemon_kiffile_save_ok_button_pressed(filename, ft);
    g_free(filename);
  }

  gtk_widget_destroy(dialog);
}


void
on_edit_startedit_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  D_CANVAS_MODE mode;

  mode = daemon_canvas_get_mode(g_canvas);

  if (mode != D_CANVAS_MODE_EDIT) {
    int ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("Start edit mode?"));
    if (ret  == GTK_RESPONSE_OK) {
      daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
    }
  }
}


/** 表示 -> 小さい */
void on_view_small_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (g_canvas)
    daemon_canvas_change_size(g_canvas, D_CANVAS_SIZE_SMALL);
}


void
on_view_middle_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (g_canvas)
    daemon_canvas_change_size(g_canvas, D_CANVAS_SIZE_MIDDLE);
}


void
on_view_big_activate                        (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (g_canvas)
    daemon_canvas_change_size(g_canvas, D_CANVAS_SIZE_BIG);
}


gboolean
on_drawingarea_expose_event            (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data)
{
  daemon_canvas_expose(g_canvas, widget, event, user_data);

  return FALSE;
}


gboolean
on_drawingarea_leave_notify_event      (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data)
{
  daemon_canvas_leave_notify(g_canvas, widget, event, user_data);

  return FALSE;
}


int network_game_start = 0;
char net_my_turn = '\0';
char net_playername[2][100];

/** ゲーム開始ダイアログ.
    各オプションメニューの初期値
*/
void on_game_optionmenu_realize             (GtkWidget       *widget,
                                        gpointer         user_data)
{
  gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 0);
}


/** CSAサーバに接続して対局 */
void on_game_remote_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  GtkWidget* dialog;
  const char* host;
  const char* port;
  const char* username;
  const char* password;
  PLAYERTYPE playertype[2];
  GtkWidget* connect_menu;
  GtkWidget* widget;

  dialog = create_remote_dialog();

  if (gtk_dialog_run(GTK_DIALOG(dialog)) != GTK_RESPONSE_OK) {
    gtk_widget_destroy(dialog);
    return;
  }

  host = gtk_entry_get_text(GTK_ENTRY(lookup_widget(dialog, "server_host")));
  port = gtk_entry_get_text(GTK_ENTRY(lookup_widget(dialog, "server_port")));
  username = gtk_entry_get_text(GTK_ENTRY(lookup_widget(dialog, 
							"username")));
  password = gtk_entry_get_text(GTK_ENTRY(lookup_widget(dialog, 
							"password")));
      
  // TODO: エラーチェック

  if (!g_canvas->network_log_window)
    g_canvas->network_log_window = create_network_log();
  gtk_widget_show(g_canvas->network_log_window);

  // TODO: マルチスレッド化
  network_log_append_message("connect to %s:%s\n", host, port);
  if (!connect_to_server(host, port)) {
    daemon_messagebox(g_canvas, _("connect failed."), GTK_MESSAGE_ERROR);
    gtk_widget_destroy(dialog);
    return;
  }

  // 接続に成功
  network_log_append_message("connect ok.\n");

  connect_menu = lookup_widget(g_canvas->window, "connect_server");
  assert(connect_menu);
  gtk_widget_set_sensitive(connect_menu, FALSE);

  // 引き続き、ログインする。
  network_log_append_message("LOGIN %s\n", username);
  csa_send_login(username, password);

  gtk_widget_destroy(dialog);

  // ゲーム開始かrejectまで待つ.
  // TODO: ブロックせずに、シグナル化？
  while (!network_game_start)
    pending_loop();
      
  if (network_game_start < 0) {
    disconnect_server();
    return;
  }

  // ゲーム開始
  daemon_dboard_set_hirate(&g_canvas->board);

  widget = lookup_widget(g_canvas->window, "retract");
  gtk_widget_set_sensitive(widget, FALSE);

  // TODO: CPU vs LAN
  if (net_my_turn == '+') {
    playertype[0] = SI_GUI_HUMAN;
    playertype[1] = SI_NETWORK_1;
  }
  else {
    playertype[0] = SI_NETWORK_1;
    playertype[1] = SI_GUI_HUMAN;
  }

  daemon_game_setup(playertype[0], net_playername[0],
		    playertype[1], net_playername[1]);

  // main loop
  daemon_canvas_game(g_canvas);
}


gboolean
on_bookwindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  assert(g_canvas);
  printf("%s: called.\n", __func__); // DEBUG

  bookwindow_item_set_active(g_canvas, FALSE);
  gtk_widget_hide(widget);

  return TRUE; // stop other handlers
}


void
on_booklist_cursor_changed             (GtkTreeView     *treeview,
                                        gpointer         user_data)
{
  GtkTreePath* path = NULL;
  const gint* idx;

  printf("cursor_changed: user_data = %d\n", (int) user_data); // DEBUG

  gtk_tree_view_get_cursor(treeview, &path, NULL);
  assert(path);
  idx = gtk_tree_path_get_indices(path);
  assert(idx);

  daemon_canvas_bookwindow_click(treeview, *idx);

  gtk_tree_path_free(path);
}


void
on_view_network_log_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (!g_canvas->network_log_window)
    g_canvas->network_log_window = create_network_log();
  gtk_widget_show(g_canvas->network_log_window);

  // TODO: 非表示
}

void
on_candidate_treeview_cursor_changed   (GtkTreeView     *treeview,
                                        gpointer         user_data)
{

}

// メニュー -> 表示 -> 棋譜ウィンドウ
void on_view_bookwindow_activate(GtkMenuItem* menu_item,
				 gpointer user_data) 
{
  Canvas* canvas = g_canvas;

  if (!bookwindow_item_is_actived(canvas)) {
    // 表示済
    printf("%s: when active.\n", __func__); // DEBUG
    gtk_widget_hide(canvas->bookwindow);
    bookwindow_item_set_active(canvas, false);
  }
  else {
    history_window_show(canvas);
  }
}


/** 初期局面まで戻す */
void on_go_first_activate(GtkBin* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;

  if (canvas->mode != D_CANVAS_MODE_BOOK) 
    return;

  printf("%s: cur_pos = %d\n", __func__, canvas->cur_pos); // DEBUG

  while (canvas->cur_pos > 0) {
    DTe te = canvas->focus_line[--canvas->cur_pos];
    daemon_dboard_back(&canvas->board, &te);
  }

  history_window_select_nth(canvas->bookwindow, canvas->cur_pos);

  // 再描画
  canvas_redraw(canvas);
  
  daemon_canvas_set_kif_sensitive(canvas);
}


/** 1手戻す */
void on_go_back_activate(GtkBin* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;

  if (canvas->mode != D_CANVAS_MODE_BOOK)
    return;

  if (canvas->cur_pos == 0)
    return;

  DTe te = canvas->focus_line[--canvas->cur_pos];
  daemon_dboard_back(&canvas->board, &te);

  // 手を選択
  history_window_select_nth(canvas->bookwindow, canvas->cur_pos);

  // 再描画
  canvas_redraw(canvas);

  daemon_canvas_set_kif_sensitive(canvas);
}


/** 1手進める */
void on_go_next_activate(GtkBin* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;

  if (canvas->mode != D_CANVAS_MODE_BOOK)
    return;

  if (canvas->cur_pos >= canvas->focus_line.size())
    return;

  DTe te = canvas->focus_line[canvas->cur_pos++];
  daemon_dboard_move(&(canvas->board), &te);

  // 棋譜ウィンドウ更新
  history_window_select_nth(canvas->bookwindow, canvas->cur_pos);

  // 再描画
  canvas_redraw(canvas);

  daemon_canvas_set_kif_sensitive(canvas);
}


void on_go_last_activate(GtkBin* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;

  if (canvas->mode != D_CANVAS_MODE_BOOK)
    return;

  while (canvas->cur_pos < canvas->focus_line.size()) {
    DTe te = canvas->focus_line[canvas->cur_pos++];
    daemon_dboard_move(&(canvas->board), &te);
  }

  history_window_select_nth(canvas->bookwindow, canvas->cur_pos);

  // 再描画
  canvas_redraw(canvas);

  daemon_canvas_set_kif_sensitive(canvas);
}



/** 上下を反転 */
void on_view_flip_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  GtkWidget* flip_menu;
  Canvas* canvas = g_canvas;
  
  // daemon_messagebox(canvas, _("Conversely, it displays."), GTK_MESSAGE_INFO);

  canvas->front = canvas->front == D_SENTE ? D_GOTE : D_SENTE;

  flip_menu = lookup_widget(canvas->window, "flip");
  assert(flip_menu);
  GTK_CHECK_MENU_ITEM(flip_menu)->active = canvas->front != D_SENTE;

  // 再描画
  canvas_redraw(canvas);
}



/** 詰め将棋 */
void on_game_checkmate_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  GtkWidget *dialog;
  int r = 0;

  dialog = create_dialog_mate();
  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK) {
    GtkWidget* sente;
    int is_sente;
    sente  = lookup_widget(dialog, "radiobutton_mate_sente");    
    assert(sente);
    is_sente = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sente));

    r = daemon_canvas_mate_start_pressed(is_sente);
  }
  gtk_widget_destroy(dialog);

  if (r)
    daemon_canvas_mate_loop();
}


void on_edit_set_hirate_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  gint ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("Set hirate?"));
  if (GTK_RESPONSE_OK != ret) 
    return;

  daemon_dboard_set_hirate(&g_canvas->board);
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
}


void on_edit_set_mate_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("Set for check mate?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_set_mate(&g_canvas->board);
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
}


void on_edit_rl_reversal_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("right/left reversal?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_set_rl_reversal(&g_canvas->board);
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
}


void on_edit_order_reversal_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("order reversal?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_set_order_reversal(&g_canvas->board);
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
}


void on_edit_flip_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("flip white/black board?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_flip(&g_canvas->board);
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
}


void on_edit_all_koma_to_pbox_activate(GtkMenuItem* menuitem, 
				       gpointer user_data)
{
  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("All piece to piecebox?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_set_all_to_pbox(&g_canvas->board);
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
}



/** メニュー -> Game -> Play Game... */
void on_game_new_game_activate(GtkBin* menuitem, gpointer user_data)
{
  GtkWidget* dialog = create_dialog_game();
  int r = 0;

  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
    r = daemon_canvas_new_game_ok(dialog);

  gtk_widget_destroy(dialog);

  if (r)
    daemon_canvas_game(g_canvas); // main loop
}


/** メニュー -> 中断 */
void on_game_stop_activate(GtkBin* menuitem, gpointer user_data)
{
  if (daemon_canvas_get_mode(g_canvas) != D_CANVAS_MODE_GAME)
    return;

  gmSetGameStatus(&g_game, SI_CHUDAN);
}


/** 待った */
void on_game_matta_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;
  int i;

  if (daemon_canvas_get_mode(canvas) != D_CANVAS_MODE_GAME)
    return;

  if (g_board.mi.count < 1)
    return;

  GtkTreeModel* model;
  GtkTreeIter iter;
  if (canvas->bookwindow) {
    GtkWidget* history_list;
    gboolean r;

    history_list = lookup_widget(canvas->bookwindow, "booklist");
    g_assert(history_list);

    model = gtk_tree_view_get_model(GTK_TREE_VIEW(history_list));
    r = gtk_tree_model_iter_nth_child(model, &iter, NULL, 
				      canvas->focus_line.size() - 1);
    g_assert(r);
  }

  // 棋譜を最大で2手削除する
  for (i = 0; i < 2 && g_board.mi.count > 0; i++) {
    boToggleNext( &g_board );
    boBack( &g_board, g_board.next );

    daemon_dmoveinfo_pop(canvas->record->mi);
    DTe te = canvas->focus_line.back();
    canvas->focus_line.pop_back();
    daemon_dboard_back(&canvas->board, &te);

    if (canvas->bookwindow)
      gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
    canvas->cur_pos--;
  }

  // 再描画
  canvas_redraw(canvas);
}


/** 投了 */
void on_game_giveup_activate(GtkBin* menuitem, gpointer user_data)
{
  if (daemon_canvas_get_mode(g_canvas) != D_CANVAS_MODE_GAME)
    return;

  gmSetGameStatus(&g_game, SI_TORYO);
}


void
on_view_move_property_activate         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  g_canvas->move_property_dialog = create_move_property_dialog();
  gtk_widget_show_all(g_canvas->move_property_dialog);
}


void
on_move_property_dialog_realize        (GtkWidget       *widget,
                                        gpointer         user_data)
{
  // 列を作る
  GtkCellRenderer* renderer;
  GtkTreeViewColumn* column;

  printf("%s: called.\n", __func__); // DEBUG

  GtkWidget* next_moves = lookup_widget(widget, "next_moves");
  assert(next_moves);

  GtkListStore* model = gtk_list_store_new(1, G_TYPE_STRING);
  gtk_tree_view_set_model(GTK_TREE_VIEW(next_moves), GTK_TREE_MODEL(model));

  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(_("Candidates"),
						    renderer,
						    "text", 0,
						    NULL);
  assert(column);
  gtk_tree_view_append_column(GTK_TREE_VIEW(next_moves), column);
}


void
on_next_moves_cursor_changed           (GtkTreeView     *treeview,
                                        gpointer         user_data)
{
  printf("%s: called.\n", __func__); // DEBUG

  // 何番目か
  GtkTreePath* path = NULL;
  gtk_tree_view_get_cursor(treeview, &path, NULL);
  assert(path);
  const gint* idx = gtk_tree_path_get_indices(path);
  assert(idx);

  // 選択した行の手は？
  KyokumenNode* n = g_canvas->record->mi;
  for (int i = 0; i < g_canvas->cur_pos; i++)
    n = n->lookup_child(g_canvas->focus_line[i]);

  DTe selected_te = n->moves[*idx].te;
  
  // 次の一手を変更するか
  if (g_canvas->focus_line[g_canvas->cur_pos] == selected_te)
    return;

  printf("change next move.\n"); // DEBUG

  // 注目する手順を更新 
  // daemon_kiffile_load_ok_button_pressed() と似たようなコード
  g_canvas->focus_line.erase(g_canvas->focus_line.begin() + g_canvas->cur_pos,
			     g_canvas->focus_line.end());
  g_canvas->focus_line.push_back(selected_te);
  n = n->lookup_child(selected_te);
  while (n && n->moves.size() > 0) {
    g_canvas->focus_line.push_back(n->moves.begin()->te);
    n = n->moves.begin()->node;
  }
  history_window_clear(g_canvas->bookwindow);
  history_window_sync(g_canvas->bookwindow, g_canvas->record);
}

