/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * FileWriter class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/filewriter.c,v $
 * $Id: filewriter.c,v 1.1.1.1 2005/12/09 09:03:04 tokita Exp $
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iconv.h>
#include "filewriter.h"


/** 初期化 */
static void daemon_filewriter_init(FileWriter* writer) 
{
  assert(writer != NULL);

  daemon_filewriter_set_outcode(writer, "CP932");
  writer->out      = NULL;
  writer->filename = NULL;
  writer->cd = (iconv_t) -1;
  writer->stat     = D_FILEWRITER_SUCCESSFUL;
}


/** コンストラクタ */
FileWriter* daemon_filewriter_new() 
{
  FileWriter* writer;

  writer = (FileWriter *)malloc(sizeof(FileWriter));
  if (writer == NULL) {
    printf("No enough memory in daemon_filewriter_new().\n");
    abort();
  }

  daemon_filewriter_init(writer);

  return writer;
}


/** デストラクタ */
void daemon_filewriter_free(FileWriter* writer) 
{
  if (writer) {
    if (writer->out)
      daemon_filewriter_close(writer);
    if (writer->filename)
      free(writer->filename);
    free(writer);
  }
}


/** 書き込み用にファイルを開く */
int daemon_filewriter_open(FileWriter* writer, const char* filename) 
{
  int len;

  assert(writer != NULL);
  assert(filename != NULL);

  len = strlen(filename);
  writer->filename = (char *)malloc(len + 1);
  strncpy(writer->filename, filename, len);
  writer->filename[len] = '\0';

  writer->out = fopen(writer->filename, "w");
  if (writer->out == NULL) {
    writer->stat = D_FILEWRITER_ERROR;
    return -1;
  }

  writer->cd = iconv_open(writer->outcode, "UTF-8");
  if (writer->cd == (iconv_t) -1) {
    fclose(writer->out);
    writer->out = NULL;
    writer->stat = D_FILEWRITER_ERROR;
    return -1;
  }

  return 0;
}


/** ファイルを閉じる */
void daemon_filewriter_close(FileWriter* writer) 
{
  assert(writer != NULL);

  iconv_close(writer->cd);
  // fflush(writer->out);
  fclose(writer->out);
  writer->out = NULL;
}


/** 文字列を出力する */
void daemon_filewriter_put(FileWriter* writer, const char* src) 
{
  size_t isize;
  size_t osize;
  char *outbuf, *result;
  size_t r;

  assert(writer != NULL);
  assert(src != NULL);
  
  isize = strlen(src);
  osize = isize * 2;
  result = outbuf = (char *)malloc(osize);
  memset(result, 0, osize);

  r = iconv(writer->cd, &src, &isize, &outbuf, &osize);
  if (r == (size_t) -1) {
    fprintf(stderr, "%s: iconv() error.\n", __func__);
    iconv(writer->cd, NULL, 0, NULL, 0); // reset
  }

  fprintf(writer->out, "%s", result);
}

#if 0
/** 入力コードをセットする。*/
void daemon_filewriter_set_incode(FileWriter* writer, const char* incode) 
{
  strncpy(writer->incode, incode, 32);
  writer->incode[strlen(incode)] = '\0';
}
#endif


/** 出力コードをセットする。 */
void daemon_filewriter_set_outcode(FileWriter* writer, const char* outcode) 
{
  strcpy(writer->outcode, outcode);
}


/** ファイル読み込み状況を返す */
D_FILEWRITER_STAT daemon_filewriter_get_stat(const FileWriter* writer) 
{
  return writer->stat;
}
