/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * Canvas class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/canvas.h,v $
 * $Id: canvas.h,v 1.1.1.1 2005/12/09 09:03:03 tokita Exp $
 */

#ifndef _CANVAS_H_
#define _CANVAS_H_

#include <gtk/gtk.h>
#include <sys/time.h>
#include "dte.h"
#include "dboard.h"
#include "sprite.h"
#include "record.h"
#include "conn.h"

#include "si/si.h"

#include <vector>
using namespace std;


typedef enum {
  /** 待機モード */
  D_CANVAS_MODE_WAIT,
  /** 棋譜モード */
  D_CANVAS_MODE_BOOK,
  /** 編集モード */
  D_CANVAS_MODE_EDIT,
  /** 対局モード */
  D_CANVAS_MODE_GAME,
  /** 詰将棋モード */
  D_CANVAS_MODE_MATE,
} D_CANVAS_MODE;

/* canvas をサイズを表す定数 */
typedef enum {
  /* 最小のサイズ */
  D_CANVAS_SIZE_SMALL  = 1,
  /* 中のサイズ */
  D_CANVAS_SIZE_MIDDLE = 2,
  /* 最大のサイズ */
  D_CANVAS_SIZE_BIG    = 3,
} D_CANVAS_SIZE;

typedef enum {
  D_CANVAS_TOOLBAR_NONE,
  D_CANVAS_TOOLBAR_ICON,
  D_CANVAS_TOOLBAR_TEXT,
  D_CANVAS_TOOLBAR_BOTH,
} D_CANVAS_TOOLBAR;

typedef enum {
  /** 該当エリアなし */
  D_CANVAS_AREA_NONE,
  /** 左駒台 */
  D_CANVAS_AREA_LEFT_KOMADAI,
  /** 右駒台 */
  D_CANVAS_AREA_RIGHT_KOMADAI,
  /** 駒箱 */
  D_CANVAS_AREA_KOMABAKO,
  /** 盤 */
  D_CANVAS_AREA_BOARD,
} D_CANVAS_AREA;

#define D_SPRIRE_KOMA_WIDTH  40
#define D_SPRIRE_KOMA_HEIGHT 40
#define D_CANCAS_SIZE_SMALL_X  640
#define D_CANCAS_SIZE_SMALL_Y  380
#define D_CANCAS_SIZE_MIDDLE_X 800
#define D_CANCAS_SIZE_MIDDLE_Y 475
#define D_CANCAS_SIZE_BIG_X    1024
#define D_CANCAS_SIZE_BIG_Y    608

#define SPRITEMAX 41


/** アプリケーションクラス */
struct Canvas 
{
  /** 主window */
  GtkWidget* window;

  /** 棋譜ウィンドウo */
  GtkWidget* bookwindow;

  /** LAN送受信ログ */
  GtkWidget* network_log_window;

  GtkWidget* move_property_dialog;

  /** canvasの横のドット数 */
  gint width;
  /** canvasの縦のドット数 */
  gint height;
  /** 描画するドローイングエリア */
  GtkWidget *drawingarea;
  /** ダブルバッファリングのためのpixmap */
  GdkPixmap *pixmap[3];
  /** ドラッグ中に使う作業用 pixmap */
  GdkPixmap *tmp_pixmap;
  /** 背景のpixmap */
  GdkPixmap *back[3];
  /** 駒の画像 */
  GdkPixmap *koma[3];
  /** 駒のマスク */
  GdkBitmap *mask[3];
  /** 背景の元の画像 */
  GdkPixbuf *back_src;
  /** 駒の元の画像 */
  GdkPixbuf *koma_src;
  /** pixmapのgc */
  GdkGC *gc;
  /** SpriteChar */
  Sprite *sprite[SPRITEMAX];
  /** 前回クリックした時刻 */
  struct timeval tv;
  /** ドラッグ中か？のフラグ */
  gboolean drag;
  /** ドラッグ中のスプライドの番号 */
  gint drag_no;
  /** スプライトとマウスカーソルの座標の差分 */
  GdkPoint diff;
  /** 盤面情報 */
  DBoard board;
  /** ゲームモード */
  D_CANVAS_MODE mode;
  /** canvas のサイズを表す定数 */
  D_CANVAS_SIZE size;

  /** record. 棋譜 */
  Record* record;

  /** 現在の表示棋譜. ゲームモードで使用。 */
  vector<DTe> focus_line;

  /** 表示棋譜の現在表示行. 棋譜の途中を指すことがある. 0 で開始局面 */
  int cur_pos;

  /** 表示する手前側の手番 */
  D_TEBAN front;

  /** 思考中か？のフラグ */
  gboolean flg_thinking;

  /** 手合い */
  D_TEAI teai;

  /** 対局者 */
  PLAYERTYPE playertype[2];
  /** GUIで入力する手 */
  TE gui_te;

  ~Canvas() {
    if (record) {
      daemon_record_free(record);
      record = NULL;
    }
  }
};


/** アプリケーションインスタンス */
extern Canvas* g_canvas;


Canvas* daemon_canvas_new           (GtkWidget *window);
// void    daemon_canvas_init          (Canvas *canvas, GtkWidget *window);
void    daemon_canvas_free          (Canvas *canvas);
void    daemon_canvas_init_sprite   (Canvas *canvas);
void    daemon_canvas_dispose_sprite(Canvas *canvas);

/* set_* , get_* */
GtkWidget*    daemon_canvas_get_window    (Canvas *canvas);
GdkGC*        daemon_canvas_get_gc        (Canvas *canvas);
GdkPixmap*    daemon_canvas_get_pixmap    (Canvas *canvas);
GdkPixmap*    daemon_canvas_get_back      (Canvas *canvas);
GtkWidget*    daemon_canvas_get_drawinarea(Canvas *canvas);
gboolean      daemon_canvas_isdrag        (Canvas *canvas);
void          daemon_canvas_set_drag      (Canvas *canvas,
					   gboolean drag);
gint          daemon_canvas_get_dragno    (Canvas *canvas);
void          daemon_canvas_set_dragno    (Canvas *canvas,
					   gint no);
GdkPoint      daemon_canvas_get_diff      (Canvas *canvas);
void          daemon_canvas_set_diff      (Canvas *canvas,
					   gint x,
					   gint y);
D_CANVAS_MODE daemon_canvas_get_mode      (Canvas *canvas);
void          daemon_canvas_set_mode      (Canvas *canvas,
					   D_CANVAS_MODE mode);
void          daemon_canvas_set_size      (Canvas* canvas,
					   D_CANVAS_SIZE size);
Sprite*       daemon_canvas_get_sprite    (Canvas* canvas, gint no);
void          daemon_canvas_set_front     (Canvas *canvas, D_TEBAN next);
D_TEBAN       daemon_canvas_get_front     (const Canvas* canvas);

/* event */
void       daemon_canvas_expose                     (Canvas *canvas,
						     GtkWidget       *widget, 
						     GdkEventExpose  *event,
						     gpointer         user_data);
void       daemon_canvas_motion_notify              (Canvas *canvas, GdkEventMotion  *event);
void       daemon_canvas_button_press               (Canvas *canvas, GdkEventButton  *event);
void       daemon_canvas_button_release             (Canvas *canvas, GdkEventButton  *event);

void       daemon_canvas_bookwindow_activate        (Canvas *canvas,
						     GtkMenuItem *menuitem,
						     gpointer user_data);
void       daemon_canvas_bookwindow_click (GtkTreeView* treeview,
					   int nth);

void       daemon_canvas_bookwindow_destroy         (Canvas *canvas,
						     GtkObject *object,
						     gpointer user_data);
void       daemon_canvas_change_size                (Canvas *canvas, D_CANVAS_SIZE size);
void       daemon_canvas_change_toolbar             (Canvas *canvas, D_CANVAS_TOOLBAR toolbar);
void       daemon_canvas_change_mode                (Canvas *canvas, D_CANVAS_MODE mode);
void       daemon_canvas_change_size_sprite         (Canvas *canvas, D_CANVAS_SIZE size);
void       daemon_canvas_timeout                    (gpointer data);
void       daemon_kiffile_load_ok_button_pressed    (const char* filename);

enum KifuFileType {
  CSA_FORMAT,
  KIF_FORMAT
};

void daemon_kiffile_save_ok_button_pressed(const char* filename,
                                           enum KifuFileType filetype);

void       daemon_canvas_leave_notify               (Canvas *canvas,
						     GtkWidget *widget,
						     GdkEventCrossing *event,
						     gpointer user_data);
void       daemon_canvas_kif_first                  (Canvas *canvas);
void       daemon_canvas_kif_back                   (Canvas *canvas);
void       daemon_canvas_kif_next                   (Canvas *canvas);
void       daemon_canvas_kif_last                   (Canvas *canvas);
void       daemon_canvas_set_kif_sensitive          (Canvas *canvas);
void       daemon_canvas_set_sensitive              (Canvas *canvas);

int daemon_canvas_mate_start_pressed(int is_sente);
void daemon_canvas_mate_loop();

void       daemon_canvas_set_mate_activate          (Canvas *canvas);
void       daemon_canvas_set_rl_reversal_activate   (Canvas *canvas);
void       daemon_canvas_set_order_reversal_activate(Canvas *canvas);
void       daemon_canvas_set_edit_flip_activate     (Canvas *canvas);
void       daemon_canvas_all_koma_to_pbox_activate  (Canvas *canvas);
int        daemon_canvas_ok_cancel_dialog           (Canvas* canvas,
						     const char* message);
int        daemon_canvas_yes_no_dialog              (Canvas* canvas,
						     const char* message);

void       daemon_canvas_mate_think_cancel();
int        daemon_canvas_new_game_ok(GtkWidget* dialog_game);

void daemon_game_setup(PLAYERTYPE playertype1, const char* playername1,
		       PLAYERTYPE playertype2, const char* playername2);

void       daemon_canvas_stop_activate              (Canvas *canvas);
void       daemon_canvas_restart_activate           (Canvas *canvas);
void       daemon_canvas_give_up_activate           (Canvas *canvas);
void       daemon_canvas_update_statusbar           (Canvas *canvas);
void       daemon_canvas_update_statusbar_te        (Canvas* canvas,
						     const DTe* te);

/* draw */
void daemon_canvas_draw_all                        (Canvas *canvas);
void daemon_canvas_draw_all_update                 (Canvas *canvas,
						    GdkRectangle *rect);
void daemon_canvas_draw_back                       (Canvas *canvas);
void daemon_canvas_draw_back_update                (Canvas *canvas,
						    GdkRectangle *rect);
void daemon_canvas_draw_back_update_from_tmp_pixmap(Canvas *canvas,
						    GdkRectangle *rect);
void daemon_canvas_draw_sprite_all                 (Canvas *canvas);
void daemon_canvas_draw_sprite_update_all          (Canvas *canvas,
						    GdkRectangle *rect);
void daemon_canvas_draw_sprite                     (Canvas *canvas,
						    gint no);
void daemon_canvas_draw_time                       (Canvas *canvas);

void canvas_redraw(Canvas* canvas);

/* etc */
void          daemon_canvas_create_back_pixmap(Canvas *canvas);
gint          daemon_canvas_on_sprite         (Canvas *canvas,
					       GdkEventButton *event);
gboolean      daemon_canvas_sprite_on_board   (Canvas *canvas,
					       Sprite *sprite);
gboolean      daemon_canvas_is_doubleclick    (Canvas* canvas);
void          daemon_canvas_create_tmp_pixmap (Canvas* canvas);
GtkWidget*    daemon_create_fileselection     (void);
GtkWidget*    daemon_create_fileselection_save(void);
void          daemon_messagebox   (Canvas* canvas, 
				   const char* s,
				   GtkMessageType mes_type);

gint          daemon_canvas_rate_xy           (const Canvas* canvas,
					       gint value);
gint          daemon_canvas_rerate_xy         (Canvas* canvas,
					       gint value);
D_CANVAS_AREA daemon_canvas_on_drag_area      (Canvas *canvas,
					       gint x, gint y,
					       gint *dest_x,
					       gint *dest_y);
GtkWidget*    daemon_create_dialog_game       (void);
void          daemon_canvas_game              (Canvas *canvas);
INPUTSTATUS   daemon_input_next_gui_human_impl(BOARD *bo,
					       TE *te);
gint          count_pbox                      (DBoard *board);
void          create_te_string                (const DBoard* board,
					       int no,
					       const DTe* te,
					       char* buf);
void          create_te_string2               (const DBoard* board,
					       int no,
					       const DTe* te,
					       char* buf);


bool bookwindow_item_is_actived(Canvas* canvas);
void bookwindow_item_set_active(Canvas* canvas, bool a);
void history_window_show(Canvas* canvas);

extern BOARD g_board;
extern GAME g_game;


#endif /* _CANVAS_H_ */

