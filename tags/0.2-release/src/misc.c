/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/misc.c,v $
 * $Id: misc.c,v 1.1.1.1 2005/12/09 09:03:06 tokita Exp $
 */

#include <gtk/gtk.h>
#include <sys/time.h>
#include <stdlib.h>
#include "misc.h"

void daemon_abort(const char* s) 
{
  printf("%s\n", s);
  abort();
}

/**
 * XPM のデータを読み込んで GdkPixmap にして返す。
 */
GdkPixmap* load_pixmap_from_data(GtkWidget* window, char **data) 
{
  GdkPixmap *pixmap;

  /* 画像を読む */
  pixmap = gdk_pixmap_create_from_xpm_d(window->window, NULL, NULL, data);
  if (!pixmap) {
    daemon_abort("No enough memory in load_pixmap_from_data(). im is NULL\n");
  }

  return pixmap;
}

GdkPixmap* load_pixmap_from_data_scaled(GtkWidget *window, const char** data,
					gint w,
					gint h) {
  GdkPixbuf *im;
  GdkPixbuf *im2;
  GdkPixmap *pixmap;

  /* 画像を読む */
  im = gdk_pixbuf_new_from_xpm_data(data);
  if (im == NULL) {
    daemon_abort("No enough memory in load_pixmap_from_data_scaled(). im is NULL\n");
  }

  pixmap = load_pixmap_from_imlib_image_scaled(window, im, w, h);
  g_object_unref(im);

  return pixmap;
}

GdkPixmap* load_pixmap_from_imlib_image_scaled(GtkWidget *window, 
					       GdkPixbuf *im,
					       gint w,
					       gint h) 
{
  GdkPixbuf *im2;
  GdkPixmap* pixmap = NULL;

  /* 画像を読む */
  im2 = gdk_pixbuf_scale_simple(im, w, h, GDK_INTERP_NEAREST);
  if (im2 == NULL) {
    daemon_abort("No enough memory in load_pixmap_from_data_scaled(). im2 is NULL\n");
  }
  gdk_pixbuf_render_pixmap_and_mask(im2, &pixmap, NULL, 0);
  g_object_unref(im2);

  return pixmap;
}

void set_color(GdkGC *gc, gushort r, gushort g, gushort b) {
  GdkColor color;

  color.red = r, color.green = g, color.blue = b;
  gdk_color_alloc(gdk_colormap_get_system(), &color);
  gdk_gc_set_background(gc, &color);
  gdk_gc_set_foreground(gc, &color);
}

/**
 * tv と now の時間差が DOUBLECLICKTIMEOUT 以内の場合は TRUE を返す。
 * それ以外は FALSE を返す。
 * @param tv  前にクリックされた時刻
 * @param now 今の時刻
 * @return 時間差が DOUBLECLICKTIMEOUT 以内の場合は TRUE
 */
gboolean check_timeval(struct timeval *tv, struct timeval *now) {
  if (2 <= now->tv_sec - tv->tv_sec) {
    /* ２秒以上差がある */
    return FALSE;
  }
  
  if (0 < now->tv_sec - tv->tv_sec) {
    if ((now->tv_usec + 1000000) - tv->tv_usec > DOUBLECLICKTIMEOUT * 1000) {
      return FALSE;
    } else {
      tv->tv_sec = 0L;
      tv->tv_usec = 0L;
      return TRUE;
    }
  } else {
    if (now->tv_usec - tv->tv_usec > DOUBLECLICKTIMEOUT * 1000) {
      return FALSE;
    } else {
      tv->tv_sec = 0L;
      tv->tv_usec = 0L;
      return TRUE;
    }
  }
}

/**
 * a と b が重なりあっているか調べる。
 * @param a 
 * @param b
 * @return 重なっていれば TRUE 、そうでなければ FALSE を返す。
 */
gboolean is_on_rectangle(GdkRectangle* a, GdkRectangle* b) {
  if (a->x <= b->x && b->x <= a->x + a->width &&
      a->y <= b->y && b->y <= a->y + a->height) {
    return TRUE;
  }
  
  if (b->x <= a->x && a->x <= b->x + b->width &&
      a->y <= b->y && b->y <= a->y + a->height) {
    return TRUE;
  }
  
  if (b->x <= a->x && a->x <= b->x + b->width &&
      b->y <= a->y && a->y <= b->y + b->height) {
    return TRUE;
  }

  if (a->x <= b->x && b->x <= a->x + a->width &&
      b->y <= a->y && a->y <= b->y + b->height) {
    return TRUE;
  }

  return FALSE;
}


/* timeout <= 0 の場合は時間制限なし  */
void pending_loop()
{
  while (gtk_events_pending())
    gtk_main_iteration();
}


gboolean daemon_on_rectangle(GdkRectangle *rect, GdkPoint *point) {
  g_assert(rect != NULL);
  g_assert(point != NULL);

  if (rect->x <= point->x &&
      rect->y <= point->y &&
      point->x <= rect->x + rect->width &&
      point->y <= rect->y + rect->height) {
    return TRUE;
  }

  return FALSE;
}

