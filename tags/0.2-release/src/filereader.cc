/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * FileReader class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/filereader.c,v $
 * $Id: filereader.c,v 1.1.1.1 2005/12/09 09:03:04 tokita Exp $
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iconv.h>
#include <ctype.h>
#include <errno.h>
#include "filereader.h"


static void daemon_filereader_init(FileReader* reader) 
{
  strcpy(reader->incode, "CP932");
  reader->filename = NULL;
  reader->in = NULL;
  reader->cd = (iconv_t) -1;
  reader->stat   = D_FILEREADER_STAT_SUCCESSFUL;
}


/** コンストラクタ */
FileReader* daemon_filereader_new()
{
  FileReader* reader;

  reader = new FileReader();
  if (reader == NULL) {
    printf("No enough memory in daemon_filereader_new().\n");
    abort();
  }

  daemon_filereader_init(reader);

  return reader;
}


/** デストラクタ */
void daemon_filereader_free(FileReader* reader) 
{
  if (reader) {
    if (reader->in)
      daemon_filereader_close(reader);
    if (reader->filename)
      free(reader->filename);
    delete reader;
  }
}


/**
 * ファイル filename を開く。
 * @return 成功の場合は 0 を返す。失敗した場合は -1 を返す。
 */
int daemon_filereader_open(FileReader* reader, const char* filename) 
{
  int len;

  assert(reader != NULL);
  assert(filename != NULL);

  len = strlen(filename);
  reader->filename = (char*) malloc(len + 1);
  strcpy(reader->filename, filename);

  reader->in = fopen(reader->filename, "r");
  if (reader->in == NULL) {
    reader->stat = D_FILEREADER_STAT_ERROR;
    return -1;
  }

  reader->cd = iconv_open("UTF-8", reader->incode);
  if (reader->cd == (iconv_t) -1) {
    fprintf(stderr, "iconv_open() error.\n"); // DEBUG
    fclose(reader->in);
    reader->in = NULL;
    reader->stat = D_FILEREADER_STAT_ERROR;
    return -1;
  }

  reader->lineno = 0;
  reader->pushed_string = "";
  return 0;
}


void daemon_filereader_close(FileReader* reader) 
{
  assert(reader != NULL);

  iconv_close(reader->cd);

  fclose(reader->in);
  reader->in = NULL;
}


/**
 * s を調べて 0x0D か 0x0A を見つけたら 0 に置き換える。
 * @param s 対象の文字列
 */
void daemon_filereader_return2zero(char* s) 
{
  for ( ; *s != '\0'; s++) {
    if (*s == 0x0D || *s == 0x0A) {
      *s = '\0';
      return;
    }
  }
}


/** 次の行を返す. 末尾の改行文字は取り去る
    読みながら文字コードを変換する */
char* daemon_filereader_next(FileReader* reader) 
{
  size_t isize;
  size_t osize;
  char *inbuf, *outbuf;
  char buf[BUFSIZ];
  size_t r;

  assert(reader != NULL);

  if (reader->pushed_string != "") {
    strcpy(reader->buf, reader->pushed_string.c_str());
    reader->pushed_string = "";
    return reader->buf;
  }

  // 1行読む. 改行文字はママ
  if (fgets(buf, BUFSIZ - 1, reader->in) == NULL) 
    return NULL;
  
  // printf("%s: fgets() = %s", __func__, buf); // DEBUG
  reader->lineno++;

  isize = strlen(buf);
  osize = isize * 2;
  inbuf  = buf;
  outbuf = reader->buf;
  memset(reader->buf, 0, osize);

  r = iconv(reader->cd, &inbuf, &isize, &outbuf, &osize);
  if (r == (size_t) -1) {
    fprintf(stderr, "iconv() error: buf = %s, error = %s\n", 
	    buf, strerror(errno)); // DEBUG
    iconv(reader->cd, NULL, 0, NULL, 0); // reset
  }

  daemon_filereader_return2zero(reader->buf);
  return reader->buf;
}


/** 入力コードをセットする。 */
void daemon_filereader_set_incode(FileReader* reader, const char* incode) 
{
  assert(reader != NULL);
  assert(incode != NULL);
  strcpy(reader->incode, incode);
}

/** 出力コードをセットする。 */
/*
void daemon_filereader_set_outcode(FileReader* reader, char *outcode) {
  assert(reader != NULL);
  assert(outcode != NULL);
  strncpy(reader->outcode, outcode, 32);
}
*/

/**
 * 行頭のスペースをスキップして文字列を返す。
 * isspace() が 1 になる文字をスキップする。
 */
static char* daemon_filereader_skip_space(char* s) 
{
  while (*s != '\0' && isspace(*s))
    s++;
  return s;
}

/**
 * 行頭が c で始まる場合はコメントとして読み飛ばす。
 * コメント以外の行を見付けたらその行を返す。
 * @param reader 
 * @param c 
 */
char* daemon_filereader_skip_comment(FileReader* reader, char c) 
{
  char* s;

  assert(reader != NULL);

  while ((s = daemon_filereader_next(reader)) != NULL) {
    s = daemon_filereader_skip_space(s);
    if (*s != c)
      break;
  }

  // printf("%s: skip_comment: %s\n", __func__, s); // DEBUG
  return s;
}


/** 文字列を reader に戻す.
    @param s 戻す文字列。ナル終端 */
void daemon_filereader_push(FileReader* reader, const char* s) 
{
  assert(reader != NULL);
#ifndef NDEBUG
  if (reader->pushed_string != "") {
    printf("warning: much pushed: %d: %s\n", 
	   reader->lineno, 
	   s);
  }
#endif // NDEBUG
  assert(strlen(s) < FILEREADER_MAXCOLUMN);

  reader->pushed_string = s;
}


D_FILEREADER_STAT daemon_filereader_get_stat(const FileReader* reader) 
{
  return reader->stat;
}

