
// Copyright (c) 2008-2009 HORIKAWA Hisashi.
// All rights reserved.

#define GNOME_DISABLE_DEPRECATED 1
#define GTK_DISABLE_DEPRECATED 1

#include <config.h>
#include <gtk/gtk.h>
#include <assert.h>
G_BEGIN_DECLS
#include "interface.h"
#include "support.h"
G_END_DECLS
#include "record.h"
#include "history_window.h"
#include "canvas.h"


static GtkTreeView* get_history_list(GtkWidget* history_window)
{
  // 棋譜ウィンドウは高々一つだけ。
  static GtkWidget* list = NULL;

  if (!history_window)
    return NULL;

  if (!list) {
    list = lookup_widget(history_window, "booklist");
    g_assert(list);
  }
  return GTK_TREE_VIEW(list);
}


static void add_columns(GtkTreeView* treeview)
{
  GtkCellRenderer* renderer;
  GtkTreeViewColumn* column;

  // column 1
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes("",
						    renderer,
						    "text", 0, // 列
						    NULL);
  gtk_tree_view_append_column(treeview, column);

  // column 2
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(_("Moves"), 
						    renderer, 
						    "text", 1, 
						    NULL);
  gtk_tree_view_append_column(treeview, column);

  // column 3
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(_("Elapsed"), 
						    renderer, 
						    "text", 2, 
						    NULL);
  gtk_tree_view_append_column(treeview, column);
}


/** 棋譜ウィンドウを生成し、GUIコントロールを整える */
GtkWidget* history_window_new()
{
  GtkWidget* window = create_bookwindow();
  g_assert(window);

  GtkTreeView* history_list = get_history_list(window);

  GtkListStore* model;
  GtkCellRenderer* cell;
  GtkTreeViewColumn* column;

  model = gtk_list_store_new(3, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING);
  gtk_tree_view_set_model(history_list, GTK_TREE_MODEL(model));

  add_columns(GTK_TREE_VIEW(history_list));

  return window;
}


/** 棋譜ウィンドウをクリア */
void history_window_clear(GtkWidget* history_window)
{
  if (!history_window)
    return;

  printf("%s: called.\n", __func__); // DEBUG

  GtkTreeView* history_list = get_history_list(history_window);
  GtkListStore* model = GTK_LIST_STORE(gtk_tree_view_get_model(history_list));
  g_assert(model);

  gtk_list_store_clear(model);
}


/** プロパティダイアログを更新 */
static void update_property_dialog() 
{
  if (!g_canvas->move_property_dialog)
    return;

  // グラフを辿って、局面ノードを得る
  KyokumenNode* n = g_canvas->record->mi;
  for (int i = 0; i < g_canvas->cur_pos; i++)
    n = n->lookup_child(g_canvas->focus_line[i]);

  // 次の一手を表示
  // TODO: ラインで表示
  GtkTreeView* next_moves = GTK_TREE_VIEW(lookup_widget(g_canvas->move_property_dialog, 
							  "next_moves"));
  assert(next_moves);
  GtkListStore* model = GTK_LIST_STORE(gtk_tree_view_get_model(next_moves));
  gtk_list_store_clear(model);

  KyokumenNode::Moves::const_iterator i;
  for (i = n->moves.begin(); i != n->moves.end(); i++) {
    GtkTreeIter iter;
    gtk_list_store_append(model, &iter);
    char buf[100];
    create_te_string(&g_canvas->board, 0, &i->te, buf);
    gtk_list_store_set(model, &iter,
		       0, buf,
		       -1);
  }
}


/** 棋譜ウィンドウで指定の行をハイライトする */
void history_window_select_nth(GtkWidget* history_window, int nth)
{
  if (!history_window)
    return;

  printf("%s: nth = %d\n", __func__, nth); // DEBUG
  if (nth < 0) {
    printf("illegal nth??\n"); // DEBUG
    return;
  }
  GtkTreeView* history_list = get_history_list(history_window);
  GtkTreeModel* model = gtk_tree_view_get_model(history_list);

  GtkTreeIter iter;
  gtk_tree_model_iter_nth_child(model, &iter, NULL, nth);

  GtkTreeSelection* selection = gtk_tree_view_get_selection(history_list);
  gtk_tree_selection_select_iter(selection, &iter);

  // 行をウィンドウ内に表示
  GtkTreePath* path = gtk_tree_path_new_from_indices(nth, -1);
  gtk_tree_view_scroll_to_cell(history_list, path, NULL, 
			       FALSE, 0.0, 0.0);
  gtk_tree_path_free(path);

  update_property_dialog();
}


/** 棋譜ウィンドウの末尾に指し手を追加.
    @param te   NULLのときは「初期局面」を追加。 */
void history_window_append_move(GtkWidget* history_window, 
				int count, const char* te_str, int sec)
{
  if (!history_window)
    return;

  GtkTreeView* history_list = get_history_list(history_window);
  GtkListStore* model = GTK_LIST_STORE(gtk_tree_view_get_model(history_list));
  assert(model);

  char tbuf[100];
  sprintf(tbuf, "%d:%02d", sec / 60, sec % 60);

  GtkTreeIter iter;
  gtk_list_store_append(model, &iter);
  gtk_list_store_set(model, &iter, 
		     0, count,
		     1, te_str, 
		     2, tbuf,
		     -1);
}


/** 棋譜モードで、指定の手に移動
    @param nth   0のとき初期局面 */
void daemon_canvas_bookwindow_click(GtkTreeView* treeview,
				    int nth)
{
  assert(g_canvas);
  assert(nth >= 0);
  printf("count = %d, nth = %d\n", g_canvas->cur_pos, nth); // DEBUG

  if (g_canvas->mode != D_CANVAS_MODE_BOOK)
    return;

  if (g_canvas->cur_pos < 0) { // DEBUG
    printf("%s: warning: cur_pos illegal??\n", __func__);
    return;
  }

  if (g_canvas->cur_pos == nth)
    return;

  if (g_canvas->cur_pos > nth) {
    // 遡る
    while (g_canvas->cur_pos > nth) {
      daemon_dboard_back(&g_canvas->board,
			 &g_canvas->focus_line[--g_canvas->cur_pos]);
    }
  }
  else {
    // 進める
    while (g_canvas->cur_pos < nth) {
      daemon_dboard_move(&g_canvas->board, 
			 &g_canvas->focus_line[g_canvas->cur_pos++]);
    }
  }

  daemon_canvas_set_kif_sensitive(g_canvas);
  update_property_dialog();

  // 再描画
  canvas_redraw(g_canvas);
}


/** 棋譜ウィンドウに棋譜を表示 */
void history_window_sync(GtkWidget* history_window, const Record* record)
{
  DBoard* board;
  int i;

  board = daemon_dboard_new();
  daemon_dboard_copy(&record->first_board, board);

  for (i = 0; i <= g_canvas->focus_line.size(); i++) {
    const DTe* te;
    char buf[100];
    if (!i)
      te = NULL;
    else
      te = &g_canvas->focus_line[i - 1];

    create_te_string(board, i, te, buf);
    history_window_append_move(history_window, i, buf, 
			       i ? 0 : 0);  // TODO: 消費時間

    if (te)
      daemon_dboard_move(board, te);
  }

  daemon_dboard_free(board);
}

