/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * FileWriter class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/filewriter.h,v $
 * $Id: filewriter.h,v 1.1.1.1 2005/12/09 09:03:04 tokita Exp $
 */

#ifndef _FILE_WRITER_H_
#define _FILE_WRITER_H_

#include <iconv.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef enum {
  /** 読み込み成功 */
  D_FILEWRITER_SUCCESSFUL = 0,
  /** 読み込み失敗 */
  D_FILEWRITER_ERROR = 1,
} D_FILEWRITER_STAT;


/** file writer */
typedef struct 
{
  /** 出力文字コード */
  char outcode[32];

  /** 出力用 FILE* */
  FILE* out;

  /** 出力先ファイル名 */
  char* filename;

  /** ファイル読み込み状況 */
  D_FILEWRITER_STAT stat;
  /** iconv で使うディスクリプタ */
  iconv_t cd;
} FileWriter;


FileWriter*       daemon_filewriter_new        (void);
void              daemon_filewriter_free       (FileWriter* writer);
int               daemon_filewriter_open       (FileWriter* writer,
						const char* filename);
void              daemon_filewriter_close      (FileWriter* writer);
void              daemon_filewriter_put        (FileWriter* writer,
						const char* s);
void              daemon_filewriter_set_incode (FileWriter* writer,
						const char* incode);
void              daemon_filewriter_set_outcode(FileWriter* writer,
						const char* outcode);
D_FILEWRITER_STAT daemon_filewriter_get_stat   (const FileWriter* writer);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _FILE_READER_H_ */
