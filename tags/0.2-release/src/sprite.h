/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * Sprite class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/sprite.h,v $
 * $Id: sprite.h,v 1.1.1.1 2005/12/09 09:03:07 tokita Exp $
 */

#ifndef _SPRITE_H_
#define _SPRITE_H_

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

  
typedef enum {
  KOMA_SPRITE = 0,
  ETC_SPRITE = 1
} SPRITE_TYPE;

/** スプライトクラス */
typedef struct {
  /** スプライトの種類 */
  SPRITE_TYPE type;
  /** sprite の横のドット数 */	
  gint width;
  /** sprite の縦のドット数 */	
  gint height;
  /** sprite のX位置 */	
  gint x;
  /** sprite のY位置 */	
  gint y;
  /** sprite の画像の表示元のX位置 */
  gint src_x;
  /** sprite の画像の表示元のY位置 */
  gint src_y;
  /** ドラッグ前のX位置 */
  gint from_x;
  /** ドラッグ前のY位置 */
  gint from_y;
  /** sprite の画像 */
  GdkPixmap* pixmap;
  /** sprite の画像 */
  GdkBitmap* mask;
  /** 表示するか？のフラグ。TRUEならば表示する。 */
  gboolean visible;
  /** 移動できるかのフラグ。TRUEならば移動できる。 */
  gboolean move;
  /** 駒種類 */
  gint koma;
} Sprite;

/*********************************************************/
/* function prototypes */
/*********************************************************/

Sprite*    daemon_sprite_new        (GdkPixmap* pixmap,
				     GdkBitmap* bitmap,
				     gint width,
				     gint height);
SPRITE_TYPE daemon_sprite_get_type  (Sprite* sprite);
gint       daemon_sprite_get_width  (Sprite* sprite);
gint       daemon_sprite_get_height (Sprite* sprite);
gint       daemon_sprite_get_x      (Sprite* sprite);
gint       daemon_sprite_get_y      (Sprite* sprite);
gint       daemon_sprite_get_src_x  (Sprite* sprite);
gint       daemon_sprite_get_src_y  (Sprite* sprite);
void       daemon_sprite_set_pixmap (Sprite* sprite, GdkPixmap *pixmap);
GdkPixmap* daemon_sprite_get_pixmap (Sprite* sprite);
gboolean   daemon_sprite_isvisible  (Sprite* sprite);
void       daemon_sprite_set_type   (Sprite* sprite,
				     SPRITE_TYPE type);
void       daemon_sprite_set_x      (Sprite* sprite,
				     gint x);
void       daemon_sprite_set_y      (Sprite* sprite,
				     gint y);
void       daemon_sprite_set_src_x  (Sprite* sprite,
				     gint x);
void       daemon_sprite_set_src_y  (Sprite* sprite,
				     gint y);
void       daemon_sprite_set_width  (Sprite* sprite,
				     gint width);
void       daemon_sprite_set_height (Sprite* sprite,
				     gint height);
gboolean   daemon_sprite_on         (Sprite* sprite,
				     int x,
				     int y);
void       daemon_sprite_set_mask   (Sprite* sprite,
				     GdkBitmap* mask);
GdkBitmap* daemon_sprite_get_mask   (Sprite* sprite);
void       daemon_sprite_set_visible(Sprite* sprite,
				     gboolean bool_);
void       daemon_sprite_set_koma   (Sprite* sprite,
				     gint koma);
gint       daemon_sprite_get_koma   (Sprite* sprite);
gboolean   daemon_sprite_ismove     (Sprite* sprite);
void       daemon_sprite_set_move   (Sprite* sprite, gboolean move);
void       daemon_sprite_set_from_x (Sprite* sprite,
				     gint x);
void       daemon_sprite_set_from_y (Sprite* sprite,
				     gint y);
gint       daemon_sprite_get_from_x (Sprite* sprite);
gint       daemon_sprite_get_from_y (Sprite* sprite);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _SPRITE_H_ */
