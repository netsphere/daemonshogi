/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * DTe class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/dte.h,v $
 * $Id: dte.h,v 1.1.1.1 2005/12/09 09:03:04 tokita Exp $
 */

#ifndef _DTE_H_
#define _DTE_H_

#include <stdint.h> // int16_t


/** 指し手 */
struct DTe 
{
  /** 移動元の場所 */
  int16_t fm;

  /** 移動先の場所 */
  int16_t to;

  /** 成りならば true、それ以外は false をセットする */
  bool nari;

  /** 駒打ちならば打ち込む駒種類、それ以外は 0 をセットする */
  int uti;
  
  /** 取った駒. undoに必要. */
  int tori;

  bool operator == (const DTe& x) const {
    return to == x.to && fm == x.fm && nari == x.nari && uti == x.uti &&
           tori == x.tori;
  }
};


/*********************************************************/
/* function prototypes */
/*********************************************************/

DTe* daemon_dte_new   (void);
void daemon_dte_free  (DTe* te);
void daemon_dte_output(const DTe* te, FILE* out);


#endif /* _DTE_H_ */
