/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * Sprite class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/sprite.c,v $
 * $Id: sprite.c,v 1.1.1.1 2005/12/09 09:03:07 tokita Exp $
 */

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include "sprite.h"
#include "misc.h"

gint src_xy[][2] = {
    { -1,  -1}, /* なし */
    {  0,   0}, /* 先手歩   0x01 */
    { 40,   0}, /* 先手香車 0x02 */
    { 80,   0}, /* 先手桂馬 0x03 */
    {120,   0}, /* 先手銀   0x04 */
    {160,   0}, /* 先手金   0x05 */
    {200,   0}, /* 先手角   0x06 */
    {240,   0}, /* 先手飛車 0x07 */
    {280,   0}, /* 先手王   0x08 */
    {  0,  40}, /* 先手と   0x09 */
    { 40,  40}, /* 先手成香 0x0A */
    { 80,  40}, /* 先手成桂 0x0B */
    {120,  40}, /* 先手成銀 0x0C */
    {280,  40}, /* 先手玉   0x0D */
    {200,  40}, /* 先手馬   0x0E */
    {240,  40}, /* 先手竜   0x0F */
    { -1,  -1}, /* 先手竜   0x10 */
    {  0,  80}, /* 後手歩   0x11 */
    { 40,  80}, /* 後手香車 0x12 */
    { 80,  80}, /* 後手桂馬 0x13 */
    {120,  80}, /* 後手銀   0x14 */
    {160,  80}, /* 後手金   0x15 */
    {200,  80}, /* 後手角   0x16 */
    {240,  80}, /* 後手飛車 0x17 */
    {280,  80}, /* 後手王   0x18 */
    {  0, 120}, /* 後手と   0x19 */
    { 40, 120}, /* 後手成香 0x1A */
    { 80, 120}, /* 後手成桂 0x1B */
    {120, 120}, /* 後手成銀 0x1C */
    {280, 120}, /* 後手玉   0x1D */
    {200, 120}, /* 後手馬   0x1E */
    {240, 120}, /* 後手竜   0x1F */
};

/**
 * Sprite を生成してそのポインタを返す。
 * @param pixmap 表示するのpixmap
 * @param width  pixmapの横サイズ
 * @param height pixmapの縦サイズ
 */
Sprite* daemon_sprite_new(GdkPixmap* pixmap,
			  GdkBitmap* mask,
			  gint width,
			  gint height) {
  Sprite* sprite;

  sprite = (Sprite*)malloc(sizeof(Sprite));
  if (sprite == NULL) {
    daemon_abort("No enough memory in daemon_sprite_new()");
  }

  sprite->pixmap  = pixmap;
  sprite->mask    = mask;
  sprite->width   = width;
  sprite->height  = height;
  sprite->x       = 0;
  sprite->y       = 0;
  sprite->visible = FALSE;
  sprite->koma    = 0;

  return sprite;
}

/**
 * widthを返す。
 * @param 対象のsprite
 * @return width
 */
gint daemon_sprite_get_width(Sprite* sprite) {
  return sprite->width;
}

/**
 * heightを返す。
 * @param 対象のsprite
 * @return height
 */
gint daemon_sprite_get_height(Sprite* sprite) {
  return sprite->height;
}

/**
 * xを返す。
 * @param 対象のsprite
 * @return x
 */
gint daemon_sprite_get_x(Sprite* sprite) {
  return sprite->x;
}

/**
 * yを返す。
 * @param 対象のsprite
 * @return y
 */
gint daemon_sprite_get_y(Sprite* sprite) {
  return sprite->y;
}

/**
 * src_xを返す。
 * @param 対象のsprite
 * @return x
 */
gint daemon_sprite_get_src_x(Sprite* sprite) {
  gint koma;

  g_assert(sprite != NULL);
  
  if (sprite->type == KOMA_SPRITE) {
    koma = daemon_sprite_get_koma(sprite);
    g_assert(1 <= koma && koma <= 0x1F);
    return src_xy[koma][0] * sprite->width / 40;
  } else {
    return 0;
  }
}

/**
 * src_yを返す。
 * @param 対象のsprite
 * @return y
 */
gint daemon_sprite_get_src_y(Sprite* sprite) {
  gint koma;
  
  g_assert(sprite != NULL);
  
  if (sprite->type == KOMA_SPRITE) {
    koma = daemon_sprite_get_koma(sprite);
    g_assert(1 <= koma && koma <= 0x1F);
    return src_xy[koma][1] * sprite->height / 40;
  } else {
    return 0;
  }
}

/**
 * xをセットする。
 * @param sprite 対象のsprite
 * @param x セットする x
 */
void daemon_sprite_set_x(Sprite* sprite, gint x) {
  sprite->x = x;
}

/**
 * yをセットする。
 * @param sprite 対象のsprite
 * @param y セットする y
 */
void daemon_sprite_set_y(Sprite* sprite, gint y) {
  sprite->y = y;
}

/**
 * src_xをセットする。
 * @param sprite 対象のsprite
 * @param src_x セットする src_x
 */
void daemon_sprite_set_src_x(Sprite* sprite, gint src_x) {
  g_assert(sprite != NULL);
  sprite->src_x = src_x;
}

/**
 * src_yをセットする。
 * @param sprite 対象のsprite
 * @param src_y セットする src_y
 */
void daemon_sprite_set_src_y(Sprite* sprite, gint src_y) {
  g_assert(sprite != NULL);
  sprite->src_y = src_y;
}

/**
 * width をセットする。
 * @param sprite 対象のsprite
 * @param width セットする width
 */
void daemon_sprite_set_width(Sprite* sprite, gint width) {
  sprite->width = width;
}

/**
 * height をセットする。
 * @param sprite 対象のsprite
 * @param height セットする height
 */
void daemon_sprite_set_height(Sprite* sprite, gint height) {
  sprite->height = height;
}

/**
 * pixmapをセットする。
 * @param sprite 対象のsprite
 * @param pixmap セットするpixmap
 */
void daemon_sprite_set_pixmap(Sprite* sprite, GdkPixmap *pixmap) {
  sprite->pixmap = pixmap;
}

/**
 * pixmapを返す。
 * @param sprite 対象のsprite
 * @return pixmap
 */
GdkPixmap* daemon_sprite_get_pixmap(Sprite* sprite) {
  return sprite->pixmap;
}

/**
 * 表示するかのフラグを返す。TRUEならば表示する。
 * @param 対象のsprite
 * @return 表示するかのフラグ
 */
gboolean daemon_sprite_isvisible(Sprite* sprite) {
  return sprite->visible;
}

/**
 * 表示するかのフラグをセットする。
 * @param sprite 対象のsprite
 * @param bool 表示するかのフラグ
 */
void daemon_sprite_set_visible(Sprite* sprite, gboolean bool_) 
{
  sprite->visible = bool_;
}

/**
 * pixmapを返す。
 * @param 対象のsprite
 * @return pixmap
 */
GdkBitmap* daemon_sprite_get_mask(Sprite* sprite) {
  return sprite->mask;
}

/**
 * pixmapをセットする。
 * @param 対象のsprite
 * @param セットするBitmap
 */
void daemon_sprite_set_mask(Sprite* sprite, GdkBitmap* mask) {
  sprite->mask = mask;
}

/** 駒種類をセットする */
void daemon_sprite_set_koma(Sprite* sprite, gint koma) {
  g_assert(sprite != NULL);
  g_assert(1 <= koma && koma <= 0x1F);
  sprite->koma = koma;
}

/** 駒種類を返す */
gint daemon_sprite_get_koma(Sprite* sprite) {
  g_assert(sprite != NULL);
  return sprite->koma;
}

/** スプライトの種類を返す */
SPRITE_TYPE daemon_sprite_get_type(Sprite* sprite) {
  return sprite->type;
}

/** スプライトの種類をセットする */
void daemon_sprite_set_type(Sprite* sprite, SPRITE_TYPE type) {
  sprite->type = type;
}

/** 移動できるかのフラグを返す。*/
gboolean daemon_sprite_ismove(Sprite* sprite) {
  return sprite->move;
}

/** 移動できるかのフラグをセットする */
void daemon_sprite_set_move(Sprite* sprite, gboolean move) {
  sprite->move = move;
}

/** ドラッグ前のX位置をセットする */
void daemon_sprite_set_from_x(Sprite* sprite, gint x) {
  sprite->from_x = x;
}

/** ドラッグ前のY位置をセットする */
void daemon_sprite_set_from_y(Sprite* sprite, gint y) {
  sprite->from_y = y;
}

/** ドラッグ前のX位置を返す */
gint daemon_sprite_get_from_x(Sprite* sprite) {
  return sprite->from_x;
}

/** ドラッグ前のY位置を返す */
gint daemon_sprite_get_from_y(Sprite* sprite) {
  return sprite->from_y;
}
