/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * DMoveInfo class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/dmoveinfo.h,v $
 * $Id: dmoveinfo.h,v 1.1.1.1 2005/12/09 09:03:04 tokita Exp $
 */

#ifndef _DMOVEINFO_H_
#define _DMOVEINFO_H_

#include "dte.h"
#include <string>
#include <vector>
using namespace std;

/** DMoveInfo が格納出来る手の最大数 */
#define DMOVEINFOMAX 2000


/** 指し手. グラフの辺 */
struct MoveEdge {
  /** 指し手 */
  DTe te;

  /** 消費時間. 単位=秒 */
  int time;

  string te_comment;
  int flag; // BAD, TSUMERO など
  struct KyokumenNode* node;

  MoveEdge(): node(NULL) {}

  ~MoveEdge() { 
    // node は解放しない 
  }
};


/** 局面と評価.
    とりあえず木構造で、合流は考えない。 */
struct KyokumenNode {
  typedef vector<MoveEdge> Moves;

  // ルート局面ではいずれもNULL
  KyokumenNode* const prev;
  DTe* move_to_back;

  /** 候補手. すべての手を格納するわけではない。 */
  Moves moves;

  /** 局面の評価. + で先手よし、- で後手よし */
  int value;

  string node_comment;

  KyokumenNode(KyokumenNode* p, const DTe* m): prev(p) { 
      move_to_back = m ? new DTe(*m) : NULL; 
  }

  ~KyokumenNode() { 
    Moves::iterator it;
    for (it = moves.begin(); it != moves.end(); it++)
      delete it->node;
    moves.clear();
    delete move_to_back;
  }

  /** 指し手（と進めたノード）を追加する
      @return   new node */
  KyokumenNode* add_child(const DTe& te, int sec) {
    MoveEdge p;
    p.te = te;
    p.time = sec;
    p.node = new KyokumenNode(this, &te);
    moves.push_back(p);
    return p.node;
  }

  void remove_child(const DTe& te) {
    Moves::iterator it;
    for (it = moves.begin(); it != moves.end(); it++) {
      if (it->te == te) {
	delete it->node;
	moves.erase(it);
	return;
      }
    }
  }

  /** 1手進んだノードを返す.
      @return 見つからなかったときは NULL */
  KyokumenNode* lookup_child(const DTe& te) const {
    Moves::const_iterator i;
    for (i = moves.begin(); i != moves.end(); i++) {
      if (i->te == te)
	return i->node;
    }
    return NULL;
  }
};


typedef KyokumenNode DMoveInfo; // for compatibility

/*********************************************************/
/* function prototypes */
/*********************************************************/

DMoveInfo* daemon_dmoveinfo_new   (KyokumenNode* p, const DTe* m);
void       daemon_dmoveinfo_init  (DMoveInfo* mi);
void       daemon_dmoveinfo_free  (DMoveInfo* mi);
void       daemon_dmoveinfo_add   (DMoveInfo* mi,
				   const DTe* te, int sec);

void daemon_dmoveinfo_pop(DMoveInfo* mi);

void       daemon_dmoveinfo_remove(DMoveInfo* mi,
				   int index);
void       daemon_dmoveinfo_output(DMoveInfo* mi,
				   FILE* out);


#endif /* _DMOVEINFO_H_ */
