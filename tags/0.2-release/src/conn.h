// -*- mode:c++ -*-
// daemonshogi - a shogi program.
// TCP/IP library.
// Copyright (c) 2008 HORIKAWA Hisashi.


#ifndef DAEMON_CONN_H
#define DAEMON_CONN_H

#include "si/si.h"
#include "dboard.h"
#include "misc.h"

G_BEGIN_DECLS


void network_log_append_message(const char* format, ...);

int connect_to_server(const char* hostname, const char* service);
void disconnect_server();

int csa_send_login(const char* username, const char* password);
int csa_send_move(const DBoard* board, int is_sente, const TE* te);
int csa_send_toryo();

int csa_wait_for_result(int* elapsed_time, int* minmax_return);

INPUTSTATUS daemon_input_next_network_impl(BOARD* bo, TE* te);

G_END_DECLS

#endif
