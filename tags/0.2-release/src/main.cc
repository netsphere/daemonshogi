/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/*
 * main.c 
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/main.c,v $
 * $Id: main.c,v 1.1.1.1 2005/12/09 09:03:06 tokita Exp $
 */

#include <config.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS
#include "interface.h"
#include "support.h"
G_END_DECLS

#include "canvas.h"
#include "misc.h"
#include "si/si.h"
#include "si/ui.h"

/** global application instance */
Canvas* g_canvas = NULL;

int main (int argc, char* argv[])
{
  GtkWidget *window;
  extern GAME g_game;

#ifdef ENABLE_NLS
  bindtextdomain(GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
  textdomain(GETTEXT_PACKAGE);
#endif

  gtk_set_locale();
  gtk_init(&argc, &argv);

  add_pixmap_directory(PACKAGE_DATA_DIR "/" PACKAGE "/pixmaps");

  g_canvas = NULL;

  /*
   * The following code was added by Glade to create one of each component
   * (except popup menus), just so that you see something after building
   * the project. Delete any components that you don't want shown initially.
   */
  window = create_window();
  gtk_widget_show(window);

  g_canvas = daemon_canvas_new(window);

  set_pending_function(pending_loop);

#if 0 // USE_SERIAL
  gmSetDeviceName(&g_game, SENTE, "/dev/cua0");
  gmSetDeviceName(&g_game, GOTE, "/dev/cua1");
#endif /* USE_SERIAL */

  gtk_main();
  return 0;
}

