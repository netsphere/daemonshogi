/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * Canvas class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/canvas.c,v $
 * $Id: canvas.c,v 1.2 2005/12/09 10:06:25 tokita Exp $
 */

#define GTK_DISABLE_DEPRECATED 1
#include <config.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>

G_BEGIN_DECLS
#include "interface.h"
#include "support.h"
// #include "callbacks.h"
G_END_DECLS
#include "canvas.h"
#include "history_window.h"
#include "misc.h"
#include "si/si.h"
#include "si/ui.h"

#include "back.xpm"
#include "koma.xpm"


/** 初期化 */
static void daemon_canvas_init(Canvas *canvas, GtkWidget *window) 
{
  GtkWidget *widget;
  g_assert(window != NULL);
  g_assert(canvas != NULL);

  canvas->window      = window;
  daemon_canvas_set_size(canvas, D_CANVAS_SIZE_SMALL);
  canvas->pixmap[0]   = gdk_pixmap_new(window->window,
				       D_CANCAS_SIZE_SMALL_X,
				       D_CANCAS_SIZE_SMALL_Y,
				       -1);
  canvas->pixmap[1]   = gdk_pixmap_new(window->window,
				       D_CANCAS_SIZE_MIDDLE_X,
				       D_CANCAS_SIZE_MIDDLE_Y,
				       -1);
  canvas->pixmap[2]   = gdk_pixmap_new(window->window,
				       D_CANCAS_SIZE_BIG_X,
				       D_CANCAS_SIZE_BIG_Y,
				       -1);
  canvas->gc          = gdk_gc_new(window->window);
  canvas->drawingarea = lookup_widget(GTK_WIDGET(window), "drawingarea");
  canvas->drag        = FALSE;
  canvas->mode        = D_CANVAS_MODE_EDIT;
  canvas->back_src    = gdk_pixbuf_new_from_xpm_data(back_xpm);
  if (canvas->back_src == NULL) {
    daemon_abort("No enough memory in daemon_canvas_init(). back_src is NULL\n");
  }
  canvas->koma_src    = gdk_pixbuf_new_from_xpm_data(koma_xpm);
  if (canvas->back_src == NULL) {
    daemon_abort("No enough memory in daemon_canvas_init(). koma_src is NULL\n");
  }
  canvas->back[0] = NULL;
  canvas->back[1] = NULL;
  canvas->back[2] = NULL;
  daemon_canvas_create_back_pixmap(canvas);
  daemon_dboard_set_hirate(&(canvas->board));
  canvas->bookwindow  = NULL;

  canvas->network_log_window = NULL;
  canvas->move_property_dialog = NULL;

  canvas->tmp_pixmap = NULL;

  /* スプライト初期化 */
  daemon_canvas_init_sprite(canvas);
  /* スプライト配置 */
  daemon_canvas_dispose_sprite(canvas);
/*
  // 消費時間を表示
  daemon_canvas_draw_time(canvas);
*/
  /* タイムアウトをセット */
  /* gtk_timeout_add(1000, (GtkFunction)daemon_canvas_timeout, (gpointer)canvas); */

  canvas->record = NULL;
  canvas->front = D_SENTE;
  daemon_canvas_set_sensitive(canvas);
  canvas->flg_thinking = FALSE;
}


/** コンストラクタ */
Canvas* daemon_canvas_new(GtkWidget* window) 
{
  Canvas* canvas;

  g_assert(window != NULL); 

  canvas = new Canvas();
  if (canvas == NULL) {
    daemon_abort(_("No enough memory in daemon_canvas_new()."));
  }

  daemon_canvas_init(canvas, window);

  /* 背景描画 */
  daemon_canvas_draw_back(canvas);
  /* 時刻更新 */
  daemon_canvas_draw_time(canvas);
  /* スプライト描画 */
  daemon_canvas_draw_sprite_all(canvas);
  /* 更新 */
  daemon_canvas_draw_all(canvas);
  /* ステータスバー更新 */
  daemon_canvas_update_statusbar(canvas);

  return canvas;
}


/** デストラクタ */
void daemon_canvas_free(Canvas* canvas) 
{
  if (canvas != NULL)
    delete canvas;
}


void daemon_canvas_init_sprite(Canvas* canvas)
{
  GdkPixbuf *im;
  GdkPixbuf *im2;
  Sprite *sprite;
  gint num, i;

  for (i=0; i<SPRITEMAX; i++) {
    canvas->sprite[i] = NULL;
  }

  num = 0;
#ifdef NOREAD
  {
    /* デーモン君のスプライト */
    GdkPixmap *pixmap;
    
    pixmap = load_pixmap_from_data(canvas->window, daemon_xpm);
    sprite = daemon_sprite_new(pixmap, NULL, 128, 128);
    daemon_sprite_set_type(sprite, ETC_SPRITE);
    canvas->sprite[num++] = sprite;
  }
  {
    /* ロゴのスプライト */
    GdkPixmap *pixmap;
    
    pixmap = load_pixmap_from_data(canvas->window, logo2_xpm);
    sprite = daemon_sprite_new(pixmap, NULL, 128, 128);
    daemon_sprite_set_type(sprite, ETC_SPRITE);
    canvas->sprite[num++] = sprite;
  }
#endif /* NOREAD */

  // いくつかの大きさの駒pixmapを作る
  im = canvas->koma_src;
  gdk_pixbuf_render_pixmap_and_mask(im, 
				    &canvas->koma[0], &canvas->mask[0],
				    128);

  im2 = gdk_pixbuf_scale_simple(im, 400, 200, GDK_INTERP_NEAREST);
  if (im2 == NULL) {
    daemon_abort("No enough memory in daemon_canvas_init_sprite(). im2 is NULL\n");
  }
  gdk_pixbuf_render_pixmap_and_mask(im2, 
				    &canvas->koma[1], &canvas->mask[1],
				    128);
  g_object_unref(im2);

  im2 = gdk_pixbuf_scale_simple(im, 512, 256, GDK_INTERP_NEAREST);
  if (im2 == NULL) {
    daemon_abort("No enough memory in daemon_canvas_init_sprite(). im2 is NULL\n");
  }
  gdk_pixbuf_render_pixmap_and_mask(im2, 
				    &canvas->koma[2], &canvas->mask[2],
				    128);
  g_object_unref(im2);

  for (i=0; i<SPRITEMAX; i++) {
    sprite = daemon_sprite_new(canvas->koma[0], canvas->mask[0], 40, 40);
    canvas->sprite[num] = sprite;
    daemon_sprite_set_type(sprite, KOMA_SPRITE);
    daemon_sprite_set_src_x(sprite, 0);
    daemon_sprite_set_src_y(sprite, 0);
    daemon_sprite_set_width(sprite, 40);
    daemon_sprite_set_height(sprite, 40);
    daemon_sprite_set_visible(sprite, FALSE);
    daemon_sprite_set_move(canvas->sprite[num], TRUE);
    num++;
  }
}


/** 持ち駒の種類の数を数える */
static int count_piece(const DBoard* board, D_TEBAN next) 
{
  gint count;
  gint i;

  count = 0;
  for (i=1; i <= 7; i++) {
    if (0 < daemon_dboard_get_piece(board, next, i)) {
      count++;
    }
  }

  return count;
}


/** スプライトを配置する */
void daemon_canvas_dispose_sprite(Canvas *canvas) {
  DBoard* board;
  Sprite* sprite;
  gint x, y, num;
  gint width, height;
  gint base_x, base_y;
  gint p;
  gint i, j;
  
  board = &(canvas->board);
  num = 0;

  base_x = 0;
  base_y = 0;

  if (canvas->size == D_CANVAS_SIZE_SMALL) {
    base_x = 140;
    base_y = 10;
  } else if (canvas->size == D_CANVAS_SIZE_MIDDLE) {
    base_x = 140 * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X;
    base_y = 10 * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X;
  } else if (canvas->size == D_CANVAS_SIZE_BIG) {
    base_x = 140 * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X;
    base_y = 10 * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X;
  } 
  else {
    /* 仕様上ありえない分岐 */
    g_assert(0);
  }
  
  for (i=0; i<SPRITEMAX; i++) {
    sprite = daemon_canvas_get_sprite(canvas, i);
    if (sprite == NULL) {
      continue;
    }
    if (daemon_sprite_get_type(sprite) != KOMA_SPRITE) {
      continue;
    }
    daemon_sprite_set_visible(sprite, FALSE);
  }

#ifdef NOREAD
  /* デーモン君のスプライト */
  sprite = daemon_canvas_get_sprite(canvas, num++);
  daemon_sprite_set_visible(sprite, TRUE);
  daemon_sprite_set_move(sprite, TRUE);
  /* ロゴのスプライト */
  sprite = daemon_canvas_get_sprite(canvas, num++);
  daemon_sprite_set_visible(sprite, TRUE);
  daemon_sprite_set_move(sprite, TRUE);
  daemon_sprite_set_x(sprite, 640 - 129);
  daemon_sprite_set_y(sprite, 1);
#endif /* NOREAD */

  /* 盤に駒を配置 */
  for (y=1; y <= 9; y++) {
    for (x=1; x <= 9; x++) {
      p = daemon_dboard_get_board(board, x, y);
      if (!p)
	continue;

      g_assert(1 <=p && p <= 0x1F);
      sprite = daemon_canvas_get_sprite(canvas, num++);
      g_assert(sprite != NULL);
      while (sprite->type != KOMA_SPRITE) {
	sprite = daemon_canvas_get_sprite(canvas, num++);
	g_assert(sprite != NULL);
      }
      width  = daemon_sprite_get_width(sprite);
      height = daemon_sprite_get_height(sprite);
      if (daemon_canvas_get_front(canvas) == D_SENTE) {
	daemon_sprite_set_x(sprite, (10 - x) * width - width + base_x);
	daemon_sprite_set_y(sprite, y * height - height + base_y);
	daemon_sprite_set_koma(sprite, p);
      }
      else {
	daemon_sprite_set_x(sprite, (10 - (10-x)) * width - width + base_x);
	daemon_sprite_set_y(sprite, (10-y) * height - height + base_y);
	daemon_sprite_set_koma(sprite, p ^ 0x10);
      }
      daemon_sprite_set_visible(sprite, TRUE);
    }
  }

  /* 持ち駒を配置する */
  {
    static const int base_y_value[9] = {
      0, 65, 65, 45, 45, 25, 5, 5, 5,
      // {0, 65, 65, 85, 85, 105, 125, 125, 125},
    };
    gint count, flg;
    gint x, y, skip_x;
    int komadai;

    // komadai = 0 で画面右手
    for (komadai = 0; komadai < 2; komadai++) {
      D_TEBAN teban;
      if (daemon_canvas_get_front(canvas) == D_SENTE)
	teban = komadai == 0 ? D_SENTE : D_GOTE;
      else
	teban = komadai == 0 ? D_GOTE : D_SENTE;

      count = count_piece(&canvas->board, teban); // 持ち駒の種類の数
      if (komadai == 0) {
	base_x = 511; // (510,210) - (640,380)
	base_y = 210 + base_y_value[count];
      }
      else {
	base_x = 129 - 40; // (0,0) - (130,170)
	base_y = 170 - base_y_value[count] - 40;
      }
      flg = 0; // 手前のとき、=0で左側、=1で右側

      // 大駒から順に表示
      for (i = 7; i >= 1; i--) {
	int p = daemon_dboard_get_piece(board, teban, i);
	if (!p)
	  continue;

	if (i == 1) {
	  skip_x = 80;
	  if (flg == 1) {
	    base_y += komadai == 0 ? +40 : -40;
	    flg = 0;
	  }
	} 
	else
	  skip_x = 20;

	// 駒を重ねて表示
	for (j = 0; j < p; j++) {
	  x = 5 + (skip_x - skip_x / p * (j + 1)) + (flg ? 60 : 0);

	  sprite = daemon_canvas_get_sprite(canvas, num++);
	  g_assert(sprite != NULL);
	  while (sprite->type != KOMA_SPRITE) {
	    sprite = daemon_canvas_get_sprite(canvas, num++);
	    g_assert(sprite != NULL);
	  }

	  daemon_sprite_set_x(sprite, 
	      daemon_canvas_rate_xy(canvas, base_x + (komadai == 0 ? +x : -x)));
	  daemon_sprite_set_y(sprite, daemon_canvas_rate_xy(canvas, base_y));
	  daemon_sprite_set_koma(sprite, i + komadai * 0x10);
	  daemon_sprite_set_visible(sprite, TRUE);
	}

	if (flg == 1) {
	  base_y += komadai == 0 ? +40 : -40;
	  flg = 0;
	} 
	else
	  flg = 1;
      }
    }

    /* 駒箱 */
    if (daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_EDIT) {
    count = count_pbox(&(canvas->board));
    base_x = 1;
    base_y = 210;
    flg = 0;
    skip_x = 20;

    for (i=8; 1 <= i; i--) {
      p = daemon_dboard_get_pbox(board, i);
      if (p == 0) {
	continue;
      }
      if (0 && i == 1) {
	skip_x = 80;
	flg = 0;
      } else {
	skip_x = 20;
      }
      for (j=0; j<p; j++) {
	x = 5 + (skip_x - skip_x / p * (j + 1)) + (flg ? 60 : 0);
	y = base_y_value[count];

	sprite = daemon_canvas_get_sprite(canvas, num++);
	g_assert(sprite != NULL);
	while (sprite->type != KOMA_SPRITE) {
	  sprite = daemon_canvas_get_sprite(canvas, num++);
	  g_assert(sprite != NULL);
	}

	daemon_sprite_set_x(sprite, daemon_canvas_rate_xy(canvas, base_x + x));
	daemon_sprite_set_y(sprite, daemon_canvas_rate_xy(canvas, base_y + y));
	daemon_sprite_set_koma(sprite, i);
	daemon_sprite_set_visible(sprite, TRUE);
      }

      if (flg == 1) {
	base_y += 40;
	flg = 0;
      } else {
	flg = 1;
      }
    }
    }
  }
}

/*************************************************************/
/* set_* , get_* */
/*************************************************************/

GtkWidget* daemon_canvas_get_window(Canvas *canvas) {
  return canvas->window;
}

GdkGC* daemon_canvas_get_gc(Canvas *canvas) {
  return canvas->gc;
}

GdkPixmap *daemon_canvas_get_pixmap(Canvas *canvas) {
  g_assert(canvas != NULL);
  if (canvas->size == D_CANVAS_SIZE_SMALL) {
    return canvas->pixmap[0];
  } else if (canvas->size == D_CANVAS_SIZE_MIDDLE) {
    return canvas->pixmap[1];
  } else if (canvas->size == D_CANVAS_SIZE_BIG) {
    return canvas->pixmap[2];
  } else {
    /* 仕様上ありえない分岐 */
    g_assert(1);
  }

  return NULL;
}

GdkPixmap *daemon_canvas_get_back(Canvas *canvas) {
  g_assert(canvas != NULL);
  if (canvas->size == D_CANVAS_SIZE_SMALL) {
    return canvas->back[0];
  } else if (canvas->size == D_CANVAS_SIZE_MIDDLE) {
    return canvas->back[1];
  } else if (canvas->size == D_CANVAS_SIZE_BIG) {
    return canvas->back[2];
  } else {
    /* 仕様上ありえない分岐 */
    g_assert(1);
  }

  return NULL;
}

GtkWidget* daemon_canvas_get_drawinarea(Canvas *canvas) {
  g_assert(canvas != NULL);
  return canvas->drawingarea;
}

gboolean daemon_canvas_isdrag(Canvas *canvas) {
  g_assert(canvas != NULL);
  return canvas->drag;
}

void daemon_canvas_set_drag(Canvas *canvas, gboolean drag) {
  g_assert(canvas != NULL);
  g_assert(drag == TRUE || drag == FALSE);
  canvas->drag = drag;
}

gint daemon_canvas_get_dragno(Canvas *canvas) {
  g_assert(canvas != NULL);
  return canvas->drag_no;
}

void daemon_canvas_set_dragno(Canvas *canvas, gint no) {
  g_assert(canvas != NULL);
  g_assert(0 <= no);
  canvas->drag_no = no;
}

GdkPoint daemon_canvas_get_diff(Canvas *canvas) {
  g_assert(canvas != NULL);
  return canvas->diff;
}

void daemon_canvas_set_diff(Canvas *canvas, gint x, gint y) {
  g_assert(canvas != NULL);
  g_assert(0 <= x);
  g_assert(0 <= y);
  canvas->diff.x = x;
  canvas->diff.y = y;
}

D_CANVAS_MODE daemon_canvas_get_mode(Canvas *canvas) {
  return canvas->mode;
}

void daemon_canvas_set_mode(Canvas *canvas, D_CANVAS_MODE mode) {
  canvas->mode = mode;
}

/** canvas のサイズをセットする */
void daemon_canvas_set_size(Canvas *canvas, D_CANVAS_SIZE size) {
  g_assert(canvas != NULL);
  g_assert(size == D_CANVAS_SIZE_SMALL ||
	   size == D_CANVAS_SIZE_MIDDLE ||
	   size == D_CANVAS_SIZE_BIG);

  canvas->size = size;
  if (size == D_CANVAS_SIZE_SMALL) {
    canvas->width  = D_CANCAS_SIZE_SMALL_X;
    canvas->height = D_CANCAS_SIZE_SMALL_Y;
  } else if (size == D_CANVAS_SIZE_MIDDLE) {
    canvas->width  = D_CANCAS_SIZE_MIDDLE_X;
    canvas->height = D_CANCAS_SIZE_MIDDLE_Y;
  } else if (size == D_CANVAS_SIZE_BIG) {
    canvas->width  = D_CANCAS_SIZE_BIG_X;
    canvas->height = D_CANCAS_SIZE_BIG_Y;
  } else {
    /* 仕様上ありえない分岐 */
    g_assert(1);
  }
}

Sprite* daemon_canvas_get_sprite(Canvas* canvas, gint no) {
  g_assert(canvas != NULL);
  g_assert(0 <= no);
  g_assert(no < SPRITEMAX);

  return canvas->sprite[no];
}

void daemon_canvas_set_front(Canvas* canvas, D_TEBAN player) 
{
  canvas->front = player;
}

D_TEBAN daemon_canvas_get_front(const Canvas* canvas) 
{
  return canvas->front;
}


/*************************************************************/
/* event */
/*************************************************************/

void daemon_canvas_expose(Canvas *canvas,
			  GtkWidget *widget,
			  GdkEventExpose *event,
			  gpointer user_data) {
  int width, height;
  int x, y;
  GdkGC* gc;
  GtkWidget* drawingarea;
  GdkPixmap* pixmap;
  GdkRectangle rect;

  rect        = event->area;
  width       = rect.width;
  height      = rect.height;
  x           = rect.x;
  y           = rect.y;
  gc          = canvas->gc;
  drawingarea = canvas->drawingarea;
  pixmap      = daemon_canvas_get_pixmap(canvas);
  
  gdk_draw_pixmap(drawingarea -> window, 
		  drawingarea -> style -> fg_gc[GTK_WIDGET_STATE(drawingarea)],
		  pixmap,
		  x, y,
		  x, y,
		  width, height);
}

void daemon_canvas_motion_notify(Canvas *canvas, GdkEventMotion  *event) {
  GdkModifierType mask;
  gint x, y;
  gint no;
  Sprite* sprite;
  GdkPoint diff;
  GdkRectangle rect;

  g_assert(canvas != NULL);
  g_assert(event != NULL);

  if (event->is_hint) {
    gdk_window_get_pointer (event->window, &x, &y, &mask);
  } else {
    x = event->x;
    y = event->y;
  }
  
  if (daemon_canvas_isdrag(canvas) == FALSE) {
    /* ドラッグ中でなければ戻る */
    return;
  }

  if (canvas->tmp_pixmap == NULL) {
    daemon_canvas_create_tmp_pixmap(canvas);
  }

  /* ダブルクリック判定用時刻を初期化 */
  canvas->tv.tv_sec  = 0;
  canvas->tv.tv_usec = 0;

  no     = daemon_canvas_get_dragno(canvas);
  sprite = daemon_canvas_get_sprite(canvas, no);
  diff   = daemon_canvas_get_diff(canvas);

  rect.x      = daemon_sprite_get_x(sprite);
  rect.y      = daemon_sprite_get_y(sprite);
  rect.width  = daemon_sprite_get_width(sprite);
  rect.height = daemon_sprite_get_height(sprite);

  daemon_sprite_set_x(sprite, event->x - diff.x);
  daemon_sprite_set_y(sprite, event->y - diff.y);

  /* 背景を描く */
  daemon_canvas_draw_back_update_from_tmp_pixmap(canvas, &rect);
  daemon_canvas_draw_sprite(canvas, no);
  daemon_canvas_draw_all_update(canvas, &rect);
}

void daemon_canvas_button_press(Canvas *canvas, GdkEventButton  *event) {
  gint     no;
  gboolean doubleclick = FALSE;
  Sprite* sprite;
  gint sprite_x, sprite_y;
  gint dest_x, dest_y;
  D_CANVAS_AREA area;
  SPRITE_TYPE type;
  D_CANVAS_MODE mode;
  gint p;

  g_assert(canvas != NULL);
  g_assert(event != NULL);

  if (event->type != GDK_BUTTON_PRESS)
    return;

  if (event->button != 1) {
    /* 1 番のボタン以外が押された */
    return;
  }

  if (canvas->mode == D_CANVAS_MODE_BOOK) {
    /* 棋譜モードではクリックの処理はしない */
    return;
  }

  if (daemon_canvas_isdrag(canvas) == TRUE) {
    /* ドラッグ中にクリックされた */
    daemon_canvas_set_drag(canvas, FALSE);
    return;
  }

  no = daemon_canvas_on_sprite(canvas, event);

  if (no == -1) {
    /* クリックされたスプライトなし */
    return;
  }

  sprite = daemon_canvas_get_sprite(canvas, no);
  doubleclick = daemon_canvas_is_doubleclick(canvas);

  sprite_x = daemon_sprite_get_x(sprite);
  sprite_y = daemon_sprite_get_y(sprite);
  area = daemon_canvas_on_drag_area(canvas, sprite_x, sprite_y, &dest_x, &dest_y);
  type = daemon_sprite_get_type(sprite);
  mode = daemon_canvas_get_mode(canvas);

  if (mode == D_CANVAS_MODE_GAME &&
      type == KOMA_SPRITE) {
    p = daemon_sprite_get_koma(sprite);

    if (daemon_canvas_get_front(canvas) == D_SENTE) {
      if (canvas->board.next == D_SENTE && 0x10 < p) {
	return;
      } else if (canvas->board.next == D_GOTE && p < 0x10) {
	return;
      }
    } 
    else {
      if (canvas->board.next == D_SENTE && p < 0x10) {
	return;
      } else if (canvas->board.next == D_GOTE && 0x10 < p) {
	return;
      }
    }

#ifdef NOREAD
    if (daemon_canvas_get_front(canvas) == D_SENTE && 0x10 < p) {
      return;
    } else if (daemon_dboard_get_front(&(canvas->board)) == D_GOTE && 0x10 < p) {
      return;
    }
#endif /* NOREAD */
  }

  if (mode == D_CANVAS_MODE_EDIT &&
      doubleclick && area == D_CANVAS_AREA_BOARD && type == KOMA_SPRITE) {
    /* ダブルクリックされた */
    gint koma;
    GdkRectangle rect;

    koma = daemon_sprite_get_koma(sprite);
    if ((koma & 0x0F) == 0x05 || (koma & 0x0F) == 0x08) {
      if (koma <= 0x10) {
	koma += 0x10;
      } else {
	koma -= 0x10;
      }
    } else if (koma <= 0x10) {
      if (koma & 0x08) {
	koma += 0x10;
      }
      koma ^= 0x08;
    } else {
      if (koma & 0x08) {
	koma -= 0x10;
      }
      koma ^= 0x08;
    }
    daemon_sprite_set_koma(sprite, koma);
    daemon_dboard_set_board(&(canvas->board), dest_x, dest_y, koma);
    
    rect.x      = daemon_sprite_get_x(sprite);
    rect.y      = daemon_sprite_get_y(sprite);
    rect.width  = daemon_sprite_get_width(sprite);
    rect.height = daemon_sprite_get_height(sprite);
    /* 背景を描く */
    daemon_canvas_draw_back_update(canvas, &rect);
    /* 駒を描く */
    daemon_canvas_draw_sprite(canvas, no);
    daemon_canvas_draw_sprite_update_all(canvas, &rect);
    daemon_canvas_draw_sprite_all(canvas);
    
    daemon_canvas_draw_all(canvas);
    
    return;
  }

  {
    /* スプライト上でクリックされた */
    gint x, y;

    if (daemon_sprite_ismove(sprite) == TRUE) {
      x = daemon_sprite_get_x(sprite);
      y = daemon_sprite_get_y(sprite);

      daemon_sprite_set_from_x(sprite, x);
      daemon_sprite_set_from_y(sprite, y);

      daemon_canvas_set_dragno(canvas, no);
      daemon_canvas_set_diff(canvas, event->x - x, event->y - y);
      daemon_canvas_set_drag(canvas, TRUE);
    }
  }
}

/**
 * (x, y) の座標が盤面のどのエリアにあるか調べる。<br>
 * 返り値が<UL>
 * <LI> 0 : 該当エリアなし
 * <LI> 1 : 左上駒台
 * <LI> 2 : 右下駒台
 * <LI> 3 : 左下駒箱
 * <LI> 4 : 盤の上
 * </UL>
 * 返り値が 4 の場合は、その盤上の座標をdest_x dest_y にセットする
 *  (1, 1) 〜 (9, 9) の値。
 */
D_CANVAS_AREA daemon_canvas_on_drag_area(Canvas *canvas, gint x, gint y,
			       gint *dest_x, gint *dest_y) {
  GdkPoint point;
  GdkRectangle left_koma, right_koma, komabako, board;

  point.x = daemon_canvas_rerate_xy(canvas, x) + D_SPRIRE_KOMA_WIDTH / 2;
  point.y = daemon_canvas_rerate_xy(canvas, y) + D_SPRIRE_KOMA_HEIGHT / 2;

  left_koma.x = 1;
  left_koma.y = 1;
  left_koma.width  = 129;
  left_koma.height = 169;
  if (daemon_on_rectangle(&left_koma, &point) == TRUE) {
    return D_CANVAS_AREA_LEFT_KOMADAI;
  }

  right_koma.x = 511;
  right_koma.y = 210;
  right_koma.width  = 129;
  right_koma.height = 169;
  if (daemon_on_rectangle(&right_koma, &point) == TRUE) {
    return D_CANVAS_AREA_RIGHT_KOMADAI;
  }

  komabako.x = 1;
  komabako.y = 210;
  komabako.width  = 129;
  komabako.height = 169;
  if (daemon_on_rectangle(&komabako, &point) == TRUE) {
    return D_CANVAS_AREA_KOMABAKO;
  }

  board.x = 140;
  board.y = 11;
  board.width  = 360;
  board.height = 360;
  if (daemon_on_rectangle(&board, &point) == TRUE) {
    /* 10 - は駒の座標の右向き、左向きを反転させるため */
    *dest_x = 10 - ((point.x - board.x) / D_SPRIRE_KOMA_WIDTH + 1);
    *dest_y = (point.y - board.y) / D_SPRIRE_KOMA_HEIGHT + 1;
    return D_CANVAS_AREA_BOARD;
  }

  return D_CANVAS_AREA_NONE;
}


extern int MATE_DEEP_MAX;
extern int THINK_DEEP_MAX;

static void button_release_on_edit_mode(Canvas* canvas, GdkEventButton* event)
{
    gint to_x, to_y;
    gint from_x, from_y;
    gint sprite_x, sprite_y;
    gint sprite_from_x, sprite_from_y;
    gint no;
    gint to_ret, from_ret;
    Sprite *sprite;
    gint p, p2;
    SPRITE_TYPE type;

    no            = daemon_canvas_get_dragno(canvas);
    sprite        = daemon_canvas_get_sprite(canvas, no);
    sprite_x      = daemon_sprite_get_x(sprite);
    sprite_y      = daemon_sprite_get_y(sprite);
    sprite_from_x = daemon_sprite_get_from_x(sprite);
    sprite_from_y = daemon_sprite_get_from_y(sprite);
    type          = daemon_sprite_get_type(sprite);

    to_x = to_y = 0;
    to_ret = daemon_canvas_on_drag_area(canvas, sprite_x, sprite_y,
					&to_x, &to_y);

    from_ret = daemon_canvas_on_drag_area(canvas, sprite_from_x, sprite_from_y,
					  &from_x, &from_y);

    p = daemon_sprite_get_koma(sprite);

    if (from_ret == D_CANVAS_AREA_NONE ||
	to_ret == D_CANVAS_AREA_NONE ||
	((p & 0xF) == 0x8 &&
	 (to_ret == D_CANVAS_AREA_LEFT_KOMADAI ||
	  to_ret == D_CANVAS_AREA_RIGHT_KOMADAI))) {
      /* 駒台、盤の外にドラッグしようとした */

      from_x = daemon_sprite_get_from_x(sprite);
      from_y = daemon_sprite_get_from_y(sprite);
      daemon_sprite_set_x(sprite, from_x);
      daemon_sprite_set_y(sprite, from_y);

      canvas_redraw(canvas);
    } 
    else if (to_x == from_x && to_y == from_y && 
	     from_ret == D_CANVAS_AREA_BOARD) {
      daemon_dboard_set_board(&(canvas->board), from_x, from_y, p);

      canvas_redraw(canvas);
    } 
    else if (type == KOMA_SPRITE) {
      if (from_ret == D_CANVAS_AREA_BOARD) {
	daemon_dboard_set_board(&(canvas->board), from_x, from_y, 0);
      } else if (from_ret == D_CANVAS_AREA_LEFT_KOMADAI) {
	daemon_dboard_dec_piece(&(canvas->board),
				daemon_canvas_get_front(canvas) ^ 1,
				p & 7);
      } else if (from_ret == D_CANVAS_AREA_RIGHT_KOMADAI) {
	daemon_dboard_dec_piece(&(canvas->board),
				daemon_canvas_get_front(canvas),
				p & 7);
      } else {
	p &= 0xF;
	p = (p != 8) ? p & 7 : p;
	daemon_dboard_dec_pbox(&(canvas->board), p);
      }
      if (to_ret == D_CANVAS_AREA_BOARD) {
	p2 = daemon_dboard_get_board(&(canvas->board), to_x, to_y);
	if ((p2 & 0xF) == 8) {
	  p2 &= 0xF;
	  p2 = (p2 != 8) ? p2 & 7 : p2;
	  daemon_dboard_add_pbox(&(canvas->board), p2);
	} else if (p2 != 0) {
	  if (daemon_canvas_get_front(canvas) == D_SENTE) {
	    daemon_dboard_add_piece(&(canvas->board),
				    (p < 0x10) ? D_SENTE : D_GOTE,
				    p2 & 7);
	  } else {
	    daemon_dboard_add_piece(&(canvas->board),
				    (p < 0x10) ? D_GOTE : D_SENTE,
				    p2 & 7);
	  }
	}
	daemon_dboard_set_board(&(canvas->board), to_x, to_y, p);
      } else if (to_ret == D_CANVAS_AREA_LEFT_KOMADAI) {
	daemon_dboard_add_piece(&(canvas->board),
				daemon_canvas_get_front(canvas) ^ 1,
				p & 7);
      } else if (to_ret == D_CANVAS_AREA_RIGHT_KOMADAI) {
	daemon_dboard_add_piece(&(canvas->board),
				daemon_canvas_get_front(canvas),
				p & 7);
      } else {
	p &= 0xF;
	p = (p != 8) ? p & 7 : p;
	daemon_dboard_add_pbox(&(canvas->board), p);
      }

      canvas_redraw(canvas);
    }
}


static void button_release_on_game_mode(Canvas* canvas, GdkEventButton* event)
{
    gint to_x, to_y;
    gint from_x, from_y;
    gint sprite_x, sprite_y;
    gint sprite_from_x, sprite_from_y;
    gint no;
    gint to_ret, from_ret;
    gint p;
    Sprite *sprite;
    SPRITE_TYPE type;

    no            = daemon_canvas_get_dragno(canvas);
    sprite        = daemon_canvas_get_sprite(canvas, no);
    sprite_x      = daemon_sprite_get_x(sprite);
    sprite_y      = daemon_sprite_get_y(sprite);
    sprite_from_x = daemon_sprite_get_from_x(sprite);
    sprite_from_y = daemon_sprite_get_from_y(sprite);
    type          = daemon_sprite_get_type(sprite);

    to_x = to_y = 0;
    to_ret = daemon_canvas_on_drag_area(canvas, sprite_x, sprite_y,
					&to_x, &to_y);

    from_ret = daemon_canvas_on_drag_area(canvas, sprite_from_x, sprite_from_y,
					  &from_x, &from_y);

    p = daemon_sprite_get_koma(sprite);

    if (from_ret == D_CANVAS_AREA_NONE ||
	to_ret == D_CANVAS_AREA_NONE ||
	to_ret != D_CANVAS_AREA_BOARD) {
      /* 駒台、盤の外にドラッグしようとした */

      from_x = daemon_sprite_get_from_x(sprite);
      from_y = daemon_sprite_get_from_y(sprite);
      daemon_sprite_set_x(sprite, from_x);
      daemon_sprite_set_y(sprite, from_y);

      canvas_redraw(canvas);
    } 
    else if (type == KOMA_SPRITE) {
      TE te, te2;
      gboolean just_te;
      gboolean just_nari;

      if (from_ret == D_CANVAS_AREA_LEFT_KOMADAI) {
	te.fm = 0;
	te.to = (to_y << 4) + to_x;
	te.nari = 0;
	te.uti = p & 7;
      } else if (from_ret == D_CANVAS_AREA_RIGHT_KOMADAI) {
	te.fm = 0;
	te.to = (to_y << 4) + to_x;
	te.nari = 0;
	te.uti = p & 7;
      } else {
	te.fm = (from_y << 4) + from_x;
	te.to = (to_y << 4) + to_x;
	te.nari = 0;
	te.uti = 0;
      }

      if (daemon_canvas_get_front(canvas) == D_GOTE) {
	gint x, y;
	if (te.fm) {
	  x = te.fm & 0xF;
	  y = te.fm >> 4;
	  te.fm = ((10 - y) << 4) + (10 - x);
	}
	x = te.to & 0xF;
	y = te.to >> 4;
	te.to = ((10 - y) << 4) + (10 - x);
      }

      te2 = te;
      if (te.uti == 0) {
	te2.nari = 1;
      }
      
      just_te = FALSE;
      just_nari = FALSE;

      if (boCheckTE(&g_board, &te)) {
	just_te = TRUE;
      } else if (boCheckTE(&g_board, &te2)) {
	just_te = TRUE;
	te.nari = 1;
      }

      if (te.nari == 0 && (p & 0xF) != OH && (p & 0xF) != KIN && (p & 8) == 0) {
	just_nari = TRUE;
      }

      if (just_te && just_nari && boCanNari(&g_board, &te)) {
	if (daemon_canvas_yes_no_dialog(canvas, _("Evolution ?")) == 
	    GTK_RESPONSE_YES) {
	  te.nari = 1;
	}
      }
      
      if (just_te) {
	canvas->gui_te = te;
#ifdef NOREAD
	/* スプライト再配置 */
	daemon_canvas_dispose_sprite(canvas);

	/* 画面再描画 */
	daemon_canvas_draw_back(canvas);
	daemon_canvas_draw_sprite_all(canvas);
	daemon_canvas_draw_time(canvas);
#endif /* NOREAD */
	daemon_canvas_draw_all(canvas);
      } 
      else {
	from_x = daemon_sprite_get_from_x(sprite);
	from_y = daemon_sprite_get_from_y(sprite);
	daemon_sprite_set_x(sprite, from_x);
	daemon_sprite_set_y(sprite, from_y);

	canvas_redraw(canvas);
      }
    }
}


void daemon_canvas_button_release(Canvas* canvas, GdkEventButton* event) 
{
  g_assert(canvas != NULL);
  g_assert(event != NULL);

  if (event->type != GDK_BUTTON_RELEASE || event->button != 1) {
    return;
  }

  if (daemon_canvas_isdrag(canvas) == FALSE)
    return;

  switch (daemon_canvas_get_mode(canvas)) {
  case D_CANVAS_MODE_EDIT:
    button_release_on_edit_mode(canvas, event);
    break;
  case D_CANVAS_MODE_GAME:
    button_release_on_game_mode(canvas, event);
    break;
  }

  daemon_canvas_set_drag(canvas, FALSE);
  g_object_unref(canvas->tmp_pixmap);
  canvas->tmp_pixmap = NULL;
}





/** 盤の大きさを変更する */
void daemon_canvas_change_size(Canvas *canvas, D_CANVAS_SIZE size) 
{
  g_assert(canvas);
  g_assert(size == D_CANVAS_SIZE_SMALL ||
	   size == D_CANVAS_SIZE_MIDDLE ||
	   size == D_CANVAS_SIZE_BIG);

  daemon_canvas_set_size(canvas, size);

  gtk_widget_set_size_request(canvas->drawingarea, 
			      canvas->width, canvas->height);

  daemon_canvas_change_size_sprite(canvas, size);

  // 再描画
  canvas_redraw(canvas);

  gtk_widget_show(canvas->drawingarea);
}


bool bookwindow_item_is_actived(Canvas* canvas) 
{
  GtkWidget* bookwindow_item = lookup_widget(canvas->window, "bookwindow");
  assert(bookwindow_item);
  return gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(bookwindow_item));
}


void bookwindow_item_set_active(Canvas* canvas, bool a)
{
  GtkWidget* bookwindow_item = lookup_widget(canvas->window, "bookwindow");
  assert(bookwindow_item);
  // gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(bookwindow_item), a);
  GTK_CHECK_MENU_ITEM(bookwindow_item)->active = a;
}


void history_window_show(Canvas* canvas)
{
  if (!canvas->bookwindow) {
    canvas->bookwindow = history_window_new();

    if (canvas->record != NULL) {
      history_window_sync(canvas->bookwindow, canvas->record);
      history_window_select_nth(canvas->bookwindow, canvas->cur_pos);
    }
  }
  gtk_widget_show(canvas->bookwindow);
  bookwindow_item_set_active(canvas, true);
}


/** モード変更 */
void daemon_canvas_change_mode(Canvas *canvas, D_CANVAS_MODE mode) 
{
  D_CANVAS_MODE before_mode;

  before_mode = daemon_canvas_get_mode(canvas);

  if (mode == D_CANVAS_MODE_EDIT) {
    daemon_dboard_set_for_edit(&(canvas->board));
    if (bookwindow_item_is_actived(canvas)) {
      gtk_widget_destroy(canvas->bookwindow);
      canvas->bookwindow = NULL;
      bookwindow_item_set_active(canvas, FALSE);
    }
    if (canvas->record != NULL) {
      // TODO: recordのクリア
      canvas->focus_line.clear();
    }
  }

  daemon_canvas_set_mode(canvas, mode);

  /* 背景の画像を編集モード用にする */
  daemon_canvas_create_back_pixmap(canvas);

  // 再描画
  canvas_redraw(canvas);

  /* ボタン、メニューの使用可／使用不可を設定する */
  daemon_canvas_set_sensitive(canvas);
  
  if (mode == D_CANVAS_MODE_BOOK) {
    daemon_canvas_set_kif_sensitive(canvas);
    if (!bookwindow_item_is_actived(canvas))
      history_window_show(canvas);
  }

  daemon_canvas_update_statusbar(canvas);
}


/** 画面の大きさ変更に合わせてスプライトの大きさを変更する */
void daemon_canvas_change_size_sprite(Canvas *canvas, D_CANVAS_SIZE size) {
  Sprite *sprite;
  GdkPixmap *koma;
  GdkBitmap *mask;
  gint width, height;
  gint i;

  koma   = NULL;
  mask   = NULL;
  width  = 0;
  height = 0;

  if (size == D_CANVAS_SIZE_SMALL) {
    koma = canvas->koma[0];
    mask = canvas->mask[0];
    width = 40;
    height = 40;
  } else if (size == D_CANVAS_SIZE_MIDDLE) {
    koma = canvas->koma[1];
    mask = canvas->mask[1];
    width = 50;
    height = 50;
  } else if (size == D_CANVAS_SIZE_BIG) {
    koma = canvas->koma[2];
    mask = canvas->mask[2];
    width = 64;
    height = 64;
  } else {
    /* 仕様上ありえない分岐 */
    g_assert(1);
  }

  for (i=0; i<SPRITEMAX; i++) {
    sprite = daemon_canvas_get_sprite(canvas, i);

    if (daemon_sprite_get_type(sprite) == ETC_SPRITE) {
      continue;
    }

    daemon_sprite_set_pixmap(sprite, koma);
    daemon_sprite_set_mask(sprite, mask);
    daemon_sprite_set_width(sprite, width);
    daemon_sprite_set_height(sprite, height);
  }
}

/*
void daemon_canvas_timeout(gpointer data)
{
  Canvas *canvas;

  canvas = (Canvas *)data;
  daemon_canvas_draw_time(canvas);
}
*/

/** 棋譜ファイルを読み込む */
void daemon_kiffile_load_ok_button_pressed(const char* filename)
{
  const gchar* buf;
  Record *record;
  D_LOADSTAT stat;
  GtkWidget *optionmenu;
  GtkWidget *widget;
  GtkWidget *menuitem;
  const gchar* menuitem_name;

  record = daemon_record_new();
  if (!record) {
    printf("new record error.\n"); 
    abort();
  }

  daemon_record_load(record, filename);
  stat = daemon_record_get_loadstat(record);
  if (stat != D_LOAD_SUCCESS) {
    // ファイル読み込み失敗
#ifndef NDEBUG
    KyokumenNode* node = record->mi;
    while (node) {
      if (node->moves.size() > 0) {
	KyokumenNode::Moves::const_iterator it;
	for (it = node->moves.begin(); it != node->moves.end(); it++)
	  daemon_dte_output(&it->te, stdout);
	printf("\n");
	node = node->moves.begin()->node;
      }
      else
	break;
    }
#endif // NDEBUG
    daemon_messagebox(g_canvas, _("A file did not open."), GTK_MESSAGE_ERROR);
    daemon_record_free(record);
    return;
  }

  // ファイル読み込み成功
  printf("%s: load success\n", __func__); // DEBUG

  daemon_record_free(g_canvas->record);
  g_canvas->record = record;

  daemon_dboard_copy(&(record->first_board), &(g_canvas->board));

  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_BOOK);

  // 注目する手筋を更新
  const KyokumenNode* p = record->mi;
  g_canvas->focus_line.clear();
  while (p && p->moves.size() > 0) {
    g_canvas->focus_line.push_back(p->moves.begin()->te);
    p = p->moves.begin()->node;
  }
  g_canvas->cur_pos = 0;
  history_window_clear(g_canvas->bookwindow);
  history_window_sync(g_canvas->bookwindow, g_canvas->record);
}


/** 棋譜ファイルを保存する */
void daemon_kiffile_save_ok_button_pressed(const char* filename, 
                                           enum KifuFileType filetype)
{
  const gchar* buf;
  int ret;
  GtkWidget *optionmenu;
  GtkWidget *widget;
  GtkWidget *menuitem;
  const gchar* menuitem_name;

  if (g_canvas->record == NULL) {
    g_canvas->record = daemon_record_new();
    daemon_dboard_copy(&(g_canvas->board), &(g_canvas->record->first_board));
  }
/*
  optionmenu = (GtkWidget *)lookup_widget(GTK_WIDGET(fileselection),
					      "optionmenu_save");
  g_assert(optionmenu != NULL);
  widget = gtk_option_menu_get_menu(GTK_OPTION_MENU(optionmenu));
  
  menuitem = gtk_menu_get_active(GTK_MENU(widget));
  menuitem_name = gtk_widget_get_name(GTK_WIDGET(menuitem));
*/

  ret = -1;
  switch (filetype)
  {
  case CSA_FORMAT:
    ret = daemon_record_output_csa(g_canvas->record, filename); 
    break;
  case KIF_FORMAT:
    ret = daemon_record_output_kif(g_canvas->record, filename); 
    break;
  default:
    assert(0);
  }

  if (ret != 0) {
    /* ファイル書き込み失敗 */
    daemon_messagebox(g_canvas, _("The writing of a file went wrong."),
		      GTK_MESSAGE_ERROR);
    return;
  }
}


/**
 * マウスカーソルがcanvasから出たときに呼ばれる。
 * ドラッグ中ならばスプライトを戻す。
 */
void daemon_canvas_leave_notify(Canvas *canvas,
				GtkWidget *widget,
				GdkEventCrossing *event,
				gpointer user_data) {
  gint no;
  Sprite *sprite;
  gint x, y;
  GdkPoint diff;
  GdkRectangle rect;

  if (daemon_canvas_isdrag(canvas) == FALSE) {
    return;
  }

  daemon_canvas_set_drag(canvas, FALSE);
  no     = daemon_canvas_get_dragno(canvas);
  sprite = daemon_canvas_get_sprite(canvas, no);
  diff   = daemon_canvas_get_diff(canvas);
  x      = daemon_sprite_get_from_x(sprite);
  y      = daemon_sprite_get_from_y(sprite);

  rect.x      = daemon_sprite_get_x(sprite);
  rect.y      = daemon_sprite_get_y(sprite);
  rect.width  = daemon_sprite_get_width(sprite);
  rect.height = daemon_sprite_get_height(sprite);

  daemon_sprite_set_x(sprite, x);
  daemon_sprite_set_y(sprite, y);

  /* 再描画 */
  daemon_canvas_draw_back_update_from_tmp_pixmap(canvas, &rect);
  daemon_canvas_draw_sprite(canvas, no);
  daemon_canvas_draw_all(canvas);
}


/** 主ウィンドウをすべて再描画 */
void canvas_redraw(Canvas* canvas)
{
  /* スプライト再配置 */
  daemon_canvas_dispose_sprite(canvas);

  /* 画面再描画 */
  daemon_canvas_draw_back(canvas);
  daemon_canvas_draw_sprite_all(canvas);
  daemon_canvas_draw_time(canvas);
  daemon_canvas_draw_all(canvas);
}



/** 棋譜モードで進む/戻るボタンを押したときによばれる */
void daemon_canvas_set_kif_sensitive(Canvas* canvas) 
{
  GtkWidget *widget;

  g_assert(canvas);
  g_assert(canvas->mode == D_CANVAS_MODE_BOOK);

  if (canvas->cur_pos == 0) {
    widget = lookup_widget(canvas->window, "first");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "back");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_first");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_back");
    gtk_widget_set_sensitive(widget, FALSE);

    widget = lookup_widget(canvas->window, "next");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "last");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_next");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_last");
    gtk_widget_set_sensitive(widget, TRUE);

    widget = lookup_widget(canvas->window, "retract");
    gtk_widget_set_sensitive(widget, FALSE);
  } 
  else if (canvas->focus_line.size() == canvas->cur_pos) {
    widget = lookup_widget(canvas->window, "first");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "back");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_first");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_back");
    gtk_widget_set_sensitive(widget, TRUE);

    widget = lookup_widget(canvas->window, "next");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "last");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_next");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_last");
    gtk_widget_set_sensitive(widget, FALSE);

    widget = lookup_widget(canvas->window, "retract");
    gtk_widget_set_sensitive(widget, FALSE);
  }
  else {
    widget = lookup_widget(canvas->window, "first");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "back");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_first");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_back");
    gtk_widget_set_sensitive(widget, TRUE);

    widget = lookup_widget(canvas->window, "next");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "last");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_next");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_last");
    gtk_widget_set_sensitive(widget, TRUE);

    widget = lookup_widget(canvas->window, "retract");
    gtk_widget_set_sensitive(widget, FALSE);
  }
}

void daemon_canvas_set_sensitive(Canvas *canvas) {
  GtkWidget *widget;

  switch (daemon_canvas_get_mode(canvas)) {
  case D_CANVAS_MODE_WAIT:
    break;
  case D_CANVAS_MODE_BOOK:
    widget = lookup_widget(canvas->window, "button_stop");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_give_up");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_first");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_back");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_next");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_last");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "retract");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "stop");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "give_up");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "first");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "back");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "next");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "last");
    gtk_widget_set_sensitive(widget, TRUE);

    widget = lookup_widget(canvas->window, "startedit");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "set_hirate");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "set_mate");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "rl_reversal");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "order_reversal");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "edit_flip");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "all_koma_to_pbox");
    gtk_widget_set_sensitive(widget, TRUE);

    widget = lookup_widget(canvas->window, "button_new_game");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "playgame");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "check_mate");
    gtk_widget_set_sensitive(widget, TRUE);

    widget = lookup_widget(canvas->window, "fileopen");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "filesave");
    gtk_widget_set_sensitive(widget, TRUE);
    break;
  case D_CANVAS_MODE_EDIT:
    widget = lookup_widget(canvas->window, "button_stop");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_give_up");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_first");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_back");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_next");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_last");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "retract");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "stop");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "give_up");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "first");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "back");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "next");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "last");
    gtk_widget_set_sensitive(widget, FALSE);

    widget = lookup_widget(canvas->window, "startedit");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "set_hirate");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "set_mate");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "rl_reversal");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "order_reversal");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "edit_flip");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "all_koma_to_pbox");
    gtk_widget_set_sensitive(widget, TRUE);

    widget = lookup_widget(canvas->window, "button_new_game");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "playgame");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "check_mate");
    gtk_widget_set_sensitive(widget, TRUE);

    widget = lookup_widget(canvas->window, "fileopen");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "filesave");
    gtk_widget_set_sensitive(widget, TRUE);
    break;
  case D_CANVAS_MODE_GAME:
    widget = lookup_widget(canvas->window, "button_stop");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_give_up");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "button_first");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_back");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_next");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_last");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "retract");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "stop");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "give_up");
    gtk_widget_set_sensitive(widget, TRUE);
    widget = lookup_widget(canvas->window, "first");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "back");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "next");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "last");
    gtk_widget_set_sensitive(widget, FALSE);

    widget = lookup_widget(canvas->window, "startedit");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "set_hirate");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "set_mate");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "rl_reversal");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "order_reversal");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "edit_flip");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "all_koma_to_pbox");
    gtk_widget_set_sensitive(widget, FALSE);

    widget = lookup_widget(canvas->window, "button_new_game");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "playgame");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "check_mate");
    gtk_widget_set_sensitive(widget, FALSE);

    widget = lookup_widget(canvas->window, "fileopen");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "filesave");
    gtk_widget_set_sensitive(widget, FALSE);
    break;
  case D_CANVAS_MODE_MATE:
    widget = lookup_widget(canvas->window, "button_stop");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_give_up");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_first");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_back");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_next");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_last");
    gtk_widget_set_sensitive(widget, FALSE);
    break;
  default:
    break;
  }
}


int daemon_canvas_mate_start_pressed(int is_sente)
{
  Canvas* canvas;

  canvas = g_canvas;

  daemon_dboard_copy_board(&(canvas->board), &g_board);
  boPlayInit(&g_board);
  boSetTsumeShogiPiece(&g_board);

  daemon_dboard_copy_from_board(&g_board, &(canvas->board));

  // 再描画
  canvas_redraw(canvas);

  if (is_sente) {
    g_board.next = SENTE;
    canvas->board.next = D_SENTE;
  } 
  else {
    g_board.next = GOTE;
    canvas->board.next = D_GOTE;
  }

  if (boCheckJustMate(&g_board) == 0) {
    daemon_messagebox(canvas, _("No good board."), GTK_MESSAGE_ERROR);
    return 0;
  }

  return 1;
}


/** 詰め将棋のmain loop */
void daemon_canvas_mate_loop()
{
  Canvas* canvas;
  GtkWidget* dialog;
  TREE root;
  int ret;
  MOVEINFO* mi = NULL;
  int i;

  canvas = g_canvas;

  dialog = gtk_message_dialog_new(GTK_WINDOW(canvas->window),
				  GTK_DIALOG_DESTROY_WITH_PARENT,
				  GTK_MESSAGE_INFO,
				  GTK_BUTTONS_CANCEL,
				  _("Now thinking..."));
  g_signal_connect_swapped(dialog, "response", 
			   G_CALLBACK(daemon_canvas_mate_think_cancel), NULL);
  gtk_widget_show(dialog);

  gmSetGameStatus(&g_game, SI_NORMAL);

#ifdef USE_HASH  
  /* ハッシュテーブル初期化 */
  newHASH();
#endif /* USE_HASH */

  initTREE(&root);

  ret = 0;

  for (i = 0; i < 8; i++ ) {
    MATE_DEEP_MAX = i;
    if ( mate(&g_board, &root, 0) ) {
      ret = 1;
      /* 詰んだ */
      mi = newMOVEINFO();
      trSetMoveinfo(root.child, mi);
      break;
    }
  }

#ifdef USE_HASH  
  /* ハッシュテーブル解放 */
  freeHASH();
#endif /* USE_HASH */

  gtk_widget_destroy(dialog);

  if (ret) {
    DTe te;

    daemon_messagebox(canvas, _("Check mate."), GTK_MESSAGE_INFO);

    if (canvas->record != NULL)
      daemon_record_free(canvas->record);
    canvas->record = daemon_record_new();

    daemon_dboard_copy(&(canvas->board), &(canvas->record->first_board));
    for (i = 0; i < mi->count; i++) {
      te.fm   = mi->te[i].fm;
      te.to   = mi->te[i].to;
      te.nari = mi->te[i].nari;
      te.uti  = mi->te[i].uti;
      daemon_record_add_dte(canvas->record, &te, 0);
      canvas->focus_line.push_back(te);
    }
    freeMOVEINFO(mi);
    // canvas->count = 0;
    // canvas->board.count = 0;
    daemon_canvas_change_mode(canvas, D_CANVAS_MODE_BOOK);
  } 
  else {
    daemon_messagebox(canvas, _("No check mate."), GTK_MESSAGE_INFO);
  }
}


/** OK, Cancel ボタンのダイアログ */
gint daemon_canvas_ok_cancel_dialog(Canvas* canvas, const char* message) 
{
  GtkWidget *dialog;
  gint reply;

  dialog = gtk_message_dialog_new(GTK_WINDOW(canvas->window),
				  GTK_DIALOG_DESTROY_WITH_PARENT,
				  GTK_MESSAGE_QUESTION,
				  GTK_BUTTONS_OK_CANCEL,
				  "%s", 
				  message);
  reply = gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);

  return reply;
}


/** ヘルパー: Yes, No ダイアログ */
int daemon_canvas_yes_no_dialog(Canvas* canvas, const char* message) 
{
  GtkWidget *dialog;
  gint reply;

  dialog = gtk_message_dialog_new(GTK_WINDOW(canvas->window),
				  GTK_DIALOG_DESTROY_WITH_PARENT,
				  GTK_MESSAGE_QUESTION,
				  GTK_BUTTONS_YES_NO,
				  "%s",
				  message);
  reply = gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);

  return reply;
}


/** 詰め将棋でのキャンセルボタン */
void daemon_canvas_mate_think_cancel()
{
  gmSetGameStatus(&g_game, SI_CHUDAN);
}


static PLAYERTYPE get_player_type(const char* option)
{
  PLAYERTYPE playertype;

  if (strcmp(option, _("HUMAN")) == 0) 
    playertype = SI_GUI_HUMAN;
  else if (strcmp(option, _("LEVEL1")) == 0)
    playertype = SI_COMPUTER_2;
  else if (strcmp(option, _("LEVEL2")) == 0) 
    playertype = SI_COMPUTER_3;
  else if (strcmp(option, _("LEVEL3")) == 0) 
    playertype = SI_COMPUTER_4;
  else if (strcmp(option, _("SERIALPORT1")) == 0) 
    playertype = SI_SERIALPORT_1;
  else if (strcmp(option, _("SERIALPORT2")) == 0) 
    playertype = SI_SERIALPORT_2;
  else {
    g_error("unknown player = '%s'", option);
  }

  return playertype;
}


/** メニュー -> ゲーム -> OK */
int daemon_canvas_new_game_ok(GtkWidget* dialog_game) 
{
  PLAYERTYPE playertype[2];
  TEAI teai;
  const gchar* player[2];
  const gchar* t;
  const gchar* playername[2];
  const gchar* tmp;
  int i;

  GtkWidget *menu_sente;
  GtkWidget *menu_gote;
  GtkWidget *menu_teai;
  GtkWidget *option_sente;
  GtkWidget *option_gote;
  GtkWidget *option_teai;
  GtkWidget *selected_sente;
  GtkWidget *selected_gote;
  GtkWidget *selected_teai;
  GtkWidget* entry_playername;

  menu_sente = lookup_widget(dialog_game, "optionmenu_sente");
  menu_gote  = lookup_widget(dialog_game, "optionmenu_gote");
  menu_teai  = lookup_widget(dialog_game, "optionmenu_teai");
  player[0] = gtk_combo_box_get_active_text(GTK_COMBO_BOX(menu_sente)); 
  player[1] = gtk_combo_box_get_active_text(GTK_COMBO_BOX(menu_gote));
  t = gtk_combo_box_get_active_text(GTK_COMBO_BOX(menu_teai));

  playertype[0] = get_player_type(player[0]);
  playertype[1] = get_player_type(player[1]);
  playername[0] = player[0];
  playername[1] = player[1];

  teai = HIRATE;
  if (strcmp(t, _("HIRATE")) == 0) {
    teai = HIRATE;
    daemon_dboard_set_hirate(&(g_canvas->board));
#ifdef NOREAD
  } else if (strcmp(t, _("NIMAIOTI")) == 0) {
    teai = NIMAIOTI;
  } else if (strcmp(t, _("YONMAIOTI")) == 0) {
    teai = YONMAIOTI;
#endif /* NOREAD */
  } 
  else if (strcmp(t, _("ETC")) == 0) {
    teai = TEAI_ETC;
  } else {
    g_error("unknown teai = '%s'", t);
  }

  if (teai == TEAI_ETC) {
    daemon_dboard_copy_board(&(g_canvas->board), &g_board);
    if (boCheckJustGame(&g_board) == 0) {
      // gtk_widget_destroy(g_canvas->dialog_game);
      daemon_messagebox(g_canvas, _("No good board."), GTK_MESSAGE_ERROR);
      return 0;
    }
  }

  for (i = 0; i < 2; i++) {
    entry_playername = lookup_widget(dialog_game, 
				     i == 0 ? "entry_sente" : "entry_gote");
    tmp = gtk_entry_get_text(GTK_ENTRY(entry_playername));
    if (tmp[0] == '\0') {
      if (playertype[i] == SI_GUI_HUMAN) {
	uid_t uid;
	struct passwd *curr_passwd;
	uid = getuid();
	curr_passwd = getpwuid(uid);
	playername[i] = curr_passwd->pw_name;
      }
      else 
	playername[i] = "COM";
    } 
    else 
      playername[i] = tmp;
  }

  daemon_game_setup(playertype[0], playername[0],
		    playertype[1], playername[1]);

  return 1;
}


void daemon_game_setup(PLAYERTYPE playertype1, const char* playername1,
		       PLAYERTYPE playertype2, const char* playername2)
{
  if (!g_canvas->record)
    g_canvas->record = daemon_record_new();

  daemon_record_set_player(g_canvas->record, D_SENTE, playername1);
  daemon_record_set_player(g_canvas->record, D_GOTE, playername2);

  g_canvas->playertype[0] = playertype1;
  g_canvas->playertype[1] = playertype2;

  // TODO: recordのクリア
  g_canvas->focus_line.clear();
  g_canvas->record->matchstat = D_UNKNOWN;
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_GAME);
}



/*************************************************************/
/* draw */
/*************************************************************/

void daemon_canvas_draw_all(Canvas *canvas) {
  int width, height;
  GdkGC* gc;
  GtkWidget* drawingarea;
  GdkPixmap* pixmap;

  width       = canvas->width;
  height      = canvas->height;
  gc          = canvas->gc;
  drawingarea = canvas->drawingarea;
  pixmap      = daemon_canvas_get_pixmap(canvas);

  gdk_draw_pixmap(drawingarea -> window,
		  drawingarea -> style -> fg_gc[GTK_WIDGET_STATE(drawingarea)],
		  pixmap,
		  0, 0,
		  0, 0,
		  width, height);
}

void daemon_canvas_draw_all_update(Canvas *canvas, GdkRectangle *rect) {
  GdkGC* gc;
  GtkWidget* drawingarea;
  GdkPixmap* pixmap;

  gc          = canvas->gc;
  drawingarea = canvas->drawingarea;
  pixmap      = daemon_canvas_get_pixmap(canvas);

  gdk_draw_pixmap(drawingarea -> window,
		  drawingarea -> style -> fg_gc[GTK_WIDGET_STATE(drawingarea)],
		  pixmap,
		  rect->x, rect->y,
		  rect->x, rect->y,
		  rect->width, rect->height);
}

void daemon_canvas_draw_back(Canvas *canvas) {
  GdkRectangle rect;
  
  rect.x = 0;
  rect.y = 0;
  rect.width  = canvas->width;
  rect.height = canvas->height;

  daemon_canvas_draw_back_update(canvas, &rect);
}

void daemon_canvas_draw_back_update(Canvas *canvas, GdkRectangle* rect) {
  GdkGC* gc;
  GdkPixmap* pixmap;
  GdkPixmap* back;
  
  gc     = canvas->gc;
  pixmap = daemon_canvas_get_pixmap(canvas);
  back   = daemon_canvas_get_back(canvas);
  
  set_color(gc, 0, 0, 0);
  gdk_gc_set_clip_rectangle(gc, rect);
  gdk_draw_pixmap(pixmap, gc, back, rect->x, rect->y, rect->x, rect->y,
		  rect->width, rect->height);
}

void daemon_canvas_draw_back_update_from_tmp_pixmap(Canvas *canvas, GdkRectangle *rect) {
  GdkGC* gc;
  GdkPixmap* pixmap;
  GdkPixmap* tmp_pixmap;
  GdkPixmap* back;
  
  gc         = canvas->gc;
  pixmap     = daemon_canvas_get_pixmap(canvas);
  tmp_pixmap = canvas->tmp_pixmap;
  back       = daemon_canvas_get_back(canvas);
  
  gdk_gc_set_clip_rectangle(gc, rect);
  gdk_draw_pixmap(pixmap, gc, tmp_pixmap, rect->x, rect->y, rect->x, rect->y,
		  rect->width, rect->height);
}

void daemon_canvas_draw_sprite_all(Canvas *canvas) {
  Sprite* sprite;
  gint i;

  g_assert(canvas != NULL);

  for (i=0; i<SPRITEMAX; i++) {
    sprite = daemon_canvas_get_sprite(canvas, i);

    if (sprite == NULL) {
      continue;
    }
    if (daemon_sprite_isvisible(sprite) == FALSE) {
      continue;
    }

    daemon_canvas_draw_sprite(canvas, i);
  }
}

void daemon_canvas_draw_sprite_update_all(Canvas *canvas, GdkRectangle* rect) {
  GdkRectangle s_rect;
  Sprite* sprite;
  gint    i;

  g_assert(canvas != NULL);

  for (i=0; i<SPRITEMAX; i++) {
    sprite = daemon_canvas_get_sprite(canvas, i);

    if (sprite == NULL) {
      continue;
    }
    if (daemon_sprite_isvisible(sprite) == FALSE) {
      continue;
    }
    s_rect.x      = daemon_sprite_get_x(sprite);
    s_rect.y      = daemon_sprite_get_y(sprite);
    s_rect.width  = daemon_sprite_get_width(sprite);
    s_rect.height = daemon_sprite_get_height(sprite);
    if (is_on_rectangle(rect, &s_rect)) {
      daemon_canvas_draw_sprite(canvas, i);
    }
  } 
}

void daemon_canvas_draw_sprite(Canvas *canvas, gint no) {
  GdkGC*     gc;
  GdkPixmap* pixmap;
  GdkPixmap* sprite_pixmap;
  GdkBitmap* mask;
  Sprite*    sprite;
  gint       width, height;
  gint       x, y;
  gint       src_x, src_y;

  g_assert(canvas != NULL);
  g_assert(0 <= no && no < SPRITEMAX);

  sprite = daemon_canvas_get_sprite(canvas, no);
  
  if (daemon_sprite_isvisible(sprite) == FALSE) {
    /* 表示しない */
    return;
  }
  
  gc            = canvas->gc;
  pixmap        = daemon_canvas_get_pixmap(canvas);
  sprite_pixmap = daemon_sprite_get_pixmap(sprite);
  mask          = daemon_sprite_get_mask(sprite);
  width         = daemon_sprite_get_width(sprite);
  height        = daemon_sprite_get_height(sprite);
  x             = daemon_sprite_get_x(sprite);
  y             = daemon_sprite_get_y(sprite);
  src_x         = daemon_sprite_get_src_x(sprite);
  src_y         = daemon_sprite_get_src_y(sprite);

  if (sprite->type == KOMA_SPRITE) {
    /* 駒の場合 */
    gdk_gc_set_clip_mask(gc, mask);
    gdk_gc_set_clip_origin(gc, x - src_x, y - src_y);
  } else {
    /* 駒以外 */
    GdkRectangle rect;
    
    rect.x = 0;
    rect.y = 0;
    rect.width  = sprite->width;
    rect.height = sprite->height;
  
    gdk_gc_set_clip_rectangle(gc, &rect);
    gdk_gc_set_clip_origin(gc, x, y);
  }
  gdk_draw_pixmap(pixmap, gc, sprite_pixmap, src_x, src_y, x, y,
		  width, height);
}


/** 消費時間を描く */
void daemon_canvas_draw_time(Canvas* canvas) 
{
  GdkGC*     gc;
  GdkPixmap* pixmap;
  GdkPixmap* back;
  // GdkFont *font;
  GdkRectangle rect;
  gint x, y, width, height, step;
  gchar buf[BUFSIZ];
  PangoLayout* layout;
  int teban;

  g_assert(canvas != NULL); 
  
  gc          = canvas->gc;
  pixmap      = daemon_canvas_get_pixmap(canvas);
  back        = daemon_canvas_get_back(canvas);
  rect.x      = 0;
  rect.y      = 0;
  rect.width  = canvas->width;
  rect.height = canvas->height;
  x = y = width = height = step = 0;

  for (teban = 0; teban < 2; teban++) {
    if (teban == 0 && daemon_canvas_get_front(canvas) == D_SENTE ||
	teban == 1 && daemon_canvas_get_front(canvas) == D_GOTE) {
      /* 画面右手 */
      x      = daemon_canvas_rate_xy(canvas, 516);
      y      = daemon_canvas_rate_xy(canvas, 171);
      width  = daemon_canvas_rate_xy(canvas, 129);
      height = daemon_canvas_rate_xy(canvas,  38);
      step   = daemon_canvas_rate_xy(canvas,  14);
    }
    else {
      /* 画面左手 */
      x      = daemon_canvas_rate_xy(canvas,   6);
      y      = daemon_canvas_rate_xy(canvas, 172);
      width  = daemon_canvas_rate_xy(canvas, 129);
      height = daemon_canvas_rate_xy(canvas,  38);
      step   = daemon_canvas_rate_xy(canvas,  14);
    }

    // font = gtk_style_get_font(canvas->window->style);
    set_color(gc, 0xFFFF, 0xFFFF, 0XFFFF);

    gdk_gc_set_clip_rectangle(gc, &rect);
    gdk_gc_set_clip_origin(gc, 0, 0);
    gdk_draw_pixmap(pixmap, gc, back, x, y, x, y, width, height);

    if (daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_GAME) {
      // int n;
      // n = teban == 0 ? daemon_dboard_get_front(&(canvas->board)) : 
      //                  daemon_dboard_get_front(&(canvas->board)) ^ 1;
      time2string(buf, g_board.total_time[teban]);
      buf[8] = '/';
      time2string(buf + 9, g_board.limit_time[teban]);
      // gdk_draw_string(pixmap, font, gc, x, y + step, buf);
      layout = gtk_widget_create_pango_layout(canvas->window, buf);
      gdk_draw_layout(pixmap, gc, x, y + step, layout);
      g_object_unref(layout);
    }

    if (canvas->record != NULL) {
      sprintf(buf, "%-20s", canvas->record->player[teban]);
      buf[16] = '\0';
      // gdk_draw_string(pixmap, font, gc, x, y + step * 2 + 3, buf);
      layout = gtk_widget_create_pango_layout(canvas->window, buf);
      gdk_draw_layout(pixmap, gc, x, y + step * 2 + 3, layout);
      g_object_unref(layout);
    }
  }

  /* 手番、前の手を表示 */
  x      = daemon_canvas_rate_xy(canvas, 516);
  y      = daemon_canvas_rate_xy(canvas, 131);
  width  = daemon_canvas_rate_xy(canvas, 129);
  height = daemon_canvas_rate_xy(canvas,  38);
  step   = daemon_canvas_rate_xy(canvas,  16);

  // font = gtk_style_get_font(canvas->window->style);
  set_color(gc, 0xFFFF, 0xFFFF, 0XFFFF);
  
  gdk_gc_set_clip_rectangle(gc, &rect);
  gdk_gc_set_clip_origin(gc, 0, 0);
  gdk_draw_pixmap(pixmap, gc, back, x, y, x, y, width, height);
  if (daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_GAME) {
    gint count;
    sprintf(buf, "%s", canvas->board.next == D_SENTE ? 
	    _("next SENTE(WHITE)") : _("next GOTE(BLACK)"));
    y += step;
    // gdk_draw_string(pixmap, font, gc, x, y, buf);
    layout = gtk_widget_create_pango_layout(canvas->window, buf);
    gdk_draw_layout(pixmap, gc, x, y, layout);
    g_object_unref(layout);

    y += step;
    count = canvas->focus_line.size();
    if (count > 0) {
      // 最後の手を表示
      DTe te = canvas->focus_line[count - 1];
      create_te_string2(&(canvas->board), count, &te, buf);
      layout = gtk_widget_create_pango_layout(canvas->window, buf);
      gdk_draw_layout(pixmap, gc, x, y, layout);
      g_object_unref(layout);
    }
  }

  daemon_canvas_draw_all_update(canvas, &rect);
}


/** ステータスバーを更新 */
void daemon_canvas_update_statusbar(Canvas *canvas) 
{
  GtkStatusbar* statusbar;
  static guint context_id = -1;
  const char* buf;

  if (daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_BOOK) {
    buf = _("BOOK MODE");
  } else if (daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_GAME) {
    buf = _("GAME MODE");
  } else if (daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_EDIT) {
    buf = _("EDIT MODE");
  } 
  else {
    assert(0);
  }

  statusbar = (GtkStatusbar*) lookup_widget(GTK_WIDGET(canvas->window), 
					    "statusbar");
  if (context_id != -1) {
    gtk_statusbar_pop(statusbar, context_id);
  }
  context_id = gtk_statusbar_get_context_id(statusbar, "status");
  gtk_statusbar_push(statusbar, context_id, buf);
}


/** ステータスバーを更新 */
void daemon_canvas_update_statusbar_te(Canvas* canvas, const DTe* te) 
{
  GtkStatusbar* statusbar;
  static guint context_id = -1;
  char buf[BUFSIZ];
  char buf2[BUFSIZ];
  gint count;

  if (daemon_canvas_get_mode(canvas) != D_CANVAS_MODE_GAME)
    return;

  count = canvas->focus_line.size();
  create_te_string(&canvas->board, count, te, buf);

  sprintf(buf2,
	  "%s %s --- %s", _("GAME MODE"),
	  buf, 
	  canvas->board.next == D_GOTE ? _("SENTE(WHITE)") : _("GOTE(BLACK)"));

  statusbar = (GtkStatusbar*) lookup_widget(GTK_WIDGET(canvas->window), 
					    "statusbar");
  if (context_id != -1) {
    gtk_statusbar_pop(statusbar, context_id);
  }
  context_id = gtk_statusbar_get_context_id(statusbar, "status");
  gtk_statusbar_push(statusbar, context_id, buf2);
}

/*************************************************************/
/* etc */
/*************************************************************/

/**
 * canvas->mode の値を見て back の内容を書き換える。
 *
 */
void daemon_canvas_create_back_pixmap(Canvas *canvas) {
  GdkRectangle rect;

  /* 駒台の座標 */
  static const gint x1 = 1, y1 = 210, x2 = 129, y2 = 378;

  g_assert(canvas != NULL);

  if (canvas->back[0] != NULL)
    g_object_unref(canvas->back[0]);
  if (canvas->back[1] != NULL)
    g_object_unref(canvas->back[1]);
  if (canvas->back[2] != NULL)
    g_object_unref(canvas->back[2]);

  canvas->back[0] = load_pixmap_from_imlib_image_scaled(canvas->window,
							canvas->back_src,
							D_CANCAS_SIZE_SMALL_X,
							D_CANCAS_SIZE_SMALL_Y);
  canvas->back[1] = load_pixmap_from_imlib_image_scaled(canvas->window,
							canvas->back_src,
							D_CANCAS_SIZE_MIDDLE_X,
							D_CANCAS_SIZE_MIDDLE_Y);
  canvas->back[2] = load_pixmap_from_imlib_image_scaled(canvas->window,
							canvas->back_src,
							D_CANCAS_SIZE_BIG_X,
							D_CANCAS_SIZE_BIG_Y);

  if (canvas->mode == D_CANVAS_MODE_EDIT) {
    return;
  }

  rect.x      = 0;
  rect.y      = 0;
  rect.width  = canvas->width;
  rect.height = canvas->height;

  gdk_gc_set_clip_rectangle(canvas->gc, &rect);
  gdk_gc_set_clip_origin(canvas->gc, 0, 0);

  /* gc に黒をセット */
  set_color(canvas->gc, 0, 0, 0);

  gdk_draw_rectangle(canvas->back[0], canvas->gc, TRUE, x1, y1, x2, y2);
  gdk_draw_rectangle(canvas->back[1],canvas->gc, TRUE,
		     x1 * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X,
		     y1 * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X,
		     x2 * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X + 1,
		     y2 * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X);
  gdk_draw_rectangle(canvas->back[2], canvas->gc, TRUE,
		     x1 * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X,
		     y1 * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X,
		     x2 * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X + 1,
		     y2 * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X);
}

/**
 * マウスカーソルがスプライトの上にあればそのスプライトの番号を返す。
 * それ以外は -1 を返す。
 * @param cavnas 対象のDragTestCanvas
 * @param event 対象のGdkEventButton
 */
gint daemon_canvas_on_sprite(Canvas* canvas, GdkEventButton  *event) {
  Sprite* sprite;
  gint x, y, width, height;
  gint i;

  for (i=0; i<SPRITEMAX; i++) {
    sprite = daemon_canvas_get_sprite(canvas, i);

    if (sprite == NULL) {
      continue;
    }

    if (daemon_sprite_ismove(sprite) == FALSE) {
      continue;
    }

    if (daemon_sprite_isvisible(sprite) == FALSE) {
      continue;
    }

    x      = daemon_sprite_get_x(sprite);
    y      = daemon_sprite_get_y(sprite);
    width  = daemon_sprite_get_width(sprite);
    height = daemon_sprite_get_height(sprite);

    if (x <= event->x && event->x <= x + width &&
	y <= event->y && event->y <= y + height) {
      /* スプライト上にマウスカーソルがある */
      return i;
    }
  }

  return -1;
}

/**
 * スプライトが盤上にあれば TREU を返す。それ以外は FALSE を返す。
 */
gboolean daemon_canvas_sprite_on_board(Canvas *canvas, Sprite *sprite) {
  GdkRectangle board;
  GdkPoint point;
  
  point.x = daemon_sprite_get_x(sprite);
  point.y = daemon_sprite_get_y(sprite);
  point.x = daemon_canvas_rate_xy(canvas, point.x);
  point.y = daemon_canvas_rate_xy(canvas, point.y);

  board.x = 140;
  board.y = 11;
  board.width  = 360;
  board.height = 360;

  return daemon_on_rectangle(&board, &point);
}

/**
 * ダブルクリックチェック。
 * @param cavnas 対象のDragTestCanvas
 * @return ダブルクリックならば TRUEを返す
 */
gboolean daemon_canvas_is_doubleclick(Canvas* canvas) {
  struct timeval  tv;
  struct timezone tz;

  gettimeofday(&tv, &tz);

  if (check_timeval(&(canvas->tv), &tv) == TRUE) {
    canvas->tv.tv_sec  = 0;
    canvas->tv.tv_usec = 0;
    return TRUE;
  }

  gettimeofday(&(canvas->tv), &tz);
  canvas->tv.tv_sec  = tv.tv_sec;
  canvas->tv.tv_usec = tv.tv_usec;

  return FALSE;
}

/** ドラッグ中に使う作業用の pixmap を作成する */
void daemon_canvas_create_tmp_pixmap(Canvas* canvas) {
  Sprite *sprite;
  GdkRectangle rect;
  GdkPixmap *pixmap;
  GdkGC *gc;
  gint no;

  g_assert(canvas != NULL);
  g_assert(daemon_canvas_isdrag(canvas) == 1);

  canvas->tmp_pixmap = gdk_pixmap_new(canvas->window->window, 
				      canvas->width,
				      canvas->height,
				      -1);
  if (canvas->tmp_pixmap == NULL) {
    daemon_abort("In daemon_canvas_create_tmp_pixmap(). tmp_pixmap is NULL.\n");
  }

  pixmap = daemon_canvas_get_pixmap(canvas);
  gc     = daemon_canvas_get_gc(canvas);
  
  no = daemon_canvas_get_dragno(canvas);
  sprite = daemon_canvas_get_sprite(canvas, no);
  rect.x      = daemon_sprite_get_x(sprite);
  rect.y      = daemon_sprite_get_y(sprite);
  rect.width  = daemon_sprite_get_width(sprite);
  rect.height = daemon_sprite_get_height(sprite);

  daemon_sprite_set_visible(sprite, FALSE);
  /* 背景を描く */
  daemon_canvas_draw_back_update(canvas, &rect);
  /* 駒を描く */
  daemon_canvas_draw_sprite_update_all(canvas, &rect);
  daemon_sprite_set_visible(sprite, TRUE);

  rect.x = 0;
  rect.y = 0;
  rect.width  = canvas->width;
  rect.height = canvas->height;
  gdk_gc_set_clip_rectangle(gc, &rect);
  gdk_draw_pixmap(canvas->tmp_pixmap, gc, pixmap, 0, 0, 0, 0,
		  canvas->width, canvas->height);

  daemon_canvas_draw_sprite(canvas, no);
}


/** メッセージボックスを表示 */
void daemon_messagebox(Canvas* canvas, const char* s, GtkMessageType mes_type)
{
  GtkWidget *messagebox;

  messagebox = gtk_message_dialog_new(GTK_WINDOW(canvas->window),
                                      GTK_DIALOG_DESTROY_WITH_PARENT,
                                      mes_type,
                                      GTK_BUTTONS_CLOSE,
                                      "%s",
                                      s);
  gtk_dialog_run(GTK_DIALOG(messagebox));
  gtk_widget_destroy(messagebox);
}


/** 拡大表示にあわせて、実際の座標を返す */
gint daemon_canvas_rate_xy(const Canvas* canvas, gint value) 
{
  if (canvas->size == D_CANVAS_SIZE_SMALL) {
  } else if (canvas->size == D_CANVAS_SIZE_MIDDLE) {
    value = value * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X;
  } else if (canvas->size == D_CANVAS_SIZE_BIG) {
    value = value * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X;
  } else {
    /* 仕様上ありえない分岐 */
    g_assert(0);
  }

  return value;
}

gint daemon_canvas_rerate_xy(Canvas* canvas, gint value) {
  if (canvas->size == D_CANVAS_SIZE_SMALL) {
  } else if (canvas->size == D_CANVAS_SIZE_MIDDLE) {
    value = value * D_CANCAS_SIZE_SMALL_X / D_CANCAS_SIZE_MIDDLE_X;
  } else if (canvas->size == D_CANVAS_SIZE_BIG) {
    value = value * D_CANCAS_SIZE_SMALL_X / D_CANCAS_SIZE_BIG_X;
  } else {
    /* 仕様上ありえない分岐 */
    g_assert(1);
  }

  return value;
}


/** 対局のメインルーチン */
void daemon_canvas_game(Canvas* canvas) 
{
  BOARD *board;
  GAME *game;
  int minmax_return;
  char send_buf[BUFSIZ];
  TE te;
  char te_buf[100];
  INPUTSTATUS (*si_input_next[2])(BOARD *bo, TE *te);
  int elapsed_time = -1;

  g_assert(canvas != NULL);
  g_assert(daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_GAME);

  si_input_next_gui_human = daemon_input_next_gui_human_impl;
  si_input_next_network = daemon_input_next_network_impl;

  board = &g_board;
  game = &g_game;

  game->player_number[SENTE] = canvas->playertype[D_SENTE];
  game->player_number[GOTE]  = canvas->playertype[D_GOTE];

  if (canvas->record == NULL)
    canvas->record = daemon_record_new();

  daemon_dboard_copy(&(canvas->board), &(canvas->record->first_board));
  // TODO: recordのクリア
  canvas->focus_line.clear();

  // 棋譜ウィンドウを整える
  if (canvas->bookwindow) {
    create_te_string(&canvas->board, 0, NULL, te_buf);
    history_window_clear(canvas->bookwindow);
    history_window_append_move(canvas->bookwindow, 0, te_buf, 0);
  }
  canvas->cur_pos = 0;

  boInit(board);
  daemon_dboard_copy_board(&(canvas->board), board);
  boPlayInit(board);

  gmSetInputFunc(game, SENTE, game->player_number[SENTE]);
  gmSetInputFunc(game, GOTE, game->player_number[GOTE]);
  si_input_next[SENTE] = gmGetInputNextPlayer(game, SENTE);
  si_input_next[GOTE]  = gmGetInputNextPlayer(game, GOTE);

  // canvas->record->time[D_SENTE] = 0;
  // canvas->record->time[D_GOTE] = 0;

#if 0 // USE_SERIAL
  if ( gmGetInputNextPlayer(game, SENTE ) == si_input_next_serialport ) {
    if (canvas->playertype[SENTE] == SI_SERIALPORT_1) {
      gmSetDeviceName(&g_game, SENTE, "/dev/cua0");
    } else {
      gmSetDeviceName(&g_game, SENTE, "/dev/cua1");
    }
    caStart(&(game->ca[SENTE]));
  }
  if ( gmGetInputNextPlayer(game, GOTE ) == si_input_next_serialport ) {
    if (canvas->playertype[GOTE] == SI_SERIALPORT_1) {
      gmSetDeviceName(&g_game, GOTE, "/dev/cua0");
    } else {
      gmSetDeviceName(&g_game, GOTE, "/dev/cua1");
    }
    caStart(&(game->ca[GOTE]));
  }
#endif /* USE_SERIAL */

  if (gmGetInputNextPlayer(game, SENTE ) != si_input_next_gui_human &&
      gmGetInputNextPlayer(game, GOTE ) != si_input_next_gui_human) {
    GtkWidget *widget;
    widget = lookup_widget(canvas->window, "give_up");
    gtk_widget_set_sensitive(widget, FALSE);
    widget = lookup_widget(canvas->window, "button_give_up");
    gtk_widget_set_sensitive(widget, FALSE);
  }

  gmSetGameStatus(game, 0);

#ifdef USE_HASH  
  /* ハッシュテーブル初期化 */
  newHASH();
#endif /* USE_HASH */

  // main loop
  while ( 1 ) {
    DTe dte;

    elapsed_time = -1;
    THINK_DEEP_MAX = gmGetComputerLevel(game, board->next);
    /* 現在の時間を記録する */
    boSetNowTime(board);
    minmax_return = (*si_input_next[board->next])(board, &te);
    /* 消費時間を記録する */
    boSetUsedTime(board);

    if (si_input_next[board->next == 0] == si_input_next_network) {
      if (minmax_return == SI_NORMAL) {
	csa_send_move(&canvas->board, board->next == 0, &te);
	csa_wait_for_result(&elapsed_time, &minmax_return);
      }
      else if (minmax_return == SI_TORYO) {
	csa_send_toryo();
	csa_wait_for_result(&elapsed_time, &minmax_return);
      }
    }
#ifdef USE_SERIAL
    else if ( minmax_return == SI_NORMAL &&
        si_input_next[(board->next == 0)] == si_input_next_serialport ) {
      /* 次の手番がシリアルポートの場合は手を送信する */
      getTE2SerialString(send_buf, board, &te);
      caSend(&(game->ca[(board->next == 0)]), send_buf);
    }
#endif /* USE_SERIAL */

    if (minmax_return != SI_NORMAL)
      break;
      
    if (te.fm == 0 && te.to == 0)
      break;

    play_move( board, &te );

    dte.fm   = te.fm;
    dte.to   = te.to;
    dte.nari = te.nari;
    dte.uti  = te.uti;
    dte.tori = canvas->board.board[dte.to];

    if (elapsed_time < 0)
      elapsed_time = board->used_time[board->mi.count - 1];
    
    daemon_record_add_dte(canvas->record, &dte, elapsed_time);
    canvas->focus_line.push_back(dte);

    // 棋譜を追加
    create_te_string(&canvas->board, board->mi.count, &dte, te_buf);
    history_window_append_move(canvas->bookwindow, board->mi.count, te_buf, 
			       elapsed_time);
    history_window_select_nth(canvas->bookwindow, board->mi.count);
    canvas->cur_pos++;

    daemon_canvas_update_statusbar_te(canvas, &dte);
    daemon_dboard_move(&(canvas->board), &dte);

    // 再描画
    canvas_redraw(canvas);
  }

  /* ステータスの判定処理 */
  if (minmax_return == SI_TORYO) {
    if (board->next == SENTE) {
      daemon_messagebox(canvas, _("Gote(white) win."), GTK_MESSAGE_INFO);
    } else {
      daemon_messagebox(canvas, _("Sente(black) win."), GTK_MESSAGE_INFO);
    }
  } else if ( minmax_return == SI_WIN ) {
    if (board->next == SENTE) {
      daemon_messagebox(canvas, _("Sente(black) win."), GTK_MESSAGE_INFO);
    } else {
      daemon_messagebox(canvas, _("Gote(white) win."), GTK_MESSAGE_INFO);
    }
  } else if ( minmax_return == SI_CHUDAN ) {
    daemon_messagebox(canvas, _("Stoped."), GTK_MESSAGE_INFO);
  } else if ( minmax_return == SI_CHUDAN_RECV ) {
    daemon_messagebox(canvas, _("Stoped."), GTK_MESSAGE_INFO);
  } else if ( minmax_return == SI_NGDATA ) {
    daemon_messagebox(canvas, _("No good data."), GTK_MESSAGE_ERROR);
  } else {
      /* 仕様上ありえない分岐 */
    g_assert(0);
  }

#ifdef USE_HASH  
  /* ハッシュテーブル解放 */
  freeHASH();
#endif /* USE_HASH */

  // daemon_dboard_copy(&(canvas->record->first_board), &(canvas->board));
  /// canvas->count = 0;
  daemon_canvas_set_mode(canvas, D_CANVAS_MODE_BOOK);
  // on_go_last_activate(NULL, NULL);
  daemon_canvas_change_mode(canvas, D_CANVAS_MODE_BOOK);
}


/**
 * 人間のGUIからの一手を te に格納する。
 * @param bo 対象のBOARD
 * @param te TE
 * @return GAMESTAT
 */
INPUTSTATUS daemon_input_next_gui_human_impl(BOARD* bo, TE* te) 
{
  TE tmp_te;
  TE *gui_te;

  gui_te = &(g_canvas->gui_te);

  teSetTE(&tmp_te, 0 , 0, 0, 0);
  teSetTE(gui_te, 0 , 0, 0, 0);
  
  while ( teCmpTE(&tmp_te, gui_te) == 0 ) {
    if ( gmGetGameStatus(&g_game) != 0 ) {
      /* 中断または、投了が入った。 */
      return gmGetGameStatus(&g_game);
    }
    processing_pending();
    usleep( 50 );
  }
  
  *te = *gui_te;

  return SI_NORMAL;
}


/** 駒箱内の駒の種類の数を数える */
gint count_pbox(DBoard *board) {
  gint count;
  gint i;

  count = 0;
  for (i=1; i <= 8; i++) {
    if (0 < daemon_dboard_get_pbox(board, i)) {
      count++;
    }
  }

  return count;
}


/** 棋譜ウィンドウ/ステータスバーに表示する手の文字列を生成する。
 */
void create_te_string(const DBoard* board, int no, const DTe* te, char* buf) 
{
  const char* x[] = {
    "",     _("1_"), _("2_"), _("3_"), _("4_"),
    _("5_"),_("6_"), _("7_"), _("8_"), _("9_"),
  };
  const char* y[] = {
    "",     _("1 "), _("2 "), _("3 "), _("4 "),
    _("5 "),_("6 "), _("7 "), _("8 "), _("9 "),
  };
  const char* piece[] = {
    "",           _("FU"), _("KYO"),     _("KEI"),
    _("GIN"),     _("KIN"),_("KAKU"),    _("HISYA"),
    _("OH"),      _("TO"), _("NARIKYO"), _("NARIKEI"),
    _("NARIGIN"), "",      _("UMA"),     _("RYU"),
  };
  const char* nari = _(" NARI");
  const char* uti = _(" UTI");
  const char* game_start = _("game start");
  int p;

  if (!te) {
    sprintf(buf, "%s", game_start);
  } 
  else if (te->uti == 0) {
    p = daemon_dboard_get_board(board, te->fm & 0xF, te->fm >> 4);
    p &= 0xF;
    sprintf(buf, "%s%s%s%s%s(%d,%d)",
	    (no % 2) ? "▲" : "▽",
	    x[te->to & 0xF],
	    y[te->to >> 4],
	    piece[p],
	    te->nari ? nari : "",
	    te->fm & 0xF,
	    te->fm >> 4
	    );
  } 
  else {
    sprintf(buf, "%s%s%s%s%s",
	    (no % 2) ? "▲" : "▽",
	    x[te->to & 0xF],
	    y[te->to >> 4],
	    piece[te->uti],
	    uti);
  }
}


void create_te_string2(const DBoard* board, int no, const DTe* te, char* buf) 
{
  const char* x[10] = {
    "",     _("1_"), _("2_"), _("3_"), _("4_"),
    _("5_"),_("6_"), _("7_"), _("8_"), _("9_"),
  };
  const char* y[10] = {
    "",     _("1 "), _("2 "), _("3 "), _("4 "),
    _("5 "),_("6 "), _("7 "), _("8 "), _("9 "),
  };
  const char* piece[16] = {
    "",           _("FU"), _("KYO"),     _("KEI"),
    _("GIN"),     _("KIN"),_("KAKU"),    _("HISYA"),
    _("OH"),      _("TO"), _("NARIKYO"), _("NARIKEI"),
    _("NARIGIN"), "",      _("UMA"),     _("RYU"),
  };
  const char* nari = _(" NARI");
  const char* uti = _(" UTI");
  const char* game_start = _("game start");
  const char* teme = _("teme");
  int p;
  DBoard *tmp_board;

  if (no == 0) {
    sprintf(buf, "%d %s %s",
	    no,
	    teme,
	    game_start);
  } 
  else if (te->uti == 0) {
    tmp_board = daemon_dboard_new();
    daemon_dboard_copy(board, tmp_board);
    daemon_dboard_back(tmp_board, te);

    p = daemon_dboard_get_board(tmp_board, te->fm & 0xF, te->fm >> 4);
    p &= 0xF;
    sprintf(buf, "%d %s %s%s%s%s(%d,%d)",
	    no,
	    teme,
	    x[te->to & 0xF],
	    y[te->to >> 4],
	    piece[p],
	    te->nari ? nari : "",
	    te->fm & 0xF,
	    te->fm >> 4
	    );
  } else {
    sprintf(buf, "%d %s %s%s%s%s",
	    no,
	    teme, 
	    x[te->to & 0xF],
	    y[te->to >> 4],
	    piece[te->uti],
	    uti
	    );
  }
}

