/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * DMoveInfo class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/dmoveinfo.c,v $
 * $Id: dmoveinfo.c,v 1.1.1.1 2005/12/09 09:03:04 tokita Exp $
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "dte.h"
#include "dmoveinfo.h"


/** コンストラクタ */
DMoveInfo* daemon_dmoveinfo_new(KyokumenNode* p, const DTe* m) 
{
  DMoveInfo* mi = new DMoveInfo(p, m);
  if (!mi) {
    printf("No enough memory in daemon_dmovenifo_new().");
    abort();
  }

  return mi;
}


/** デストラクタ */
void daemon_dmoveinfo_free(DMoveInfo* mi) 
{
  if (mi)
    delete mi;
}


/** main_line の先端に手を追加 */
void daemon_dmoveinfo_add(DMoveInfo* mi, const DTe* te, int sec) 
{
  assert(mi != NULL);
  assert(te != NULL);

  while (mi->moves.size() > 0)
    mi = mi->moves.begin()->node;

  mi->add_child(*te, sec);
}


/** main_line の先端の手を削除する */
void daemon_dmoveinfo_pop(DMoveInfo* mi) 
{
  assert(mi != NULL);

  while (mi->moves.size() > 0)
    mi = mi->moves.begin()->node;

  if (!mi->prev) // 一手もないとき
    return;

  DTe te = *mi->move_to_back;
  mi = mi->prev;
  mi->remove_child(te);
}


#if 0
/** index 番目の DTe へのポインタを得る */
const DTe* daemon_dmoveinfo_get(const DMoveInfo* mi, int index) 
{
  assert(mi != NULL);

  if (index >= mi->size())
    return NULL;

  return &mi->at(index).te;
}


/** index 番目の DTe を削除し空いた所に最後の要素を詰める */
void daemon_dmoveinfo_remove(DMoveInfo* mi, int index) {
  assert(mi != NULL);
  assert(index < DMOVEINFOMAX);
  if (DMOVEINFOMAX <= index) {
    return;
  }
  assert(index <= mi->count);
  if (mi->count < index) {
    return;
  }
  if (index == mi->count) {
    mi->count--;
    return;
  }
  mi->te[mi->count] = mi->te[index];
}


void daemon_dmoveinfo_output(DMoveInfo* mi, FILE* out) {
  int i;

  assert(mi != NULL);
  assert(out != NULL);

  for (i=0; i<mi->count; i++) {
    daemon_dte_output(&(mi->te[i]), out);
  }
}


int daemon_dmoveinfo_size(const DMoveInfo* mi) 
{
  return mi->size();
}
#endif
