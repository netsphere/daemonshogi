
// Copyright (c) 2008-2009 HORIKAWA Hisashi.
// All rights reserved.

#include "dte.h"

G_BEGIN_DECLS

GtkWidget* history_window_new();

void history_window_append_move(GtkWidget* history_window, 
				int count, const char* te_str, int sec);

void history_window_clear(GtkWidget* history_window);

void history_window_select_nth(GtkWidget* history_window, int nth);

void history_window_sync(GtkWidget* history_window, const Record* record);

G_END_DECLS
