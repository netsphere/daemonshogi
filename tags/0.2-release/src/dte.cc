/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * DTe class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/dte.c,v $
 * $Id: dte.c,v 1.1.1.1 2005/12/09 09:03:04 tokita Exp $
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "dte.h"

DTe* daemon_dte_new(void) {
  DTe* te;

  te = (DTe*) malloc(sizeof(DTe));
  if (te == NULL) {
    printf("No enough memory in daemon_dte_new().");
    abort();
  }

  return te;
}

void daemon_dte_free(DTe* te) {
  assert(te != NULL);
  free(te);
}

void daemon_dte_output(const DTe* te, FILE* out) {
  assert(te != NULL);
  assert(out != NULL);
  if (te->uti) {
    fprintf(out, "(%d,%d) %d uti\n", te->to & 0xF, te->to >> 4, te->uti);
  } else {
    fprintf(out, "(%d,%d)->(%d,%d)%s\n",
	    te->fm & 0xF, te->fm >> 4,
	    te->to & 0xF, te->to >> 4,
	    te->nari ? " nari" : "");
  }
}

