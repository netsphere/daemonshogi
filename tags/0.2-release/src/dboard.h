/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * DBoard class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/dboard.h,v $
 * $Id: dboard.h,v 1.1.1.1 2005/12/09 09:03:04 tokita Exp $
 */

#ifndef _DBOARD_H_
#define _DBOARD_H_

#include <stdio.h>
#include "dte.h"
#include "si/si.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/** 盤の外の値 */
#define DWALL (0x20)

/** 手番の値 */
typedef enum {
  /** 先手の値 */
  D_SENTE = 0,
  /** 後手の値 */
  D_GOTE = 1,
} D_TEBAN;


/** 局面情報クラス.
    BOARD と互換性はない. */
typedef struct {
  /** 盤面を記録する配列 */
  int board[256];
  /** 持ち駒を記録する配列 */
  int piece[2][8];

  /** 手番 */
  D_TEBAN next;

  /** 駒箱 */
  int pbox[9];
} DBoard;


/*********************************************************/
/* function prototypes */
/*********************************************************/

DBoard* daemon_dboard_new       (void);
void    daemon_dboard_init      (DBoard *bo);
void    daemon_dboard_free      (DBoard *bo);
void    daemon_dboard_set_board (DBoard *bo,
				 int x,
				 int y,
				 int p);
int     daemon_dboard_get_board (const DBoard* bo,
				 int x, int y);
void    daemon_dboard_set_piece (DBoard *bo,
				 int next,
				 int p,
				 int n);
void    daemon_dboard_add_piece (DBoard *bo,
				 int next,
				 int p);
void    daemon_dboard_dec_piece (DBoard *bo,
				 int next,
				 int p);
int     daemon_dboard_get_piece (const DBoard* bo,
				 int next,
				 int p);
void    daemon_dboard_set_hirate(DBoard *bo);
void    daemon_dboard_set_mate  (DBoard *bo);
void    daemon_dboard_set_rl_reversal(DBoard *bo);
void    daemon_dboard_set_order_reversal(DBoard *bo);
void    daemon_dboard_flip      (DBoard *bo);
void    daemon_dboard_set_all_to_pbox(DBoard *bo);
void    daemon_dboard_set_for_edit(DBoard *bo);
void    daemon_dboard_output    (const DBoard* bo, FILE *out); 
void    daemon_dboard_move      (DBoard *bo, const DTe* te);
void    daemon_dboard_back      (DBoard *bo, const DTe* te);
// int     daemon_dboard_can_back  (DBoard *bo);
void    daemon_dboard_copy      (const DBoard* src, DBoard *dest);
void    daemon_dboard_toggle_next(DBoard *bo);
void    daemon_dboard_set_pbox  (DBoard *bo, int p, int n);
int     daemon_dboard_get_pbox  (DBoard *bo, int p);
void    daemon_dboard_add_pbox  (DBoard *bo, int p);
void    daemon_dboard_dec_pbox  (DBoard *bo, int p);
void    daemon_dboard_copy_board(DBoard *src, BOARD *dest);
void    daemon_dboard_copy_from_board(BOARD *src, DBoard *dest);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _DBOARD_H_ */
