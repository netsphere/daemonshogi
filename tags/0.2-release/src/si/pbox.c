/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * pbox.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/pbox.c,v $
 * $Id: pbox.c,v 1.1.1.1 2005/12/09 09:03:10 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"

/**
 * 新しくメモリを確保してPBOXのポインタを返す。
 * @return PBOXへのポインタ
 */
PBOX *newPBOX(void) {
  PBOX *b;

  b = (PBOX *) malloc(sizeof(PBOX));
  if (b == NULL) {
    si_abort("No enough memory. In pbox.c#newPBOX()");
  }
  pbInit(b);

  return b;
}

/**
 * pb のメモリを解放する。
 * @param pb 対象のPBOX
 */
void freePBOX(PBOX *pb) {
#ifdef DEBUG
  assert( pb != NULL );
#endif /* DEBUG */
  
  free( pb );
}

/**
 * pb の内容を初期化する。使われていない駒としてPBOXへセットする。
 * @param pb 対象のPBOX
 */
void pbInit(PBOX * pb) {
  int i;
  
#ifdef DEBUG
  assert( pb != NULL );
#endif /* DEBUG */
  
  for (i = 0; i < 9; i++)
    pb->count[i] = 0;

  /* oh */
  pb->number[8][0] = 1;
  pb->number[8][1] = 2;
  /* hisya */
  pb->number[7][0] = 3;
  pb->number[7][1] = 4;
  /* kaku */
  pb->number[6][0] = 5;
  pb->number[6][1] = 6;
  /* kin */
  for (i = 0; i < 4; i++)
    pb->number[5][i] = 7 + i;
  /* gin */
  for (i = 0; i < 4; i++)
    pb->number[4][i] = 11 + i;
  /* kei */
  for (i = 0; i < 4; i++)
    pb->number[3][i] = 15 + i;
    /* kyosya */
  for (i = 0; i < 4; i++)
    pb->number[2][i] = 19 + i;
  /* fu */
  for (i = 0; i < 18; i++)
    pb->number[1][i] = 23 + i;
}

/**
 * 駒箱に駒を戻す。
 * @param pb 対象のPBOX
 * @param n 駒コード
 */
void pbPush(PBOX * pb, int n) {
#ifdef DEBUG
  assert( pb != NULL );
  assert( 1 <= n && n <=40 );
  
  /* 戻しすぎじゃないかチェック */
  if (23 <= n) {                   /* fu */
    assert(0 <= pb->count[1]-1);
  } else if (19 <= n) {            /* kyo */
    assert(0 <= pb->count[2]-1);
  } else if (15 <= n) {            /* kei */
    assert(0 <= pb->count[3]-1);
  } else if (11 <= n) {            /* gin */
    assert(0 <= pb->count[4]-1);
  } else if (7 <= n) {             /* kin */
    assert(0 <= pb->count[5]-1);
  } else if (5 <= n) {             /* kaku */
    assert(0 <= pb->count[6]-1);
  } else if (3 <= n) {             /* hisya */
    assert(0 <= pb->count[7]-1);
  } else if ( n == 1 || n == 2 ) { /* oh */
    assert(0 <= pb->count[8]-1);
  }
#endif /* DEBUG */
  
  if (23 <= n) {              /* fu */
    pb->number[1][--pb->count[1]] = n;
  } else if (19 <= n) {       /* kyo */
    pb->number[2][--pb->count[2]] = n;
  } else if (15 <= n) {       /* kei */
    pb->number[3][--pb->count[3]] = n;
  } else if (11 <= n) {       /* gin */
    pb->number[4][--pb->count[4]] = n;
  } else if (7 <= n) {        /* kin */
    pb->number[5][--pb->count[5]] = n;
  } else if (5 <= n) {        /* kaku */
    pb->number[6][--pb->count[6]] = n;
  } else if (3 <= n) {        /* hisya */
    pb->number[7][--pb->count[7]] = n;
  } else if (n == 2) {        /* gote oh */
    pb->count[8]--;
  } else if (n == 1) {        /* sente oh */
    pb->count[8]--;
  }
}

/**
 * 駒箱から駒を取り出す。
 * @param pb 対象の駒コード
 * @param p 駒種類
 * @return 取りだした駒の駒コード
 */
int pbPop(PBOX * pb, int p) {
#ifdef DEBUG
  assert( pb != NULL );
  assert( p );
  assert( 0x01<=(p&0x0F) && (p&0x0F)<=0x0F );
  assert( 0x00==(p&0x10) || 0x10==(p&0x10) );
  
  /* 取りだしすぎじゃないかチェック */
  if (p == 0x08 || p == 0x18) {
    assert(pb->count[8]+1 <= 2);
  }
#endif /* DEBUG */
  
  /* oh */
  if (p == 0x08) {
    pb->count[ 8 ]++;
    return pb->number[ 8 ][ 0 ];
  }
  if (p == 0x18) {
    pb->count[ 8 ]++;
    return pb->number[ 8 ][ 1 ];
  }

  p &= 0x0F;

  if ( 0x07 < p )
    p -= 0x08;
  
#ifdef DEBUG
  /* 取りだしすぎじゃないかチェック */
  if ( p == 1 ) {
    assert(pb->count[p]+1 <= 18);
  } else if ( p == 2 ) {
    assert(pb->count[p]+1 <= 4);
  } else if ( p == 3 ) {
    assert(pb->count[p]+1 <= 4);
  } else if ( p == 4 ) {
    assert(pb->count[p]+1 <= 4);
  } else if ( p == 5 ) {
    assert(pb->count[p]+1 <= 4);
  } else if ( p == 6 ) {
    assert(pb->count[p]+1 <= 2);
  } else if ( p == 7 ) {
    assert(pb->count[p]+1 <= 2);
  }
#endif /* DEBUG */

  return pb->number[p][pb->count[p]++];
}

/* 駒が多すぎないか調べる
 * OK : 0を返す
 * NG : 1を返す
 */
int pbCheck(PBOX * pb) {
  if ( 18 < pb->count[1] ) {
    return 1;
  } else if ( 4 < pb->count[2] ) {
    return 1;
  } else if ( 4 < pb->count[3] ) {
    return 1;
  } else if ( 4 < pb->count[4] ) {
    return 1;
  } else if ( 4 < pb->count[5] ) {
    return 1;
  } else if ( 2 < pb->count[6] ) {
    return 1;
  } else if ( 2 < pb->count[7] ) {
    return 1;
  } else if ( 2 < pb->count[8] ) {
    return 1;
  }
  
  return 0;
}
