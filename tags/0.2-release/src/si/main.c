/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * main.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/main.c,v $
 * $Id: main.c,v 1.1.1.1 2005/12/09 09:03:09 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include "getopt.h"
#include "si.h"
#include "ui.h"

#ifndef LINT
static const char copyright[] =
  "SI SHOGI " VERSION "-" REVISION " copyright(c)2000-2002 Masahiko Tokita";
#endif /* LINT */

void usage( void ) {
  fprintf( stderr, "SI SHOGI %s-%s\n", VERSION, REVISION );
  fprintf( stderr, "usage : si [BOARD file]\n" );
  fprintf( stderr, "        -n        Change display mode.\n" );
  fprintf( stderr, "        -h -v     Print help. This output.\n" );

  exit( 0 );
}

int main(int argc, char *argv[]) {
  int c;
  /* BOARD *bo = newBOARDwithInit(); */
  extern GAME g_game;
  extern BOARD g_board;
  extern int flgDisplayMode;
  
  /* for getopt */
  extern char *optarg;
  extern int optind;

  boInit( &g_board );

  while ( (c=si_getopt( argc, argv, "b:hm:nv")) != -1 ) {
    switch ( c ) {
    case 'b':
      loadBOARD( optarg, &g_board );
      break;
    case 'n':
      flgDisplayMode = 1;
      break;
    case 'h':
    case 'v':
    default:
      usage();
      break;
    }
  }
  
  argc -= optind;
  argv += optind;

  if ( argv[ 0 ] == '\0' ) {
    boSetHirate( &g_board );
    /* boSetHirate( bo ); */
  } else {
    if ( loadBOARD( argv[ 0 ], &g_board ) ) {
    /* if ( loadBOARD( argv[ 0 ], bo ) ) { */
      fprintf( stderr, "File not found.\n" );
      exit( 0 );
    }
  }

#ifdef USE_HASH  
#ifdef DEBUG
  printf("start newHASH()\n");
#endif /* DEBUG */
  /* ハッシュテーブル初期化 */
  newHASH();
#ifdef DEBUG
  printf("end newHASH()\n");
#endif /* DEBUG */
#endif /* USE_HASH */

  initGAME(&g_game);

  gmSetInputFunc(&g_game, SENTE, SI_HUMAN);
  gmSetInputFunc(&g_game, GOTE, SI_COMPUTER_3);
#ifdef USE_SERIAL
  gmSetDeviceName(&g_game, SENTE, "/dev/cua1");
  gmSetDeviceName(&g_game, GOTE, "/dev/cua1");
#endif /* USE_SERIAL */
  
  /* ゲーム開始 */
  play(&g_board, &g_game);
  /* play(bo, &g_game); */
  
#ifdef USE_HASH  
  /* ハッシュテーブル解放 */
  freeHASH();
#endif /* USE_HASH */

  return 0;
}
