/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * uke.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/uke.c,v $
 * $Id: uke.c,v 1.2 2005/12/12 02:27:15 tokita Exp $
 */

#include <stdlib.h>
#include <assert.h>
#include "si.h"
#include "ui.h"

void ukeTori(BOARD *bo, MOVEINFO *mi, int king_xy, int xy, int next);
void ukeMove(BOARD *bo, MOVEINFO *mi, int king_xy, int rxy, int next);
void ukeMove2(BOARD *bo, MOVEINFO *mi, int king_xy, int *rxy, int next);
void ukeAigoma(BOARD *bo, MOVEINFO *mi, int king_xy, int rxy, int next);
  
/**
 * 王手された状態で受けを探す。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param next 受け側の手番
 */
void uke(BOARD *bo, MOVEINFO *mi, int next) {
  int king_xy;
  int num_of_ohte;
  int rxy[toBoardCountMax];
  extern int toBoard3[2][256][toBoardCountMax];
  extern int toBoardCount3[2][256];

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
  assert( next == 0 || next == 1 );
  assert( bo->board[ 0 ] == WALL );
  assert( bo->board[ 0x9A ] == WALL );
#endif /* DEBUG */

  mi->count = 0;
  
  king_xy = bo->code_xy[1 + next][0];
  
  /*
   * 王手している駒の数を数える。
   * ２つならば王が動くしかない。
   * １つならば、
   * ・王が動く
   * ・王手している駒を取る
   * ・合い駒する
   * の３種類の受けがある。
   */ 
  num_of_ohte = toBoardCount3[ (next == 0) ][ king_xy ];
  
#ifdef DEBUG
  assert( num_of_ohte == 1 || num_of_ohte == 2 );
#endif /* DEBUG */

  if ( num_of_ohte == 1 ) {
    int n, xy;
    n = toBoard3[ (next == 0) ][ king_xy ][ 0 ];
    xy = bo->code_xy[ n ][ 0 ];
    
    /* 王手している駒を取って受ける */
    ukeTori(bo, mi, king_xy, xy, next);

    /* 合い駒して受ける */
    ukeAigoma(bo, mi, king_xy, xy, next);
     
    /* 王が動いて受ける */
    ukeMove(bo, mi, king_xy, xy, next);
  } else {
    int n;
    /* 両王手の場合 */
    /* 王が動いて受ける */
    n = toBoard3[ (next == 0) ][ king_xy ][ 0 ];
    rxy[ 0 ] = bo->code_xy[ n ][ 0 ];
    n = toBoard3[ (next == 0) ][ king_xy ][ 1 ];
    rxy[ 1 ] = bo->code_xy[ n ][ 0 ];
    
    ukeMove2(bo, mi, king_xy, rxy, next);
  }
}

/**
 * 指定した場所に効いている駒を数える。
 * next で指定した逆の手番の効きを探す。
 * @param bo 対象のBOARD
 * @param xy 場所
 * @param next 次の手番
 * @return 効きの数
 */
int countKiki(BOARD *bo, int xy, int *rxy, int next) {
  int count;
  int i, n;
  extern int toBoard3[2][256][toBoardCountMax];
  extern int toBoardCount3[2][256];

  count = 0;
  next = (next == 0);

  for (i=0; i<toBoardCount3[ next ][ xy ]; i++) {
#ifdef DEBUG
  assert(i < toBoardCountMax);
#endif /* DEBUG */

    n = toBoard3[ next ][ xy ][ i ];
    rxy[count++] = bo->code_xy[ n ][ 0 ];
  }

#ifdef DEBUG
  assert(count < toBoardCountMax);
#endif /* DEBUG */
  
  return count;
}

/**
 * 指定した場所に遠くに効く駒(香車、角、飛車、馬、竜)が効いているか調べる。
 * 効いていたら 1 を返す。効いていない場合は 0 を返す。
 * next で指定した逆の手番の効きを探す。
 * @param bo 対象のBOARD
 * @param xy 場所
 * @param next 手番
 * @return 効きがあれば 1 、効きが無ければ 0 を返す。
 */
int ukeRemoteKiki(BOARD *bo, int xy, int next) {
  int i, p, n;
  extern int toBoardCount3[2][256];
  extern int toBoard3[2][256][toBoardCountMax];

#ifdef DEBUG
  assert( bo != NULL );
  assert( 1 <= (xy & 0x0F) && (xy & 0x0F) <= 9 );
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( next == 0 || next == 1 );
#endif /* DEBUG */
  
  next = (next == 0);
  
  for (i=0; i<toBoardCount3[ next ][ xy ]; i++) {
#ifdef DEBUG
    assert(i < toBoardCountMax);
#endif /* DEBUG */

    n = toBoard3[ next ][ xy ][ i ];
    p = bo->code_xy[ n ][ 1 ];
    p &= 0x0F;
    if ( p == KYO || p == KAKU || p == HISYA || p == UMA || p == RYU ) {
      return 1;
    }
  }

  return 0;
}

/**
 * 駒を取って王手を受ける手を探す。
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 * @param xy 場所
 * @param next 次の手番
 */
void ukeTori(BOARD *bo, MOVEINFO *mi, int king_xy, int xy, int next) {
  TE te;
  int i, n, p;
  extern int toBoard3[2][256][toBoardCountMax];
  extern int toBoardCount3[2][256];

  for (i=0; i<toBoardCount3[ next ][ xy ]; i++) {
    n = toBoard3[ next ][ xy ][ i ];
    p = bo->code_xy[ n ][ 1 ];
#ifdef DEBUG
    assert( getTeban(p) == next );
#endif /* DEBUG */
    
    if ( p == OH || p == (OH + 0x10) )
      continue;

    p &= 0x0F;
    te.fm = bo->code_xy[ n ][ 0 ];
    te.to = xy;
    te.nari = 0;
    te.uti = 0;
     
    if ( next == SENTE ) {
      if ( p == FU && (te.to >> 4) == 1 ) {
	te.nari = 1;
      } else if ( p == KYO && (te.to >> 4) == 1 ) {
	te.nari = 1;
      } else if ( p == KEI && (te.to >> 4) <= 2 ) {
	te.nari = 1;
      }
    } else {
      if ( p == FU && (te.to >> 4) == 9 ) {
	te.nari = 1;
      } else if ( p == KYO && (te.to >> 4) == 9 ) {
	te.nari = 1;
      } else if ( p == KEI && 8 <= (te.to >> 4) ) {
	te.nari = 1;
      }
    }
    
    if ( ukeAkiohte(bo, &te, king_xy, next) )
      continue;
    
    miAddWithNari2(mi, te, p, next);
  }
}

/**
 * 王が動いての受けを探す。
 * わかりにくくなっているでの修正する。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVENIFO
 * @param king_xy 王の位置
 * @param next 受け側の手番
 */
void ukeMove(BOARD *bo, MOVEINFO *mi, int king_xy, int rxy, int next) {
  /* 遠くに効く駒かどうか判定するためのテーブル */
  static int remote[32] = {
    0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1,
    0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1,
  };
  static signed int tolist[] = {
    -17, -16, -15,
    -1, 1, 
    15, 16, 17,
  };
  static TE te;
  int xy;
  int v, w, flg, ng;
  int i, p;
  extern int toBoardCount3[2][256];

  if ( remote[ bo->board[rxy] ] ) {
    int rp = bo->board[ rxy ] & 0x0F;
      
    flg = 1;
    
    /* 遠くに効く駒(飛車、角、香)で王手されていおり、動けな方向がある */
    v = (rxy & 0x0F) - (king_xy & 0x0F);
    w = (rxy >> 4) - (king_xy >> 4);

    if ( 0 < v )
      v = -1;
    else if ( v < 0 )
      v = 1;
    if ( 0 < w )
      w = -1;
    else if ( w < 0 )
      w = 1;
    
    if ( next ) {
      /* gote */
      if ( rp == KAKU || rp == UMA ) {
	if ( v  == 0 || w == 0 )
	  flg = 0;
      } else if ( rp == HISYA || rp == RYU ) {
	if ( v  != 0 && w != 0 )
	  flg = 0;
      } else if ( rp == KYO ) {
	if ( v != 0 || w != -1 )
	  flg = 0;
      }
    } else {
      /* sente */
      if ( rp == KAKU || rp == UMA ) {
	if ( v  == 0 || w == 0 )
	  flg = 0;
      } else if ( rp == HISYA || rp == RYU ) {
	if ( v  != 0 && w != 0 )
	  flg = 0;
      } else if ( rp == KYO ) {
	if ( v != 0 || w != 1 )
	  flg = 0;
      }
    }
    
    ng = king_xy + ( (16 * w) + v );
  } else {
    flg = 0;
    ng = 0;
  }
  
  for (i=0; i<8; i++) {
    xy = king_xy + tolist[i];
    p = bo->board[ xy ];
    if ( p == WALL )
      continue;
    if ( p && getTeban(p) == next )
      continue;
    if ( flg && xy == ng )
      continue;
    if ( toBoardCount3[ (next == 0) ][ xy ] )
      continue;

    te.fm = king_xy;
    te.to = xy;
    te.nari = 0;
    te.uti = 0;
    miAdd(mi, &te);
  }
}

/**
 * 王が動いての受けを探す。両王手用。
 * わかりにくくなっているでの修正する。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVENIFO
 * @param king_xy 王の位置
 * @param rxy 王手している駒のリスト
 * @param next 受け側の手番
 */
void ukeMove2(BOARD *bo, MOVEINFO *mi, int king_xy, int *rxy, int next) {
  /* 遠くに効く駒かどうか判定するためのテーブル */
  static int remote[32] = {
    0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1,
    0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1,
  };
  static TE te;
  int xy;
  int v, w;
  int flg[2];
  int ng[2];
  int i, p;
  extern int toBoardCount3[2][256];
  extern const int arr_round_to[8];

  ng[0] = 0;
  ng[1] = 0;
   
  if ( remote[ bo->board[rxy[0]] ] ) {
    int rp = bo->board[rxy[0]] & 0x0F;
    
    flg[0] = 1;

    /* 遠くに効く駒(飛車、角、香)で王手されていおり、動けな方向がある */
    v = (rxy[0] & 0x0F) - (king_xy & 0x0F);
    w = (rxy[0] >> 4) - (king_xy >> 4);

    if ( 0 < v )
      v = -1;
    else if ( v < 0 )
      v = 1;
    if ( 0 < w )
      w = -1;
    else if ( w < 0 )
      w = 1;
    
    if ( next ) {
      /* gote */
      if ( rp == KAKU || rp == UMA ) {
	if ( v  == 0 || w == 0 )
	  flg[0] = 0;
      } else if ( rp == HISYA || rp == RYU ) {
	if ( v  != 0 && w != 0 )
	  flg[0] = 0;
      } else if ( rp == KYO ) {
	if ( v != 0 || w != -1 )
	  flg[0] = 0;
      }
    } else {
      /* sente */
      if ( rp == KAKU || rp == UMA ) {
	if ( v  == 0 || w == 0 )
	  flg[0] = 0;
      } else if ( rp == HISYA || rp == RYU ) {
	if ( v  != 0 && w != 0 )
	  flg[0] = 0;
      } else if ( rp == KYO ) {
	if ( v != 0 || w != 1 )
	  flg[0] = 0;
      }
    }

    ng[0] = king_xy + ( (16 * w) + v );
  } else {
    flg[0] = 0;
  }
  if ( remote[ bo->board[rxy[1]] ] ) {
    int rp = bo->board[rxy[1]] & 0x0F;
    
    flg[1] = 1;

    /* 遠くに効く駒(飛車、角、香)で王手されていおり、動けな方向がある */
    v = (rxy[1] & 0x0F) - (king_xy & 0x0F);
    w = (rxy[1] >> 4) - (king_xy >> 4);

    if ( 0 < v )
      v = -1;
    else if ( v < 0 )
      v = 1;
    if ( 0 < w )
      w = -1;
    else if ( w < 0 )
      w = 1;
    
    
    if ( next ) {
      /* gote */
      if ( rp == KAKU || rp == UMA ) {
	if ( v  == 0 || w == 0 )
	  flg[1] = 0;
      } else if ( rp == HISYA || rp == RYU ) {
	if ( v  != 0 && w != 0 )
	  flg[1] = 0;
      } else if ( rp == KYO ) {
	if ( v != 0 || w != -1 )
	  flg[1] = 0;
      }
    } else {
      /* sente */
      if ( rp == KAKU || rp == UMA ) {
	if ( v  == 0 || w == 0 )
	  flg[1] = 0;
      } else if ( rp == HISYA || rp == RYU ) {
	if ( v  != 0 && w != 0 )
	  flg[1] = 0;
      } else if ( rp == KYO ) {
	if ( v != 0 || w != 1 )
	  flg[1] = 0;
      }
    }
      
    ng[1] = king_xy + ( (16 * w) + v );
  } else {
    flg[1] = 0;
  }
  
  for (i=0; i<8; i++) {
    xy = king_xy + arr_round_to[i];
    p = bo->board[ xy ];
    if ( p == WALL )
      continue;
    if ( p && getTeban(p) == next )
      continue;
    if ( flg[0] && xy == ng[0] )
      continue;
    if ( flg[1] && xy == ng[1] )
      continue;
    if ( toBoardCount3[ (next == 0) ][ xy ] )
      continue;

    te.fm = king_xy;
    te.to = xy;
    te.nari = 0;
    te.uti = 0;
    miAdd(mi, &te);
  }
}

/**
 * 合い駒して王手を受ける。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param king_xy 王の場所
 * @param rxy 王手している駒の数
 * @param next 受け側の手番
 */
void ukeAigoma(BOARD *bo, MOVEINFO *mi, int king_xy, int rxy, int next) {
  static MOVEINFO mi1;
  static TE te;
  int xy;
  int v, w;
  int i, j, p;
  extern int toBoardCount3[2][256];

  p = bo->board[ rxy ] & 0xF;
  if ( p != KYO && p != KAKU && p != HISYA && p != UMA && p != RYU )
    return;
  
  v = (rxy & 0x0F) - (king_xy & 0x0F);
  w = (rxy >> 4) - (king_xy >> 4);

  if ( abs(v) <= 1 && abs(w) <= 1) {
    /* 合い駒できないので戻る */
    return;
  }
  
  if ( 0 < v )
    v = 1;
  else if ( v < 0 )
    v = -1;
  
  if ( 0 < w )
    w = 1;
  else if ( w < 0 )
    w = -1;
  
  for (i=1; 1; i++) {
    xy = king_xy + ((w * i) << 4) + (v * i);
    p = bo->board[ xy ];

#ifdef DEBUG
    assert( p != WALL );
#endif /* DEBUG */

    if ( p )
      break;

    if ( toBoardCount3[ next ][ xy ] == 0 )
      continue;

    for (j=1; j<8; j++) {
      if ( bo->piece[next][j] ) {
	if ( j == FU ) {
	  /* 二歩じゃないか調べる */
	  if ( next ) {
	    if ( checkFuGote( bo, xy & 0x0F ) )
	      continue;
	  } else {
	    if ( checkFuSente( bo, xy & 0x0F ) )
	      continue;
	  }
	}
	te.fm = 0;
	te.to = xy;
	te.nari = 0;
	te.uti = j;
	miAdd( mi, &te );
      }
    }

    /* 王と王手している駒の間に駒を動かして受ける手を探す */
    if ( (boGetPiece(bo, mi1.te[j].fm) & OH) == OH ) {
      continue;
    }
    moveto2(bo, &mi1, xy, next );
    for (j=0; j<mi1.count; j++) {
      if ( ukeAkiohte(bo, &(mi1.te[j]), king_xy, bo->next) ) {
        continue;
      } 
      miAdd( mi, &(mi1.te[j]) );
    }
  }
}

/**
 * 王手している駒を取って受けた時に、遠くに効く駒(香車、角、飛車、馬、竜)など
 * によって王手になっていないか調べる。
 * @param bo 対象のBOARD
 * @param te 駒を取って受ける手
 * @param king_xy 王の場所
 * @param next 受け方の手番
 * @return 駒を取ったときに王手がなければ 0 を返す。あれば 1 以上を返す。
 */
int ukeAkiohte(BOARD *bo, TE *te, int king_xy, int next) {
  extern int toBoardCount3[2][256];
  extern int toBoard3[2][256][toBoardCountMax];
  static int tmp_n[toBoardCountMax];
  int flag;
  int i;

#ifdef DEBUG
  assert( bo != NULL );
  assert( te != NULL );
  if ( te->uti ) {
    assert( 1 <= te->uti && te->uti <= 7 );
  } else {
    assert( 1 <= (te->fm & 0x0F) && (te->fm & 0x0F) <= 9 );
    assert( 1 <= (te->fm >> 4) && (te->fm >> 4) <= 9 );
  }
  assert( 1 <= (te->to & 0x0F) && (te->to & 0x0F) <= 9 );
  assert( 1 <= (te->to >> 4) && (te->to >> 4) <= 9 );
  assert( next == 0 || next == 1 );
#endif /* DEBUG */
  
  if ( te->uti != 0 ) {
    /* 駒打ちは戻る */
    return 0;
  }
  
  if ( ukeRemoteKiki( bo, te->fm, next ) == 0 ) {
    /* 香車、角、飛車、馬、竜の効きはないので戻る。 */
    return 0;
  }
  
  /* toBoardCount3 の登録順番を取っておく */
  for ( i=0; i<toBoardCountMax; i++ ) {
    tmp_n[ i ] = toBoard3[ next ][ te->to ][ i ];
  }

  boMove_mate( bo, te, next );
  if ( toBoardCount3[ (next == 0) ][ king_xy ] )
    flag = 1;
  else
    flag = 0;
  boBack_mate( bo, next );

  /* toBoardCount3 の登録順番を戻す */
  for ( i=0; i<toBoardCountMax; i++ ) {
    toBoard3[ next ][ te->to ][ i ] = tmp_n[ i ];
  }

  return flag;
}
