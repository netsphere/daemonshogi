/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * ui.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/ui.c,v $
 * $Id: ui.c,v 1.2 2005/12/12 08:49:36 tokita Exp $
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "si.h"
#include "ui.h"
#ifdef USE_SERIAL
#include "comaccess.h"
#endif /* USE_SERIAL */

static void (*pending_function)(void) = NULL;
void putCmdHelp(void);

/** GUIによる入力への関数へのポインタ */
INPUTSTATUS (*si_input_next_gui_human)(BOARD* bo, TE* te) = NULL; 
INPUTSTATUS (*si_input_next_network)(BOARD* bo, TE* te) = NULL;

/**
 * ゲーム用にBOARDを表示する。
 * 消費時間の表示も行う。
 * @param bo 対象のBOARD
 * @param gm 対象のGAME
 */
void putBOARDforGAME(BOARD *bo, GAME *gm) {
  char buf[BUFSIZ];
  extern int flgDisplayMode;

#ifdef DEBUG
  assert( flgDisplayMode == 0 || flgDisplayMode == 1 );
  assert( bo != NULL );
  assert( gm != NULL );
#endif /* DEBUG */
  
  if ( flgDisplayMode )
    fputBOARD(stdout, 0, bo);
  else 
    fputBOARD(stdout, 1, bo);
  
  printf("SENTE TIME - ");
  time2string(buf, bo->total_time[SENTE]);
  printf("%s/", buf);
  time2string(buf, bo->limit_time[SENTE]);
  printf("%s\n", buf);
  
  printf("GOTE TIME  - ");
  time2string(buf, bo->total_time[GOTE]);
  printf("%s/", buf);
  time2string(buf, bo->limit_time[GOTE]);
  printf("%s\n", buf);
}

void putBOARD(BOARD *brd) {
  extern int flgDisplayMode;

#ifdef DEBUG
  assert( brd != NULL );
#endif /* DEBUG */
  
  if ( flgDisplayMode )
    fputBOARD(stdout, 0, brd);
  else 
    fputBOARD(stdout, 1, brd);
}

void fputBOARD(FILE *out, int mode, BOARD *bo) {
  int i, j;
  int c;

#ifdef DEBUG
  assert( out != NULL && bo != NULL );
  assert( mode == 0 || mode == 1 );
#endif /* DEBUG */

  fprintf(out, "/Board\n");
  for (i = 1; i <= 9; i++) {
    for (j = 9; 0<j; j--) {
      c = boGetPiece( bo, (i << 4) + j );
      if (c) {
	if (mode)
	  fprintf(out, "%c%X ", (c & 0x10) ? 'v' : '^', c & 0x0F);
	else
	  fprintf(out, "%02X ", c);
      } else {
	if (mode)
	  fprintf(out, " . ");
	else
	  fprintf(out, "00 ");
      }
    }
    fprintf(out, "\n");
  }
  fprintf(out, "/Piece\n");
  fprintf(out, "%d %d %d %d %d %d %02d\n",
	 bo->piece[0][7],
	 bo->piece[0][6],
	 bo->piece[0][5],
	 bo->piece[0][4],
	 bo->piece[0][3],
	 bo->piece[0][2],
	 bo->piece[0][1]);
  fprintf(out, "%d %d %d %d %d %d %02d\n",
	 bo->piece[1][7],
	 bo->piece[1][6],
	 bo->piece[1][5],
	 bo->piece[1][4],
	 bo->piece[1][3],
	 bo->piece[1][2],
	 bo->piece[1][1]);
  fprintf(out, "/Next move\n");
  if (mode)
    fprintf(out, "%s\n", bo->next ? "gote" : "sente");
  else
    fprintf(out, "%d\n", bo->next);
  
#ifdef DEBUG
  printf("key : %08LX\n", bo->key);
#endif /* DEBUG */
  
  return;
}

void fputNoBOARD(FILE *out, BOARD *bo) {
  int i, j;
  int c;

#ifdef DEBUG
  assert( out != NULL && bo != NULL );
#endif /* DEBUG */

  fprintf(out, "/Board\n");
  for (i = 1; i <= 9; i++) {
    for (j = 9; 0<j; j--) {
      c = bo->noBoard[(i<<4) +j];
      if (c) {
	fprintf(out, "%02d ", c);
      } else {
	fprintf(out, "00 ");
      }
    }
    fprintf(out, "\n");
  }
}

#ifdef NOREAD
void fputToBOARD(FILE *out, BOARD *bo) {
  int i, j;
  int c;

#ifdef DEBUG
  assert( out != NULL && bo != NULL );
#endif /* DEBUG */

  fprintf(out, "/Board\n");
  for (i = 1; i <= 9; i++) {
    for (j = 9; 0<j; j--) {
      c = bo->toBoardCount[(i<<4) +j];
      if (c) {
	fprintf(out, "%02d ", c);
      } else {
	fprintf(out, "00 ");
      }
    }
    fprintf(out, "\n");
  }
}
#endif /* NOREAD */
      
void printBOARD(BOARD * brd) {
    int x, y;

#ifdef DEBUG
  assert( brd != NULL );
#endif /* DEBUG */

  printf( "------------------\n" );
  for (y = 1; y <= 9; y++) {
    for (x = 9; 0<x; x--)
      printf("%02x", boGetPiece(brd, (y << 4) + x));
    printf("\n");
  }
  printf( "------------------\n" );

  for (x = 0; x < 7; x++)
    printf("%d", brd->piece[0][x]);
  printf("\n");
  for (x = 0; x < 7; x++)
    printf("%d", brd->piece[1][x]);
  printf("\n");
  printf( "next : %d\n", brd->next );
}

void printCode_xy(BOARD *bo) {
  int i;

  for (i = 0; i<41; i++) {
    printf("%2d = (%02X, %02X), ", i,
	   (bo->code_xy[i][0] >> 4) + ((bo->code_xy[i][0]&0xF)<<4),
	   bo->code_xy[i][1]);
    if ((i % 5) == 4)
      printf("\n");
  }

  printf("\n");
}

void printNoBOARD(BOARD * brd) {
  int x, y;

#ifdef DEBUG
  assert( brd != NULL );
#endif /* DEBUG */

  printf( "No BOARD\n" );
  printf( "------------------\n" );
  for (y = 1; y <= 9; y++) {
    for (x = 9; 0<x; x--)
      printf("%02d", brd->noBoard[ (y << 4) + x ] );
    printf("\n");
  }
  printf( "------------------\n" );
}

void printMOVEINFO(MOVEINFO * mi) {
  int i;

#ifdef DEBUG
  assert( mi != NULL );
#endif /* DEBUG */

  printf( "MOVEINFO\n" );
  for (i = 0; i < mi->count; i++) {
    printTE(&(mi->te[i]));
  }
  printf("count : %d\n", mi->count);
}

void printMOVEINFO2(MOVEINFO2 * mi) {
  int i;

#ifdef DEBUG
  assert( mi != NULL );
#endif /* DEBUG */

  printf( "MOVEINFO\n" );
  for (i = 0; i < mi->count; i++) {
    printTE(&(mi->te[i]));
  }
  printf("count : %d\n", mi->count);
}

void printTE(TE * te) {
#ifdef DEBUG
  assert( te != NULL );
  assert( 0 <= te->uti && te->uti <= 7 );
#endif /* DEBUG */
  
  if ( te->uti ) {
    printf("(%d,%d) %d uti\n",
	   (te->to)&0x0f, (te->to)>>4, te->uti);
  } else {
    printf("(%d,%d)->(%d,%d)%s\n",
	   (te->fm)&0x0f, (te->fm)>>4,
	   (te->to)&0x0f, (te->to)>>4,
	   te->nari ? "nari" : "");
  }
}

void fprintTE(FILE *out, TE * te) {
#ifdef DEBUG
  assert( te != NULL );
  assert( 0 <= te->uti && te->uti <= 7 );
#endif /* DEBUG */
  
  if ( te->uti ) {
    fprintf(out, "(%d,%d) %d uti\n",
	   (te->to)&0x0f, (te->to)>>4, te->uti);
  } else {
    fprintf(out, "(%d,%d)->(%d,%d)%s\n",
	   (te->fm)&0x0f, (te->fm)>>4,
	   (te->to)&0x0f, (te->to)>>4,
	   te->nari ? "nari" : "");
  }
}

#ifdef NOREAD
void printToBOARD(BOARD *bo, int x, int y) {
  int i;

#ifdef DEBUG
  assert( bo != NULL );
  assert( 1 <= x && x <= 9 && 1 <= y && y <= 9 );
#endif /* DEBUG */

  printf( "toBoard\n" );
  for (i = 0; i < bo->toBoardCount[(y << 4) + x]; i++ ) {
    printf( "%d", bo->toBoard[(y << 4) + x][i] );
  }
}
#endif /* NOREAD */

void putPBOX(PBOX *pb) {
  fputPBOX(stdout, pb);
}

void fputPBOX(FILE *out, PBOX *pb) {
  static char *name[] = {
    "dummy", "fu", "kyo", "kei", "gin", "kin", "kaku", "hisya", "oh",
  };
  int i, j;
  
#ifdef DEBUG
  assert( out != NULL );
  assert( pb != NULL );
#endif /* DEBUG */

  for ( i=1; i<9; i++ ) {
    fprintf( out, "%-5s : %2d   ", name[i], pb->count[i] );
    for ( j=0; j<pb->count[i]; j++ ) {
      fprintf( out, "%2d, ", pb->number[i][j] );
    }
    fprintf( out, "\n" );
  }
}

void printTREE(TREE *tree, int n) {
  int i;
  
  if ( tree == NULL )
    return;
  
  for (i=0; i<n; i++) {
    putchar(' ');
  }

  /* printf("%X : ", (int)tree); */
  putchar(n % 2 ? 'v' : '^');
  printTE(&(tree->te));
  /* putBOARD(bo); */

  if ( tree->child != NULL )
    printTREE(tree->child, n + 1);

  if ( tree->brother != NULL )
    printTREE(tree->brother, n);
}

/**
 * ペンディングされたイベントを処理する関数へのポインタをセットする。
 * @param f ペンディングされたイベントを処理する関数へのポインタ
 */
void set_pending_function(void (*f)(void)) {
  pending_function = f;
}

/**
 * GUI のペンディングされたイベントを処理する。
 */
void processing_pending() {
  if ( pending_function == NULL )
    return;
  
  (*pending_function)();
}

/* -------------------------------------------------------- */
/* 手の入力関数 */
/* -------------------------------------------------------- */

/**
 * 画面から文字ベースで次の一手を入力し te に格納する。
 * @param bo 対象のBOARD
 * @param te TE
 * @return GAMESTAT
 */
INPUTSTATUS si_input_next_human(BOARD *bo, TE *tte) {
  char buf[BUFSIZ];
  TE te;

  while ( 1 ) {
    printf("$ ");
    (void)fgets(buf, BUFSIZ - 1, stdin);
    if ( buf[0] == '\n' ) {
    } else if ( strncmp( buf, "back", 4 ) == 0 ) {
      if ( 0 < bo->mi.count ) {
	boToggleNext( bo );
	boBack( bo, bo->next );
	boToggleNext( bo );
	boBack( bo, bo->next );
	putBOARD( bo );
      }
    } else if ( strncmp( buf, "help", 4 ) == 0 ) {
      putCmdHelp();
    } else if ( strncmp( buf, "disp", 4 ) == 0 ) {
      extern int flgDisplayMode;
      flgDisplayMode = (1 - flgDisplayMode);
    } else if ( strncmp( buf, "toryo", 4 ) == 0 ) {
      return SI_TORYO;
    } else if ( strncmp( buf, "make", 4 ) == 0 ) {
      return SI_TORYO;
    } else if ( strncmp( buf, "quit", 4 ) == 0 ) {
      return SI_CHUDAN;
    } else if ( strncmp( buf, "board", 5 ) == 0 ) {
      putBOARD(bo);
    } else if ( isdigit(buf[0]) && isdigit(buf[1]) &&
	        isdigit(buf[2]) && isdigit(buf[3]) ) {
      te.fm = ((buf[1]-'0') << 4) + buf[0]-'0';
      te.to = ((buf[3]-'0') << 4) + buf[2]-'0';
      te.nari = ( buf[4] == 'n' );
      te.uti = 0;
      if ( boCheckTE( bo, &te ) ) {
	*tte = te;
	break;
      } else {
	printf("No good move\n");
      }
    } else if ( isdigit(buf[0]) && isdigit(buf[1]) &&
	        buf[2] == 'u' && isdigit(buf[3]) ) {
      te.to = ((buf[1]-'0') << 4) + buf[0]-'0';
      te.fm = 0;
      te.nari = 0;
      te.uti = buf[3] - '0';
      if ( boCheckTE( bo, &te ) ) {
	*tte = te;
	break;
      } else {
	printf("No good move\n");
      }
    } else {
      printf("Unknown command : %s", buf);
    }
  }

  return SI_NORMAL;
}

/**
 * プログラムの一手を te に格納する。
 * @param bo 対象のBOARD
 * @param te TE
 * @return GAMESTAT
 */
INPUTSTATUS si_input_next_computer(BOARD *bo, TE *te) {
  extern TE best_te;
  INPUTSTATUS ret;
  
  bo->point[SENTE] = grade(bo, SENTE);
  bo->point[GOTE] = grade(bo, GOTE);
  
  ret = minmax(bo);
  *te = best_te;

  return ret;
}

/**
 * シリアルポートから一手を入力し te に格納する。 
 * @param bo 対象のBOARD
 * @param te TE
 * @return GAMESTAT
 */
#ifdef USE_SERIAL
INPUTSTATUS si_input_next_serialport(BOARD *bo, TE *te) {
  static const char *PIECE_NAME[] = {
    "", 
    "FU", "KY", "KE", "GI", "KI", "KA", "HI", "OU",
    "TO", "NY", "NK", "NG", "",   "UM", "RY", "",
  };
  char buf[ BUFSIZ ];
  int i;
  extern GAME g_game;

#ifdef DEBUG
  printf("This is 'si_input_next_serialport()' before\n");
#endif /* DEBUG */
  if ( caAccept(&(g_game.ca[ bo->next ]), buf) == 1 ) {
    /* 中断が入って戻ってきた */
    return SI_CHUDAN;
  }
#ifdef DEBUG
  printf("This is 'si_input_next_serialport()' after\n");
  printf("accept : '%s'\n", buf);
#endif /* DEBUG */

  if ( buf[0] != '+' && buf[0] != '-' && buf[0] != '%' ) {
    /* 想定外の文字列が送信されて来た */
    printf("buf[0] = '%02X'\n", buf[0]);
    return SI_NGDATA;
  }
  
  if ( buf[0] == '%' ) {
    /* 特殊なコマンドの処理 */
    te->fm = 0;
    te->to = 0;
    te->nari = 0;
    te->uti = 0;
    return SI_TORYO;
  }
  
  te->fm = (buf[1] - '0') + ((buf[2] - '0') << 4);
  te->to = (buf[3] - '0') + ((buf[4] - '0') << 4);
  te->nari = 0;
  te->uti = 0;
  
  if ( te->fm == 0 ) {
    for (i=1; i<=7; i++) {
      if ( strncmp(buf + 5, PIECE_NAME[i], 2) == 0 ) {
	te->uti = i;
	break;
      }
    }
  } else if ( (boGetPiece(bo, te->fm) & 0x8) == 0 ) {
    for (i=9; i<=15; i++) {
      if ( strncmp(buf + 5, PIECE_NAME[i], 2) == 0 ) {
	te->nari = 1;
      }
    }
  }
    
  printTE( te );
  
  return SI_NORMAL;
}
#endif /* USE_SERIAL */

void putCmdHelp(void) {
  printf("- command\n");
  printf("\n");
  printf("6978 move (6,9) to (7,8)\n");
  printf("34u1 koma uti (6,9) fu\n");
  printf("\n");
  printf("         fu    1 , kyo  2\n");
  printf("         kei   3 , gin  4\n");
  printf("         kin   5 , kaku 5\n");
  printf("         hisya 7\n");
  printf("\n");
  printf("toryo or make : Lost.\n");
  printf("quit   : Exit this program.\n");
  printf("disp   : Change to display mode.\n");
  printf("board  : Display board.\n");
  printf("\n");
}
