/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/**
 * think.c
 * $Id: think.c,v 1.1.1.1 2005/12/09 09:03:10 tokita Exp $
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/think.c,v $
 */

#include <stdio.h>
#include <assert.h>
#include "si.h"
#include "ui.h"

void think_remove_akiohte(BOARD *bo, MOVEINFO *mi, int king_xy);
void think_remove_ng_oh_move(BOARD *bo, MOVEINFO *mi, int king_xy);
int think_is_tsumero(BOARD *bo, int search_depth);
void think_tsumero_uke(BOARD *bo, MOVEINFO *mi);

/**
 * 侯補手を探す。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param depth 読みの深さ
 */
void think(BOARD *bo, MOVEINFO *mi, int depth) {
  int king_xy, king_xy2;
  int num_of_ohte;
  extern int THINK_DEEP_MAX;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
  assert( 0 <= depth );
#endif /* DEBUG */
  
  mi->count = 0;

  /* 自分の王の位置 */
  king_xy = bo->code_xy[1 + bo->next][0];
  /* 相手の王の位置 */
  king_xy2 = bo->code_xy[1 + (bo->next == 0)][0];
  
  /* 王手さているか調べる */
  num_of_ohte = boGetToBoardCount(king_xy, (bo->next == 0));

#ifdef DEBUG
  assert( 0 <= num_of_ohte && num_of_ohte <= 2 );
#endif /* DEBUG */
  
  if ( num_of_ohte ) {
    /* 王手さていたら受けを探す */
    uke( bo, mi, bo->next );
    return;
  }

  if ( depth == 0 ) {
    if ( think_is_tsumero(bo, 5) ) {
      /* 詰めろになっていた */
      /* 詰めろ逃れ専用ルーチンを呼ぶ */
      think_tsumero_uke(bo, mi);
      if ( 0 < mi->count ) {
	return;
      }
      
      /* 受ける手がない場合は王手を探す */
      ohte(bo, mi);
      
      if ( 0 < mi->count ) {
	return;
      }
      
      /* 王手もない場合は合法手をセットして終了する */
      search_053(bo, mi);
      search_068(bo, mi);
      /* ダブった手があったら削除する */
      miRemoveDuplicationTE( mi );

      /* 駒が動いて空き王手になる手は削除する */
      think_remove_akiohte(bo, mi, king_xy);

      /* 相手の駒の効きがあるところに王が動く手があったら削除する */
      think_remove_ng_oh_move(bo, mi, king_xy);
      
      return;
    }
  }
  
#undef NOREAD
#ifdef NOREAD
  for ( i=0; i<1; i++ ) {
    MATE_DEEP_MAX = i;
    if ( mate(bo, &root, 0) ) {
      /* 詰んだ */
      printTREE(root.child, 0);
      break;
    }
  }
    
  if ( root.child != NULL ) {
    /* check mate */
    miAdd( mi, &(root.child->te) );
    freeTREE( root.child );
    return;
  }
#endif /* NOREAD */

  search_011(bo, mi);
  search_013(bo, mi);
  search_014(bo, mi);
  search_012(bo, mi);
  if ( mi->count < 2 ) {
    search_024a(bo, mi);
  }
  if ( depth == 0 ) {
    search_068(bo, mi);
  }
  if ( depth <= THINK_DEEP_MAX - 3 ) {
    search_022(bo, mi);
    search_027(bo, mi);
    search_024a(bo, mi);
    search_031a(bo, mi);
    search_031b(bo, mi);
  }
  if ( depth <= 3 ) {
    search_046(bo, mi);
    search_048(bo, mi);
    search_044(bo, mi);
    search_041(bo, mi);
    search_047(bo, mi);
  }
  if ( depth <= 1 ) {
    search_053(bo, mi);
  }

  /* ダブった手があったら削除する */
  miRemoveDuplicationTE( mi );

  /* 駒が動いて空き王手になる手は削除する */
  think_remove_akiohte(bo, mi, king_xy);

  /* 相手の駒の効きがあるところに王が動く手があったら削除する */
  think_remove_ng_oh_move(bo, mi, king_xy);
  
  return;
}

/**
 * 駒が動いて空き王手になる手は削除する
 * @param bo 対象のBOARD
 * @param mi 対象のMOVEINFO
 */
void think_remove_akiohte(BOARD *bo, MOVEINFO *mi, int king_xy) {
  int i;

  for ( i=0; i<mi->count; ) {
    if ( ukeAkiohte( bo, &(mi->te[ i ]), king_xy, bo->next ) ) {
      miRemoveTE2( mi, i );
    } else {
      i++;
    }
  }
}

/**
 * 相手の駒の効きがあるところに王が動く手があったら削除する
 * @param bo 対象のBOARD
 * @param mi 対象のMOVEINFO
 */
void think_remove_ng_oh_move(BOARD *bo, MOVEINFO *mi, int king_xy) {
  int i;

  for ( i=0; i<mi->count; ) {
    if ( mi->te[ i ].fm == king_xy &&
	0 < boGetToBoardCount(mi->te[ i ].to, (bo->next == 0)) ) {
      miRemoveTE2( mi, i );
    } else {
      i++;
    }
  }
}

/**
 * 自玉が詰めろになっていないか調べる。
 * 詰めろならば 1 を返す。なければ 0 を返す。
 * @param bo 対象のBOARD
 * @param search_depth 詰みを調べる深さ
 * @reutrn 詰めろならば 1 を返す。なければ 0 を返す。
 */
int think_is_tsumero(BOARD *bo, int search_depth) {
  int flg_mate;
  int i;
  TREE root;
  extern int MATE_DEEP_MAX;

  initTREE( &root );
  
  flg_mate = 0;
  /* 自玉が詰めろになっていないか調べる */
  boToggleNext(bo);
  for ( i=0; i<search_depth; i++ ) {
    MATE_DEEP_MAX = i;
    if ( mate(bo, &root, 0) ) {
      /* 詰んだ */
      flg_mate = 1;
      break;
    }
  }
  boToggleNext(bo);
  if ( root.child != NULL ) {
    freeTREE( root.child );
  }
  
  return flg_mate;
}

/**
 * 詰めろを受ける手を検索する。
 * @param bo 対象のBOARD
 * @param mi 手を記録するMOVEINFO
 */
void think_tsumero_uke(BOARD *bo, MOVEINFO *mi) {
  MOVEINFO mi1;
  int king_xy;
  TE te;
  int i;
  
  king_xy = bo->code_xy[1 + bo->next][0];

  mi1.count = 0;
  /* 直前に動いた相手の駒を取る手 */
  search_011(bo, &mi1);
  /* 相手の王への王手 */
  search_013(bo, &mi1);
  /* 王が動く手 */
  search_049(bo, &mi1);
  /* 自分の王周辺に動く手 */
  search_pressure(bo, &mi1, king_xy, bo->next);
  /* 直前に指した駒への歩打ち */ 
  search_047(bo, &mi1);
  /* 自分の王周辺に駒を打ち込む手 */
  search_031b(bo, &mi1);
  
  /* ダブった手があったら削除する */
  miRemoveDuplicationTE( &mi1 );

  /* 駒が動いて空き王手になる手は削除する */
  think_remove_akiohte(bo, &mi1, king_xy);

  /* 相手の駒の効きがあるところに王が動く手があったら削除する */
  think_remove_ng_oh_move(bo, &mi1, king_xy);

  for (i=0; i<mi1.count; i++) {
    te = mi1.te[i];
    boMove_mate(bo, &te, bo->next);
    if ( think_is_tsumero(bo, 5) == 0 ) {
      /* 詰みを防いだ。侯補手に加える */
      miAdd(mi, &te);
    }
    boBack_mate(bo, bo->next);
  }
}
