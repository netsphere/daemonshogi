/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * globals.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/globals.c,v $
 * $Id: globals.c,v 1.1.1.1 2005/12/09 09:03:08 tokita Exp $
 */

#include "si.h"
#ifdef USE_SERIAL
#include "comaccess.h"
#endif /* USE_SERIAL */

/* -------------------------------------------------------- */
/* 他のファイル内の関数から更新される変数 */

int flgDisplayMode = 0;

/** 詰将棋の読みの深さの最大 */
int MATE_DEEP_MAX = 0;

/** 指し手探索での読みの深さの最大 */
int THINK_DEEP_MAX = 0;

long count_new_board = 0;
long count_crash2 = 0;
long count_hash_hit = 0;
long count_entry_hash = 0;
long count_boMove = 0;
long count_minmax = 0;

long G_COUNT_HASH_CHECK_MATE = 0;
long G_COUNT_HASH_NOT_CHECK_MATE = 0;

long G_COUNT_ALPHA_CUT;
long G_COUNT_BETA_CUT;

/** f_min() 関数内でハッシュに登録された回数 */
long G_F_MIN_PUT_HASH;
/** f_max() 関数内でハッシュに登録された回数 */
long G_F_MAX_PUT_HASH;

/** GAME */
GAME g_game;

/** 将棋盤 */
BOARD g_board;

/** 最良手 */
TE best_te;

/** 利きのある場所と駒コード */
int toBoard3[2][256][toBoardCountMax];
/** 効きの数 */
int toBoardCount3[2][256];

/** 駒コード毎の移動先情報 */
MOVEINFO code_to[41];

/** 詰将棋用 */
MOVEINFO g_mi[ 100 ];
/* MOVEINFO *g_mi; */

#ifdef USE_SERIAL
COMACCESS g_ca[2];
#endif /* USE_SERIAL */


/* -------------------------------------------------------- */
/* ここから静的なデータ */

/** 盤上の位置の配列 */
int arr_xy[] = {
  0,
  0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
  0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
  0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
  0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49,
  0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59,
  0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69,
  0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79,
  0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
  0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99,
};

/**
 * 周り８方向への移動データ。
 * -16 : 上
 * -15 : 右斜め上
 *  -1 : 右
 *  15 : 右斜め下
 *  16 : 下
 *  17 : 左斜め下
 *   1 : 左
 * -17 : 左斜め上
 */
int arr_round_to[8] = {
  -16, -17, -1, 15, 16, 17, 1, -15,
};

/** 周り２マス分の移動データ */
int arr_rount_to24[24] = {
  -34, -33, -32, -31, -30, 
  -18, -17, -16, -15, -14,
   -2,  -1,        1,   2,
   14,  15,  16,  17,  18, 
   30,  31,  32,  33,  34,
};


/** 持駒に出来る最大数の配列 */
int arr_num_of_piece[] = {
  /* dummy */
  0, 
  /*歩 香 桂 銀 金 角 飛 */
    18, 4, 4, 4, 4, 2, 2,
};

/** 盤面評価の時に使う点数表 */
int arr_point[] = {
/*        歩    香    桂    銀    金    角    飛 */
      0, 100,  430,  450,  640,  690,  890, 1040, /* 基本 */
      0, 420,  630,  640,  670,    0, 1150, 1300, /* 成り */
      0, 100,  430,  450,  640,  690,  890, 1040, /* 基本 */
      0, 420,  630,  640,  670,    0, 1150, 1300, /* 成り */
};

/** 持駒のときの点数表 */
int arr_piece[] = {
/*    歩   香   桂   銀   金    角    飛 */
  0, 115, 480, 510, 720, 780, 1110, 1270,
};

/**
 * 玉からの相対的な位置の評価点数。
 * 先手玉に対する後手の駒の点数。
 * 先手で王が４九で金が５七の場合、X軸は +1 、Y軸は +2 の位置になる。
 * 先手の場合は後手玉にたいする先手の駒の点数。
 */
int arr_point_xy[17][9] = {
  /*  0   +1   +2   +3  +4  +5  +6  +7  +8 (X軸) */
  {  50,  50,  50,  50, 50, 50, 50, 50, 50, }, /* +8 の位置(Y軸) */
  {  50,  50,  50,  50, 50, 50, 50, 50, 50, }, /* +7 の位置(Y軸) */
  {  62,  60,  58,  52, 50, 50, 50, 50, 50, }, /* +6 の位置(Y軸) */
  {  80,  78,  72,  67, 55, 51, 50, 50, 50, }, /* +5 の位置(Y軸) */
  { 100,  99,  95,  87, 78, 69, 50, 50, 50, }, /* +4 の位置(Y軸) */
  { 140, 130, 110, 100, 95, 75, 54, 50, 50, }, /* +3 の位置(Y軸) */
  { 170, 160, 142, 114, 98, 80, 62, 55, 50, }, /* +2 の位置(Y軸) */
  { 170, 165, 150, 121, 94, 78, 58, 52, 50, }, /* +1 の位置(Y軸) */
  { 170, 145, 137, 115, 91, 75, 57, 50, 50, }, /*  0 の位置(Y軸) */
  { 132, 132, 129, 102, 84, 71, 51, 50, 50, }, /* -1 の位置(Y軸) */
  { 100,  97,  95,  85, 70, 62, 50, 50, 50, }, /* -2 の位置(Y軸) */
  {  90,  85,  80,  68, 60, 53, 50, 50, 50, }, /* -3 の位置(Y軸) */
  {  70,  66,  62,  55, 52, 50, 50, 50, 50, }, /* -4 の位置(Y軸) */
  {  54,  53,  51,  55, 52, 50, 50, 50, 50, }, /* -5 の位置(Y軸) */
  {  50,  50,  50,  50, 50, 50, 50, 50, 50, }, /* -6 の位置(Y軸) */
  {  50,  50,  50,  50, 50, 50, 50, 50, 50, }, /* -7 の位置(Y軸) */
  {  50,  50,  50,  50, 50, 50, 50, 50, 50, }, /* -8 の位置(Y軸) */
};

/**
 * 玉からの相対的な位置の評価点数。
 * 先手玉に対する先手駒の点数。
 * 後手の場合は後手玉に対する先手駒の点数。
 */
int arr_point_xy2[17][9] = {
  /*  0   +1   +2   +3  +4  +5  +6  +7  +8 (X軸) */
  {  50,  50,  50,  50, 50, 50, 50, 50, 50, }, /* +8 の位置(Y軸) */
  {  56,  53,  50,  50, 50, 50, 50, 50, 50, }, /* +7 の位置(Y軸) */
  {  64,  61,  55,  50, 50, 50, 50, 50, 50, }, /* +6 の位置(Y軸) */
  {  79,  77,  70,  65, 54, 51, 50, 50, 50, }, /* +5 の位置(Y軸) */
  { 100,  99,  95,  87, 74, 58, 50, 50, 50, }, /* +4 の位置(Y軸) */
  { 116, 117, 101,  95, 88, 67, 54, 50, 50, }, /* +3 の位置(Y軸) */
  { 131, 129, 124, 114, 90, 71, 59, 51, 50, }, /* +2 の位置(Y軸) */
  { 137, 138, 132, 116, 96, 76, 61, 53, 50, }, /* +1 の位置(Y軸) */
  { 142, 142, 136, 118, 98, 79, 64, 52, 50, }, /*  0 の位置(Y軸) */
  { 132, 132, 129, 109, 95, 75, 60, 51, 50, }, /* -1 の位置(Y軸) */
  { 121, 120, 105,  97, 84, 66, 54, 50, 50, }, /* -2 の位置(Y軸) */
  {  95,  93,  89,  75, 68, 58, 51, 50, 50, }, /* -3 の位置(Y軸) */
  {  79,  76,  69,  60, 53, 50, 50, 50, 50, }, /* -4 の位置(Y軸) */
  {  64,  61,  55,  51, 50, 50, 50, 50, 50, }, /* -5 の位置(Y軸) */
  {  56,  52,  50,  50, 50, 50, 50, 50, 50, }, /* -6 の位置(Y軸) */
  {  50,  50,  50,  50, 50, 50, 50, 50, 50, }, /* -7 の位置(Y軸) */
  {  50,  50,  50,  50, 50, 50, 50, 50, 50, }, /* -8 の位置(Y軸) */
};

/** 先手陣の領域 */
int arr_sentejin[] = {
  0,
  0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99,
  0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
  0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79,
};

/** 後手陣の領域 */
int arr_gotejin[] = {
  0,
  0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
  0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
  0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
};
