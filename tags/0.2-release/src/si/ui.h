/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * ui.h
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/ui.h,v $
 * $Id: ui.h,v 1.1.1.1 2005/12/09 09:03:10 tokita Exp $
 */

#ifndef _UI_H_
#define _UI_H_

#include <stdio.h>
#include "si.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* charcter base UI */

void putBOARDforGAME(BOARD *brd, GAME *gm);
void putBOARD(BOARD *brd);
void fputBOARD(FILE *out, int mode, BOARD *bo);
void fputNoBOARD(FILE *out, BOARD *bo);
void fputToBOARD(FILE *out, BOARD *bo);
void printBOARD(BOARD * bo);
void printCode_xy(BOARD * bo);
void printNoBOARD(BOARD * bo);
void printMOVEINFO(MOVEINFO * mi);
void printMOVEINFO2(MOVEINFO2 * mi);
void printTE(TE * te);
void fprintTE(FILE *out, TE * te);
void putPBOX(PBOX *pbox);
void fputPBOX(FILE *out, PBOX *pbox);
void printTREE(TREE *bo, int n);

/* for GUI support */

void set_pending_function(void (*f)(void));
void processing_pending(void);

extern INPUTSTATUS (*si_input_next_gui_human)(BOARD* bo, TE* te);
extern INPUTSTATUS (*si_input_next_network)(BOARD* bo, TE* te);

/* file I/O */
void CSA_output_auto(BOARD *bo);
void CSA_output(BOARD *bo, char *filename);
int loadBOARD(char *filename, BOARD * brd);

/* input functions */
INPUTSTATUS si_input_next_human(BOARD *bo, TE *te);
INPUTSTATUS si_input_next_computer(BOARD *bo, TE *te);
INPUTSTATUS si_input_next_serialport(BOARD *bo, TE *te);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _UI_H_ */

