/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * moveto.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/moveto.c,v $
 * $Id: moveto.c,v 1.1.1.1 2005/12/09 09:03:10 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"

void movetoKei(MOVEINFO * mi, BOARD * bo, int xy, int next);
void movetoNormal(BOARD * bo, int xy, int piece, MOVEINFO *mi);
void movetoLong(MOVEINFO * mi, BOARD * bo, int xy, int next);

void movetoKei2(MOVEINFO * mi, BOARD * bo, int xy, int next);
void movetoNormal2(BOARD * bo, int xy, int piece, MOVEINFO *mi);
void movetoLong2(MOVEINFO * mi, BOARD * bo, int xy, int next);

static const int normal_to_count[] = {
  /* dummy */
  0,

  /* sente */
  1, 0, 0, 5, 6, 0, 0, 8,
  6, 6, 6, 6, 0, 4, 4, 0,

  /* gote */
  1, 0, 0, 5, 6, 0, 0, 8,
  6, 6, 6, 6, 0, 4, 4, 0,
};

static const int normal_to[][8] = {
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dummy */

  /* sente */
  { -16,   0,   0,   0,   0,   0,   0,   0, }, /* fu */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kei */
  { -16, -17,  15,  17, -15,   0,   0,   0, }, /* gin */
  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* kin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kaku */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* hisya */
  { -16, -17,  -1,  15,  16,  17,   1, -15, }, /* oh */

  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* to */
  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* narikyo */
  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* narikei */
  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* narigin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dymmy */
  { -16,  -1,  16,   1,   0,   0,   0,   0, }, /* uma */
  { -17,  15,  17, -15,   0,   0,   0,   0, }, /* ryu */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dummy */

  /* gote */
  {  16,   0,   0,   0,   0,   0,   0,   0, }, /* fu */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kei */
  { -17,  15,  16,  17, -15,   0,   0,   0, }, /* gin */
  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* kin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kaku */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* hisya */
  { -16, -17,  -1,  15,  16,  17,   1, -15, }, /* oh */

  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* to */
  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* narikyo */
  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* narikei */
  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* narigin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dymmy */
  { -16,  -1,  16,   1,   0,   0,   0,   0, }, /* uma */
  { -17,  15,  17, -15,   0,   0,   0,   0, }, /* ryu */
};

/**
 * 駒の動ける場所を探す。
 * @param bo 対象のBOARD
 * @param xy 駒の場所
 * @param mi 結果を格納するMOVEINFO
 */
void moveto(BOARD * bo, int xy, MOVEINFO *mi) {
  int piece, next, p;
  int x, y;

#ifdef DEBUG
  assert( bo != NULL );
  assert( 1 <= (xy&0x0f) && (xy&0x0f) <= 9 && 1 <= (xy>>4) && (xy>>4) <= 9 );
  assert( mi != NULL );
  
  assert(bo->next == 0 || bo->next == 1);
#endif /* DEBUG */

  mi->count = 0;
  
  piece = bo->board[ xy ];
  next = (0x10 & piece) ? GOTE : SENTE;

  p = piece & 0x0F;
  
  x = (xy & 0x0F);
  y = (xy >> 4);
  
  if (p == KEI) {
    movetoKei(mi, bo, xy, next);
    return;
  }

  movetoNormal(bo, xy, piece, mi);

  if (p == KAKU || p == HISYA || p == UMA || p == RYU || p == KYO) {
    movetoLong(mi, bo, xy, next);
  }
}

/**
 * 駒の動ける場所を探す。mi のカウンタをリセットしない版。
 * @param bo 対象のBOARD
 * @param xy 駒の場所
 * @param mi 結果を格納するMOVEINFO
 */
void moveto_add(BOARD * bo, int xy, MOVEINFO *mi) {
  int piece, next, p;
  int x, y;

#ifdef DEBUG
  assert( bo != NULL );
  assert( 1 <= (xy&0x0f) && (xy&0x0f) <= 9 && 1 <= (xy>>4) && (xy>>4) <= 9 );
  assert( mi != NULL );
  
  assert(bo->next == 0 || bo->next == 1);
#endif /* DEBUG */

  piece = bo->board[ xy ];
  next = (0x10 & piece) ? GOTE : SENTE;

  p = piece & 0x0F;
  
  x = (xy & 0x0F);
  y = (xy >> 4);
  
  if (p == KEI) {
    movetoKei(mi, bo, xy, next);
    return;
  }

  movetoNormal(bo, xy, piece, mi);

  if (p == KAKU || p == HISYA || p == UMA || p == RYU || p == KYO) {
    movetoLong(mi, bo, xy, next);
  }
}

/**
 * 駒の動ける場所を探す。効き探す用。
 * @param bo 対象のBOARD
 * @param xy 駒の場所
 * @param mi 結果を格納するMOVEINFO
 */
void movetoKiki(BOARD * bo, int xy, MOVEINFO *mi) {
  int piece, next, p;

#ifdef DEBUG
  assert( bo != NULL );
  assert( 1 <= (xy&0x0f) && (xy&0x0f) <= 9 && 1 <= (xy>>4) && (xy>>4) <= 9 );
  assert( mi != NULL );
  
  assert(bo->next == 0 || bo->next == 1);
#endif /* DEBUG */

  mi->count = 0;
  
  piece = bo->board[ xy ];
  next = (0x10 & piece) ? GOTE : SENTE;

  p = piece & 0x0F;
  
  if (p == KEI) {
    movetoKei2(mi, bo, xy, next);
    return;
  }
  
  movetoNormal2(bo, xy, piece, mi);

  if (p == KAKU || p == HISYA || p == UMA || p == RYU || p == KYO) {
    movetoLong2(mi, bo, xy, next);
  }
}

/**
 * 歩、銀、金、王、と、成杏、成圭、成銀、馬、竜の動きを調べる。
 * @param bo 対象のBOARD
 * @param xy 場所
 * @param piece 駒
 * @param mi 結果を格納するMOVEINFO
 */
void movetoNormal(BOARD * bo, int xy, int piece, MOVEINFO *mi) {
  TE te;
  int vw;
  int next;
  int i, c, p;
  
  for (i = 0; i < normal_to_count[piece]; i++) {
    vw = xy + normal_to[piece][i];

#ifdef DEBUG
    assert( 0 <= (vw >> 4) && (vw >> 4) <= 10 );
    assert( 0 <= (vw & 0xF) && (vw & 0xF) <= 10 );
#endif /* DEBUG */

    c = boGetPiece(bo, vw);
    if (c == WALL)
      continue;
    
    next = (piece & 0x10) == 0x10;
    
    if ( c ) { /* 移動先に駒がある場合 */
      if (next) {
	/* gote */
	if (c & 0x10)
	  continue;
      } else {
	/* sente */
	if ((c & 0x10) == 0)
	  continue;
      }
    }

    te.fm = xy;
    te.to = vw;
    te.nari = 0;
    te.uti = 0;
    
    p = piece & 0x0F;

    if ( p == KIN || p == OH || (p & 8) ) {
      miAdd(mi, &te);
    }else if ( p == FU ) {
      if ( next ) {
	if ( (vw >>4) == 9 ) {
	  te.nari = 1;
	  miAdd(mi, &te);
	} else {
	  miAddWithNari(mi, &te, next);
	}
      } else {
	if ( (vw >> 4)== 1 ) {
	  te.nari = 1;
	  miAdd(mi, &te);
	} else {
	  miAddWithNari(mi, &te, next);
	}
      }
    } else {
      miAddWithNari(mi, &te, next);
    }
  }
}

/**
 * 歩、銀、金、王、と、成杏、成圭、成銀、馬、竜の動きを調べる。
 * 効きを調べる用。
 * コードは正しいけど、非常に分かりにくくなっているので
 * 整理する。
 * @param bo 対象のBOARD
 * @param xy 場所
 * @param piece 駒
 * @param mi 結果を格納するMOVEINFO
 */
void movetoNormal2(BOARD * bo, int xy, int piece, MOVEINFO *mi) {
  TE te;
  int vw;
  int next;
  int i, c, p;

  /*
  x = xy & 0x0F;
  y = xy >> 4;
   */

  for (i = 0; i < normal_to_count[piece]; i++) {
    /*
    v = x + direc[piece * 8 + i][0];
    w = y + direc[piece * 8 + i][1];
     */
    
    vw = xy + normal_to[piece][i];

    c = boGetPiece(bo, vw);
    if (c == WALL)
      continue;
    
    next = (piece & 0x10) == 0x10;
    
    te.fm = xy;
    te.to = vw;
    te.nari = 0;
    te.uti = 0;
    
    p = piece & 0x0F;

    if ( p == KIN || p == OH || (p & 8) ) {
      miAdd(mi, &te);
    }else if ( p == FU ) {
      if ( next ) {
	if ( (vw >> 4) == 9 ) {
	  te.nari = 1;
	  miAdd(mi, &te);
	} else {
	  miAddWithNari(mi, &te, next);
	}
      } else {
	if ( (vw >> 4) == 1 ) {
	  te.nari = 1;
	  miAdd(mi, &te);
	} else {
	  miAddWithNari(mi, &te, next);
	}
      }
    } else {
      miAddWithNari(mi, &te, next);
    }
  }
}

/**
 * 桂馬の動ける場所を探す。
 * @param mi 結果を格納するMOVEINFO
 * @param bo 対象のBOARD
 * @param xy 座標
 * @param next 駒の持ち主
 */
void movetoKei(MOVEINFO * mi, BOARD * bo, int xy, int next) {
  TE te;

#ifdef DEBUG
  assert( mi != NULL );
  assert( bo != NULL );
  assert( 1 <= (xy & 0x0F) && (xy & 0x0F) <= 9 &&
	 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( bo->next == 0 || bo->next == 1 );
#endif /* DEBUG */
  
  if ( next ) {
    /* gote */
    if ( bo->board[ xy + 0x21 ] != WALL &&
	notGotePiece(bo->board[ xy + 0x21 ]) ) {
      te.fm = xy;
      te.to = xy + 0x21;
      te.uti = 0;
      if ( 0x61 <= xy ) {
	te.nari = 1;
	miAdd(mi, &te);
      }
      else {
	te.nari = 1;
	miAddWithNari(mi, &te, next);
      }
    }
    if ( bo->board[ xy + 0x1F ] != WALL &&
	notGotePiece(bo->board[ xy + 0x1F ]) ) {
      te.fm = xy;
      te.to = xy + 0x1F;
      te.uti = 0;
      if ( 0x61 <= xy ) {
	te.nari = 1;
	miAdd(mi, &te);
      }
      else {
	miAddWithNari(mi, &te, next);
      }
    }
  } else {
    /* sente */
    if ( bo->board[ xy - 0x21 ] != WALL &&
	notSentePiece(bo->board[ xy - 0x21 ]) ) {
      te.fm = xy;
      te.to = xy - 0x21;
      te.uti = 0;
      if ( xy <= 0x49 ) {
	te.nari = 1;
	miAdd(mi, &te);
      }
      else {
	miAddWithNari(mi, &te, next);
      }
    }
    if ( bo->board[ xy - 0x1F ] != WALL &&
	notSentePiece(bo->board[ xy - 0x1F ]) ) {
      te.fm = xy;
      te.to = xy - 0x1F;
      te.uti = 0;
      if ( xy <= 0x49 ) {
	te.nari = 1;
	miAdd(mi, &te);
      }
      else {
	miAddWithNari(mi, &te, next);
      }
    }
  }
}

/**
 * 桂馬の動ける場所を探す。効きを調べる用。
 * @param mi 結果を格納するMOVEINFO
 * @param bo 対象のBOARD
 * @param xy 座標
 * @param next 駒の持ち主
 */
void movetoKei2(MOVEINFO * mi, BOARD * bo, int xy, int next) {
  TE te;

#ifdef DEBUG
  assert( mi != NULL );
  assert( bo != NULL );
  assert( 1 <= (xy & 0x0F) && (xy & 0x0F) <= 9 &&
	 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( bo->next == 0 || bo->next == 1 );
#endif /* DEBUG */
  
  if ( next ) {
    /* gote */
    if ( bo->board[ xy + 0x21 ] != WALL && (xy + 0x21) <= 0x99 ) {
      te.fm = xy;
      te.to = xy + 0x21;
      te.uti = 0;
      if ( 0x61 <= xy ) {
	te.nari = 1;
	miAdd(mi, &te);
      }
      else {
	te.nari = 0;
	miAddWithNari(mi, &te, next);
      }
    }
    if ( bo->board[ xy + 0x1F ] != WALL && (xy + 0x1F) <= 0x99 ) {
      te.fm = xy;
      te.to = xy + 0x1F;
      te.uti = 0;
      if ( 0x61 <= xy ) {
	te.nari = 1;
	miAdd(mi, &te);
      }
      else {
	te.nari = 0;
	miAddWithNari(mi, &te, next);
      }
    }
  } else {
    /* sente */
    if ( bo->board[ xy - 0x21 ] != WALL && 0x11 <= (xy - 0x21) ) {
      te.fm = xy;
      te.to = xy - 0x21;
      te.uti = 0;
      if ( xy <= 0x49 ) {
	te.nari = 1;
	miAdd(mi, &te);
      }
      else {
	te.nari = 0;
	miAddWithNari(mi, &te, next);
      }
    }
    if ( bo->board[ xy - 0x1F ] != WALL && 0x11 <= (xy - 0x1F) ) {
      te.fm = xy;
      te.to = xy - 0x1F;
      te.uti = 0;
      if ( xy <= 0x49 ) {
	te.nari = 1;
	miAdd(mi, &te);
      }
      else {
	te.nari = 0;
	miAddWithNari(mi, &te, next);
      }
    }
  }
}

static const int remote_to_count[] = {
  0,
  0, 1, 0, 0, 0, 4, 4, 0,
  0, 0, 0, 0, 0, 4, 4, 0,
  0, 1, 0, 0, 0, 4, 4, 0,
  0, 0, 0, 0, 0, 4, 4, 0,
};

static const int remote_to[][8] = {
  { 0, 0, 0, 0, 0, 0, 0, 0, }, /* dummy */

  /* sente */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* fu */
  { -16,   0,   0,   0,   0,   0,   0,   0, }, /* kyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kei */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* gin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kin */
  {  17,  15, -15, -17,   0,   0,   0,   0, }, /* kaku */
  {   1,  -1, -16,  16,   0,   0,   0,   0, }, /* hisya */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* oh */

  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* to */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narikyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narikei */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narigin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dymmy */
  {  17,  15, -15, -17,   0,   0,   0,   0, }, /* uma */
  {   1,  -1, -16,  16,   0,   0,   0,   0, }, /* ryu */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dummy */

  /* gote */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* fu */
  {  16,   0,   0,   0,   0,   0,   0,   0, }, /* kyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kei */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* gin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kin */
  {  17,  15, -15, -17,   0,   0,   0,   0, }, /* kaku */
  {   1,  -1, -16,  16,   0,   0,   0,   0, }, /* hisya */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* oh */

  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* to */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narikyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narikei */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narigin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dymmy */
  {  17,  15, -15, -17,   0,   0,   0,   0, }, /* uma */
  {   1,  -1, -16,  16,   0,   0,   0,   0, }, /* ryu */
};

/**
 * 遠くに効く駒の動きを調べる。
 * @param mi 結果を格納するMOVEINFO
 * @param bo 対象のBOARD
 * @param x 駒のx座標
 * @param y 駒のy座標
 * @param next 駒の持ち主
 */
void movetoLong(MOVEINFO * mi, BOARD * bo, int xy, int next) {
  int p, i, j;
  int c, flag;
  int vw;
  int piece = boGetPiece(bo, xy);
  TE te;
  
#ifdef DEBUG
  assert( mi != NULL );
  assert( bo != NULL );
  assert( 1 <= (xy & 0xf) && (xy & 0xf) <= 9 );
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( next == 0 || next == 1 );
#endif /* DEBUG */

  p = piece & 0x0F;

  for (i = 0; i < remote_to_count[piece]; i++) {
    vw = xy;
    for (j = 1, flag = 0; !flag; j++) {
      vw += remote_to[piece][i];
      
      if ((vw & 0xF) < 1 || 9 < (vw & 0xF) || (vw >> 4) < 1 || 9 < (vw >> 4))
	goto end;

      c = boGetPiece(bo, vw);
      if (c == WALL)
	goto end;
      if (c) {
	if (next) {	/* sente */
	  if (c & 0x10)
	    break;
	} else {	/* gote */
	  if (!(c & 0x10))
	    break;
	}
	flag = 1;
      }

      te.fm = xy;
      te.to = vw;
      te.nari = 0;
      te.uti = 0;

      if ( p == UMA || p == RYU ) {
	miAdd(mi, &te);
      } else if ( p == KYO ) {
	if ( next ) {
	  if ( (vw >> 4) == 9 ) {
	    te.nari = 1;
	    miAdd(mi, &te);
	  } else {
	    miAddWithNari(mi, &te, next);
	  }
	} else {
	  if ( (vw >> 4) == 1 ) {
	    te.nari = 1;
	    miAdd(mi, &te);
	  } else {
	    miAddWithNari(mi, &te, next);
	  }
	}
      } else {
	miAddWithNari(mi, &te, next);
      }
    }

    end:;
  }
}

/**
 * 遠くに効く駒の動きを調べる。効きを調べる用。
 * @param mi 結果を格納するMOVEINFO
 * @param bo 対象のBOARD
 * @param x 駒のx座標
 * @param y 駒のy座標
 * @param next 駒の持ち主
 */
void movetoLong2(MOVEINFO * mi, BOARD * bo, int xy, int next) {
  int p, i, j;
  int flag;
  int vw;
  int piece;
  TE te;
  
#ifdef DEBUG
  assert( mi != NULL );
  assert( bo != NULL );
  assert( next == 0 || next == 1 );
#endif /* DEBUG */

  piece = bo->board[ xy ];
  p = piece & 0x0F;

  te.uti = 0;
  
  if ( next == SENTE ) {

  for (i = 0; i < remote_to_count[piece]; i++) {
    vw = xy;
    for (j = 0, flag = 0; (flag == 0) && (j < 8); j++) {
      vw += remote_to[piece][i];

      if ( bo->board[ vw ] == WALL )
	break;
      
      if ( bo->board[ vw ] )
	flag = 1;
      
      te.fm = xy;
      te.to = vw;
      te.nari = 0;
      
      if ( p == UMA || p == RYU ) {
	miAdd(mi, &te);
      } else if ( p == KYO ) {
	if ( vw <= 0x19 ) {
	  te.nari = 1;
	  miAdd(mi, &te);
	} else {
	  miAddWithNariSente(mi, &te);
	}
      } else {
	miAddWithNariSente(mi, &te);
      }
    }
  }
   
  } else {
  
  for (i = 0; i < remote_to_count[piece]; i++) {
    vw = xy;
    for (j = 0, flag = 0; (flag == 0) && (j < 8); j++) {
      vw += remote_to[piece][i];

      if ( bo->board[ vw ] == WALL )
	break;
      
      if ( bo->board[ vw ] )
	flag = 1;
      
      te.fm = xy;
      te.to = vw;
      te.nari = 0;
      
      if ( p == UMA || p == RYU ) {
	miAdd(mi, &te);
      } else if ( p == KYO ) {
	if ( 0x91 <= vw ) {
	  te.nari = 1;
	  miAdd(mi, &te);
	} else {
	  miAddWithNariGote(mi, &te);
	}
      } else {
	miAddWithNariGote(mi, &te);
      }
    }
  }
  }
}

/**
 * xy に動ける next 手番の駒を探して MOVEINFO に te を記録する。
 * ただし王は除く。
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 * @param xy 場所
 * @param next 次の手番
 */
void moveto2(BOARD *bo, MOVEINFO *mi, int xy, int next) {
  TE te;
  int i, n, p;
  extern int toBoard3[2][256][toBoardCountMax];
  extern int toBoardCount3[2][256];

  mi->count = 0;

  for (i=0; i<toBoardCount3[ next ][ xy ]; i++) {
    n = toBoard3[ next ][ xy ][ i ];
    p = bo->code_xy[ n ][ 1 ];

    if ( p == OH || p == (OH + 0x10) )
      continue;

    p &= 0x0F;
    te.fm = bo->code_xy[ n ][ 0 ];
    te.to = xy;
    te.nari = 0;
    te.uti = 0;
    
    miAddWithNari2(mi, te, p, next);
  }
}

/**
 * xy に動ける next 手番の駒を探して MOVEINFO に te を記録する。
 * mi をクリアしないで手を追加する。自分の駒がある場合は mi に記録しない。
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 * @param xy 場所
 * @param next 次の手番
 */
void moveto2_add(BOARD *bo, MOVEINFO *mi, int xy, int next) {
  TE te;
  int i, n, p, p2;
  extern int toBoard3[2][256][toBoardCountMax];
  extern int toBoardCount3[2][256];
  
  p2 = boGetPiece(bo, xy);
  if ( p2 != 0 && getTeban(p2) == next ) {
    return;
  }

  for (i=0; i<toBoardCount3[ next ][ xy ]; i++) {
    n = toBoard3[ next ][ xy ][ i ];
    p = bo->code_xy[ n ][ 1 ];

    p &= 0x0F;
    te.fm = bo->code_xy[ n ][ 0 ];
    te.to = xy;
    te.nari = 0;
    te.uti = 0;
    
    miAddWithNari2(mi, te, p, next);
  }
}
