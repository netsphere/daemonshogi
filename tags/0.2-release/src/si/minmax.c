/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/**
 * mixmax.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/minmax.c,v $
 * $Id: minmax.c,v 1.1.1.1 2005/12/09 09:03:09 tokita Exp $
 */

#include <stdio.h>
#include <assert.h>
#include "si.h"
#include "ui.h"

#ifdef DEBUG
static long max_point;
static long min_point;
#endif /* DEBUG */

/* minmax.c の中ではハッシュを使わない */
#undef USE_HASH

int jouseki(BOARD *bo, TE *te);

/**
 * min/max 探索を行う。
 * @param bo 対象のBOARD
 * @return 探索結果を返す。
 *         SI_NORMALならば通常の手。SI_WINならばプログラムの勝ち。
 *         SI_LOSTならばプログラムの負け。
 */
INPUTSTATUS minmax(BOARD *bo) {
  int alpha, beta;
  int i;
  static TREE root;
  static MOVEINFO mi;
  int king_xy;
  int num_of_ohte;
  extern TE best_te;
  extern int MATE_DEEP_MAX;
  extern GAME g_game;
#ifdef DEBUG
  extern long G_COUNT_ALPHA_CUT;
  extern long G_COUNT_BETA_CUT;
  extern long G_F_MIN_PUT_HASH;
  extern long G_F_MAX_PUT_HASH;
#endif /* DEBUG */

#ifdef DEBUG
  G_COUNT_ALPHA_CUT = 0;
  G_COUNT_BETA_CUT = 0;
  G_F_MIN_PUT_HASH = 0;
  G_F_MAX_PUT_HASH = 0;
#endif /* DEBUG */

#ifdef NOREAD   
  if ( jouseki(bo, &best_te) ) {
    /* 定跡手があった */
     
    return SI_NORMAL;
  }
#endif /* NOREAD */
   
  king_xy = bo->code_xy[ 1 + (bo->next == 0) ][ 0 ];

  best_te.fm = 0;
  best_te.to = 0;
  best_te.nari = 0;
  best_te.uti = 0;
  
  moveto2(bo, &mi, king_xy, bo->next);

  if ( 0 < mi.count ) {
    /* 王手された状態で思考ルーチンに入った */
    return SI_WIN;
  }
  
  alpha = -30000;
  beta = 30000;
  
  king_xy = bo->code_xy[1 + bo->next][0];
  /* 王手さているか調べる */
  num_of_ohte = boGetToBoardCount(king_xy, (bo->next == 0));

  if ( num_of_ohte == 0 ) {
    /* 自玉が王手されていなかったら詰みがあるか調べる */
    initTREE( &root );

    for ( i=0; i<6; i++ ) {
      MATE_DEEP_MAX = i;
      if ( mate(bo, &root, 0) ) {
	/* 詰んだ */
	break;
      }
    }
    
    if ( root.child != NULL ) {
      /* check mate */
      best_te.fm = root.child->te.fm;
      best_te.to = root.child->te.to;
      best_te.nari = root.child->te.nari;
      best_te.uti = root.child->te.uti;
    
      freeTREE( root.child );
      return SI_NORMAL;
    }
  }
  
#ifdef DEBUG
  max_point = -3000000;
  min_point = 3000000;
#endif /* DEBUG */
  
  f_max( bo, 0, alpha, beta );

  if ( gmGetGameStatus(&g_game) == SI_CHUDAN ) {
    return SI_CHUDAN_RECV;
  } 

  if ( best_te.fm == 0 && best_te.to == 0 && best_te.nari == 0 &&
       best_te.uti == 0 ) {
    /* 指し手がない。負け */
    return SI_TORYO;
  }

  return SI_NORMAL;
}

/**
 * min関数
 * 対戦相手の局面を探索する。盤面評価が小さくなるように手を選ぶ。
 * @param bo 対象のBOARD
 * @param depth 検索の深さ
 * @param alpha alpha値
 * @param beta beta値
 * @return
 */
int f_min(BOARD *bo, int depth, int alpha, int beta) {
  MOVEINFO *mi;
  int point, value;
  int flg_tori;
  int prev_beta;
  int i;
  extern int THINK_DEEP_MAX;
  extern long count_minmax;
  extern GAME g_game;
  
#ifdef DEBUG
  extern long G_COUNT_BETA_CUT;
#endif /* DEBUG */

  count_minmax++;

#ifdef USE_GUI  
  if ( depth < 2 ) {
    /* 中断や投了が入ってないか調べる */
    if ( gmGetGameStatus(&g_game) == SI_CHUDAN ||
	 gmGetGameStatus(&g_game) == SI_TORYO ||
	 gmGetChudanFlg(&g_game) == 1) {
      /* 中断または投了が入ったので終了する */
      return -30000;
    }
    /* GUIのペンディングされたイベントを処理する */
    processing_pending();
  }
#endif /* USE_GUI */
  

#ifdef DEBUG
  assert( bo != NULL );
#endif /* DEBUG */

  if ( depth >= THINK_DEEP_MAX ) {
    return bo->point[ (bo->next == 0) ] - bo->point[ bo->next ];
  }

#ifdef USE_HASH
  /* ハッシュに盤面が登録されていないか調べる */
  if ( hsGetBOARDMinMax(bo, &point, bo->mi.count - depth, 0 ) ) {
    /* 登録されていた */
    return point;
  }
#endif /* USE_HASH */

  point = 99999999;

  mi = newMOVEINFO();

  think( bo, mi, depth );

  prev_beta = beta;

  if ( mi->count == 0 ) {
    /* ルール上指せる手がない。敗け */
    freeMOVEINFO( mi );

    return -30000;
  }

  for (i=0; i<mi->count; i++) {
    flg_tori = boMove( bo, &(mi->te[ i ]), bo->next);
    depth++;
    boToggleNext( bo );
    /*
    if ( flg_tori && 10 <= THINK_DEEP_MAX )
      flg_tori = 0;
    if ( flg_tori && depth != THINK_DEEP_MAX )
      flg_tori = 0;
    if ( flg_tori )
      THINK_DEEP_MAX += 2;
    */
    value = f_max(bo, depth, alpha, beta);
    /*
    if ( flg_tori )
      THINK_DEEP_MAX -= 2;
    */
    boToggleNext( bo );
    boBack(bo, bo->next);
    depth--;

    /*
    if ( value < beta ) {
      beta = value;
    }
    if ( beta <= alpha ) {
      freeMOVEINFO( mi );
      return beta;
    }
    */

#define NOREAD2
#ifdef NOREAD2
    if ( value < point ) {
      point = value;
      if ( point < beta )
	beta = point;

      /* 枝刈り */
      if ( alpha >= beta ) {
#ifdef USE_HASH
	/* ここでハッシュへ局面と点数を記録する */
	hsPutBOARDMinMax(bo, value, bo->mi.count - depth, 0);
#endif /* USE_HASH */
#ifdef DEBUG
	G_COUNT_BETA_CUT++;
#endif /* DEBUG */
	freeMOVEINFO( mi );
	return beta;
      }
    }
#endif /* NOREAD2 */

  }
  
  freeMOVEINFO( mi );

  if ( prev_beta != beta ) {
#ifdef USE_HASH
    /* 一回でも beta 値が更新されたらハッシュに登録する */
    hsPutBOARDMinMax(bo, point, bo->mi.count - depth, 0);
#endif /* USE_HASH */
  }

  return point;
}

/**
 * max関数
 * 自分の局面を探索する。盤面評価が大きくなるように手を選ぶ。
 * @param bo 対象のBOARD
 * @param depth 検索の深さ
 * @param alpha alpha値
 * @param beta beta値
 * @return
 */
int f_max(BOARD *bo, int depth, int alpha, int beta) {
  MOVEINFO *mi;
  int point, value;
  int prev_alpha;
  int i;
  extern int THINK_DEEP_MAX;
  extern TE best_te;
  extern long count_minmax;
  int flg_tori;
#ifdef DEBUG
  int dpoint;
  extern long G_COUNT_ALPHA_CUT;
#endif /* DEBUG */
  
  count_minmax++;

  if ( depth >= THINK_DEEP_MAX ) {
    return bo->point[ bo->next ] - bo->point[ (bo->next == 0)];
  }

  /* ハッシュに盤面が登録されていないか調べる */
#ifdef USE_HASH
  if ( hsGetBOARDMinMax(bo, &point, bo->mi.count - depth, 0 ) ) {
    /* 登録されていた */
    return point;
  }
#endif /* USE_HASH */

  point = -99999999;

  mi = newMOVEINFO();
  mi->count = 0;

  think( bo, mi, depth );
  
  if ( mi->count == 0 ) {
    /* ルール上指せる手がない。敗け */
    freeMOVEINFO( mi );

    return -30000;
  }

  if ( depth == 0 ) {
    best_te = mi->te[ 0 ];
  }

  prev_alpha = alpha;

  for (i=0; i<mi->count; i++) {
#ifdef DEBUG
    dpoint = bo->point[ 0 ];
#endif /* DEBUG */
    /*
    for ( j=0; j<depth; j++ )
      putchar(' ');
    printTE( &(mi->te[ i ]) );
     */ 
    flg_tori = boMove( bo, &(mi->te[ i ]), bo->next);
    depth++;
    boToggleNext( bo );
    /*
    if ( flg_tori && 10 <= THINK_DEEP_MAX )
      flg_tori = 0;
    if ( flg_tori && depth != THINK_DEEP_MAX )
      flg_tori = 0;
    if ( flg_tori )
      THINK_DEEP_MAX += 2;
    */
    value = f_min(bo, depth, alpha, beta);
    /*
    if ( flg_tori )
      THINK_DEEP_MAX -= 2;
    */
    boToggleNext( bo );
    boBack(bo, bo->next);
    depth--;

    /*
    if ( alpha < value ) {
      alpha = value;
      if ( depth == 0 ) {
	best_te = mi->te[ i ];
      }
    }
    if ( beta <= alpha ) {
      freeMOVEINFO( mi );
      return alpha;
    }
     */

    if ( point < value ) {
      point = value;
      if ( alpha < point ) {
	alpha = point;
      }

      /* 枝刈り */
      if ( alpha >= beta ) {
#ifdef USE_HASH
	/* ここでハッシュへ局面と点数を記録する */
	hsPutBOARDMinMax(bo, value, bo->mi.count - depth, 0);
#endif /* USE_HASH */
#ifdef DEBUG
	G_COUNT_ALPHA_CUT++;
#endif /* DEBUG */
	/* printf("  alpha cut (alpha,beta)=(%d,%d)\n", alpha, beta); */
	freeMOVEINFO( mi );
	return alpha;
      }

      if ( depth == 0 ) {
#ifdef DEBUG      
	printf("hit - %d - ", point);
	printTE( &(mi->te[ i ]) );
#endif /* DEBUG */
	best_te = mi->te[ i ];
      }
    }

#ifdef DEBUG    
    assert( dpoint == bo->point[ 0 ] );
#endif /* DEBUG */
  }

  freeMOVEINFO( mi );

  if ( prev_alpha != alpha ) {
#ifdef USE_HASH
    /* 一回でも alph値が更新されたらハッシュに登録する */
    hsPutBOARDMinMax(bo, point, bo->mi.count - depth, 0);
#endif /* USE_HASH */
  }

  /* return alpha; */
  return point;
}

/**
 * 定跡手があるか調べてあったら te に格納する。
 * @param bo 対象のBOARD
 * @return 1 ならば定跡手があった。 0 ならばなし。
 */
int jouseki(BOARD *bo, TE *te) {
  if ( bo->next == SENTE ) {
    if ( bo->mi.count == 0 ) {
      te->fm = 0x77;
      te->to = 0x67;
      te->nari = 0;
      te->uti = 0;
      return 1;
    } else if ( bo->mi.count == 2 ) {
      te->fm = 0x96;
      te->to = 0x87;
      te->nari = 0;
      te->uti = 0;
      return 1;
    }
  } else {
    if ( bo->mi.count == 1 ) {
      te->fm = 0x14;
      te->to = 0x23;
      te->nari = 0;
      te->uti = 0;
      return 1;
    } else if ( bo->mi.count == 3 ) {
      te->fm = 0x33;
      te->to = 0x43;
      te->nari = 0;
      te->uti = 0;
      return 1;
    }
  }
   
  return 0;
}

