/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * tree.c
 * TREE構造体関係
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/tree.c,v $
 * $Id: tree.c,v 1.1.1.1 2005/12/09 09:03:10 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"
#include "ui.h"

void trSearch(TREE *tree, MOVEINFO *mi, MOVEINFO *t_mi, int *max, int count);

/**
 * 新しくメモリを確保してTREEのポインタを返す。
 * @return TREEのポインタ
 */
TREE *newTREE(void) {
  TREE *tree;

  tree = (TREE *) malloc(sizeof(TREE));
  if (tree == NULL) {
    si_abort("No enough memory. In tree.c#newTREE()");
  }

  initTREE(tree);

  return tree;
}

/**
 * tree を初期化する。
 * @param 対象のTREE
 */
void initTREE(TREE *tree) {
  tree->te.fm   = 0;
  tree->te.to   = 0;
  tree->te.nari = 0;
  tree->te.uti  = 0;
  tree->parent  = NULL;
  tree->brother = NULL;
  tree->child   = NULL;
}

/**
 * tree のメモリを解放する。
 * @param bo 対象のTREE
 */
void freeTREE(TREE *tree) {
#ifdef DEBUG
  assert ( tree != NULL );
#endif /* DEBUG */

  if ( tree->child != NULL )
    freeTREE( tree->child );

  if ( tree->brother != NULL )
    freeTREE( tree->brother );

  free(tree);
}

void trSearch(TREE *tree, MOVEINFO *mi, MOVEINFO *t_mi, int *max, int count) {
  /* miAdd(t_mi, &(tree->te)); */
  t_mi->te[count - 1] = tree->te;

  if (tree->child != NULL) {
    trSearch(tree->child, mi, t_mi, max, count + 1);
    /* t_mi->count--; */
  } else {
    if (*max <= count) {
      *max = count;
      t_mi->count = count;
      miCopy(mi, t_mi);
    }
  }
  if (tree->brother != NULL) {
    /* t_mi->count--; */
    trSearch(tree->brother, mi, t_mi, max, count);
  }
}

/**
 * tree の内容を MOVEINFO に格納する。
 *
 */	       
void trSetMoveinfo(TREE *tree, MOVEINFO *mi) {
  MOVEINFO t_mi;
  int max, count;

  assert(tree != NULL);
  assert(mi != NULL);

  max = 0;
  count = 1;
  t_mi.count = 0;

  trSearch(tree, mi, &t_mi, &max, count);
}

