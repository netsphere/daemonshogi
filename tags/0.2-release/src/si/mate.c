/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/**
 * mate.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/mate.c,v $
 * $Id: mate.c,v 1.1.1.1 2005/12/09 09:03:09 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"
#include "ui.h"

void addTREEChild(TREE *parent, TREE *child);
TREE *findPointerMe(TREE *tree, TREE *target);
void removeTREEChild(TREE *tree);

/**
 * bo の局面に詰みがあるか調べる。
 * 詰みを発見した場合は tree に手順をセットする。
 * ( tree 以下のデータ構造は freeTREE() を使って開放する必要がある。
 * freeTREE(tree->child) のようにする。 )
 * gn は現在の読みの深さをセットする。通常は 0 をセットするだけでよい。
 * ( gn は mate() 自身が再帰呼出しするときに +1 されてセットされる )
 * 最大の読みの深さは MATE_DEEP_MAX に指定する。
 * @param bo 対象のBOARD
 * @param gn 手数
 * @return CHECK_MATE の場合、詰みがある。NOT_CHECK_MATE は詰みが見付から
 *         なかった。
 */
MATESTATUS mate(BOARD *bo, TREE *tree, int gn) {
  MOVEINFO *mi1, *mi2;
  TREE *tree1, *tree2;
  extern int MATE_DEEP_MAX;
  int count;
  int i, j;
  int king_xy;
  extern GAME g_game;
  /* extern MOVEINFO g_mi[]; */
#ifdef USE_HASH
  extern long count_hash_hit;
  MATESTATUS mate_stat;
#endif /* USE_HASH */

#ifdef USE_GUI  
  /* GUIのペンディングされたイベントを処理する */
  if (gn < 3) {
    /* 中断や投了が入ってないか調べる */
    if ( gmGetGameStatus(&g_game) == SI_CHUDAN ||
	 gmGetGameStatus(&g_game) == SI_TORYO ||
	 gmGetChudanFlg(&g_game) == 1) {
      /* 中断または投了が入ったので終了する */
      return NOT_CHECK_MATE;
    }
    processing_pending();
  }
#endif /* USE_GUI */

#ifdef USE_HASH
  if ( hsGetBOARD( bo, &mate_stat, bo->mi.count - gn ,0 ) ) {
    count_hash_hit++;
    /* 盤面が登録されていた */
    if ( mate_stat == CHECK_MATE ) {
      /* 詰みの結論が出ているので調べなくよい */
      return CHECK_MATE;
    } else if ( mate_stat == NOT_CHECK_MATE ) {
      /* 不詰み */
#ifdef DEBUG
      assert(1); /* 現在は不詰みは登録していない */
#endif /* DEBUG */
      return NOT_CHECK_MATE;
    } else {
      /* 不明 */
    }
  }
#endif /* USE_HASH */

  mi1 = newMOVEINFO();
  /* mi1 = &(g_mi[ (gn << 1) + 1 ]); */
  
  ohte(bo, mi1);
  
#ifdef USE_HASH_NO
  if ( mi1->count == 0 ) {
    /* 王手がない */
    hsPutBOARD( bo, NOT_CHECK_MATE, bo->mi.count - gn, 0);
  }
#endif /* USE_HASH */

  for (i=0; i<mi1->count; i++) {
    boMove_mate( bo, &(mi1->te[i]), bo->next );
    king_xy = bo->code_xy[bo->next + 1][0];
    if (king_xy != 0 && boGetToBoardCount(king_xy, (bo->next == 0))) {
      /* 逆王手になっている */
      boBack_mate( bo, bo->next );
      continue;
    }

    mi2 = newMOVEINFO();
    /* mi2 = &(g_mi[(gn << 1) + 2]); */

    uke(bo, mi2, (bo->next == 0));
    
    if ( mi2->count == 0 && mi1->te[i].uti == FU ) {
      /* 打ち歩詰め。 */
      freeMOVEINFO( mi2 );
      boBack_mate( bo, bo->next );
      continue;
    }
    
    tree1 = newTREE();
    tree1->te = mi1->te[i];
    addTREEChild( tree, tree1 );
    
    if ( mi2->count == 0 ) {
      /* 詰み */

      freeMOVEINFO( mi1 );
      freeMOVEINFO( mi2 );

      boBack_mate( bo, bo->next );

#ifdef USE_HASH
      hsPutBOARD( bo, CHECK_MATE, bo->mi.count - gn, 0);
#endif /* USE_HASH */
      return CHECK_MATE;
    }

    if ( MATE_DEEP_MAX <= gn ) {
      freeMOVEINFO( mi2 );
      boBack_mate( bo, bo->next );
      removeTREEChild( tree1 );
      continue;
    }

    count = 0;
    for (j=0; j<mi2->count; j++) {
      boMove_mate( bo, &(mi2->te[j]), (bo->next == 0));
      tree2 = newTREE();
      tree2->te = mi2->te[j];
      addTREEChild( tree1, tree2 );

      if ( mate( bo, tree2, gn + 1 ) == NOT_CHECK_MATE ) {
	/* 詰まなかった */
	/* この王手(mi1->te[i])では詰まない */
	boBack_mate( bo, bo->next );
#ifdef USE_HASH_NO
	hsPutBOARD( bo, NOT_CHECK_MATE, bo->mi.count - gn, 0 );
#endif /* USE_HASH */
	goto NEXT;
      } else {
	count++;
      }
      boBack_mate( bo, bo->next );
    }
    if ( mi2->count == count ) {
      /* 詰み */

      freeMOVEINFO( mi1 );
      freeMOVEINFO( mi2 );

      boBack_mate( bo, bo->next );

#ifdef USE_HASH    
      hsPutBOARD( bo, CHECK_MATE, bo->mi.count - gn, 0 );
#endif /* USE_HASH */
      return CHECK_MATE;
    }

    NEXT:;

    freeMOVEINFO( mi2 );
    removeTREEChild( tree1 );
    boBack_mate( bo, bo->next );
  }
  
  freeMOVEINFO( mi1 );

  return NOT_CHECK_MATE;
}

/**
 * 共謀数を使った詰将棋解答関数。
 * bo の局面に詰みがあるか調べる。
 * 詰みを発見した場合は tree に手順をセットする。
 * ( tree 以下のデータ構造は freeTREE() を使って開放する必要がある。
 * freeTREE(tree->child) のようにする。 )
 * gn は現在の読みの深さをセットする。通常は 0 をセットするだけでよい。
 * ( gn は mate() 自身が再帰呼出しするときに +1 されてセットされる )
 * 最大の読みの深さは MATE_DEEP_MAX に指定する。
 * @param bo 対象のBOARD
 * @param gn 手数
 * @return CHECK_MATE の場合、詰みがある。NOT_CHECK_MATE は詰みが見付から
 *         なかった。
 */
MATESTATUS cmate(BOARD *bo, TREE *tree, int cn) {
  MOVEINFO *mi1, *mi2;
  TREE *tree1, *tree2;
  int count;
  int i, j;
  int king_xy;
  /* extern MOVEINFO g_mi[]; */
#ifdef USE_HASH
  extern long count_hash_hit;
  int mate_stat;
#endif /* USE_HASH */
  
  /* printf("cn = %d\n", cn); */

#ifdef USE_GUI
  /* GUIのペンディングされたイベントを処理する */
  if ((cn % 2) == 0) {
    processing_pending();
  }
#endif /* USE_GUI */

#ifdef USE_HASH
  if ( hsGetBOARD( bo, &mate_stat, bo->mi.count, 0 ) ) {
    count_hash_hit++;
    /* 盤面が登録されていた */
    if ( mate_stat == CHECK_MATE ) {
      /* 詰みの結論が出ているので調べなくよい */
      return CHECK_MATE;
    } else if ( mate_stat == NOT_CHECK_MATE ) {
      /* 不詰み */
#ifdef DEBUG
      assert(1); /* 現在は不詰みは登録していない */
#endif /* DEBUG */
      return NOT_CHECK_MATE;
    } else {
      /* 不明 */
    }
  }
#endif /* USE_HASH */

  mi1 = newMOVEINFO();
  /* mi1 = &(g_mi[ (gn << 1) + 1 ]); */
  
  ohte(bo, mi1);
  
#ifdef USE_HASH_NO
  if ( mi1->count == 0 ) {
    /* 王手がない */
    hsPutBOARD( bo, NOT_CHECK_MATE, bo->mi.count, 0);
  }
#endif /* USE_HASH */

  for (i=0; i<mi1->count; i++) {
    boMove_mate( bo, &(mi1->te[i]), bo->next );
    king_xy = bo->code_xy[bo->next + 1][0];
    if (king_xy != 0 && boGetToBoardCount(king_xy, (bo->next == 0))) {
      /* 逆王手になっている */
      boBack_mate( bo, bo->next );
      continue;
    }

    mi2 = newMOVEINFO();
    /* mi2 = &(g_mi[(gn << 1) + 2]); */

    uke(bo, mi2, (bo->next == 0));
    
    if ( mi2->count == 0 && mi1->te[i].uti == FU ) {
      /* 打ち歩詰め。 */
      freeMOVEINFO( mi2 );
      boBack_mate( bo, bo->next );
      continue;
    }
    
    tree1 = newTREE();
    tree1->te = mi1->te[i];
    addTREEChild( tree, tree1 );
    
    if ( mi2->count == 0 ) {
      /* 詰み */

      freeMOVEINFO( mi1 );
      freeMOVEINFO( mi2 );

      boBack_mate( bo, bo->next == 0);

#ifdef USE_HASH
      hsPutBOARD( bo, CHECK_MATE, bo->mi.count, 0);
#endif /* USE_HASH */
      return CHECK_MATE;
    }
    
    cn -= mi2->count;

    if (cn <= 0) {
      /* 不詰み */
      cn += mi2->count;
      
      freeMOVEINFO( mi2 );
      boBack_mate( bo, bo->next );
      removeTREEChild( tree1 );
      
      continue;
    }

    count = 0;
    for (j=0; j<mi2->count; j++) {
      boMove_mate( bo, &(mi2->te[j]), (bo->next == 0));
      tree2 = newTREE();
      tree2->te = mi2->te[j];
      addTREEChild( tree1, tree2 );

      if ( cmate( bo, tree2, cn) == NOT_CHECK_MATE ) {
	/* 詰まなかった */
	/* この王手(mi1->te[i])では詰まない */
	boBack_mate( bo, bo->next );
#ifdef USE_HASH_NO
	hsPutBOARD( bo, NOT_CHECK_MATE, bo->mi.count - gn, 0 );
#endif /* USE_HASH */
	goto NEXT;
      } else {
	/* 詰み */
	count++;
      }
      boBack_mate( bo, bo->next );
    }
    if ( mi2->count == count ) {
      /* 詰み */

      freeMOVEINFO( mi1 );
      freeMOVEINFO( mi2 );

      boBack_mate( bo, bo->next );

#ifdef USE_HASH    
      hsPutBOARD( bo, CHECK_MATE, bo->mi.count, 0 );
#endif /* USE_HASH */
      return CHECK_MATE;
    }

    NEXT:;

    cn += mi2->count;
    
    freeMOVEINFO( mi2 );
    removeTREEChild( tree1 );
    boBack_mate( bo, bo->next );
  }
  
  freeMOVEINFO( mi1 );

  return NOT_CHECK_MATE;
}

/**
 * parent に子の盤面のポインタ child を加える
 * @param parent 親のポインタ
 * @param child 子のポインタ
 */
void addTREEChild(TREE *parent, TREE *child) {
#ifdef DEBUG
  assert( parent != NULL );
  assert( child != NULL );
#endif /* DEBUG */

  if ( parent->child == NULL ) {
    parent->child = child;
    parent->child->parent = parent;
    parent->child->child = NULL;
    parent->child->brother = NULL;
  } else {
    TREE *tmp;
    tmp = parent->child;
    parent->child = child;
    parent->child->parent = parent;
    parent->child->child = NULL;
    parent->child->brother = tmp;
  }
}

/**
 * tree の兄弟のツリーをたどって target の一つ上の兄弟を探してそれを
 * 返す。
 * @param tree 検索対象のツリーへのポインタ
 * @param target 兄弟のツリーへのポインタ
 */
TREE *findPointerMe(TREE *tree, TREE *target) {
  TREE *tmp;
#ifdef DEBUG
  assert ( tree != NULL );
  assert ( target != NULL );
#endif /* DEBUG */

  tmp = tree;
  
  while ( 1 ) {
    if ( tmp->brother == target )
      break;
    tmp = tmp->brother;
#ifdef DEBUG
    assert ( tmp != NULL );
#endif /* DEBUG */
  }
  
  return tmp;
}

/**
 * 不詰が確定した手をツリーから削除する。
 * @param tree ツリー
 */
void removeTREEChild(TREE *tree) {
#ifdef DEBUG
  assert ( tree != NULL );
  assert ( tree->parent != NULL );
#endif /* DEBUG */

  if ( tree->child != NULL )
    freeTREE( tree->child );
  
  if ( tree->brother != NULL ) {
    if ( tree->parent->child == tree ) {
      tree->parent->child = tree->brother;
    } else {
      TREE *tmp;
      tmp = findPointerMe( tree->parent->child, tree );
      tmp->brother = tree->brother;
    }
  }
  
  tree->parent->child = NULL;

  free(tree);
}
