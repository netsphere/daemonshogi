/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * si.h
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/si.h,v $
 * $Id: si.h,v 1.1.1.1 2005/12/09 09:03:10 tokita Exp $
 */

#ifndef _SI_H_
#define _SI_H_

#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef VERSION
#define VERSION "0.0.2"
#endif /* VERSION */

#ifndef REVISION
#define REVISION "unknown"
#endif /* VERSION */

/** 検索中にハッシュを使う場合はこのフラグを定義する */
#define USE_HASH 1
/** 検索中にGUIのシグナルを処理する場合はこのフラグを定義する */
#define USE_GUI 1
/** シリアルポートを使って対局する場合はこのフラグを定義する */
#define USE_SERIAL 1
/** このフラグが定義されている場合は 歩、角、飛車 の成らずは検索しない */
/* #define NOREC_NARAZU 1 */

#ifdef USE_SERIAL
#include "comaccess.h"
#endif /* USE_SERIAL */

#define WALL (0x20)

/** デフォルトの制限時間 */
#define DEFAULT_LIMIT_TIME 1500

typedef enum {
  SENTE = 0,
  GOTE  = 1
} TEBAN;

/** 駒種類の定義 */
typedef enum {
  RYU     = 0x0F,
  UMA     = 0x0E,
  NARIGIN = 0x0C,
  NARIKEI = 0x0B,
  NARIKYO = 0x0A,
  TOKIN   = 0x09,
  OH      = 0x08,
  HISYA   = 0x07,
  KAKU    = 0x06,
  KIN     = 0x05,
  GIN     = 0x04,
  KEI     = 0x03,
  KYO     = 0x02,
  FU      = 0x01
} KOMATYPE;

/** 移動先情報の最大引数 */
#define toBoardCountMax 32

#ifndef DEBUG
/* マクロ関数を使うか？のフラグ。定義されていたらマクロを使う */
#define USE_MISC_MACRO
#endif /* DEBUG */

#ifdef _MSC_VER
typedef __int64 si_int64;
#else
typedef long long si_int64;
#endif /* WIN32 */

/** mate() が返すステータス */
typedef enum {
  /** 詰みあり */
  CHECK_MATE = 1,
  /** 不詰み */
  NOT_CHECK_MATE = 0,
  /** 不明(ハッシュからデータを取り出すときだけ使う) */
  UNKNOWN_CHECK_MATE = -1,
} MATESTATUS;

/** 手合 */
typedef enum {
    /** 平手 */
  HIRATE,
  NIMAIOTI,
  YONMAIOTI,
  TEAI_ETC,
} TEAI;

/** si_input_next_* が返すゲームのステータス */
typedef enum {
  /** 通常の手 */
  SI_NORMAL = 0,
  /** 手番のプレイヤーが勝ち(相手が反則手を指した) */
  SI_WIN = 1, 
  /** 手番のプレイヤーが投了 */
  SI_TORYO = 2,
  /** 手番のプレイヤーが待った */
  SI_MATTA = 3,
  /** 手番のプレイヤーがゲームを中断 */
  SI_CHUDAN = 4,
  /** 手番のプレイヤーが中断を受信 */
  SI_CHUDAN_RECV = 5,
  /** 手番のプレイヤーが入玉で勝ちを宣言 */
  SI_KACHI = 6,
  /** 手番のプレイヤーが入玉で引き分けを宣言 */
  SI_HIKIWAKE = 7,
  /** 送信されてきた手が不正な手だった */
  SI_NGDATA = 8,
  /** 手番のプレイヤーが時間切れ負け */
  SI_TIMEOUT = 9
} INPUTSTATUS;

typedef enum {
  /** 人間による入力 */
  SI_HUMAN = 0,
  /** プログラムレベル１による入力 */
  SI_COMPUTER_1 = 1,
  /** プログラムレベル２による入力 */
  SI_COMPUTER_2 = 2,
  /** プログラムレベル３による入力 */
  SI_COMPUTER_3 = 3,
  /** プログラムレベル４による入力 */
  SI_COMPUTER_4 = 4,
  /** プログラムレベル５による入力 */
  SI_COMPUTER_5 = 5,
  /** シリアルポートからの入力１ */
  SI_SERIALPORT_1 = 6,
  /** シリアルポートからの入力２ */
  SI_SERIALPORT_2 = 7,
  /** ネットワークからの入力１ */
  SI_NETWORK_1 = 8,
  /** ネットワークからの入力２ */
  SI_NETWORK_2 = 9,
  /** GUIを使った人間による入力 */
  SI_GUI_HUMAN = 10
} PLAYERTYPE;

/* 駒の評価点の基礎値 */
enum {
  /* 歩が上に行く程可算する点数 */
  FU_POSITION_POINT = 10,
  /* 自分の王への距離の評価点の率。減らす程点数が高くなる */
  MY_OH_DISTANCE_POINT = 200,
  /* 相手の王への距離の評価点の率。減らす程点数が高くなる */
  ONES_OH_DISTANCE_POINT = 100,
  /** 
   * 王の周りの安全度の点数。
   * 相手の効きが3ある場合は 3 × OH_SAFE_POINT を盤面評価から差し引く。
   */
  OH_SAFE_POINT = 100,
  /* 駒の動きの評価。 (駒の動ける数 << NUM_OF_MOVE_POINT) を加算する。 */
  /* NUM_OF_MOVE_POINT = 6, */
  NUM_OF_MOVE_POINT = 2,
  /* 王と飛車が近いときに減点するポイント */
  OH_HISYA_POINT = 10,
};

/** 盤上に出ている駒以外の駒を格納する構造体 */
typedef struct {
  /** 駒の数を格納する */
  int count[9];
  /** 各駒の駒コードを格納する */
  int number[9][18];
} PBOX;

/** １手を表す構造体 */
typedef struct {
  /** 移動先の場所 */
  int to;
  /** 移動元の場所 */
  int fm;
  /** 成りならば 1 、それ以外は 0 をセットする */
  int nari;
  /** 駒打ちならば打ち込む駒種類 、それ以外は 0 をセットする */
  int uti;
} TE;

#define MOVEINFOMAX 500

/** 指し手をまとめて格納する構造体 */
typedef struct {
  /** 指し手 */
  TE te[MOVEINFOMAX];
  /** 指し手の数 */
  int count;
} MOVEINFO;

#define MOVEINFO2MAX 2000

/** 指し手をまとめて格納する構造体。長手数用 */
typedef struct {
  /** 指し手 */
  TE te[MOVEINFO2MAX];
  /** 指し手の数 */
  int count;
} MOVEINFO2;

/** 盤面の状態を表す構造体 */
typedef struct {
  /** 盤面の駒種類 */
  int board[256];
  /** 盤面の駒コード */
  int noBoard[256];
  /** 駒コード毎の駒種類と位置 */
  int code_xy[41][2];
  /** 持駒 */
  int piece[2][8];
  /** 次の手番 */
  int next;
  /** 駒箱 */
  PBOX pb;
  /** ハッシュ値 */
  si_int64 key;
  /** 局面の評価点数 */
  int point[2];
  /** 手順を記録しておく MOVEINFO2 */
  MOVEINFO2 mi;
  /** 取った駒があったら覚えておく */
  int tori[MOVEINFO2MAX];
  /** 取った駒のカウント */
  int tori_count;
  /** 消費時間の合計 */
  time_t total_time[2];
  /** 制限時間 */
  time_t limit_time[2];
  /** 思考開始前の時刻 */
  time_t before_think_time;
  /** １手づつの消費時間 */
  time_t used_time[MOVEINFO2MAX];
} BOARD;

/** 指し手情報をツリー状に格納するための構造体 */
struct tagTREE {
  /** 直前の一手 */
  TE te;
  /** 親へのポインタ */
  struct tagTREE *parent;
  /** 兄弟へのポインタ */
  struct tagTREE *brother;
  /** 子へのポインタ */
  struct tagTREE *child;
  /** 盤面の点数 */
  int point;
};

typedef struct tagTREE TREE;

/** 盤面を記録するハッシュマップ */
typedef struct {
  /** 盤面データの縦の合計 */
  /* int vboard[3]; */
  /** ハッシュキー */
  si_int64 key;
  /** 局面の評価値 */
  int point;
  /** 詰将棋のステータス */
  int mate_stat;
  /** 持駒 */
  int piece[ 2 ];
  /** 記録したときの手数 */
  int count;
  /** 使用済みか？のフラグ */
  int flguse;
} HASH;

/** ゲームの進行に必要な情報を集めた構造体 */
typedef struct {
  /** 手合 */
  TEAI teai;
  /** player number */
  PLAYERTYPE player_number[2];
  /** 入力を受ける関数へのポインタ */
  INPUTSTATUS (*si_input_next[2])(BOARD *bo, TE *te);
  /** コンピュータの思考レベル */
  int computer_level[2];
#ifdef USE_SERIAL
  /** シリアルポートを使い通信対局するのための構造体 */
  COMACCESS ca[2];
#endif /* USE_SERIAL */
  /** 1 の場合、時間切れは負けとする */
  int flg_run_out_to_lost;

  /**
   * 中断等が入ったときに入力待ちループから抜けるためのフラグ。
   * 0 ならば通常通り。
   * SI_CHUDAN ならば中断が入った。SI_TORYO ならば投了。
   * 中断または投了が入った場合、入力待ちループからすぐ抜ける。
   */
  int game_status;
  
  /**
   * 中断フラグへのポインタ
   */
  int *chudan;
} GAME;

/* ------------------- function prototype ------------------- */

/* BOARD */
BOARD *newBOARD(void);
BOARD *newBOARDwithInit(void);
void freeBOARD(BOARD *bo);
void boInit(BOARD * bo);
void boPlayInit(BOARD *bo);
void boSetHirate(BOARD * bo);

void boSetPiece(BOARD *bo, int xy, int p);
int boGetPiece(BOARD *bo, int xy);
int boGetNoBoard(BOARD *bo, int xy);
int boGetToBoardCount(int xy, int next);
MOVEINFO * boGetCodeTo(int no);

void boSetNumber(BOARD * bo);
void boSetToBOARD(BOARD * bo);
void boSetToPBOX(BOARD *bo, int x, int y);
BOARD *boCopy(BOARD * bo);
void boCopy2(BOARD *src, BOARD *dest);
void boPushPiece(BOARD *bo, int p);
void boPushPiece2(BOARD *bo, int p, int next);
void boSetPiece3(BOARD *bo, int p, int num, int next);
void boPopToBoard(BOARD *bo, int xy);
void boPushToBoard(BOARD *bo, int xy);
int boMove(BOARD *bo, TE *te, int next);
void boBack(BOARD *bo, int next);
void boMove_mate(BOARD *bo, TE *te, int next);
void boBack_mate(BOARD *bo, int next);
void boPopRemotePiece(BOARD *bo, int xy, MOVEINFO *mi);
int boCmp(BOARD *bo1, BOARD *bo2);
void boSetTsumeShogiPiece(BOARD *bo);
int boCheckTE(BOARD *bo, TE *te);
int boCheckTEOhte(BOARD *bo, TE *te);
void boToggleNext(BOARD *bo);
int boCanNari(BOARD *bo, TE *te);
void boSetNowTime(BOARD *bo);
void boSetUsedTime(BOARD *bo);
int boCheckJustGame(BOARD *bo);
int boCheckJustMate(BOARD *bo);

/* MOVEINFO */
MOVEINFO *newMOVEINFO(void);
void freeMOVEINFO(MOVEINFO *mi);
void miAppend(MOVEINFO * mi, TE * te);
void miAppendNari(MOVEINFO * mi, TE * te, int next);
void miAdd(MOVEINFO * mi, TE * te);
void miAddWithNari(MOVEINFO * mi, TE * te, int next);
void miAddWithNari2(MOVEINFO * mi, TE te, int p, int next);
void miAddWithNariSente(MOVEINFO * mi, TE * te);
void miAddWithNariGote(MOVEINFO * mi, TE * te);
TE *miGet(MOVEINFO *mi, int index);
void miRemoveTE(MOVEINFO *mi, TE *te );
void miRemoveTE2(MOVEINFO *mi, int num );
void miRemoveDuplicationTE(MOVEINFO *mi);
void miCopy(MOVEINFO *dest, MOVEINFO *src);

/* MOVEINFO */
MOVEINFO2 *newMOVEINFO2(void);
void freeMOVEINFO2(MOVEINFO2 *mi);
void mi2Add(MOVEINFO2 * mi, TE * te);
void mi2Copy(MOVEINFO2 *dest, MOVEINFO2 *src);

/* TE */
TE *newTE(int x, int y, int v, int w, int nari);
#ifndef USE_MISC_MACRO
int teCmpTE( TE *te1, TE * te2 );
#endif /* USE_MISC_MACRO */
void teSetTE(TE *te, int fm, int to, int nari, int uti);
int teCSAimoto(TE *te);
int teCSAisaki(TE *te);
int teCSAinaru(TE *te);
void teCSASet(TE *te, int imoto, int isaki);

/* moveto */
void moveto(BOARD * bo, int xy, MOVEINFO *mi);
void moveto_add(BOARD * bo, int xy, MOVEINFO *mi);
void movetoKiki(BOARD * bo, int xy, MOVEINFO *mi);
void moveto2(BOARD *bo, MOVEINFO *mi, int xy, int next);
void moveto2_add(BOARD *bo, MOVEINFO *mi, int xy, int next);

/* pbox */
PBOX *newPBOX(void);
void freePBOX(PBOX *pb);
void pbInit(PBOX * pb);
void pbPush(PBOX * pb, int p);
int pbPop(PBOX * pb, int p);
int pbCheck(PBOX * pb);

/* ohte */
void ohte(BOARD *bo, MOVEINFO *mi);

void normalOhte(BOARD *bo, MOVEINFO *mi, int king_xy);
void remoteOhte(BOARD *bo, MOVEINFO *mi, int king_xy);
void keiOhte(BOARD *bo, MOVEINFO *mi, int king_xy);
void utiOhte(BOARD *bo, MOVEINFO *mi, int king_xy);
void UtiOhte2(BOARD *bo, MOVEINFO *mi, int king_xy);
void akiOhte(BOARD *bo, MOVEINFO *mi, int king_xy);

/* uke */
void uke(BOARD *bo, MOVEINFO *mi, int next);
int countKiki(BOARD *bo, int king_xy, int *rxy, int next);
int ukeRemoteKiki(BOARD *bo, int xy, int next);
int ukeAkiohte(BOARD *bo, TE *te, int king_xy, int next);

/* mate */
MATESTATUS mate(BOARD *bo, TREE *tree, int gn);
MATESTATUS cmate(BOARD *bo, TREE *tree, int gn);

/* tree */
TREE *newTREE(void);
void initTREE(TREE *tree);
void freeTREE(TREE *tree);
void trSetMoveinfo(TREE *tree, MOVEINFO *mi);

/* hash */
void newHASH(void);
void initHASH(void);
void freeHASH(void);
si_int64 hsGetHashKey(BOARD *bo);
void hsPutBOARD(BOARD *bo, int p, int count, int r_count);
int hsGetBOARD(BOARD *bo, int *p, int count, int r_count);
void hsPutBOARDMinMax(BOARD *bo, int point, int count, int r_count);
int hsGetBOARDMinMax(BOARD *bo, int *point, int count, int r_count);
void add_hash_value(BOARD *bo, int xy);
void sub_hash_value(BOARD *bo, int xy);
void hsSetPiece(HASH *hs, BOARD *bo);
int hsSuperiorBOARD(HASH *hs, BOARD *bo);
int hsCmpPiece(HASH *hs, BOARD *bo, int next);

/* grade */
int grade(BOARD *bo, int next);
void add_grade(BOARD *bo, int xy, int next );
void sub_grade(BOARD *bo, int xy, int next );
void add_piece_grade(BOARD *bo, int piece, int next );
void sub_piece_grade(BOARD *bo, int piece, int next );
int oh_safe_grade(BOARD *bo, int king_xy, int next);
void add_oh_safe_grade(BOARD *bo, int next);
void sub_oh_safe_grade(BOARD *bo, int next);
void add_moving_grade(BOARD *bo, int xy, int next);
void sub_moving_grade(BOARD *bo, int xy, int next);

/* search */
void search_011(BOARD *bo, MOVEINFO *mi);
void search_012(BOARD *bo, MOVEINFO *mi);
void search_013(BOARD *bo, MOVEINFO *mi);
void search_014(BOARD *bo, MOVEINFO *mi);
void search_022(BOARD *bo, MOVEINFO *mi);
void search_023(BOARD *bo, MOVEINFO *mi);
void search_024a(BOARD *bo, MOVEINFO *mi);
void search_027(BOARD *bo, MOVEINFO *mi);
void search_031a(BOARD *bo, MOVEINFO *mi);
void search_031b(BOARD *bo, MOVEINFO *mi);
void search_041(BOARD *bo, MOVEINFO *mi);
void search_044(BOARD *bo, MOVEINFO *mi);
void search_046(BOARD *bo, MOVEINFO *mi);
void search_047(BOARD *bo, MOVEINFO *mi);
void search_048(BOARD *bo, MOVEINFO *mi);
void search_049(BOARD *bo, MOVEINFO *mi);
void search_053(BOARD *bo, MOVEINFO *mi);
void search_068(BOARD *bo, MOVEINFO *mi);
void search_pressure(BOARD *bo, MOVEINFO *mi, int to_xy, int next);
void search_pressure_uti(BOARD *bo, MOVEINFO *mi, int to_xy, int next);


/* play */
void play(BOARD *bo, GAME *gm);
void play_move(BOARD *bo, TE *te);
void getTE2SerialString(char *buf, BOARD *bo, TE *te);
void play_lost(BOARD *bo, GAME *gm);
void play_toryo(BOARD *bo, GAME *gm);
void play_win(BOARD *bo, GAME *gm);
void play_chudan(BOARD *bo, GAME *gm);
void play_chudan_recv(BOARD *bo, GAME *gm);
void play_ngdata(BOARD *bo, GAME *gm);
void putHelp(void);

/* think */
void think(BOARD *bo, MOVEINFO *mi, int depth);

/* minmax */
INPUTSTATUS minmax(BOARD *bo);
int f_min(BOARD *bo, int depth, int alpha, int beta);
int f_max(BOARD *bo, int depth, int alpha, int beta);

/* GAME */
GAME *newGAME(void);
void initGAME(GAME *game);
void freeGAME(GAME *game);
INPUTSTATUS (*gmGetInputNextPlayer(GAME *gm, int next))(BOARD *, TE *);
char *gmGetDeviceName(GAME *gm, int next);
int gmGetComputerLevel(GAME *gm, int next);
void gmSetInputFunc(GAME *gm, int next, PLAYERTYPE p);
void gmSetDeviceName(GAME *gm, int next, char *s);
void gmSetGameStatus(GAME *gm, int status);
INPUTSTATUS gmGetGameStatus(GAME* gm);
void gmSetChudan(GAME *gm, int *chudan);
int *gmGetChudan(GAME *gm);
int gmGetChudanFlg(GAME *gm);

/* misc */

void si_abort(char *s);
void time2string(char *buf, time_t t);

#ifdef USE_MISC_MACRO
/* macro */
#define isGote(b,x,y) (((b)->board[ ((y)<<4) + (x) ])&0x10)
/* #define boPushPiece(bo, p) ((bo)->piece[(bo)->next][(p)&0x07]++) */
#define boPushPiece2(bo, p, n) ((bo)->piece[(n)][(p)&0x07]++)
#define miAdd(m, t) ((m)->te[(m)->count++]=*(t))
#define mi2Add(m, t) ((m)->te[(m)->count++]=*(t))
#define miGet(m, i) (&((m)->te[(i)]))
#define getTeban(p) (((p)&0x10)==0x10)
#define teCmpTE(t, u) ((t)->fm != (u)->fm || (t)->to != (u)->to || \
                       (t)->nari != (u)->nari || (t)->uti != (u)->uti )
#define boSetPiece(bo,xy,p) ((bo)->board[(xy)]=(p))
#define boGetPiece(bo,xy) ((bo)->board[(xy)])
#define boGetNoBoard(bo,xy) (bo->noBoard[(xy)])
#define boToggleNext(bo) ((bo)->next = ((bo)->next == 0))
extern int toBoardCount3[2][256];
#define boGetToBoardCount(xy, n) (toBoardCount3[(n)][(xy)])
extern MOVEINFO code_to[41];
#define boGetCodeTo(n) (&(code_to[(n)]))
#else /* USE_MISC_MACRO */
int piece(BOARD *b, int x, int y);
int isGote(BOARD *b, int x, int y);
int getTeban(int p);
#endif /* USE_MISC_MACRO */
int notSentePiece(int p);
int notGotePiece(int p);
int checkFuSente(BOARD *bo, int x);
int checkFuGote(BOARD *bo, int x);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _SI_H_ */
