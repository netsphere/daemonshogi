#!/usr/bin/perl
#
# $Id: mkhashval.pl,v 1.2 2005/12/09 09:55:45 tokita Exp $
# $Source: /cvsroot/daemonshogi/daemonshogi/src/si/mkhashval.pl,v $

&main;

sub main {
  &putHeader;

  srand( 54321 );

  print "const si_int64 hashval[32][16 * 11] = {\n";
    
  for ($i=0; $i<32; $i++) {
    print "\t{\n";
    for ($j=0; $j<(16 * 11 / 4); $j++) {
      printf("%sLL, %sLL, %sLL, %sLL,\n", &mk64bitrand, &mk64bitrand, &mk64bitrand, &mk64bitrand);	    
    }
    print "\t},\n";
  }
    
  print "};\n";
}

sub putHeader {
  print <<_HEADER_
/**
 * randam numbers table
 * \$Id\$
 * \$Source\$
 */

_HEADER_
}

sub mk64bitrand {
  return sprintf("0x%04X%04X%04X%04X", rand(0x10000), rand(0x10000),
        rand(0x10000), rand(0x10000) );
}

