/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/**
 * comaccess.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/comaccess.c,v $
 * $Id: comaccess.c,v 1.2 2005/12/12 02:52:11 tokita Exp $
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include "comaccess.h"
#include "si.h"
#include "ui.h"

#ifdef USE_SERIAL

#ifndef VSWTC
#define VSWTC 7
#endif /* VSWTC */

ssize_t wait_read(int fd, void *buf, size_t size);

/**
 * 新しくメモリを確保してCOMACCESSのポインタを返す。
 * @return COMACCESSEのポインタ
 */
COMACCESS *newCOMACCESS(void) {
  COMACCESS *ca;

  ca = (COMACCESS *) malloc(sizeof(COMACCESS));
  if (ca == NULL) {
    printf("No enough memory. In tree.c/newTREE()");
    abort();
  }

  initCOMACCESS(ca);

  return ca;
}

/**
 * ca のメモリを解放する。
 * @param bo 対象のCOMACCESS
 */
void freeCOMACCESS(COMACCESS *ca) {
#ifdef DEBUG
  assert( ca != NULL );
#endif /* DEBUG */
   
  free(ca); 
}

/**
 * ca を初期化する。
 * @param ca 対象のCOMACCESS
 */
void initCOMACCESS(COMACCESS *ca) {
   static const char s[] = "/dev/cua0";
   
   strncpy(ca->DEVICENAME, s, strlen(s));
   ca->DEVICENAME[strlen(s)] = '\0';
}

/**
 * デバイス名をセットする。
 * @param ca 対象のCOMACCESS
 * @param str デバイス名
 */
void caSetDEVICENAME(COMACCESS *ca, char *str) {
   strncpy(ca->DEVICENAME, str, strlen(str));
   ca->DEVICENAME[strlen(str)] = '\0';
}

/**
 * 通信対局をはじめれるように設定する。
 * next に 0 をセットした場合は先手用に設定する。0 以外の場合は
 * 後手用に設定する。
 * @param ca 対象の COMACCESS
 * @param next 0 の場合、先手用。0 以外の場合は後手用。
 */
void caStart(COMACCESS *ca) {
  ca->fd = open(ca->DEVICENAME, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (ca->fd < 0) {perror(ca->DEVICENAME); exit(-1); }
   
  /* 現在のシリアルポートの設定を待避させる*/
  tcgetattr(ca->fd, &(ca->oldtio)); 
  memset(&(ca->newtio), 0, sizeof(ca->newtio));
   
/* 
  BAUDRATE: ボーレートの設定．cfsetispeed と cfsetospeed も使用できる．
  CRTSCTS : 出力のハードウェアフロー制御 (必要な結線が全てされているケー
            ブルを使う場合のみ．Serial-HOWTO の7章を参照のこと)
  CS8     : 8n1 (8 ビット，ノンパリティ，ストップビット 1)
  CLOCAL  : ローカル接続，モデム制御なし
  CREAD   : 受信文字(receiving characters)を有効にする．
*/
  ca->newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;

  /* IGNPAR  : ignore bytes with parity errors */
  /* (パリティエラーのデータは無視する )*/
  ca->newtio.c_iflag = IGNPAR;

  /* Raw output (Rawモードでの出力)*/
  ca->newtio.c_oflag = 0;
  
  /* Set input mode (non-canonical,no echo,....) */
  ca->newtio.c_lflag = 0;
  
  ca->newtio.c_cc[VINTR]    = 0x03;     /* Ctrl-c */
  ca->newtio.c_cc[VQUIT]    = 0x1C;     /* Ctrl-\ */
  ca->newtio.c_cc[VERASE]   = 0x7F;     /* del */
  ca->newtio.c_cc[VKILL]    = 0x15;     /* @ */
  ca->newtio.c_cc[VEOF]     = 0x04;     /* Ctrl-d */
  ca->newtio.c_cc[VTIME]    = 0x00;     /* キャラクタ間タイマを使わない */
  ca->newtio.c_cc[VMIN]     = 0x01;     /* 1文字来るまで，読み込みをブロックする */
  ca->newtio.c_cc[VSWTC]    = 0x00;     /* '\0' */
  ca->newtio.c_cc[VSTART]   = 0x11;     /* Ctrl-q */ 
  ca->newtio.c_cc[VSTOP]    = 0x13;     /* Ctrl-s */
  ca->newtio.c_cc[VSUSP]    = 0x1A;     /* Ctrl-z */
  ca->newtio.c_cc[VEOL]     = 0x00;     /* '\0' */
  ca->newtio.c_cc[VREPRINT] = 0x12;     /* Ctrl-r */
  ca->newtio.c_cc[VDISCARD] = 0x0F;     /* Ctrl-u */
  ca->newtio.c_cc[VWERASE]  = 0x17;     /* Ctrl-w */
  ca->newtio.c_cc[VLNEXT]   = 0x16;     /* Ctrl-v */
  ca->newtio.c_cc[VEOL2]    = 0x00;     /* '\0' */

  tcflush(ca->fd, TCIFLUSH);
  tcsetattr(ca->fd,TCSANOW,&(ca->newtio));
}

/**
 * ca を閉じる。
 * @param 対象のCOMACCESS
 */
void caClose(COMACCESS *ca) {
  /* ポートの設定をプログラム開始時のものに戻す */
  tcsetattr(ca->fd,TCSANOW,&(ca->oldtio));
   
  close(ca->fd);
}

/**
 * 入力待ち付き read() 。
 * processing_pending() を実行して
 * ペンディングされていたイベントを処理する。
 * 中断が入った場合 -2 を返す。
 * @param fd ファイル・ディスクリプター
 * @param buf 文字を格納するバッファ
 * @param size 入力を行うバイト数
 * @return 入力成功の場合は read() の返す値そのままを返す。
 *         入力待ちでゲームの中断が入った場合は -2 を返す。
 */
ssize_t wait_read(int fd, void *buf, size_t size) {
  int ret;

  while ( 1 ) {
    ret = read(fd, buf, size);
    if ( 0 < ret ) {
      return ret;
    } else {
      /* 入力がなかった */
      extern GAME g_game;
      if ( g_game.game_status == 1 ) {
	/* 中断が入った。ループから抜ける。 */
	return -2;
      }
      /* GUI の処理待ちになっているイベントを処理する */
      processing_pending();
    }
  }
}

/**
 * 受信する。受信した文字列は buf に格納する。buf は BUFSIZ 以上の大きさの
 * メモリが確保されていることを前提にしている。
 * 文字列は LF(16進の0A) を受信するまで読み続ける。
 * @param ca 対象のCOMACCESS
 * @param buf 受信した文字列を格納する
 * @return 0 の場合は正常。1 の場合は中断等が入って受信しないで戻ってきた。
 */
int caAccept(COMACCESS *ca, char *buf) {
  int i;
   
  for ( i=0; i<BUFSIZ; i++ ) {
    if ( wait_read(ca->fd, buf+i, 1) == -2 ) {
      /* 中断が入って戻ってきた */
      return 1;
    }
    if ( buf[i] == 0x0D ) {
      i--;
      continue;
    }
    if ( buf[i] == 0x0A )
      break;
  }
  buf[i] = '\0';
  
  return 0;
}

/**
 * 文字列を送信する。
 * @param ca 対象のCOMACCESS
 * @param shr 送信する文字列
 */
int caSend(COMACCESS *ca, char *str) {
  return write(ca->fd, str, strlen(str));;
}

#endif /* USE_SERIAL */
