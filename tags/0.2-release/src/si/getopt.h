/**
 * getopt.h
 * $Id: getopt.h,v 1.1.1.1 2005/12/09 09:03:08 tokita Exp $
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/getopt.h,v $
 */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef UNIX			/* avoid conflict with stdlib.h */
int si_getopt(int argc, char **argv, char *opts);
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */
