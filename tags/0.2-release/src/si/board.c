/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * board.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/board.c,v $
 * $Id: board.c,v 1.1.1.1 2005/12/09 09:03:07 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include "si.h"
#include "ui.h"

/* -------------------------------------------------------- */
/* BOARDの初期化関係関数 */
/* -------------------------------------------------------- */

/**
 * 新しくメモリを確保してBOARDのポインタを返す。
 * @return BOARDのポインタ
 */
BOARD *newBOARD(void) {
  BOARD *b;
   extern long count_new_board;

   count_new_board++;

   b = (BOARD *)malloc(sizeof(BOARD));
   if (b == NULL) {
     si_abort("No enough memory. In board.c#newBOARD()");
   }

   return b;
}

/**
 * 新しくメモリを確保してBOARDのポインタを返す。初期化付き。
 * @return BOARDのポインタ
 */
BOARD *newBOARDwithInit(void) {
  BOARD *b = newBOARD();
  
  boInit(b);
  
  return b;
}

/**
 * bo のメモリを解放する。
 * @param bo 対象のBOARD
 */
void freeBOARD(BOARD *bo) {
#ifdef DEBUG
  assert ( bo != NULL );
#endif /* DEBUG */
  
  free(bo);
}

/** 
 * 盤面を初期化する。
 * bo 対象のBOARD
 */
void boInit(BOARD * bo) {
  int i, j;
  extern int toBoard3[2][256][toBoardCountMax];
  extern int toBoardCount3[2][256];
  extern MOVEINFO code_to[41];
  
#ifdef DEBUG
  assert( bo != NULL );
#endif /* DEBUG */
  
  for (i = 0; i < 256; i++) {
    /* 盤の外に WALL(0x20) で壁を作る */
    boSetPiece( bo, i, WALL );
    bo->noBoard[i] = 0;
    toBoardCount3[SENTE][i] = 0;
    toBoardCount3[GOTE][i] = 0;
    for (j = 0; j < toBoardCountMax; j++) {
      toBoard3[SENTE][i][j] = 0;
      toBoard3[GOTE][i][j] = 0;
    }
  }
  for (i = 0; i < 41; i++) {
    bo->code_xy[i][0] = 0;
    bo->code_xy[i][1] = 0;
    code_to[i].count = 0;
  }
  for (i = 0; i <= 10; i++)
    for (j = 0; j <= 10; j++)
      boSetPiece( bo, (i << 4) + j, WALL );
  for (i = 1; i <= 9; i++)
    for (j = 1; j <= 9; j++)
      /* 中は 0 で初期化 */
      boSetPiece( bo, (i << 4) + j, 0 );
  
  bo->point[0] = 0;
  bo->point[1] = 0;
  
  pbInit(&(bo->pb));
  
  bo->mi.count = 0;
  for (i = 0; i<MOVEINFOMAX; i++) {
    bo->tori[ i ] = 0;
  }
  bo->tori_count = 0;
  
  bo->total_time[SENTE] = 0;
  bo->total_time[GOTE] = 0;
  bo->limit_time[SENTE] = DEFAULT_LIMIT_TIME;
  bo->limit_time[GOTE] = DEFAULT_LIMIT_TIME;
  bo->before_think_time = 0;
  for (i = 0; i<MOVEINFO2MAX; i++) {
    bo->used_time[i] = 0;
  }
}

/**
 * 新規対局用に盤面と時間設定を初期化する。
 * @param bo 対象のBOARD
 */
void boPlayInit(BOARD *bo) {
  pbInit( &(bo->pb) );
  boSetNumber(bo);
  boSetToBOARD(bo);
  
  /* 消費時間に 0 をセットする */
  bo->total_time[0] = 0;
  bo->total_time[1] = 0;
  
  /* 持ち時間に1500秒(25分)をセットする */
  bo->limit_time[0] = 1500;
  bo->limit_time[1] = 1500;
#ifdef USE_HASH
  bo->key = hsGetHashKey(bo);
#endif /* USE_HASH */
  
  bo->mi.count = 0;
  bo->tori_count = 0;
}

/**
 * bo に平手の配置をセットする。
 * @param bo 対象のBOARD
 */
void boSetHirate(BOARD * bo) {
  /* 平手の配置パターン */
  static int hirate[] = {
    0x12, 0x13, 0x14, 0x15, 0x18, 0x15, 0x14, 0x13, 0x12,
    0x00, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x17, 0x00,
    0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00,
    0x02, 0x03, 0x04, 0x05, 0x08, 0x05, 0x04, 0x03, 0x02,
  };
  int i, j;
  
#ifdef DEBUG
  assert( bo != NULL );
#endif /* DEBUG */
  
  for (i = 0; i < 9; i++)
    for (j = 0; j < 9; j++)
      /* 平手の配置を盤面にセットする */
      boSetPiece( bo, (i + 1) * 16 + (j + 1), hirate[i * 9 + j] );
  for (i = 0; i < 7; i++) {
    /* 持ち駒を初期化 */
    bo->piece[0][i] = 0;
    bo->piece[1][i] = 0;
  }
}

/* -------------------------------------------------------- */
/* BOARDの初期化関係関数 ここまで*/
/* -------------------------------------------------------- */

/* -------------------------------------------------------- */
/* BOARDへのアクセスメソッド get***() set***() */
/* -------------------------------------------------------- */

/**
 * BOARD に駒種類をセットする。
 * @param bo 対象のBOARD
 */
#ifndef USE_MISC_MACRO
void boSetPiece(BOARD *bo, int xy, int p) {
#ifdef DEBUG
  assert(bo != NULL);
  if ( p == WALL ) {
    assert( 0 <= xy  && xy <= 0xFF );
  } else {
    assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
    assert( 1 <= (xy & 0xF) && (xy & 0xF) <= 9 );
  }
  assert((0 <= p && p <= 0x1F) || p == WALL);
  assert(p != 0x0D && p != 0x1D);
#endif /* DEBUG */
  bo->board[ xy ] = p;
}
#endif /* USE_MISC_MACRO */

/**
 * BOARD に駒種類をセットする。
 * @param bo 対象のBOARD
 */
#ifndef USE_MISC_MACRO
int boGetPiece(BOARD *bo, int xy) {
#ifdef DEBUG
  assert(bo != NULL);
  assert(0 <= (xy >> 4) && (xy >> 4) <= 10);
  assert(0 <= (xy & 0xF) && (xy & 0xF) <= 10);
#endif /* DEBUG */
  return bo->board[ xy ];
}
#endif /* USE_MISC_MACRO */

/**
 * xy の位置の駒コードを返す。
 * @param bo 対象のBOARD
 * @param xy 調べる位置
 * @return 駒コード
 */
#ifndef USE_MISC_MACRO
int boGetNoBoard(BOARD *bo, int xy) {
#ifdef DEBUG
  assert(bo != NULL);
  assert(0 <= (xy >> 4) && (xy >> 4) <= 10);
  assert(0 <= (xy & 0xF) && (xy & 0xF) <= 10);
#endif /* DEBUG */
  return bo->noBoard[xy];
}
#endif /* USE_MISC_MACRO */

/**
 * xy の next 手番の効きの数を返す。
 * toBoardCount3 へのアクセスメソッド。
 * @param xy 位置
 * @param next 手番
 * @return 効きの数
 */
#ifndef USE_MISC_MACRO
int boGetToBoardCount(int xy, int next) {
  extern int toBoardCount3[ 2 ][ 256 ];
#ifdef DEBUG
  assert(0 <= (xy >> 4) && (xy >> 4) <= 10);
  assert(0 <= (xy & 0xF) && (xy & 0xF) <= 10);
  assert(next == 0 || next == 1);
#endif /* DEBUG */
  return toBoardCount3[ next ][ xy ];
}
#endif /* USE_MISC_MACRO */

/**
 * 駒コード毎の移動先情報の mi へのポインタを返す。
 * @param no 駒コード
 * @return 移動先情報へのポインタ
 */
#ifndef USE_MISC_MACRO
MOVEINFO * boGetCodeTo(int no) {
  extern MOVEINFO code_to[41];
#ifdef DEBUG
  assert(1 <= no && no <= 40);
#endif /* DEBUG */
  return &code_to[no];
}
#endif /* USE_MISC_MACRO */

/* -------------------------------------------------------- */
/* BOARDへのアクセスメソッド get***() set***() ここまで */
/* -------------------------------------------------------- */

/**
 * BOARD に駒コードをセットする。
 * @param bo 対象のBOARD
 */
void boSetNumber(BOARD * bo) {
  int x, y, n;
  
#ifdef DEBUG
  assert( bo != NULL );
#endif /* DEBUG */
  
  for (y = 1; y <= 9; y++)
    for (x = 1; x <= 9; x++)
      if (boGetPiece(bo, (y << 4) + x)) {
	n = pbPop(&(bo->pb), boGetPiece(bo, (y << 4) + x));
	bo->noBoard[(y << 4) + x] = n;
	bo->code_xy[n][0] = (y << 4) + x;
	bo->code_xy[n][1] = boGetPiece(bo, (y << 4) + x);
      } else {
	bo->noBoard[(y << 4) + x] = 0;
      }
}

/**
 * BOARD.toBoard に値をセットする。
 * この関数を呼び出す前に boSetNumber() で bo.noBoard[] に駒コードをセット
 * して置く必用がある。
 * @param bo BOARD
 */
void boSetToBOARD(BOARD * bo) {
  int x, y, i;
  int fm, to, n, nn;
  int pfm, pto;
  int next;
  MOVEINFO *mi;
  extern int arr_xy[];
  extern int toBoardCount3[2][256];
  extern int toBoard3[2][256][toBoardCountMax];
  
#ifdef DEBUG
  assert( bo != NULL );
#endif /* DEBUG */
  
  pfm = 0;
  pto = 0;
  
  for (i = 1; i<=81; i++) {
    toBoardCount3[SENTE][ arr_xy[i] ] = 0;
    toBoardCount3[GOTE][ arr_xy[i] ] = 0;
  }
  
  for (y = 1; y <= 9; y++) {
    for (x = 1; x <= 9; x++) {
      if ( boGetPiece(bo, (y << 4) + x) == 0 )
	continue;
      nn = boGetNoBoard(bo, (y << 4) + x);
      mi = boGetCodeTo(nn);
      movetoKiki(bo, (y<<4) + x, mi);
      for (i = 0; i < mi->count; i++) {
	fm = mi->te[i].fm;
	to = mi->te[i].to;
	
	/* 成りの分をダブって記録しないようにする */
	if ( pfm == fm && pto == to )
	  continue;
	pfm = fm;
	pto = to;
	n = bo->noBoard[fm];
	next = getTeban(bo->board[fm]);
	toBoard3[next][to][toBoardCount3[next][to]++] = n;
      }
    }
  }
}

/**
 * 盤上の(x,y)の駒を駒箱に戻す。
 * (x,y) には 0 をセットする。
 */
void boSetToPBOX(BOARD *bo, int x, int y) {
  int c;
#ifdef DEBUG
  assert( bo != NULL );
#endif /* DEBUG */
  
  if ( boGetPiece( bo, (y << 4) + x ) == 0 ) /* 何もなければ戻る */
    return;
  c = bo->noBoard[(y<<4) + x];
  pbPush(&(bo->pb), c);
  boSetPiece(bo, (y << 4) + 4, 0);
}

/**
 * BOARDのコピーを作成する。
 */
BOARD *boCopy(BOARD *src) {
  BOARD *dest;
#ifdef DEBUG
  assert( src != NULL );
#endif /* DEBUG */
  
  dest = newBOARD();
  
  /* 構造体をコピーするのに memcpy() を使っているがちょっとあやしいかも。 */
  memcpy(dest, src, sizeof(BOARD));
  
  return dest;
}

/**
 * BOARDのコピーを作成する。
 * @param src コピー元のBOARD
 * @param dest コピー先のBOARD
 */
void boCopy2(BOARD *src, BOARD *dest) {
#ifdef DEBUG
  assert( src != NULL );
  assert( dest != NULL );
#endif /* DEBUG */
  
  /* 構造体をコピーするのに memcpy() を使っているがちょっとあやしいかも。 */
  memcpy(dest, src, sizeof(BOARD));
}

/**
 * 取った駒をBOARDにセットする。
 * (これもマクロを使った方が速くなる)
 * @param bo BOARD
 * @param p 持駒にする駒種類
 */
#ifndef USE_MISC_MACRO
void boPushPiece(BOARD *bo, int p) {
#ifdef DEBUG
  assert( bo != NULL );
  assert( 1<=p && p<=0x1F );
  assert( p != 8 && p != 0x18 );
#endif /* DEBUG */
  
  bo->piece[bo->next][p & 0x07]++;
}
#endif /* USE_MISC_MACRO */

/**
 * 取った駒をBOARDにセットする。手番付き。
 * (これもマクロを使った方が速くなる)
 * @param bo BOARD
 * @param p 持駒にする駒種類
 */
#ifndef USE_MISC_MACRO
void boPushPiece2(BOARD *bo, int p, int next) {
#ifdef DEBUG
  assert( bo != NULL );
  assert( 1<=p && p<=0x1F );
  assert( p != 8 && p != 0x18 );
#endif /* DEBUG */
  
  bo->piece[next][p & 0x07]++;
}
#endif /* USE_MISC_MACRO */

/**
 * 持ち駒の数をセットする。
 *
 */
void boSetPiece3(BOARD *bo, int p, int num, int next) {
#ifdef DEBUG
  assert(bo != NULL);
#endif /* DEBUG */
  
  bo->piece[next][p] = num;
}

/**
 * BOARD.toBoard から xy の駒の移動先データを取り除き１つ詰める。
 * @param bo BOARD
 * @param xy 座標
 */
void boPopToBoard(BOARD *bo, int xy) {
  int pfm, pto;
  int fm, to;
  int i, j, n, c;
  int next;
  MOVEINFO *mi;
  extern int toBoard3[2][256][toBoardCountMax];
  extern int toBoardCount3[2][256];
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( 1 <= (xy&0x0f) && (xy&0x0f) <= 9 );
  assert( 1 <= (xy>>4) && (xy>>4) <= 9 );
  assert( bo->board[ xy ] != 0 );
#endif /* DEBUG */
  
  n = bo->noBoard[ xy ];
  mi = boGetCodeTo(n);
  next = getTeban(bo->board[ xy ]);
  
  pfm = 0;
  pto = 0;
  for (i=0; i<mi->count; i++) {
    fm = mi->te[i].fm;
    to = mi->te[i].to;
    
    /* 成りの分をダブって記録しないようにする */
    if ( pfm == fm && pto == to )
      continue;
    pfm = fm;
    pto = to;
    c = toBoardCount3[next][to];
    for (j=0; j<c; j++) {
      if ( toBoard3[next][to][j] == n ) {
	toBoard3[next][to][j] = toBoard3[next][to][c-1];
	toBoard3[next][to][c-1] = 0;
	toBoardCount3[next][to]--;
	break;
      }
    }
  }
  
  mi->count = 0;
}

/**
 * BOARD.toBoard に xy の駒の移動先データをセットする。
 * @param bo BOARD
 * @param xy 座標
 */
void boPushToBoard(BOARD *bo, int xy) {
  int pfm, pto;
  int fm, to;
  int i, n;
  int next;
  MOVEINFO *mi;
  extern int toBoard3[2][256][toBoardCountMax];
  extern int toBoardCount3[2][256];
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( 1 <= (xy&0x0f) && (xy&0x0f) <= 9 );
  assert( 1 <= (xy>>4) && (xy>>4) <= 9 );
  assert( bo->board[ xy ] != 0 );
#endif /* DEBUG */
  
  n = bo->noBoard[ xy ];
  next = getTeban(bo->board[ xy ]);
  
  mi = boGetCodeTo(n);
  movetoKiki(bo, xy, mi);
  pfm = 0;
  pto = 0;
  for (i = 0; i < mi->count; i++) {
    fm = mi->te[i].fm;
    to = mi->te[i].to;
    
    /* 成りの分をダブって記録しないようにする */
    if ( pfm == fm && pto == to )
      continue;
    pfm = fm;
    pto = to;
#ifdef DEBUG
    assert( toBoardCount3[next][to] + 1 < toBoardCountMax );
#endif /* DEBUG */
    toBoard3[next][to][toBoardCount3[next][to]++] = n;
  }
}

/*
 * BOARD上の駒を動かす。
 * @param bo BOARD
 * @param te 移動情報
 * @param next 手番
 */
int boMove(BOARD *bo, TE *te, int next) {
  static MOVEINFO mi;
  int king_xy;
  int c, n, p, i;
  int flg_tori;
  extern long count_boMove;
  
#ifndef NDEBUG
  assert( bo->board[ te->fm ] != 0 );
  assert( 0 <= te->fm );
  if ( te->fm != 0 ) {
    assert( 1 <= (te->fm & 0x0f) && (te->fm & 0x0f) <= 9 );
    assert( 1 <= (te->fm >> 4) && (te->fm >> 4) <=9 );
  }
  assert( 1 <= (te->to & 0x0f) && (te->to & 0x0f) <=9 );
  assert( 1 <= (te->to >> 4) && (te->to >> 4) <=9 );
  
  if (te->uti == 0) {
    /* 移動先に自分の駒があるか調べる */
    if ( bo->board[ te->fm ] & 0x10 ) {
      /* gote */
      assert( (bo->board[ te->to ] & 0x10) == 0 );
    } else {
      /* sente */
      if ( bo->board[ te->to ] )
	assert( (bo->board[ te->to ] & 0x10) == 0x10 );
    }
  }
  
  /* 成れないのに成りにしていないか調べる */
  if ( te->nari ) {
    if ( bo->board[ te->fm ] & 0x10 ) {
      /* gote */
      assert( (bo->board[ te->fm ] + 8) <= 0x1F );
    } else {
      /* sente */
      assert( (bo->board[ te->fm ] + 8) <= 0x0F );
    }
  }
#endif /* DEBUG */
  
  flg_tori = 0;
  
  king_xy = bo->code_xy[ 1 + next ][ 0 ];
  
  count_boMove++;
  
  mi2Add( &(bo->mi), te );
  
  /* 王の安全度の評価を盤面評価に足す */
  add_oh_safe_grade(bo, SENTE);
  add_oh_safe_grade(bo, GOTE);
  
  if ( te->uti == 0 ) {
    /* 駒を移動する */
    c = bo->board[ te->to ];
    
    /* 盤面評価点数から te->fm の位置の駒の点数を引く */
    sub_grade(bo, te->fm, next );
    
    if ( c != 0 ) {
      /* 移動先に駒があった */
      
      /* 盤上から c の評価点数を引く */
      sub_grade(bo, te->to, (next == 0));
      
#ifdef USE_HASH
      /* ハッシュ値から c の駒の値を引いておく */
      sub_hash_value(bo, te->to);
#endif /* USE_HASH */
      
      /* 取った駒をBOARDにセットする */
      boPushPiece2(bo, c, next);
      
      n = bo->noBoard[ te->to ];
      bo->code_xy[ n ][ 0 ] = 0;
      /* 駒コードを戻す */
      pbPush(&(bo->pb), n);
      /* 取られる駒の効きを外す */
      boPopToBoard(bo, te->to);
      /* 取られる駒を覚えておく */
      bo->tori[ bo->tori_count++ ] = c;
      /* 盤面評価点数から取った駒 c の得点を足す */
      add_piece_grade(bo, c & 7, next );
      
      flg_tori = 1;
    } else {
      bo->tori[ bo->tori_count++ ] = 0;
    }
    
    boPopRemotePiece(bo, te->fm, &mi);
    
    /* 盤面評価点数から te->fm の位置の駒の点数を引く */
    /* sub_grade(bo, te->fm, next ); */
    
#ifdef USE_HASH
    /* ハッシュ値に te->fm の位置の駒の値を引いておく */
    sub_hash_value(bo, te->fm);
#endif /* USE_HASH */
    
    p = bo->board[ te->fm ];
    boPopToBoard(bo, te->fm);
    boSetPiece( bo, te->fm, 0 );
    
    for (i=0; i<mi.count; i++) {
      boPushToBoard(bo, mi.te[i].to);
    }
    
    boPopRemotePiece(bo, te->to, &mi);
    
    if ( te->nari ) {
#ifdef DEBUG
      assert( 1 <= (p & 0x0F) && (p & 0x0F) <= 7 );
#endif /* DEBUG */
      boSetPiece( bo, te->to, p + 8 );
    } else {
      boSetPiece( bo, te->to, p );
    }
    
    n = bo->noBoard[ te->fm ];
    bo->noBoard[ te->fm ] = 0;
    bo->noBoard[ te->to ] = n;
    bo->code_xy[n][0] = te->to;
    bo->code_xy[n][1] = bo->board[ te->to ];
    boPushToBoard(bo, te->to);
    
#ifdef USE_HASH
    /* ハッシュ値に te->to の位置の駒の値を足す */
    add_hash_value(bo, te->to);
#endif /* USE_HASH */
    
    for (i=0; i<mi.count; i++) {
      boPushToBoard(bo, mi.te[i].to);
    }
    
    /* 盤面評価点数に te->to の位置の駒の点数を加える */
    add_grade(bo, te->to, next );
  } else {
    /* 駒打ち場合 */
#ifdef DEBUG
    /* 打ち込もうとしたことろが空じゃない */
    assert( bo->board[ te->to ] == 0 );
#endif /* DEBUG */
    
    /* 盤面評価点数から取った駒 te->uti の得点を引く */
    sub_piece_grade(bo, te->uti, next);
    
    /* koma uti */
    boPopRemotePiece(bo, te->to, &mi);
    
    boSetPiece( bo, te->to, te->uti + (next << 4) );
    
#ifdef USE_HASH
    /* ハッシュ値に te->to の位置の駒の値を足す */
    add_hash_value(bo, te->to);
#endif /* USE_HASH */
    
#ifdef DEBUG
    assert( 0 <= bo->piece[ next ][ te->uti ] - 1 );
#endif /* DEBUG */
    bo->piece[ next ][ te->uti ]--;
    
    n = pbPop(&(bo->pb), te->uti);
    
    bo->noBoard[ te->to ] = n;
    bo->code_xy[n][0] = te->to;
    bo->code_xy[n][1] = bo->board[ te->to ];
    
    /* 打ち込んだ駒の効きをセットする */
    boPushToBoard(bo, te->to);
    
    for (i=0; i<mi.count; i++) {
      boPushToBoard(bo, mi.te[i].to);
    }
    
    /* 盤面評価点数に te->to の位置の駒の点数を加える */
    add_grade(bo, te->to, next);
    
    bo->tori[ bo->tori_count++ ] = 0;
  }
  
  /* 王の安全度の評価を盤面評価から引く */
  sub_oh_safe_grade(bo, SENTE);
  sub_oh_safe_grade(bo, GOTE);
  
#ifdef USE_HASH  
  /* ハッシュ値を再設定 */
  /* bo->key = hsGetHashKey( bo ); */
#endif /* USE_HASH */
  
  return flg_tori;
}

/**
 * 一手戻す。
 * @param bo 対象のBOARD
 * @param next 手番
 */
void boBack(BOARD *bo, int next) {
  static MOVEINFO mi;
  static TE te;
  int king_xy;
  int p, n, t;
  int i;
  int flg_tori;
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( 0 <= bo->mi.count - 1 );
  assert( 0 <= bo->tori_count - 1 );
#endif /* DEBUG */
  
  king_xy = bo->code_xy[ 1 + next ][ 0 ];
  
  te = bo->mi.te[ --bo->mi.count ];
  t = bo->tori[ --bo->tori_count ];
  
  flg_tori = 0;
  
  /* 王の安全度の評価を盤面評価に足す */
  add_oh_safe_grade(bo, SENTE);
  add_oh_safe_grade(bo, GOTE);
  
  if ( te.uti == 0 ) {
    /* 盤面評価点数から te.to の位置の駒の点数を引く */
    sub_grade(bo, te.to, next );
    
    boPopRemotePiece(bo, te.to, &mi);
    
#ifdef USE_HASH
    /* ハッシュ値から te.to の駒の値を引いておく */
    sub_hash_value(bo, te.to);
#endif /* USE_HASH */
    
    p = bo->board[ te.to ];
    n = bo->noBoard[ te.to ];
    boPopToBoard(bo, te.to);
    bo->board[ te.to ] = 0;
    bo->noBoard[ te.to ] = 0;
    
    if ( t ) {
      /* 駒を取っていた場合 */
      int n;
      
      flg_tori = 1;
      
      boSetPiece( bo, te.to, t );
      n = pbPop( &(bo->pb), t );
      bo->noBoard[ te.to ] = n;
      bo->code_xy[ n ][ 0 ] = te.to;
      bo->code_xy[ n ][ 1 ] = t;
      boPushToBoard(bo, te.to);
      
#ifdef NOREAD
      /* 盤面評価点数に te.to の位置の駒の点数を加える */
      add_grade(bo, te.to, (next == 0));
#endif /* NOREAD */
      
#ifdef USE_HASH
      /* ハッシュ値に te.to の位置の駒の値を足す */
      add_hash_value(bo, te.to);
#endif /* USE_HASH */
      
#ifdef DEBUG
      assert( 0 <= bo->piece[ getTeban(p) ][ t & 7 ] - 1 );
#endif /* DEBUG */
      bo->piece[ getTeban(p) ][ t & 7 ]--;
      
      /* 盤面評価点数から取った駒 t の得点を引く */
      sub_piece_grade(bo, t & 7, next);
    }
    
    for (i=0; i<mi.count; i++) {
      boPushToBoard(bo, mi.te[i].to);
    }
    
    boPopRemotePiece(bo, te.fm, &mi);
    
    bo->noBoard[ te.fm ] = n;
    bo->code_xy[n][0] = te.fm;
    if ( te.nari ) 
      p &= 0x17;
    bo->code_xy[n][1] = p;
    boSetPiece( bo, te.fm, p );
    boPushToBoard(bo, te.fm);
    
    /* 盤面評価点数に te.fm の位置の駒の点数を加える */
    add_grade(bo, te.fm, next);
    
#ifdef USE_HASH
    /* ハッシュ値に te.fm の位置の駒の値を足す */
    add_hash_value(bo, te.fm);
#endif /* USE_HASH */
    
    for (i=0; i<mi.count; i++) {
      boPushToBoard(bo, mi.te[i].to);
    }
    
    if ( flg_tori ) {
      /* 盤面評価点数に te.to の位置の駒の点数を加える */
      add_grade(bo, te.to, (next == 0));
    }
  } else {
    /* 駒打ちだった場合 */
#ifdef DEBUG
    assert( 1 <= te.uti && te.uti <= 7 );
    assert( bo->board[ te.to ] != 0 );
    assert( bo->board[ te.to ] != WALL );
#endif /* DEBUG */
    
    /* 盤面評価点数から te.to の位置の駒の点数を引く */
    sub_grade(bo, te.to, next);
    
    boPopRemotePiece(bo, te.to, &mi);
    
#ifdef USE_HASH
    /* ハッシュ値から te.to の位置の駒の値を引く */
    sub_hash_value(bo, te.to);
#endif /* USE_HASH */
    
    p = bo->board[ te.to ];
    n = bo->noBoard[ te.to ];
    boPopToBoard(bo, te.to);
    boSetPiece( bo, te.to, 0 );
    bo->noBoard[ te.to ] = 0;
    
    for (i=0; i<mi.count; i++) {
      boPushToBoard(bo, mi.te[i].to);
    }
    
#ifdef DEBUG
    {
      extern const int arr_num_of_piece[];
      assert( 1 <= te.uti && te.uti <= 7 );
      assert( bo->piece[ getTeban(p) ][ te.uti ] + 1 <= arr_num_of_piece[ te.uti ] );
    }
#endif /* DEBUG */
    bo->piece[ getTeban(p) ][ te.uti ]++;
    pbPush( &(bo->pb), n );
    
    /* 盤面評価点数に取った駒 te.uti の得点を加える */
    add_piece_grade(bo, te.uti, next);
  }
  
  /* 王の安全度の評価を盤面評価から引く */
  sub_oh_safe_grade(bo, SENTE);
  sub_oh_safe_grade(bo, GOTE);
  
#ifdef USE_HASH  
  /* ハッシュ値を再設定 */
  /* bo->key = hsGetHashKey( bo ); */
#endif /* USE_HASH */
}

/*
 * BOARD上の駒を動かす。
 * @param bo BOARD
 * @param te 移動情報
 * @param next 手番
 */
void boMove_mate(BOARD *bo, TE *te, int next) {
  static MOVEINFO mi;
  int king_xy;
  int c, n, p, i;
  extern long count_boMove;
#ifdef DEBUG
  assert( bo->board[ te->fm ] != 0 );
  assert( 0 <= te->fm );
  if ( te->fm != 0 ) {
    assert( 1 <= (te->fm & 0x0f) && (te->fm & 0x0f) <= 9 );
    assert( 1 <= (te->fm >> 4) && (te->fm >> 4) <=9 );
  }
  assert( 1 <= (te->to & 0x0f) && (te->to & 0x0f) <=9 );
  assert( 1 <= (te->to >> 4) && (te->to >> 4) <=9 );
  
  /* 移動先に自分の駒があるか調べる */
  if ( bo->board[ te->fm ] & 0x10 ) {
    /* gote */
    assert( (bo->board[ te->to ] & 0x10) == 0 );
  } else {
    /* sente */
    if ( bo->board[ te->to ] )
      assert( (bo->board[ te->to ] & 0x10) == 0x10 );
  }
  
  /* 成れないのに成りにしていないか調べる */
  if ( te->nari ) {
    if ( bo->board[ te->fm ] & 0x10 ) {
      /* gote */
      assert( (bo->board[ te->fm ] + 8) <= 0x1F );
    } else {
      /* sente */
      assert( (bo->board[ te->fm ] + 8) <= 0x0F );
    }
  }
#endif /* DEBUG */
  
  king_xy = bo->code_xy[ 1 + next ][ 0 ];
  
  count_boMove++;
  
  mi2Add( &(bo->mi), te );
  
  if ( te->uti == 0 ) {
    /* 駒を移動する */
    c = bo->board[ te->to ];
    if ( c != 0 ) {
      /* 移動先に駒があった */
      
#ifdef USE_HASH
      /* ハッシュ値から c の駒の値を引いておく */
      sub_hash_value(bo, te->to);
#endif /* USE_HASH */
      
      /* 取った駒をBOARDにセットする */
      boPushPiece2(bo, c, next);
      
      n = bo->noBoard[ te->to ];
      bo->code_xy[ n ][ 0 ] = 0;
      /* 駒コードを戻す */
      pbPush(&(bo->pb), n);
      /* 取られる駒の効きを外す */
      boPopToBoard(bo, te->to);
      /* 取られる駒を覚えておく */
      bo->tori[ bo->tori_count++ ] = c;
      /* 盤面評価点数から取った駒 c の得点を引く */
      /* sub_piece_grade(bo, c, next ); */
    } else {
      bo->tori[ bo->tori_count++ ] = 0;
    }
    
    boPopRemotePiece(bo, te->fm, &mi);
    
    /* 盤面評価点数から te->fm の位置の駒の点数を引く */
    /* sub_grade(bo, king_xy, te->fm, next ); */
    
#ifdef USE_HASH
    /* ハッシュ値から te->fm の位置の駒の値を引いておく */
    sub_hash_value(bo, te->fm);
#endif /* USE_HASH */
    
    p = bo->board[ te->fm ];
    boPopToBoard(bo, te->fm);
    boSetPiece( bo, te->fm, 0 );
    
    for (i=0; i<mi.count; i++) {
      boPushToBoard(bo, mi.te[i].to);
    }
    
    boPopRemotePiece(bo, te->to, &mi);
    
    if ( te->nari ) {
#ifdef DEBUG
      assert( 1 <= (p & 0x0F) && (p & 0x0F) <= 7 );
#endif /* DEBUG */
      boSetPiece( bo, te->to, p + 8 );
    } else {
      boSetPiece( bo, te->to, p );
    }
    
    /* 盤面評価点数に te->to の位置の駒の点数を加える */
    /* add_grade(bo, king_xy, te->to, next ); */
    
#ifdef USE_HASH
    /* ハッシュ値に te->to の位置の駒の値を足す */
    add_hash_value(bo, te->to);
#endif /* USE_HASH */
    
    n = bo->noBoard[ te->fm ];
    bo->noBoard[ te->fm ] = 0;
    bo->noBoard[ te->to ] = n;
    bo->code_xy[n][0] = te->to;
    bo->code_xy[n][1] = bo->board[ te->to ];
    boPushToBoard(bo, te->to);
    
    for (i=0; i<mi.count; i++) {
      boPushToBoard(bo, mi.te[i].to);
    }
  } else {
    /* 駒打ち場合 */
#ifdef DEBUG
    /* 打ち込もうとしたことろが空じゃない */
    assert( bo->board[ te->to ] == 0 );
#endif /* DEBUG */
    
    /* koma uti */
    boPopRemotePiece(bo, te->to, &mi);
    
    boSetPiece( bo, te->to, te->uti + (next << 4) );
    
    /* 盤面評価点数に te->to の位置の駒の点数を加える */
    /* add_grade(bo, king_xy, te->to, next); */
    
#ifdef USE_HASH
    /* ハッシュ値に te->to の位置の駒の値を足す */
    add_hash_value(bo, te->to);
#endif /* USE_HASH */
    
#ifdef DEBUG
    assert( 0 <= bo->piece[ next ][ te->uti ] - 1 );
#endif /* DEBUG */
    bo->piece[ next ][ te->uti ]--;
    
    /* 盤面評価点数から取った駒 te->uti の得点を引く */
    /* sub_piece_grade(bo, te->uti, next); */
    
    n = pbPop(&(bo->pb), te->uti);
    
    bo->noBoard[ te->to ] = n;
    bo->code_xy[n][0] = te->to;
    bo->code_xy[n][1] = bo->board[ te->to ];
    
    /* 打ち込んだ駒の効きをセットする */
    boPushToBoard(bo, te->to);
    
    for (i=0; i<mi.count; i++) {
      boPushToBoard(bo, mi.te[i].to);
    }
    
    bo->tori[ bo->tori_count++ ] = 0;
  }
  
#ifdef USE_HASH  
  /* ハッシュ値を再設定 */
  /* bo->key = hsGetHashKey( bo ); */
#endif /* USE_HASH */
}


/**
 * 一手戻す。
 * @param bo 対象のBOARD
 * @param next 手番
 */
void boBack_mate(BOARD *bo, int next) {
  static MOVEINFO mi;
  static TE te;
  int king_xy;
  int p, n, t;
  int i;
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( 0 <= bo->mi.count - 1 );
  assert( 0 <= bo->tori_count - 1 );
#endif /* DEBUG */
  
  king_xy = bo->code_xy[ 1 + next ][ 0 ];
  
  te = bo->mi.te[ --bo->mi.count ];
  t = bo->tori[ --bo->tori_count ];
  
  if ( te.uti == 0 ) {
    boPopRemotePiece(bo, te.to, &mi);
    
    /* 盤面評価点数から te.to の位置の駒の点数を引く */
    /* sub_grade(bo, king_xy, te.to, next ); */
    
#ifdef USE_HASH
    /* ハッシュ値から te.to の駒の値を引いておく */
    sub_hash_value(bo, te.to);
#endif /* USE_HASH */
    
    p = bo->board[ te.to ];
    n = bo->noBoard[ te.to ];
    boPopToBoard(bo, te.to);
    boSetPiece( bo, te.to, 0 );
    bo->noBoard[ te.to ] = 0;
    
    if ( t ) {
      int n;
      /* 駒を取っていた場合 */
      
      boSetPiece( bo, te.to, t );
      n = pbPop( &(bo->pb), t );
      bo->noBoard[ te.to ] = n;
      bo->code_xy[ n ][ 0 ] = te.to;
      bo->code_xy[ n ][ 1 ] = t;
      boPushToBoard(bo, te.to);
      
      /* 盤面評価点数に te.to の位置の駒の点数を加える */
      /* add_grade(bo, king_xy, te.to, next); */
      
#ifdef USE_HASH
      /* ハッシュ値に te.to の位置の駒の値を足す */
      add_hash_value(bo, te.to);
#endif /* USE_HASH */
      
#ifdef DEBUG
      assert( 0 <= bo->piece[ getTeban(p) ][ t & 7 ] - 1 );
#endif /* DEBUG */
      bo->piece[ getTeban(p) ][ t & 7 ]--;
      
      /* 盤面評価点数から取った駒 t の得点を引く */
      /* sub_piece_grade(bo, t & 7, next); */
    }
    
    for (i=0; i<mi.count; i++) {
      boPushToBoard(bo, mi.te[i].to);
    }
    
    boPopRemotePiece(bo, te.fm, &mi);
    
    bo->noBoard[ te.fm ] = n;
    bo->code_xy[n][0] = te.fm;
    if ( te.nari ) 
      p &= 0x17;
    bo->code_xy[n][1] = p;
    boSetPiece( bo, te.fm, p );
    boPushToBoard(bo, te.fm);
    
    /* 盤面評価点数に te.fm の位置の駒の点数を加える */
    /* add_grade(bo, king_xy, te.fm, next); */
    
#ifdef USE_HASH
    /* ハッシュ値に te.fm の位置の駒の値を足す */
    add_hash_value(bo, te.fm);
#endif /* USE_HASH */
    
    for (i=0; i<mi.count; i++) {
      boPushToBoard(bo, mi.te[i].to);
    }
  } else {
    /* 駒打ちだった場合 */
#ifdef DEBUG
    assert( 1 <= te.uti && te.uti <= 7 );
    assert( bo->board[ te.to ] != 0 );
    assert( bo->board[ te.to ] != WALL );
#endif /* DEBUG */
    
    boPopRemotePiece(bo, te.to, &mi);
    
    /* 盤面評価点数から te.to の位置の駒の点数を引く */
    /* sub_grade(bo, king_xy, te.to, next); */
    
#ifdef USE_HASH
    /* ハッシュ値から te.to の位置の駒の値を引く */
    sub_hash_value(bo, te.to);
#endif /* USE_HASH */
    
    p = bo->board[ te.to ];
    n = bo->noBoard[ te.to ];
    boPopToBoard(bo, te.to);
    boSetPiece( bo, te.to, 0 );
    bo->noBoard[ te.to ] = 0;
    
    for (i=0; i<mi.count; i++) {
      boPushToBoard(bo, mi.te[i].to);
    }
    
#ifdef DEBUG
    {
      extern const int arr_num_of_piece[];
      assert( 1 <= te.uti && te.uti <= 7 );
      assert( bo->piece[ getTeban(p) ][ te.uti ] + 1 <= arr_num_of_piece[ te.uti ] );
    }
#endif /* DEBUG */
    bo->piece[ getTeban(p) ][ te.uti ]++;
    pbPush( &(bo->pb), n );
    
    /* 盤面評価点数に取った駒 te.uti の得点を加える */
    /* add_piece_grade(bo, te.uti, next); */
  }
  
#ifdef USE_HASH  
  /* ハッシュ値を再設定 */
  /* bo->key = hsGetHashKey( bo ); */
#endif /* USE_HASH */
}

/**
 * xyに香、角、飛車、馬、竜の効きがある場合、それらの効きを外し
 * 効きがあった駒の場所を記録しておく。
 * @param bo 対象のBOARD
 * @param xy 場所
 * @param mi 結果を記録するMOVEINFO
 */
void boPopRemotePiece(BOARD *bo, int xy, MOVEINFO *mi) {
  static TE te;
  int n, p;
  int i;
  extern int toBoard3[2][256][toBoardCountMax];
  extern int toBoardCount3[2][256];
  
  /* miAdd() 内の引数チェックでエラーにならないようにするため */
  te.fm = 0x11;

  mi->count = 0;
  
  /* sente */
  for ( i=0; i<=toBoardCount3[ SENTE ][ xy ] ; ) {
    n = toBoard3[ SENTE ][ xy ][ i ];
    p = bo->code_xy[ n ][ 1 ] & 0xF;
    if ( p != KYO && p != KAKU && p != HISYA && p != UMA && p != RYU ) {
      i++;
      if ( i == toBoardCount3[ SENTE ][ xy ] )
	break;
      continue;
    }
    te.to = bo->code_xy[ n ][ 0 ];
    miAdd( mi, &te );
    boPopToBoard( bo, bo->code_xy[ n ][ 0 ] );
  }

  /* gote */
  for ( i=0; i<=toBoardCount3[ GOTE ][ xy ] ; ) {
    n = toBoard3[ GOTE ][ xy ][ i ];
    p = bo->code_xy[ n ][ 1 ] & 0xF;
    if ( p != KYO && p != KAKU && p != HISYA && p != UMA && p != RYU ) {
      i++;
      if ( i == toBoardCount3[ GOTE ][ xy ] )
	break;
      continue;
    }
    te.to = bo->code_xy[ n ][ 0 ];
    miAdd( mi, &te );
    boPopToBoard( bo, bo->code_xy[ n ][ 0 ] );
  }
}

/**
 * bo1 と bo2 を比較する。内容が等しければ 0 を返す。
 * 違いがあれば 0 以外を返す。
 * @param bo1 対象のBOARD
 * @param bo2 対象のBOARD
 * @return 比較結果
 */
int boCmp(BOARD *bo1, BOARD *bo2) {
  if ( memcmp( bo1->board, bo2->board, sizeof( bo1->board ) ) )
    return 1;

  if ( memcmp( bo1->piece[ 0 ], bo2->piece[ 0 ], sizeof( bo1->piece[ 0 ] ) ) )
    return 1;

  if ( memcmp( bo1->piece[ 1 ], bo2->piece[ 1 ], sizeof( bo1->piece[ 0 ] ) ) )
    return 1;

  return 0;
}

/**
 * 使っていない駒を詰将棋用に受け側の持駒としてセットする。
 * @param bo 対象のBOARD
 */
void boSetTsumeShogiPiece(BOARD *bo) {
  static int c[] = {
    0, 
    18, 4, 4, 4, 4, 2, 2,
  };
  int i;

  for ( i=1; i<=7; i++ ) {
    bo->piece[ (bo->next == 0) ][ i ] =
      c[ i ] - bo->pb.count[ i ] - bo->piece[ bo->next ][ i ];
  }
}

/**
 * ルール上正しい１手かチェックする。
 * @param bo 対象のBOARD
 * @param te 調べる一手
 * @return 正しければ 1 を返す。正しくなければ 0 を返す。
 */
int boCheckTE(BOARD *bo, TE *te) {
  static MOVEINFO mi;
  int flg_check;
  int i;
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( te != NULL );
  assert( te->nari == 0 || te->nari == 1 );
#endif /* DEBUG */

  if ( (te->to >> 4) < 1 || 9 < (te->to >> 4) )
    return 0;
  if ( (te->to & 0x0F) < 1 || 9 < (te->to & 0x0F) )
    return 0;
  
  if ( te->fm != 0 ) {
    /* 駒を動かす場合 */
    if ( (te->fm >> 4) < 1 || 9 < (te->fm >> 4) )
      return 0;
    if ( (te->fm & 0x0F) < 1 || 9 < (te->fm & 0x0F) )
      return 0;
    
    if ( bo->board[ te->fm ] == 0 )
      return 0;
  
    if ( bo->board[ te->fm ] == WALL )
      return 0;
    
    if ( getTeban(bo->board[ te->fm ]) != bo->next )
      return 0;
  } else {
    /* 駒打ちの場合 */
    int p = te->uti;

    if ( te->uti < 1 || 7 < te->uti )
      return 0;
    
    if ( bo->piece[ bo->next ][ te->uti ] == 0 )
      return 0;
    if ( bo->board[ te->to ] != 0 )
      return 0;

    if (bo->next == SENTE) {
      if ((p == FU || p == KYO) && te->to <= 0x19) {
	return 0;
      } else if (p == KEI && te->to <= 0x29) {
	return 0;
      }
    } else {
      if ((p == FU || p == KYO) && 0x91 <= te->to) {
	return 0;
      } else if (p == KEI && 0x81 <= te->to) {
	return 0;
      }
    }
    
    if (p == FU && bo->next == SENTE) {
      for (i=1; i<=9; i++) {
	if (FU == boGetPiece(bo, (i << 4) + (te->to & 0xF))) {
	  return 0;
	}
      }
    }

    if (p == FU && bo->next == GOTE) {
      for (i=1; i<=9; i++) {
	if (FU + 0x10 == boGetPiece(bo, (i << 4) + (te->to & 0xF))) {
	  return 0;
	}
      }
    }

    return 1;
  }

  /* ルール上指せる手か調べる */
  flg_check = 0;
  moveto( bo, te->fm, &mi );
  for ( i=0; i<mi.count; i++ ) {
    if ( mi.te[i].fm == te->fm &&
	mi.te[i].to == te->to &&
	mi.te[i].nari == te->nari &&
	mi.te[i].uti == te->uti	)
      flg_check = 1;
  }
  
  if ( flg_check == 0 ) {
    return 0;
  }

  return boCheckTEOhte(bo, te);
}

/**
 * te を指した後に手番の王が王手になっていないか調べる。
 * @return 正しければ 1 を返す。正しくなければ 0 を返す。
 * @param bo 対象のBOARD
 * @param te 調べるTE
 */
int boCheckTEOhte(BOARD *bo, TE *te) {
  MOVEINFO mi;
  int king_xy;
  
  boMove_mate(bo, te, bo->next);
  king_xy = bo->code_xy[ 1 + bo->next ][ 0 ];
  moveto2(bo, &mi, king_xy, (bo->next == 0));
  boBack_mate( bo, bo->next );
  
  if ( mi.count == 0 ) {
    return 1;
  } else {
    return 0;
  }
    
}

/**
 * 先手後手を入れ換える。
 * @param bo 対象のBOARD
 */
#ifndef USE_MISC_MACRO
void boToggleNext(BOARD *bo) {
#ifdef DEBUG
  assert( bo != NULL );
  assert( bo->next == SENTE || bo->next == GOTE );
#endif /* DEBUG */
  bo->next = (bo->next == 0);
}
#endif /* USE_MISC_MACRO */

/**
 * te の手が成りが可能か調べる。
 * 返り値が 0 だったら成れない、1 だったら成れる。2 だったら成るしかない。
 * @param bo 対象のBOARD
 * @param te 対象のTE
 * @return 0 だったら成れない、1 だったら成れる。2 だったら成るしかない。
 */
int boCanNari(BOARD *bo, TE *te) {
  int p;
  int next;
  int fm ,to;

  if ( te->uti ) {
    return 0;
  }

  p = boGetPiece(bo, te->fm);
  next = getTeban(p);

#ifdef DEBUG
  assert(bo != NULL);
  assert(te != NULL);
  assert(p != 0 && p != WALL);
#endif /* DEBUG */

  p &= 0x0F;

  /* 王と金は成れない。戻る */
  if (p == OH || p == KIN) {
    return 0;
  }
  /* もう成っている駒はなれない。戻る */
  if ((p & 0x08) == 0x08) {
    return 0;
  }

  fm = (te->fm >> 4);
  to = (te->to >> 4);

  if ( next == SENTE ) {
    if ( (p == FU || p == KYO) && to <= 1 ) {
      return 2;
    } else if ( p == KEI && to <= 2 ) {
      return 2;
    } else if (fm <= 3 || to <= 3) {
      return 1;
    }
  } else {
    if ( (p == FU || p == KYO) && 9 <= to ) {
      return 2;
    } else if ( p == KEI && 8 <= to ) {
      return 2;
    } else if (7 <= fm || 7 <= to) {
      return 1;
    }
  }

  return 0;
}

/**
 * 現在の時刻を記録する。
 * (思考時間の測定用)
 * @param bo 対象のBOARD
 */
void boSetNowTime(BOARD *bo) {
  bo->before_think_time = time(NULL);
}

/**
 * 消費した時間を記録する。
 * @param bo 対象のBOARD
 */ 
void boSetUsedTime(BOARD *bo) {
  time_t used_time;
  
  used_time = time(NULL) - bo->before_think_time;
#ifdef DEBUG
  assert( 0 <= used_time );
#endif /* DEBUG */
  if ( used_time <= 0 ) {
    used_time = 1;
  }
  
  bo->total_time[bo->next] += used_time;
  
  bo->used_time[bo->mi.count] = used_time;
}

/**
 * 対局用に正当な盤面か調べる。
 * @param bo 対象のBOARD
 * @return 1 ならば正当。0 ならば不正。
 */
int boCheckJustGame(BOARD *bo) {
  extern int arr_xy[81];
  MOVEINFO mi;
  int oh_xy[2];
  int piece[10];
  const int piecemax[] = {
    0, 18, 4, 4, 4, 4, 2, 2, 1, 1,
  };
  int xy, p, i;
  int x, y, flg;

  assert(bo != NULL);

  oh_xy[0] = 0;
  oh_xy[1] = 0;
  for (i=0; i<10; i++) {
    piece[i] = 0;
  }

  for (i=1; i<=81; i++) {
    xy = arr_xy[i];

    p = boGetPiece(bo, xy);

    if (p == 0) {
      continue;
    }

    movetoKiki(bo, xy, &mi);

    if (mi.count == 0) {
      return 0;
    }

    if (p  == 8) {
      piece[8]++;
      oh_xy[0] = xy;
    } else if (p == 0x18) {
      piece[9]++;
      oh_xy[1] = xy;
    } else {
      piece[p & 7]++;
    }
  }

  for (x=1; x<=9; x++) {
    flg = 0;
    for (y=1; y<=9; y++) {
      p = boGetPiece(bo, (y << 4) + x);
      if (p == FU) {
	if (flg == 1) {
	  return 0;
	}
	flg = 1;
      }
    }
  }

  for (x=1; x<=9; x++) {
    flg = 0;
    for (y=1; y<=9; y++) {
      p = boGetPiece(bo, (y << 4) + x);
      if (p == FU + 0x10) {
	if (flg == 1) {
	  return 0;
	}
	flg = 1;
      }
    }
  }

  /* 持ち駒の数を調べる */
  for (i=1; i<=7; i++) {
    if (piecemax[i] < bo->piece[0][i]) {
      return 0;
    }
    if (piecemax[i] < bo->piece[1][i]) {
      return 0;
    }
    piece[i] += bo->piece[0][i];
    piece[i] += bo->piece[1][i];
  }

  /* 駒全部の数を数える */
  for (i=1; i<=8; i++) {
    if (piecemax[i] < piece[i]) {
      return 0;
    }
  }

  if (oh_xy[0] == 0) {
    return 0;
  }
  if (oh_xy[1] == 0) {
    return 0;
  }

  return 1;
}

/**
 * 詰将棋用に正当な盤面か調べる。
 * @param bo 対象のBOARD
 * @return 1 ならば正当。0 ならば不正。
 */
int boCheckJustMate(BOARD *bo) {
  extern int arr_xy[81];
  MOVEINFO mi;
  int oh_xy[2];
  int piece[10];
  const int piecemax[] = {
    0, 18, 4, 4, 4, 4, 2, 2, 1, 1,
  };
  int xy, p, i;
  int x, y, flg;

  assert(bo != NULL);

  oh_xy[0] = 0;
  oh_xy[1] = 0;
  for (i=0; i<10; i++) {
    piece[i] = 0;
  }

  for (i=1; i<=81; i++) {
    xy = arr_xy[i];

    p = boGetPiece(bo, xy);

    if (p == 0) {
      continue;
    }

    movetoKiki(bo, xy, &mi);

    if (mi.count == 0) {
      return 0;
    }

    if (p  == 8) {
      piece[8]++;
      oh_xy[0] = xy;
    } else if (p == 0x18) {
      piece[9]++;
      oh_xy[1] = xy;
    } else {
      piece[p & 7]++;
    }
  }

  for (x=1; x<=9; x++) {
    flg = 0;
    for (y=1; y<=9; y++) {
      p = boGetPiece(bo, (y << 4) + x);
      if (p == FU) {
	if (flg == 1) {
	  return 0;
	}
	flg = 1;
      }
    }
  }

  for (x=1; x<=9; x++) {
    flg = 0;
    for (y=1; y<=9; y++) {
      p = boGetPiece(bo, (y << 4) + x);
      if (p == FU + 0x10) {
	if (flg == 1) {
	  return 0;
	}
	flg = 1;
      }
    }
  }

  /* 持ち駒の数を調べる */
  for (i=1; i<=7; i++) {
    if (piecemax[i] < bo->piece[0][i]) {
      return 0;
    }
    if (piecemax[i] < bo->piece[1][i]) {
      return 0;
    }
    piece[i] += bo->piece[0][i];
    piece[i] += bo->piece[1][i];
  }

  /* 駒全部の数を数える */
  for (i=1; i<=9; i++) {
    if (piecemax[i] < piece[i]) {
      return 0;
    }
  }

  if (bo->next == SENTE) {
    if (oh_xy[1] == 0) {
      return 0;
    }
  } else {
    if (oh_xy[0] == 0) {
      return 0;
    }
  }
  
  if (bo->next == SENTE) {
    if (0 < boGetToBoardCount(oh_xy[1], SENTE)) {
      return 0;
    }
  } else {
    if (0 < boGetToBoardCount(oh_xy[0], GOTE)) {
      return 0;
    }
  }

  return 1;
}

