/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * fileio.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/fileio.c,v $
 * $Id: fileio.c,v 1.1.1.1 2005/12/09 09:03:08 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include "si.h"
#include "ui.h"

#define READ_FILE_ERROR (1)

#ifndef BUFSIZ
  #define BUFSIZ 256
#endif /* BUFSIZ */

char *commentStep_gets(char *buff, int Len, FILE * FP);
char *findTopStr_gets(char *buff, char *str, int Len, FILE * FP);

/**
 * CSA形式棋譜ファイルを出力する。ファイル名は日時から自動で付けられる。
 * @param bo 対象のBOARD
 */
void CSA_output_auto(BOARD *bo) {
  time_t t;
  struct tm *tm;
  char buf[BUFSIZ];

  t = time(NULL);
  tm = localtime(&t);

  sprintf(buf, "%04d%02d%02d%02d%02d.csa",
	  tm->tm_year + 1900,
	  tm->tm_mon + 1,
	  tm->tm_mday,
	  tm->tm_hour,
	  tm->tm_min);

  CSA_output(bo, buf);
}

/**
 * CSA形式棋譜ファイルを出力する。
 * @param bo 対象のBOARD
 * @param filename 出力するファイル名
 */
void CSA_output(BOARD *bo, char *filename) {
  FILE *fp;
  time_t t;
  struct tm *tm;
  MOVEINFO2 *mi;
  MOVEINFO2 *tmp_mi;
  TE *te;
  int i;
  static char *PIECE_NAME[] = {
    "", 
    "FU", "KY", "KE", "GI", "KI", "KA", "HI", "OU", 
    "TO", "NY", "NK", "NG", "",   "UM", "RY", "",
  };
  
  fp = fopen(filename, "w");
  if (fp == NULL) {
    return;
  }

  t = time(NULL);
  tm = localtime(&t);
  mi = &(bo->mi);
  tmp_mi = newMOVEINFO2();
  mi2Copy(tmp_mi, mi);

  /* 盤面を１手目まで戻す */
  while ( 0 < mi->count ) {
    boBack(bo, (mi->count % 2) == 0);
    boToggleNext(bo);
  }

  fprintf(fp, "'SI SHOGI version %s-%s 棋譜ファイル\n", VERSION, REVISION);
  fprintf(fp, "'ファイル名: %s\n", filename);
  fprintf(fp, "'対  局  日: %04d/%02d/%02d\n",
	  tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday);
  fprintf(fp, "'開始時刻  : %02d:%02d:%02d\n",
	  tm->tm_hour, tm->tm_min, tm->tm_sec);
  fprintf(fp, "'対局者名\n");
  fprintf(fp, "N+SENTE\n");
  fprintf(fp, "N-GOTE\n");
  fprintf(fp, "'盤面\n");
  fprintf(fp, "P1-KY-KE-GI-KI-OU-KI-GI-KE-KY\n");
  fprintf(fp, "P2 * -HI *  *  *  *  * -KA * \n");
  fprintf(fp, "P3-FU-FU-FU-FU-FU-FU-FU-FU-FU\n");
  fprintf(fp, "P4 *  *  *  *  *  *  *  *  * \n");
  fprintf(fp, "P5 *  *  *  *  *  *  *  *  * \n");
  fprintf(fp, "P6 *  *  *  *  *  *  *  *  * \n");
  fprintf(fp, "P7+FU+FU+FU+FU+FU+FU+FU+FU+FU\n");
  fprintf(fp, "P8 * +KA *  *  *  *  * +HI * \n");
  fprintf(fp, "P9+KY+KE+GI+KI+OU+KI+GI+KE+KY\n");
  fprintf(fp, "'持駒\n");
  fprintf(fp, "'手番\n");
  fprintf(fp, "+\n");
  fprintf(fp, "'指し手と消費時間\n");

  for (i=0; i<tmp_mi->count; i++) {
    te = &(tmp_mi->te[i]);

    /* 手番出力 */
    fprintf(fp, "%c", i % 2 ? '-' : '+');

    fprintf(fp, "%d", (te->fm) & 0xF);
    fprintf(fp, "%d", (te->fm) >> 4);
    fprintf(fp, "%d", (te->to) & 0xF);
    fprintf(fp, "%d", (te->to) >> 4);
    if ( tmp_mi->te[i].fm == 0 ) {
      fprintf(fp, "%s\n", PIECE_NAME[te->uti]);
    } else if ( tmp_mi->te[i].nari == 0 ) {
      fprintf(fp, "%s\n", PIECE_NAME[boGetPiece(bo, te->fm) & 0xF]);
    } else {
      fprintf(fp, "%s\n", PIECE_NAME[(boGetPiece(bo, te->fm) & 0xF) + 0x08]);
    }
    fprintf(fp, "T%ld\n", bo->used_time[bo->mi.count]);
    
    /* 一手進める */
    boMove(bo, te, (i % 2));
    boToggleNext(bo);
  }

  fprintf(fp, "%%TORYO\n");
  
  fclose(fp);
}

/*
 * int loadBOARD( char *filename, BOARD *board ) ;
 *
 *     filename  で示される   si_board  形式のファイルを読み込み
 *     board  に盤面データとして記録する.
 *
 *     戻り値
 *     0: 正常.読み込み成功.
 *     READ_FILE_ERROR: 読み込み失敗.
 */
int loadBOARD(char *filename, BOARD * brd) {
  FILE *fp;
  char buff[BUFSIZ];
  char *endptr;
  int i, j;
  
  if ((fp = fopen(filename, "r")) == NULL) {
    return READ_FILE_ERROR;
  }
  findTopStr_gets(buff, "/Board", BUFSIZ-1, fp);

  for (i = 0; i < 9; i++) {
    if (commentStep_gets(buff, BUFSIZ-1, fp) == NULL) {
      fclose(fp);
      return READ_FILE_ERROR;
    }
    for (j = 8; 0 <= j; j--) {
      if (isxdigit((int)(buff[j * 3])) && isxdigit((int)(buff[j * 3 + 1]))) {
	boSetPiece( brd, ((i+1) << 4) + 9 - j, 
		 (int) strtol((const char *) (buff + j * 3), &endptr, 16) );
      } else {
	fclose(fp);
	puts( "err3\n" );
	return READ_FILE_ERROR;
      }
    }
  }

  findTopStr_gets(buff, "/Piece", BUFSIZ-1, fp);

  /* sente's piece */
  if (commentStep_gets(buff, BUFSIZ-1, fp) == NULL) {
    fclose(fp);
    return READ_FILE_ERROR;
  }
  brd->piece[0][7] = (int) (buff[0] - 0x30); /* hisya */
  brd->piece[0][6] = (int) (buff[2] - 0x30); /* kaku */
  brd->piece[0][5] = (int) (buff[4] - 0x30); /* kin */
  brd->piece[0][4] = (int) (buff[6] - 0x30); /* gin */
  brd->piece[0][3] = (int) (buff[8] - 0x30); /* kei */
  brd->piece[0][2] = (int) (buff[10] - 0x30); /* kyo */ 
  brd->piece[0][1] = (int) (buff[12] - 0x30) * 10 + buff[13] - 0x30; /* fu */
  brd->piece[0][0] = (int) (buff[10] - 0x30); /* dummy */

  /* gote's piece */
  if (commentStep_gets(buff, BUFSIZ-1, fp) == NULL) {
    fclose(fp);
    return READ_FILE_ERROR;
  }
  brd->piece[1][7] = (int) (buff[0] - 0x30); /* hisya */
  brd->piece[1][6] = (int) (buff[2] - 0x30); /* kaku */
  brd->piece[1][5] = (int) (buff[4] - 0x30); /* kin */
  brd->piece[1][4] = (int) (buff[6] - 0x30); /* gin */
  brd->piece[1][3] = (int) (buff[8] - 0x30); /* kei */
  brd->piece[1][2] = (int) (buff[10] - 0x30); /* kyo */
  brd->piece[1][1] = (int) (buff[12] - 0x30) * 10 + buff[13] - 0x30; /* fu */
  brd->piece[1][0] = (int) (buff[10] - 0x30); /* dummy */
  
  findTopStr_gets(buff, "/Next move", 255, fp);

  if (commentStep_gets(buff, BUFSIZ-1, fp) == NULL) {
    fclose(fp);
    return READ_FILE_ERROR;
  }
  brd->next = (int) atoi(buff);
  fclose(fp);

  /* init BOARD */
  boSetNumber(brd);
  boSetToBOARD(brd);
  
#ifdef USE_HASH
  brd->key = hsGetHashKey( brd );
#endif /* USE_HASH */
  
  return 0;
}

char *commentStep_gets(char *buffer, int len, FILE * fp) {
  do {
    if (fgets((char *) buffer, len, fp) == NULL)
      return NULL;
    while (isspace((int)(*buffer)))
      buffer++;
  } while (*buffer == ';' || *buffer == '\n');
  
  return buffer;
}

char *findTopStr_gets(char *buffer, char *str, int len, FILE * fp) {
  do {
    if (commentStep_gets(buffer, len, fp) == NULL)
      return NULL;
    while (isspace((int)(*buffer)))
      buffer++;
  } while (strncmp((char *) buffer, str, strlen(str)));
  
  return buffer;
}
