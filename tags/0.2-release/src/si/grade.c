/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * grade.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/grade.c,v $
 * $Id: grade.c,v 1.1.1.1 2005/12/09 09:03:08 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"
#include "ui.h"

/**
 * 歩が前に進んだときにポインタを加算するか？のフラグ。
 * 定義されていたら加算する。
 */
#undef USE_ADD_FU_ADVANCE_POINT

/**
 * 桂、香車は上に行く程ポイントを減らすか？のフラグ。
 * 定義されていたら減らす。
 */
#undef USE_DEC_KEI_KYO_POINT

/**
 * 盤面の駒の種類位置、持駒の数から盤面評価点数を計算して返す。
 * @param bo 対象のBOARD
 * @param next 計算する手番
 * @return 盤面評価点数
 */
int grade(BOARD *bo, int next) {
  MOVEINFO *mi;
  int point, p, piece_p, n;
  int king_xy, kx, ky;
  int jx, jy;
  int x, y;
  int v, w;
  int i;
  extern const int arr_point[];
  extern const int arr_piece[];
  extern const int arr_xy[];
  extern const int arr_point_xy[17][9];
  extern const int arr_point_xy2[17][9];

#ifdef DEBUG
  assert( bo != NULL );
  assert( next == 0 || next == 1 );
#endif /* DEBUG */

  /* 自玉の位置 */
  king_xy = bo->code_xy[1 + next][0];
  kx = king_xy & 0xF;
  ky = king_xy >> 4;

  /* 相手玉の位置 */
  king_xy = bo->code_xy[1 + (next==0)][0];
  jx = king_xy & 0xF;
  jy = king_xy >> 4;

  point = 0;

  for (i = 1; i <= 81; i++) {
    p = boGetPiece(bo, arr_xy[ i ]);
    if ( p == 0 || getTeban( p ) != next || p == OH || p == (OH | 0x10))
      continue;
    
    n = boGetNoBoard(bo, arr_xy[ i ]);

    piece_p = arr_point[ p ];
    point += piece_p;
    /* printf("%02X : %d\n", p, piece_p); */

    x = arr_xy[ i ] & 0xF;
    y = arr_xy[ i ] >> 4;

    /* 歩は上へ行く程ポイントが高い */
#ifdef USE_ADD_FU_ADVANCE_POINT
    if ( (p & 0xF) == FU ) {
      if ( next == SENTE ) {
	bo->point[ SENTE ] += (10-y) * FU_POSITION_POINT;
      } else {
	bo->point[ GOTE ] += y * FU_POSITION_POINT;
      }
    }
#endif /* USE_ADD_FU_ADVANCE_POINT */
#ifdef USE_DEC_KEI_KYO_POINT
    if ( (p & 0xF) == KYO ) {
      /* 香車は上に動く程点数を減らす */
      if ( next == SENTE ) {
	point -= (10-y) * 10;
      } else {
	point -= y * 10;
      }
    } else if ( (p & 0xF) == KEI ) {
      /* 桂馬は上に動く程点数を減らす */
      if ( next == SENTE ) {
	point -= (10-y) * 10;
      } else {
	point -= y * 10;
      }
    }
#endif /* USE_DEC_KEI_KYO_POINT */

    /* 飛車、角の場合は位置によるポイント増減はしない */
    /* if ( ((p & HISYA) != HISYA) || ((p & KAKU) == KAKU) ) { */
    if ( ((p & 0xF) != HISYA) && ((p & 0xF) == KAKU) ) {
      v = abs(kx - x);
      if ( next == SENTE )
	w = ky - y;
      else 
	w = y - ky;
      
      /* point += arr_point_xy2[ 8 - w ][ v ]; */
      point += piece_p * ( arr_point_xy2[ 8 - w ][ v ] - 100 ) / MY_OH_DISTANCE_POINT;
    
      v = abs(jx - x);
      if ( next == SENTE )
	w = jy - y;
      else 
	w = y - jy;

      /* point += arr_point_xy[ 8 - w ][ v ]; */
      point += piece_p * ( arr_point_xy[ 8 - w ][ v ] - 100 ) / ONES_OH_DISTANCE_POINT;
    }

    if ( (p & 0xF) == HISYA && getTeban(p) == next ) {
      /* 「王飛接近するべからず」の格言通り王と飛車が近い場合は減点する */
      int hx, hy;

      hx = abs(kx - x);
      hy = abs(ky - y);
      if ( 4 < hx + hy ) {
	point -= ((hx + hy) * OH_HISYA_POINT);
      }
    }
    
    /* 駒の動ける数を評価に加える */
    mi = boGetCodeTo(n);
    point += mi->count << NUM_OF_MOVE_POINT;
  }
  
  for (i = 1; i<=7; i++) {
    point += arr_piece[ i ] * bo->piece[ next ][ i ];
  }
  
  bo->point[ next ] = point;
  
  /* 王の安全度の評価 */
  add_oh_safe_grade(bo, next);

  return point;
}

/**
 * 盤面評価点数に xy の位置の駒の点数を加える。
 * @param bo 対象のBOARD
 * @param xy 対象の駒の位置
 * @param next 盤面評価点数を増減する手番
 */
void add_grade(BOARD *bo, int xy, int next ) {
  MOVEINFO *mi;
  int king_xy;
  int kx, ky;
  int jx, jy;
  int x, y;
  int v, w;
  int p, piece_p, n;
  extern const int arr_point[];
  extern const int arr_point_xy[17][9];
  extern const int arr_point_xy2[17][9];
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( bo->board[xy] != 0 );
  assert( bo->board[xy] != WALL );
  assert( getTeban(bo->board[xy]) == next );
  assert( next == 0 || next == 1 );
#endif /* DEBUG */
  
  /* 自玉の位置 */
  king_xy = bo->code_xy[1 + next][0];
  kx = king_xy & 0xF;
  ky = king_xy >> 4;
  
#ifdef DEBUG
  assert( bo->board[king_xy] == (0x08 + next * 0x10) );
#endif /* DEBUG */
  
  /* p = bo->board[ xy ]; */
  p = boGetPiece(bo, xy);
  if ( p == 0 || getTeban( p ) != next || p == OH || p == (OH | 0x10))
    return;
  
  n = boGetNoBoard(bo, xy);
  
  /* 相手玉の位置 */
  king_xy = bo->code_xy[1 + (next==0)][0];
  jx = king_xy & 0xF;
  jy = king_xy >> 4;

  x = xy & 0xF;
  y = xy >> 4;

  piece_p = arr_point[ p ];
  bo->point[ next ] += piece_p;

  /* 歩は上へ行く程ポイントが高い */
#ifdef USE_ADD_FU_ADVANCE_POINT
  if ( (p & 0xF) == FU ) {
    if ( next == SENTE ) {
      bo->point[ SENTE ] += (10-y) * FU_POSITION_POINT;
    } else {
      bo->point[ GOTE ] += y * FU_POSITION_POINT;
    }
  }
#endif /* USE_ADD_FU_ADVANCE_POINT */
#ifdef USE_DEC_KEI_KYO_POINT
  if ( (p & 0xF) == KYO ) {
    if ( next == SENTE ) {
      bo->point[ SENTE ] -= (10-y) * 5;
    } else {
      bo->point[ GOTE ] -= y * 5;
    }
  } else if ( (p & 0xF) == KEI ) {
    /* 桂馬は上に動く程点数を減らす */
    if ( next == SENTE ) {
      bo->point[ SENTE ] -= (10-y) * 5;
    } else {
      bo->point[ GOTE ] -= y * 5;
    }
  }
#endif /* USE_DEC_KEI_KYO_POINT */

  /* 飛車、角の場合は位置によるポイント増減はしない */
  /* if ( ((p & HISYA) != HISYA) || ((p & KAKU) == KAKU) ) { */
  if ( ((p & 0xF) != HISYA) && ((p & 0xF) == KAKU) ) {
    v = abs(kx - x);
    if ( next == SENTE )
      w = ky - y;
    else 
      w = y - ky;

    /* bo->point[ next ] += arr_point_xy2[ 8 - w ][ v ]; */
    bo->point[ next ] += piece_p * ( arr_point_xy2[ 8 - w ][ v ] - 100 ) / MY_OH_DISTANCE_POINT;

    v = abs(jx - x);
    if ( next == SENTE )
      w = jy - y;
    else 
      w = y - jy;

    /* bo->point[ next ] += arr_point_xy[ 8 - w ][ v ]; */
    bo->point[ next ] += piece_p * ( arr_point_xy[ 8 - w ][ v ] - 100 ) / ONES_OH_DISTANCE_POINT;
  }
  
  if ( (p & 0xF) == HISYA && getTeban(p) == next ) {
    /* 「王飛接近するべからず」の格言通り王と飛車が近い場合は減点する */
    int hx, hy;
    
    hx = abs(kx - x);
    hy = abs(ky - y);
    if ( 4 < hx + hy ) {
      bo->point[ next ] -= ((hx + hy) * OH_HISYA_POINT);
    }
  }

  /* 駒の動ける数を評価に加える */
  mi = boGetCodeTo(n);
  bo->point[ next ] += mi->count << NUM_OF_MOVE_POINT;
}

/**
 * 盤面評価点数から xy の位置の駒の点数を引く。
 * @param bo 対象のBOARD
 * @param xy 対象の駒の位置
 * @param next 盤面評価点数を増減する手番
 */
void sub_grade(BOARD *bo, int xy, int next ) {
  MOVEINFO *mi;
  int king_xy;
  int kx, ky;
  int jx, jy;
  int x, y;
  int v, w;
  int p, piece_p, n;
  extern const int arr_point[];
  extern const int arr_point_xy[17][9];
  extern const int arr_point_xy2[17][9];
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( bo->board[xy] != 0 );
  assert( bo->board[xy] != WALL );
  assert( getTeban(bo->board[xy]) == next );
  assert( next == 0 || next == 1 );
#endif /* DEBUG */

  /* 自玉の位置 */
  king_xy = bo->code_xy[1 + next][0];
  kx = king_xy & 0xF;
  ky = king_xy >> 4;
  
#ifdef DEBUG
  assert( bo->board[king_xy] == (0x08 + next * 0x10) );
#endif /* DEBUG */
  
  /* p = bo->board[ xy ]; */
  p = boGetPiece(bo, xy);
  if ( p == 0 || getTeban( p ) != next || p == OH || p == (OH | 0x10))
    return;
  
  n = boGetNoBoard(bo, xy);

  /* 相手玉の位置 */
  king_xy = bo->code_xy[1 + (next==0)][0];
  jx = king_xy & 0xF;
  jy = king_xy >> 4;

  x = xy & 0xF;
  y = xy >> 4;

  piece_p = arr_point[ p ];
  bo->point[ next ] -= piece_p;

  /* 歩は上へ行く程ポイントが高い */
#ifdef USE_ADD_FU_ADVANCE_POINT
  if ( (p & 0xF) == FU ) {
    if ( next == SENTE ) {
      bo->point[ SENTE ] -= (10-y) * FU_POSITION_POINT;
    } else {
      bo->point[ GOTE ] -= y * FU_POSITION_POINT;
    }
  }
#endif /* USE_ADD_FU_ADVANCE_POINT */
#ifdef USE_DEC_KEI_KYO_POINT
  if ( (p & 0xF) == KYO ) {
    if ( next == SENTE ) {
      bo->point[ SENTE ] += (10-y) * 5;
    } else {
      bo->point[ GOTE ] += y * 5;
    }
  } else if ( (p & 0xF) == KEI ) {
    /* 桂馬は上に動く程点数を減らす */
    if ( next == SENTE ) {
      bo->point[ SENTE ] += (10-y) * 5;
    } else {
      bo->point[ GOTE ] += y * 5;
    }
  }
#endif /* USE_DEC_KEI_KYO_POINT */

  /* 飛車、角の場合は位置によるポイント増減はしない */
  /* if ( ((p & HISYA) != HISYA) || ((p & KAKU) == KAKU) ) { */
  if ( ((p & 0xF) != HISYA) && ((p & 0xF) == KAKU) ) {
    v = abs(kx - x);
    if ( next == SENTE )
      w = ky - y;
    else 
      w = y - ky;

    /* bo->point[ next ] -= arr_point_xy2[ 8 - w ][ v ]; */
    bo->point[ next ] -= piece_p * ( arr_point_xy2[ 8 - w ][ v ] - 100 ) / MY_OH_DISTANCE_POINT;

    v = abs(jx - x);
    if ( next == SENTE )
      w = jy - y;
    else 
      w = y - jy;

    /* bo->point[ next ] -= arr_point_xy[ 8 - w ][ v ]; */
    bo->point[ next ] -= piece_p * ( arr_point_xy[ 8 - w ][ v ] - 100 ) / ONES_OH_DISTANCE_POINT;
  }
  
  if ( (p & 0xF) == HISYA && getTeban(p) == next ) {
    /* 「王飛接近するべからず」の格言通り王と飛車が近い場合は減点する */
    int hx, hy;
    
    hx = abs(kx - x);
    hy = abs(ky - y);
    if ( 4 < hx + hy ) {
      bo->point[ next ] += ((hx + hy) * OH_HISYA_POINT);
    }
  }

  /* 駒の動ける数を評価に加える */
  mi = boGetCodeTo(n);
  bo->point[ next ] -= mi->count << NUM_OF_MOVE_POINT;
}

/**
 * 盤面評価点数に取った駒 piece の得点を加える。
 * @param bo 対象のBOARD
 * @param piece 取った駒
 * @param next 盤面評価点数を増減する手番
 */
void add_piece_grade(BOARD *bo, int piece, int next ) {
  extern const int arr_piece[];
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( 1 <= piece && piece <=7 );
  assert( next == 0 || next == 1 );
#endif /* DEBUG */

  bo->point[ next ] += arr_piece[ piece ];
}

/**
 * 盤面評価点数から取った駒 piece の得点を引く。
 * @param bo 対象のBOARD
 * @param piece 取った駒
 * @param next 盤面評価点数を増減する手番
 */
void sub_piece_grade(BOARD *bo, int piece, int next ) {
  extern const int arr_piece[];
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( 1 <= piece && piece <=7 );
  assert( next == 0 || next == 1 );
#endif /* DEBUG */

  bo->point[ next ] -= arr_piece[ piece ];
}

/**
 * 王の周りの安全度を評価する。
 * 
 * ・王の周り８マスに相手の駒の効きの数×１０点マイナス
 * ・自分の飛車が自陣内にいるときに王と近い場合はマイナス
 * 
 * @param bo 盤面
 * @param next 効きを探す手番
 */
int oh_safe_grade(BOARD *bo, int king_xy, int next) {
  extern const int arr_round_to[8];
  int point;
  int i;

  point = 0;
  
  for ( i=0; i<8; i++ ) {
    point += boGetToBoardCount(king_xy + arr_round_to[ i ], next);
  }

  return point * OH_SAFE_POINT;
}

/**
 * 王の安全度の評価点数を盤面評価に足す。
 * @param bo 対象のBOARD
 * @param next 評価する手番
 */
void add_oh_safe_grade(BOARD *bo, int next) {
  int king_xy;
  
  /* 自玉の位置 */
  king_xy = bo->code_xy[1 + next][0];

  bo->point[ next ] += oh_safe_grade(bo, king_xy, (next == 0));
}

/**
 * 王の安全度の評価点数を盤面評価から引く。 
 * @param bo 対象のBOARD
 * @param next 評価する手番
 */
void sub_oh_safe_grade(BOARD *bo, int next) {
  int king_xy;
  
  /* 自玉の位置 */
  king_xy = bo->code_xy[1 + next][0];

  bo->point[ next ] -= oh_safe_grade(bo, king_xy, (next == 0));
}

/**
 * 駒の動きの評価値を加点する。
 * @param bo 対象のBOARD
 * @param xy 評価する駒の位置
 * @param next 評価する手番
 */
void add_moving_grade(BOARD *bo, int xy, int next) {
  MOVEINFO *mi;
  int n;
  
  n = boGetNoBoard(bo, xy);
  mi = boGetCodeTo(n);
  bo->point[ next ] += mi->count << 3;
}

/**
 * 駒の動きの評価値を減点する。
 * @param bo 対象のBOARD
 * @param xy 評価する駒の位置
 * @param next 評価する手番
 */
void sub_moving_grade(BOARD *bo, int xy, int next) {
  MOVEINFO *mi;
  int n;
  
  n = boGetNoBoard(bo, xy);
  mi = boGetCodeTo(n);
  bo->point[ next ] -= mi->count << 3;
}
