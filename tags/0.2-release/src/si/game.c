/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/**
 * game.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/game.c,v $
 * $Id: game.c,v 1.1.1.1 2005/12/09 09:03:08 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "si.h"
#include "ui.h"
#ifdef USE_SERIAL
#include "comaccess.h"
#endif /* USE_SERIAL */

/**
 * 初期化付きでGAMEを生成する。
 * @return GAMEのポインタ
 */
GAME *newGAME(void) {
  GAME *game;

  game = (GAME *) malloc(sizeof(GAME));
  if (game == NULL) {
    si_abort("No enough memory. In moveinfo.c#newGAME()");
  }

  initGAME(game);

  return game;
}

/**
 * game を初期化する。
 * @param game 対象のGAME
 */
void initGAME(GAME *gm) {
#ifdef DEBUG
  assert(gm != NULL);
#endif /* DEBUG */

  gm->teai = HIRATE;
  gm->player_number[SENTE] = SI_COMPUTER_3;
  gm->player_number[GOTE]  = SI_COMPUTER_3;
  gm->si_input_next[SENTE] = si_input_next_computer;
  gm->si_input_next[GOTE]  = si_input_next_computer;
  gm->computer_level[SENTE] = 5;
  gm->computer_level[GOTE]  = 5;
#ifdef USE_SERIAL
  initCOMACCESS(&(gm->ca[SENTE]));
  initCOMACCESS(&(gm->ca[GOTE]));
#endif /* USE_SERIAL */
  gm->flg_run_out_to_lost = 1;
  gm->game_status = 0;
  gm->chudan = NULL;
}

/**
 * game のメモリを解放する。
 * @param game 対象のGAME
 */
void freeGAME(GAME *game) {
#ifdef DEBUG
  assert(game != NULL);
#endif /* DEBUG */
  
  free( game );
}

/**
 * 次の手を入力する関数へのポインタを返す。
 * @param gm 対象のGAME
 * @param next 次の手番
 * @return 次の手を入力する関数へのポインタ
 */ 
INPUTSTATUS (*gmGetInputNextPlayer(GAME *gm, int next))(BOARD *, TE *) {
  return gm->si_input_next[next];
}

/**
 * シリアルポートのデバイルファイルの名前を返す。
 * @param gm 対象のGAME
 * @param next 次の手番
 * @return 次の手を入力する関数へのポインタ
 */ 
char *gmGetDeviceName(GAME *gm, int next) {
#ifdef USE_SERIAL
  return gm->ca[next].DEVICENAME;
#else
  return NULL;
#endif /* USE_SERIAL */  
}

/**
 * コンピューターのレベルを返す。
 * @param gm 対象のGAME
 * @param next 次の手番
 * @return コンピューターのレベル
 */ 
int gmGetComputerLevel(GAME *gm, int next) {
  return gm->computer_level[next];
}

/**
 * 対戦を行うプレイヤーを設定する。
 * ３つ目の引数には次のいずれかをセットする。
 * <pre>
 * 人間による入力
 * SI_HUMAN
 * プログラムレベル１による入力
 * SI_COMPUTER_1
 * プログラムレベル２による入力
 * SI_COMPUTER_2
 * プログラムレベル３による入力
 * SI_COMPUTER_3
 * プログラムレベル４による入力
 * SI_COMPUTER_4
 * プログラムレベル５による入力
 * SI_COMPUTER_5
 * シリアルポートからの入力１
 * SI_SERIALPORT_1
 * シリアルポートからの入力２
 * SI_SERIALPORT_2
 * </pre>
 * 
 * @param gm 対象GAME
 * @param next 設定する手番
 * @param p 設定するプレイヤー
 */ 
void gmSetInputFunc(GAME* gm, int next, PLAYERTYPE p) 
{
  assert(gm != NULL);
  assert(next == SENTE || next == GOTE); 
  assert( p == SI_HUMAN ||
	  p == SI_COMPUTER_1 ||
	  p == SI_COMPUTER_2 ||
	  p == SI_COMPUTER_3 ||
	  p == SI_COMPUTER_4 ||
	  p == SI_COMPUTER_5 ||
	  p == SI_SERIALPORT_1 ||
	  p == SI_SERIALPORT_2 ||
	  p == SI_NETWORK_1 ||
  //	  p == SI_NETWORK_2 ||
	  p == SI_GUI_HUMAN );

  gm->player_number[next] = p;
   
  if ( p == SI_HUMAN ) {
    gm->si_input_next[next] = si_input_next_human;
  } else if ( p == SI_COMPUTER_1 ) {
    gm->si_input_next[next] = si_input_next_computer;
    gm->computer_level[next] = 3;
  } else if ( p == SI_COMPUTER_2 ) {
    gm->si_input_next[next] = si_input_next_computer;
    gm->computer_level[next] = 4;
  } else if ( p == SI_COMPUTER_3 ) {
    gm->si_input_next[next] = si_input_next_computer;
    gm->computer_level[next] = 5;
  } else if ( p == SI_COMPUTER_4 ) {
    gm->si_input_next[next] = si_input_next_computer;
    gm->computer_level[next] = 6;
  } else if ( p == SI_COMPUTER_5 ) {
    gm->si_input_next[next] = si_input_next_computer;
    gm->computer_level[next] = 7;
  } else if ( p == SI_SERIALPORT_1 ) {
#ifdef USE_SERIAL
    gm->si_input_next[next] = si_input_next_serialport;
#endif /* USE_SERIAL */
  } else if ( p == SI_SERIALPORT_2 ) {
#ifdef USE_SERIAL
    gm->si_input_next[next] = si_input_next_serialport;
#endif /* USE_SERIAL */
  } 
  else if (p == SI_NETWORK_1)
    gm->si_input_next[next] = si_input_next_network;
  else if ( p == SI_GUI_HUMAN )
    gm->si_input_next[next] = si_input_next_gui_human;
  else {
    assert(0);
  }
}

/**
 * デバイス名をセットする。
 * @param gm 対象のGAME
 * @param next セットする手番 
 * @param str デバイス名
 */
void gmSetDeviceName(GAME *gm, int next, char *s) {
#ifdef USE_SERIAL
  caSetDEVICENAME(&(gm->ca[next]), s);
#endif /* USE_SERIAL */
}

/**
 * ゲームのステータスをセットする。
 * @param gm 対象のGAME
 * @param status ゲームのステータス
 */
void gmSetGameStatus(GAME *gm, int status) {
#ifdef DEBUG
  assert(gm != NULL);
  assert(status == 0 || status == SI_CHUDAN || status == SI_TORYO);
#endif /* DEBUG */
  gm->game_status = status;
}

/**
 * ゲームのステータスを返す。
 * @param gm 対象のGAME
 * @return ゲームのステータス
 */
INPUTSTATUS gmGetGameStatus(GAME* gm) 
{
#ifdef DEBUG
  assert(gm != NULL);
  assert(gm->game_status == 0 ||
	 gm->game_status == SI_CHUDAN ||
	 gm->game_status == SI_TORYO);
#endif /* DEBUG */
  return gm->game_status;
}

/**
 * 中断フラグへのポインタをセットする。
 * @param gm 対象のGAME
 * @param chudan 中断フラグへのポインタ
 */ 
void gmSetChudan(GAME *gm, int *chudan) {
  gm->chudan = chudan;
}

/**
 * 中断フラグへのポインタを返す。
 * @param gm 対象のGAME
 * @return 中断フラグへのポインタ
 */ 
int *gmGetChudan(GAME *gm) {
  return gm->chudan;
}

/**
 * 中断フラグの値を返す。
 * @param gm 対象のGAME
 * @return 中断フラグの値
 */ 
int gmGetChudanFlg(GAME *gm) {
  if (gm->chudan == NULL) {
    return 0;
  }
  
  return *gm->chudan;
}
