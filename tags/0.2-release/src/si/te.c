/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/**
 * te.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/te.c,v $
 * $Id: te.c,v 1.1.1.1 2005/12/09 09:03:10 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"

/**
 * csa shogi の座標を si の座標に変換するテーブル。
 */ 
static const int csa2si_table[] = {
  0,
  0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
  0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
  0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
  0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49,
  0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59,
  0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69,
  0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79,
  0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
  0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99,
};

/**
 * si の座標を csa shogi の座標に変換するテーブル。
 */ 
static const int si2csa_table[] = {
   0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,
   0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 0, 0, 0, 0, 0, 0,
   0, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0, 0, 0, 0, 0, 0,
   0, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 0, 0, 0, 0, 0,
   0, 28, 29, 30, 31, 32, 33, 34, 35, 36, 0, 0, 0, 0, 0, 0,
   0, 37, 38, 39, 40, 41, 42, 43, 44, 45, 0, 0, 0, 0, 0, 0,
   0, 46, 47, 48, 49, 50, 51, 52, 53, 54, 0, 0, 0, 0, 0, 0,
   0, 55, 56, 57, 58, 59, 60, 61, 62, 63, 0, 0, 0, 0, 0, 0,
   0, 64, 65, 66, 67, 68, 69, 70, 71, 72, 0, 0, 0, 0, 0, 0,
   0, 73, 74, 75, 76, 77, 78, 79, 80, 81, 0, 0, 0, 0, 0, 0,
};

/**
 * 初期化付きでTEを生成する。
 * @param x 移動元のX座標
 * @param y 移動元のY座標
 * @param v 移動先のX座標
 * @param w 移動先のY座標
 * @param nari 成りフラグ。0 なら成らず、1 なら成り。
 * @return TEのポインタ
 */
TE *newTE( int x, int y, int v, int w, int nari ) {
  TE *te;

#ifdef DEBUG
  assert(1 <= x && x <= 9 && 1 <= y && y <= 9);
  assert(1 <= v && v <= 9 && 1 <= w && w <= 9);
  assert(nari == 0 || nari == 1);
#endif /* DEBUG */

  te = (TE *) malloc(sizeof(TE));
  if (te == NULL) {
    si_abort("No enough memory. In moveinfo.c#newTE()");
  }

  te->fm = (y << 4) + x;
  te->to = (w << 4) + v;
  te->nari = nari;
  te->uti = 0;

  return te;
}

/**
 * 手を比較し同じならば 0 を返す。違いがあれば非 0 を返す。
 * @param te1 比較する手１
 * @param te2 比較する手２
 * @return 同じならば 0 、違いがあれば非 0 を返す
 */
#ifndef USE_MISC_MACRO
int teCmpTE( TE *te1, TE * te2 ) {
#ifdef DEBUG
  assert( te1 != NULL );
  assert( te2 != NULL );
#endif /* DEBUG */  
  
  if ( te1->fm == te2->fm &&
      te1->to == te2->to &&
      te1->nari == te2->nari &&
      te1->uti == te2->uti ) {
    return 0;
  }
  
  return 1;
}
#endif /* USE_MISC_MACRO */

/**
 * 手をセットする。
 * @param te 手を格納する te
 * @param fm 移動元
 * @param to 移動先
 * @param nari 成り
 * @param uti 打ちの場合駒種類
 */
void teSetTE(TE *te, int fm, int to, int nari, int uti) {
#ifdef DEBUG
  assert(te != NULL);
  assert(0 <= fm && fm <= 10);
  assert(0 <= to && to <= 10);
  assert(nari == 0 || nari == 1);
  assert(0 <= uti && uti <= 7);
#endif /* DEBUG */

  te->fm = fm;
  te->to = to;
  te->nari = nari;
  te->uti = uti;
}

/**
 * te の移動元を CSA shogi の座標に変換して返す。
 * @param te 
 * @return CSAshogi の移動元の座標を返す。
 */ 
int teCSAimoto(TE *te) {
  if (te->uti) {
    /* 駒打ちの場合 */
    return te->uti + 100;
  }
  return si2csa_table[te->fm];
}

/**
 * te の移動先を CSA shogi の座標に変換して返す。
 * @param te 
 * @return CSAshogi の移動先の座標を返す。
 */
int teCSAisaki(TE *te) {
  return si2csa_table[te->to];
}

/**
 * te が成りの場合は 1 を返す。
 * それ以外は 0 を返す。
 * すでに成っている駒の移動は 0 を返す。
 * @param te 
 * @return 成りならば 0 を返す。
 */
int teCSAinaru(TE *te) {
  return te->nari;
}

/**
 * imoto, isaki の内容を解析して te をセットする。
 * @param te 対象のTE
 * @param imoto CSAshogi の移動元
 * @param isaki CSAshogi の移動先
 */
void teCSASet(TE *te, int imoto, int isaki) {
  if (101 <= imoto) {
    /* 駒打ちの場合 */
    te->uti = imoto - 100;
    te->to = csa2si_table[isaki];
    te->fm = 0;
    te->nari = 0;
  } else {
    te->uti = 0;
    te->fm = csa2si_table[imoto];
    if (100 < isaki) {
      /* 成りの場合 */
      te->nari = 1;
      isaki -= 100;
    } else {
      te->nari = 0;
    }
    te->to = csa2si_table[isaki];
  }
}
