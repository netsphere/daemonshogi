/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * moveinfo.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/moveinfo.c,v $
 * $Id: moveinfo.c,v 1.1.1.1 2005/12/09 09:03:09 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "si.h"

/**
 * 新しくメモリを確保してMOVEINFOのポインタを返す。
 * @return MOVEINFOのポインタ
 */
MOVEINFO *newMOVEINFO(void) {
  MOVEINFO *mi;
  mi = (MOVEINFO *) malloc(sizeof(MOVEINFO));
  if (mi == NULL) {
    si_abort("No enough memory. In moveinfo.c#newMOVEFINO()");
  }
  mi->count = 0;

  return mi;
}

/**
 * mi のメモリを解放する。
 * @param mi 対象のMOVEINFO
 */
void freeMOVEINFO(MOVEINFO *mi) {
#ifdef DEBUG
  assert(mi != NULL);
#endif /* DEBUG */
  
  free( mi );
} 

#ifndef USE_MISC_MACRO
/**
 * MOVEINFO に TE を追加する。
 * @param mi 対象のmi
 * @param te 追加するte
 */
void miAdd(MOVEINFO * mi, TE * te) {
#ifdef DEBUG
  assert(mi != NULL);
  assert(te != NULL);

  if ( te->uti ) {
    assert(te->fm == 0);
    assert(FU <= te->uti && te->uti <= HISYA);
  } else {
    assert(1 <= (te->fm & 0xf) && (te->fm & 0xf) <= 9);
    assert(1 <= (te->fm >> 4) && (te->fm >> 4) <= 9);
  }
  assert(1 <= (te->to & 0xf) && (te->to & 0xf) <= 9);
  assert(1 <= (te->to >> 4) && (te->to >> 4) <= 9);

  /* out of bounds check */
  assert(mi->count < MOVEINFOMAX);
#endif /* DEBUG */
  
  if ( MOVEINFOMAX <= mi->count )
    return;

  mi->te[mi->count++] = *te;
}
#endif /* USE_MISC_MACRO */

/**
 * MOVEINFO に TE を追加する。成れる場合は成りも記録する。
 * @param mi 対象のmi
 * @param te 追加するte
 * @param next 手番
 */
void miAddWithNari(MOVEINFO * mi, TE * te, int next) {
#ifdef NOREC_NARAZU
  int p;
  extern BOARD g_board;
#endif /* NOREC_NARAZU */
#ifdef DEBUG
  assert(mi != NULL);
  assert(te != NULL);
  assert(1 <= (te->fm >> 4) && (te->fm >> 4) <= 9);
  assert(1 <= (te->fm & 0xF) && (te->fm & 0xF) <= 9);
  assert(1 <= (te->to >> 4) && (te->to >> 4) <= 9);
  assert(1 <= (te->to & 0xF) && (te->to & 0xF) <= 9);
  assert(next == 0 || next == 1);
#endif /* DEBUG */

#ifdef NOREC_NARAZU
  p = boGetPiece(&g_board, te->fm) & 0xF;
  if ( next == GOTE ) {
    if ( (p == FU || p == KAKU || p == HISYA) ) {
      if ( 0x70 <= te->fm || 0x70 <= te->to ) {
	te->nari = 1;
	miAdd(mi, te);
      } else {
	te->nari = 0;
	miAdd(mi, te);
      }
    } else {
      /* 歩、角、飛車以外 */
      if ( 0x70 <= te->fm || 0x70 <= te->to ) {
	te->nari = 1;
	miAdd(mi, te);
      }
      te->nari = 0;
      miAdd(mi, te);
    }
  } else {
    if ( (p == FU || p == KAKU || p == HISYA) ) {
      if ( te->fm <= 0x39 || te->to <= 0x39 ) {
	te->nari = 1;
	miAdd(mi, te);
      } else {
	te->nari = 0;
	miAdd(mi, te);
      }
    } else {
      /* 歩、角、飛車以外 */
      if ( te->fm <= 0x39 || te->to <= 0x39 ) {
	te->nari = 1;
	miAdd(mi, te);
      }
      te->nari = 0;
      miAdd(mi, te);
    }
  }
#else
  if (next) { /* gote */
    if (0x70 <= te->fm || 0x70 <= te->to) {
      te->nari = 1;
      miAdd(mi, te);
    }
  } else { /* sente */
    if (te->fm <= 0x39 || te->to <= 0x39) {
      te->nari = 1;
      miAdd(mi, te);
    }
  }
  te->nari = 0;
  miAdd(mi, te);
#endif /* NOREC_NARAZU */
}

/**
 * MOVEINFO に TE を追加する。成れる場合は成りも記録する。先手用。
 * @param mi 対象のmi
 * @param te 追加するte
 */
void miAddWithNariSente(MOVEINFO * mi, TE * te) {
#ifdef DEBUG
  assert(mi != NULL);
  assert(te != NULL);
#endif /* DEBUG */

  if (te->fm <= 0x39 || te->to <= 0x39) {
    te->nari = 1;
    miAdd(mi, te);
  }

  te->nari = 0;
  miAdd(mi, te);
}

/**
 * MOVEINFO に TE を追加する。成れる場合は成りも記録する。後手用。
 * @param mi 対象のmi
 * @param te 追加するte
 */
void miAddWithNariGote(MOVEINFO * mi, TE * te) {
#ifdef DEBUG
  assert(mi != NULL);
  assert(te != NULL);
#endif /* DEBUG */

  if (0x70 <= te->fm || 0x70 <= te->to) {
    te->nari = 1;
    miAdd(mi, te);
  }
    
  te->nari = 0;
  miAdd(mi, te);
}

/**
 * MOVEINFO に成りを TE を加える。
 * 不成らずができない場合は記録しない。
 * 
 * @param mi MOVEINFO
 * @param te 登録する te
 * @param p 登録する te の駒種類
 * @param next 次の手番
 */
void miAddWithNari2(MOVEINFO * mi, TE te, int p, int next) {
  int xy;
  int w;
  
#ifdef DEBUG
  assert(mi != NULL);
  assert(1 <= (te.fm >> 4) && (te.fm >> 4) <= 9);
  assert(1 <= (te.fm & 0xF) && (te.fm & 0xF) <= 9);
  assert(1 <= (te.to >> 4) && (te.to >> 4) <= 9);
  assert(1 <= (te.to & 0xF) && (te.to & 0xF) <= 9);
  assert(next == SENTE || next == GOTE);
  assert(p != 0 && p != WALL);
#endif /* DEBUG */

  xy = te.to;
  w = (xy >> 4);

  /** 金、王、成り駒はこのまま miAdd() する */
  if ( p == KIN || p == OH || (p & 8) ) {
    miAdd(mi, &te);
    return;
  }

  if ( p == FU || p == KYO ) {
    if ( next ) {
      if ( w == 9 ) {
	te.nari = 1;
	miAdd(mi, &te);
      } else {
	miAddWithNari(mi, &te, next);
      }
    } else {
      if ( w == 1 ) {
	te.nari = 1;
	miAdd(mi, &te);
      } else {
	miAddWithNari(mi, &te, next);
      }
    }
  } else if ( p == KEI ) {
    if ( next == GOTE ) {
      if ( 8 <= w ) {
	te.nari = 1;
	miAdd(mi, &te);
      } else {
	miAddWithNari(mi, &te, next);
      }
    } else {
      if ( w <= 2 ) {
	te.nari = 1;
	miAdd(mi, &te);
      } else {
	miAddWithNari(mi, &te, next);
      }
    }
  } else {
    miAddWithNari(mi, &te, next);
  }
}

/**
 * mi に格納されている index 番目の te へのポインタを返す。
 * @param mi 対象のMOVEINFO
 * @param index ポインタを取り出す番号
 * @return teへのポインタ
 */
#ifndef USE_MISC_MACRO
TE *miGet(MOVEINFO *mi, int index) {
#ifdef DEBUG
  assert(mi != NULL);
  assert(0 <= index && index < mi->count); 
#endif /* DEBUG */
  return &(mi->te[index]);
}
#endif /* USE_MISC_MACRO */

/**
 * miからteを削除して間が空いたら詰める。
 * @param mi 対象のMOVEINFO
 * @param te 対象のTE
 */
void miRemoveTE(MOVEINFO *mi, TE *te ) {
  int i;

#ifdef DEBUG
  assert( mi != NULL );
  assert( te != NULL );
#endif /* DEBUG */
  
  for (i=0; i<mi->count; i++) {
    if ( mi->te[ i ].fm == te->fm &&
	mi->te[ i ].to == te->to &&
	mi->te[ i ].nari == te->nari &&
	mi->te[ i ].uti == te->uti ) {
      if ( mi->count - 1 == i ) {
	mi->count--;
	return;
      }
      
      mi->te[ i ] = mi->te[ mi->count - 1 ];
      mi->count--;
      return;
    }
  }
}

/**
 * 手の番号を指定してmiからteを削除して間が空いたら詰める。
 * @param mi 対象のMOVEINFO
 * @param num 削除する手の番号
 */
void miRemoveTE2(MOVEINFO *mi, int num ) {
#ifdef DEBUG
  assert( mi != NULL );
  assert( 0 <= num && num <= mi->count );
#endif /* DEBUG */
  
  if ( mi->count - 1 == num ) {
    mi->count--;
    return;
  }
      
  mi->te[ num ] = mi->te[ mi->count - 1 ];
  mi->count--;
  return;
}

/**
 * miから重複する手を削除する。削除するときは先に登録された手を残す。
 * @param mi 対象のMOVEINFO
 */ 
void miRemoveDuplicationTE(MOVEINFO *mi) {
  int i, j;

#ifdef DEBUG
  assert( mi != NULL );
#endif /* DEBUG */

  for ( i=0; i<mi->count; i++ ) {
    for ( j=i+1; j<mi->count; ) {
      if ( ! teCmpTE( &(mi->te[ i ]), &(mi->te[ j ]) ) ) {
	miRemoveTE2( mi, j );
      } else {
	j++;
      }
    }
  }
}

/**
 * src の内容を dst へコピーする。
 * @param src コピー元
 * @param dst コピー先
 */
void miCopy(MOVEINFO *dest, MOVEINFO *src) {
#ifdef DEBUG
  assert(dest != NULL);
  assert(src != NULL);
  assert(0 <= src->count && src->count <= MOVEINFOMAX);
#endif /* DEBUG */
  memcpy(dest, src, sizeof(MOVEINFO));
}

