/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * comaccess.h
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/comaccess.h,v $
 * $Id: comaccess.h,v 1.1.1.1 2005/12/09 09:03:07 tokita Exp $
 */

#include "si.h"

#ifdef USE_SERIAL

#ifndef _COMACCESS_H_
#define _COMACCESS_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <termios.h>

/** ボーレートを 1200 baud に設定する */
#define BAUDRATE B1200
#define _POSIX_SOURCE 1  /* POSIX 準拠のソース */

/** デバイスファイル名の最大文字数 */
#define DEVICENAMESIZE 1024

/** COMポートを使った通信対局のための構造体 */
typedef struct {
  /** デバイスファイルのファイル・ディスクリプター */
  int fd;
  /** デバイスファイル名 */
  char DEVICENAME[DEVICENAMESIZE];
  /** バックアップ用のターミナル設定 */
  struct termios oldtio;
  /** ターミナル設定 */
  struct termios newtio;
} COMACCESS;

COMACCESS *newCOMACCESS(void);
void freeCOMACCESS(COMACCESS *ca);
void initCOMACCESS(COMACCESS *ca);
void caSetDEVICENAME(COMACCESS *ca, char *str);
void caStart(COMACCESS *ca);
void caClose(COMACCESS *ca);
int caAccept(COMACCESS *ca, char *buf);
int caSend(COMACCESS *ca, char *str);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _COMACCESS_H_ */

#endif /* USE_SERIAL */

