/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * search.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/search.c,v $
 * $Id: search.c,v 1.1.1.1 2005/12/09 09:03:10 tokita Exp $
 */

#include <stdlib.h>
#include <assert.h>

#include "si.h"

/** 
 * 直前に動いた相手の駒を取る手を探す。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 */
void search_011(BOARD *bo, MOVEINFO *mi) {
  static TE te;
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  if ( bo->mi.count == 0 )
    return;

  /* 直前の１手 */
  te = bo->mi.te[ bo->mi.count - 1 ];
  
  moveto2_add(bo, mi, te.to, bo->next);
}

/**
 * 駒を取る手を探す。
 * @param bo 対象のBOARD
 * @param mi 結果を記録するMOVEINFO
 */
void search_012(BOARD *bo, MOVEINFO *mi) {
  extern int arr_xy[];
  int xy;
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  for (xy=1; xy<=81; xy++) {
#ifdef DEBUG
    assert( bo->board[ arr_xy[ xy ] ] != WALL );
#endif /* DEBUG */
    if ( bo->board[ arr_xy[ xy ] ] == 0 )
      continue;
    if ( getTeban(bo->board[ arr_xy[ xy ] ]) == bo->next )
      continue;
    moveto2_add( bo, mi, arr_xy[ xy ], bo->next);
  }
}

/**
 * 有効王手。
 * (駒損しない王手)
 * @param bo 対象のBOARD
 * @param mi 結果を記録するMOVEINFO
 */
void search_013(BOARD *bo, MOVEINFO *mi) {
  static MOVEINFO mi1;
  int i;

#ifdef DEBUG
    assert( bo != NULL );
    assert( mi != NULL );
#endif /* DEBUG */
  
  ohte( bo, &mi1 );

  for ( i=0; i<mi1.count; i++ ) {
    miAdd( mi, &(mi1.te[ i ]) );
  }
}

/**
 * 取られる最大の駒が逃げる手。
 */
void search_014(BOARD *bo, MOVEINFO *mi) {
  extern int arr_xy[];
  extern int arr_point[];
  int xy;
  int max, max_xy;
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  max = 0;
  max_xy = 0;
  
  for (xy=1; xy<=81; xy++) {
    if ( bo->board[ arr_xy[ xy ] ] == 0 )
      continue;
#ifdef DEBUG
    assert( bo->board[ arr_xy[ xy ] ] != WALL );
#endif /* DEBUG */
    if ( getTeban(bo->board[ arr_xy[ xy ] ]) != bo->next )
      continue;
    if (boGetToBoardCount(arr_xy[ xy ], (bo->next == 0)))
      continue;

    if ( max < arr_point[ (bo->board[ arr_xy[ xy ] ] & 0x0F) ] ) {
      max = arr_point[ (bo->board[ arr_xy[ xy ] ] & 0x0F) ];
      max_xy = arr_xy[ xy ];
    }
  }
  
  if ( max == 0 )
    return;

  moveto_add( bo, max_xy, mi);
}

/**
 * 駒当たり(両取り含む)。 (022)
 */
void search_022(BOARD *bo, MOVEINFO *mi) {
  extern int arr_xy[82];
  int xy;
  int p;
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  for (xy=1; xy<=81; xy++) {
    p = boGetPiece(bo, arr_xy[xy]);
#ifdef DEBUG
    assert( p != WALL );
#endif /* DEBUG */    
    if ( p == 0 )
      continue;

    if ( bo->next != getTeban(p) )
      continue;

    /*
    remoteOhte(bo, mi, arr_xy[xy]);
    UtiOhte2(bo, mi, arr_xy[xy]);
    akiOhte(bo, mi, arr_xy[xy]);
    utiOhte(bo, mi, arr_xy[xy]);
    keiOhte(bo, mi, arr_xy[xy]);
    */
    normalOhte(bo, mi, arr_xy[xy]);
  }
}

/**
 * 空き王手。
 */
void search_023(BOARD *bo, MOVEINFO *mi) {
  /* ohte()内に含まれる */
}

/**
 * 相手玉に駒を近づける手。 
 * @param bo 対象のBOARD
 * @param mi 結果を格納するmi
 */
void search_024a(BOARD *bo, MOVEINFO *mi) {
  int king_xy;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  king_xy = bo->code_xy[1 + bo->next][0];

  search_pressure(bo, mi, king_xy, bo->next);
}

/**
 * 飛車、角を敵陣へ打ち込む手を検索する。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するmi
 */
void search_027(BOARD *bo, MOVEINFO *mi) {
  static TE te;
  int xy;
  extern const int arr_sentejin[];
  extern const int arr_gotejin[];
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  if ( bo->next ) {
    /* gote */
    if ( bo->piece[ 1 ][ 7 ] == 0 && bo->piece[ 1 ][ 6 ] == 0 )
      return;
    
    te.fm = 0;
    te.nari = 0;
    
    for (xy=1; xy<=27; xy++) {
      if ( bo->board[ arr_sentejin[ xy ] ] != 0 )
	continue;
      
      te.to = arr_sentejin[ xy ];
      
      if ( bo->piece[ 1 ][ 7 ] ) {
	te.uti = 7;
	miAdd( mi, &te );
      }
      if ( bo->piece[ 1 ][ 6 ] ) {
	te.uti = 6;
	miAdd( mi, &te );
      }
    }
  } else {
    /* sente */
    if ( bo->piece[ 0 ][ 7 ] == 0 && bo->piece[ 0 ][ 6 ] == 0 )
      return;

    te.fm = 0;
    te.nari = 0;
    
    for (xy=1; xy<=27; xy++) {
      if ( bo->board[ arr_gotejin[ xy ] ] != 0 )
	continue;
      
      te.to = arr_gotejin[ xy ];
      
      if ( bo->piece[ 0 ][ 7 ] ) {
	te.uti = 7;
	miAdd( mi, &te );
      }
      if ( bo->piece[ 0 ][ 6 ] ) {
	te.uti = 6;
	miAdd( mi, &te );
      }
    }
  }
}

/**
 * 相手の王周辺に駒を打ち込む手。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するmi
 */
void search_031a(BOARD *bo, MOVEINFO *mi) {
  int king_xy;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  king_xy = bo->code_xy[1 + (bo->next == 0)][0];

  search_pressure_uti(bo, mi, king_xy, bo->next);
}

/**
 * 自分の王周辺に駒を打ち込む手。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するmi
 */
void search_031b(BOARD *bo, MOVEINFO *mi) {
  int king_xy;


#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  king_xy = bo->code_xy[1 + bo->next][0];

  search_pressure_uti(bo, mi, king_xy, bo->next);
}

/**
 * 飛車、香車の先の歩を突く。または歩を打ち込む。
 * @param bo 対象BOARD
 * @param mi 結果を格納するMOVEINFO
 */ 
void search_041(BOARD *bo, MOVEINFO *mi) {
  TE te;
  int xy, y, p, n;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  for ( n=3; n<=4; n++ ) {
    xy = bo->code_xy[ n ][ 0 ];
    if ( xy == 0 ) {
      continue;
    }
    p = bo->code_xy[ n ][ 1 ];
    if (getTeban(p) != bo->next) {
      continue;
    }
    
    if ( bo->next == SENTE ) {
      /* sente */
      y = checkFuSente( bo, xy & 0x0F );
      if ( y == 0 ) {
	/* 歩打ち */
	if ( bo->piece[SENTE][FU] ) {
	  int i;
	  for (i = (xy & 0xF) + 0x20; i<=xy-0x10; i+=0x10) {
	    if (boGetPiece(bo, i)) {
	      continue;
	    }

	    te.fm = 0;
	    te.to = i;
	    te.nari = 0;
	    te.uti = FU;
	    
	    miAdd(mi, &te);
	  }
	}
	continue;
      }
      xy = (xy & 0x0F ) + (y << 4);
      p = bo->board[ xy - 0x10 ];
      if ( p == WALL )
	continue;
      if ( ! notSentePiece(p) )
	continue;
      
      te.fm = xy;
      te.to = xy - 0x10;
      te.nari = 0;
      te.uti = 0;

      miAdd( mi, &te );
    } else {
      /* gote */
      y = checkFuGote( bo, xy & 0x0F );
      if ( y == 0 ) {
	/* 歩打ち */
	if ( bo->piece[GOTE][FU] ) {
	  int i;
	  for (i = (xy & 0xF) + 0x80; xy + 0x10 <= i; i-=0x10) {
	    if (boGetPiece(bo, i)) {
	      continue;
	    }

	    te.fm = 0;
	    te.to = i;
	    te.nari = 0;
	    te.uti = FU;
	  
	    miAdd(mi, &te);
	  }
	}
	continue;
      }
      xy = (xy & 0x0F ) + (y << 4);
      p = bo->board[ xy + 0x10 ];
      if ( p == WALL )
	continue;
      if ( ! notGotePiece(p) )
	continue;
      
      te.fm = xy;
      te.to = xy + 0x10;
      te.nari = 0;
      te.uti = 0;
      
      miAdd( mi, &te );
    }
  }

  for ( n=19; n<=22; n++ ) {
    xy = bo->code_xy[ n ][ 0 ];
    if ( xy == 0 )
      continue;
    p = bo->code_xy[ n ][ 1 ];
    
    if ( bo->next == SENTE ) {
      /* sente */
      y = checkFuSente( bo, xy & 0x0F );
      if ( y == 0 )
	continue;
      xy = (xy & 0x0F ) + (y << 4);
      p = bo->board[ xy - 0x10 ];
      if ( p == WALL )
	continue;
      if ( ! notSentePiece(p) )
	continue;
      
      te.fm = xy;
      te.to = xy - 0x10;
      te.nari = 0;
      te.uti = 0;

      miAdd( mi, &te );
    } else {
      /* gote */
      y = checkFuGote( bo, xy & 0x0F );
      if ( y == 0 )
	continue;
      xy = (xy & 0x0F ) + (y << 4);
      p = bo->board[ xy + 0x10 ];
      if ( p == WALL )
	continue;
      if ( ! notGotePiece(p) )
	continue;
      
      te.fm = xy;
      te.to = xy + 0x10;
      te.nari = 0;
      te.uti = 0;
      
      miAdd( mi, &te );
    }
  }
}

/**
 * 歩を垂らす手。
 * @param bo 対象BOARD
 * @param mi 結果を格納するMOVEINFO
 */ 
void search_044(BOARD *bo, MOVEINFO *mi) {
  TE te;
  int xy;
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  if ( bo->next == SENTE ) {
    /* sente */
    
    if ( bo->piece[ SENTE ][ 1 ] == 0 )
      return;
    
    for ( xy=0x41; xy <= 0x49; xy++ ) {
      if ( bo->board[ xy ] )
	continue;

      if ( checkFuSente( bo, xy & 0x0F ) )
	continue;
      
      if ( ! notSentePiece(bo->board[ xy - 0x10 ]) )
	continue;
      
      te.fm = 0;
      te.to = xy;
      te.nari = 0;
      te.uti = 1;
      
      miAdd( mi, &te );
    }
  } else {
    /* gote */
    
    if ( bo->piece[ GOTE ][ 1 ] == 0 )
      return;
    
    for ( xy=0x61; xy <= 0x69; xy++ ) {
      if ( bo->board[ xy ] )
	continue;
      
      if ( checkFuGote( bo, xy & 0x0F ) )
	continue;
      
      if ( ! notGotePiece(bo->board[ xy - 0x10 ]) )
	continue;
      
      te.fm = 0;
      te.to = xy;
      te.nari = 0;
      te.uti = 1;
      
      miAdd( mi, &te );
    }
  }
}

/**
 * 銀金が動く手
 * (敵陣へ金類が寄っていく手。 (046))
 * @param bo 対象BOARD
 * @param mi 結果を格納するMOVEINFO
 */ 
void search_046(BOARD *bo, MOVEINFO *mi) {
  int p, xy, n;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  for ( n=7; n<=14; n++ ) {
    xy = bo->code_xy[ n ][ 0 ];
    if ( xy == 0 ) {
      continue;
    }
    p = boGetPiece(bo, xy);
    if ( p == 0 || p != bo->code_xy[n][1] ) {
      continue;
    }
    
    if ( getTeban(p) != bo->next )
      continue;

    moveto_add(bo, xy, mi);
  }  
}

/**
 * 直前に指した駒への歩打ち。
 * @param bo 対象BOARD
 * @param mi 結果を格納するMOVEINFO
 */ 
void search_047(BOARD *bo, MOVEINFO *mi) {
  static TE te;
  int xy;
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  if ( bo->mi.count == 0 )
    return;

  /* 直前の１手 */
  te = bo->mi.te[ bo->mi.count - 1 ];

  if ( bo->next == SENTE ) {
    /* sente */
    if ( bo->piece[ SENTE ][ 1 ] == 0 )
      return;
    xy = te.to + 0x10;
    if ( checkFuSente( bo, xy & 0xF ) )
      return;
  } else {
    /* gote */
    if ( bo->piece[ GOTE ][ 1 ] == 0 )
      return;
    xy = te.to - 0x10;
    if ( checkFuGote( bo, xy & 0xF ) )
      return;
  }
  
  if ( bo->board[ xy ] != 0 ) {
    return;
  }
  
  te.fm = 0;
  te.to = xy;
  te.nari = 0;
  te.uti = 1;
  
  miAdd( mi, &te );
}

/**
 * 直前に動いてた駒に対する当たり。
 * @param bo 対象BOARD
 * @param mi 結果を格納するMOVEINFO
 */ 
void search_048(BOARD *bo, MOVEINFO *mi) {
  TE te;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  if ( bo->mi.count == 0 )
    return;

  /* 直前の１手 */
  te = bo->mi.te[ bo->mi.count - 1 ];

  search_pressure(bo, mi, te.to, bo->next);
}

/**
 * 王が動く手。
 * @param bo 対象BOARD
 * @param mi 結果を格納するMOVEINFO
 */ 
void search_049(BOARD *bo, MOVEINFO *mi) {
  extern const int arr_round_to[8];
  int king_xy;
  int xy;
  int piece;
  int i;
  TE te;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  king_xy = bo->code_xy[1 + bo->next][0];

  te.fm = king_xy;
  te.nari = 0;
  te.uti = 0;

  for (i=0 ;i<8; i++) {
    xy = king_xy + arr_round_to[i];
    piece = boGetPiece(bo, xy);

    if ( piece == WALL ) {
      continue;
    }
    if ( 0 < boGetToBoardCount(xy, (bo->next == 0)) ) {
      continue;
    }
    if ( piece != 0 && getTeban(piece) == bo->next ) {
      continue;
    }
    te.to = xy;
    miAdd(mi, &te);
  }
}

/**
 * でたらめに盤上の駒が動く手を検索する。
 * @param bo 対象BOARD
 * @param mi 結果を格納するMOVEINFO
 */ 
void search_053(BOARD *bo, MOVEINFO *mi) {
  extern int arr_xy[82];
  int p;
  int xy;
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  for ( xy = 1; xy<=81; xy++ ) {
    p = bo->board[ arr_xy[ xy ] ];
    if ( p == 0 )
      continue;
    if ( p == WALL )
      continue;
    if ( getTeban( p ) != bo->next )
      continue;
    
    moveto_add( bo, arr_xy[ xy ], mi );
  }
}

/**
 * でたらめに駒を打つ手を調べる。 
 * @param bo 対象BOARD
 * @param mi 結果を格納するMOVEINFO
 */ 
void search_068(BOARD *bo, MOVEINFO *mi) {
  extern int arr_xy[82];
  static TE te;
  int xy, i;
  int y;
  
#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

  te.fm = 0;
  te.nari = 0;

  for ( xy=1; xy<=81; xy++ ) {
    te.to = arr_xy[ xy ];
    
    if ( bo->board[ te.to ] != 0 ) {
      continue;
    }
    if ( boGetToBoardCount( te.to, (bo->next == 0) ) ) {
      /* 相手の駒の効きがあるところには打たない */
      continue;
    }
    
    y = (te.to >> 4);
    
    for ( i=1; i<=7; i++ ) {
      if ( bo->piece[ bo->next ][ i ] ) {
	if ( i == FU ) {
	  if ( bo->next == SENTE ) {
	    if ( y == 0 || checkFuSente(bo, arr_xy[ xy ] & 0x0F ) )
	      continue;
	  } else {
	    if ( y == 9 || checkFuGote(bo, arr_xy[ xy ] & 0x0F ) )
	      continue;
	  }
	} else if ( i == KYO ) {
	  if ( bo->next == SENTE ) {
	    if ( y == 0 )
	      continue;
	  } else {
	    if ( y == 9 )
	      continue;
	  }
	} else if ( i == KEI ) {
	  if ( bo->next == SENTE ) {
	    if ( y < 3 )
	      continue;
	  } else {
	    if ( 7 < y )
	      continue;
	  }
	}
	te.uti = i;
	miAdd( mi, &te );
      }
    }
  }
}

/**
 * xy の駒の周り２マス以内に動く bo->next 手番の手を探す。
 * @param bo 対象BOARD
 * @param mi 結果を格納するMOVEINFO
 * @param next 駒を動かす方の手番
 */ 
void search_pressure(BOARD *bo, MOVEINFO *mi, int to_xy, int next) {
  extern const int arr_rount_to24[24];
  int xy;
  int p;
  int i;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
  assert( 1 <= (to_xy >> 4) && (to_xy >> 4) <= 9);
  assert( 1 <= (to_xy & 0xF) && (to_xy & 0xF) <= 9);
#endif /* DEBUG */

  for (i=0; i<24; i++) {
    xy = to_xy + arr_rount_to24[i];

    if ( (xy & 0xF) < 0 || 10 < (xy & 0xF)) {
      continue;
    }
    if ( (xy >> 4) < 0 || 10 < (xy >> 4)) {
      continue;
    }

    p = boGetPiece(bo, xy);
    if ( p == WALL ) {
      continue;
    }

    moveto2_add(bo, mi, xy, next);
  }
}

/**
 * to_xy の周辺に駒を打ち込む手。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するmi
 * @param to_xy 目標の座標
 * @param next 駒を打つ方の手番
 */
void search_pressure_uti(BOARD *bo, MOVEINFO *mi, int to_xy, int next) {
  extern const int arr_rount_to24[24];
  TE te;
  int xy;
  int i, p;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
  assert( 1 <= (to_xy >> 4) && (to_xy >> 4) <= 9);
  assert( 1 <= (to_xy & 0xF) && (to_xy & 0xF) <= 9);
#endif /* DEBUG */

  te.fm = 0;
  te.nari = 0;

  for (i=0; i<24; i++) {
    xy = to_xy + arr_rount_to24[i];
    
    if ( xy < 0x11 || 0x99 < xy ) {
      continue;
    }

    if ( (xy >> 4) < 1 || 9 < (xy >> 4) ) { 
      continue;
    }

    if ( (xy & 0xF) < 1 || 9 < (xy & 0xF) ) {
      continue;
    }

    if ( boGetPiece(bo, xy) != 0 ) {
      continue;
    }

    te.to = xy;

    for ( p=1; p<=7; p++ ) {
      if ( bo->piece[ next ][ p ] ) {
	if ( p == FU ) {
	  if ( next == SENTE ) {
	    if ( checkFuSente(bo, xy & 0x0F ) )
	      continue;
	  } else {
	    if ( checkFuGote(bo, xy & 0x0F ) )
	      continue;
	  }
	} else if ( p == KYO ) {
	  if ( next == SENTE ) {
	    if ( (xy >> 4) <= 1 ) {
	      continue;
	    }
	  } else {
	    if ( 9 <= (xy >> 4) ) {
	      continue;
	    }
	  }
	} else if ( p == KEI ) {
	  if ( next == SENTE ) {
	    if ( (xy >> 4) <= 2 ) {
	      continue;
	    }
	  } else {
	    if ( 8 <= (xy >> 4) ) {
	      continue;
	    }
	  }
	}
	te.uti = p;
	miAdd( mi, &te );
      }
    }
  }
}

