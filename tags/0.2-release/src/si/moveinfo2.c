/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * moveinfo2.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/moveinfo2.c,v $
 * $Id: moveinfo2.c,v 1.1.1.1 2005/12/09 09:03:09 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "si.h"

/**
 * 新しくメモリを確保してMOVEINFOのポインタを返す。
 * @return MOVEINFO2のポインタ
 */
MOVEINFO2 *newMOVEINFO2(void) {
  MOVEINFO2 *mi;
  mi = (MOVEINFO2 *) malloc(sizeof(MOVEINFO2));
  if (mi == NULL) {
    si_abort("No enough memory. In moveinfo2.c#newMOVEFINO2()");
  }
  mi->count = 0;

  return mi;
}

/**
 * mi のメモリを解放する。
 * @param mi 対象のMOVEINFO2
 */
void freeMOVEINFO2(MOVEINFO2 *mi) {
#ifdef DEBUG
  assert(mi != NULL);
#endif /* DEBUG */
  
  free( mi );
} 

#ifndef USE_MISC_MACRO
/**
 * MOVEINFO2 に TE を追加する。
 * @param mi 対象のmi
 * @param te 追加するte
 */
void mi2Add(MOVEINFO2 * mi, TE * te) {
#ifdef DEBUG
  assert(mi != NULL);
  assert(te != NULL);
  
  /* out of bounds check */
  /* assert(mi->count < MOVEINFO2MAX); */
#endif /* DEBUG */
  
  if ( MOVEINFO2MAX <= mi->count )
    return;

  mi->te[mi->count++] = *te;
}
#endif /* USE_MISC_MACRO */

/**
 * src の内容を dst へコピーする。
 * @param src コピー元
 * @param dst コピー先
 */
void mi2Copy(MOVEINFO2 *dest, MOVEINFO2 *src) {
#ifdef DEBUG
  assert(dest != NULL);
  assert(src != NULL);
  assert(0 <= src->count && src->count <= MOVEINFO2MAX);
#endif /* DEBUG */
  memcpy(dest, src, sizeof(MOVEINFO2));
}

