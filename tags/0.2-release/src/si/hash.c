/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * hash.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/hash.c,v $
 * $Id: hash.c,v 1.3 2005/12/09 11:51:03 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "si.h"
#include "ui.h"

static const unsigned long MEM_SIZE= (2 * 1024 * 1024);
unsigned long HASH_MAX = 0x1FFFF;
#include "hashval.h"
static void *hash;

/**
 * ハッシュテーブル用にメモリを確保する。
 */
void newHASH(void) {
  hash = (void *)malloc( sizeof(HASH) * (HASH_MAX + 1));
  if (hash == NULL) {
    si_abort("No enough memory. In hash.c#newHASH()\n");
  }

  initHASH();
}

/**
 * ハッシュテーブルを初期化する。
 */
void initHASH(void) {
  memset( (void *)hash, 0, sizeof(HASH) * HASH_MAX );
}

/** 
 * ハッシュテーブルを解放する。
 */
void freeHASH(void) {
  free(hash);
}

/**
 * BOARD からハッシュ値を生成する。
 * @param bo 対象のBOARD
 * @return int ハッシュ値(24bit)
 */
si_int64 hsGetHashKey(BOARD *bo) {
  si_int64 key;
  extern const int arr_xy[];
  int i;

#ifdef DEBUG
  assert( bo != NULL );
#endif /* DEBUG */

  key = 0;

  if ( bo->next == SENTE ) {
    for ( i=1; i<=81; i++ ) {
      key ^= hashval[bo->board[ arr_xy[ i ] ]][ arr_xy[ i ] ];
    }
  } else {
    /* 後手番の場合のハッシュキーはビット反転する */
    for ( i=1; i<=81; i++ ) {
      key ^= ( ~ hashval[bo->board[ arr_xy[ i ] ]][ arr_xy[ i ] ] );
    }
  }

  return key;
}

/**
 * BOARDをハッシュテーブルに登録する。
 * @param bo 対象のBOARD
 * @param mate_stat 盤面の評価点数
 * @param count 手数
 * @param r_count 相対手数
 */
void hsPutBOARD(BOARD *bo, int mate_stat, int count, int r_count) {
  HASH *hs;
  si_int64 key;
  si_int64 offset;
  extern long count_entry_hash;
  extern long count_crash2;

#ifdef DEBUG
  assert( bo != NULL );
#endif /* DEBUG */

  key = bo->key;

  offset = key & HASH_MAX;

  hs = (HASH *)((char *)hash + (sizeof(HASH) * offset));

  if ((count + 1) < hs->count && hs->key == key) {
    /* 同じ key が登録されていた */
    count_crash2++;

    if ( hsSuperiorBOARD(hs, bo) == 0 ) {
      /* より少ない持駒で詰んでいる */
      /* hs の持駒を再設定 */
      hsSetPiece(hs, bo);
    }
    
    return;
  }

  count_entry_hash++;

  hs->key = key;
  hs->mate_stat = mate_stat;
  hsSetPiece( hs, bo );
  hs->count = count;
}

/**
 * 盤面の point を取りだす。
 * @param bo 対象のBOARD
 * @param p pointを格納する変数のポインタ
 * @param count 手数
 * @param r_count 相対手数
 * @return 盤面が見つかったら 1 、見つからなかったら 0 を返す。
 */
int hsGetBOARD(BOARD *bo, int *mate_stat, int count, int r_count) {
  HASH *hs;
  si_int64 key;
  si_int64 offset;
  extern long G_COUNT_HASH_CHECK_MATE;
  /* extern long G_COUNT_HASH_NOT_CHECK_MATE; */

#ifdef DEBUG
  assert( bo != NULL );
#endif /* DEBUG */

  key = bo->key;
  offset = key & HASH_MAX;
  hs = (HASH *)((char *)hash + (sizeof(HASH) * offset));

  if (key != hs->key) {
    /* 違う盤面であった */
    return 0;
  }
  
  if ( hs->mate_stat == CHECK_MATE && count <= hs->count ) {
    /* 盤面の優越関係を調べる */
    if ( hsSuperiorBOARD(hs, bo) ) {
      *mate_stat = CHECK_MATE;
      G_COUNT_HASH_CHECK_MATE++;
      return 1;
    }
  }
  /*
    else if ( hs->point == NOT_CHECK_MATE && count <= hs->count ) {
    *mate_stat = NOT_CHECK_MATE;
    G_COUNT_HASH_NOT_CHECK_MATE++;
    return 1;
  }
  */
  else if ( hs->point == UNKNOWN_CHECK_MATE ) {
    /* 不明 */
    *mate_stat = UNKNOWN_CHECK_MATE;
    return 1;
  }

  /* 見付からなかった */
  return 0;
}

/**
 * min/max検索用にBOARDをハッシュテーブルに登録する。
 * @param bo 対象のBOARD
 * @param mate_stat 盤面の評価点数
 * @param count 手数
 * @param r_count 相対手数
 */
void hsPutBOARDMinMax(BOARD *bo, int point, int count, int r_count) {
  HASH *hs;
  si_int64 key;
  si_int64 offset;
  extern long count_entry_hash;

#ifdef DEBUG
  assert( bo != NULL );
#endif /* DEBUG */

  key = bo->key;
  offset = key & HASH_MAX;

  hs = (HASH *)((char *)hash + (sizeof(HASH) * offset));

  if ( ! ( hs->count <= count + 1 ) ) {
    /* 同じ key が登録されていた */
    
    /* 同じ盤面か？ */
    if (key == hs->key) {
      /* 違う盤面だったので戻る。本当はここで再ハッシュする。 */
      return;
    }
    
    return;
  }

  count_entry_hash++;

  /*
  if ( point == 778 ) {
    putBOARD(bo);
  }
  */

  hs->key = key;
  hs->point = point;
  hsSetPiece( hs, bo );
  hs->count = count;
  /* hs->flguse = 1; */
}

/**
 * 盤面の point を取りだす。min/max検索用。
 * @param bo 対象のBOARD
 * @param p pointを格納する変数のポインタ
 * @param count 手数
 * @param r_count 相対手数
 * @return 盤面が見つかったら 1 、見つからなかったら 0 を返す。
 */
int hsGetBOARDMinMax(BOARD *bo, int *point, int count, int r_count) {
  HASH *hs;
  si_int64 key;
  si_int64 offset;

#ifdef DEBUG
  assert( bo != NULL );
#endif /* DEBUG */

  key = bo->key;
  offset = key & HASH_MAX;
  hs = (HASH *)((char *)hash + (sizeof(HASH) * offset));

  /*
  if ( hs->flguse == 0 )
    return 0;
  */
  
  if ( count <= hs->count ) {
    if ( bo->key != key ) {
      /* 登録されていた盤面と相違があった */
      return 0;
    }

    if ( hsCmpPiece(hs, bo, bo->next) ) {
      /* 登録されていた盤面と相違があった */
      /* printf("not same board2.\n"); */
      return 0;
    }

    /*
    if ( hs->point == 778 ) {
      printf("hsGetBOARDMinMax()\n");
      putBOARD(bo);
    }
     */ 
    
    *point = hs->point;

    return 1;
  }

  /* 見付からなかった */
  return 0;
}

/**
 * HASHに持駒情報をセットする。
 * @param hs 対象のHASH
 * @param bo 対象のBOARD
 */
void hsSetPiece(HASH *hs, BOARD *bo) {
  hs->piece[ SENTE ] =
    bo->piece[ SENTE ][ 1 ] +
    (bo->piece[ SENTE ][ 2 ] << 5) +
    (bo->piece[ SENTE ][ 3 ] << 8) +
    (bo->piece[ SENTE ][ 4 ] << 11) +
    (bo->piece[ SENTE ][ 5 ] << 14) +
    (bo->piece[ SENTE ][ 6 ] << 17) +
    (bo->piece[ SENTE ][ 7 ] << 19);
  hs->piece[ GOTE ] =
    bo->piece[ GOTE ][ 1 ] +
    (bo->piece[ GOTE ][ 2 ] << 5) +
    (bo->piece[ GOTE ][ 3 ] << 8) +
    (bo->piece[ GOTE ][ 4 ] << 11) +
    (bo->piece[ GOTE ][ 5 ] << 14) +
    (bo->piece[ GOTE ][ 6 ] << 17) +
    (bo->piece[ GOTE ][ 7 ] << 19);
}

/**
 * 盤面の優越関係を調べる。
 * hs の持駒が bo の持駒の部分集合だったら 1 を返す。それ以外は 0 を返す。
 * hs の盤面で詰むと分かっていて且つ bo と同じ盤面且つ hs の持駒が
 * bo の持駒の部分集合だった場合 bo は詰む。
 * @param hs 対象のHASH
 * @param bo 対象のBOARD
 * @return bo の持駒が hs の持駒の部分集合だったら 1 を返す。
 *         それ以外は 0 を返す。
 */
int hsSuperiorBOARD(HASH *hs, BOARD *bo) {
  int next = bo->next;
  int p = hs->piece[ next ];

  if ( bo->piece[ next ][ FU ] < (p & 0x1f)) {
    /* 歩の場合 */
    return 0;
  }
  if ( bo->piece[ next ][ KYO ] < ((p >> 5) & 0x7)) {
    /* 香の場合 */
    return 0;
  }
  if ( bo->piece[ next ][ KEI ] < ((p >> 8) & 0x7)) {
    /* 桂の場合 */
    return 0;
  }
  if ( bo->piece[ next ][ GIN ] < ((p >> 11) & 0x7)) {
    /* 銀の場合 */
    return 0;
  }
  if ( bo->piece[ next ][ GIN ] < ((p >> 14) & 0x7)) {
    /* 金の場合 */
    return 0;
  }
  if ( bo->piece[ next ][ KAKU ] < ((p >> 17) & 0x3)) {
    /* 角の場合 */
    return 0;
  }
  if ( bo->piece[ next ][ HISYA ] < ((p >> 19) & 0x3)) {
    /* 飛の場合 */
    return 0;
  }
  
  return 1;
}

/**
 * 指定した手番の持駒を比較する。
 * 同じなら 0 を返す。相違がある場合は 1 を返す。
 * @param hs 対象のHASH
 * @param bo 対象のBOARD
 * @param next 手番
 * @return 持駒が同じなら 0 を返す。相違がある場合は 1 を返す。
 */
int hsCmpPiece(HASH *hs, BOARD *bo, int next) {
  int p = hs->piece[ next ];

  if ( bo->piece[ next ][ FU ] < (p & 0x1f)) {
    /* 歩の場合 */
    return 0;
  }
  if ( bo->piece[ next ][ KYO ] < ((p >> 5) & 0x7)) {
    /* 香の場合 */
    return 0;
  }
  if ( bo->piece[ next ][ KEI ] < ((p >> 8) & 0x7)) {
    /* 桂の場合 */
    return 0;
  }
  if ( bo->piece[ next ][ GIN ] < ((p >> 11) & 0x7)) {
    /* 銀の場合 */
    return 0;
  }
  if ( bo->piece[ next ][ GIN ] < ((p >> 14) & 0x7)) {
    /* 金の場合 */
    return 0;
  }
  if ( bo->piece[ next ][ KAKU ] < ((p >> 17) & 0x3)) {
    /* 角の場合 */
    return 0;
  }
  if ( bo->piece[ next ][ HISYA ] < ((p >> 19) & 0x3)) {
    /* 飛の場合 */
    return 0;
  }
  
  return 1;
}

/**
 * XYの位置のハッシュ値を足す。
 * @param bo 対象のBOARD
 * @param xy 駒の位置
 */
void add_hash_value(BOARD *bo, int xy) {
  int p;
  
#ifdef DEBUG
  assert(bo != NULL);
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( 1 <= (xy & 0xF) && (xy & 0xF) <= 9 );
#endif /* DEBUG */
  
  p = boGetPiece(bo, xy);

  if ( bo->next == SENTE ) {
    bo->key ^= hashval[ p ][ xy ];
  } else {
    bo->key ^= ( ~ hashval[ p ][ xy ] );
  }
}

/**
 * XYの位置のハッシュ値を外す。
 * @param bo 対象のBOARD
 * @param xy 駒の位置
 */
void sub_hash_value(BOARD *bo, int xy) {
  int p;
  
#ifdef DEBUG
  assert(bo != NULL);
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( 1 <= (xy & 0xF) && (xy & 0xF) <= 9 );
#endif /* DEBUG */
  
  p = boGetPiece(bo, xy);
  
  if ( bo->next == SENTE ) {
    bo->key ^= hashval[ p ][ xy ];
  } else {
    bo->key ^= ( ~ hashval[ p ][ xy ] );
  }
}
