/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * ohte.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/ohte.c,v $
 * $Id: ohte.c,v 1.1.1.1 2005/12/09 09:03:10 tokita Exp $
 */

#include <stdio.h>
#include <assert.h>
#include "si.h"
#include "ui.h"

/**
 * 王手を検索して mi に格納する。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 */
void ohte(BOARD *bo, MOVEINFO *mi) {
  int king_xy;
  
  mi->count = 0;

  if (bo->next) {
    /* gote */
    king_xy = bo->code_xy[1][0];
  } else {
    /* sente */
    king_xy = bo->code_xy[2][0];
  }
  
  remoteOhte(bo, mi, king_xy);
  UtiOhte2(bo, mi, king_xy);
  akiOhte(bo, mi, king_xy);
  utiOhte(bo, mi, king_xy);
  keiOhte(bo, mi, king_xy);
  normalOhte(bo, mi, king_xy);
}

/** 
 * 歩、香車の成り、桂馬、銀、金、角の成り、飛車の成り、
 * と、成り香、成り桂、成り銀、馬の縦横、竜の斜めでの王手を検索する。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 */
void normalOhte(BOARD *bo, MOVEINFO *mi, int king_xy) {
  static const int can_ohte[8][32][2] = {
      {
	/* -16 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 1, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* -17 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymmy, fu, kyo, kei, */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymny, to, narikyo, narikei */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* -1 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* +15 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymmy, fu, kyo, kei, */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymny, to, narikyo, narikei */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* +16 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 1, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* +17 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymmy, fu, kyo, kei, */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymny, to, narikyo, narikei */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* +1 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* -15 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymmy, fu, kyo, kei, */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymny, to, narikyo, narikei */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
  };
  static const int can_ohte_to[8] = {
    -16, -17, -1, 15, 16, 17, 1, -15,
  };
  static TE te;
  static MOVEINFO mi1;
  int xy, fm;
  int p, n;
  int i, j;
  
  for (i = 0; i<8; i++) {
    xy = king_xy + can_ohte_to[ i ];
    if ( xy < 0x11 )
      continue;
    
    if ( bo->board[xy] == WALL )
      continue;
    
    if ( bo->board[xy] != 0 && ( getTeban(bo->board[xy]) == bo->next ) )
      continue;
    
    moveto2(bo, &mi1, xy, bo->next);
    
    for (j=0; j<mi1.count; j++ ) {
      if ( mi1.te[j].nari ) {
	if (j+1<mi1.count && mi1.te[j].to == mi1.te[j+1].to) {
	  continue;
	}
      }
      fm = mi1.te[j].fm;
      p = bo->board[ fm ];
      n = bo->noBoard[ fm ];
      if ( p == WALL )
	continue;
      if ( can_ohte[i][p][0] ) {
	te.fm = fm;
	te.to = xy;
	te.nari = 0;
	te.uti = 0;
	miAdd( mi, &te );
      }
      if ( can_ohte[i][p][1] && (
	  ( getTeban(p) == 0 && ( fm <= 0x39 || xy <= 0x39 ) ) ||
 	  ( getTeban(p) == 1 && ( 0x71 <= fm || 0x71 <= xy ) ) )
	  ) {
	te.fm = fm;
	te.to = xy;
	te.nari = 1;
	te.uti = 0;
	miAdd( mi, &te );
      }
    }
  }
}

/**
 * 桂の王手を探す。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 */
void keiOhte(BOARD *bo, MOVEINFO *mi, int xy) {
  static TE te;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */
  
  if ( bo->next ) {
    /* gote */

    if ( bo->board[xy - 0x40] == 0x13 &&
	bo->board[xy - 0x21] != WALL &&
	notGotePiece(bo->board[xy - 0x21]) ) {
      te.fm = xy - 0x40;
      te.to = xy - 0x21;
      te.nari = 0;
      te.uti = 0;
      miAdd(mi, &te);
    }
    if ( bo->board[xy - 0x40] == 0x13 &&
	bo->board[xy - 0x1F] != WALL &&
	notGotePiece(bo->board[xy - 0x1F]) ) {
      te.fm = xy - 0x40;
      te.to = xy - 0x1F;
      te.nari = 0;
      te.uti = 0;
      miAdd(mi, &te);
    }
    if ( bo->board[xy - 0x3E] == 0x13 &&
	bo->board[xy - 0x1F] != WALL &&
	notGotePiece(bo->board[xy - 0x1F]) ) {
      te.fm = xy - 0x3E;
      te.to = xy - 0x1f;
      te.nari = 0;
      te.uti = 0;
      miAdd(mi, &te);
    }
    if ( bo->board[xy - 0x42] == 0x13 &&
	bo->board[xy - 0x21] != WALL &&
	notGotePiece(bo->board[xy - 0x21]) ) {
      te.fm = xy - 0x42;
      te.to = xy - 0x21;
      te.nari = 0;
      te.uti = 0;
      miAdd(mi, &te);
    }
  } else {
    /* sente */
    
    if ( bo->board[xy + 0x40] == 0x03 &&
	bo->board[xy + 0x21] != WALL &&
	notSentePiece(bo->board[xy + 0x21]) ) {
      te.fm = xy + 0x40;
      te.to = xy + 0x21;
      te.nari = 0;
      te.uti = 0;
      miAdd(mi, &te);
    }
    if ( bo->board[xy + 0x40] == 0x03 &&
	bo->board[xy + 0x1F] != WALL &&
	notSentePiece(bo->board[xy + 0x1F])) {
      te.fm = xy + 0x40;
      te.to = xy + 0x1F;
      te.nari = 0;
      te.uti = 0;
      miAdd(mi, &te);
    }
    if ( bo->board[xy + 0x3E] == 0x03 &&
	bo->board[xy + 0x1F] != WALL &&
	notSentePiece(bo->board[xy + 0x1F])) {
      te.fm = xy + 0x3E;
      te.to = xy + 0x1F;
      te.nari = 0;
      te.uti = 0;
      miAdd(mi, &te);
    }
    if ( bo->board[xy + 0x42] == 0x03 &&
	bo->board[xy + 0x21] != WALL &&
	notSentePiece(bo->board[xy + 0x21])) {
      te.fm = xy + 0x42;
      te.to = xy + 0x21;
      te.nari = 0;
      te.uti = 0;
      miAdd(mi, &te);
    }
  }
}

/**
 * 遠くに効く駒(飛車、角、香)の王手を探す。<br>
 * かなり読みにくいコードになっている。
 * @param bo 検索するBOARD
 * @param mi 王手を格納するMOVEINFO
 */
void remoteOhte(BOARD *bo, MOVEINFO *mi, int king_xy) {
  static TE te;
  int i, j;
  int n, p;
  int next;
  extern int toBoard3[2][256][toBoardCountMax];
  extern int toBoardCount3[2][256];

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */
  
  next = bo->next;

  /* 右から左へ王手 */
  for (i=king_xy+1; 1; i++) {
    for (j=0; j<toBoardCount3[next][i]; j++) {
      n = toBoard3[next][i][j];
      if ( n == 3 || n == 4 ) {
	p = bo->code_xy[n][1];
	if ( bo->next ) {
	  if ( getTeban(p) && (bo->board[ i ] & 0x10) == 0 ) {
	    /* gote */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    te.uti = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, GOTE);
	  }
	} else { 
	  if ( getTeban(p) == 0 && notSentePiece(bo->board[ i ]) ) {
	    /* sente */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    te.uti = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, SENTE);
	  }
	}
      }
    }

    if ( bo->board[i] )
      break;
  }

  /* 左から右へ王手 */
  for (i=king_xy-1; 1; i--) {
    for (j=0; j<toBoardCount3[next][i]; j++) {
      n = toBoard3[next][i][j];
      if ( n == 3 || n == 4 ) {
	p = bo->code_xy[n][1];
	
	if ( bo->next ) {
	  if ( getTeban(p) && (bo->board[ i ] & 0x10) == 0 ) {
	    /* gote */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    te.uti = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, GOTE);
	  }
	} else { 
	  if ( getTeban(p) == 0 && notSentePiece(bo->board[ i ])) {
	    /* sente */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    te.uti = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, SENTE);
	  }
	}
      }
    }

    if ( bo->board[i] )
      break;
  }

  /* 下段から上段へ王手 */
  for (i=king_xy+0x10; 1; i+=0x10) {
    for (j=0; j<toBoardCount3[next][i]; j++) {
      n = toBoard3[next][i][j];
      if ( n == 3 || n == 4 || (bo->next == SENTE && 19 <= n && n <= 22 ) ) {
	p = bo->code_xy[n][1];
	if ( p == NARIKYO )
	  continue;
	if ( p == KYO && notSentePiece(bo->board[ i ]) ) {
	  te.fm = bo->code_xy[n][0];
	  te.to = i;
	  te.nari = 0;
	  te.uti = 0;
	  if ( king_xy + 0x10 < i )
	    miAdd(mi, &te);
	  else 
	    miAddWithNari(mi, &te, SENTE);
	  continue;
	}
	if ( bo->next ) {
	  if ( getTeban(p) && (bo->board[ i ] & 0x10) == 0 ) {
	    /* gote */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    te.uti = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, GOTE);
	  }
	} else { 
	  if ( getTeban(p) == 0 && notSentePiece(bo->board[ i ]) ) {
	    /* sente */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    te.uti = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, SENTE);
	  }
	}
      }
    }

    if ( bo->board[i] )
      break;
  }

  /* 上段から下段へ王手 */
  for (i=king_xy-0x10; 1; i-=0x10) {
    for (j=0; j<toBoardCount3[next][i]; j++) {
      n = toBoard3[next][i][j];
      if ( n == 3 || n == 4 || (bo->next == GOTE && 19 <= n && n <= 22 ) ) {
	p = bo->code_xy[n][1];
	if ( p == NARIKYO + 0x10 )
	  continue;
	if ( p == ( KYO + 0x10 ) && (bo->board[ i ] & 0x10) == 0 ) {
	  te.fm = bo->code_xy[n][0];
	  te.to = i;
	  te.nari = 0;
	  te.uti = 0;
	  if ( i < king_xy - 0x10 )
	    miAdd(mi, &te);
	  else 
	    miAddWithNari(mi, &te, SENTE);
	  continue;
	}
	if ( bo->next ) {
	  if ( getTeban(p) && (bo->board[ i ] & 0x10) == 0 ) {
	    /* gote */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    te.uti = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, GOTE);
	  }
	} else { 
	  if ( getTeban(p) == 0 && notSentePiece(bo->board[ i ]) ) {
	    /* sente */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    te.uti = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, SENTE);
	  }
	}
      }
    }

    if ( bo->board[i] )
      break;
  }

  /* 右斜め上へ王手 */
  for (i=king_xy+0x11; 1; i+=0x11) {
    for (j=0; j<toBoardCount3[next][i]; j++) {
      n = toBoard3[next][i][j];
      if ( n == 5 || n == 6 ) {
	p = bo->code_xy[n][1];
	if ( bo->next ) {
	  if ( getTeban(p) && (bo->board[ i ] & 0x10) == 0 ) {
	    /* gote */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, GOTE);
	  }
	} else { 
	  if ( getTeban(p) == 0 && notSentePiece(bo->board[ i ])) {
	    /* sente */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, SENTE);
	  }
	}
      }
    }

    if ( bo->board[i] )
      break;
  }

  /* 右斜め下へ王手 */
  for (i=king_xy-0xF; 1; i-=0xF) {
    for (j=0; j<toBoardCount3[next][i]; j++) {
      n = toBoard3[next][i][j];
      if ( n == 5 || n == 6 ) {
	p = bo->code_xy[n][1];
	if ( bo->next ) {
	  if ( getTeban(p) && (bo->board[ i ] & 0x10) == 0 ) {
	    /* gote */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, GOTE);
	  }
	} else { 
	  if ( getTeban(p) == 0 && notSentePiece(bo->board[ i ]) ) {
	    /* sente */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, SENTE);
	  }
	}
      }
    }

    if ( bo->board[i] )
      break;
  }

  /* 右斜め下へ王手 */
  for (i=king_xy-0x11; 1; i-=0x11) {
    for (j=0; j<toBoardCount3[next][i]; j++) {
      n = toBoard3[next][i][j];
      if ( n == 5 || n == 6 ) {
	p = bo->code_xy[n][1];
	if ( bo->next ) {
	  if ( getTeban(p) && (bo->board[ i ] & 0x10) == 0 ) {
	    /* gote */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, GOTE);
	  }
	} else { 
	  if ( getTeban(p) == 0 && notSentePiece(bo->board[ i ]) ) {
	    /* sente */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, SENTE);
	  }
	}
      }
    }

    if ( bo->board[i] )
      break;
  }

  /* 左斜め上へ王手 */
  for (i=king_xy+0xF; 1; i+=0xF) {
    for (j=0; j<toBoardCount3[next][i]; j++) {
      n = toBoard3[next][i][j];
      if ( n == 5 || n == 6 ) {
	p = bo->code_xy[n][1];
	if ( bo->next ) {
	  if ( getTeban(p) && (bo->board[ i ] & 0x10) == 0 ) {
	    /* gote */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, GOTE);
	  }
	} else {
	  if ( getTeban(p) == 0 && notSentePiece(bo->board[ i ]) ) {
	    /* sente */
	    te.fm = bo->code_xy[n][0];
	    te.to = i;
	    te.nari = 0;
	    if ( p & 0x08 )
	      miAdd(mi, &te);
	    else
	      miAddWithNari(mi, &te, SENTE);
	  }
	}
      }
    }

    if ( bo->board[i] )
      break;
  }
}

/**
 * 歩、圭、銀、金の打ち込んで王手を探す。
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 */
void utiOhte(BOARD *bo, MOVEINFO *mi, int king_xy) {
  /**
   * 8方向分打てる駒の種類。余計な計算をさけるために同じ値を先手用、
   * 後手用に用意している。
   * 1 が歩、4 が銀、5 金。
   */
  static int UtiPiece[16][4] = {
      /* sente */
      {1, 4, 5, 0,}, /* 下 */
      {4, 5, 0, 0,}, /* 左斜め下 */
      {5, 0, 0, 0,}, /* 左、、と続く */
      {4, 0, 0, 0,},
      {5, 0, 0, 0,},
      {4, 0, 0, 0,},
      {5, 0, 0, 0,},
      {4, 5, 0, 0,},

      /* gote */
      {1, 4, 5, 0,},
      {4, 5, 0, 0,},
      {5, 0, 0, 0,},
      {4, 0, 0, 0,},
      {5, 0, 0, 0,},
      {4, 0, 0, 0,},
      {5, 0, 0, 0,},
      {4, 5, 0, 0,},
  };
  /** 8 方向分のXYの増分と、先手用と後手用で2セット */
  static int d2xy[16][2] = {
      { 0,  1,}, /* 下 */
      { 1,  1,}, /* 左斜め下 */
      { 1,  0,}, /* 左、、と続く */
      { 1, -1,},
      { 0, -1,},
      {-1, -1,},
      {-1,  0,},
      {-1,  1,},
    
      { 0, -1,},
      {-1, -1,},
      {-1,  0,},
      {-1,  1,},
      { 0,  1,},
      { 1,  1,},
      { 1,  0,},
      { 1, -1,},
  };
  /** work用 TE */
  static TE te;
  int xy;
  /** 方向を表す */
  int d;
  /** work用 */
  int p;
  /** ただのカウンタ */
  int i;

  if ( bo->next ) {
    /* gote */
    if ( 0x31 <= king_xy && bo->board[king_xy - 0x21] == 0 && bo->piece[1][KEI] ) {
      te.fm = 0;
      te.to = king_xy - 0x21;
      te.nari = 0;
      te.uti = KEI;
      miAdd(mi, &te);
    }
    if ( 0x31 <= king_xy && bo->board[king_xy - 0x1F] == 0 && bo->piece[1][KEI] ) {
      te.fm = 0;
      te.to = king_xy - 0x1F;
      te.nari = 0;
      te.uti = KEI;
      miAdd(mi, &te);
    }

    for (d=8; d<16; d++) {
      xy = king_xy + (d2xy[d][1] << 4 ) + d2xy[d][0];

      if ( bo->board[xy] )
	continue;

      for (i=0; UtiPiece[d][i]; i++) {
	p = UtiPiece[d][i];
	if ( p == FU && checkFuGote(bo, king_xy & 0x0F) ) {
	  continue;
	}
	if (bo->piece[bo->next][p]) {
	  te.fm = 0;
	  te.to = xy;
	  te.nari = 0;
	  te.uti = p;
	  miAdd(mi, &te);
	}
      }
    }
  } else {
    /* sente */
    if ( king_xy <= 0x79 && bo->board[king_xy + 0x21] == 0 && bo->piece[0][3] ) {
      te.fm = 0;
      te.to = king_xy + 0x21;
      te.nari = 0;
      te.uti = KEI;
      miAdd(mi, &te);
    }
    if ( king_xy <= 0x79 && bo->board[king_xy + 0x1F] == 0 && bo->piece[0][3] ) {
      te.fm = 0;
      te.to = king_xy + 0x1F;
      te.nari = 0;
      te.uti = KEI;
      miAdd(mi, &te);
    }
    
    for (d=0; d<8; d++) {
      xy = king_xy + (d2xy[d][1] << 4 ) + d2xy[d][0];

      if ( bo->board[xy] )
	continue;

      for (i=0; UtiPiece[d][i]; i++) {
	p = UtiPiece[d][i];
	if ( p == FU && checkFuSente(bo, king_xy & 0x0F) ) {
	  continue;
	}
	if (bo->piece[bo->next][p]) {
	  te.fm = 0;
	  te.to = xy;
	  te.nari = 0;
	  te.uti = p;
	  miAdd(mi, &te);
	}
      }
    }
  }
}

/**
 * 遠くに効く駒(飛車、角、香)を打ち込んでの王手を探す。
 * @param bo 対象のBOARD
 * @param mi 結果を納めるMOVEINFO
 */
void UtiOhte2(BOARD *bo, MOVEINFO *mi, int king_xy) {
  int i;
  static TE te;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */
  
  /* kyo */
  if ( bo->next ) {
    /* gote */
    king_xy = bo->code_xy[1][0];

    if (bo->piece[1][KYO]) {
      for (i = king_xy - 0x10; bo->board[i] == 0; i -= 0x10) {
	te.fm = 0;
	te.to = i;
	te.nari = 0;
	te.uti = KYO;
	miAdd(mi, &te);
      }
    }
  } else {
    /* sente */
    king_xy = bo->code_xy[2][0];

    if (bo->piece[0][KYO]) {
      for (i = king_xy + 0x10; bo->board[i] == 0; i += 0x10) {
	te.fm = 0;
	te.to = i;
	te.nari = 0;
	te.uti = KYO;
	miAdd(mi, &te);
      }
    }
  }

  /* hisya */
  if (bo->piece[bo->next][HISYA]) {
    for (i = king_xy + 1; bo->board[i] == 0; i++) {
      te.fm = 0;
      te.to = i;
      te.nari = 0;
      te.uti = HISYA;
      miAdd(mi, &te);
    }
    for (i = king_xy - 1; bo->board[i] == 0; i--) {
      te.fm = 0;
      te.to = i;
      te.nari = 0;
      te.uti = HISYA;
      miAdd(mi, &te);
    }
    for (i = king_xy + 0x10; bo->board[i] == 0; i += 0x10) {
      te.fm = 0;
      te.to = i;
      te.nari = 0;
      te.uti = HISYA;
      miAdd(mi, &te);
    }
    for (i = king_xy - 0x10; bo->board[i] == 0; i -= 0x10) {
      te.fm = 0;
      te.to = i;
      te.nari = 0;
      te.uti = HISYA;
      miAdd(mi, &te);
    }
  }

  /* kaku */
  if (bo->piece[bo->next][KAKU]) {
    for (i = king_xy + 0x11; bo->board[i] == 0; i+=0x11) {
      te.fm = 0;
      te.to = i;
      te.nari = 0;
      te.uti = KAKU;
      miAdd(mi, &te);
    }
    for (i = king_xy + 0x0F; bo->board[i] == 0; i+=0x0F) {
      te.fm = 0;
      te.to = i;
      te.nari = 0;
      te.uti = KAKU;
      miAdd(mi, &te);
    }
    for (i = king_xy - 0x0F; bo->board[i] == 0; i -= 0x0F) {
      te.fm = 0;
      te.to = i;
      te.nari = 0;
      te.uti = KAKU;
      miAdd(mi, &te);
    }
    for (i = king_xy - 0x11; bo->board[i] == 0; i -= 0x11) {
      te.fm = 0;
      te.to = i;
      te.nari = 0;
      te.uti = KAKU;
      miAdd(mi, &te);
    }
  }
}

/**
 * 空き王手を探す。
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 */
void akiOhte(BOARD *bo, MOVEINFO *mi, int king_xy) {
  /** 8 方向分のXYの増分と、先手用と後手用で2セット */
  static int d2xy[16][2] = {
      { 0,  1,}, /* 下 */
      { 1,  1,}, /* 左斜め下 */
      { 1,  0,}, /* 左、、と続く */
      { 1, -1,},
      { 0, -1,},
      {-1, -1,},
      {-1,  0,},
      {-1,  1,},
    
      { 0, -1,},
      {-1, -1,},
      {-1,  0,},
      {-1,  1,},
      { 0,  1,},
      { 1,  1,},
      { 1,  0,},
      { 1, -1,},
  };
  /* 遠くに効く駒かどうか判定するためのテーブル */
  static int remote[32] = {
    0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1,
    0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1,
  };
  static MOVEINFO mi1;
  static TE te;
  int fxy, gxy;
  int d, v, w, p;
  int i, j;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

#ifdef DEBUG
  assert( 1 <= (king_xy & 0x0F) && (king_xy & 0x0F) <= 9 );
  assert( 1 <= (king_xy >> 4) && (king_xy >> 4) <= 9 );
#endif /* DEBUG */

  for (d=0; d<8; d++) {
    v = d2xy[d + (bo->next ? 8 : 0)][0];
    w = d2xy[d + (bo->next ? 8 : 0)][1];
    
    for (i=1; 1; i++) {
      fxy= king_xy + ((w*i) << 4) + (v*i);

      if ( (fxy & 0x0F) < 1 || 9 < (fxy & 0x0F) ) 
	break;
      if ( (fxy >> 4) < 1 || 9 < (fxy >> 4) ) 
	break;
      
      p = bo->board[ fxy ];

      if ( p == 0 )
	continue;
      if ( p == WALL )
	break;

      if ( getTeban(p) != bo->next ) {
	goto NEXT1;
      }
      
      for (i++; 1; i++) {
	gxy= king_xy + ((w*i) << 4) + (v*i);
	p = bo->board[ gxy ];
	
	if ( p == 0 )
	  continue;
	if ( p == WALL )
	  break;

	if ( getTeban(p) != bo->next )
	  goto NEXT1;
	if ( remote[p] == 0 )
	  goto NEXT1;
	
	if ( p == (KYO + 0x10) && d != 0 )
	  goto NEXT1;
	if ( p == KYO && d != 0 )
	  goto NEXT1;

	if ( d % 2 && (p & 0x0F) != 0x06 && (p & 0x0F) != 0x0E) {
	  goto NEXT1;
	}
	if ( !(d % 2) && (p & 0x0F) != 0x02 && (p & 0x0F) != 0x07 &&
	    (p & 0x0F) != 0x0F) {
	  goto NEXT1;
	}
	
	moveto(bo, fxy, &mi1);
	for (j=0; j<mi1.count; j++) {
	  if (d == 0 || d == 4) {
	    if ( (mi1.te[j].to & 0x0F) - (fxy & 0x0F) != 0 ) {
	      te.fm = fxy;
	      te.to = mi1.te[j].to;
	      te.nari = mi1.te[j].nari;
	      te.uti = 0;
	      miAdd(mi, &te);
	    }
	  }
	  if (d == 1 || d == 5) {
	    if ( (mi1.te[j].to & 0x0F) - (fxy & 0x0F) !=
		(mi1.te[j].to >> 4) - (fxy >> 4) ) {
	      te.fm = fxy;
	      te.to = mi1.te[j].to;
	      te.nari = mi1.te[j].nari;
	      te.uti = 0;
	      miAdd(mi, &te);
	    }
	  }
	  if (d == 2 || d == 6) {
	    if ( (mi1.te[j].to >> 4) - (fxy >> 4) != 0 ) {
	      te.fm = fxy;
	      te.to = mi1.te[j].to;
	      te.nari = mi1.te[j].nari;
	      te.uti = 0;
	      miAdd(mi, &te);
	    }
	  }
	  if (d == 3 || d == 7) {
	    if ( -((mi1.te[j].to & 0x0F) - (fxy & 0x0F)) !=
		   (mi1.te[j].to >> 4) - (fxy >> 4) ) {
	      te.fm = fxy;
	      te.to = mi1.te[j].to;
	      te.nari = mi1.te[j].nari;
	      te.uti = 0;
	      miAdd(mi, &te);
	    }
	  }
	}
	break;
      }
    }
    NEXT1:;
  }
}

