/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/misc.h,v $
 * $Id: misc.h,v 1.1.1.1 2005/12/09 09:03:06 tokita Exp $
 */

#ifndef _MISC_H_
#define _MISC_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/** ダブルクリックと判定するまでの時間(ミリ秒) */
#define DOUBLECLICKTIMEOUT 400

void       daemon_abort         (const char* s);
GdkPixmap* load_pixmap_from_data(GtkWidget* window, char **data);
GdkPixmap* load_pixmap_from_data_scaled(GtkWidget* window, const char** data,
					gint w,
					gint h);
GdkPixmap* load_pixmap_from_imlib_image_scaled(GtkWidget* window,
					       GdkPixbuf *im,
					       gint w,
					       gint h);
void set_color(GdkGC *gc, gushort r, gushort g, gushort b);
gboolean check_timeval(struct timeval *tv, struct timeval *now);
gboolean is_on_rectangle(GdkRectangle* a, GdkRectangle* b);
void pending_loop(void);
gboolean daemon_on_rectangle(GdkRectangle *rect, GdkPoint *point);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _MISC_H_ */



