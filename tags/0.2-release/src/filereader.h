/*
 * Daemonshogi -- a GTK+ based, Simple shogi(japanese chess) program.
 * Copyright (C) 2002 Masahiko Tokita
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * FileReader class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/filereader.h,v $
 * $Id: filereader.h,v 1.1.1.1 2005/12/09 09:03:04 tokita Exp $
 */

#ifndef _FILE_READER_H_
#define _FILE_READER_H_

#include <iconv.h> // iconv_t
#include <string>
using namespace std;

/** 一行の最大文字数 */
#define FILEREADER_MAXCOLUMN 120

typedef enum {
  D_FILEREADER_STAT_SUCCESSFUL,
  D_FILEREADER_STAT_ERROR,
} D_FILEREADER_STAT;


/**
 * テキストファイルを読み込むを行なう抽象クラス。
 * 文字コード変換も行なう。デフォルトは入力 SJIS 、出力は EUC-JP。
 * 一行 120 文字、1500 行まで読み込む
 */
struct FileReader {
  /** 入力文字コード */
  char incode[32];

  /** 読み込むファイルの FILE 構造体へのポインタ */
  FILE* in;

  /** 読み込むファイル名 */
  char* filename;

  /** 行番号 */
  int lineno;

  /** ファイル読み込み状況 */
  D_FILEREADER_STAT stat;

  /** iconv() で使うディスクリプタ */
  iconv_t cd;

  /** push された文字列 */
  string pushed_string;

  /** buffer */
  char buf[BUFSIZ * 3];
};

FileReader*       daemon_filereader_new         (void);
void              daemon_filereader_free        (FileReader* reader);
int               daemon_filereader_open        (FileReader* reader,
						 const char* filename);
void              daemon_filereader_close       (FileReader* reader);
char*             daemon_filereader_next        (FileReader* reader);
void              daemon_filereader_set_incode  (FileReader* reader,
						 const char *incode);
void              daemon_filereader_set_outcode (FileReader* reader,
						 char *outcode);
char*             daemon_filereader_skip_comment(FileReader* reader, char c);
void              daemon_filereader_push        (FileReader* reader, 
						 const char* s);
void              daemon_filereader_return2zero (char *s);
D_FILEREADER_STAT daemon_filereader_get_stat    (const FileReader* reader);


#endif /* _FILE_READER_H_ */
