/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * FileWriter class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/filewriter.h,v $
 * $Id: filewriter.h,v 1.1.1.1 2005/12/09 09:03:04 tokita Exp $
 */

#ifndef _FILE_WRITER_H_
#define _FILE_WRITER_H_

#include <iconv.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef enum {
  /** 読み込み成功 */
  D_FILEWRITER_SUCCESSFUL = 0,
  /** 読み込み失敗 */
  D_FILEWRITER_ERROR = 1,
} D_FILEWRITER_STAT;


/** ファイルへの出力と文字コード変換をおこなう.
    内部コードはUTF-8固定。
 */
typedef struct 
{
  /** 出力文字コード */
  char outcode[32];

  /** 出力用 FILE* */
  FILE* out;

  /** 出力先ファイル名 */
  char* filename;

  /** ファイル読み込み状況 */
  D_FILEWRITER_STAT stat;
  /** iconv で使うディスクリプタ */
  iconv_t cd;
} FileWriter;


FileWriter*       daemon_filewriter_new        (void);
void              daemon_filewriter_free       (FileWriter* writer);
int               daemon_filewriter_open       (FileWriter* writer,
						const char* filename);
void              daemon_filewriter_close      (FileWriter* writer);
void              daemon_filewriter_put        (FileWriter* writer,
						const char* s);

// void              daemon_filewriter_set_incode (FileWriter* writer,
// 						const char* incode);

/** 出力文字コードを設定する */
void              daemon_filewriter_set_outcode(FileWriter* writer,
						const char* outcode);

D_FILEWRITER_STAT daemon_filewriter_get_stat   (const FileWriter* writer);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _FILE_READER_H_ */
