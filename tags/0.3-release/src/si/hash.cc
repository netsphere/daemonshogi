/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <tcutil.h>  // TCMAP
#include <stddef.h> // offsetof
#include "si.h"
#include "ui.h"

#include "hashval.h"


/**
 * 置換表 transposition table.
 * オンメモリは LRUアルゴリズムで表から追い出す.
 * TCMAPは末尾に追加される。先頭を追い出すだけでいい。
 *
 * \todo 分散化するときに Tokyo Tyrantなどに移行
 * http://alpha.mixi.co.jp/blog/?p=166
 */
static TCMAP* ttable_mate[2] = {NULL, NULL};

/** 指し将棋用 */
static TCMAP* ttable = NULL;

/** 1レコード48バイトとして、データで30Mバイト. */
#define TTABLE_COUNT 625000


/**
 * ハッシュテーブル用にメモリを確保する。
 */
void newHASH()
{
  if ( sizeof(HASH) != HASH_HEADER_SIZE + HASH_ENT_SIZE ||
       offsetof(HASH, ent) != HASH_HEADER_SIZE ) {
    printf( "entry size: expected = %d, actual = %ld\n",
            HASH_HEADER_SIZE + HASH_ENT_SIZE, sizeof(HASH) );
    printf( "entry header size: expected = %d, actual = %ld\n",
            HASH_HEADER_SIZE, offsetof(HASH, ent) );
    abort();
  }

  if (ttable)
    freeHASH();

  ttable_mate[0] = tcmapnew();
  ttable_mate[1] = tcmapnew();
  ttable = tcmapnew();
}


/** 
 * ハッシュテーブルを解放する。
 */
void freeHASH()
{
  if (ttable) {
    tcmapdel(ttable_mate[0]);
    tcmapdel(ttable_mate[1]);
    tcmapdel(ttable);

    ttable_mate[0] = NULL;
    ttable_mate[1] = NULL;
    ttable = NULL;
  }
}


extern const int arr_xy[];

/**
 * BOARD からハッシュ値を生成する。持ち駒は考慮しない。
 * @param bo 対象のBOARD
 * @return uint64_t ハッシュ値 (64bit)
 */
uint64_t hsGetHashKey(const BOARD* bo)
{
  uint64_t key;
  int i;

#ifdef DEBUG
  assert( bo != NULL );
#endif /* DEBUG */

  key = 0;

  if ( bo->next == SENTE ) {
    for ( i=1; i<=81; i++ ) {
      key ^= hashval[bo->board[ arr_xy[ i ] ]][ arr_xy[ i ] ];
    }
  } else {
    /* 後手番の場合のハッシュキーはビット反転する */
    for ( i=1; i<=81; i++ ) {
      key ^= ( ~ hashval[bo->board[ arr_xy[ i ] ]][ arr_xy[ i ] ] );
    }
  }

  return key;
}


/**
 * HASHに持駒情報をセットする。
 * @param hs 対象のエントリ
 * @param bo 対象のBOARD
 */
static void hsSetPiece(EntVariant* hs, const BOARD* bo)
{
  hs->packed_inhand[ SENTE ] =
    bo->inhand[ SENTE ][ 1 ] +   // 18ヶ ... 5ビット
    (bo->inhand[ SENTE ][ 2 ] << 5) +
    (bo->inhand[ SENTE ][ 3 ] << 8) +
    (bo->inhand[ SENTE ][ 4 ] << 11) +
    (bo->inhand[ SENTE ][ 5 ] << 14) +
    (bo->inhand[ SENTE ][ 6 ] << 17) +
    (bo->inhand[ SENTE ][ 7 ] << 19);

  hs->packed_inhand[ GOTE ] =
    bo->inhand[ GOTE ][ 1 ] +
    (bo->inhand[ GOTE ][ 2 ] << 5) +
    (bo->inhand[ GOTE ][ 3 ] << 8) +
    (bo->inhand[ GOTE ][ 4 ] << 11) +
    (bo->inhand[ GOTE ][ 5 ] << 14) +
    (bo->inhand[ GOTE ][ 6 ] << 17) +
    (bo->inhand[ GOTE ][ 7 ] << 19);
}


static EntVariant make_v(const BOARD* bo, int32_t phi, int32_t delta,
                         int te_count, const TE* te)
{
  EntVariant v;

  hsSetPiece(&v, bo);
  v.phi = phi; v.delta = delta;
  v.te_count = te_count;
  v.best_move = te ? te_pack(te) : 0;

  return v;
}


extern long count_entry_hash;
extern long count_crash2;


/**
 * 持駒を比較する。
 * @param hs 対象のHASH
 * @param bo 対象のBOARD
 * @return 持駒が同じなら 0 を返す。相違がある場合は 1 を返す。
 */
static int hsCmpPiece(const EntVariant* hs, const BOARD* bo)
{
  int next = bo->next;
  int oppo = 1 - bo->next;
  uint32_t p = hs->packed_inhand[ next ];
  uint32_t q = hs->packed_inhand[ oppo ];

  if ( bo->inhand[ next ][ FU ] != (p & 0x1f) ||
       bo->inhand[ next ][ KYO ] != ((p >> 5) & 0x7) ||
       bo->inhand[ next ][ KEI ] != ((p >> 8) & 0x7) ||
       bo->inhand[ next ][ GIN ] != ((p >> 11) & 0x7) ||
       bo->inhand[ next ][ KIN ] != ((p >> 14) & 0x7) ||
       bo->inhand[ next ][ KAKU ] != ((p >> 17) & 0x3) ||
       bo->inhand[ next ][ HISYA ] != ((p >> 19) & 0x3) ||

       bo->inhand[ oppo ][ FU ] != (q & 0x1f) ||
       bo->inhand[ oppo ][ KYO ] != ((q >> 5) & 0x7) ||
       bo->inhand[ oppo ][ KEI ] != ((q >> 8) & 0x7) ||
       bo->inhand[ oppo ][ GIN ] != ((q >> 11) & 0x7) ||
       bo->inhand[ oppo ][ KIN ] != ((q >> 14) & 0x7) ||
       bo->inhand[ oppo ][ KAKU ] != ((q >> 17) & 0x3) ||
       bo->inhand[ oppo ][ HISYA ] != ((q >> 19) & 0x3) ) {
    return 1;
  }

  return 0;
}


/**
 * 盤面の優越関係を調べる。
 * 手番側の持ち駒が同じか多く、かつ相手側の持ち駒が同じか少ないとき
 * 優越しているという。
 * ある局面で詰みがあるとき、その局面を優越している局面でも詰む。
 *
 * @param hs 対象のHASH
 * @param bo 対象のBOARD
 *
 * @return hsでの持ち駒がboでの持ち駒を優越しているとき > 0, 逆のとき < 0, 
 *         どちらも優越していないとき0
 */
static int hsSuperiorBOARD(const EntVariant* hs, const BOARD* bo)
{
  int next = bo->next;
  int oppo = 1 - bo->next;
  uint32_t p = hs->packed_inhand[ next ];
  uint32_t q = hs->packed_inhand[ oppo ];

  // 駒箱に駒があることがあるので、両sideを調べる。
  if ( bo->inhand[ next ][ FU ] >= (p & 0x1f) &&
       bo->inhand[ next ][ KYO ] >= ((p >> 5) & 0x7) &&
       bo->inhand[ next ][ KEI ] >= ((p >> 8) & 0x7) &&
       bo->inhand[ next ][ GIN ] >= ((p >> 11) & 0x7) &&
       bo->inhand[ next ][ KIN ] >= ((p >> 14) & 0x7) &&
       bo->inhand[ next ][ KAKU ] >= ((p >> 17) & 0x3) &&
       bo->inhand[ next ][ HISYA ] >= ((p >> 19) & 0x3) &&

       bo->inhand[ oppo ][ FU ] <= (q & 0x1f) &&
       bo->inhand[ oppo ][ KYO ] <= ((q >> 5) & 0x7) &&
       bo->inhand[ oppo ][ KEI ] <= ((q >> 8) & 0x7) &&
       bo->inhand[ oppo ][ GIN ] <= ((q >> 11) & 0x7) &&
       bo->inhand[ oppo ][ KIN ] <= ((q >> 14) & 0x7) &&
       bo->inhand[ oppo ][ KAKU ] <= ((q >> 17) & 0x3) &&
       bo->inhand[ oppo ][ HISYA ] <= ((q >> 19) & 0x3) ) {
    return -1;
  }
  else if ( bo->inhand[ next ][ FU ] <= (p & 0x1f) &&
            bo->inhand[ next ][ KYO ] <= ((p >> 5) & 0x7) &&
            bo->inhand[ next ][ KEI ] <= ((p >> 8) & 0x7) &&
            bo->inhand[ next ][ GIN ] <= ((p >> 11) & 0x7) &&
            bo->inhand[ next ][ KIN ] <= ((p >> 14) & 0x7) &&
            bo->inhand[ next ][ KAKU ] <= ((p >> 17) & 0x3) &&
            bo->inhand[ next ][ HISYA ] <= ((p >> 19) & 0x3) &&

            bo->inhand[ oppo ][ FU ] >= (q & 0x1f) &&
            bo->inhand[ oppo ][ KYO ] >= ((q >> 5) & 0x7) &&
            bo->inhand[ oppo ][ KEI ] >= ((q >> 8) & 0x7) &&
            bo->inhand[ oppo ][ GIN ] >= ((q >> 11) & 0x7) &&
            bo->inhand[ oppo ][ KIN ] >= ((q >> 14) & 0x7) &&
            bo->inhand[ oppo ][ KAKU ] >= ((q >> 17) & 0x3) &&
            bo->inhand[ oppo ][ HISYA ] >= ((q >> 19) & 0x3) ) {
    return 1;
  }

  return 0;
}


/**
 * 局面を置換表に登録する。詰め将棋用。
 *
 * @param offense_side 攻め方
 * @param bo 対象のBOARD
 * @param phi   攻め方ノードでは証明数.
 * @param delta 攻め方ノードでは反証数. (phi, delta) = (0, +INF) で勝ち
 * @param te_count 手数. 深さではない.
 * @param best_move 最善手. 投了はNULL
 */
void hsPutBOARD( int offense_side, const BOARD* bo, 
                 int32_t phi, int32_t delta, int te_count, const TE* best_move )
{
  assert( bo != NULL );

  int size = 0;
  // エンディアンは考慮しない。
  // TODO: エンディアンの異なるノードが参加するときは要修正
  const HASH* old_ent = static_cast<const HASH*>(
       tcmapget(ttable_mate[offense_side], &bo->key, sizeof(bo->key), &size) );

  if (!old_ent) {
    HASH n;
    n.key = bo->key;
    n.ent[0] = make_v(bo, phi, delta, te_count, best_move);
    tcmapput( ttable_mate[offense_side], &bo->key, sizeof(bo->key), 
	      &n, sizeof(HASH) );
    return;
  }

  HASH* new_ent = static_cast<HASH*>( malloc(size + HASH_ENT_SIZE) );
  new_ent->key = bo->key;

  int count = 0;
  bool to_add = true;
  int i;

  if ( phi == 0 && delta >= VAL_INF ) {
    // 勝ち. 攻め方の持ち駒がより多い情報は捨ててよい
    for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
      const EntVariant* ent = old_ent->ent + i;

      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) > 0 ) {
        // より少ない持駒で詰んでいる. 持ち駒ベクトルが零点に近い
        // -> 既存のは削除してよい
        // skip
      }
      else if ( hsSuperiorBOARD(ent, bo) < 0 && (ent->phi == 0 && ent->delta >= VAL_INF) ) {
        // 既存のも詰み. 新たに追加する必要なし
        to_add = false;
        new_ent->ent[count++] = *ent;
      }
      else {
        // ベクトルが違う. 両方生き
        new_ent->ent[count++] = *ent;
      }
    }
  }
  else if ( phi >= VAL_INF && delta == 0 ) {
    // 不詰み. 持ち駒がより少ない不詰み情報は捨ててよい
    for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
      const EntVariant* ent = old_ent->ent + i;

      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) < 0 ) {
        // skip
      }
      else if ( hsSuperiorBOARD(ent, bo) > 0 && (ent->phi >= VAL_INF && ent->delta == 0) ) {
        // 既存のも不詰み
        to_add = false;
        new_ent->ent[count++] = *ent;
      }
      else
        new_ent->ent[count++] = *ent;
    }
  }
  else {
    // 探索途中. 持ち駒が一致し、一番深いものを1つだけ残す
    for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
      const EntVariant* ent = old_ent->ent + i;

      if ( ent->phi == 0 && ent->delta >= VAL_INF ) {
        // 詰みがある.
        if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) < 0 ) {
          to_add = false;
          new_ent->ent[count++] = *ent;
        }
        else
          new_ent->ent[count++] = *ent;
      }
      else if (ent->phi >= VAL_INF && ent->delta == 0) {
        if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) > 0 ) {
          to_add = false;
          new_ent->ent[count++] = *ent;
        }
        else
          new_ent->ent[count++] = *ent;
      }
      else {
        if ( ent->te_count < te_count || !hsCmpPiece(ent, bo) ) {
          // 深さで判定するのは不味い. 深いところから戻りながら更新していく
          // skip
        }
        else
          new_ent->ent[count++] = *ent;
      }
    }
  }

  // 追加すべきときだけ実際に書き込む
  if (to_add) {
    new_ent->ent[count++] = make_v(bo, phi, delta, te_count, best_move);
    tcmapput( ttable_mate[offense_side], &bo->key, sizeof(bo->key),
              new_ent, HASH_HEADER_SIZE + HASH_ENT_SIZE * count );

    tcmapmove( ttable_mate[offense_side], &bo->key, sizeof(bo->key), false );

    if ( tcmaprnum(ttable_mate[offense_side]) > TTABLE_COUNT )
      tcmapcutfront(ttable_mate[offense_side], TTABLE_COUNT / 3);
  }
  else
    tcmapmove( ttable_mate[offense_side], &bo->key, sizeof(bo->key), false );

  free(new_ent);
}


extern long G_COUNT_HASH_CHECK_MATE;
/* extern long G_COUNT_HASH_NOT_CHECK_MATE; */

/**
 * 局面の得点 (score) を取り出す。
 *
 * @param offense_side 攻め方
 * @param bo 対象のBOARD
 * @param phi 攻め方ノードで証明数.
 * @param delta 攻め方ノードで反証数.
 * @param te 最善手
 * @param te_count 手数. 深さではない
 *
 * @return 盤面が見つかったら 1 、見つからなかったら 0 を返す。
 */
int hsGetBOARD( int offense_side, const BOARD* bo, 
                int* phi, int* delta, TE* te, int te_count )
{
  assert( bo != NULL );
  assert( phi && delta );

  int size = 0;
  const HASH* cntr = static_cast<const HASH*>(
      tcmapget(ttable_mate[offense_side], &bo->key, sizeof(bo->key), &size) );
  if (!cntr)
    return 0;

  int i;
  for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
    const EntVariant* ent = cntr->ent + i;

    if ( ent->phi == 0 && ent->delta >= VAL_INF ) {
      // 現局面のほうが優越してれば詰み
      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) < 0 ) {
	*phi = ent->phi; *delta = ent->delta;
        if (te)
          *te = te_unpack(ent->best_move);
	return 1;
      }
    }
    else if ( ent->phi >= VAL_INF && ent->delta == 0 ) {
      // 不詰み
      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) > 0 ) {
	*phi = ent->phi; *delta = ent->delta;
        if (te)
          *te = te_unpack(ent->best_move);
	return 1;
      }
    }
    else {
      if (ent->te_count < te_count)
        continue;

      // TODO: ここでも優越関係を使える？
      if ( !hsCmpPiece(ent, bo) ) {
	*phi = ent->phi; *delta = ent->delta;
        if (te)
          *te = te_unpack(ent->best_move);
	return 1;
      }
    }
  }

  // 見付からなかった
  return 0;
}


extern long count_entry_hash;

/**
 * min/max検索用にBOARDをハッシュテーブルに登録する。
 * @param bo 対象のBOARD
 * @param score 盤面の評価点数
 * @param te_count 手数. 深さではない
 * @param best_move 最善手. 投了のときはNULL
 */
void hsPutBOARDMinMax( const BOARD* bo, int32_t score, int te_count, 
                       const TE* best_move )
{
  assert( bo != NULL );

  // TODO: 優越関係が使えるかな？
  int size = 0;
  const HASH* old_ent = static_cast<const HASH*>(
                	 tcmapget(ttable, &bo->key, sizeof(bo->key), &size) );

  if (!old_ent) {
    HASH n;
    n.key = bo->key;
    n.ent[0] = make_v(bo, score, 0, te_count, best_move);
    tcmapput( ttable, &bo->key, sizeof(bo->key), &n, sizeof(HASH) );
    return;
  }

  HASH* new_ent = static_cast<HASH*>( malloc(size + HASH_ENT_SIZE) );
  new_ent->key = bo->key;

  int count = 0;
  bool to_add = true;
  int i;

  if (score >= VAL_INF) {
    // 勝ち
    for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
      const EntVariant* ent = old_ent->ent + i;

      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) > 0 ) {
        // より少ない持ち駒
        // skip
      }
      else if ( hsSuperiorBOARD(ent, bo) < 0 && ent->phi >= VAL_INF ) {
        // hsPutBOARD() とはここが違うだけ
        to_add = false;
        new_ent->ent[count++] = *ent;
      }
      else
        new_ent->ent[count++] = *ent;
    }
  }
  else if (score <= -VAL_INF) {
    // 負け
    for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
      const EntVariant* ent = old_ent->ent + i;

      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) < 0 ) {
        // skip
      }
      else if ( hsSuperiorBOARD(ent, bo) > 0 && ent->phi <= -VAL_INF ) {
        to_add = false;
        new_ent->ent[count++] = *ent;
      }
      else
        new_ent->ent[count++] = *ent;
    }
  }
  else {
    // 探索途中
    for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
      const EntVariant* ent = old_ent->ent + i;

      if ( ent->phi >= +VAL_INF ) {
        // 既存のエントリが勝ち
        if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) < 0 ) {
          to_add = false;
          new_ent->ent[count++] = *ent;
        }
        else
          new_ent->ent[count++] = *ent;
      }
      else if (ent->phi <= -VAL_INF) {
        if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) > 0 ) {
          to_add = false;
          new_ent->ent[count++] = *ent;
        }
        else
          new_ent->ent[count++] = *ent;
      }
      else {
        if ( ent->te_count < te_count || !hsCmpPiece(ent, bo) ) {
          // skip
        }
        else
          new_ent->ent[count++] = *ent;
      }
    } // of for
  }

  if (to_add) {
    new_ent->ent[count++] = make_v(bo, score, 0, te_count, best_move);
    tcmapput( ttable, &bo->key, sizeof(bo->key), new_ent, 
	      HASH_HEADER_SIZE + HASH_ENT_SIZE * count );

    tcmapmove( ttable, &bo->key, sizeof(bo->key), false );

    if ( tcmaprnum(ttable) > TTABLE_COUNT )
      tcmapcutfront(ttable, TTABLE_COUNT / 3);
  }
  else
    tcmapmove( ttable, &bo->key, sizeof(bo->key), false );

  free(new_ent);
}


/**
 * 盤面の得点 (score) を取りだす。min/max検索用。
 * @param bo 対象のBOARD
 * @param score 得点を格納する変数のポインタ
 * @param te_count 手数. 深さではない
 *
 * @return 盤面が見つかったら 1 、見つからなかったら 0 を返す。
 */
int hsGetBOARDMinMax( const BOARD* bo, int32_t* score, TE* te, int te_count )
{
  assert( bo != NULL );
  assert( score );

  int size = 0;
  const HASH* cntr = static_cast<const HASH*>(
   		      tcmapget(ttable, &bo->key, sizeof(bo->key), &size) );
  if (!cntr)
    return 0;

  int i;
  for (i = 0; i < (size - HASH_HEADER_SIZE) / HASH_ENT_SIZE; i++) {
    const EntVariant* ent = cntr->ent + i;

    if ( ent->phi >= +VAL_INF ) {
      // 勝ち.
      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) < 0 ) {
        *score = ent->phi;
        if (te)
          *te = te_unpack(ent->best_move);
        return 1;
      }
    }
    else if ( ent->phi <= -VAL_INF ) {
      // 負け
      if ( !hsCmpPiece(ent, bo) || hsSuperiorBOARD(ent, bo) > 0 ) {
        *score = ent->phi;
        if (te)
          *te = te_unpack(ent->best_move);
        return 1;
      }
    }
    else {
      if (ent->te_count < te_count)
        continue;

      if ( !hsCmpPiece(ent, bo) ) {
        *score = ent->phi;
        if (te)
          *te = te_unpack(ent->best_move);
        return 1;
      }
    }
  } // of for

  // 見付からなかった
  return 0;
}


/**
 * XYの位置のハッシュ値を足す。
 * @param bo 対象のBOARD
 * @param xy 駒の位置
 */
void bo_add_hash_value(BOARD* bo, Xy xy)
{
  int p;
  
#ifdef DEBUG
  assert(bo != NULL);
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( 1 <= (xy & 0xF) && (xy & 0xF) <= 9 );
#endif /* DEBUG */
  
  p = boGetPiece(bo, xy);

  if ( bo->next == SENTE ) {
    bo->key ^= hashval[ p ][ xy ];
    bo->hash_all ^= hashval[p][xy];
  }
  else {
    bo->key ^= ( ~ hashval[ p ][ xy ] );
    bo->hash_all ^= ~ hashval[p][xy];
  }
}


/**
 * XYの位置のハッシュ値を外す。
 * @param bo 対象のBOARD
 * @param xy 駒の位置
 */
void bo_sub_hash_value(BOARD* bo, Xy xy)
{
  int p;
  
#ifdef DEBUG
  assert(bo != NULL);
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( 1 <= (xy & 0xF) && (xy & 0xF) <= 9 );
#endif /* DEBUG */
  
  p = boGetPiece(bo, xy);
  
  if ( bo->next == SENTE ) {
    bo->key ^= hashval[ p ][ xy ];
    bo->hash_all ^= hashval[p][xy];
  }
  else {
    bo->key ^= ( ~ hashval[ p ][ xy ] );
    bo->hash_all ^= ~ hashval[p][xy];
  }
}


/** 使用中のエントリの個数を得る */
int hs_entry_count(int tag)
{
  switch (tag) {
  case 0:
    return tcmaprnum(ttable_mate[0]);
  case 1:
    return tcmaprnum(ttable_mate[1]);
  case 2:
    return tcmaprnum(ttable);
  default:
    assert(0);
  }
}
