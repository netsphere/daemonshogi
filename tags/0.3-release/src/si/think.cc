/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <assert.h>
#include "si.h"
#include "ui.h"
#include <algorithm>
#include <string.h>

using namespace std;

// static int think_is_tsumero(BOARD *bo, int search_depth);
// void think_tsumero_uke(BOARD *bo, MOVEINFO *mi);

extern int THINK_DEEP_MAX;


/** 合法手のみ残す */
static void correct_moves(const BOARD* bo, MOVEINFO* mi)
{
  MOVEINFO legal_moves;
  int i;

  make_moves( bo, &legal_moves );
  for (i = 0; i < mi->count; ) {
    if ( !mi_includes(&legal_moves, mi->te[i]) )
      miRemoveTE2( mi, i );
    else
      i++;
  }
}


/**
 * 侯補手を探す。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param depth 読みの深さ
 */
void think(BOARD *bo, MOVEINFO *mi, int depth) 
{
  int king_xy, king_xy2;
  int num_of_ohte;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
  assert( 0 <= depth );
#endif /* DEBUG */
  
  mi->count = 0;

  /* 自分の王の位置 */
  king_xy = bo->code_xy[1 + bo->next][0];
  /* 相手の王の位置 */
  king_xy2 = bo->code_xy[1 + (bo->next == 0)][0];
  
  /* 王手されているか調べる */
  num_of_ohte = bo_attack_count(bo, (bo->next == 0), king_xy);

#ifdef DEBUG
  assert( 0 <= num_of_ohte && num_of_ohte <= 2 );
#endif /* DEBUG */
  
  if ( num_of_ohte ) {
    /* 王手されていたら受けを探す */
    uke( bo, mi );
    return;
  }
#if 0
  if ( depth == 0 ) {
    if ( think_is_tsumero(bo, 5) ) {
      /* 詰めろになっていた */
      /* 詰めろ逃れ専用ルーチンを呼ぶ */
      think_tsumero_uke(bo, mi);
      if ( 0 < mi->count ) {
	return;
      }
      
      /* 受ける手がない場合は王手を探す */
      ohte(bo, mi);
      
      if ( 0 < mi->count ) {
	return;
      }
      
      /* 王手もない場合は合法手をセットして終了する */
      search_053(bo, mi);
      search_068(bo, mi);
      /* ダブった手があったら削除する */
      miRemoveDuplicationTE( mi );

      // 合法手かどうか
      correct_moves( bo, mi );
      
      return;
    }
  }
#endif // 0

#if 0
  for ( i=0; i<1; i++ ) {
    MATE_DEEP_MAX = i;
    if ( mate(bo, &root, 0) ) {
      /* 詰んだ */
      printTREE(root.child, 0);
      break;
    }
  }
    
  if ( root.child != NULL ) {
    /* check mate */
    miAdd( mi, &(root.child->te) );
    freeTREE( root.child );
    return;
  }
#endif /* NOREAD */

  search_011(bo, mi);
  search_013(bo, mi);
  search_014(bo, mi);
  search_012(bo, mi);
  if ( mi->count < 2 ) {
    search_024a(bo, mi);
  }
  if ( depth == 0 ) {
    search_068(bo, mi);
  }
  if ( depth <= THINK_DEEP_MAX - 3 ) {
    search_022(bo, mi);
    search_027(bo, mi);
    search_024a(bo, mi);
    search_031a(bo, mi);
    search_031b(bo, mi);
  }
  if ( depth <= 3 ) {
    search_046(bo, mi);
    search_048(bo, mi);
    search_044(bo, mi);
    search_041(bo, mi);
    search_047(bo, mi);
  }
  if ( depth <= 1 ) {
    search_053(bo, mi);
  }

  /* ダブった手があったら削除する */
  miRemoveDuplicationTE( mi );

  // 合法手かどうか
  correct_moves( bo, mi );

  return;
}


/**
 * 詰めろを掛けているか調べる。
 *
 * @param bo 対象のBOARD
 * @param teban 詰めろを掛けているか調べる手番
 * @reutrn 詰めろならば 1 を返す。なければ 0 を返す。
 */
bool think_is_tsumero(BOARD* bo, int teban)
{
  assert(teban == 0 || teban == 1);

  bool flg_mate, flg_toggle;
  TE te_ret;

  if (bo->next != teban) {
    boToggleNext(bo);
    flg_toggle = true;
  }
  else
    flg_toggle = false;

  flg_mate = dfpn_mate(bo, 21, &te_ret) == CHECK_MATE ? true : false;

  if (flg_toggle)
    boToggleNext(bo);

  return flg_mate;
}


/** sideによる相手玉の周りへの利きの合計 */
static int count_attack_around(const BOARD* bo, int side)
{
  Xy const oppo_king_xy = bo->code_xy[ 1 + (side == 0)][0];
  int r = 0;

  for (int i = 0; i < 24; i++) {
    Xy xy = oppo_king_xy + arr_round_to24[i];
    if (bo->board[xy] == WALL)
      continue;
    r += bo_attack_count(bo, side, xy);
  }

  return r;
}


/** sideによる自玉の周りへの利きの合計 */
static int count_defense_around(const BOARD* bo, int side)
{
  Xy const my_king_xy = bo->code_xy[ 1 + side][0];
  int r = 0;

  for (int i = 0; i < 24; i++) {
    Xy xy = my_king_xy + arr_round_to24[i];
    if (bo->board[xy] == WALL)
      continue;
    r += bo_attack_count(bo, side, xy);
  }

  return r;
}


/**
 * sideの自玉が行ける範囲を数え上げる.
 * とりあえず、2手で行ける範囲
 */
static int count_king_movables(const BOARD* bo, int side)
{
  Xy king_xy = bo->code_xy[1 + bo->next][0];

  int r[69];
  memset( r, 0, 69 * sizeof(int) );

  Xy xy;
  for (int i = 0; i < 8; i++) {
    xy = king_xy + arr_round_to[i];
    if (bo->board[xy] == WALL)
      continue;
    if (bo->board[xy] && getTeban(bo->board[xy]) == bo->next)
      continue;
    if (bo_attack_count(bo, 1 - bo->next, xy))
      continue;

    for (int j = 0; j < 8; j++) {
      Xy xy2 = xy + arr_round_to[j];
      if (bo->board[xy2] == WALL)
        continue;
      if (bo->board[xy2] && getTeban(bo->board[xy2]) == bo->next)
        continue;
      if (bo_attack_count(bo, 1 - bo->next, xy2))
        continue;

      r[xy2 - king_xy + 34] = 1;
    }
  }

  r[34] = 0;
  return count(r, r + 69, 1);
}


/**
 * 詰めろを受ける手を検索する。相手玉に対する詰みはない
 * @param bo 対象のBOARD
 * @param mi_ret 手を記録するMOVEINFO
 */
void think_tsumero_uke(BOARD* bo, MOVEINFO* mi_ret)
{
  MOVEINFO mi1;
  int i;
  int my_side = bo->next;
  Xy king_xy = bo->code_xy[1 + bo->next][0];

#if 0
  mi1.count = 0;

  /* 直前に動いた相手の駒を取る手 */
  search_011(bo, &mi1);

  /* 相手の王への王手 */
  // search_013(bo, &mi1);         ... これが不味い. 王手は受けにならない

  /* 王が動く手 */
  search_049(bo, &mi1);
  /* 自分の王周辺に動く手 */
  search_pressure(bo, &mi1, king_xy, bo->next);
  /* 直前に指した駒への歩打ち */ 
  search_047(bo, &mi1);
  /* 自分の王周辺に駒を打ち込む手 */
  search_031b(bo, &mi1);

  /* ダブった手があったら削除する */
  miRemoveDuplicationTE( &mi1 );
  // 合法手だけ残す
  correct_moves( bo, &mi1 );
#endif

  make_moves(bo, &mi1);

  mi_ret->count = 0;
  if ( mi1.count == 0 )
    return;

  for (i = 0; i < mi1.count; i++) {
    assert( mi1.te[i].hint == 0 || mi1.te[i].hint == TE_STUPID );
    if (mi1.te[i].hint == TE_STUPID)
      continue;

    int ma = count_defense_around(bo, my_side);
    int oa = count_attack_around(bo, 1 - my_side);
    int mc = count_king_movables(bo, my_side);

    boMove_mate( bo, mi1.te[i] );

    if ( count_defense_around(bo, my_side) <= ma &&
         count_attack_around(bo, 1 - my_side) >= oa &&
         count_king_movables(bo, my_side) <= mc ) {
      boBack_mate( bo );
      continue;
    }

    TE te_ret;
    if ( dfpn_mate(bo, 21, &te_ret) == NOT_CHECK_MATE ) {
      // 詰みを防いだ。侯補手に加える
      miAdd( mi_ret, &mi1.te[i] );
    }
    boBack_mate( bo );
  }
}


/**
 * 必至探索 (受け側)
 * @return 必至で true
 */
static bool think_hisshi_defense( BOARD* bo, int tenuki, int depth )
{
  MOVEINFO mi2;
  int const my_side = bo->next;

  TE te_ret;
  if ( bo->code_xy[1 + (bo->next == 0)][0] && dfpn_mate(bo, 21, &te_ret) == CHECK_MATE )
    return false;

  make_moves(bo, &mi2);
  if (mi2.count == 0) {
    // 受けなし.
    return true;
  }

  // 逃げ切った
  if (depth <= 0)
    return false;

  // ANDノード: すべてが必至でなければ勝ち
  for (int i = 0; i < mi2.count; i++) {
    assert( mi2.te[i].hint == 0 || mi2.te[i].hint == TE_STUPID );
    if ( mi2.te[i].hint == TE_STUPID )
      continue;

    int ma = count_defense_around(bo, my_side);
    int oa = count_attack_around(bo, 1 - my_side);

    boMove_mate( bo, mi2.te[i] );

    if ( count_defense_around(bo, my_side) <= ma &&
         count_attack_around(bo, 1 - my_side) >= oa ) {
      boBack_mate( bo );
      continue;
    }

    TE te;
    if ( !think_hisshi(bo, tenuki, depth - 1, &te) ) {
      // 必至ではない
      boBack_mate( bo );
      return false;
    }

    boBack_mate( bo );
  }

  return true;
}


/**
 * 必至を探す
 *
 * @return 必至があった場合 true, 見つからなかった場合 false
 */
bool think_hisshi(BOARD* bo, int tenuki, int depth, TE* te_ret)
{
  MOVEINFO mi;
  int const my_side = bo->next;

  if (tenuki <= 0)
    return dfpn_mate(bo, 21, te_ret) == CHECK_MATE;

  make_moves(bo, &mi);

  if (!mi.count)
    return false;

  // ORノード: どれかが勝ちならok
  for (int i = 0; i < mi.count; i++) {
    assert( mi.te[i].hint == 0 || mi.te[i].hint == TE_STUPID );
    if ( mi.te[i].hint == TE_STUPID )
      continue;

    int ka = count_attack_around(bo, my_side);
    boMove_mate( bo, mi.te[i] );

    // このぐらい大胆に枝刈りしないと終わらない
    if (count_attack_around(bo, my_side) <= ka) {
      boBack_mate(bo);
      continue;
    }

    bool is_check = bo_attack_count( bo, my_side, bo->code_xy[1 + (my_side == 0)][0] );
    // TE te;
    if ( think_hisshi_defense(bo, is_check ? tenuki : tenuki - 1, depth - 1) ) {
      // 必至
      boBack_mate( bo );
      *te_ret = mi.te[i];
      return true;
    }

    boBack_mate( bo );
  }

  return false;
}


/** nodeの手番から見た評価値 */
int32_t think_eval(const BOARD* bo) 
{
  int32_t score = 0;
  int x, y;

  // 大駒の可動性を加える
  for (y = 1; y <= 9; y++) {
    for (x = 1; x <= 9; x++) {
      PieceKind p = bo->board[(y << 4) + x];
      if ( (p & 0x7) == HISYA || (p & 0x7) == KAKU ) {
	if (getTeban(p) == bo->next) {
	  score += bo_piece_kiki_count(bo, bo->noBoard[(y << 4) + x]) *
	    NUM_OF_MOVE_POINT;
	}
	else {
	  score -= bo_piece_kiki_count(bo, bo->noBoard[(y << 4) + x]) *
	    NUM_OF_MOVE_POINT;
	}
      }
    }
  }

  return score + bo->point[bo->next] - bo->point[bo->next == 0];
}
