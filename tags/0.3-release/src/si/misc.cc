/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"

/**
 * 致命的エラーが発生したときに呼ばれる(動作に必要なメモリが確保でき
 * なかった等)。
 * @param s エラー内容を表す文字列
 */
void si_abort(const char* s)
{
  printf("si_abort: %s\n", s);
  abort();
}

/**
 * buf に時間を表す文字列を格納する。
 * 00:00:00 の形式で表示する。内容は 時:分:秒。
 * ただし t に100 時間以上の時間が格納されている場合 99 時間として表示する。
 * buf には BUFSIZ 以上のメモリが確保されれていることを想定している。
 * @param buf 格納先のポインタ
 * @param t 表示する時間(秒)
 */
void time2string(char *buf, time_t t) {
  int hour;
  int minute;
  int second;

#ifdef DEBUG
  assert( buf != NULL );
#endif /* DEBUG */
  
  hour = t / 3600;
  if ( 99 < hour ) {
    hour = 99;
  }

  minute = (t % 3600) / 60;
  if ( 59 < minute ) {
    minute = 59;
  }
  
  second = t % 60;

  sprintf(buf, "%02d:%02d:%02d", hour, minute, second);
}

#ifndef USE_MISC_MACRO

/**
 * bo の (x,y) の位置の駒が後手の駒だったら非ゼロを返す。
 * それ以外(先手の駒、駒がない)の場合はゼロを返す。
 * @param bo 対象のBOARD
 * @param x X座標の位置
 * @param y Y座標の位置
 */
int isGote(BOARD *bo, int x, int y) {
#ifdef DEBUG
  assert( bo != NULL );
  assert( 0 <= x && x <=10 );
  assert( 0 <= y && y <=10 );
#endif /* DEBUG */
  return (bo->board[ (y<<4) + x ]) & 0x10;
}

/**
 * 駒種類の手番を返す。0ならば先手、1ならば後手。
 * @param p 駒種類
 * @return 0ならば先手、1ならば後手。
 */
int getTeban(PieceKind p) // __attribute__((const))
{
#ifdef DEBUG
  assert( p != 0 );
  assert( p != WALL );
#endif /* DEBUG */
  
  return (p & 0x10) == 0x10;
}

#endif /* USE_MISC_MACRO */

/**
 * 先手の駒ではない場合 1 を返す。
 * 
 * @param p 駒種類
 * @return 
 */
int notSentePiece(int p) {
#ifdef DEBUG
  assert( p != WALL );
#endif /* DEBUG */

  if ( p == 0 )
    return 1;
  
  if ( (p & 0x10) == 0x10 )
    return 1;
  
  return 0;
}

/**
 * 後手の駒ではない場合 1 を返す。
 * 
 * @param p 駒種類
 * @return 
 */
int notGotePiece( int p ) {
#ifdef DEBUG
  assert( p != WALL );
#endif /* DEBUG */

  if ( p == 0 )
    return 1;
  
  if ( (p & 0x10) == 0x10 )
    return 0;
  
  return 1;
}


/**
 * 指定したx軸に先手の歩があるか調べる。
 * あったらその歩の Y 座標を返す。なかったら 0 を返す。
 * @param bo 調べるBOARD
 * @param x 指定するx軸
 * @return なかった場合 0 、あったら Y座標(1〜9)を返す。
 */
int checkFuSente(const BOARD* bo, int x)
{
  int i;

#ifdef DEBUG
  assert( bo != NULL );
  assert( 1 <= x && x <= 9 );
#endif /* DEBUG */

  for (i = 0x10; i <= 0x99; i += 0x10) {
    if ( bo->board[ i + x ] == FU )
      return i >> 4;
  }

  return 0;
}


/**
 * 指定したx軸に後手の歩があるか調べる。
 * あったらその歩の Y 座標を返す。なかったら0を返す。
 * @param bo 調べるBOARD
 * @param x 指定するx軸
 * @return なかった場合 0 、あったら Y座標(1〜9)を返す。
 */
int checkFuGote(const BOARD* bo, int x)
{
  int i;

#ifdef DEBUG
  assert( bo != NULL );
  assert( 1 <= x && x <= 9 );
#endif /* DEBUG */

  for (i = 0x10; i <= 0x99; i += 0x10) {
    if ( bo->board[ i + x ] == FU + 0x10 ) 
      return i >> 4;
  }

  return 0;
}

