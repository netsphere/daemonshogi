/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"
#include "ui.h"
#include "taboo_list.h"
#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>
using namespace std;

#define DEBUGLEVEL 0

void addTREEChild(TREE *parent, TREE *child);
TREE *findPointerMe(TREE *tree, TREE *target);
void removeTREEChild(TREE *tree);


struct PDPair {
  int32_t phi, delta;
/*
  PDPair& operator = (const PDPair& y) {
    phi = y.phi;
    delta = y.delta;
    return *this;
  }
 */
};


/** 
 * タブーリストと置換表の両方から探す
 * @return 見つかったときtrue 
 */
static bool lookup_hash( const TabooList<PDPair>* taboo_list, 
			 int offense_side, const BOARD* bo, int te_count,
			 int32_t* phi, int32_t* delta )
{
  PDPair ent;
  if ( taboo_list->lookup(bo, &ent) ) {
    *phi = ent.phi; *delta = ent.delta;
#if DEBUGLEVEL >= 3
    printf("taboo hit! k=%" PRIx64 "\n", bo->hash_all);
#endif
    return true;
  }

  if ( hsGetBOARD(offense_side, bo, phi, delta, NULL, te_count) )
    return true;

  *phi = 1;  // TODO: ありそうな局面はよい (低い) 値
  *delta = 1;
  return false;
}


static int32_t sum_step(int32_t x, int32_t y) __attribute__((const));

static int32_t sum_step(int32_t x, int32_t y) 
{
  if (x >= VAL_INF || y >= VAL_INF)
    return VAL_INF;
  else if (x >= VAL_MAY_MATCH || y >= VAL_MAY_MATCH)
    return VAL_MAY_MATCH;
  else {
    return min(x + y, VAL_MAY_MATCH - 1);
  }
}


/**
 * 手を選ぶ. 子ノードのうち最もpnが小さいもの.
 * node が攻め方の場合は、子ノードのdelta最小を選ぶ.
 * ひと通り展開するので、move ordering は効果ない
 */
static void expand_and_best_move( const TabooList<PDPair>* taboo_list,
				  int offense_side,
                                  BOARD* node, // const MOVEINFO& mi,
                                  int te_count,
                                  int rest_depth, int rest_node,
                                  PDPair* best_child,
				  int32_t* child_phi_sum,
				  int32_t* second_child_delta,
                                  TE* best_move )
{
  // 手を展開する
  MOVEINFO mi;
  if (node->next == offense_side)
    ohte(node, &mi);
  else
    uke(node, &mi);

#if 0
  for (int ii = 1; ii < mi.count; ii++) {
    for (int jj = 0; jj < ii; jj++) {
      if (mi.te[ii] == mi.te[jj]) {
        printBOARD(node);
        mi_print(node, &mi);
        printf("duplicate!!!: #%d #%d\n", ii, jj);
        assert(0);
      }
    }
  }
#endif

  best_child->delta = +VAL_INF;
  *child_phi_sum = 0;

  if (!mi.count) {
    best_move->fm = 0; best_move->to = 0; best_move->nari = 0; best_move->uti = 0;
    return;
  }

  if ( node->next != offense_side && 
       (rest_depth <= 0 || rest_node <= 0) ) {
    // 逃げ切った
    best_child->delta = 0;
    *child_phi_sum = +VAL_MAY_MATCH;
#if DEBUGLEVEL >= 3
    printf("%d: win.\n", rest_depth);
#endif
    *best_move = mi.te[0];
    return;
  }

  *second_child_delta = +VAL_INF;
  const TE* may_best = mi.te;  // 手がある限り何か選ぶ

#if DEBUGLEVEL >= 3
  printf("%d: ", rest_depth);
#endif
  for (int i = 0; i < mi.count; i++) {
    assert(mi.te[i].hint == 0 || mi.te[i].hint == TE_STUPID);
    if (mi.te[i].hint == TE_STUPID)
      continue;

    boMove_mate(node, mi.te[i]);

    int32_t child_phi, child_delta;
    lookup_hash( taboo_list, offense_side, node, te_count,
                 &child_phi, &child_delta );
#if DEBUGLEVEL >= 3
    uint64_t node_key = node->key;
#endif
    if (child_delta < best_child->delta) {
      // best_move を設定
      *second_child_delta = best_child->delta;
      best_child->phi = child_phi;
      best_child->delta = child_delta;
      may_best = &mi.te[i];
    }
    else if (child_delta < *second_child_delta) {
      // 2位を更新
      *second_child_delta = child_delta;
    }

    *child_phi_sum = sum_step(*child_phi_sum, child_phi);

    boBack_mate(node);
#if DEBUGLEVEL >= 3
    printf("%s=%" PRIx64 "(%d,%d), ", te_str(node, &mi.te[i]).c_str(), node_key,
           child_phi, child_delta);
#endif 

    if (child_phi >= +VAL_MAY_MATCH)
      break; // nodeの手番側の勝ち確定
  }
#if DEBUGLEVEL >= 3
  printf("\n");
#endif

  *best_move = *may_best;
#if DEBUGLEVEL >= 3
  printf( "best = %s\n", te_str(node, may_best).c_str() );
#endif
}


static void dfpn_search( TabooList<PDPair>* taboo_list, int offense_side,
                         BOARD* node, 
                         int32_t threshold_phi, int32_t threshold_delta,
                         int te_count, int depth, int* node_limit )
{
  // 置換表を確認しない -> 必ず一度はノードを展開する

#if DEBUGLEVEL >= 3
  printf("%d: dfpn_search() enter: (%d, %d), %d\n", depth, threshold_phi, threshold_delta, *node_limit);
#endif

  // サイクル回避
  PDPair cyc;
  cyc.phi = threshold_phi; cyc.delta = threshold_delta;
  taboo_list->add(node, cyc);

  PDPair best_child;
  int32_t child_phi_sum;
  TE best_move, prev_move;

  prev_move.fm = 0; prev_move.to = 0; prev_move.nari = 0; prev_move.uti = 0;

  while (true) {
    int32_t second_child_delta;

    best_move.fm = 0; best_move.to = 0; best_move.nari = 0; best_move.uti = 0;

    // ノードを展開して証明数を得る
    expand_and_best_move( taboo_list, offense_side, node, // mi,
                          te_count, depth, *node_limit,
                          &best_child,
			  &child_phi_sum, &second_child_delta, 
			  &best_move );
    // しきい値を超えた？
    if ( best_child.delta >= threshold_phi || child_phi_sum >= threshold_delta )
      break;

    assert(best_move.to); // 候補手が一つでもあれば必ず設定される

    if (best_move == prev_move) {
      printf("warning: loop???\n"); // DEBUG
    }

#if DEBUGLEVEL >= 3
    printf("%d: %s (%d, %d)\n", depth, te_str(node, &best_move).c_str(),
           best_child.delta, child_phi_sum);
#endif

    // 一番見込みの有りそうな手で探索範囲を広げる
    boMove_mate(node, best_move);
    // このしきい値を超える -> delta cutが起こる
    int32_t threshold_child_phi = threshold_delta - 
                                  (child_phi_sum - best_child.phi);

    // このしきい値を超える -> 2位の方が有望
    int32_t threshold_child_delta = min(threshold_phi, second_child_delta + 1);

    dfpn_search( taboo_list, offense_side, node,
                 threshold_child_phi, threshold_child_delta,
                 te_count, depth - 1, node_limit );
    boBack_mate(node);

    prev_move = best_move;
  }

  if (node->next == offense_side && child_phi_sum >= VAL_MAY_MATCH) {
    // 新たに勝ちが判明

    // TODO: 証明駒を調べる

    // TODO: 合い駒対策
  }

  // ノードの証明数を更新
  hsPutBOARD( offense_side, node, best_child.delta, child_phi_sum,
              te_count, &best_move );
  (*node_limit)--;
  taboo_list->remove(node);

#if DEBUGLEVEL >= 3
    printf("%d: put %" PRIx64 "-> (%d,%d)\n", depth, node->key, best_child.delta, child_phi_sum);
#endif
}


MATE_STATE dfpn_mate( BOARD* bo, int depth, TE* best_move )
{
  assert(best_move);

  TabooList<PDPair> taboo_list;
  int node_limit = 6000; // magic
  dfpn_search( &taboo_list, bo->next, bo, +VAL_MAY_MATCH, +VAL_MAY_MATCH,
               bo->mi.count, depth, &node_limit );

  int32_t phi, delta;
  int r = hsGetBOARD( bo->next, bo, &phi, &delta, best_move, 0 );
  if (!r) {
    si_abort("internal error: not found result\n");
  }

  if (phi == 0 && delta >= VAL_MAY_MATCH)
    return CHECK_MATE;
  else
    return NOT_CHECK_MATE;
}


extern GAME g_game;
extern long count_hash_hit;

#if 1

/** 詰将棋: 受け方 */
static MATE_STATE mate_defense( BOARD* bo, TREE* tree, int gn )
{
  MOVEINFO mi2;
  TREE* tree2;
  int phi, delta;
  int offense_side = 1 - bo->next;
  int j;

#ifdef USE_HASH
  if ( hsGetBOARD(offense_side, bo, &phi, &delta, NULL, MATE_DEEP_MAX - gn) ) {
    count_hash_hit++;
    /* 盤面が登録されていた */
    if ( phi == 0 && delta >= VAL_INF )
      return CHECK_MATE; // 詰み
    else if ( phi >= VAL_INF && delta == 0 )
      return NOT_CHECK_MATE; // 不詰み
    else {
      // TODO:
      assert(0);
    }
  }
#endif /* USE_HASH */

  // 受ける手
  uke( bo, &mi2 );

  if ( mi2.count == 0 ) {
    // 受けなし. 詰み
    hsPutBOARD( offense_side, bo, VAL_INF, 0, 0, NULL );
    return CHECK_MATE;
  }

  if ( MATE_DEEP_MAX <= gn ) {
    // まだ手がある. 逃げ切った
    // TODO: 置換表への登録
    return NOT_CHECK_MATE;
  }

  // ANDノード: すべてが詰まないといけない
  // count = 0;
  for (j = 0; j < mi2.count; j++) {
    boMove_mate( bo, mi2.te[j] ); // 受け方

    tree2 = newTREE();
    tree2->te = mi2.te[j];
    addTREEChild( tree, tree2 );

    if ( mate(bo, tree2, gn + 1) == NOT_CHECK_MATE ) {
      /* 詰まなかった */
      boBack_mate( bo );
      removeTREEChild( tree2 );

      // 置換表には登録しない・・・手数オーバーかもしれない
      // hsPutBOARD( bo->next, bo, 0, VAL_INF, ... );

      return NOT_CHECK_MATE;
    }

    // 詰み
    boBack_mate( bo );
  }

  // 置換表には mate() で登録
  return CHECK_MATE;
}


/**
 * bo の局面に詰みがあるか調べる。
 * 詰みを発見した場合は tree に手順をセットする。
 * ( tree 以下のデータ構造は freeTREE() を使って開放する必要がある。
 * freeTREE(tree->child) のようにする。 )
 *
 * @param gn 現在の読みの深さ.
 *     gn は現在の読みの深さをセットする。通常は 0 をセットするだけでよい。
 *     ( gn は mate() 自身が再帰呼出しするときに +1 されてセットされる )
 *     最大の読みの深さは MATE_DEEP_MAX に指定する。
 *
 * @param bo 対象のBOARD
 *
 * @return CHECK_MATE の場合、詰みがある。NOT_CHECK_MATE は詰みが見付から
 *         なかった。
 */
MATE_STATE mate( BOARD* bo, TREE* tree, int gn ) 
{
  MOVEINFO mi1;
  TREE* tree1;
  // int count;
  int i;
  // int king_xy;
  int my_side;
  int phi, delta;

#ifdef USE_GUI  
  /* GUIのペンディングされたイベントを処理する */
  if (gn < 3) {
    /* 中断や投了が入ってないか調べる */
    if ( gmGetGameStatus(&g_game) == SI_CHUDAN ||
	 gmGetGameStatus(&g_game) == SI_TORYO ||
	 gmGetChudanFlg(&g_game) == 1) {
      /* 中断または投了が入ったので終了する */
      return NOT_CHECK_MATE;
    }
    processing_pending();
  }
#endif /* USE_GUI */

#ifdef USE_HASH
  if ( hsGetBOARD(bo->next, bo, &phi, &delta, NULL, MATE_DEEP_MAX - gn) ) {
    count_hash_hit++;
    /* 盤面が登録されていた */
    if ( phi == 0 && delta >= VAL_INF )
      return CHECK_MATE; // 詰み
    else if ( phi >= VAL_INF && delta == 0 )
      return NOT_CHECK_MATE; // 不詰み
    else {
      // TODO:
      assert(0); 
    }
  }
#endif /* USE_HASH */

  // mi1 = newMOVEINFO();
  ohte(bo, &mi1);
  
  if ( mi1.count == 0 ) {
    /* 王手がない */
    hsPutBOARD( bo->next, bo, VAL_INF, 0, 0, NULL );
    return NOT_CHECK_MATE;
  }

  // ORノード: どれか一つでも詰めばいい
  my_side = bo->next;
  for (i = 0; i < mi1.count; i++) {
    boMove_mate( bo, mi1.te[i] ); //攻め方
#ifdef DEBUG
    // 攻め方の玉に王手が掛かっていないこと
    king_xy = bo->code_xy[ my_side + 1 ][0];
    if ( king_xy != 0 ) {
      assert( bo_attack_count(bo, 1 - my_side, king_xy) == 0 );
    }
#endif

    tree1 = newTREE();
    tree1->te = mi1.te[i];
    addTREEChild( tree, tree1 );

    if ( mate_defense(bo, tree1, gn) == CHECK_MATE ) {
      // 詰み
      boBack_mate( bo );

      // TODO: 最小持ち駒
      assert( my_side == bo->next );
      hsPutBOARD( bo->next, bo, 0, VAL_INF, 0, NULL );

      return CHECK_MATE;
    }

    // 詰まず
    boBack_mate( bo );
    removeTREEChild( tree1 );
  }

  // 置換表には登録しない・・・手数オーバーで詰まずかもしれない
  return NOT_CHECK_MATE;
}

#endif // 0


/**
 * parent に子の盤面のポインタ child を加える
 * @param parent 親のポインタ
 * @param child 子のポインタ
 */
void addTREEChild(TREE *parent, TREE *child) {
#ifdef DEBUG
  assert( parent != NULL );
  assert( child != NULL );
#endif /* DEBUG */

  if ( parent->child == NULL ) {
    parent->child = child;
    parent->child->parent = parent;
    parent->child->child = NULL;
    parent->child->brother = NULL;
  } else {
    TREE *tmp;
    tmp = parent->child;
    parent->child = child;
    parent->child->parent = parent;
    parent->child->child = NULL;
    parent->child->brother = tmp;
  }
}

/**
 * tree の兄弟のツリーをたどって target の一つ上の兄弟を探してそれを
 * 返す。
 * @param tree 検索対象のツリーへのポインタ
 * @param target 兄弟のツリーへのポインタ
 */
TREE *findPointerMe(TREE *tree, TREE *target) {
  TREE *tmp;
#ifdef DEBUG
  assert ( tree != NULL );
  assert ( target != NULL );
#endif /* DEBUG */

  tmp = tree;
  
  while ( 1 ) {
    if ( tmp->brother == target )
      break;
    tmp = tmp->brother;
#ifdef DEBUG
    assert ( tmp != NULL );
#endif /* DEBUG */
  }
  
  return tmp;
}

/**
 * 不詰が確定した手をツリーから削除する。
 * @param tree ツリー
 */
void removeTREEChild(TREE *tree) {
#ifdef DEBUG
  assert ( tree != NULL );
  assert ( tree->parent != NULL );
#endif /* DEBUG */

  if ( tree->child != NULL )
    freeTREE( tree->child );
  
  if ( tree->brother != NULL ) {
    if ( tree->parent->child == tree ) {
      tree->parent->child = tree->brother;
    } else {
      TREE *tmp;
      tmp = findPointerMe( tree->parent->child, tree );
      tmp->brother = tree->brother;
    }
  }
  
  tree->parent->child = NULL;

  free(tree);
}
