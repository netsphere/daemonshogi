/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"
#include "ui.h"

/**
 * 歩が前に進んだときにポインタを加算するか？のフラグ。
 * 定義されていたら加算する。
 */
#undef USE_ADD_FU_ADVANCE_POINT

/**
 * 桂、香車は上に行く程ポイントを減らすか？のフラグ。
 * 定義されていたら減らす。
 */
#undef USE_DEC_KEI_KYO_POINT


extern const int arr_piece[];
extern const int arr_point[];
extern const int arr_xy[];
extern const int arr_point_xy[17][9];
extern const int arr_point_xy2[17][9];

/**
 * 盤面の駒の種類位置、持駒の数から盤面評価点数を計算して返す。
 * @param bo 対象のBOARD
 * @param next 計算する手番. boの手番と同じとは限らない
 * @return 盤面評価点数
 */
int grade(const BOARD* bo, int next)
{
  int point, p, piece_p, n;
  int king_xy, kx, ky;
  int jx, jy;
  int x, y;
  int v, w;
  int i, j;

  assert( bo != NULL );
  assert( next == 0 || next == 1 );

  /* 自玉の位置 */
  king_xy = bo->code_xy[1 + next][0];
  kx = king_xy & 0xF;
  ky = king_xy >> 4;

  /* 相手玉の位置 */
  Xy oppo_king_xy = bo->code_xy[1 + (next==0)][0];
  jx = king_xy & 0xF;
  jy = king_xy >> 4;

  point = 0;

  for (i = 1; i <= 81; i++) {
    p = boGetPiece(bo, arr_xy[ i ]);
    if ( p && getTeban(p) == next )
      point += add_grade( bo, arr_xy[i], next );

#if 0      
    if ( p == 0 || getTeban( p ) != next || p == OH || p == (OH | 0x10))
      continue;
    
    n = boGetNoBoard(bo, arr_xy[ i ]);

    piece_p = arr_point[ p ];
    point += piece_p;
    /* printf("%02X : %d\n", p, piece_p); */

    x = arr_xy[ i ] & 0xF;
    y = arr_xy[ i ] >> 4;

    /* 歩は上へ行く程ポイントが高い */
#ifdef USE_ADD_FU_ADVANCE_POINT
    if ( (p & 0xF) == FU ) {
      if ( next == SENTE ) {
	bo->point[ SENTE ] += (10-y) * FU_POSITION_POINT;
      } else {
	bo->point[ GOTE ] += y * FU_POSITION_POINT;
      }
    }
#endif /* USE_ADD_FU_ADVANCE_POINT */
#ifdef USE_DEC_KEI_KYO_POINT
    if ( (p & 0xF) == KYO ) {
      /* 香車は上に動く程点数を減らす */
      if ( next == SENTE ) {
	point -= (10-y) * 10;
      } else {
	point -= y * 10;
      }
    } else if ( (p & 0xF) == KEI ) {
      /* 桂馬は上に動く程点数を減らす */
      if ( next == SENTE ) {
	point -= (10-y) * 10;
      } else {
	point -= y * 10;
      }
    }
#endif /* USE_DEC_KEI_KYO_POINT */

    /* 飛車、角の場合は位置によるポイント増減はしない */
    /* if ( ((p & HISYA) != HISYA) || ((p & KAKU) == KAKU) ) { */
    if ( ((p & 0xF) != HISYA) && ((p & 0xF) == KAKU) ) {
      v = abs(kx - x);
      if ( next == SENTE )
	w = ky - y;
      else 
	w = y - ky;
      
      /* point += arr_point_xy2[ 8 - w ][ v ]; */
      point += piece_p * ( arr_point_xy2[ 8 - w ][ v ] - 100 ) / MY_OH_DISTANCE_POINT;
    
      v = abs(jx - x);
      if ( next == SENTE )
	w = jy - y;
      else 
	w = y - jy;

      /* point += arr_point_xy[ 8 - w ][ v ]; */
      point += piece_p * ( arr_point_xy[ 8 - w ][ v ] - 100 ) / ONES_OH_DISTANCE_POINT;
    }

    if ( (p & 0xF) == HISYA && getTeban(p) == next ) {
      /* 「王飛接近するべからず」の格言通り王と飛車が近い場合は減点する */
      int hx, hy;

      hx = abs(kx - x);
      hy = abs(ky - y);
      if ( 4 < hx + hy ) {
	point -= ((hx + hy) * OH_HISYA_POINT);
      }
    }
    
    /* 駒の動ける数を評価に加える */
    point += bo_piece_kiki_count(bo, n) << NUM_OF_MOVE_POINT;
#endif // 0
  }

  for (i = 1; i<=7; i++) {
    for (j = 0; j < bo->inhand[next][i]; j++)
      point += score_inhand(bo, i, next);
  }
  
  /* 王の安全度の評価 */
  point -= add_oh_safe_grade(bo, next);

  // bo->point[ next ] = point;

  return point;
}


extern const int arr_point[];
extern const int arr_point_xy[17][9];
extern const int arr_point_xy2[17][9];

/**
 * 盤面評価点数に xy の位置の駒の点数を加える。
 * @param bo 対象のBOARD
 * @param xy 対象の駒の位置
 * @param next 盤面評価点数を増減する手番. boの手番と同じとは限らない
 */
int add_grade( const BOARD* bo, Xy xy, int next )
{
  Xy king_xy;
  int kx, ky;
  int jx, jy;
  int x, y;
  int v, w;
  PieceKind p;
  int piece_p, n;
  int score;

  assert( bo != NULL );
  assert( bo->board[xy] != 0 );
  assert( bo->board[xy] != WALL );
  assert( getTeban(bo->board[xy]) == next );
  assert( next == 0 || next == 1 );
  
  /* 自玉の位置 */
  king_xy = bo->code_xy[1 + next][0];
  kx = king_xy & 0xF;
  ky = king_xy >> 4;
  
  assert( bo->board[king_xy] == (0x08 + next * 0x10) );
  
  p = boGetPiece(bo, xy);
  if ( p == OH || p == (OH | 0x10) )
    return 0;
  
  n = boGetNoBoard(bo, xy);
  
  /* 相手玉の位置 */
  Xy oppo_king_xy = bo->code_xy[1 + (next==0)][0];
  jx = oppo_king_xy & 0xF;
  jy = oppo_king_xy >> 4;

  x = xy & 0xF;
  y = xy >> 4;

  // 基礎点数
  piece_p = arr_point[ p ];
  score = piece_p;

  /* 歩は上へ行く程ポイントが高い */
#ifdef USE_ADD_FU_ADVANCE_POINT
  if ( (p & 0xF) == FU ) {
    if ( next == SENTE ) {
      score += (10-y) * FU_POSITION_POINT;
    } else {
      score += y * FU_POSITION_POINT;
    }
  }
#endif /* USE_ADD_FU_ADVANCE_POINT */

#ifdef USE_DEC_KEI_KYO_POINT
  if ( (p & 0xF) == KYO ) {
    // 香車は上に動くほど点数を減らす
    if ( next == SENTE ) {
      score -= (10-y) * 5;
    } else {
      score -= y * 5;
    }
  } else if ( (p & 0xF) == KEI ) {
    /* 桂馬は上に動く程点数を減らす */
    if ( next == SENTE ) {
      score -= (10-y) * 5;
    } else {
      score -= y * 5;
    }
  }
#endif /* USE_DEC_KEI_KYO_POINT */

  /* 飛車、角の場合は位置によるポイント増減はしない */
  if ( (p & 0x7) != HISYA && (p & 0x7) != KAKU ) {
    v = abs(kx - x);
    if ( next == SENTE )
      w = ky - y;
    else 
      w = y - ky;

    /* bo->point[ next ] += arr_point_xy2[ 8 - w ][ v ]; */
    score += piece_p * ( arr_point_xy2[ 8 - w ][ v ] - 100 ) / MY_OH_DISTANCE_POINT;

    v = abs(jx - x);
    if ( next == SENTE )
      w = jy - y;
    else 
      w = y - jy;

    /* bo->point[ next ] += arr_point_xy[ 8 - w ][ v ]; */
    score += piece_p * ( arr_point_xy[ 8 - w ][ v ] - 100 ) / ONES_OH_DISTANCE_POINT;
  }
  
  if ( (p & 0x7) == HISYA && getTeban(p) == next ) {
    /* 「王飛接近するべからず」の格言通り王と飛車が近い場合は減点する */
    int hx, hy;
    
    hx = abs(kx - x);
    hy = abs(ky - y);
    if ( 4 > hx + hy ) {
      score -= (4 - (hx + hy)) * OH_HISYA_POINT;
    }
  }

  /* 駒の動ける数を評価に加える */
  // movables は boMove() で正しく更新できないので、think_eval() で計算する
  // score += bo_piece_kiki_count(bo, n) * NUM_OF_MOVE_POINT;

  return score;
}


extern const int arr_piece[];

/**
 * 持ち駒 piece 1枚の得点。
 * 増やすときはboを変更した後で呼び出すこと。減らすときはboを変更する前。
 * \todo 歩切れの評価など
 * @param bo 対象のBOARD
 * @param piece 取った駒
 * @param next 盤面評価点数を増減する手番. boの手番と同じとは限らない
 */
int score_inhand(const BOARD* bo, PieceKind piece, int next ) 
{
#ifdef DEBUG
  assert( bo != NULL );
  assert( 1 <= piece && piece <=7 );
  assert( next == 0 || next == 1 );
#endif /* DEBUG */

  return arr_piece[ piece ];
}


/**
 * 王の周りの安全度を評価する。
 * 
 * ・王の周り８マスに相手の駒の利きの数×１０点マイナス
 * 
 * @param bo 盤面
 * @param king_xy 調べたい玉の位置
 * @param next 利きを探す手番
 */
int oh_safe_grade(const BOARD* bo, int king_xy, int next) 
{
  int point;
  int i;

  assert( king_xy >= 0x11 && king_xy <= 0x99 );

  point = 0;
  
  for ( i=0; i<8; i++ ) {
    if (bo->board[king_xy + arr_round_to[i]] == WALL)
      continue; // TODO: いくらか足した方が？
    point += bo_attack_count(bo, next, king_xy + arr_round_to[ i ]);
  }

  return point * OH_SAFE_POINT;
}


/**
 * 王の安全度 (自玉の周りの相手による利き) の評価点数を盤面評価に足す。
 * @param bo 対象のBOARD
 * @param next 評価する手番
 */
int add_oh_safe_grade(const BOARD* bo, int next) 
{
  int king_xy;
  
  /* 自玉の位置 */
  king_xy = bo->code_xy[1 + next][0];
  return oh_safe_grade(bo, king_xy, (next == 0));
}

/**
 * 王の安全度の評価点数を盤面評価から引く。 
 * @param bo 対象のBOARD
 * @param next 評価する手番
 */
void sub_oh_safe_grade(BOARD *bo, int next) {
  int king_xy;
  
  /* 自玉の位置 */
  king_xy = bo->code_xy[1 + next][0];

  bo->point[ next ] -= oh_safe_grade(bo, king_xy, (next == 0));
}

#if 0
/**
 * 駒の動きの評価値を加点する。
 * @param bo 対象のBOARD
 * @param xy 評価する駒の位置
 * @param next 評価する手番
 */
void add_moving_grade(BOARD *bo, int xy, int next)
{
  // const KikiInfo* mi;
  int n;
  
  n = boGetNoBoard(bo, xy);
  // mi = boGetCodeTo(bo, n);
  bo->point[ next ] += bo_piece_kiki_count(bo, n) << 3;
}

/**
 * 駒の動きの評価値を減点する。
 * @param bo 対象のBOARD
 * @param xy 評価する駒の位置
 * @param next 評価する手番
 */
void sub_moving_grade(BOARD *bo, int xy, int next)
{
  // const KikiInfo* mi;
  int n;
  
  n = boGetNoBoard(bo, xy);
  // mi = boGetCodeTo(bo, n);
  bo->point[ next ] -= bo_piece_kiki_count(bo, n) << 3;
}
#endif
