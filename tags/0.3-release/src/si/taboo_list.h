/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <tr1/unordered_map>

/**
 * サイクルを検出するためのクラス
 * 必ずより深いものが見つかるから、登録時の深さは不要
 */
template <typename ValueType>
class TabooList
{
  typedef std::tr1::unordered_map<uint64_t, ValueType> Map;
  Map map_;

public:
  void add(const BOARD* bo, const ValueType& v) {
    map_.insert( typename Map::value_type(bo->hash_all, v) );
  }

  void remove(const BOARD* bo) {
    map_.erase(bo->hash_all);
  }

  /** @return 見つかったときtrue */
  bool lookup( const BOARD* bo, ValueType* v ) const {
    typename Map::const_iterator it;
    if ( (it = map_.find(bo->hash_all)) != map_.end() ) {
      *v = it->second;
      // v->phi = it->phi; v->delta = it->delta;
      return true;
    }
    return false;
  }
};

