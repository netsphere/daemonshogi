
# 持ち駒を含めたハッシュ値を作るための種を生成する

require 'digest/sha1'

print "#include <stdint.h>\n"
print "const uint64_t hashseed_inhand[2][8][19] = {\n"

key = 'xxx'
(0..1).each {
  print "{\n"
  (0..7).each {
    print "{ "
    print (0..18).map {
      key = Digest::SHA1.hexdigest(key)
      "0x#{key[0..15]}ULL"
    }.join(", ")
    print " },\n"
  }
  print "}, \n"
}

print "};\n"
