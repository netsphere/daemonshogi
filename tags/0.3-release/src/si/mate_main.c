/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * main.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/mate_main.c,v $
 * $Id: mate_main.c,v 1.1.1.1 2005/12/09 09:03:09 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include "getopt.h"
#include "si.h"
#include "ui.h"

#ifndef LINT
static const char copyright[] =
  "SI SHOGI " VERSION "-" REVISION " copyright(c)2000-2002 Masahiko Tokita";
#endif /* LINT */

void usage( void ) {
  fprintf( stderr, "si tsume shogi master\n" );
  fprintf( stderr, "usage : mate [-d num]\n" );
  fprintf( stderr, "        -d num    Debug lebel. Default is 0, max 5.\n" );
  fprintf( stderr, "        -n        Change display mode.\n" );
  fprintf( stderr, "        -h -v     Print help. This output.\n" );

  exit( 0 );
}


/** 開始 */
int main(int argc, char* argv[])
{
  // 検索木のルート
  TREE root;
  int i;
  char c;
  extern BOARD g_board;
  /* for getopt */
  extern char *optarg;
  extern int optind;

  extern int MATE_DEEP_MAX;
  extern int flgDisplayMode;
  
  // extern long count_new_board;
  extern long count_crash2;
  extern long count_hash_hit;
  extern long count_entry_hash;
  // extern long count_boMove;
  extern long G_COUNT_HASH_CHECK_MATE;
  extern long G_COUNT_HASH_NOT_CHECK_MATE;
  
  extern const unsigned long HASH_MAX;

  boInit( &g_board );
  
  while ( (c=si_getopt( argc, argv, "d:hm:nv")) != -1 ) {
    switch ( c ) {
    case 'm':
      MATE_DEEP_MAX = atoi( optarg );
      break;
    case 'n':
      flgDisplayMode = 1;
      break;
    case 'h':
    case 'v':
    default:
      usage();
      break;
    }
  }

  argc -= optind;
  argv += optind;

  if ( argv[ 0 ] == '\0' ) {
    if ( loadBOARD( "std.brd", &g_board ) ) {
      fprintf( stderr, "File not found.\n" );
      exit( 0 );
    }
  } else {
    if ( loadBOARD( argv[ 0 ], &g_board ) ) {
      fprintf( stderr, "File not found.\n" );
      exit( 0 );
    }
  }
  
   /* g_mi = (MOVEINFO *)malloc( sizeof(MOVEINFO) * 100 ); */

  boSetTsumeShogiPiece(&g_board);
  
  printf("hashmem : %ld\n", sizeof(HASH) * HASH_MAX / 1024 / 1024);

  putBOARD(&g_board);

  if (boCheckJustMate(&g_board) == 0) {
    printf("No good board file.\n");
    exit(1);
  }

#ifdef USE_HASH  
  /* ハッシュテーブル初期化 */
#ifdef DEBUG
  printf("start newHASH()\n");
#endif /* DEBUG */
  newHASH();
#ifdef DEBUG
  printf("end newHASH()\n");
#endif /* DEBUG */
#endif /* USE_HASH */

  initTREE( &root );

  for ( i=0; i<8; i++ ) {
    MATE_DEEP_MAX = i;
    printf("MATE_MAX : %d\n", i * 2 + 1);
    if ( mate(&g_board, &root, 0) ) {
      /* 詰んだ */
      printTREE(root.child, 0);
      break;
    }
  }
  
#ifdef USE_HASH  
  /* ハッシュテーブル解放 */
  freeHASH();
#endif /* USE_HASH */

  // printf("count_new_board : %ld\n", count_new_board);
  printf("count_crash2 : %ld\n", count_crash2);
  printf("count_hash_hit : %ld\n", count_hash_hit);
  printf("count_entry_hash : %ld\n", count_entry_hash);
  // printf("count_boMove : %ld\n", count_boMove);
  printf("G_COUNT_HASH_CHECK_MATE : %ld\n", G_COUNT_HASH_CHECK_MATE);
  printf("G_COUNT_HASH_NOT_CHECK_MATE : %ld\n", G_COUNT_HASH_NOT_CHECK_MATE);

  return 0;
}
