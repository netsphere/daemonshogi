/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * next_main.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/next_main.c,v $
 * $Id: next_main.c,v 1.1.1.1 2005/12/09 09:03:10 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include "getopt.h"
#include "si.h"
#include "ui.h"

#ifndef LINT
static const char copyright[] =
  "SI SHOGI " VERSION "-" REVISION " copyright(c)2000-2002 Masahiko Tokita";
#endif /* LINT */

void usage( void ) {
  fprintf( stderr, "si tsume shogi master\n" );
  fprintf( stderr, "usage : next [-d num]\n" );
  fprintf( stderr, "        -d num    Debug lebel. Default is 0, max 5.\n" );
  fprintf( stderr, "        -n        Change display mode.\n" );
  fprintf( stderr, "        -h -v     Print help. This output.\n" );

  exit( 0 );
}


/** 開始 */
int main(int argc, char* argv[])
{
  BOARD *bo = newBOARDwithInit();
  extern TE best_te;
  char c;
  
  /* for getopt */
  extern char *optarg;
  extern int optind;
  /* int i, x, y; */

  extern int MATE_DEEP_MAX;
  extern int flgDisplayMode;
  extern int THINK_DEEP_MAX;
  
  // extern long count_new_board;
  extern long count_crash2;
  extern long count_hash_hit;
  extern long count_entry_hash;
  // extern long count_boMove;

  while ( (c=si_getopt( argc, argv, "d:hm:nv")) != -1 ) {
    switch ( c ) {
     case 'm':
      MATE_DEEP_MAX = atoi( optarg );
      break;
     case 'n':
      flgDisplayMode = 1;
      break;
     case 'h':
     case 'v':
     default:
      usage();
      break;
    }
  }

  argc -= optind;
  argv += optind;

  if ( argv[ 0 ] == '\0' ) {
    if ( loadBOARD( "test.brd", bo ) ) {
      fprintf( stderr, "File not found.\n" );
      exit( 0 );
    }
  } else {
    if ( loadBOARD( argv[ 0 ], bo ) ) {
      fprintf( stderr, "File not found.\n" );
      exit( 0 );
    }
  }
  
  /* g_mi = (MOVEINFO *)malloc( sizeof(MOVEINFO) * 100 ); */

  bo->point[ 0 ] = grade(bo, 0);
  bo->point[ 1 ] = grade(bo, 1);
  putBOARD(bo);

#ifdef USE_HASH  
  /* ハッシュテーブル初期化 */
  newHASH();
#endif /* USE_HASH */


  THINK_DEEP_MAX = 5;
  if ( minmax( bo ) == SI_NORMAL ) {
    printTE( &best_te );
    boMove( bo, best_te );
    // bo->next = (bo->next == 0);
    putBOARD(bo);
  } else {
    putBOARD(bo);
    printf("TORYOU.\n");
  }
  printf("sente : %d\n", bo->point[ 0 ]);
  printf("gote  : %d\n", bo->point[ 1 ]);
  
  freeBOARD(bo);

#ifdef USE_HASH  
  /* ハッシュテーブル解放 */
  freeHASH();
#endif /* USE_HASH */

  // printf("count_new_board : %ld\n", count_new_board);
  printf("count_crash2 : %ld\n", count_crash2);
  printf("count_hash_hit : %ld\n", count_hash_hit);
  printf("count_entry_hash : %ld\n", count_entry_hash);
  // printf("count_boMove : %ld\n", count_boMove);

  return 0;
}
