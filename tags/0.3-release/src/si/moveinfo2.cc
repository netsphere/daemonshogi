/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "si.h"

/**
 * 新しくメモリを確保してMOVEINFOのポインタを返す。
 * @return MOVEINFO2のポインタ
 */
MOVEINFO2 *newMOVEINFO2(void) {
  MOVEINFO2 *mi;
  mi = (MOVEINFO2 *) malloc(sizeof(MOVEINFO2));
  if (mi == NULL) {
    si_abort("No enough memory. In moveinfo2.c#newMOVEFINO2()");
  }
  mi->count = 0;

  return mi;
}

/**
 * mi のメモリを解放する。
 * @param mi 対象のMOVEINFO2
 */
void freeMOVEINFO2(MOVEINFO2 *mi) {
#ifdef DEBUG
  assert(mi != NULL);
#endif /* DEBUG */
  
  free( mi );
} 

#ifndef USE_MISC_MACRO
/**
 * MOVEINFO2 に TE を追加する。
 * @param mi 対象のmi
 * @param te 追加するte
 */
void mi2Add(MOVEINFO2* mi, const TE* te) 
{
  assert(mi != NULL);
  assert(te != NULL);
  
  if ( MOVEINFO2MAX <= mi->count ) {
    printf("%s: error: array overflow.\n", __func__);
    return;
  }

  mi->te[mi->count++] = *te;
}
#endif /* USE_MISC_MACRO */

/**
 * src の内容を dst へコピーする。
 * @param src コピー元
 * @param dest コピー先
 */
void mi2Copy(MOVEINFO2* dest, const MOVEINFO2* src)
{
#ifdef DEBUG
  assert(dest != NULL);
  assert(src != NULL);
  assert(0 <= src->count && src->count <= MOVEINFO2MAX);
#endif /* DEBUG */
  memcpy(dest, src, sizeof(MOVEINFO2));
}

