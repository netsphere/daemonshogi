/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "si.h"
#include "ui.h"
#ifdef USE_SERIAL
#include "comaccess.h"
#endif /* USE_SERIAL */

/**
 * 初期化付きでGAMEを生成する。
 * @return GAMEのポインタ
 */
GAME *newGAME(void) {
  GAME *game;

  game = (GAME *) malloc(sizeof(GAME));
  if (game == NULL) {
    si_abort("No enough memory. In moveinfo.c#newGAME()");
  }

  initGAME(game);

  return game;
}

/**
 * game を初期化する。
 * @param gm 対象のGAME
 */
void initGAME(GAME *gm) {
#ifdef DEBUG
  assert(gm != NULL);
#endif /* DEBUG */

  gm->teai = HIRATE;
  gm->player_number[SENTE] = SI_COMPUTER_3;
  gm->player_number[GOTE]  = SI_COMPUTER_3;
  gm->si_input_next[SENTE] = si_input_next_computer;
  gm->si_input_next[GOTE]  = si_input_next_computer;
  gm->computer_level[SENTE] = 5;
  gm->computer_level[GOTE]  = 5;
#ifdef USE_SERIAL
  initCOMACCESS(&(gm->ca[SENTE]));
  initCOMACCESS(&(gm->ca[GOTE]));
#endif /* USE_SERIAL */
  gm->flg_run_out_to_lost = 1;
  gm->game_status = SI_NORMAL;
  gm->chudan = NULL;
}

/**
 * game のメモリを解放する。
 * @param game 対象のGAME
 */
void freeGAME(GAME *game) {
#ifdef DEBUG
  assert(game != NULL);
#endif /* DEBUG */
  
  free( game );
}

/**
 * 次の手を入力する関数へのポインタを返す。
 * @param gm 対象のGAME
 * @param next 次の手番
 * @return 次の手を入力する関数へのポインタ
 */ 
INPUTSTATUS (*gmGetInputNextPlayer(GAME *gm, int next))(BOARD *, TE *) {
  return gm->si_input_next[next];
}


#ifdef USE_SERIAL
/**
 * シリアルポートのデバイルファイルの名前を返す。
 * @param gm 対象のGAME
 * @param next 次の手番
 * @return 次の手を入力する関数へのポインタ
 */ 
char *gmGetDeviceName(GAME *gm, int next)
{
  return gm->ca[next].DEVICENAME;
}
#endif /* USE_SERIAL */


/**
 * コンピューターのレベルを返す。
 * @param gm 対象のGAME
 * @param next 次の手番
 * @return コンピューターのレベル
 */ 
int gmGetComputerLevel(GAME *gm, int next) {
  return gm->computer_level[next];
}

/**
 * 対戦を行うプレイヤーを設定する。
 * ３つ目の引数には次のいずれかをセットする。
 * <pre>
 * 人間による入力
 * SI_HUMAN
 * プログラムレベル１による入力
 * SI_COMPUTER_1
 * プログラムレベル２による入力
 * SI_COMPUTER_2
 * プログラムレベル３による入力
 * SI_COMPUTER_3
 * プログラムレベル４による入力
 * SI_COMPUTER_4
 * プログラムレベル５による入力
 * SI_COMPUTER_5
 * シリアルポートからの入力１
 * SI_SERIALPORT_1
 * シリアルポートからの入力２
 * SI_SERIALPORT_2
 * </pre>
 * 
 * @param gm 対象GAME
 * @param next 設定する手番
 * @param p 設定するプレイヤー
 */ 
void gmSetInputFunc(GAME* gm, int next, PLAYERTYPE p, InputCallbackFunc fn) 
{
  assert(gm != NULL);
  assert(next == SENTE || next == GOTE); 

  gm->player_number[next] = p;

  // 初期値
  if ( p == SI_HUMAN ) {
    gm->si_input_next[next] = si_input_next_human;
  } else if ( p == SI_COMPUTER_1 ) {
    gm->si_input_next[next] = si_input_next_computer;
    gm->computer_level[next] = 3;
  } else if ( p == SI_COMPUTER_2 ) {
    gm->si_input_next[next] = si_input_next_computer;
    gm->computer_level[next] = 4;
  } else if ( p == SI_COMPUTER_3 ) {
    gm->si_input_next[next] = si_input_next_computer;
    gm->computer_level[next] = 5;
  } else if ( p == SI_COMPUTER_4 ) {
    gm->si_input_next[next] = si_input_next_computer;
    gm->computer_level[next] = 6;
  } else if ( p == SI_COMPUTER_5 ) {
    gm->si_input_next[next] = si_input_next_computer;
    gm->computer_level[next] = 7;
  } 
#ifdef USE_SERIAL
  else if ( p == SI_SERIALPORT_1 ) {
    gm->si_input_next[next] = si_input_next_serialport;
  } else if ( p == SI_SERIALPORT_2 ) {
    gm->si_input_next[next] = si_input_next_serialport;
  } 
#endif /* USE_SERIAL */
  else if (p == SI_NETWORK_1)
    gm->si_input_next[next] = NULL;
  else if (p == SI_CHILD_PROCESS)
    gm->si_input_next[next] = NULL; // for bonanza
  else if ( p == SI_GUI_HUMAN )
    gm->si_input_next[next] = NULL;
  else {
    assert(0);
  }

  if (fn)
    gm->si_input_next[next] = fn;
}


#ifdef USE_SERIAL
/**
 * デバイス名をセットする。
 * @param gm 対象のGAME
 * @param next セットする手番 
 * @param s デバイス名
 */
void gmSetDeviceName(GAME *gm, int next, char *s)
{
  caSetDEVICENAME(&(gm->ca[next]), s);

}
#endif /* USE_SERIAL */


/**
 * ゲームのステータスをセットする。
 * @param gm 対象のGAME
 * @param status ゲームのステータス
 */
void gmSetGameStatus(GAME* gm, INPUTSTATUS status) 
{
#ifdef DEBUG
  assert(gm != NULL);
  assert(status == 0 || status == SI_CHUDAN || status == SI_TORYO);
#endif /* DEBUG */
  gm->game_status = status;
}

/**
 * ゲームのステータスを返す。
 * @param gm 対象のGAME
 * @return ゲームのステータス
 */
INPUTSTATUS gmGetGameStatus(GAME* gm) 
{
#ifdef DEBUG
  assert(gm != NULL);
  assert(gm->game_status == SI_NORMAL ||
	 gm->game_status == SI_CHUDAN ||
	 gm->game_status == SI_TORYO);
#endif /* DEBUG */
  return gm->game_status;
}

/**
 * 中断フラグへのポインタをセットする。
 * @param gm 対象のGAME
 * @param chudan 中断フラグへのポインタ
 */ 
void gmSetChudan(GAME *gm, int *chudan) {
  gm->chudan = chudan;
}

/**
 * 中断フラグへのポインタを返す。
 * @param gm 対象のGAME
 * @return 中断フラグへのポインタ
 */ 
int *gmGetChudan(GAME *gm) {
  return gm->chudan;
}

/**
 * 中断フラグの値を返す。
 * @param gm 対象のGAME
 * @return 中断フラグの値
 */ 
int gmGetChudanFlg(GAME *gm) {
  if (gm->chudan == NULL) {
    return 0;
  }
  
  return *gm->chudan;
}
