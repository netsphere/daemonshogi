/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "si.h"
#include "ui.h"
#ifdef USE_SERIAL
#include "comaccess.h"
#endif /* USE_SERIAL */

INPUTSTATUS (*si_input_next[2])(BOARD *bo, TE *te);

/**
 * 対局開始
 */
void play(BOARD *main_bo, GAME *gm) 
{
  char send_buf[ BUFSIZ ];
  TE te;
  extern TE best_te;
  extern int THINK_DEEP_MAX;
  int ret;
#ifdef DEBUG
  extern long count_minmax;
#endif /* DEBUG */
    
  printf("Welcome si shogi.\n");
  printf("---\n");
  putBOARD( main_bo );
  printf("\n");

  boPlayInit(main_bo);
  
  si_input_next[SENTE] = gmGetInputNextPlayer(gm, SENTE);
  si_input_next[GOTE]  = gmGetInputNextPlayer(gm, GOTE);

#ifdef USE_SERIAL
  if ( gmGetInputNextPlayer(gm, SENTE ) == si_input_next_serialport ) {
    caStart(&(gm->ca[SENTE]));
  }
  if ( gmGetInputNextPlayer(gm, GOTE ) == si_input_next_serialport ) {
    caStart(&(gm->ca[GOTE]));
  }
#endif /* USE_SERIAL */

  while ( 1 ) {
    THINK_DEEP_MAX = gmGetComputerLevel(gm, main_bo->next);
    /* 現在の時間を記録する */
    boSetNowTime(main_bo);
    ret = (*si_input_next[main_bo->next])(main_bo, &te);
    /* 消費時間を記録する */
    boSetUsedTime(main_bo);
#ifdef DEBUG
    printf("used_time : %ld\n", main_bo->used_time[main_bo->mi.count]);
#endif /* DEBUG */
#ifdef USE_SERIAL
    if ( ret == SI_NORMAL &&
	si_input_next[(main_bo->next == 0)] == si_input_next_serialport ) {
      /* 次の手番がシリアルポートの場合は手を送信する */
      getTE2SerialString(send_buf, main_bo, &te);
      caSend(&(gm->ca[(main_bo->next == 0)]), send_buf);
      printf("send_buf : %s\n", send_buf);
    }
#endif /* USE_SERIAL */

    /* ステータスの判定処理 */
    if ( ret == SI_TORYO) {
      play_toryo(main_bo, gm);
      break;
    } else if ( ret == SI_WIN ) {
      play_win(main_bo, gm);
      break;
    } else if ( ret == SI_CHUDAN ) {
      play_chudan(main_bo, gm);
      break;
    } else if ( ret == SI_CHUDAN_RECV ) {
      play_chudan_recv(main_bo, gm);
      break;
    } else if ( ret == SI_NGDATA ) {
      play_ngdata(main_bo, gm);
      break;
    } else {
#ifdef DEBUG
      /* 仕様上ありえない分岐 */
      assert( 1 );
#endif /* DEBUG */
    }
    printTE( &best_te );
    
    play_move( main_bo, &te );
    putBOARDforGAME( main_bo, gm );
#ifdef DEBUG
    printf("count_minmax : %ld\n", count_minmax);
#endif /* DEBUG */
  }
  
  CSA_output_auto(main_bo);
}


/** canvas.c から呼び出される */
void play_move(BOARD* main_bo, const TE* te) 
{
#ifdef DEBUG
  extern long G_COUNT_ALPHA_CUT;
  extern long G_COUNT_BETA_CUT;
#endif /* DEBUG */
  
  boMove( main_bo, *te );

  // main_bo->point[ 0 ] = grade(main_bo, 0);
  // main_bo->point[ 1 ] = grade(main_bo, 1);
  // main_bo->next = ( main_bo->next == 0 );
#ifdef DEBUG
  printf("sente : %d\n", main_bo->point[ 0 ]);
  printf("gote  : %d\n", main_bo->point[ 1 ]);
  printf("alpha cut : %ld\n", G_COUNT_ALPHA_CUT);
  printf("beta cut  : %ld\n", G_COUNT_BETA_CUT);
#endif /* DEBUG */
}

/**
 * ステータスが SI_TORYO だった時の処理。
 * @param bo 対象のBOARD
 */
void play_toryo(BOARD *bo, GAME *gm) {
#ifdef USE_SERIAL
  if (si_input_next[(bo->next == 0)] == si_input_next_serialport ) {
    caSend(&(gm->ca[(bo->next == 0)]), "%TORYO\n");
    caClose(&(gm->ca[(bo->next == 0)]));
  }
#endif /* USE_SERIAL */
#ifdef DEBUG
  printf("sente : %d\n", bo->point[ 0 ]);
  printf("gote  : %d\n", bo->point[ 1 ]);
#endif /* DEBUG */
  printf("%s win.(SI_TORYO)\n", bo->next == GOTE ? "sente" : "gote");
}

/**
 * ステータスが SI_WIN だった時の処理。
 * @param bo 対象のBOARD
 */
void play_win(BOARD *bo, GAME *gm) {
#ifdef DEBUG
  printf("sente : %d\n", bo->point[ 0 ]);
  printf("gote  : %d\n", bo->point[ 1 ]);
#endif /* DEBUG */
  printf("%s win.(SI_WIN)\n", bo->next == SENTE ? "sente" : "gote");
}

/**
 * ステータスが SI_CHUDAN だった時の処理。
 * @param bo 対象のBOARD
 */
void play_chudan(BOARD *bo, GAME *gm) {
  printf("CHUDAN by %s\n", bo->next == SENTE ? "SENTE" : "GOTE");
}

/**
 * ステータスが SI_CHUDAN_RECV だった時の処理。
 * @param bo 対象のBOARD
 */
void play_chudan_recv(BOARD *bo, GAME *gm) {
  printf("CHUDAN by %s\n", bo->next == SENTE ? "GOTE" : "SENTE");
}

/**
 * ステータスが SI_NGDATA だった時の処理。
 * @param bo 対象のBOARD
 */
void play_ngdata(BOARD *bo, GAME *gm) {
  printf("NGDATA by %s\n", bo->next == SENTE ? "SENTE" : "GOTE");
}

void putHelp(void) {
  printf("- command\n");
  printf("\n");
  printf("6978 move (6,9) to (7,8)\n");
  printf("34u1 koma uti (6,9) fu\n");
  printf("\n");
  printf("         fu    1 , kyo  2\n");
  printf("         kei   3 , gin  4\n");
  printf("         kin   5 , kaku 5\n");
  printf("         hisya 7\n");
  printf("\n");
  printf("quit  : Exit this program.\n");
  printf("disp  : Change to display mode.\n");
  printf("board : Display board.\n");
  printf("\n");
}


extern const char* CSA_PIECE_NAME[];

/**
 * シリアルポートに送信する手の文字列を生成する。
 * @param buf 生成した文字列を格納するポインタ
 * @param bo 対象のBOARD
 * @param te 変換する手
 */
void getTE2SerialString(char *buf, BOARD *bo, TE *te) 
{
  int p;
  
  /* 手番をセット */
  buf[ 0 ] = bo->next ? '-' : '+';

  /* 移動元の X 座標 */
  buf[ 1 ] = (te->fm & 0xF) + '0';
  /* 移動元の Y 座標 */
  buf[ 2 ] = (te->fm >> 4)  + '0';
  /* 移動先の X 座標 */
  buf[ 3 ] = (te->to & 0xF) + '0';
  /* 移動先の Y 座標 */
  buf[ 4 ] = (te->to >> 4)  + '0';
  
  boMove_mate(bo, *te );
  p = boGetPiece(bo, te->to) & 0xF;
  boBack_mate( bo );

#ifdef DEBUG
  assert( p != 0 );
  assert( p != WALL );
#endif /* DEBUG */
  
  strncpy(buf + 5, CSA_PIECE_NAME[p], 2);
  buf[ 7 ] = '\n';
  buf[ 8 ] = '\0';
}
