/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <assert.h>
#include "si.h"
#include "ui.h"
#include "taboo_list.h"
#include <algorithm>
using namespace std;

/* minmax.c の中ではハッシュを使わない */
// #undef USE_HASH

extern TE best_te;
// extern int MATE_DEEP_MAX;
extern GAME g_game;

#ifdef DEBUG
  extern long G_COUNT_ALPHA_CUT;
  extern long G_COUNT_BETA_CUT;
  extern long G_F_MIN_PUT_HASH;
  extern long G_F_MAX_PUT_HASH;
#endif /* DEBUG */


#if 1 // USE_BFS
static bool lookup_hash( const TabooList<int32_t>* taboo_list, const BOARD* bo,
                         int te_count, int32_t* score )
{
  if ( taboo_list->lookup(bo, score) ) {
    return true;
  }

  // ここが詰め将棋と共通化できない
  if ( hsGetBOARDMinMax(bo, score, NULL, te_count) )
    return true;

  // 仮評価からスタート
  *score = think_eval(bo);
  return false;
}


static void expand_and_best_move( const TabooList<int32_t>* taboo_list,
                                  BOARD* node,
                                  int te_count,
                                  int rest_depth, int rest_node,
                                  int32_t* best_child,
                                  int32_t* second_child,
                                  TE* best_move )
{
  MOVEINFO mi;
  make_moves(node, &mi);

  *best_child = +VAL_INF;

  if (!mi.count) {
    // 負け
    best_move->fm = 0; best_move->to = 0; best_move->nari = 0; best_move->uti = 0;
    return;
  }

  const TE* may_best = mi.te; // 手がある限り何か選ぶ

  for (int i = 0; i < mi.count; i++) {
    assert( mi.te[i].hint == 0 || mi.te[i].hint == TE_STUPID );
    if (mi.te[i].hint == TE_STUPID)
      continue;

    boMove(node, mi.te[i]);

    int32_t child_score;  // child nodeにとってよい値 -> 大きい
    if (rest_depth <= 1)
      child_score = think_eval(node);
    else
      lookup_hash( taboo_list, node, te_count, &child_score );

    if (child_score < *best_child) {
      // bestを更新
      *second_child = *best_child;
      *best_child = child_score;
      may_best = mi.te + i;
    }
    else if (child_score < *second_child) {
      // 2位のみ更新
      *second_child = child_score;
    }

    boBack(node);

    if (child_score <= -VAL_MAY_MATCH)
      break;  // nodeの手番側の勝ち確定
  }

  *best_move = *may_best;
}


static void bfs_search( TabooList<int32_t>* taboo_list, BOARD* node,
                        int32_t threshold_ceil, int32_t threshold_floor,
                        int te_count,
                        int depth, int* node_limit )
{
  // サイクル回避
  taboo_list->add(node, threshold_floor);

  int32_t best_child;
  TE best_move, prev_move;

  prev_move.fm = 0; prev_move.to = 0; prev_move.nari = 0; prev_move.uti = 0;

  while (true) {
    int32_t second_child;

    best_move.fm = 0; best_move.to = 0; best_move.nari = 0; best_move.uti = 0;

    // ノードを展開
    expand_and_best_move( taboo_list, node,
                          te_count,
                          depth, *node_limit,
                          &best_child,
                          &second_child,
                          &best_move );

    // 閾値を超えた？
    if ( -best_child <= threshold_floor || -best_child >= threshold_ceil )
      break;

    assert(best_move.to);

    // 先端 or 探索ノードを使い果たした
    if ( prev_move == best_move || *node_limit < 0 )
      break;

    boMove(node, best_move);

    int32_t threshold_child_ceil = min(-threshold_floor, second_child + 1);
    int32_t threshold_child_floor = -threshold_ceil;
    bfs_search( taboo_list, node, threshold_child_ceil, threshold_child_floor,
                te_count, depth - 1, node_limit );

    boBack(node);
    prev_move = best_move;
  }

  hsPutBOARDMinMax( node, -best_child, te_count, &best_move );
  (*node_limit)--;
  taboo_list->remove(node);
}


INPUTSTATUS bfs_minmax( BOARD* bo, int depth, TE* best_move )
{
  assert(best_move);

  TabooList<int32_t> taboo_list;
  int node_limit = 7000; // magic
  bfs_search( &taboo_list, bo, +VAL_MAY_MATCH, -VAL_MAY_MATCH,
              bo->mi.count, depth, &node_limit );

  if ( gmGetGameStatus(&g_game) == SI_CHUDAN )
    return SI_CHUDAN_RECV;

  int32_t score = 0;
  int r = hsGetBOARDMinMax( bo, &score, best_move, 0 );
  if (!r) {
    si_abort("internal error: not found result\n");
  }

  if (score <= -VAL_MAY_MATCH)
    return SI_TORYO;

  return SI_NORMAL;
}
#endif // USE_BFS


extern int THINK_DEEP_MAX;
extern TE best_te;
extern long count_minmax;

/**
 * alpha-beta pruning search
 * 自分の局面を探索する。盤面評価が大きくなるように手を選ぶ。
 * @param bo 対象のBOARD
 * @param depth 検索の深さ
 * @param alpha alpha値
 * @param beta beta値
 * @return
 */
static int nega_max(BOARD* bo, int depth, int alpha, int beta) 
{
  MOVEINFO *mi;
  int best_score, value;
  // int prev_alpha;
  int i;
  // int flg_tori;
#ifdef DEBUG
  int dpoint;
  extern long G_COUNT_ALPHA_CUT;
#endif /* DEBUG */

  // printf("%s: alpha = %d, beta = %d, depth = %d\n", 
  // __func__, alpha, beta, depth); // DEBUG

#ifdef USE_GUI  
  if ( depth < 2 ) {
    /* 中断や投了が入ってないか調べる */
    if ( gmGetGameStatus(&g_game) == SI_CHUDAN ||
	 gmGetGameStatus(&g_game) == SI_TORYO ||
	 gmGetChudanFlg(&g_game) == 1) {
      /* 中断または投了が入ったので終了する */
      return -30000;
    }
    /* GUIのペンディングされたイベントを処理する */
    processing_pending();
  }
#endif /* USE_GUI */

  // count_minmax++;

  if ( depth >= THINK_DEEP_MAX )
    return think_eval(bo);

  /* ハッシュに盤面が登録されていないか調べる */
#if 0
  if ( hsGetBOARDMinMax(bo, &point, bo->mi.count - depth, 0 ) ) {
    /* 登録されていた */
    return point;
  }
#endif /* USE_HASH */

  mi = newMOVEINFO();
  mi->count = 0;

  if ( depth == 0 && think_is_tsumero(bo, bo->next == 0) ) {
    // 詰めろを掛けられていて、こちらからの詰みがない
    think_tsumero_uke(bo, mi);
  }
  else
    think( bo, mi, depth );

  if ( mi->count == 0 ) {
    /* ルール上指せる手がない。敗け */
    freeMOVEINFO( mi );
    return -VAL_INF;
  }

  if ( depth == 0 ) 
    best_te = mi->te[ 0 ];
  best_score = alpha;

  // prev_alpha = alpha;

  for (i = 0; i < mi->count; i++) {
#ifdef DEBUG
    int j;
    dpoint = bo->point[ 0 ];
#endif /* DEBUG */

    boMove( bo, mi->te[i] );
    value = - nega_max(bo, depth + 1, -beta, -best_score);
    boBack( bo );

    if ( value > best_score ) {
      best_score = value;
      if ( depth == 0 )
        best_te = mi->te[ i ];

#if 0
    /* 一回でも alph値が更新されたらハッシュに登録する */
    hsPutBOARDMinMax(bo, point, bo->mi.count - depth, 0);
#endif /* USE_HASH */
    }

    /* 枝刈り */
    if ( best_score >= beta ) {
#if 0
      /* ここでハッシュへ局面と点数を記録する */
      hsPutBOARDMinMax(bo, value, bo->mi.count - depth, 0);
#endif /* USE_HASH */
#ifdef DEBUG
      G_COUNT_ALPHA_CUT++;
#endif /* DEBUG */
      /* printf("  alpha cut (alpha,beta)=(%d,%d)\n", alpha, beta); */

      break;
    }

#ifdef DEBUG    
    assert( dpoint == bo->point[ 0 ] );
#endif /* DEBUG */
  }

  freeMOVEINFO( mi );
  return best_score;
}


/**
 * 定跡手があるか調べてあったら te に格納する。
 * @param bo 対象のBOARD
 * @return 1 ならば定跡手があった。 0 ならばなし。
 */
static int jouseki(const BOARD* bo, TE* te)
{
  if ( bo->next == SENTE ) {
    if ( bo->mi.count == 0 ) {
      te->fm = 0x96;
      te->to = 0x87;
      te->nari = 0;
      te->uti = 0;
      return 1;
    } else if ( bo->mi.count == 2 ) {
      te->fm = 0x93;
      te->to = 0x84;
      te->nari = 0;
      te->uti = 0;
      return 1;
    }
  } else {
    if ( bo->mi.count == 1 ) {
      te->fm = 0x14;
      te->to = 0x23;
      te->nari = 0;
      te->uti = 0;
      return 1;
    } else if ( bo->mi.count == 3 ) {
      te->fm = 0x17;
      te->to = 0x26;
      te->nari = 0;
      te->uti = 0;
      return 1;
    }
  }
   
  return 0;
}


/**
 * min/max 探索を行う。
 * @param bo 対象のBOARD
 * @return 探索結果を返す。
 *         SI_NORMALならば通常の手。SI_WINならばプログラムの勝ち。
 *         SI_LOSTならばプログラムの負け。
 */
INPUTSTATUS minmax(BOARD* bo) 
{
  int alpha, beta;

#ifdef DEBUG
  G_COUNT_ALPHA_CUT = 0;
  G_COUNT_BETA_CUT = 0;
  G_F_MIN_PUT_HASH = 0;
  G_F_MAX_PUT_HASH = 0;
#endif /* DEBUG */

  if ( jouseki(bo, &best_te) ) {
    /* 定跡手があった */
     
    return SI_NORMAL;
  }

  best_te.fm = 0;
  best_te.to = 0;
  best_te.nari = 0;
  best_te.uti = 0;
  
  alpha = -VAL_INF;
  beta = +VAL_INF;

  // 詰みがあるか
  TE te_ret;
  if ( dfpn_mate(bo, 21, &te_ret) ) {
    /* check mate */
    printf("%s: checkmate.\n", __func__); // DEBUG

    best_te = te_ret;
    return SI_NORMAL;
  }

#if 0
  if ( think_hisshi(bo, 1, 5, &te_ret) ) {
      // 必至で勝ち
      best_te = te_ret;
      return SI_NORMAL;
    }
  }
#endif

  // 通常探索
  nega_max( bo, 0, alpha, beta );

  if ( gmGetGameStatus(&g_game) == SI_CHUDAN ) {
    return SI_CHUDAN_RECV;
  } 

  if ( best_te.to == 0 ) {
    /* 指し手がない。負け */
    return SI_TORYO;
  }

  return SI_NORMAL;
}


