/* -*- mode:c++ -*-
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _SI_H_
#define _SI_H_

#include <time.h>
#include <stdint.h>  // uint64_t
#include <stdbool.h> // bool
#include <assert.h>
#include <ostream>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef VERSION
#define VERSION "0.0.2"
#endif /* VERSION */

#ifndef REVISION
#define REVISION "unknown"
#endif /* VERSION */


//////////////////////////////////////////////////////////////////
// 開発のためのフラグ. ライブラリ利用者は変更不可

/** 検索中にハッシュを使う場合はこのフラグを定義する */
#define USE_HASH 1
// #undef USE_HASH

/** pin情報を使うか */
#undef USE_PIN

/** シリアルポートを使って対局する場合はこのフラグを定義する */
// #undef USE_SERIAL 


//////////////////////////////////////////////////////////////////
// ライブラリ利用者が選択する

/** 検索中にGUIのシグナルを処理する場合はこのフラグを定義する */
#define USE_GUI 1

/* マクロ関数を使うか？のフラグ。定義されていたらマクロを使う */
// #define USE_MISC_MACRO 1
#undef USE_MISC_MACRO

/** デフォルトの制限時間 */
#define DEFAULT_LIMIT_TIME 1500


/** 手番インデックス */
typedef enum {
  SENTE = 0,
  GOTE  = 1
} TEBAN;


/** 駒種. 引き算はない */
typedef uint16_t PieceKind;

/** 駒種の定義 */
typedef enum {
  RYU     = 0x0F,
  UMA     = 0x0E,
  // skip
  NARIGIN = 0x0C,
  NARIKEI = 0x0B,
  NARIKYO = 0x0A,
  TOKIN   = 0x09,
  OH      = 0x08,
  HISYA   = 0x07,
  KAKU    = 0x06,
  KIN     = 0x05,
  GIN     = 0x04,
  KEI     = 0x03,
  KYO     = 0x02,
  FU      = 0x01
} KOMATYPE;

#define WALL 0x20


/** mate() が返すステータス */
enum MATE_STATE {
  /** 不詰み */
  NOT_CHECK_MATE = 0,

  /** 詰みあり */
  CHECK_MATE = 1,

  /** 不明(ハッシュからデータを取り出すときだけ使う) */
  UNKNOWN_CHECK_MATE = -1,
};


/** 手合 */
typedef enum {
    /** 平手 */
  HIRATE,
  NIMAIOTI,
  YONMAIOTI,
  TEAI_ETC,
} TEAI;


/** si_input_next_* が返すゲームのステータス */
typedef enum {
  /** 通常の手 */
  SI_NORMAL = 0,
  /** 手番のプレイヤーが勝ち(相手が反則手を指した) */
  SI_WIN = 1, 
  /** 手番のプレイヤーが投了 */
  SI_TORYO = 2,
  /** 手番のプレイヤーが待った */
  SI_MATTA = 3,
  /** 手番のプレイヤーがゲームを中断 */
  SI_CHUDAN = 4,
  /** 手番のプレイヤーが中断を受信 */
  SI_CHUDAN_RECV = 5,
  /** 手番のプレイヤーが入玉で勝ちを宣言 */
  SI_KACHI = 6,
  /** 手番のプレイヤーが入玉で引き分けを宣言 */
  SI_HIKIWAKE = 7,
  /** 送信されてきた手が不正な手だった */
  SI_NGDATA = 8,
  /** 手番のプレイヤーが時間切れ負け */
  SI_TIMEOUT = 9
} INPUTSTATUS;

typedef enum {
  /** 人間による入力 */
  SI_HUMAN = 0,
  /** プログラムレベル１による入力 */
  SI_COMPUTER_1 = 1,
  /** プログラムレベル２による入力 */
  SI_COMPUTER_2 = 2,
  /** プログラムレベル３による入力 */
  SI_COMPUTER_3 = 3,
  /** プログラムレベル４による入力 */
  SI_COMPUTER_4 = 4,
  /** プログラムレベル５による入力 */
  SI_COMPUTER_5 = 5,
#ifdef USE_SERIAL
  /** シリアルポートからの入力１ */
  SI_SERIALPORT_1 = 6,
  /** シリアルポートからの入力２ */
  SI_SERIALPORT_2 = 7,
#endif
  /** ネットワークからの入力１ */
  SI_NETWORK_1 = 8,
  /** 子プロセスとパイプで通信 */
  SI_CHILD_PROCESS = 9,
  /** GUIを使った人間による入力 */
  SI_GUI_HUMAN = 10
} PLAYERTYPE;


/** 
 * 駒の評価点の基礎値.
 * \todo "si.h" の外へ 
 */
enum {
  /* 歩が上に行く程可算する点数 */
  FU_POSITION_POINT = 10,
  /* 自分の王への距離の評価点の率。減らす程点数が高くなる */
  MY_OH_DISTANCE_POINT = 200,
  /* 相手の王への距離の評価点の率。減らす程点数が高くなる */
  ONES_OH_DISTANCE_POINT = 100,
  /** 
   * 王の周りの安全度の点数。
   * 相手の利きが3ある場合は 3 × OH_SAFE_POINT を盤面評価から差し引く。
   */
  OH_SAFE_POINT = 100,

  /** 駒の動きの評価. (駒の動ける数 * NUM_OF_MOVE_POINT) を加算する。 */
  NUM_OF_MOVE_POINT = 4,

  /* 王と飛車が近いときに減点するポイント */
  OH_HISYA_POINT = 10,
};

/** 勝ちの値. 評価ルーチンでの値域の外側でなければならない */
#define VAL_INF 300000
#define VAL_MAY_MATCH (VAL_INF - 10)


/** 盤上に出ている駒以外の駒を格納する構造体 */
typedef struct {
  /** 駒の数を格納する */
  int count[9];
  /** 各駒の駒コードを格納する */
  int number[9][18];
} PBOX;


#define TE_STUPID (-1000)

typedef int16_t Xy;
typedef int16_t XyDiff;


/**
 * 指し手を表す構造体.
 *
 * \todo 取った駒 (captured) を記録するようにする. undoに必要. プレイヤー、成りは付けたまま.
 * \todo 移動する駒も記録する. utiの意味を変更し、打つ手はfmで判定するようにする。
 */
struct TE
{
  /** 移動元の場所 */
  Xy fm;

  /** 移動先の場所 */
  Xy to;

  /** 成り (promote) ならば true, それ以外は false をセットする */
  bool nari;

  /** 駒打ち (put) ならば打ち込む駒種、それ以外は 0 をセットする */
  PieceKind uti;

  /** ヒント情報. TE_STUPID */
  int hint;
};


/** 可能な合法手の最大は593 */
#define MOVEINFOMAX 593

/** 指し手 (候補手) を格納する構造体 */
typedef struct {
  /** 指し手 */
  TE te[MOVEINFOMAX + 1];
  /** 指し手の数 */
  int count;
} MOVEINFO;


#define MOVEINFO2MAX 2000

/** 指し手 (手順) を格納する構造体. */
typedef struct {
  /** 指し手 */
  TE te[MOVEINFO2MAX];

  /** 指し手の数 */
  int count;
} MOVEINFO2;


/** 移動先情報の最大引数 */
#define KIKI_FROM_MAX 10

typedef struct {
  Xy from[ KIKI_FROM_MAX ]; // short=10, long=5
  int count;
} KikiFrom;


/**
 * 局面の状態を表す構造体.
 * 局面 = 盤面 + 持ち駒 + 手番.
 */
struct BOARD
{
  /** 局面の情報。*/
  //@{

  /** 1行追加の余白 */
  PieceKind ban_padding__[16];

  /** 盤面の駒種. 
      インデックスは0x11..0x99まで。1一 -> 0x11, 9九 -> 0x99. 
      2六 -> 0x62 (yxの順).
      駒種は下から1-3ビットが素の駒種、ビット4が成り、ビット5が
      プレイヤ (1=後手) */
  PieceKind board[16 * 12];

  /** 持駒. [0]が先手、[1]が後手の持ち駒。1行128ビット
      手番、成りは戻して格納。 */
  int16_t inhand[2][8];

  /** 手番 */
  int next;

  /** 手順を記録しておく. 千日手検出 */
  MOVEINFO2 mi;

  /** 過去の局面のハッシュ値. [0] が初期局面の. \todo 非常に効率が悪い */
  uint64_t hash_history[ MOVEINFO2MAX + 1 ];

  //@}

  /** 利きなど、局面情報から生成できるもの */
  //@{

  /** 盤面上xyにある駒の駒コード. 次の関係がある。
      \code
        board[sq] = piece_kind
        noBoard[sq] = idx  // idx ... 玉=1..2, 飛=3..4, ... 歩=23..40
        code_xy[idx][0] = sq
        code_xy[idx][1] = piece_kind
      \endcode
  */
  int noBoard[16 * 10];

  /** 駒コード毎の駒種類と位置.
      インデックスは1始まり. 先手玉は[1][x], 後手玉は[2][x] で固定。 */
  int16_t code_xy[41][2];

  /** 盤上以外の駒の駒コード入れ. 盤上と合わせて40枚 */
  PBOX pb;

#ifdef USE_HASH
  /** ハッシュキー. 
      盤上の駒のみで、持ち駒は反映させない */
  uint64_t key;

  /** 持ち駒も含めたハッシュ値 */
  uint64_t hash_all;
#endif // USE_HASH

  /** 駒コードごとの利きの数. 自駒への利きもカウントする */
  int piece_kiki_count[41];

  /** それぞれの場所に対して利きを付けている駒の場所 */
  KikiFrom short_attack[2][16 * 10];
  KikiFrom long_attack[2][16 * 10];

#ifdef USE_PIN
  /** pinされていればその方向。(上からなら+16など。)
       たかだか一つの駒にしかpinされない。 */
  int pinned[16 * 10];
#endif

  //@}

  //////////////////////////////////////////////////////////////
  // 思考用

  /** 
   * 局面の評価点数. boMove() で更新できるものだけ. 
   * この変数を直接使わずに、think_eval() で評価を得ること 
   */
  int32_t point[2];

  /** 取った駒があったら覚えておく.
      \todo 指し手クラス TE へ移動 */
  PieceKind tori[MOVEINFO2MAX];

  /** 取った駒のカウント.
      \todo 指し手クラス TE へ移動 */
  int tori_count;

  /** 消費時間の合計 */
  time_t total_time[2];
  /** 制限時間 */
  time_t limit_time[2];
  /** 思考開始前の時刻 */
  time_t before_think_time;
  /** １手づつの消費時間 */
  time_t used_time[MOVEINFO2MAX];
};


/** 指し手情報をツリー状に格納するための構造体 */
struct tagTREE {
  /** 直前の一手 */
  TE te;
  /** 親へのポインタ */
  struct tagTREE *parent;

  /** 兄弟へのポインタ */
  struct tagTREE* brother;

  /** 先頭の子へのポインタ. 子はchildとそのbrother. */
  struct tagTREE* child;

  /** 盤面の点数 */
  int point;
};

typedef struct tagTREE TREE;


/** 置換表のエントリ. 持ち駒も区別する. PODでなければならない */
struct EntVariant 
{
  /** 持駒 (packed). ビットの数で表すわけではないので、
      計算できるのは一致のみ. */
  uint32_t packed_inhand[2];

  /** 得点 */
  int32_t phi, delta;

  /** 記録したときの手数 (深さではない) */
  int32_t te_count;

  /** 最善手 */
  uint32_t best_move;
};
#define HASH_ENT_SIZE 24 // x86-64: 8の倍数


/** 盤面を記録する置換表のエントリ. PODでなければならない */
typedef struct 
{
  /** 持ち駒を含まないハッシュ値.
   \todo 削除する？ */
  uint64_t key;

  /** 得点. 複数記録する */
  struct EntVariant ent[1];
} HASH;
#define HASH_HEADER_SIZE 8 // ent[]は除く


/** ゲームの進行に必要な情報を集めた構造体 */
typedef struct 
{
  /** 手合 */
  TEAI teai;

  /** player type */
  PLAYERTYPE player_number[2];

  /** 入力を受ける関数へのポインタ */
  INPUTSTATUS (*si_input_next[2])(BOARD *bo, TE *te);

  /** コンピュータの思考レベル */
  int computer_level[2];
#ifdef USE_SERIAL
  /** シリアルポートを使い通信対局するのための構造体 */
  COMACCESS ca[2];
#endif /* USE_SERIAL */
  /** 1 の場合、時間切れは負けとする */
  int flg_run_out_to_lost;

  /**
   * 中断等が入ったときに入力待ちループから抜けるためのフラグ。
   * 0 ならば通常通り。
   * SI_CHUDAN ならば中断が入った。SI_TORYO ならば投了。
   * 中断または投了が入った場合、入力待ちループからすぐ抜ける。
   */
  INPUTSTATUS game_status;
  
  /**
   * 中断フラグへのポインタ
   */
  int *chudan;
} GAME;


/* ------------------- function prototype ------------------- */

/* BOARD */

BOARD* newBOARDwithInit();

void freeBOARD(BOARD* bo);
void boInit(BOARD * bo);
void boPlayInit(BOARD *bo);
void boSetHirate(BOARD * bo);


/** 駒種をセットする */
static inline void boSetPiece(BOARD* bo, Xy xy, PieceKind p)
{
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( 1 <= (xy & 0xF) && (xy & 0xF) <= 9 );

  bo->board[xy] = p;
}


/**
 * xyにある駒の駒種を得る.
 * @param bo 対象のBOARD
 * @param xy 座標. 1手分だけはみ出してもよい
 */
static inline PieceKind boGetPiece(const BOARD* bo, Xy xy)
{
  assert(-1 <= (xy >> 4) && (xy >> 4) <= 11);
  assert(0 <= (xy & 0xF) && (xy & 0xF) <= 10);

  return bo->board[xy];
}


int boGetNoBoard(const BOARD* bo, int xy);


static inline int bo_piece_kiki_count(const BOARD* bo, int no) {
  assert( no >= 1 && no <= 40 );
  return bo->piece_kiki_count[no];
}


void boSetToBOARD(BOARD * bo);
void boSetToPBOX(BOARD *bo, int x, int y);

/** 局面を複製する */
BOARD* boCopy(const BOARD* bo);

/** 局面を手 te で進める. 局面 bo を更新する */
int boMove(BOARD* bo, const TE& te);

void boBack(BOARD* bo);

void boMove_mate(BOARD* bo, const TE& te);
void boBack_mate(BOARD* bo);

void boSetTsumeShogiPiece(BOARD *bo);

/** 手が合法手か */
bool is_valid_move(const BOARD* bo, const TE* te);

bool is_move_sennichite(const BOARD* bo, const TE* te, bool* forbidden);


/** 
 * 手番を入れ替える. 手数などは変更しない。
 * 明示的に呼び出すのはパスするときのみ。 
 */
void boToggleNext(BOARD* bo);

int can_move_promote(const BOARD* bo, const TE* te);

void boSetNowTime(BOARD *bo);
void boSetUsedTime(BOARD *bo);

/** 本将棋として局面が妥当かどうか */
int boCheckJustGame(const BOARD* bo);

int boCheckJustMate(const BOARD* bo);

/** sideによる攻撃を数える */
static inline int bo_attack_count(const BOARD* bo, int side, Xy xy) {
  assert(xy >= 0x11 && xy <= 0x99);
  assert(side == 0 || side == 1);
  return bo->short_attack[side][xy].count + bo->long_attack[side][xy].count;
}

void bo_update_board_by_csa_line(BOARD* bo, const char* s, int* error);

bool bo_set_by_text( BOARD* bo, const char* s);

int boCmp(const BOARD* bo1, const BOARD* bo2);


/* moveinfo.c */

MOVEINFO *newMOVEINFO(void);

void freeMOVEINFO(MOVEINFO *mi);

/** mi に手を追加する */
static inline void miAdd(MOVEINFO* mi, const TE* te) 
{
  assert( mi->count < MOVEINFOMAX );
  // assert( te->hint == 0 || te->hint == TE_STUPID );

  mi->te[mi->count++] = *te;
}

/** miに手を追加する。成らずと成りの両方。 */
void miAddWithNari(const BOARD* board, MOVEINFO* mi, const TE* te, int next);

void mi_add_with_pin_check( const BOARD* bo, MOVEINFO* mi, const TE* te );

void miRemoveTE(MOVEINFO *mi, TE *te );
void miRemoveTE2(MOVEINFO *mi, int num );
void miRemoveDuplicationTE(MOVEINFO *mi);

/** コピーする */
void miCopy(MOVEINFO* dest, const MOVEINFO* src);

void mi_print(const BOARD* bo, const MOVEINFO* mi);
bool mi_includes( const MOVEINFO* mi, const TE& te );

int norm_direc(Xy xy1, Xy xy2) __attribute__((const));


/* moveinfo2.c */

MOVEINFO2 *newMOVEINFO2(void);
void freeMOVEINFO2(MOVEINFO2 *mi);
void mi2Add(MOVEINFO2* mi, const TE* te);

void mi2Copy(MOVEINFO2* dest, const MOVEINFO2* src);


/* te.c: クラスTE関係 */

/** 手を生成 */
TE te_make_move(Xy from, Xy to, bool promote);

TE te_make_put(Xy to, PieceKind pie);

static inline bool te_is_put(const TE* te) {
  return te->fm == 0;
}

/** 指し手をpackする */
static inline uint32_t te_pack(const TE* te) {
  return (te->fm << 24) + (te->to << 16) + (te->nari << 8) + te->uti;
}

static inline TE te_unpack(uint32_t packed_te) {
  TE te;
  te.fm = (packed_te >> 24) & 0xff;
  te.to = (packed_te >> 16) & 0xff;
  te.nari = (packed_te >> 8) & 0xff;
  te.uti = packed_te & 0xff;
  return te;
}

int teCmpTE( const TE* te1, const TE* te2 );

inline bool operator == (const TE& x, const TE& y) {
  return !teCmpTE(&x, &y);
}

std::ostream& operator << (std::ostream& ost, const TE& te);


/** デバッグ用に手を文字列化 */
std::string te_str(const BOARD* bo, const TE* te);

void teSetTE(TE* te, Xy fm, Xy to, int nari, PieceKind uti);

int teCSAimoto(TE *te);
int teCSAisaki(TE *te);
int teCSAinaru(TE *te);
void teCSASet(TE *te, int imoto, int isaki);


/* moveto.cc */

extern const int remote_to_count[];
extern const int remote_to[][8];
extern const XyDiff normal_to[][8];
extern const int normal_to_count[];

/** 合法手をすべて生成する */
void make_moves(const BOARD* bo, MOVEINFO* mi);

/** xyにある駒での指し手をmiに追加 */
void moveto(const BOARD* bo, int xy, MOVEINFO* mi);

/** xyにある駒による長い利きを付ける */
void bo_add_long_attack(BOARD* bo, Xy xy);

/** xyにある駒による長い利きを取り除く */
void bo_remove_long_attack(BOARD* bo, Xy xy, PieceKind pie);

/** xyにある駒の利き情報を付ける */
void bo_movetoKiki(BOARD* bo, Xy xy);

/** xyにある駒の利き情報を取り除く */
void boPopToBoard(BOARD* bo, Xy xy);

void moveto2(const BOARD* bo, MOVEINFO* mi, Xy xy, int next);
void moveto2_add(const BOARD* bo, MOVEINFO* mi, Xy xy, int next);

/** 駒 pie を打つ手を生成 */
void append_a_put_move( const BOARD* bo, MOVEINFO* mi, PieceKind pie, Xy xy );

void append_put_moves( const BOARD* bo, Xy xy, MOVEINFO* mi );


/* pbox */
PBOX *newPBOX(void);
void freePBOX(PBOX *pb);
void pbInit(PBOX * pb);
void pbPush(PBOX * pb, int p);
int pbPop(PBOX * pb, int p);
int pbCheck(PBOX * pb);


/* ohte.cc */

void ohte(const BOARD* bo, MOVEINFO* mi);

void normalOhte(const BOARD *bo, MOVEINFO *mi, Xy king_xy);
void remoteOhte(const BOARD* bo, MOVEINFO *mi, Xy king_xy);
void keiOhte(const BOARD* bo, MOVEINFO* mi, Xy king_xy);
void utiOhte(const BOARD *bo, MOVEINFO *mi, Xy king_xy);
void UtiOhte2(const BOARD* bo, MOVEINFO* mi, Xy king_xy);
void akiOhte(const BOARD* bo, MOVEINFO* mi, Xy king_xy);


/* uke.c */

/** 王手を受ける手を生成する */
void uke(const BOARD* bo, MOVEINFO* mi );

void append_king_moves(const BOARD* bo, MOVEINFO* mi, Xy king_xy, int next);


/* mate.cc */

extern int MATE_DEEP_MAX;

MATE_STATE mate(BOARD *bo, TREE *tree, int gn);

MATE_STATE dfpn_mate( BOARD* bo, int depth, TE* best_move );


/* tree */
TREE *newTREE(void);
void initTREE(TREE *tree);
void freeTREE(TREE *tree);
void trSetMoveinfo(TREE *tree, MOVEINFO *mi);


/* hash.cc */

/** 置換表を初期化する */
void newHASH();

void freeHASH();

uint64_t hsGetHashKey(const BOARD* bo);

/** 局面を置換表に登録する。詰将棋用 */
void hsPutBOARD( int offense_side, const BOARD* bo, int32_t phi, int32_t delta,
                 int deep, const TE* best_move );

int hsGetBOARD( int offense_side, const BOARD* bo, int* phi, int* delta, TE* te,
                int req_deep );

void hsPutBOARDMinMax( const BOARD* bo, int32_t score, int deep, const TE* best_move );

int hsGetBOARDMinMax( const BOARD* bo, int32_t* score, TE* te, int te_count );

void bo_add_hash_value(BOARD* bo, Xy xy);
void bo_sub_hash_value(BOARD* bo, Xy xy);

int hs_entry_count(int tag);


/* grade */
extern const int arr_round_to[];
extern const int arr_round_to24[];
extern const int arr_xy[];
extern const int arr_point[]; // TODO: 思考ルーチンへ
extern const int arr_piece[]; // TODO: 思考ルーチンへ

int grade(const BOARD* bo, int next);
int add_grade(const BOARD* bo, Xy xy, int next );

int score_inhand( const BOARD* bo, PieceKind piece, int next );

int oh_safe_grade(const BOARD* bo, int king_xy, int next);

int add_oh_safe_grade(const BOARD* bo, int next);

void sub_oh_safe_grade(BOARD *bo, int next);
void add_moving_grade(BOARD *bo, int xy, int next);
void sub_moving_grade(BOARD *bo, int xy, int next);

/* search */
void search_011(BOARD *bo, MOVEINFO *mi);
void search_012(BOARD *bo, MOVEINFO *mi);
void search_013(BOARD *bo, MOVEINFO *mi);
void search_014(BOARD *bo, MOVEINFO *mi);
void search_022(BOARD *bo, MOVEINFO *mi);
void search_023(BOARD *bo, MOVEINFO *mi);
void search_024a(BOARD *bo, MOVEINFO *mi);
void search_027(BOARD *bo, MOVEINFO *mi);
void search_031a(BOARD *bo, MOVEINFO *mi);
void search_031b(BOARD *bo, MOVEINFO *mi);
void search_041(BOARD *bo, MOVEINFO *mi);
void search_044(BOARD *bo, MOVEINFO *mi);
void search_046(BOARD *bo, MOVEINFO *mi);
void search_047(BOARD *bo, MOVEINFO *mi);
void search_048(BOARD *bo, MOVEINFO *mi);
void search_049(const BOARD* bo, MOVEINFO* mi);
void search_053(BOARD *bo, MOVEINFO *mi);
void search_068(BOARD *bo, MOVEINFO *mi);
void search_pressure(const BOARD* bo, MOVEINFO* mi, int to_xy, int next);
void search_pressure_uti(BOARD *bo, MOVEINFO *mi, int to_xy, int next);


/* play */
void play(BOARD *bo, GAME *gm);

void play_move(BOARD* bo, const TE* te);

void getTE2SerialString(char *buf, BOARD *bo, TE *te);
void play_lost(BOARD *bo, GAME *gm);
void play_toryo(BOARD *bo, GAME *gm);
void play_win(BOARD *bo, GAME *gm);
void play_chudan(BOARD *bo, GAME *gm);
void play_chudan_recv(BOARD *bo, GAME *gm);
void play_ngdata(BOARD *bo, GAME *gm);
void putHelp(void);

/* think */
void think(BOARD *bo, MOVEINFO *mi, int depth);

bool think_is_tsumero(BOARD* bo, int teban);
void think_tsumero_uke(BOARD* bo, MOVEINFO* mi_ret);

bool think_hisshi(BOARD* bo, int tenuki, int depth, TE* te_ret);

int32_t think_eval(const BOARD* bo);


/* minmax */
INPUTSTATUS minmax(BOARD *bo);
INPUTSTATUS bfs_minmax(BOARD* bo, int depth, TE* best_move);

/* GAME */
GAME *newGAME(void);
void initGAME(GAME *game);
void freeGAME(GAME *game);

typedef INPUTSTATUS (*InputCallbackFunc)(BOARD* bo, TE* te);
InputCallbackFunc gmGetInputNextPlayer(GAME* gm, int next);

char *gmGetDeviceName(GAME *gm, int next);
int gmGetComputerLevel(GAME *gm, int next);

/** プレイヤー種別とコールバック関数を設定 */
void gmSetInputFunc(GAME* gm, int next, PLAYERTYPE p, InputCallbackFunc fn);

void gmSetDeviceName(GAME *gm, int next, char *s);

void gmSetGameStatus(GAME* gm, INPUTSTATUS status);

INPUTSTATUS gmGetGameStatus(GAME* gm);
void gmSetChudan(GAME *gm, int *chudan);
int *gmGetChudan(GAME *gm);
int gmGetChudanFlg(GAME *gm);

/* misc */
void si_abort(const char* s);
void time2string(char *buf, time_t t);

#ifdef USE_MISC_MACRO
/* macro */

#define mi2Add(m, t) ((m)->te[(m)->count++]=*(t))

#define getTeban(p) (((p)&0x10)==0x10)

#define boGetNoBoard(bo,xy) (bo->noBoard[(xy)])


#else /* USE_MISC_MACRO */
int piece(BOARD *b, int x, int y);

int getTeban(PieceKind p) __attribute__((const));

#endif /* USE_MISC_MACRO */
int notSentePiece(int p);
int notGotePiece(int p);

/** 歩がすでにないかどうか */
int checkFuSente(const BOARD* bo, int x);
int checkFuGote(const BOARD* bo, int x);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _SI_H_ */
