/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "si.h"


/**
 * 新しくメモリを確保してMOVEINFOのポインタを返す。
 * @return MOVEINFOのポインタ
 */
MOVEINFO* newMOVEINFO()
{
  MOVEINFO *mi;
  mi = (MOVEINFO *) malloc(sizeof(MOVEINFO));
  if (mi == NULL) {
    si_abort("No enough memory. In moveinfo.c#newMOVEFINO()");
  }
  mi->count = 0;

  return mi;
}


/**
 * mi のメモリを解放する。
 * @param mi 対象のMOVEINFO
 */
void freeMOVEINFO(MOVEINFO* mi)
{
  assert(mi != NULL);
  free( mi );
} 


/** 
 * xy1からxy2に向かう方向.
 * @return xy1 == xy2 のときまたは8方向以外のとき 0. 桂馬のときは常に0
 */
int norm_direc(Xy xy1, Xy xy2) // __attribute__((const))
{
  if (xy1 == xy2)
    return 0;

  int16_t x1 = (xy1 & 0xf);
  int16_t y1 = (xy1 >> 4);
  int16_t x2 = (xy2 & 0xf);
  int16_t y2 = (xy2 >> 4);

  if (x1 == x2)
    return y2 > y1 ? +16 : -16;

  if (y1 == y2)
    return x1 < x2 ? +1 : -1;

  if (x1 < x2) {
    if (x2 - x1 == y2 - y1)
      return +17;
    else if (x2 - x1 == -(y2 - y1))
      return -15;
    else
      return 0;
  }
  else { // x1 > x2
    if (x2 - x1 == y2 - y1)
      return -17;
    else if (x2 - x1 == -(y2 - y1))
      return +15;
    else
      return 0;
  }
}


#ifndef USE_PIN
/**
 * 局面 bo で指し手 te を指したときに、自玉に相手方の (長い) 利きが
 * 掛かってしまうか
 * @param bo 対象のBOARD
 * @param te 指そうとする手
 * @param king_xy 自玉の場所
 * @param next 手を指そうとするプレイヤ
 * @return 王手がなければ 0 を返す。あれば 1 以上を返す。
 */
static int is_sunuki(const BOARD* bo, const TE* te, Xy king_xy, int next) 
{
  assert(te->fm >= 0x11 && te->fm <= 0x99);

  BOARD* tmp_bo;
  int flag;
  int i;

  if (!king_xy) // 詰将棋で自玉がない
    return 0;

  int to_king_diff = norm_direc(te->fm, king_xy);
  if (!to_king_diff)
    return 0; // 玉と軸が違う

  int may_pinned = 0;
  for (i = 0; i < bo->long_attack[1 - next][te->fm].count; i++) {
    if ( norm_direc(bo->long_attack[1 - next][te->fm].from[i], te->fm) == 
	 to_king_diff ) {
      may_pinned = to_king_diff;
      break;
    }
  }
  if (!may_pinned)
    return 0;

  if ( norm_direc(te->fm, te->to) == may_pinned || 
       norm_direc(te->fm, te->to) == -may_pinned )
    return 0;
  
  // 実際に動かしてみる
  // TODO: 本当は利きだけ更新すればいい。

  tmp_bo = boCopy(bo);
  boMove_mate( tmp_bo, *te );

  flag = bo_attack_count(tmp_bo, next == 0, king_xy);

  freeBOARD(tmp_bo);

  return flag;
}
#endif // !USE_PIN


/** 玉の移動の手を生成 */
static void append_a_king_move(const BOARD* bo, MOVEINFO* mi, const TE* te)
{
  assert( te->nari == 0 );
  assert( te->hint == 0 || te->hint == TE_STUPID );

  if ( bo_attack_count(bo, bo->next == 0, te->to) )
    return;

  // 王手されていたか
  // 引く手だけダメ
  int i;
  for (i = 0; i < bo->long_attack[1 - bo->next][te->fm].count; i++) {
    int ng_direc = norm_direc( bo->long_attack[1 - bo->next][te->fm].from[i], 
                               te->fm );
    if ( ng_direc == te->to - te->fm )
      return;
  }

  miAdd( mi, te );
}


/**
 * 王が動く指し手を生成する
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param king_xy 王の位置
 * @param next 受け側の手番
 */
void append_king_moves(const BOARD* bo, MOVEINFO* mi, Xy king_xy, int next) 
{
  static const int tolist[] = {
    -17, -16, -15,
    -1, 1, 
    15, 16, 17,
  };
  TE te;
  int i;
/*  
  int ng_direc[2]; // 高々2つから王手される

  // 長い攻撃を受けている -> 動けない方向がある
  for (i = 0; i < bo->long_attack[1 - next][king_xy].count; i++) {
    ng_direc[i] = norm_direc(bo->long_attack[1 - next][king_xy].from[i], 
			     king_xy);
  }
*/
  te.fm = king_xy;
  te.nari = 0;
  te.uti = 0;
  te.hint = 0;
  for (i = 0; i < 8; i++) {
    PieceKind p;
/*
    for (j = 0; j < bo->long_attack[1 - next][king_xy].count; j++) {
      if ( tolist[i] == ng_direc[j] ) // 引く手だけ
        goto next;
    }
*/
    te.to = king_xy + tolist[i];
    p = bo->board[ te.to ];
    if ( p == WALL )
      continue;
    if ( p && getTeban(p) == next )
      continue;
/*
    if ( bo_attack_count(bo, next == 0, te.to) )
      continue;
*/
    append_a_king_move(bo, mi, &te);
  }
}


/**
 * MOVEINFO に指し手を追加する。
 * 歩、香、飛、角の成らずには STUPID フラグを付ける
 * 玉以外の駒を移動する手は必ずこの関数を通じて追加すること。(pinチェック)
 * @param board 現在の局面
 * @param mi 対象のmi
 * @param te_ 追加するte
 */
void mi_add_with_pin_check( const BOARD* bo, MOVEINFO* mi, const TE* te_ )
{
  PieceKind pie;
  TE te;
  
  assert(bo);
  assert(mi != NULL);
  assert(te_ != NULL);
  assert(1 <= (te_->fm >> 4) && (te_->fm >> 4) <= 9);
  assert(1 <= (te_->fm & 0xF) && (te_->fm & 0xF) <= 9);
  assert(1 <= (te_->to >> 4) && (te_->to >> 4) <= 9);
  assert(1 <= (te_->to & 0xF) && (te_->to & 0xF) <= 9);
  assert( bo->board[te_->fm] && getTeban(bo->board[te_->fm]) == bo->next );

  te = *te_;
  te.hint = 0;

  if ( (bo->board[te_->fm] & 0xf) == OH ) {
    append_a_king_move(bo, mi, &te);
    return;
  }

  // pinされていないか
#ifdef USE_PIN
  if ( bo->pinned[te_->fm] ) {
    if (bo->pinned[te_->fm] != norm_direc(te_->fm, te_->to) &&
	bo->pinned[te_->fm] != -norm_direc(te_->fm, te_->to) )
      return;
  }
#else
  if ( is_sunuki(bo, te_, bo->code_xy[1 + bo->next][0], bo->next) )
    return;
#endif

  pie = boGetPiece(bo, te.fm) & 0xF;
  if ( bo->next == GOTE ) {
    if ( !(te.fm >= 0x71 || te.to >= 0x71) ) {
      if (!te.nari) miAdd(mi, &te);
      return;
    }

    // 成り強制か、stupidか
    switch (pie) {
    case FU:
      if (te.to >= 0x91) {
        if (te.nari) miAdd(mi, &te);
      }
      else {
        if (te.nari) 
	  miAdd(mi, &te);
	else {
	  te.hint = TE_STUPID; miAdd(mi, &te);
	}
      }
      break;
    case KYO:
      if (te.to >= 0x91) {
        if (te.nari) miAdd(mi, &te);
      }
      else if (te.to >= 0x81) {  // 2段目のときのみ
	if (te.nari)
	  miAdd(mi, &te);
	else {
	  te.hint = TE_STUPID; miAdd(mi, &te);
	}
      }
      else
        miAdd(mi, &te);
      break;
    case KAKU: case HISYA:
      if (te.nari) 
	miAdd(mi, &te);
      else {
	te.hint = TE_STUPID; miAdd(mi, &te);
      }
      break;
    case KEI:
      if (te.to >= 0x81) {
        if (te.nari) miAdd(mi, &te);
      }
      else 
	miAdd(mi, &te);
      break;
    case GIN:
      miAdd(mi, &te);
      break;
    default: // 金、成り駒
      if (!te.nari) miAdd(mi, &te);
      break;
    }
  }
  else { // 先手
    if ( !(te.fm <= 0x39 || te.to <= 0x39) ) {
      if (!te.nari) miAdd(mi, &te);
      return;
    }

    switch (pie) {
    case FU:
      if (te.to <= 0x19) {
        if (te.nari) miAdd(mi, &te);
      }
      else {
        if (te.nari)
	  miAdd(mi, &te);
	else {
	  te.hint = TE_STUPID; miAdd(mi, &te);
	}
      }
      break;
    case KYO:
      if (te.to <= 0x19) {
        if (te.nari) miAdd(mi, &te);
      }
      else if (te.to <= 0x29) {  // 2段目のときのみ
        if (te.nari)
	  miAdd(mi, &te);
        else {
	  te.hint = TE_STUPID; miAdd(mi, &te);
	}
      }
      else
        miAdd(mi, &te);
      break;
    case KAKU: case HISYA:
      if (te.nari)
	miAdd(mi, &te);
      else {
        te.hint = TE_STUPID; miAdd(mi, &te);
      }
      break;
    case KEI:
      if (te.to <= 0x29) {
        if (te.nari) miAdd(mi, &te);
      }
      else
        miAdd(mi, &te);
      break;
    case GIN:
      miAdd(mi, &te);
      break;
    default: // 金、成り駒
      if (!te.nari) miAdd(mi, &te);
      break;
    }
  }
}


/**
 * MOVEINFO に指し手を追加する。成らずと成りの両方。
 * 歩、香、飛、角の成らずには STUPID フラグを付ける
 * 玉以外の駒を移動する手は必ずこの関数を通じて追加すること。(pinチェック)
 * @param board 現在の局面
 * @param mi 対象のmi
 * @param te_ 追加するte
 * @param next 手番
 */
void miAddWithNari(const BOARD* board, MOVEINFO* mi, const TE* te_, int next)
{
  PieceKind pie;
  TE te;
  
  assert(board);
  assert(mi != NULL);
  assert(te_ != NULL);
  assert(1 <= (te_->fm >> 4) && (te_->fm >> 4) <= 9);
  assert(1 <= (te_->fm & 0xF) && (te_->fm & 0xF) <= 9);
  assert(1 <= (te_->to >> 4) && (te_->to >> 4) <= 9);
  assert(1 <= (te_->to & 0xF) && (te_->to & 0xF) <= 9);
  assert( board->board[te_->fm] && getTeban(board->board[te_->fm]) == next );
  assert( board->next == next );

  te = *te_;
  te.hint = 0;

  if ( (board->board[te_->fm] & 0xf) == OH ) {
    append_a_king_move(board, mi, &te);
    return;
  }

  // pinされていないか
#ifdef USE_PIN
  if ( board->pinned[te_->fm] ) {
    if (board->pinned[te_->fm] != norm_direc(te_->fm, te_->to) &&
	board->pinned[te_->fm] != -norm_direc(te_->fm, te_->to) )
      return;
  }
#else
  if ( is_sunuki(board, te_, board->code_xy[1 + next][0], next) )
    return;
#endif

  pie = boGetPiece(board, te.fm) & 0xF;
  if ( next == GOTE ) {
    switch (pie) {
    case FU:
      if (te.to >= 0x91) {
        te.nari = true; miAdd(mi, &te);
      }
      else if (te.to >= 0x71) {
        te.nari = true; miAdd(mi, &te);
        te.nari = false; te.hint = TE_STUPID; miAdd(mi, &te);
      }
      else {
        te.nari = false; miAdd(mi, &te);
      }
      break;
    case KYO:
      if (te.to >= 0x91) {
        te.nari = true; miAdd(mi, &te);
      }
      else if (te.to >= 0x81) {  // 2段目のときのみ
        te.nari = true; miAdd(mi, &te);
        te.nari = false; te.hint = TE_STUPID; miAdd(mi, &te);
      }
      else if (te.to >= 0x71) {
        te.nari = true; miAdd(mi, &te);
        te.nari = false; miAdd(mi, &te);
      }
      else {
        te.nari = false; miAdd(mi, &te);
      }
      break;
    case KAKU: case HISYA:
      if ( 0x70 <= te.fm || 0x70 <= te.to ) {
        te.nari = true; miAdd(mi, &te);
        te.nari = false; te.hint = TE_STUPID; miAdd(mi, &te);
      }
      else {
        te.nari = false; miAdd(mi, &te);
      }
      break;
    case KEI:
      if (te.to >= 0x81) {
        te.nari = true; miAdd(mi, &te);
      }
      else if (te.to >= 0x71) {
        te.nari = true; miAdd(mi, &te);
        te.nari = false; miAdd(mi, &te);
      }
      else {
        te.nari = false; miAdd(mi, &te);
      }
      break;
    case GIN:
      if (te.fm >= 0x71 || te.to >= 0x71) {
        te.nari = true; miAdd(mi, &te);
      }
      te.nari = false; miAdd(mi, &te);
      break;
    default: // 金、玉、成り駒
      te.nari = false; miAdd(mi, &te);
      break;
    }
  }
  else { // 先手
    switch (pie) {
    case FU:
      if (te.to <= 0x19) {
        te.nari = true; miAdd(mi, &te);
      }
      else if (te.to <= 0x39) {
        te.nari = true; miAdd(mi, &te);
        te.nari = false; te.hint = TE_STUPID; miAdd(mi, &te);
      }
      else {
        te.nari = false; miAdd(mi, &te);
      }
      break;
    case KYO:
      if (te.to <= 0x19) {
        te.nari = true; miAdd(mi, &te);
      }
      else if (te.to <= 0x29) {  // 2段目のときのみ
        te.nari = true; miAdd(mi, &te);
        te.nari = false; te.hint = TE_STUPID; miAdd(mi, &te);
      }
      else if (te.to <= 0x39) {
        te.nari = true; miAdd(mi, &te);
        te.nari = false; miAdd(mi, &te);
      }
      else {
        te.nari = false; miAdd(mi, &te);
      }
      break;
    case KAKU: case HISYA:
      if (te.fm <= 0x39 || te.to <= 0x39) {
        te.nari = true; miAdd(mi, &te);
        te.nari = false; te.hint = TE_STUPID; miAdd(mi, &te);
      }
      else {
        te.nari = false; miAdd(mi, &te);
      }
      break;
    case KEI:
      if (te.to <= 0x29) {
        te.nari = true; miAdd(mi, &te);
      }
      else if (te.to <= 0x39) {
        te.nari = true; miAdd(mi, &te);
        te.nari = false; miAdd(mi, &te);
      }
      else {
        te.nari = false; miAdd(mi, &te);
      }
      break;
    case GIN:
      if (te.fm <= 0x39 || te.to <= 0x39) {
        te.nari = true; miAdd(mi, &te);
      }
      te.nari = false; miAdd(mi, &te);
      break;
    default: // 金、玉、成り駒
      te.nari = false; miAdd(mi, &te);
      break;
    }
  }
}


/**
 * miからteを削除して間が空いたら詰める。
 * @param mi 対象のMOVEINFO
 * @param te 対象のTE
 */
void miRemoveTE(MOVEINFO *mi, TE *te ) {
  int i;

#ifdef DEBUG
  assert( mi != NULL );
  assert( te != NULL );
#endif /* DEBUG */
  
  for (i=0; i<mi->count; i++) {
    if ( mi->te[ i ].fm == te->fm &&
	mi->te[ i ].to == te->to &&
	mi->te[ i ].nari == te->nari &&
	mi->te[ i ].uti == te->uti ) {
      if ( mi->count - 1 == i ) {
	mi->count--;
	return;
      }
      
      mi->te[ i ] = mi->te[ mi->count - 1 ];
      mi->count--;
      return;
    }
  }
}


/**
 * 手の番号を指定してmiからteを削除して間が空いたら詰める。
 * @param mi 対象のMOVEINFO
 * @param num 削除する手の番号
 */
void miRemoveTE2( MOVEINFO* mi, int num ) 
{
#ifdef DEBUG
  assert( mi != NULL );
  assert( 0 <= num && num <= mi->count );
#endif /* DEBUG */
  
  if ( mi->count - 1 == num ) {
    mi->count--;
    return;
  }
      
  mi->te[ num ] = mi->te[ mi->count - 1 ];
  mi->count--;
  return;
}


/**
 * miから重複する手を削除する。削除するときは先に登録された手を残す。
 * @param mi 対象のMOVEINFO
 */ 
void miRemoveDuplicationTE(MOVEINFO* mi) 
{
  int i, j;

#ifdef DEBUG
  assert( mi != NULL );
#endif /* DEBUG */

  for ( i=0; i<mi->count; i++ ) {
    for ( j=i+1; j<mi->count; ) {
      if ( ! teCmpTE( &(mi->te[ i ]), &(mi->te[ j ]) ) ) {
	miRemoveTE2( mi, j );
      } else {
	j++;
      }
    }
  }
}


/**
 * src の内容を dst へコピーする。
 * @param src コピー元
 * @param dest コピー先
 */
void miCopy(MOVEINFO* dest, const MOVEINFO* src)
{
  assert(dest != NULL);
  assert(src != NULL);
  // assert(0 <= src->count && src->count <= MOVEINFOMAX);

  memcpy(dest, src, sizeof(MOVEINFO));
}


/** 手の一覧を表示する */
void mi_print(const BOARD* bo, const MOVEINFO* mi)
{
  for (int i = 0; i < mi->count; i++)
    printf( "%s ", te_str(bo, &mi->te[i]).c_str() );

  printf("\n");
}


/** @return 見つかったとき true */
bool mi_includes( const MOVEINFO* mi, const TE& te ) 
{
  for (int i = 0; i < mi->count; i++) {
    if ( !teCmpTE(&mi->te[i], &te) )
      return true;
  }
  return false;
}
