/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include "si.h"
#include "ui.h"
#include <algorithm>
using namespace std;

#include "hashseed_inhand.h"

/* -------------------------------------------------------- */
/* BOARDの初期化関係関数 */
/* -------------------------------------------------------- */


// extern long count_new_board;

/**
 * 新しくメモリを確保してBOARDのポインタを返す。
 * @return BOARDのポインタ
 */
static BOARD* newBOARD()
{
  BOARD* b;

  // count_new_board++;

  b = (BOARD*) malloc(sizeof(BOARD));
  if (b == NULL) {
    si_abort("No enough memory. In board.c#newBOARD()");
  }

  return b;
}


/**
 * 新しくメモリを確保してBOARDのポインタを返す。初期化付き。
 * @return BOARDのポインタ
 */
BOARD* newBOARDwithInit()
{
  BOARD* b = newBOARD();
  boInit(b);
  
  return b;
}


/**
 * bo のメモリを解放する。
 * @param bo 対象のBOARD
 */
void freeBOARD(BOARD* bo) 
{
  assert( bo != NULL );
  free(bo);
}


/** 
 * 盤面を初期化する。
 * bo 対象のBOARD
 */
void boInit(BOARD* bo) 
{
  int i, j;
  
  assert( bo != NULL );
  
  for (i = -16; i < 16 * 12; i++) {
    // 盤の外に WALL (0x20) で壁を作る
    bo->board[i] = WALL;
  }

  for (i = 0; i < 41; i++) {
    bo->code_xy[i][0] = 0;
    bo->code_xy[i][1] = 0;
    // bo->kiki[i].count = 0;
    bo->piece_kiki_count[i] = 0;
  }

  for (i = 1; i <= 9; i++) {
    for (j = 1; j <= 9; j++) {
      int sq = (i << 4) + j;
      
      // 中は 0 で初期化
      bo->board[sq] = 0;
      bo->noBoard[sq] = 0;

      // 利きをクリア
#ifdef USE_PIN
      bo->pinned[sq] = 0;
#endif
      bo->short_attack[0][sq].count = 0;
      bo->short_attack[1][sq].count = 0;
      bo->long_attack[0][sq].count = 0;
      bo->long_attack[1][sq].count = 0;
    }
  }
  
  bo->point[0] = 0;
  bo->point[1] = 0;

  // 駒はすべて駒箱に.
  pbInit(&(bo->pb));
  
  bo->mi.count = 0;
  for (i = 0; i<MOVEINFOMAX; i++) {
    bo->tori[ i ] = 0;
  }
  bo->tori_count = 0;
  
  bo->total_time[SENTE] = 0;
  bo->total_time[GOTE] = 0;
  bo->limit_time[SENTE] = DEFAULT_LIMIT_TIME;
  bo->limit_time[GOTE] = DEFAULT_LIMIT_TIME;
  bo->before_think_time = 0;
  for (i = 0; i<MOVEINFO2MAX; i++) {
    bo->used_time[i] = 0;
  }
  bo->next = 0;
}


/**
 * 対局用に利き情報や消費時間を初期化する。
 * この関数を呼び出す前に、盤上や持ち駒を設定しておく。
 * @param bo 対象のBOARD
 */
void boPlayInit(BOARD* bo) 
{
  // pbInit( &(bo->pb) );
  boSetToBOARD(bo);
  
  /* 消費時間に 0 をセットする */
  bo->total_time[0] = 0;
  bo->total_time[1] = 0;
  
  /* 持ち時間に1500秒(25分)をセットする */
  bo->limit_time[0] = 1500;
  bo->limit_time[1] = 1500;

  bo->mi.count = 0;
  bo->tori_count = 0;
}


/**
 * bo に平手の配置をセットする。
 * @param bo 対象のBOARD
 */
void boSetHirate(BOARD* bo) 
{
  /* 平手の配置パターン */
  static const PieceKind hirate[] = {
    0x12, 0x13, 0x14, 0x15, 0x18, 0x15, 0x14, 0x13, 0x12,
    0x00, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x17, 0x00,
    0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00,
    0x02, 0x03, 0x04, 0x05, 0x08, 0x05, 0x04, 0x03, 0x02,
  };
  int i, j;
  
  assert( bo != NULL );
  
  for (i = 0; i < 9; i++) {
    for (j = 0; j < 9; j++)
      /* 平手の配置を盤面にセットする */
      boSetPiece( bo, (i + 1) * 16 + (j + 1), hirate[i * 9 + j] );
  }
  for (i = 0; i <= 7; i++) {
    /* 持ち駒を初期化 */
    bo->inhand[0][i] = 0;
    bo->inhand[1][i] = 0;
  }
  bo->next = 0;

  // 利きを更新
  boSetToBOARD(bo);
}

/* -------------------------------------------------------- */
/* BOARDの初期化関係関数 ここまで*/
/* -------------------------------------------------------- */

/* -------------------------------------------------------- */
/* BOARDへのアクセスメソッド get***() set***() */
/* -------------------------------------------------------- */


/**
 * xy の位置の駒コードを返す。
 * @param bo 対象のBOARD
 * @param xy 調べる位置
 * @return 駒コード
 */
#ifndef USE_MISC_MACRO
int boGetNoBoard(const BOARD* bo, int xy) 
{
  assert(bo != NULL);
  assert(0 <= (xy >> 4) && (xy >> 4) <= 10);
  assert(0 <= (xy & 0xF) && (xy & 0xF) <= 10);

  return bo->noBoard[xy];
}
#endif /* USE_MISC_MACRO */



/* -------------------------------------------------------- */
/* BOARDへのアクセスメソッド get***() set***() ここまで */
/* -------------------------------------------------------- */


/**
 * 盤面データ board に基づいて駒コードcode_xyをセットする。
 * @param bo 対象のBOARD
 */
static void update_code_xy(BOARD* bo) 
{
  int x, y, n;
  
  assert( bo != NULL );
  
  for (y = 1; y <= 9; y++) {
    for (x = 1; x <= 9; x++) {
      PieceKind pie;
      if ((pie = bo->board[(y << 4) + x]) != 0) {
	n = pbPop(&bo->pb, pie);
	assert( n >= 1 && n <= 40 );
	bo->noBoard[(y << 4) + x] = n;
	bo->code_xy[n][0] = (y << 4) + x;
	bo->code_xy[n][1] = pie;
      } 
      else
	bo->noBoard[(y << 4) + x] = 0;
    }
  }
}


extern const int arr_xy[];
// extern const uint64_t hashseed_inhand[2][9][19];

/**
 * board に基づいて盤面すべての利き情報を設定する。
 * (盤面, 持ち駒, 手番) を更新した後で呼び出すこと。
 * @param bo BOARD
 */
void boSetToBOARD(BOARD* bo) 
{
  int x, y, i;
  int fm, nn;

  assert( bo != NULL );

  pbInit( &bo->pb );
  update_code_xy(bo);

  for (i = 1; i <= 81; i++) {
    bo->short_attack[0][ arr_xy[i] ].count = 0;
    bo->short_attack[1][ arr_xy[i] ].count = 0;
    bo->long_attack[ 0 ][ arr_xy[i] ].count = 0;
    bo->long_attack[ 1 ][ arr_xy[i] ].count = 0;
  }

  // 利きを更新
  for (y = 1; y <= 9; y++) {
    for (x = 1; x <= 9; x++) {
      fm = (y << 4) + x;
      if ( boGetPiece(bo, fm) == 0 )
        continue;
      nn = boGetNoBoard(bo, fm);
      bo_movetoKiki(bo, fm); // 利きを更新
    }
  }

#ifdef USE_HASH
  bo->key = hsGetHashKey(bo);

  int j, k;
  uint64_t inhand_hash = 0;
  for (i = 0; i < 2; i++) {
    for (j = 1; j <= 7; j++) {
      // 個数分だけ編み込む. 0のときは bo->key = bo->hash_all
      for (k = 0; k < bo->inhand[i][j]; k++)
        inhand_hash ^= hashseed_inhand[i][j][k + 1];
    }
  }

  if (bo->next) // 手番 = 後手
    bo->hash_all = bo->key ^ ~ inhand_hash;
  else
    bo->hash_all = bo->key ^ inhand_hash;

  bo->hash_history[0] = bo->hash_all;
#endif /* USE_HASH */

  // 得点を設定
  if ( bo->code_xy[1][0] && bo->code_xy[2][0] ) {
    bo->point[0] = grade(bo, 0);
    bo->point[1] = grade(bo, 1);
  }
}


/**
 * BOARDのコピーを作成する。
 */
BOARD* boCopy(const BOARD* src) 
{
  BOARD* dest;

  assert( src != NULL );
  
  dest = newBOARD();
  
  /* 構造体をコピーするのに memcpy() を使っているがちょっとあやしいかも。 */
  memcpy(dest, src, sizeof(BOARD));
  
  return dest;
}


/**
 * 持ち駒を増やす。
 * @param bo BOARD
 * @param p 持駒にする駒種類
 * @param n 持ち駒にする駒の駒コード
 */
static void inc_inhand(BOARD* bo, PieceKind p, int n) 
{
  assert( bo != NULL );
  assert( 1<=p && p<=0x1F );
  assert( p != 8 && p != 0x18 );

  int count = ++bo->inhand[bo->next][p & 0x07];
  pbPush( &bo->pb, n);

#ifdef USE_HASH  
  if (bo->next) 
    bo->hash_all ^= ~ hashseed_inhand[ bo->next][p & 7][ count ];
  else
    bo->hash_all ^= hashseed_inhand[ bo->next][p & 7][ count ];    
#endif
}


/**
 * 持ち駒を減らす.
 * @return 駒コード
 */
static int dec_inhand(BOARD* bo, PieceKind p)
{
#ifdef USE_HASH
  if (bo->next)
    bo->hash_all ^= ~ hashseed_inhand[ bo->next][p & 7][ bo->inhand[bo->next][p & 7]];
  else
    bo->hash_all ^= hashseed_inhand[ bo->next][p & 7][ bo->inhand[bo->next][p & 7]];
#endif

  bo->inhand[bo->next][p & 7]--;
  return pbPop( &bo->pb, p );
}

 
/**
 * xyに対して香、角、飛、馬、竜の利きがあった場合、それらの利きを外し
 * 利きがあった駒の場所を記録しておく。
 * @param bo 対象のBOARD
 * @param xy 場所
 * @param kiki_from xyに対して利きのある座標 (先手も後手も)
 * @return xyに対する香～龍の利きの数
 */
static int boPopRemotePiece(BOARD* bo, Xy xy, Xy* kiki_from) 
{
  int side;
  int kiki_count = 0;

  for (side = 0; side < 2; side++) {
    while (bo->long_attack[side][xy].count > 0) {
      Xy from_xy = bo->long_attack[side][xy].from[0];
      kiki_from[ kiki_count++ ] = from_xy;
      bo_remove_long_attack( bo, from_xy, bo->board[from_xy] );
    }
  }

  return kiki_count;
}


/** 駒を置き、周りからの利きを更新しない */
static void put_piece(BOARD* bo, int xy, PieceKind pie, int pie_n)
{
  assert(xy >= 0x11 && xy <= 0x99);
  assert(pie > 0);
  assert(pie_n > 0);

  bo->board[xy] = pie;
  bo->noBoard[xy] = pie_n;
  bo->code_xy[pie_n][0] = xy;
  bo->code_xy[pie_n][1] = pie;

  bo_movetoKiki(bo, xy); // xyからの利きを付ける

#ifdef USE_HASH
  /* ハッシュ値に te->to の位置の駒の値を足す */
  bo_add_hash_value(bo, xy);
#endif /* USE_HASH */
}


/** 駒を置き、利きを更新する */
static void put_piece_with_kiki(BOARD* bo, Xy xy, PieceKind pie, int pie_n)
{
  assert(xy >= 0x11 && xy <= 0x99);

  Xy kiki_from[10];
  int kiki_count;
  int i;

  kiki_count = boPopRemotePiece(bo, xy, kiki_from);

  put_piece(bo, xy, pie, pie_n);

  for (i = 0; i < kiki_count; i++)
    bo_add_long_attack(bo, kiki_from[i]); // 長い利きのみ復活
}


/** 駒を取り除き、周りからの利きを更新しない */
static void remove_piece(BOARD* bo, Xy xy)
{
#ifdef USE_HASH
  /* ハッシュ値に te->fm の位置の駒の値を引いておく */
  bo_sub_hash_value(bo, xy);
#endif /* USE_HASH */

  boPopToBoard(bo, xy); // xyからの利きを除く

  bo->code_xy[bo->noBoard[xy]][0] = 0;
  bo->board[xy] = 0;
  bo->noBoard[xy] = 0;
}


/** 駒を取り除き、利きを更新する */
static void remove_piece_with_kiki(BOARD* bo, Xy xy)
{
  Xy kiki_from[6];
  int kiki_count;
  int i;

  assert(bo);
  assert(bo->board[xy]);
  assert(xy >= 0x11 && xy <= 0x99);

#ifndef NDEBUG
  int orig_count = bo->long_attack[0][xy].count + bo->long_attack[1][xy].count;
#endif
  kiki_count = boPopRemotePiece(bo, xy, kiki_from);
#ifndef NDEBUG
  assert(kiki_count <= 6);
  assert(orig_count == kiki_count);
#endif
  remove_piece(bo, xy);

  // 周りからの長い利きを復活
  for (i = 0; i < kiki_count; i++)
    bo_add_long_attack( bo, kiki_from[i] );
}


#ifndef NDEBUG
static void assert_move(const BOARD* bo, const TE& te)
{
  // assert(bo->next == next);
  assert( 1 <= (te.to & 0x0f) && (te.to & 0x0f) <=9 );
  assert( 1 <= (te.to >> 4) && (te.to >> 4) <=9 );
  if ( !te_is_put(&te) ) {
    assert( 1 <= (te.fm & 0x0f) && (te.fm & 0x0f) <= 9 );
    assert( 1 <= (te.fm >> 4) && (te.fm >> 4) <=9 );

    assert( bo->board[ te.fm ] != 0 );

    // 移動先に自分の駒があってはいけない
    if ( bo->board[ te.to ] ) {
      if ( bo->board[ te.fm ] & 0x10 )
	assert( (bo->board[ te.to ] & 0x10) == 0 );
      else
	assert( (bo->board[ te.to ] & 0x10) == 0x10 );
    }

    /* 成れないのに成りにしていないか調べる */
    if ( te.nari ) {
      assert( (bo->board[ te.fm ] & 0x0F) <= 7 );
    }
  }
  else {
    assert(bo->inhand[ bo->next ][ te.uti ] > 0);

    assert(te.fm == 0);
    assert( bo->board[ te.to ] == 0 );

    assert(te.nari == 0);
  }
}
#endif // !NDEBUG


/*
 * BOARD上の駒を動かす。
 * @param bo BOARD
 * @param te 移動情報
 */
int boMove( BOARD* bo, const TE& te )
{
  int c, n;
  PieceKind p = 0;
  int flg_tori;

  // printf("\n%s: in: point = (%d, %d)\n", __func__, bo->point[0], bo->point[1]);

#ifndef NDEBUG
  assert_move(bo, te);
#endif /* !NDEBUG */
  
  flg_tori = 0;
  int next = bo->next;

  // 手順を記録
  mi2Add( &bo->mi, &te ); 

  // 王の安全度の評価を盤面評価に足す (戻す)
  bo->point[SENTE] += add_oh_safe_grade(bo, SENTE);
  bo->point[GOTE] += add_oh_safe_grade(bo, GOTE);
  
  if ( !te_is_put(&te) ) {
    /* 駒を移動する */

    p = bo->board[ te.fm ];
    n = bo->noBoard[ te.fm ];

    // 移動元.
    // printf("from:%d ", -add_grade(bo, te.fm, next));
    bo->point[next] -= add_grade(bo, te.fm, next );
    remove_piece_with_kiki(bo, te.fm);

    c = bo->board[ te.to ];
    if ( c != 0 ) {
      /* 移動先に駒があった */
      
      // 持ち駒に加える
      inc_inhand(bo, c, bo->noBoard[te.to]);
      // printf("inhand:%d ", +score_inhand(bo, c & 7, next));
      bo->point[next] += score_inhand( bo, c & 7, next );

      // 盤上から駒を取り除く
      // printf("remove_to(op):%d ", -add_grade(bo, te.to, (next == 0)));
      bo->point[1 - next] -= add_grade(bo, te.to, (next == 0));
      remove_piece(bo, te.to);

      /* 取られる駒を覚えておく */
      bo->tori[ bo->tori_count++ ] = c;
      flg_tori = 1;
    } 
    else {
      bo->tori[ bo->tori_count++ ] = 0;
    }

    // 移動先
    if ( te.nari )
      p += 8;

    if (flg_tori)
      put_piece(bo, te.to, p, n);
    else
      put_piece_with_kiki(bo, te.to, p, n);
    // printf("to:%d ", add_grade(bo, te.to, next));
    bo->point[next] += add_grade(bo, te.to, next );
  } 
  else {
    // 駒打ちの場合
    
    // 持ち駒を減らす
    // printf("dec_inhand:%d ", -score_inhand(bo, te.uti, next));
    bo->point[next] -= score_inhand( bo, te.uti, next );
    n = dec_inhand(bo, te.uti);

    // 駒を置く
    put_piece_with_kiki(bo, te.to, te.uti + (next << 4), n);
    // printf("put:%d ", add_grade(bo, te.to, next));
    bo->point[next] += add_grade(bo, te.to, next);
    
    bo->tori[ bo->tori_count++ ] = 0;
  }
  
  /* 王の安全度の評価を盤面評価から引く */
  sub_oh_safe_grade(bo, SENTE);
  sub_oh_safe_grade(bo, GOTE);

  boToggleNext(bo);
  bo->hash_history[bo->mi.count] = bo->hash_all;

  if ((p & 0xf) == OH) {
    bo->point[0] = grade(bo, SENTE);
    bo->point[1] = grade(bo, GOTE);
  }

#ifndef NDEBUG
  // if ( !te_is_put(&te) ) {
  int nn = bo->noBoard[te.to];
  assert(bo->code_xy[nn][0] == te.to);
  assert(bo->code_xy[nn][1] == bo->board[te.to]);
  //}
#endif
  // printf("\n%s: out: point = (%d, %d)\n", __func__, bo->point[0], bo->point[1]);

  return flg_tori;
}


#ifndef NDEBUG
static void assert_back(const BOARD* bo)
{
  assert( bo != NULL );
  // assert(bo->next == next);
  assert( bo->mi.count > 0 );
  assert( bo->tori_count > 0 );
}
#endif // !NDEBUG


/**
 * 一手戻す。
 * @param bo 対象のBOARD
 */
void boBack( BOARD* bo ) 
{
  PieceKind p = 0;
  int n;
  PieceKind cap;

  // printf("\n%s: in: point = (%d, %d)\n", __func__, bo->point[0], bo->point[1]);
  
#ifndef NDEBUG
  assert_back(bo);
#endif /* !NDEBUG */

  boToggleNext(bo);
  int next = bo->next;

  const TE& te = bo->mi.te[ --bo->mi.count ];
  cap = bo->tori[ --bo->tori_count ];
  
  /* 王の安全度の評価を盤面評価に足す */
  bo->point[SENTE] += add_oh_safe_grade(bo, SENTE);
  bo->point[GOTE] += add_oh_safe_grade(bo, GOTE);
  
  if ( !te_is_put(&te) ) {
    p = bo->board[ te.to ];
    n = bo->noBoard[ te.to ];

    /* 盤面評価点数から te.to の位置の駒の点数を引く */
    // printf("to:%d ", -add_grade(bo, te.to, next));
    bo->point[next] -= add_grade(bo, te.to, next );

    if (cap) {
      // 駒を取っていた場合
      remove_piece(bo, te.to);

      // 持ち駒から減らす
      // printf("inhand:%d ", -score_inhand(bo, cap & 7, next));
      bo->point[next] -= score_inhand(bo, cap & 7, next);
      int cap_n = dec_inhand(bo, cap);

      // 駒を戻す
      put_piece(bo, te.to, cap, cap_n);
      // printf("restore(op):%d ", add_grade(bo, te.to, (next == 0)));
      bo->point[1 - next] += add_grade(bo, te.to, (next == 0));
    }
    else
      remove_piece_with_kiki(bo, te.to);

    // 移動元
    if ( te.nari )
      p &= 0x17;
    put_piece_with_kiki(bo, te.fm, p, n);
    // printf("from:%d ", add_grade(bo, te.fm, next));
    bo->point[next] += add_grade(bo, te.fm, next);
  } 
  else {
    /* 駒打ちだった場合 */

    p = bo->board[ te.to ];
    n = bo->noBoard[ te.to ];
    
    // 取り除く
    // printf("remove:%d ", -add_grade(bo, te.to, next));
    bo->point[next] -= add_grade(bo, te.to, next);
    remove_piece_with_kiki(bo, te.to);

    // 持ち駒に戻す
    inc_inhand(bo, te.uti, n);
    // printf("to_inhand:%d ", +score_inhand(bo, te.uti, next));
    bo->point[next] += score_inhand(bo, te.uti, next);
  }
  
  /* 王の安全度の評価を盤面評価から引く */
  sub_oh_safe_grade(bo, SENTE);
  sub_oh_safe_grade(bo, GOTE);

  if ( (p & 0xf) == OH ) {
    bo->point[0] = grade(bo, SENTE);
    bo->point[1] = grade(bo, GOTE);
  }
  // printf("\n%s: out: point = (%d, %d)\n", __func__, bo->point[0], bo->point[1]);
}


/*
 * BOARD上の駒を動かす。
 * @param bo BOARD
 * @param te 移動情報
 * @param next 手番
 */
void boMove_mate(BOARD* bo, const TE& te)
{
  int c, n, p;
  int flg_tori;

#ifndef NDEBUG
  assert_move(bo, te);
#endif /* !NDEBUG */
  
  flg_tori = 0;

  // count_boMove++;
  mi2Add( &bo->mi, &te );
  
  if ( !te_is_put(&te) ) {
    /* 駒を移動する */

    p = bo->board[ te.fm ];
    n = bo->noBoard[ te.fm ];

    // 移動元
    remove_piece_with_kiki(bo, te.fm);

    c = bo->board[ te.to ];
    if ( c != 0 ) {
      /* 移動先に駒があった */
      
      // 持ち駒に加える
      inc_inhand(bo, c, bo->noBoard[te.to]);

      remove_piece(bo, te.to);

      /* 取られる駒を覚えておく */
      bo->tori[ bo->tori_count++ ] = c;
      flg_tori = 1;
    } 
    else  {
      bo->tori[ bo->tori_count++ ] = 0;
      flg_tori = 0;
    }

    // 移動先    
    if (te.nari)
      p += 8;

    if (flg_tori)
      put_piece(bo, te.to, p, n);
    else
      put_piece_with_kiki(bo, te.to, p, n);
  } 
  else {
    /* 駒打ちの場合 */
    n = dec_inhand(bo, te.uti);

    put_piece_with_kiki(bo, te.to, te.uti + (bo->next << 4), n);

    bo->tori[ bo->tori_count++ ] = 0;
  }

  boToggleNext(bo);
  bo->hash_history[bo->mi.count] = bo->hash_all;
}


/**
 * 一手戻す。
 * @param bo 対象のBOARD
 */
void boBack_mate( BOARD* bo )
{
  int p, n, t;
  
#ifndef NDEBUG
  assert_back(bo);
#endif /* !NDEBUG */

  boToggleNext(bo);

  const TE& te = bo->mi.te[ --bo->mi.count ];
  t = bo->tori[ --bo->tori_count ];

  if ( !te_is_put(&te) ) {
    p = bo->board[ te.to ];
    n = bo->noBoard[ te.to ];

    if ( t ) {
      /* 駒を取っていた場合 */

      remove_piece(bo, te.to);

      int cap_n = dec_inhand(bo, t);

      put_piece(bo, te.to, t, cap_n);
    }
    else
      remove_piece_with_kiki(bo, te.to);

    // 移動元
    if ( te.nari ) 
      p &= 0x17;
    put_piece_with_kiki(bo, te.fm, p, n);
  } 
  else {
    /* 駒打ちだった場合 */
#ifndef NDEBUG
    assert( 1 <= te.uti && te.uti <= 7 );
    assert( bo->board[ te.to ] != 0 );
    assert( bo->board[ te.to ] != WALL );
#endif /* DEBUG */
    
    p = bo->board[ te.to ];
    n = bo->noBoard[ te.to ];

    remove_piece_with_kiki(bo, te.to);

    inc_inhand(bo, te.uti, n);
  }
}


/**
 * bo1 と bo2 を比較する。
 *
 * @param bo1 対象のBOARD
 * @param bo2 対象のBOARD
 * @return 等しければ0を、違いがあれば0以外を返す
 */
int boCmp(const BOARD* bo1, const BOARD* bo2) 
{
#ifdef USE_HASH
  if ( bo1->hash_all != bo2->hash_all )
    return 1;
#endif

  if ( memcmp( bo1->board, bo2->board, sizeof( bo1->board ) ) )
    return 1;

  if ( memcmp( bo1->inhand[0], bo2->inhand[0], sizeof( bo1->inhand[0] ) ) )
    return 1;

  if ( memcmp( bo1->inhand[1], bo2->inhand[1], sizeof( bo1->inhand[1] ) ) )
    return 1;

  return 0;
}


/**
 * 使っていない駒を詰将棋用に受け側の持駒としてセットする。
 * この関数を呼び出した後で、boPlayInit() なり boSetToBOARD() を呼び出して
 * ハッシュを更新すること。
 * @param bo 対象のBOARD
 */
void boSetTsumeShogiPiece(BOARD* bo)
{
  static const int c[] = {
    0, 
    18, 4, 4, 4, 4, 2, 2,
  };
  int i;

  for ( i = 1; i <= 7; i++ ) {
    bo->inhand[ (bo->next == 0) ][ i ] =
      c[ i ] - bo->pb.count[ i ] - bo->inhand[ bo->next ][ i ];
  }
}


/**
 * ルール上正しい１手かチェックする。
 * @param bo 対象のBOARD
 * @param te 調べる一手
 * @return 正しければ true を返す。正しくなければ false を返す。
 */
bool is_valid_move(const BOARD* bo, const TE* te)
{
  MOVEINFO mi;

  make_moves(bo, &mi);
  return mi_includes(&mi, *te);
}


/**
 * 先手後手を入れ換える。
 * @param bo 対象のBOARD
 */
void boToggleNext(BOARD* bo)
{
#ifdef DEBUG
  assert( bo != NULL );
  assert( bo->next == SENTE || bo->next == GOTE );
#endif /* DEBUG */
  
  bo->next = (bo->next == 0);
  bo->key = ~ bo->key;
  bo->hash_all = ~ bo->hash_all;
}


/**
 * te の手が成りが可能か調べる。
 * @param bo 対象のBOARD
 * @param te 対象のTE
 * @return 成れないときは 0, どちらも可能 1, 成るしかない 2
 */
int can_move_promote(const BOARD* bo, const TE* te) 
{
  int p;
  int next;
  int fm ,to;

  assert(bo != NULL);
  assert(te != NULL);

  if ( te_is_put(te) )
    return 0;

  assert( te->fm >= 0x11 && te->fm <= 0x99 );
  assert( bo->board[te->fm] != 0 );

  p = boGetPiece(bo, te->fm);
  next = getTeban(p);

  p &= 0x0F;

  /* 王と金は成れない。戻る */
  if (p == OH || p == KIN)
    return 0;

  /* もう成っている駒はなれない。戻る */
  if ((p & 0x08) == 0x08)
    return 0;

  fm = (te->fm >> 4);
  to = (te->to >> 4);

  if ( next == SENTE ) {
    if ( (p == FU || p == KYO) && to <= 1 )
      return 2;
    else if ( p == KEI && to <= 2 )
      return 2;
    else if (fm <= 3 || to <= 3)
      return 1;
  }
  else {
    if ( (p == FU || p == KYO) && 9 <= to )
      return 2;
    else if ( p == KEI && 8 <= to )
      return 2;
    else if (7 <= fm || 7 <= to)
      return 1;
  }

  return 0;
}


/**
 * 千日手 (同一局面4回) になる手か？
 * @param bo 現在の局面
 * @param te これから指そうとする手
 * @param forbidden 連続王手の千日手のときtrueに出力する
 *
 * @return 千日手のときtrue
 */
bool is_move_sennichite(const BOARD* bo, const TE* te, bool* forbidden)
{
  BOARD* tmp_bo = boCopy(bo);
  boMove_mate(tmp_bo, *te);
  uint64_t hash = tmp_bo->hash_all;
  if ( count(tmp_bo->hash_history,
             tmp_bo->hash_history + tmp_bo->mi.count + 1, hash) >= 4 ) {
    // 連続王手の千日手かどうか

    // http://www.shogi.or.jp/faq/
    // 全部の手が王手のとき以外は、通常の千日手扱い
    
    return true;
  }

  return false;
}


/**
 * 現在の時刻を記録する。
 * (思考時間の測定用)
 * @param bo 対象のBOARD
 */
void boSetNowTime(BOARD *bo) {
  bo->before_think_time = time(NULL);
}


/**
 * 消費した時間を記録する。
 * @param bo 対象のBOARD
 */ 
void boSetUsedTime(BOARD *bo) {
  time_t used_time;
  
  used_time = time(NULL) - bo->before_think_time;
#ifdef DEBUG
  assert( 0 <= used_time );
#endif /* DEBUG */
  if ( used_time <= 0 ) {
    used_time = 1;
  }
  
  bo->total_time[bo->next] += used_time;
  
  bo->used_time[bo->mi.count] = used_time;
}


static int bo_is_correct_mate(const BOARD* bo, Xy* oh_xy)
{
  // const KikiInfo* mi;
  // int oh_xy[2];
  int piece[9]; // 駒種ごとの駒の数
  static const int piecemax[] = {
    0, 18, 4, 4, 4, 4, 2, 2, 2,
  };
  Xy xy;
  int p, i;
  int x, y, flg;

  assert(bo != NULL);

  oh_xy[0] = 0;
  oh_xy[1] = 0;
  for (i = 0; i < 9; i++) {
    piece[i] = 0;
  }

  for (y = 1; y <= 9; y++) {
    for (x = 1; x <= 9; x++) {
      xy = (y << 4) + x;

      p = boGetPiece(bo, xy);
      if (p == 0)
	continue;

      // 動かせない駒はないか
      if ( (p == FU && y <= 1) ||
	   (p == KYO && y <= 1) ||
	   (p == KEI && y <= 2) )
	return 0;
      if ( (p == FU + 0x10 && y >= 9) ||
	   (p == KYO + 0x10 && y >= 9) ||
	   (p == KEI + 0x10 && y >= 8) )
	return 0;

      // 玉は高々1枚ずつ
      if (p == OH) {
	if (oh_xy[0])
	  return 0;
	oh_xy[0] = xy;
	piece[8]++;
      }
      else if (p == OH + 0x10) {
	if (oh_xy[1])
	  return 0;
	oh_xy[1] = xy;
	piece[8]++;
      }
      else
	piece[p & 7]++;
    }
  }

  // 二歩
  for (x=1; x<=9; x++) {
    flg = 0;
    for (y=1; y<=9; y++) {
      p = boGetPiece(bo, (y << 4) + x);
      if (p == FU) {
	if (flg == 1)
	  return 0;
	flg = 1;
      }
    }
  }

  for (x=1; x<=9; x++) {
    flg = 0;
    for (y=1; y<=9; y++) {
      p = boGetPiece(bo, (y << 4) + x);
      if (p == FU + 0x10) {
	if (flg == 1)
	  return 0;
	flg = 1;
      }
    }
  }

  /* 持ち駒の数を調べる */
  for (i=1; i<=7; i++) {
    piece[i] += bo->inhand[0][i];
    piece[i] += bo->inhand[1][i];
  }

  /* 駒全部の数を数える */
  for (i = 1; i <= 8; i++) {
    if (piecemax[i] < piece[i])
      return 0;
  }

  // 受け玉
  if (bo->next == SENTE) {
    if (oh_xy[1] == 0)
      return 0;
  } 
  else {
    if (oh_xy[0] == 0)
      return 0;
  }
  
  if (bo->next == SENTE) {
    // 後手に王手が掛かっていてはいけない
    if ( bo_attack_count(bo, SENTE, oh_xy[1]) > 0 )
      return 0;
  }
  else {
    if ( bo_attack_count(bo, GOTE, oh_xy[0]) > 0 )
      return 0;
  }

  return 1;
}


/**
 * 対局用に正当な盤面か調べる。
 * @param bo 対象のBOARD
 * @return 1 ならば正当。0 ならば不正。
 */
int boCheckJustGame(const BOARD* bo)
{
  Xy oh_xy[2];
  if ( !bo_is_correct_mate(bo, oh_xy) )
    return 0;

  if (oh_xy[0] == 0)
    return 0;

  if (oh_xy[1] == 0)
    return 0;

  return 1;
}


/**
 * 詰将棋用に正当な盤面か調べる。
 * @param bo 対象のBOARD
 * @return 1 ならば正当。0 ならば不正。
 */
int boCheckJustMate(const BOARD* bo)
{
  Xy oh_xy[2];
  return bo_is_correct_mate(bo, oh_xy);
}


extern const char* CSA_PIECE_NAME[];

/** 盤面をコンソールに出力 */
void printBOARD(const BOARD* brd)
{
    int x, y;

#ifdef DEBUG
  assert( brd != NULL );
#endif /* DEBUG */

  for (y = 1; y <= 9; y++) {
    printf("P%d", y);
    for (x = 9; x >= 1; x--) {
      PieceKind p = boGetPiece(brd, (y << 4) + x);
      if (!p)
        printf(" * ");
      else
        printf("%c%s", (getTeban(p) == 0 ? '+' : '-'), CSA_PIECE_NAME[p & 0xf]);
    }
    printf("\n");
  }

  // 持ち駒
  int i, side;
  for (side = 0; side < 2; side++) {
    if ( *max_element(brd->inhand[side] + 1, brd->inhand[side] + 8) > 0) {
      printf("P%c", side == 0 ? '+' : '-');
      for (x = 1; x <= 7; x++) {
        for (i = 0; i < brd->inhand[ side ][x]; i++)
          printf("00%s", CSA_PIECE_NAME[x]);
      }
      printf("\n");
    }
  }
  printf( "%c\n", brd->next == 0 ? '+' : '-' );
}

