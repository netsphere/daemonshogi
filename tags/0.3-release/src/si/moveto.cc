/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"


const int normal_to_count[] = {
  /* dummy */
  0,

  /* sente */
  1, 0, 2, 5, 6, 0, 0, 8,
  6, 6, 6, 6, 0, 4, 4, 0,

  /* gote */
  1, 0, 2, 5, 6, 0, 0, 8,
  6, 6, 6, 6, 0, 4, 4, 0,
};


/** 長い攻撃でない移動可能先 */
const XyDiff normal_to[][8] = {
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dummy */

  /* sente */
  { -16,   0,   0,   0,   0,   0,   0,   0, }, /* fu */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kyo */
  { -33, -31,   0,   0,   0,   0,   0,   0, }, /* kei */
  { -16, -17,  15,  17, -15,   0,   0,   0, }, /* gin */
  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* kin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kaku */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* hisya */
  { -16, -17,  -1,  15,  16,  17,   1, -15, }, /* oh */

  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* to */
  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* narikyo */
  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* narikei */
  { -16, -17,  -1,  16,   1, -15,   0,   0, }, /* narigin */
  {   0,   0,   0,   0,   0,   0,   0,   0, },
  { -16,  -1,  16,   1,   0,   0,   0,   0, }, /* uma */
  { -17,  15,  17, -15,   0,   0,   0,   0, }, /* ryu */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dummy */

  /* gote */
  {  16,   0,   0,   0,   0,   0,   0,   0, }, /* fu */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kyo */
  {  31,  33,   0,   0,   0,   0,   0,   0, }, /* kei */
  { -17,  15,  16,  17, -15,   0,   0,   0, }, /* gin */
  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* kin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kaku */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* hisya */
  { -16, -17,  -1,  15,  16,  17,   1, -15, }, /* oh */

  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* to */
  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* narikyo */
  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* narikei */
  { -16,  -1,  15,  16,  17,   1,   0,   0, }, /* narigin */
  {   0,   0,   0,   0,   0,   0,   0,   0, },
  { -16,  -1,  16,   1,   0,   0,   0,   0, }, /* uma */
  { -17,  15,  17, -15,   0,   0,   0,   0, }, /* ryu */
};


/**
 * 長い攻撃ではない指し手を生成する。飛・角などの短い攻撃も。
 * @param bo 対象のBOARD
 * @param xy 場所
 * @param piece 駒
 * @param mi 結果を格納するMOVEINFO
 */
static void append_short_onboard_moves(const BOARD* bo, Xy xy, PieceKind piece, 
                                       MOVEINFO* mi)
{
  int i, c;

  TE te;
  te.fm = xy;
  te.nari = 0;
  te.uti = 0;

  int next = (piece & 0x10) == 0x10;

  for (i = 0; i < normal_to_count[piece]; i++) {
    int vw;

    vw = xy + normal_to[piece][i];

#ifdef DEBUG
    assert( -1 <= (vw >> 4) && (vw >> 4) <= 11 );
    assert( 0 <= (vw & 0xF) && (vw & 0xF) <= 10 );
#endif /* DEBUG */

    c = boGetPiece(bo, vw);
    if (c == WALL)
      continue;

    if ( c ) { /* 移動先に駒がある場合 */
      if ( getTeban(c) == next )
	continue;
    }

    te.to = vw;
    miAddWithNari(bo, mi, &te, next);
  }
}


/**
 * 長い攻撃以外の利きを付ける.
 * @param bo 対象のBOARD
 * @param xy 場所
 * @param piece 駒種. 成り、プレイヤーを含む
 */
static void movetoNormal2(BOARD* bo, Xy xy, PieceKind piece)
{
  assert( bo->board[xy] );
  assert( bo->board[xy] == piece );

  int side = ((piece & 0x10) >> 4);
  int n = bo->noBoard[xy];
  int i;

  for (i = 0; i < normal_to_count[piece]; i++) {
    PieceKind c;
    Xy vw;

    vw = xy + normal_to[piece][i];
    c = bo->board[vw];
    if (c == WALL)
      continue;

    bo->piece_kiki_count[n]++; // 壁以外はすべて効かす
    bo->short_attack[side][vw].from[ bo->short_attack[side][vw].count++ ] = xy;
  }
}


/** 配列から一致するものを削除する */
static void erase(int16_t* begin, int16_t* end, int16_t value)
{
  int16_t* p;
  for (p = begin; p != end; p++) {
    if (*p == value) {
      *p = *(end - 1);
      *(end - 1) = 0;
      return;
    }
  }
}


/** 短い利きを取り除く*/
static void remove_short_attack( BOARD* bo, Xy xy )
{
  assert( bo->board[xy] );

  PieceKind piece = bo->board[xy];
  int n = bo->noBoard[xy];
  int side = getTeban(bo->board[xy]);
  int i;

  for (i = 0; i < normal_to_count[piece]; i++) {
    Xy vw;

    vw = xy + normal_to[piece][i];
    if ( bo->board[vw] == WALL )
      continue;

    erase( bo->short_attack[side][vw].from,
           bo->short_attack[side][vw].from + bo->short_attack[side][vw].count,
           xy );
    bo->short_attack[side][vw].count--;

    bo->piece_kiki_count[n]--;
  }
}


/** remoteOhte() でも利用する */
const int remote_to_count[] = {
  0,
  0, 1, 0, 0, 0, 4, 4, 0,
  0, 0, 0, 0, 0, 4, 4, 0,
  0, 1, 0, 0, 0, 4, 4, 0,
  0, 0, 0, 0, 0, 4, 4, 0,
};

const int remote_to[][8] = {
  { 0, 0, 0, 0, 0, 0, 0, 0, }, /* dummy */

  /* sente */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* fu */
  { -16,   0,   0,   0,   0,   0,   0,   0, }, /* kyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kei */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* gin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kin */
  {  17,  15, -15, -17,   0,   0,   0,   0, }, /* kaku */
  {   1,  -1, -16,  16,   0,   0,   0,   0, }, /* hisya */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* oh */

  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* to */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narikyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narikei */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narigin */
  {   0,   0,   0,   0,   0,   0,   0,   0, },
  {  17,  15, -15, -17,   0,   0,   0,   0, }, /* uma */
  {   1,  -1, -16,  16,   0,   0,   0,   0, }, /* ryu */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* dummy */

  /* gote */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* fu */
  {  16,   0,   0,   0,   0,   0,   0,   0, }, /* kyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kei */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* gin */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* kin */
  {  17,  15, -15, -17,   0,   0,   0,   0, }, /* kaku */
  {   1,  -1, -16,  16,   0,   0,   0,   0, }, /* hisya */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* oh */

  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* to */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narikyo */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narikei */
  {   0,   0,   0,   0,   0,   0,   0,   0, }, /* narigin */
  {   0,   0,   0,   0,   0,   0,   0,   0, },
  {  17,  15, -15, -17,   0,   0,   0,   0, }, /* uma */
  {   1,  -1, -16,  16,   0,   0,   0,   0, }, /* ryu */
};


/**
 * 長い攻撃の指し手を生成する
 * @param mi 結果を格納するMOVEINFO
 * @param bo 対象のBOARD
 * @param xy 駒の座標
 * @param next 駒の持ち主
 */
static void movetoLong(MOVEINFO* mi, const BOARD* bo, Xy xy, int next)
{
  int p, i;
  int c;
  int vw;
  PieceKind piece = boGetPiece(bo, xy);
  TE te;

  assert( bo != NULL );
  assert( 1 <= (xy & 0xf) && (xy & 0xf) <= 9 );
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( bo->board[xy] != 0 );
  assert( next == 0 || next == 1 );
  assert( mi != NULL );

  p = piece & 0x0F;

  te.fm = xy;
  te.nari = 0;
  te.uti = 0;
  for (i = 0; i < remote_to_count[piece]; i++) {
    // こちらでもpinされていないか確認しておく
#ifdef USE_PIN
    if (bo->pinned[xy]) {
      if (bo->pinned[xy] != remote_to[piece][i] &&
           bo->pinned[xy] != -remote_to[piece][i])
        continue;
    }
#endif

    bool flag = false;
    vw = xy;
    while (1) {
      vw += remote_to[piece][i];

      c = boGetPiece(bo, vw);
      if (c == WALL)
        break;

      if (c) {
        // 自分の駒があるとダメ
	if ( getTeban(c) == next )
	  break;
        flag = true;
      }

      te.to = vw;
      miAddWithNari(bo, mi, &te, next);

      if (flag)
        break;
    }
  }
}


/**
 * xy にある駒の動ける場所 (指し手) を探してmiに追加する。
 * @param bo 対象のBOARD
 * @param xy 駒の場所
 * @param mi 結果を格納するMOVEINFO
 */
void moveto(const BOARD* bo, int xy, MOVEINFO* mi)
{
  PieceKind piece;
  int p;

  assert( bo != NULL );
  assert( 1 <= (xy & 0x0f) && (xy & 0x0f) <= 9 );
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( bo->board[xy] != 0);
  assert(bo->next == 0 || bo->next == 1);
  assert( mi != NULL );

  // mi->count = 0;

  piece = bo->board[ xy ];
  p = piece & 0x0F;
  if ( p == OH )
    append_king_moves( bo, mi, xy, bo->next );
  else
    append_short_onboard_moves( bo, xy, piece, mi );

  if (p == KAKU || p == HISYA || p == UMA || p == RYU || p == KYO)
    movetoLong(mi, bo, xy, bo->next);
}


/**
 * 打ち歩詰めしていないか
 * @param bo 歩を打つ前の局面
 * @param te 歩を打つ手
 * @return 打ち歩詰めのときtrue
 */
static bool is_uchifuzume(const BOARD* bo, const TE& te)
{
  int defense;
  Xy defense_king;
  int i;

  if (bo->next) {
    // 後手が攻め
    defense = 0;
    defense_king = te.to + 16;
  }
  else {
    // 先手が攻め
    defense = 1;
    defense_king = te.to - 16;
  }

  // 歩に攻め方の利きが付いてなければ取れる
  if ( bo_attack_count(bo, 1 - defense, te.to) == 0 )
    return false;

  // 受け玉の周りに攻撃側の利きのないところがあればok
  for (i = 0; i < 8; i++) {
    Xy sq = defense_king + normal_to[ OH + (defense << 4) ][i];
    if (bo->board[sq] == WALL)
      continue;

    if ( (!bo->board[sq] || getTeban(bo->board[sq]) != defense) && 
         bo_attack_count(bo, 1 - defense, sq) == 0 )
      return false;
  }

  // 受け側の駒で取れるか (短いの)
  for (i = 0; i < bo->short_attack[defense][te.to].count; i++) {
    Xy defense_from = bo->short_attack[defense][te.to].from[i];
    if ( (bo->board[defense_from] & 0xf) == OH )
      continue;
#ifdef USE_PIN
    // これは厳密ではない。
    if (!bo->pinned[defense_from])
      return false;
#else
    if (bo->long_attack[1 - defense][defense_from].count == 0)
      return false;
#endif // USE_PIN
  }

  // 受け側の駒で取れるか (長いの)
  for (i = 0; i < bo->long_attack[defense][te.to].count; i++) {
    Xy defense_from = bo->long_attack[defense][te.to].from[i];

#ifdef USE_PIN
    // これは厳密ではない。
    if (!bo->pinned[defense_from])
      return false;
#else
    if (bo->long_attack[1 - defense][defense_from].count == 0)
      return false;
#endif // USE_PIN
  }
  
  // 実際に動かしてみる
  BOARD* tmp_bo = boCopy(bo);
  boMove_mate( tmp_bo, te );

  MOVEINFO tmp_mi;
  make_moves(tmp_bo, &tmp_mi);
  bool ret = tmp_mi.count == 0;  // 詰み

  freeBOARD(tmp_bo);

  return ret;
}


/**
 * ある駒を打つ手を生成.
 * utiOhte() から呼び出される. 打ち歩詰めになったりするときは追加しない。
 * @param bo 局面
 * @param mi 追加するMOVEINFO
 * @param pie 駒種
 * @param xy 座標. 0x11-0x99まで
 */
void append_a_put_move( const BOARD* bo, MOVEINFO* mi, PieceKind pie, Xy xy )
{
  TE te;

  te.fm = 0;
  te.to = xy;
  te.uti = pie;
  te.nari = false;
  te.hint = 0;

  if (bo->next == 0) {
    // 先手
    if (pie == FU) {
      if (checkFuSente(bo, xy & 0xf) || xy <= 0x19)
        return;

      // 打ち歩詰め？
      if (bo->board[xy - 16] == (OH + 0x10)) {
        if (is_uchifuzume(bo, te))
          return;
      }
    }
    else if (pie == KYO) {
      if (xy <= 0x19)
        return;
    }
    else if (pie == KEI) {
      if (xy <= 0x29)
        return;
    }
  }
  else {
    // 後手
    if (pie == FU) {
      if (checkFuGote(bo, xy & 0xf) || xy >= 0x91)
        return;

      if (bo->board[xy + 16] == OH) {
        if (is_uchifuzume(bo, te))
          return;
      }
    }
    else if (pie == KYO) {
      if (xy >= 0x91)
        return;
    }
    else if (pie == KEI) {
      if (xy >= 0x81)
        return;
    }
  }

  miAdd(mi, &te);
}


/**
 * ある場所に打つ手をすべて生成
 * search_068() と共通
 */
void append_put_moves(const BOARD* bo, Xy xy, MOVEINFO* mi)
{
  assert( xy >= 0x11 && xy <= 0x99 );
  assert( !bo->board[xy] );

  int i;
  for (i = 1; i <= 7; i++) {
    if ( !bo->inhand[bo->next][i] )
      continue;

    append_a_put_move(bo, mi, i, xy);
  }
}


/**
 * 局面で着手可能な指し手を生成する
 * @param bo 局面
 * @param mi 手一覧
 */
void make_moves(const BOARD* bo, MOVEINFO* mi)
{
  int x, y;

  // 王手を掛けられているか
  Xy king_xy = bo->code_xy[bo->next + 1][0];
  if ( king_xy && bo_attack_count(bo, 1 - bo->next, king_xy) ) {
    uke(bo, mi );
    return;
  }

  mi->count = 0;

  for (y = 1; y <= 9; y++) {
    for (x = 1; x <= 9; x++) {
      int xy = (y << 4) + x;

      if (bo->board[xy]) {
        // xyから移動する手
        if ( getTeban(bo->board[xy]) == bo->next ) 
          moveto(bo, xy, mi);
      }
      else {
        // 打つ手
        append_put_moves(bo, xy, mi);
      }
    }
  }
}


/**
 * 長い攻撃による利きを付ける.
 * @param bo 対象のBOARD
 * @param xy 駒の座標
 */
void bo_add_long_attack(BOARD* bo, Xy xy)
{
  assert( bo != NULL );
  assert( bo->board[xy] );

  int p, i;
  int side = getTeban(bo->board[xy]);
  int vw;
  PieceKind piece;
  
  int n = bo->noBoard[xy];
  piece = bo->board[ xy ];
  p = piece & 0x0F;

  for (i = 0; i < remote_to_count[piece]; i++) {
    vw = xy;
    while (1) {
      vw += remote_to[piece][i];

      if ( bo->board[ vw ] == WALL )
        break;

      // mi->to[mi->count++] = vw;
      bo->piece_kiki_count[n]++;
      bo->long_attack[side][vw].from[ bo->long_attack[side][vw].count++ ] = xy;

      if ( bo->board[ vw ] ) {
#ifdef USE_PIN
        if ((bo->board[vw] & 0x10) != (next << 4)) { 
          // 相手の駒 ... pin情報を更新
	  int vw2 = vw;
          do {
	    vw2 += remote_to[piece][i];
	  } while (!bo->board[vw2]);

	  if (bo->board[vw2] == OH + ((1 - next) << 4) ) 
	    bo->pinned[vw] = remote_to[piece][i];
	}
#endif
        break;
      }
    }
  }
}


/** 長い利きを取り除く */
void bo_remove_long_attack(BOARD* bo, Xy xy, PieceKind piece)
{
  assert( bo->board[xy] );

  int side = getTeban(bo->board[xy]);
  int i;
  int* pie_kiki_count = &bo->piece_kiki_count[ bo->noBoard[xy] ];

  for (i = 0; i < remote_to_count[piece]; i++) {
    Xy vw = xy;
    while (1) {
      vw += remote_to[piece][i];

      if ( bo->board[ vw ] == WALL )
        break;

      erase( bo->long_attack[side][vw].from,
             bo->long_attack[side][vw].from + bo->long_attack[side][vw].count,
             xy );
      bo->long_attack[side][vw].count--;

      // erase( pie_kiki->to, pie_kiki->to + pie_kiki->count, vw);
      (*pie_kiki_count)--;

      if (bo->board[vw]) {
#ifdef USE_PIN
        ...
#endif
        break;
      }
    }
  }
}


/**
 * xy にある駒の利き (短いのも長いのも) を取り除く。
 * @param bo BOARD
 * @param xy 座標
 */
void boPopToBoard(BOARD* bo, Xy xy) 
{
  // int fm, to;
  int n;
  int next;
  // KikiInfo* mi;
  
  assert( bo != NULL );
  assert( 1 <= (xy&0x0f) && (xy&0x0f) <= 9 );
  assert( 1 <= (xy>>4) && (xy>>4) <= 9 );
  assert( bo->board[ xy ] != 0 );
  
  n = bo->noBoard[ xy ];
  // mi = boGetCodeTo(bo, n);
  next = getTeban(bo->board[ xy ]);

  remove_short_attack( bo, xy );

  int p = bo->board[xy] & 0xf;
  if (p == KAKU || p == HISYA || p == UMA || p == RYU || p == KYO)
    bo_remove_long_attack( bo, xy, bo->board[xy] );

  assert( bo->piece_kiki_count[n] == 0 );
}


/**
 * xyにある駒の利き情報 kiki, toBoard3 を更新する.
 * @param bo 対象のBOARD
 * @param xy 駒の場所
 */
void bo_movetoKiki(BOARD* bo, Xy xy)
{
  PieceKind piece;
  int next, p;
  int nn;
  // KikiInfo* mi;

  // assert( bo != NULL );
  assert( 1 <= (xy & 0x0f) && (xy & 0x0f) <= 9);
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert(bo->next == 0 || bo->next == 1);
  assert(bo->board[xy] != 0);

  nn = bo->noBoard[xy];
  // mi = &bo->kiki[nn];
  bo->piece_kiki_count[nn] = 0;
  
  piece = bo->board[ xy ];
  next = (0x10 & piece) ? GOTE : SENTE;

  movetoNormal2(bo, xy, piece);

  p = piece & 0x0F;
  if (p == KAKU || p == HISYA || p == UMA || p == RYU || p == KYO)
    bo_add_long_attack(bo, xy);
}


/**
 * xy に動ける next 手番の駒を探して MOVEINFO に te を追加する。
 * ただし王は除く。受け/王手生成用。
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 * @param xy 場所
 * @param next 次の手番
 */
void moveto2(const BOARD* bo, MOVEINFO* mi, Xy xy, int next)
{
  assert( !bo->board[xy] || getTeban(bo->board[xy]) != next );

  TE te;
  int i, p;

  // mi->count = 0;

  te.nari = 0;
  te.uti = 0;
  te.to = xy;

  // 短いの
  for (i = 0; i < bo->short_attack[ next ][ xy ].count; i++) {
    te.fm = bo->short_attack[ next ][ xy ].from[i];

    p = bo->board[ te.fm ];
    if ( p == OH || p == (OH + 0x10) )
      continue;

    miAddWithNari(bo, mi, &te, next);
  }

  // 長いの
  for (i = 0; i < bo->long_attack[ next ][ xy ].count; i++) {
    te.fm = bo->long_attack[ next ][ xy ].from[i];

    miAddWithNari(bo, mi, &te, next);
  }
}


/**
 * xy に動ける next 手番の駒を探して MOVEINFO に te を記録する。
 * mi をクリアしないで手を追加する。自分の駒がある場合は mi に記録しない。
 * 取る手の生成用
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 * @param xy 場所
 * @param next 次の手番
 */
void moveto2_add(const BOARD* bo, MOVEINFO* mi, Xy xy, int next)
{
  TE te;
  int i, p2;
  
  p2 = boGetPiece(bo, xy);
  if ( p2 != 0 && getTeban(p2) == next ) 
    return;

  te.nari = 0;
  te.uti = 0;
  te.to = xy;

  // 短いの
  for (i = 0; i < bo->short_attack[ next ][ xy ].count; i++) {
    te.fm = bo->short_attack[ next ][ xy ].from[i];
    
    miAddWithNari(bo, mi, &te, next);
  }

  for (i = 0; i < bo->long_attack[ next ][ xy ].count; i++) {
    te.fm = bo->long_attack[ next ][ xy ].from[i];
 
    miAddWithNari(bo, mi, &te, next);
  }
}
