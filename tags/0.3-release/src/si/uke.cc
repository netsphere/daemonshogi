/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdlib.h>
#include <assert.h>
#include "si.h"
#include "ui.h"


static void ukeAigoma(const BOARD* bo, MOVEINFO* mi, Xy king_xy, Xy rxy, 
		      int next);


/**
 * 王手された状態で受けを探す。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 */
void uke(const BOARD* bo, MOVEINFO* mi )
{
  Xy king_xy;
  int num_of_ohte;

#ifndef NDEBUG
  assert( bo != NULL );
  assert( mi != NULL );
  // assert( next == 0 || next == 1 );
  // assert( bo->next == next );
  // assert( bo->board[ 0 ] == WALL );
  // assert( bo->board[ 0x9A ] == WALL );
#endif /* DEBUG */

  mi->count = 0;
  int next = bo->next;

  // 自玉
  king_xy = bo->code_xy[1 + bo->next][0];
  
  /*
   * 王手している駒の数を数える。
   * ２つならば王が動くしかない。
   * １つならば、
   * ・王が動く
   * ・王手している駒を取る
   * ・合い駒する
   * の３種類の受けがある。
   */ 
  num_of_ohte = bo_attack_count(bo, next == 0, king_xy);
  
#ifdef DEBUG
  assert( num_of_ohte == 1 || num_of_ohte == 2 );
#endif /* DEBUG */

  if ( num_of_ohte == 1 ) {
    int xy;
    if (bo->short_attack[ next == 0 ][ king_xy ].count)
      xy = bo->short_attack[ next == 0 ][ king_xy ].from[0];
    else
      xy = bo->long_attack[ next == 0 ][ king_xy ].from[0];

    /* 王手している駒を取って受ける */
    moveto2(bo, mi, xy, next);

    /* 合い駒して受ける */
    ukeAigoma(bo, mi, king_xy, xy, next);
     
    /* 王が動いて受ける */
    append_king_moves(bo, mi, king_xy, next);
  }
  else {
    /* 両王手の場合 */
    /* 王が動いて受ける */
    // n = bo->toBoard3[ (next == 0) ][ king_xy ][ 0 ];
    // rxy[ 0 ] = bo->code_xy[ n ][ 0 ];
    // n = bo->toBoard3[ (next == 0) ][ king_xy ][ 1 ];
    // rxy[ 1 ] = bo->code_xy[ n ][ 0 ];
    
    append_king_moves(bo, mi, king_xy, next);
  }
}


/**
 * 合い駒して王手を受ける。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param king_xy 王の場所
   @param rxy 王手している駒の場所
 * @param next 受け側の手番
 */
static void ukeAigoma(const BOARD* bo, MOVEINFO* mi, Xy king_xy, Xy rxy, 
                      int next) 
{
  int xy;
  int v, w;
  int i, p;

  p = bo->board[ rxy ] & 0xF;
  if (bo->long_attack[1 - next][king_xy].count == 0)
    return;
  // if ( p != KYO && p != KAKU && p != HISYA && p != UMA && p != RYU )
  //   return;
  
  v = (rxy & 0x0F) - (king_xy & 0x0F);
  w = (rxy >> 4) - (king_xy >> 4);

  if ( abs(v) <= 1 && abs(w) <= 1) {
    /* 合い駒できないので戻る */
    return;
  }
  
  if ( 0 < v )
    v = 1;
  else if ( v < 0 )
    v = -1;
  
  if ( 0 < w )
    w = 1;
  else if ( w < 0 )
    w = -1;

  // 空白のところに駒を置いていく
  for (i = 1; 1; i++) {
    xy = king_xy + ((w * i) << 4) + (v * i);
    p = bo->board[ xy ];

#ifdef DEBUG
    assert( p != WALL );
#endif /* DEBUG */

    if ( p )
      break;

    // 移動して合い駒. 当然、玉を動かす手は除く。
    moveto2(bo, mi, xy, next);

    // 打つ
    append_put_moves( bo, xy, mi );
  }
}

