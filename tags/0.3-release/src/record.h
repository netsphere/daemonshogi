/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Record class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/record.h,v $
 * $Id: record.h,v 1.1.1.1 2005/12/09 09:03:07 tokita Exp $
 */

#ifndef _RECORD_H_
#define _RECORD_H_

#include "dte.h"
#include "dmoveinfo.h"
#include "dboard.h"
#include <string>
// using namespace std;


/** 対局者の名前の最大文字数 */
#define MAXPLAYERNAME 256

/** 手合いを表す定数 */
typedef enum {
  /** 平手 */
  D_HIRATE = 0,
  /** 香落ち */
  D_KYOOCHI = 1,
  /** 角落ち */
  D_KAKUOCHI = 2,
  /** 飛車落ち */
  D_HISYAOCHI = 3,
  /** 角香落ち */
  D_KAKUKYOOCHI = 4,
  /** 飛車香落ち */
  D_HISYAKYOOCHI = 5,
  /** 二枚落ち */
  D_NIMAIOCHI = 6,
  /** 四枚落ち */
  D_YONMAIOCHI = 7,
  /** 六枚落ち */
  D_ROKUMAIOCHI = 8,
  /** 八枚落ち */
  D_HACHIMAIOCHI = 9,
} D_TEAI;


/** 棋譜ファイルフォーマット.
    daemon_record_research_format() が返す */
typedef enum {
  D_FORMAT_ERROR = 0,
  D_FORMAT_UNKNOWN = 1,
  D_FORMAT_CSA = 2,
  D_FORMAT_KIF = 3,
} D_RECFORMAT;

enum D_LOADSTAT {
  /** 読み込み成功 */
  D_LOAD_SUCCESS = 0,
  /** 読み込み失敗 */
  D_LOAD_ERROR = 1,
};


#if 0
/** 対局結果の値 */
enum D_MATCHSTAT {
  /** 不明 */
  D_UNKNOWN = 0,
  /** 投了 */
  D_TORYO = 1,
  /** 待った */
  D_MATTA = 2,
  /** 千日手引き分け */
  D_SENNICHITE = 3,
  /** 持将棋 */
  D_JISHOGI = 4,
  /** 詰み */
  D_TSUMI = 5,
  /** 詰み */
  D_FUZUMI = 6,
  /** 指し手の文字列が正しくない。あるいはルール上指せない等 */
  D_ERROR = 7,
};
#endif


/** ゲーム情報を表す.
    変化を含むので、勝敗は一意に決まらない */
struct Record 
{
  /** 対局者の名前 */
  char player[2][MAXPLAYERNAME];

  /** 手合い */
  D_TEAI teai;

  /** 初期配置図 */
  DBoard first_board;

  /** 開始日時 (00:00:00 UTC, January 1, 1970 からの秒数)*/
  time_t start_time;

  /** 手の内容 (変化を含む). */
  KyokumenNode* mi;

  /** 読み込み状況 */
  D_LOADSTAT loadstat;

  /** ファイル読み込み中に発生したエラー */
  std::string strerror;

  /** 勝敗状況 */
  // D_MATCHSTAT matchstat;

  /** コンストラクタ */
  Record();
  ~Record() { delete mi; }

  /** 状態をエラー状態にする。 */
  void set_error(const char* message) {
    loadstat = D_LOAD_ERROR;
    strerror = message;
  }
};


/*********************************************************/
/* function prototypes */
/*********************************************************/

Record*     daemon_record_new            (void);
void        daemon_record_free           (Record *record);

// void daemon_record_add_dte(Record* record, const DTe* te, int t);

int daemon_record_output_csa     (Record* record,
				  const char* filename);

int daemon_record_output_csa_code(Record* record,
				  const char* filename,
				  const char* code);

int daemon_record_output_kif     (Record* record,
				  const char* filename);

int daemon_record_output_kif_code(Record* record,
				  const char* filename,
				  const char* code);

void daemon_record_load           (Record* record, const char* filename);

/** 文字コード指定するバージョン */
void daemon_record_load_code(Record* record, 
			     const char* filename, 
			     const char* encoding);

void daemon_record_load_csa       (Record* record, const char* filename);

/** CSA形式. 文字コードを指定 */
void daemon_record_load_csa_code  (Record* record, 
				   const char* filename, 
				   const char* encoding);

void daemon_record_load_kif       (Record* record, const char* filename);

/** KIF形式. 文字コードを指定 */
void daemon_record_load_kif_code  (Record* record, 
				   const char* filename, 
				   const char* encoding);

// int         daemon_record_read_lines     (char *filename,
// 					  char *outbuf[],
// 					  int maxline);

// D_RECFORMAT daemon_record_research_format(const char* filename);

D_LOADSTAT  daemon_record_get_loadstat   (const Record* record);

// D_MATCHSTAT daemon_record_get_matchstat  (const Record* record);

// char*       daemon_record_get_player     (Record *record,
// 					  D_TEBAN next);

void        daemon_record_set_player     (Record *record,
					  D_TEBAN next,
					  const char* s);


#endif /* _RECORD_H_ */
