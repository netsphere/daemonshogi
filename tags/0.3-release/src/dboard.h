/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _DBOARD_H_
#define _DBOARD_H_

#include <stdio.h>
#include "dte.h"
#include "si/si.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/** 盤の外の値 */
#define DWALL (0x20)

/** 手番の値 */
typedef enum {
  /** 先手の値 */
  D_SENTE = 0,
  /** 後手の値 */
  D_GOTE = 1,
} D_TEBAN;


/**
 * 局面情報クラス.
 * \todo BOARD と統合
 */
struct DBoard
{
  /** 余白 */
  PieceKind ban_padding__[16];

  /** 盤面を記録する配列 */
  PieceKind board[16 * 12];

  /** 持ち駒を記録する配列. 1行128ビット */
  int16_t inhand[2][8];

  /** 手番 */
  D_TEBAN next;

  /** 駒箱. 詰将棋のために玉を格納できるようにする */
  int16_t pbox[9];
};


/*********************************************************/
/* function prototypes */
/*********************************************************/

DBoard* daemon_dboard_new       (void);
void    daemon_dboard_init      (DBoard *bo);
void    daemon_dboard_free      (DBoard *bo);
void    daemon_dboard_set_board (DBoard *bo,
				 int x,
				 int y,
				 int p);
int     daemon_dboard_get_board (const DBoard* bo,
				 int x, int y);
void    daemon_dboard_set_piece (DBoard *bo,
				 int next,
				 int p,
				 int n);
void    daemon_dboard_add_piece (DBoard *bo,
				 int next,
				 int p);
void    daemon_dboard_dec_piece (DBoard *bo,
				 int next,
				 int p);
int     daemon_dboard_get_piece (const DBoard* bo,
				 int next,
				 int p);
void    daemon_dboard_set_hirate(DBoard *bo);
void    daemon_dboard_set_mate  (DBoard *bo);
void    daemon_dboard_set_rl_reversal(DBoard *bo);
void    daemon_dboard_set_order_reversal(DBoard *bo);
void    daemon_dboard_flip      (DBoard *bo);
void    daemon_dboard_set_all_to_pbox(DBoard *bo);
void    daemon_dboard_set_for_edit(DBoard *bo);
void    daemon_dboard_output    (const DBoard* bo, FILE *out); 
void    daemon_dboard_move      (DBoard *bo, const DTe* te);
void    daemon_dboard_back      (DBoard *bo, const DTe* te);
// int     daemon_dboard_can_back  (DBoard *bo);
void    daemon_dboard_copy      (const DBoard* src, DBoard *dest);
void    daemon_dboard_toggle_next(DBoard *bo);
void    daemon_dboard_set_pbox  (DBoard *bo, int p, int n);
int     daemon_dboard_get_pbox  (DBoard *bo, int p);
void    daemon_dboard_add_pbox  (DBoard *bo, int p);
void    daemon_dboard_dec_pbox  (DBoard *bo, int p);
void    daemon_dboard_copy_board(const DBoard* src, BOARD* dest);
void    daemon_dboard_copy_from_board(const BOARD* src, DBoard* dest);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _DBOARD_H_ */
