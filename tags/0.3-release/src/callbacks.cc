/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#define GTK_DISABLE_DEPRECATED 1
#define GDK_DISABLE_DEPRECATED 1
#define G_DISABLE_DEPRECATED 1
#define _XOPEN_SOURCE 700 // SUSv4

#include <config.h>

#include <pthread.h>
#include <gtk/gtk.h>
#include <assert.h>
#include <stdlib.h>

G_BEGIN_DECLS
#include "callbacks.h"
#include "interface.h"
#include "support.h"
G_END_DECLS
#include "canvas.h"
#include "conn.h"
#include "history_window.h"
using namespace std;


/** メニュー -> ファイル -> 棋譜ファイルを開く */
void
on_file_open_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  GtkWidget* dialog;
  GtkFileFilter* filter;

  dialog = gtk_file_chooser_dialog_new(_("fileselection"),
                                       GTK_WINDOW(g_canvas->window),
                                       GTK_FILE_CHOOSER_ACTION_OPEN,
                                       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                       GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                                       NULL);
  filter = gtk_file_filter_new();
  gtk_file_filter_set_name(filter, _("CSA format, KIF format"));
  gtk_file_filter_add_pattern(filter, "*.csa");
  gtk_file_filter_add_pattern(filter, "*.kif");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);

  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
    char* filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    daemon_kiffile_load_ok_button_pressed(filename);
    g_free(filename);
  }
  gtk_widget_destroy(dialog);
}


void
on_file_exit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  exit(0);
}


/** daemonshogi について */
void
on_help_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  GtkWidget* about = create_about_dialog();
  gtk_dialog_run(GTK_DIALOG(about));
  gtk_widget_destroy(about);
}


gboolean
on_window_delete_event                 (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  return FALSE;
}


void
on_window_destroy                      (GtkObject       *object,
                                        gpointer         user_data)
{
  exit(0);
}


gboolean
on_drawingarea_motion_notify_event     (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data)
{
  daemon_canvas_motion_notify(g_canvas, event);

  return FALSE;
}


gboolean
on_drawingarea_button_press_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  daemon_canvas_button_press(g_canvas, event);

  return FALSE;
}


gboolean
on_drawingarea_button_release_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  daemon_canvas_button_release(g_canvas, event);

  return FALSE;
}


void
on_file_saveas_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  GtkWidget* dialog;
  GtkFileFilter *csa_filter, *kif_filter;

  dialog = gtk_file_chooser_dialog_new(_("save book file"),
                                       GTK_WINDOW(g_canvas->window),
                                       GTK_FILE_CHOOSER_ACTION_SAVE,
                                       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                       GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                       NULL);

  csa_filter = gtk_file_filter_new();
  gtk_file_filter_set_name(csa_filter, "CSA format");
  gtk_file_filter_add_pattern(csa_filter, "*.csa");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), csa_filter);

  kif_filter = gtk_file_filter_new();
  gtk_file_filter_set_name(kif_filter, "KIF format");
  gtk_file_filter_add_pattern(kif_filter, "*.kif");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), kif_filter);

  gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);

  if (gtk_dialog_run(GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
    char* filename;
    GtkFileFilter* filter;
    enum KifuFileType ft;

    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    filter = gtk_file_chooser_get_filter(GTK_FILE_CHOOSER(dialog));
    if (filter == csa_filter)
      ft = CSA_FORMAT;
    else if (filter == kif_filter)
      ft = KIF_FORMAT;
    else
      assert(0);
    
    daemon_kiffile_save_ok_button_pressed(filename, ft);
    g_free(filename);
  }

  gtk_widget_destroy(dialog);
}


void
on_edit_startedit_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  D_CANVAS_MODE mode;

  mode = daemon_canvas_get_mode(g_canvas);

  if (mode != D_CANVAS_MODE_EDIT) {
    int ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("Start edit mode?"));
    if (ret  == GTK_RESPONSE_OK) {
      daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
    }
  }
}


/** 表示 -> 小さい */
void on_view_small_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (g_canvas)
    daemon_canvas_change_size(g_canvas, D_CANVAS_SIZE_SMALL);
}


void
on_view_middle_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (g_canvas)
    daemon_canvas_change_size(g_canvas, D_CANVAS_SIZE_MIDDLE);
}


void
on_view_big_activate                        (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (g_canvas)
    daemon_canvas_change_size(g_canvas, D_CANVAS_SIZE_BIG);
}


/** ウィンドウの露出。転送するだけ。 */
gboolean
on_drawingarea_expose_event( GtkWidget* widget,
                             GdkEventExpose* event,
                             gpointer user_data )
{
  gdk_draw_drawable( widget->window, 
                     widget->style->fg_gc[ GTK_WIDGET_STATE(widget) ],
                     daemon_canvas_get_pixmap(g_canvas), // src
                     event->area.x,
                     event->area.y,
                     event->area.x,
                     event->area.y,
                     event->area.width,
                     event->area.height );

  return FALSE;
}


gboolean
on_drawingarea_leave_notify_event      (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data)
{
  daemon_canvas_leave_notify(g_canvas, widget, event, user_data);

  return FALSE;
}


// int network_game_start = 0;
// char net_my_turn = '\0';
// char net_playername[2][100];
extern NetGameInfo game_info;


/** 
 * ゲーム開始ダイアログ.
 * 各オプションメニューの初期値を設定する
 */
void on_game_optionmenu_realize             (GtkWidget       *widget,
                                        gpointer         user_data)
{
  gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 0);
}


static ConnectThreadArg worker;
static guint idle_id = 0;
GtkWidget* connect_status_dialog = NULL;
static pthread_t thread_id;

static void connect_status_set_text(const char* txt)
{
  if (!connect_status_dialog)
    return;

  gtk_label_set_text(
         GTK_LABEL(lookup_widget(connect_status_dialog, "message")), txt);
}


void daemon_start_game_via_network();

/** 接続できたかどうか */
static gboolean on_connecting_idle(gpointer data)
{
  if (worker.status_queue.size() > 0) {
    string status;
    while (worker.status_queue.size() > 0) {
      status = worker.status_queue.front(); // back() して clear() だと取りこぼす
      worker.status_queue.pop_front();
    }
    connect_status_set_text(status.c_str());
  }

  if (!worker.result)
    return TRUE; // 引き続き呼び出してもらう.

  if (worker.result < 0) {
    // 失敗
    gtk_widget_destroy(connect_status_dialog);
    connect_status_dialog = NULL;

    disconnect_server();
    daemon_messagebox(g_canvas, _("connection failed"), GTK_MESSAGE_ERROR);
    g_signal_emit_by_name(
		  G_OBJECT(lookup_widget(g_canvas->window, "connect_server")),
		  "activate", NULL);
    return FALSE;
  }

  // 接続に成功
  connect_status_set_text(_("connected."));

  g_signal_connect_after(G_OBJECT(g_canvas->window), "notify::startgame",
			 daemon_start_game_via_network, NULL);
  // ログインする
  csa_login_and_wait_socket(worker.username.c_str(), worker.password.c_str());

  return FALSE; // アイドル待ちは終了
}


extern void add_watch_socket();

/**
 * ネットワーク経由でのゲーム開始.
 * on_socket_read() から呼び出される
 */
void daemon_start_game_via_network()
{
  PLAYERTYPE playertype[2];

  add_watch_socket();

  daemon_dboard_set_hirate(&g_canvas->board);

  if (game_info.my_turn == '+') {
    playertype[0] = worker.player_type;
    playertype[1] = SI_NETWORK_1;
  }
  else {
    playertype[0] = SI_NETWORK_1;
    playertype[1] = worker.player_type;

    on_view_flip_activate(NULL, NULL);
  }

  daemon_game_setup(playertype[0], game_info.player_name[0].c_str(),
                    playertype[1], game_info.player_name[1].c_str());

  // main loop
  daemon_canvas_game(g_canvas);
}


extern PLAYERTYPE get_player_type(int combo_index);
// int network_game_start = 0;

/** ゲーム -> CSAサーバに接続 */
void on_game_remote_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (worker.hostname == "") {
    worker.hostname = "localhost";
    worker.port = "4081";
  }

  GtkWidget* dialog;

  dialog = create_remote_dialog();
  GtkEntry* server_host_entry = GTK_ENTRY(lookup_widget(dialog, "server_host"));
  GtkEntry* server_port_entry = GTK_ENTRY(lookup_widget(dialog, "server_port"));
  GtkEntry* username_entry = GTK_ENTRY(lookup_widget(dialog, "username"));
  GtkEntry* password_entry = GTK_ENTRY(lookup_widget(dialog, "password"));

  gtk_entry_set_text(server_host_entry, worker.hostname.c_str());
  gtk_entry_set_text(server_port_entry, worker.port.c_str());
  gtk_entry_set_text(username_entry, worker.username.c_str());
  gtk_entry_set_text(password_entry, worker.password.c_str());

  while (1) {
    if (gtk_dialog_run(GTK_DIALOG(dialog)) != GTK_RESPONSE_OK) {
      gtk_widget_destroy(dialog);
      return;
    }

    worker.hostname = gtk_entry_get_text(server_host_entry);
    worker.port = gtk_entry_get_text(server_port_entry);
    worker.username = gtk_entry_get_text(username_entry);
    worker.password = gtk_entry_get_text(password_entry);
    GtkWidget* w = lookup_widget(dialog, "playertype_combobox");
    worker.player_type = get_player_type(
			     gtk_combo_box_get_active(GTK_COMBO_BOX(w)));

    // TODO: 設定ファイルへの保存

    // TODO: エラーチェックをしっかりと
    string err;
    if (worker.hostname.size() == 0)
      err += "ホスト名を入力してください。\n";
    if ( worker.port.size() == 0 || atoi(worker.port.c_str()) == 0 )
      err += "ポート番号を入力してください。\n";
    if (worker.username.size() == 0)
      err += "ユーザ名を入力してください。\n";
    if (worker.password.size() == 0)
      err += "パスワードを入力してください。\n";

    if (err != "") {
      gtk_label_set_text(GTK_LABEL(lookup_widget(dialog, "error_message")), 
			 err.c_str());
    }
    else
      break;
  }

  gtk_widget_destroy(dialog);

#if 0
  if (!g_canvas->network_log_window)
    g_canvas->network_log_window = create_network_log();
  gtk_widget_show(g_canvas->network_log_window);
#endif // 0

  connect_status_dialog = create_connecting_dialog();
  gtk_window_set_modal(GTK_WINDOW(connect_status_dialog), TRUE);
  gtk_widget_show(connect_status_dialog);

  // 接続は子スレッドでおこなう
  worker.result = 0;
  // worker.to_stop = 0;
  if (pthread_create(&thread_id, NULL, csa_connect_thread_main, &worker)) {
    daemon_messagebox(g_canvas, "internal error: thread create failed.", 
		      GTK_MESSAGE_ERROR);
    return;
  }
  pthread_detach(thread_id);

  // TODO: こんな感じでパイプを通したほうがいいのかもしらんが
  // http://d.hatena.ne.jp/viver/20081015/p1
  idle_id = g_idle_add(on_connecting_idle, NULL);
}


gboolean
on_bookwindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  assert(g_canvas);

  history_window_menuitem_set_active(g_canvas, false);
  gtk_widget_hide(widget);

  return TRUE; // stop other handlers
}


void
on_booklist_cursor_changed             (GtkTreeView     *treeview,
                                        gpointer         user_data)
{
  GtkTreePath* path = NULL;
  const gint* idx;

  gtk_tree_view_get_cursor(treeview, &path, NULL);
  assert(path);
  idx = gtk_tree_path_get_indices(path);
  assert(idx);

  daemon_canvas_bookwindow_click(treeview, *idx);

  gtk_tree_path_free(path);
}


void
on_view_network_log_activate(GtkMenuItem* menuitem,
			     gpointer         user_data)
{
#if 0
  if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menuitem))) {
    if (!g_canvas->network_log_window)
      g_canvas->network_log_window = create_network_log();
    gtk_widget_show(g_canvas->network_log_window);
  }
  else
    gtk_widget_hide(g_canvas->network_log_window);
#endif // 0
}


void
on_candidate_treeview_cursor_changed   (GtkTreeView     *treeview,
                                        gpointer         user_data)
{

}

// メニュー -> 表示 -> 棋譜ウィンドウ
void on_view_bookwindow_activate(GtkMenuItem* menu_item,
				 gpointer user_data) 
{
  if (!bookwindow_item_is_actived(g_canvas)) {
    // 表示済
    printf("%s: when active.\n", __func__); // DEBUG
    g_canvas->history->hide_window();
  }
  else
    g_canvas->history->show_window();
}


/** 初期局面まで戻す */
void on_go_first_activate(GtkBin* menuitem, gpointer user_data)
{
  printf("%s: cur_pos = %d\n", __func__, g_canvas->history->cur_pos); // DEBUG
  
  Canvas* canvas = g_canvas;

  if (canvas->mode != D_CANVAS_MODE_BOOK) 
    return;

  while (canvas->history->cur_pos > 0) {
    DTe te = canvas->history->focus_line[--canvas->history->cur_pos].first;
    daemon_dboard_back(&canvas->board, &te);
  }

  canvas->history->select_nth(canvas->history->cur_pos);

  // 再描画
  canvas_redraw(canvas);
  
  daemon_canvas_set_kif_sensitive(canvas);
}


/** 1手戻す */
void on_go_back_activate(GtkBin* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;

  if (canvas->mode != D_CANVAS_MODE_BOOK)
    return;

  if (canvas->history->cur_pos == 0)
    return;

  DTe te = canvas->history->focus_line[--canvas->history->cur_pos].first;
  daemon_dboard_back(&canvas->board, &te);

  // 手を選択
  canvas->history->select_nth(canvas->history->cur_pos);

  // 再描画
  canvas_redraw(canvas);

  daemon_canvas_set_kif_sensitive(canvas);
}


/** 1手進める */
void on_go_next_activate(GtkBin* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;

  if (canvas->mode != D_CANVAS_MODE_BOOK)
    return;

  if (canvas->history->cur_pos >= canvas->history->focus_line.size())
    return;

  DTe te = canvas->history->focus_line[canvas->history->cur_pos].first;
  if (te.special != DTe::NORMAL_MOVE)
    return;

  daemon_dboard_move(&(canvas->board), &te);
  canvas->history->cur_pos++;

  // 棋譜ウィンドウ更新
  canvas->history->select_nth(canvas->history->cur_pos);

  // 再描画
  canvas_redraw(canvas);

  daemon_canvas_set_kif_sensitive(canvas);
}


void on_go_last_activate(GtkBin* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;

  if (canvas->mode != D_CANVAS_MODE_BOOK)
    return;

  while (canvas->history->cur_pos < canvas->history->focus_line.size()) {
    DTe te = canvas->history->focus_line[canvas->history->cur_pos].first;
    if (te.special != DTe::NORMAL_MOVE)
      break;
    daemon_dboard_move(&(canvas->board), &te);
    canvas->history->cur_pos++;
  }

  canvas->history->select_nth(canvas->history->cur_pos);

  // 再描画
  canvas_redraw(canvas);

  daemon_canvas_set_kif_sensitive(canvas);
}


/** 上下を反転 */
void on_view_flip_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  GtkWidget* flip_menu;
  Canvas* canvas = g_canvas;

  canvas->front = canvas->front == D_SENTE ? D_GOTE : D_SENTE;

  flip_menu = lookup_widget(canvas->window, "flip");
  assert(flip_menu);
  GTK_CHECK_MENU_ITEM(flip_menu)->active = canvas->front != D_SENTE;

  // 再描画
  canvas_redraw(canvas);
}


/** 詰め将棋 */
void on_game_checkmate_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  GtkWidget *dialog;
  int r = 0;

  dialog = create_dialog_mate();
  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK) {
    GtkWidget* sente;
    int is_sente;
    sente  = lookup_widget(dialog, "radiobutton_mate_sente");    
    assert(sente);
    is_sente = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sente));

    r = daemon_canvas_mate_start_pressed(is_sente);
  }
  gtk_widget_destroy(dialog);

  if (r)
    daemon_canvas_mate_loop();
}


void on_edit_set_hirate_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  gint ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("Set hirate?"));
  if (GTK_RESPONSE_OK != ret) 
    return;

  daemon_dboard_set_hirate(&g_canvas->board);
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
}


void on_edit_set_mate_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("Set for check mate?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_set_mate(&g_canvas->board);
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
}


void on_edit_rl_reversal_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("right/left reversal?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_set_rl_reversal(&g_canvas->board);
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
}


void on_edit_order_reversal_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("order reversal?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_set_order_reversal(&g_canvas->board);
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
}


void on_edit_flip_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("flip white/black board?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_flip(&g_canvas->board);
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
}


void on_edit_all_koma_to_pbox_activate(GtkMenuItem* menuitem, 
				       gpointer user_data)
{
  int ret;

  ret = daemon_canvas_ok_cancel_dialog(g_canvas, _("All piece to piecebox?"));
  if (GTK_RESPONSE_OK != ret)
    return;

  daemon_dboard_set_all_to_pbox(&g_canvas->board);
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
}


/** メニュー -> Game -> Play Game... */
void on_game_new_game_activate(GtkBin* menuitem, gpointer user_data)
{
  GtkWidget* dialog = create_dialog_game();
  int r = 0;

  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
    r = daemon_canvas_new_game_ok(dialog);

  gtk_widget_destroy(dialog);

  if (r)
    daemon_canvas_game(g_canvas); // main loop
}


/** メニュー -> 中断 */
void on_game_stop_activate(GtkBin* menuitem, gpointer user_data)
{
  if (daemon_canvas_get_mode(g_canvas) != D_CANVAS_MODE_GAME)
    return;

  gmSetGameStatus(&g_game, SI_CHUDAN);
}


/** 待った */
void on_game_matta_activate(GtkMenuItem* menuitem, gpointer user_data)
{
  Canvas* canvas = g_canvas;
  int i;

  if (daemon_canvas_get_mode(canvas) != D_CANVAS_MODE_GAME)
    return;

  if (g_board.mi.count < 1)
    return;

  // 棋譜を最大で2手削除する
  for (i = 0; i < 2 && g_board.mi.count > 0; i++) {
    // boToggleNext( &g_board );
    boBack( &g_board );
    DTe te = canvas->history->focus_line.back().first;
    daemon_dboard_back(&canvas->board, &te);

    daemon_dmoveinfo_pop(canvas->record->mi);
    canvas->history->remove_last();
  }

  // 再描画
  canvas_redraw(canvas);
}


/** 投了 */
void on_game_giveup_activate(GtkBin* menuitem, gpointer user_data)
{
  if (daemon_canvas_get_mode(g_canvas) != D_CANVAS_MODE_GAME)
    return;

  gmSetGameStatus(&g_game, SI_TORYO);
}


void
on_view_move_property_activate         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menuitem))) {
    if (!g_canvas->move_property_dialog)
      g_canvas->move_property_dialog = create_move_property_dialog();
    gtk_widget_show_all(g_canvas->move_property_dialog);
  }
  else 
    gtk_widget_hide(g_canvas->move_property_dialog);
}


void on_move_annot_changed ( GtkCellRendererCombo *combo,
                             gchar                *path_string,
                             GtkTreeIter          *new_iter,
                             gpointer              user_data)
{
}


void on_move_comment_edited ( GtkCellRendererText *renderer,
                              gchar               *path,
                              gchar               *new_text,
                              gpointer             user_data)
{
}


/** 変化選択ダイアログの構築 */
void on_move_property_dialog_realize( GtkWidget* widget, gpointer user_data )
{
  GtkCellRenderer* renderer;
  GtkTreeViewColumn* column;

  // printf("%s: called.\n", __func__); // DEBUG

  GtkWidget* next_moves = lookup_widget(widget, "next_moves");
  assert(next_moves);

  GtkListStore* model = gtk_list_store_new(3,
                               G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
  gtk_tree_view_set_model(GTK_TREE_VIEW(next_moves), GTK_TREE_MODEL(model));

  // 列1
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(
    _("Move"), // title
    renderer,  // cell
    "text", 0, // レンダラの"text"をカラム0に
    NULL);
  assert(column);
  gtk_tree_view_append_column( GTK_TREE_VIEW(next_moves), column );

  // 列2
  GtkListStore* annot_model = gtk_list_store_new(1, G_TYPE_STRING);
  static const char* annot_str[] = {
    _("poor"),         // "?"  疑問手 = 悪い手. 奥ゆかしい
    _("questionable"), // "?!" 疑わしい手
    NULL
  };
  for (int i = 0; annot_str[i]; i++) {
    GtkTreeIter iter;
    gtk_list_store_append(annot_model, &iter);
    gtk_list_store_set(annot_model, &iter, 0, annot_str[i], -1);
  }
  renderer = gtk_cell_renderer_combo_new();
  g_object_set(G_OBJECT(renderer), "model", annot_model, NULL);
  g_signal_connect(G_OBJECT(renderer), "changed", 
		   G_CALLBACK(on_move_annot_changed), NULL);
  column = gtk_tree_view_column_new_with_attributes( _("Annot"),
                                                     renderer,
                                                     "text", 1,
                                                     NULL );
  gtk_tree_view_append_column(GTK_TREE_VIEW(next_moves), column);

  // 列3
  renderer = gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(renderer), "editable", TRUE, NULL);
  g_signal_connect(G_OBJECT(renderer), "edited", 
		   G_CALLBACK(on_move_comment_edited), NULL);
  column = gtk_tree_view_column_new_with_attributes( _("Comment"),
                                                     renderer,
                                                     "text", 2,
                                                     NULL );
  gtk_tree_view_append_column(GTK_TREE_VIEW(next_moves), column);

  // 次の手を表示
  alt_moves_dialog_update();
}


/** 変化選択ダイアログで次の手を選択 */
void
on_next_moves_cursor_changed           (GtkTreeView     *treeview,
                                        gpointer         user_data)
{
  printf("%s: called.\n", __func__); // DEBUG

  // 何番目か
  GtkTreePath* path = NULL;
  gtk_tree_view_get_cursor(treeview, &path, NULL);
  assert(path);
  const gint* idx = gtk_tree_path_get_indices(path);
  assert(idx);

  // まず現在の局面までたどって
  KyokumenNode* n = g_canvas->record->mi;
  for (int i = 0; i < g_canvas->history->cur_pos; i++)
    n = n->lookup_child( g_canvas->history->focus_line[i].first );

  DTe selected_te = n->moves[*idx].te;
  
  // 次の一手を変更するか
  if (g_canvas->history->focus_line[g_canvas->history->cur_pos].first == 
      selected_te)
    return;

  printf("change next move.\n"); // DEBUG

  // 注目する手順を更新
  g_canvas->history->sync( *g_canvas->record, g_canvas->history->cur_pos, 
			   selected_te );
}


gboolean
on_network_log_delete_event            (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  GtkWidget* menuitem = lookup_widget(g_canvas->window, "network_log_menu");
  assert(menuitem);

  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(menuitem), FALSE);
  gtk_widget_hide(widget);

  return TRUE; // stop other handlers
}


gboolean
on_move_property_dialog_delete_event   (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  GtkWidget* menuitem = lookup_widget(g_canvas->window, "move_property");
  assert(menuitem);

  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(menuitem), FALSE);
  gtk_widget_hide(widget);

  return TRUE; // stop other handlers
}


void
on_bonanza_setting_button_activate     (GtkButton       *button,
                                        gpointer         user_data)
{

}


/** 接続中ダイアログ -> cancel button */
void
on_connecting_cancelbutton_activate    (GtkButton       *button,
                                        gpointer         user_data)
{
  // worker.to_stop = 1;

  gtk_widget_destroy( connect_status_dialog );
  connect_status_dialog = NULL;
}


/** 主ウィンドウ */
void
on_window_realize                      (GtkWidget       *widget,
                                        gpointer         user_data)
{
  canvas_build(g_canvas);
}

void
on_remote_dialog_realize               (GtkWidget       *widget,
                                        gpointer         user_data)
{
  GtkWidget* w = lookup_widget(widget, "playertype_combobox");
  assert(w);
  gtk_combo_box_set_active(GTK_COMBO_BOX(w), 0);
}


extern NetGameInfo game_info;

/** サーバからの対局提案ダイアログ */
void
on_network_game_dialog_realize         (GtkWidget       *widget,
                                        gpointer         user_data)
{
  int oppo = game_info.my_turn == '+' ? 1 : 0;
  gtk_label_set_text( GTK_LABEL(lookup_widget(widget, "opponent_name")),
                      game_info.player_name[oppo].c_str() );
  gtk_label_set_text( GTK_LABEL(lookup_widget(widget, "teban")),
                      game_info.my_turn == '+' ? _("SENTE (BLACK)") : _("GOTE (WHITE)") );
}

