/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "dboard.h"

/** DBoard を生成してそのポインタを返す。 */
DBoard* daemon_dboard_new(void) {
  DBoard* bo;
  
  bo = (DBoard *)malloc(sizeof(DBoard));
  if (bo == NULL) {
    printf("No enough memory in daemon_dboard_new().");
    abort();
  }
  
  daemon_dboard_init(bo);
  
  return bo;
}

/** 初期化 */
void daemon_dboard_init(DBoard* bo) 
{
  int i, x, y;
  
  for (i = -16; i < 16 * 12; i++)
    bo->board[i] = DWALL;

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      bo->board[(y << 4) + x] = 0;
    }
  }
  for (i=0; i<8; i++) {
    bo->inhand[D_SENTE][i] = 0;
    bo->inhand[D_GOTE][i] = 0;
  }
  bo->next = D_SENTE;

  for (i=0; i<9; i++) {
    bo->pbox[i] = 0;
  }
}


/** DBoard を解放する */
void daemon_dboard_free(DBoard* bo)
{
  assert(bo != NULL);
  
  free(bo);
}

/** (x, y)に駒 p をセットする。 */
void daemon_dboard_set_board(DBoard* bo, int x, int y, int p) {
  assert(bo != NULL);
  assert(1 <= x && x <= 9);
  assert(1 <= y && y <= 9);
  assert(0 <= p && p <= 0x1F);

  bo->board[(y << 4) + x] = p;
}

/** (x, y)の駒を返す。 */
int daemon_dboard_get_board(const DBoard* bo, int x, int y) 
{
  assert(bo != NULL);
  assert(1 <= x && x <= 9);
  assert(1 <= y && y <= 9);

  return bo->board[(y << 4) + x];
}

/** next 手番の持ち駒 p の枚数をセットする。 */
void daemon_dboard_set_piece(DBoard* bo, int next, int p, int n) 
{
  bo->inhand[next][p] = n;
}


/** next 手番の持ち駒 p の枚数を一枚プラスする。 */
void daemon_dboard_add_piece(DBoard* bo, int next, int p) 
{
  bo->inhand[next][p]++;
}


/** next 手番の持ち駒 p の枚数を一枚減らす。 */
void daemon_dboard_dec_piece(DBoard* bo, int next, int p) 
{
  bo->inhand[next][p]--;
}


/**
 * 持ち駒 p の枚数を返す。
 * @param bo 局面
 * @param next 手番
 * @param p 駒種
 */
int daemon_dboard_get_piece(const DBoard* bo, int next, int p) 
{
  return bo->inhand[next][p];
}

/** 平手の盤面をセットする */
void daemon_dboard_set_hirate(DBoard* bo) {
  /* 平手の配置パターン */
  static const int hirate[] = {
    0x12, 0x13, 0x14, 0x15, 0x18, 0x15, 0x14, 0x13, 0x12,
    0x00, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x17, 0x00,
    0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00,
    0x02, 0x03, 0x04, 0x05, 0x08, 0x05, 0x04, 0x03, 0x02,
  };
  int x, y;
  
  daemon_dboard_init(bo);
  
  for (y = 0; y < 9; y++) {
    for (x = 0; x < 9; x++) {
      daemon_dboard_set_board(bo, x + 1, y + 1, hirate[y * 9 + x]);
    }
  }
}

/** 詰将棋用に盤面をセットする */
void daemon_dboard_set_mate(DBoard *board) {
  daemon_dboard_init(board);

  daemon_dboard_set_board(board, 5, 1, 0x18);

  daemon_dboard_set_piece(board, D_GOTE, 1, 18);
  daemon_dboard_set_piece(board, D_GOTE, 2, 4);
  daemon_dboard_set_piece(board, D_GOTE, 3, 4);
  daemon_dboard_set_piece(board, D_GOTE, 4, 4);
  daemon_dboard_set_piece(board, D_GOTE, 5, 4); 
  daemon_dboard_set_piece(board, D_GOTE, 6, 2);
  daemon_dboard_set_piece(board, D_GOTE, 7, 2);

  daemon_dboard_set_pbox(board, 8, 1);
}

void daemon_dboard_set_rl_reversal(DBoard *board) {
  int x, y, p;
  DBoard *tmp;

  tmp = daemon_dboard_new();

  daemon_dboard_copy(board, tmp);

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      p = daemon_dboard_get_board(tmp, x, y);
      daemon_dboard_set_board(board, 10 - x, y, p);
    }
  }

  daemon_dboard_free(tmp);
}

void daemon_dboard_set_order_reversal(DBoard *board) {
  int x, y, p;
  DBoard *tmp;

  tmp = daemon_dboard_new();

  daemon_dboard_copy(board, tmp);

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      p = daemon_dboard_get_board(tmp, x, y);
      if (p != 0) {
	p ^= 0x10;
      }
      daemon_dboard_set_board(board, x, 10 - y, p);
    }
  }

  daemon_dboard_free(tmp);
}

/** 先手後手の盤面を入れ換える */
void daemon_dboard_flip(DBoard *board) {
  int x, y, p, i;
  DBoard *tmp;

  tmp = daemon_dboard_new();

  daemon_dboard_copy(board, tmp);

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      p = daemon_dboard_get_board(tmp, x, y);
      if (p != 0) {
	p ^= 0x10;
      }
      daemon_dboard_set_board(board, 10 - x, 10 - y, p);
    }
  }

  for (i=1; i<=7; i++) {
    p = daemon_dboard_get_piece(tmp, D_SENTE, i);
    daemon_dboard_set_piece(board, D_GOTE, i, p);
    p = daemon_dboard_get_piece(tmp, D_GOTE, i);
    daemon_dboard_set_piece(board, D_SENTE, i, p);
  }

  daemon_dboard_free(tmp);
}

/** 全ての駒を駒箱へ移動する */
void daemon_dboard_set_all_to_pbox(DBoard *board) {
  int x, y, i;
  int piecemax[9] = {
    0, 18, 4, 4, 4, 4, 2, 2, 2,
  };

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      daemon_dboard_set_board(board, x, y, 0);
    }
  }

  for (i=1; i<=7; i++) {
    daemon_dboard_set_piece(board, D_GOTE, i, 0);
    daemon_dboard_set_piece(board, D_SENTE, i, 0);
  }

  for (i=1; i<=8; i++) {
    daemon_dboard_set_pbox(board, i, piecemax[i]);
  }
}

/** 編集用に使っていない駒を pbox に格納する */
void daemon_dboard_set_for_edit(DBoard *board) {
  int piece[9];
  int x, y, p;
  int i;
  int piecemax[9] = {
    0, 18, 4, 4, 4, 4, 2, 2, 2,
  };
  
  for (i=0; i<9; i++) {
    piece[i] = 0;
  }

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      p = daemon_dboard_get_board(board, x, y);
      if ((p & 0xF) == 8) {
	p = 8;
      } else {
	p &= 7;
      }
      piece[p]++;
    }
  }

  for (i=1; i<=7; i++) {
    p = daemon_dboard_get_piece(board, D_SENTE, i);
    piece[i] += p;
    p = daemon_dboard_get_piece(board, D_GOTE, i);
    piece[i] += p;
  }

  for (i=1; i<=8; i++) {
    daemon_dboard_set_pbox(board, i, piecemax[i] - piece[i]);
  }
}

/** out に bo の内容を出力する */
void daemon_dboard_output(const DBoard* bo, FILE* out) 
{
  int x, y, i;
  int p;
  int mode = 1;

  assert(bo != NULL);
  assert(out != NULL);

  for (y = 1; y <= 9; y++) {
    for (x = 9; 0 < x; x--) {
      p = daemon_dboard_get_board(bo, x, y);
      if (p) {
	if (mode)
	  fprintf(out, "%c%X ", (p & 0x10) ? 'v' : '^', p & 0x0F);
	else
	  fprintf(out, "%02X ", p);
      } else {
	if (mode)
	  fprintf(out, " . ");
	else
	  fprintf(out, "00 ");
      }
    }
    fprintf(out, "\n");
  }

  fprintf(out, "%2d ", daemon_dboard_get_piece(bo, D_SENTE, 1));
  for (i=2; i<=7; i++) {
    fprintf(out, "%d ", daemon_dboard_get_piece(bo, D_SENTE, i));
  }
  fprintf(out, "\n");
  fprintf(out, "%2d ", daemon_dboard_get_piece(bo, D_GOTE, 1));
  for (i=2; i<=7; i++) {
    fprintf(out, "%d ", daemon_dboard_get_piece(bo, D_GOTE, i));
  }
  fprintf(out, "\n");
}


/**
 * te の内容で一手進める。
 * @param bo 対象のDBoard
 * @param te 指し手
 */
void daemon_dboard_move(DBoard* bo, const DTe* te) 
{
  int p;

#ifndef NDEBUG
  assert(bo != NULL);
  assert(te != NULL);

  if ( te->special == DTe::NORMAL_MOVE ) {
    if ( te_is_put(te) ) {
      assert(bo->board[te->to] == 0);
      assert(te->nari == 0);
    }
    else {
      assert(bo->board[te->fm] != 0);
      if ((bo->board[te->fm] & 0x10) != (bo->next == D_SENTE ? 0 : 0x10)) {
	printf("te->fm = %x, bo->board[te->fm] = %x, but bo->next = %d\n", 
	       te->fm,
	       bo->board[te->fm],
	       bo->next);
	abort();
      }
      assert(te->tori == bo->board[te->to]);
    }
  }
#endif // !NDEBUG

  if ( te->special != DTe::NORMAL_MOVE )
    return;

  if ( te_is_put(te) ) {
    /* 駒打ち */
    bo->board[te->to] = te->uti + (bo->next == D_GOTE ? 0x10 : 0);
    daemon_dboard_dec_piece(bo, bo->next, te->uti);
  } 
  else {
    /* 駒移動 */

    p = bo->board[te->fm];
    bo->board[te->fm] = 0;

    if (bo->board[te->to] != 0)
      daemon_dboard_add_piece(bo, bo->next, bo->board[te->to]& 7);

    if (te->nari) {
      p |= 0x08;
    }
    bo->board[te->to] = p;
  }

  daemon_dboard_toggle_next(bo);
}


/**
 * 一手戻す。
 * @param bo 対象のDBoard
 * @param te undoしたい手.
 */
void daemon_dboard_back(DBoard* bo, const DTe* te) 
{
  int p;

  assert(bo != NULL);
  assert(te);

  if (te->special != DTe::NORMAL_MOVE)
    return;

  daemon_dboard_toggle_next(bo);

  if ( te_is_put(te) ) {
    /* 駒打ち */
    bo->board[te->to] = 0;
    daemon_dboard_add_piece(bo, bo->next, te->uti);
  } 
  else {
    /* 駒移動 */
    p = bo->board[te->to];
    bo->board[te->to] = te->tori; // bo->tori[bo->count];
    if (te->tori != 0) {
      daemon_dboard_dec_piece(bo, bo->next, te->tori & 7);
    }
    if (te->nari && p != 0x08 && p != 0x18) {
      p &= 0x7;
      p += (bo->next == D_GOTE) ? 0x10 : 0;
    }
    bo->board[te->fm] = p;
  }
}


/** DBoard の中見を src から dest にコピーする */
void daemon_dboard_copy(const DBoard* src, DBoard *dest) 
{
  *dest = *src;
}


/** 先手、後手を入れ換える */
void daemon_dboard_toggle_next(DBoard *bo) 
{
  assert(bo->next == D_SENTE || bo->next == D_GOTE);

  if (bo->next == D_SENTE) {
    bo->next = D_GOTE;
  } else {
    bo->next = D_SENTE;
  }
}

void daemon_dboard_set_pbox(DBoard *bo, int p, int n) {
  assert(bo != NULL);
  assert(1 <= p && p <= 8);
  assert(0 <= n && n <= 18);

  bo->pbox[p] = n;
}

int daemon_dboard_get_pbox(DBoard *bo, int p) {
  assert(bo != NULL);
  assert(1 <= p && p <= 8);

  return bo->pbox[p];
}

void daemon_dboard_add_pbox(DBoard *bo, int p) {
  assert(bo != NULL);
  assert(1 <= p && p <= 8);

  bo->pbox[p]++;
}

void daemon_dboard_dec_pbox(DBoard *bo, int p) {
  assert(bo != NULL);
  assert(1 <= p && p <= 8);

  bo->pbox[p]--;
}


/**
 * DBoard の内容を BOARD へコピーする。
 */
void daemon_dboard_copy_board(const DBoard* dbo, BOARD* bo) 
{
  int x, y, p, i;
  int pnum;

  assert(dbo != NULL);
  assert(bo != NULL);

  boInit(bo);

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      p = daemon_dboard_get_board(dbo, x, y);
      boSetPiece(bo, (y << 4) + x, p); 
    }
  }

  for (i=1; i<=7; i++) {
    pnum = daemon_dboard_get_piece(dbo, 0, i);
    bo->inhand[0][i] = pnum;
  }

  for (i=1; i<=7; i++) {
    pnum = daemon_dboard_get_piece(dbo, 1, i);
    bo->inhand[1][i] = pnum;
  }

  bo->next = dbo->next;
  boSetToBOARD(bo);
}


/**
 * BOARD の内容を DBoard へコピーする。
 */
void daemon_dboard_copy_from_board(const BOARD* src, DBoard* dest) 
{
  int x, y, p, i;

  assert(src != NULL);
  assert(src->next == 0 || src->next == 1);
  assert(dest != NULL);

  daemon_dboard_init(dest);

  for (y=1; y<=9; y++) {
    for (x=1; x<=9; x++) {
      p = boGetPiece(src, (y << 4) + x); 
      daemon_dboard_set_board(dest, x, y, p);
    }
  }

  for (i=1; i<=7; i++) {
    p = src->inhand[0][i];
    daemon_dboard_set_piece(dest, 0, i, p);

    p = src->inhand[1][i];
    daemon_dboard_set_piece(dest, 1, i, p);
  }

  dest->next = src->next == 0 ? D_SENTE : D_GOTE;
}

