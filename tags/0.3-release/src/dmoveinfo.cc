/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "dte.h"
#include "dmoveinfo.h"


/** コンストラクタ */
DMoveInfo* daemon_dmoveinfo_new(KyokumenNode* p, const DTe* m) 
{
  DMoveInfo* mi = new DMoveInfo(p, m);
  if (!mi) {
    printf("No enough memory in daemon_dmovenifo_new().");
    abort();
  }

  return mi;
}


/** デストラクタ */
void daemon_dmoveinfo_free(DMoveInfo* mi) 
{
  if (mi)
    delete mi;
}


/** main_line の先端に手を追加 */
void daemon_dmoveinfo_add(DMoveInfo* mi, const DTe* te, int sec) 
{
  assert(mi != NULL);
  assert(te != NULL);

  while (mi->moves.size() > 0)
    mi = mi->moves.begin()->node;

  mi->add_child(*te, sec, "");
}


/** main_line の先端の手を削除する */
void daemon_dmoveinfo_pop(DMoveInfo* mi) 
{
  assert(mi != NULL);

  while (mi->moves.size() > 0)
    mi = mi->moves.begin()->node;

  if (!mi->prev) // 一手もないとき
    return;

  DTe te = *mi->move_to_back;
  mi = mi->prev;
  mi->remove_child(te);
}

