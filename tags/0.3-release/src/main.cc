/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * main.c 
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/main.c,v $
 * $Id: main.c,v 1.1.1.1 2005/12/09 09:03:06 tokita Exp $
 */


#define GTK_DISABLE_DEPRECATED 1
#define GDK_DISABLE_DEPRECATED 1
#define G_DISABLE_DEPRECATED 1

#include <config.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS
#include "interface.h"
#include "support.h"
G_END_DECLS

#include "canvas.h"
#include "misc.h"
#include "si/si.h"
#include "si/ui.h"

/** global application instance */
Canvas* g_canvas = NULL;

int main (int argc, char* argv[])
{
  GtkWidget *window;

#ifdef ENABLE_NLS
  bindtextdomain(GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
  textdomain(GETTEXT_PACKAGE);
#endif

  gtk_set_locale();
  gtk_init(&argc, &argv);

  add_pixmap_directory( PACKAGE_DATA_DIR "/" PACKAGE );

  if (!g_thread_supported())
    g_thread_init(NULL);

  window = create_window();
  g_canvas = daemon_canvas_new(window);

  set_pending_function(pending_loop);

  gtk_widget_show(window);

  gtk_main();
  return 0;
}

