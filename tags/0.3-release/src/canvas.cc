/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#define GTK_DISABLE_DEPRECATED 1
#define GDK_DISABLE_DEPRECATED 1
#define G_DISABLE_DEPRECATED 1

#include <config.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>

G_BEGIN_DECLS
#include "interface.h"
#include "support.h"
// #include "callbacks.h"
G_END_DECLS
#include "canvas.h"
#include "history_window.h"
#include "misc.h"
#include "si/si.h"
#include "si/ui.h"

#include "back.xpm"
#include "koma.xpm"


static void daemon_canvas_draw_sprite_all                 (Canvas *canvas);
static void daemon_canvas_draw_sprite_update_all          (Canvas *canvas,
						    GdkRectangle *rect);
static void daemon_canvas_draw_sprite                     (Canvas *canvas,
						    gint no);


/** 初期化 */
static void daemon_canvas_init( Canvas* canvas )
{
  // GtkWidget *widget;
  // g_assert(window != NULL);
  g_assert(canvas != NULL);

  // canvas->window      = window;
  GtkWidget* window = canvas->window;

  daemon_canvas_set_size(canvas, D_CANVAS_SIZE_SMALL);
  canvas->pixmap[0]   = gdk_pixmap_new(window->window,
				       D_CANCAS_SIZE_SMALL_X,
				       D_CANCAS_SIZE_SMALL_Y,
				       -1);
  canvas->pixmap[1]   = gdk_pixmap_new(window->window,
				       D_CANCAS_SIZE_MIDDLE_X,
				       D_CANCAS_SIZE_MIDDLE_Y,
				       -1);
  canvas->pixmap[2]   = gdk_pixmap_new(window->window,
				       D_CANCAS_SIZE_BIG_X,
				       D_CANCAS_SIZE_BIG_Y,
				       -1);
  canvas->gc          = gdk_gc_new(window->window);
  canvas->drawingarea = lookup_widget(GTK_WIDGET(window), "drawingarea");
  canvas->drag        = FALSE;
  canvas->mode        = D_CANVAS_MODE_EDIT;
  canvas->back_src    = gdk_pixbuf_new_from_xpm_data(back_xpm);
  if (canvas->back_src == NULL) {
    daemon_abort("No enough memory in daemon_canvas_init(). back_src is NULL\n");
  }
  canvas->koma_src    = gdk_pixbuf_new_from_xpm_data(koma_xpm);
  if (canvas->back_src == NULL) {
    daemon_abort("No enough memory in daemon_canvas_init(). koma_src is NULL\n");
  }
  canvas->back[0] = NULL;
  canvas->back[1] = NULL;
  canvas->back[2] = NULL;
  daemon_canvas_create_back_pixmap(canvas);
  daemon_dboard_set_hirate(&(canvas->board));

  canvas->history = new History(canvas);

  // canvas->network_log_window = NULL;
  canvas->move_property_dialog = NULL;

  canvas->tmp_pixmap = NULL;

  /* スプライト初期化 */
  daemon_canvas_init_sprite(canvas);
  /* スプライト配置 */
  daemon_canvas_dispose_sprite(canvas);
/*
  // 消費時間を表示
  daemon_canvas_draw_time(canvas);
*/

  canvas->record = NULL;
  canvas->front = D_SENTE;
  daemon_canvas_set_sensitive(canvas);
  canvas->flg_thinking = FALSE;
}


/** コンストラクタ */
Canvas* daemon_canvas_new(GtkWidget* window) 
{
  Canvas* canvas;

  g_assert(window != NULL); 

  canvas = new Canvas();
  if (canvas == NULL) {
    daemon_abort(_("No enough memory in daemon_canvas_new()."));
  }
  canvas->window = window;

  return canvas;
}


static void daemon_canvas_invalidate_rect(Canvas* canvas, 
					  const GdkRectangle* rect) 
{
  gdk_window_invalidate_rect(canvas->drawingarea->window,
                             rect, FALSE);
}


static void daemon_canvas_invalidate(Canvas* canvas) 
{
  GdkRectangle rect;

  rect.x = 0;
  rect.y = 0;
  rect.width = canvas->width;
  rect.height = canvas->height;

  daemon_canvas_invalidate_rect(canvas, &rect);
}


/** gtk+オブジェクトを構築する */
void canvas_build(Canvas* canvas)
{
  daemon_canvas_init(canvas);

  /* 背景描画 */
  daemon_canvas_draw_back(canvas);

  /* 手番、時刻更新 */
  daemon_canvas_draw_time(canvas);

  /* スプライト描画 */
  daemon_canvas_draw_sprite_all(canvas);
  /* 更新 */
  daemon_canvas_invalidate(canvas);
  
  /* ステータスバー更新 */
  daemon_canvas_update_statusbar(canvas);
}


/** デストラクタ */
void daemon_canvas_free(Canvas* canvas) 
{
  if (canvas != NULL)
    delete canvas;
}


void daemon_canvas_init_sprite(Canvas* canvas)
{
  GdkPixbuf *im;
  GdkPixbuf *im2;
  Sprite *sprite;
  gint num, i;

  for (i=0; i<SPRITEMAX; i++) {
    canvas->sprite[i] = NULL;
  }

  num = 0;

  // いくつかの大きさの駒pixmapを作る
  im = canvas->koma_src;
  gdk_pixbuf_render_pixmap_and_mask(im, 
				    &canvas->koma[0], &canvas->mask[0],
				    128);

  im2 = gdk_pixbuf_scale_simple(im, 400, 200, GDK_INTERP_NEAREST);
  if (im2 == NULL) {
    daemon_abort("No enough memory in daemon_canvas_init_sprite(). im2 is NULL\n");
  }
  gdk_pixbuf_render_pixmap_and_mask(im2, 
				    &canvas->koma[1], &canvas->mask[1],
				    128);
  g_object_unref(im2);

  im2 = gdk_pixbuf_scale_simple(im, 512, 256, GDK_INTERP_NEAREST);
  if (im2 == NULL) {
    daemon_abort("No enough memory in daemon_canvas_init_sprite(). im2 is NULL\n");
  }
  gdk_pixbuf_render_pixmap_and_mask(im2, 
				    &canvas->koma[2], &canvas->mask[2],
				    128);
  g_object_unref(im2);

  for (i=0; i<SPRITEMAX; i++) {
    sprite = daemon_sprite_new(canvas->koma[0], canvas->mask[0], 0, 0, 40, 40);
    canvas->sprite[num] = sprite;
    daemon_sprite_set_type(sprite, KOMA_SPRITE);
    // daemon_sprite_set_src_x(sprite, 0);
    // daemon_sprite_set_src_y(sprite, 0);
    // daemon_sprite_set_width(sprite, 40);
    // daemon_sprite_set_height(sprite, 40);
    daemon_sprite_set_visible(sprite, FALSE);
    daemon_sprite_set_move(canvas->sprite[num], TRUE);
    num++;
  }
}


/** 持ち駒の種類の数を数える */
static int count_piece(const DBoard* board, D_TEBAN next) 
{
  gint count;
  gint i;

  count = 0;
  for (i=1; i <= 7; i++) {
    if (0 < daemon_dboard_get_piece(board, next, i)) {
      count++;
    }
  }

  return count;
}


/** スプライトを配置する */
void daemon_canvas_dispose_sprite(Canvas* canvas) 
{
  DBoard* board;
  Sprite* sprite;
  gint x, y, num;
  gint width, height;
  gint base_x, base_y;
  gint p;
  gint i, j;
  
  board = &(canvas->board);
  num = 0;

  base_x = 0;
  base_y = 0;

  if (canvas->size == D_CANVAS_SIZE_SMALL) {
    base_x = 140;
    base_y = 10;
  } else if (canvas->size == D_CANVAS_SIZE_MIDDLE) {
    base_x = 140 * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X;
    base_y = 10 * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X;
  } else if (canvas->size == D_CANVAS_SIZE_BIG) {
    base_x = 140 * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X;
    base_y = 10 * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X;
  } 
  else {
    /* 仕様上ありえない分岐 */
    g_assert(0);
  }
  
  for (i=0; i<SPRITEMAX; i++) {
    sprite = daemon_canvas_get_sprite(canvas, i);
    if (sprite == NULL) {
      continue;
    }
    if (daemon_sprite_get_type(sprite) != KOMA_SPRITE) {
      continue;
    }
    daemon_sprite_set_visible(sprite, FALSE);
  }

  /* 盤に駒を配置 */
  for (y=1; y <= 9; y++) {
    for (x=1; x <= 9; x++) {
      p = daemon_dboard_get_board(board, x, y);
      if (!p)
	continue;

      g_assert(1 <=p && p <= 0x1F);
      sprite = daemon_canvas_get_sprite(canvas, num++);
      g_assert(sprite != NULL);
      while (sprite->type != KOMA_SPRITE) {
	sprite = daemon_canvas_get_sprite(canvas, num++);
	g_assert(sprite != NULL);
      }
      width  = daemon_sprite_get_width(sprite);
      height = daemon_sprite_get_height(sprite);
      if (daemon_canvas_get_front(canvas) == D_SENTE) {
	daemon_sprite_set_x(sprite, (10 - x) * width - width + base_x);
	daemon_sprite_set_y(sprite, y * height - height + base_y);
	daemon_sprite_set_koma(sprite, p);
      }
      else {
	daemon_sprite_set_x(sprite, (10 - (10-x)) * width - width + base_x);
	daemon_sprite_set_y(sprite, (10-y) * height - height + base_y);
	daemon_sprite_set_koma(sprite, p ^ 0x10);
      }
      daemon_sprite_set_visible(sprite, TRUE);
    }
  }

  /* 持ち駒を配置する */
  {
    static const int base_y_value[9] = {
      0, 65, 65, 45, 45, 25, 5, 5, 5,
      // {0, 65, 65, 85, 85, 105, 125, 125, 125},
    };
    gint count, flg;
    gint x, y, skip_x;
    int komadai;

    // komadai = 0 で画面右手
    for (komadai = 0; komadai < 2; komadai++) {
      D_TEBAN teban;
      if (daemon_canvas_get_front(canvas) == D_SENTE)
	teban = komadai == 0 ? D_SENTE : D_GOTE;
      else
	teban = komadai == 0 ? D_GOTE : D_SENTE;

      count = count_piece(&canvas->board, teban); // 持ち駒の種類の数
      if (komadai == 0) {
	base_x = 511; // (510,210) - (640,380)
	base_y = 210 + base_y_value[count];
      }
      else {
	base_x = 129 - 40; // (0,0) - (130,170)
	base_y = 170 - base_y_value[count] - 40;
      }
      flg = 0; // 手前のとき、=0で左側、=1で右側

      // 大駒から順に表示
      for (i = 7; i >= 1; i--) {
	int p = daemon_dboard_get_piece(board, teban, i);
	if (!p)
	  continue;

	if (i == 1) {
	  skip_x = 80;
	  if (flg == 1) {
	    base_y += komadai == 0 ? +40 : -40;
	    flg = 0;
	  }
	} 
	else
	  skip_x = 20;

	// 駒を重ねて表示
	for (j = 0; j < p; j++) {
	  x = 5 + (skip_x - skip_x / p * (j + 1)) + (flg ? 60 : 0);

	  sprite = daemon_canvas_get_sprite(canvas, num++);
	  g_assert(sprite != NULL);
	  while (sprite->type != KOMA_SPRITE) {
	    sprite = daemon_canvas_get_sprite(canvas, num++);
	    g_assert(sprite != NULL);
	  }

	  daemon_sprite_set_x(sprite, 
	      daemon_canvas_rate_xy(canvas, base_x + (komadai == 0 ? +x : -x)));
	  daemon_sprite_set_y(sprite, daemon_canvas_rate_xy(canvas, base_y));
	  daemon_sprite_set_koma(sprite, i + komadai * 0x10);
	  daemon_sprite_set_visible(sprite, TRUE);
	}

	if (flg == 1) {
	  base_y += komadai == 0 ? +40 : -40;
	  flg = 0;
	} 
	else
	  flg = 1;
      }
    }

    /* 駒箱 */
    if (daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_EDIT) {
    count = count_pbox(&(canvas->board));
    base_x = 1;
    base_y = 210;
    flg = 0;
    skip_x = 20;

    for (i=8; 1 <= i; i--) {
      p = daemon_dboard_get_pbox(board, i);
      if (p == 0) {
	continue;
      }
      if (0 && i == 1) {
	skip_x = 80;
	flg = 0;
      } else {
	skip_x = 20;
      }
      for (j=0; j<p; j++) {
	x = 5 + (skip_x - skip_x / p * (j + 1)) + (flg ? 60 : 0);
	y = base_y_value[count];

	sprite = daemon_canvas_get_sprite(canvas, num++);
	g_assert(sprite != NULL);
	//	while (sprite->type != KOMA_SPRITE) {
	//sprite = daemon_canvas_get_sprite(canvas, num++);
	// g_assert(sprite != NULL);
	// }

	daemon_sprite_set_x(sprite, daemon_canvas_rate_xy(canvas, base_x + x));
	daemon_sprite_set_y(sprite, daemon_canvas_rate_xy(canvas, base_y + y));
	daemon_sprite_set_koma(sprite, i);
	daemon_sprite_set_visible(sprite, TRUE);
      }

      if (flg == 1) {
	base_y += 40;
	flg = 0;
      } else {
	flg = 1;
      }
    }
    }
  }
}

/*************************************************************/
/* set_* , get_* */
/*************************************************************/

GtkWidget* daemon_canvas_get_window(Canvas *canvas) {
  return canvas->window;
}

GdkGC* daemon_canvas_get_gc(Canvas *canvas) {
  return canvas->gc;
}

GdkPixmap *daemon_canvas_get_pixmap(Canvas *canvas) {
  g_assert(canvas != NULL);
  if (canvas->size == D_CANVAS_SIZE_SMALL) {
    return canvas->pixmap[0];
  } else if (canvas->size == D_CANVAS_SIZE_MIDDLE) {
    return canvas->pixmap[1];
  } else if (canvas->size == D_CANVAS_SIZE_BIG) {
    return canvas->pixmap[2];
  } else {
    /* 仕様上ありえない分岐 */
    g_assert(1);
  }

  return NULL;
}

GdkPixmap *daemon_canvas_get_back(Canvas *canvas) {
  g_assert(canvas != NULL);
  if (canvas->size == D_CANVAS_SIZE_SMALL) {
    return canvas->back[0];
  } else if (canvas->size == D_CANVAS_SIZE_MIDDLE) {
    return canvas->back[1];
  } else if (canvas->size == D_CANVAS_SIZE_BIG) {
    return canvas->back[2];
  } else {
    /* 仕様上ありえない分岐 */
    g_assert(1);
  }

  return NULL;
}

GtkWidget* daemon_canvas_get_drawinarea(Canvas *canvas) {
  g_assert(canvas != NULL);
  return canvas->drawingarea;
}

gboolean daemon_canvas_isdrag(Canvas *canvas) {
  g_assert(canvas != NULL);
  return canvas->drag;
}

void daemon_canvas_set_drag(Canvas *canvas, gboolean drag) {
  g_assert(canvas != NULL);
  g_assert(drag == TRUE || drag == FALSE);
  canvas->drag = drag;
}

gint daemon_canvas_get_dragno(Canvas *canvas) {
  g_assert(canvas != NULL);
  return canvas->drag_no;
}

void daemon_canvas_set_dragno(Canvas *canvas, gint no) {
  g_assert(canvas != NULL);
  g_assert(0 <= no);
  canvas->drag_no = no;
}

GdkPoint daemon_canvas_get_diff(Canvas *canvas) {
  g_assert(canvas != NULL);
  return canvas->diff;
}

void daemon_canvas_set_diff(Canvas *canvas, gint x, gint y) {
  g_assert(canvas != NULL);
  g_assert(0 <= x);
  g_assert(0 <= y);
  canvas->diff.x = x;
  canvas->diff.y = y;
}

D_CANVAS_MODE daemon_canvas_get_mode(const Canvas* canvas)
{
  return canvas->mode;
}

void daemon_canvas_set_mode(Canvas *canvas, D_CANVAS_MODE mode) {
  canvas->mode = mode;
}

/** canvas のサイズをセットする */
void daemon_canvas_set_size(Canvas *canvas, D_CANVAS_SIZE size) {
  g_assert(canvas != NULL);
  g_assert(size == D_CANVAS_SIZE_SMALL ||
	   size == D_CANVAS_SIZE_MIDDLE ||
	   size == D_CANVAS_SIZE_BIG);

  canvas->size = size;
  if (size == D_CANVAS_SIZE_SMALL) {
    canvas->width  = D_CANCAS_SIZE_SMALL_X;
    canvas->height = D_CANCAS_SIZE_SMALL_Y;
  } else if (size == D_CANVAS_SIZE_MIDDLE) {
    canvas->width  = D_CANCAS_SIZE_MIDDLE_X;
    canvas->height = D_CANCAS_SIZE_MIDDLE_Y;
  } else if (size == D_CANVAS_SIZE_BIG) {
    canvas->width  = D_CANCAS_SIZE_BIG_X;
    canvas->height = D_CANCAS_SIZE_BIG_Y;
  } else {
    /* 仕様上ありえない分岐 */
    g_assert(1);
  }
}

Sprite* daemon_canvas_get_sprite(Canvas* canvas, gint no) {
  g_assert(canvas != NULL);
  g_assert(0 <= no);
  g_assert(no < SPRITEMAX);

  return canvas->sprite[no];
}

void daemon_canvas_set_front(Canvas* canvas, D_TEBAN player) 
{
  canvas->front = player;
}

D_TEBAN daemon_canvas_get_front(const Canvas* canvas) 
{
  return canvas->front;
}


/*************************************************************/
/* event */
/*************************************************************/

/** マウスドラッグ */
void daemon_canvas_motion_notify(Canvas* canvas, GdkEventMotion* event)
{
  GdkModifierType mask;
  gint x, y;
  gint no;
  Sprite* sprite;
  GdkPoint diff;
  GdkRectangle rect;

  g_assert(canvas != NULL);
  g_assert(event != NULL);

  if (event->is_hint) {
    gdk_window_get_pointer (event->window, &x, &y, &mask);
  } else {
    x = event->x;
    y = event->y;
  }
  
  if (daemon_canvas_isdrag(canvas) == FALSE) {
    /* ドラッグ中でなければ戻る */
    return;
  }

  if (canvas->tmp_pixmap == NULL) {
    daemon_canvas_create_tmp_pixmap(canvas);
  }

  /* ダブルクリック判定用時刻を初期化 */
  canvas->tv.tv_sec  = 0;
  canvas->tv.tv_usec = 0;

  no     = daemon_canvas_get_dragno(canvas);
  sprite = daemon_canvas_get_sprite(canvas, no);
  diff   = daemon_canvas_get_diff(canvas);

  rect.x      = daemon_sprite_get_x(sprite);
  rect.y      = daemon_sprite_get_y(sprite);
  rect.width  = daemon_sprite_get_width(sprite);
  rect.height = daemon_sprite_get_height(sprite);

  daemon_sprite_set_x(sprite, event->x - diff.x);
  daemon_sprite_set_y(sprite, event->y - diff.y);

  /* 背景を描く */
  daemon_canvas_draw_back_update_from_tmp_pixmap(canvas, &rect);
  daemon_canvas_draw_sprite(canvas, no);
  daemon_canvas_invalidate_rect(canvas, &rect);
}


/** マウスボタンが押された */
void daemon_canvas_button_press( Canvas* canvas, GdkEventButton* event)
{
  gint     no;
  gboolean doubleclick = FALSE;
  Sprite* sprite;
  gint sprite_x, sprite_y;
  gint dest_x, dest_y;
  D_CANVAS_AREA area;
  SPRITE_TYPE type;
  D_CANVAS_MODE mode;
  gint p;

  g_assert(canvas != NULL);
  g_assert(event != NULL);

  if (event->type != GDK_BUTTON_PRESS)
    return;

  if (event->button != 1) {
    /* 1 番のボタン以外が押された */
    return;
  }

#if 0
  if (canvas->mode == D_CANVAS_MODE_BOOK) {
    /* 棋譜モードではクリックの処理はしない */
    return;
  }
#endif // 0

  if (daemon_canvas_isdrag(canvas) == TRUE) {
    /* ドラッグ中にクリックされた */
    daemon_canvas_set_drag(canvas, FALSE);
    return;
  }

  no = daemon_canvas_on_sprite(canvas, event);

  if (no == -1) {
    /* クリックされたスプライトなし */
    return;
  }

  sprite = daemon_canvas_get_sprite(canvas, no);
  doubleclick = daemon_canvas_is_doubleclick(canvas);

  sprite_x = daemon_sprite_get_x(sprite);
  sprite_y = daemon_sprite_get_y(sprite);
  area = daemon_canvas_on_drag_area(canvas, sprite_x, sprite_y, &dest_x, &dest_y);
  type = daemon_sprite_get_type(sprite);
  mode = daemon_canvas_get_mode(canvas);

  if ( mode == D_CANVAS_MODE_GAME || mode == D_CANVAS_MODE_BOOK ) {
    if (type != KOMA_SPRITE)
      return;

    if ( mode == D_CANVAS_MODE_GAME &&
         canvas->playertype[canvas->board.next] != SI_GUI_HUMAN ) {
      printf("expected %d, but %d\n",
             SI_HUMAN, canvas->playertype[canvas->board.next]);  // DEBUG
      return;
    }

    p = daemon_sprite_get_koma(sprite);

    if (daemon_canvas_get_front(canvas) == D_SENTE) {
      if (canvas->board.next == D_SENTE && 0x10 < p) {
	return;
      } else if (canvas->board.next == D_GOTE && p < 0x10) {
	return;
      }
    } 
    else {
      if (canvas->board.next == D_SENTE && p < 0x10) {
	return;
      } else if (canvas->board.next == D_GOTE && 0x10 < p) {
	return;
      }
    }
  }
  else if ( mode == D_CANVAS_MODE_EDIT ) {
    if (doubleclick && area == D_CANVAS_AREA_BOARD && type == KOMA_SPRITE) {
      /* ダブルクリックされた */
    gint koma;
    GdkRectangle rect;

    koma = daemon_sprite_get_koma(sprite);
    if ((koma & 0x0F) == 0x05 || (koma & 0x0F) == 0x08) {
      if (koma <= 0x10) {
	koma += 0x10;
      } else {
	koma -= 0x10;
      }
    } else if (koma <= 0x10) {
      if (koma & 0x08) {
	koma += 0x10;
      }
      koma ^= 0x08;
    } else {
      if (koma & 0x08) {
	koma -= 0x10;
      }
      koma ^= 0x08;
    }
    daemon_sprite_set_koma(sprite, koma);
    daemon_dboard_set_board(&(canvas->board), dest_x, dest_y, koma);
    
    rect.x      = daemon_sprite_get_x(sprite);
    rect.y      = daemon_sprite_get_y(sprite);
    rect.width  = daemon_sprite_get_width(sprite);
    rect.height = daemon_sprite_get_height(sprite);
    /* 背景を描く */
    daemon_canvas_draw_back_update(canvas, &rect);
    /* 駒を描く */
    daemon_canvas_draw_sprite(canvas, no);
    daemon_canvas_draw_sprite_update_all(canvas, &rect);
    daemon_canvas_draw_sprite_all(canvas);
    
    daemon_canvas_invalidate(canvas);
    
      return;
    }
  }

  // スプライト上で押された -> ドラッグ開始 
  gint x, y;

    if (daemon_sprite_ismove(sprite) == TRUE) {
      x = daemon_sprite_get_x(sprite);
      y = daemon_sprite_get_y(sprite);

      daemon_sprite_set_from_x(sprite, x);
      daemon_sprite_set_from_y(sprite, y);

      daemon_canvas_set_dragno(canvas, no);
      daemon_canvas_set_diff(canvas, event->x - x, event->y - y);
      daemon_canvas_set_drag(canvas, TRUE);
    }
}

/**
 * (x, y) の座標が盤面のどのエリアにあるか調べる。<br>
 * 返り値が<UL>
 * <LI> 0 : 該当エリアなし
 * <LI> 1 : 左上駒台
 * <LI> 2 : 右下駒台
 * <LI> 3 : 左下駒箱
 * <LI> 4 : 盤の上
 * </UL>
 * 返り値が 4 の場合は、その盤上の座標をdest_x dest_y にセットする
 *  (1, 1) 〜 (9, 9) の値。
 */
D_CANVAS_AREA daemon_canvas_on_drag_area(Canvas *canvas, gint x, gint y,
			       gint *dest_x, gint *dest_y) {
  GdkPoint point;
  GdkRectangle left_koma, right_koma, komabako, board;

  point.x = daemon_canvas_rerate_xy(canvas, x) + D_SPRITE_KOMA_WIDTH / 2;
  point.y = daemon_canvas_rerate_xy(canvas, y) + D_SPRITE_KOMA_HEIGHT / 2;

  left_koma.x = 1;
  left_koma.y = 1;
  left_koma.width  = 129;
  left_koma.height = 169;
  if (daemon_on_rectangle(&left_koma, &point) == TRUE) {
    return D_CANVAS_AREA_LEFT_KOMADAI;
  }

  right_koma.x = 511;
  right_koma.y = 210;
  right_koma.width  = 129;
  right_koma.height = 169;
  if (daemon_on_rectangle(&right_koma, &point) == TRUE) {
    return D_CANVAS_AREA_RIGHT_KOMADAI;
  }

  komabako.x = 1;
  komabako.y = 210;
  komabako.width  = 129;
  komabako.height = 169;
  if (daemon_on_rectangle(&komabako, &point) == TRUE) {
    return D_CANVAS_AREA_KOMABAKO;
  }

  board.x = 140;
  board.y = 11;
  board.width  = 360;
  board.height = 360;
  if (daemon_on_rectangle(&board, &point) == TRUE) {
    /* 10 - は駒の座標の右向き、左向きを反転させるため */
    *dest_x = 10 - ((point.x - board.x) / D_SPRITE_KOMA_WIDTH + 1);
    *dest_y = (point.y - board.y) / D_SPRITE_KOMA_HEIGHT + 1;
    return D_CANVAS_AREA_BOARD;
  }

  return D_CANVAS_AREA_NONE;
}


extern int MATE_DEEP_MAX;
extern int THINK_DEEP_MAX;

static void button_release_on_edit_mode(Canvas* canvas, GdkEventButton* event)
{
    gint to_x, to_y;
    gint from_x, from_y;
    gint sprite_x, sprite_y;
    gint sprite_from_x, sprite_from_y;
    gint no;
    gint to_ret, from_ret;
    Sprite *sprite;
    gint p, p2;
    SPRITE_TYPE type;

    no            = daemon_canvas_get_dragno(canvas);
    sprite        = daemon_canvas_get_sprite(canvas, no);
    sprite_x      = daemon_sprite_get_x(sprite);
    sprite_y      = daemon_sprite_get_y(sprite);
    sprite_from_x = daemon_sprite_get_from_x(sprite);
    sprite_from_y = daemon_sprite_get_from_y(sprite);
    type          = daemon_sprite_get_type(sprite);

    to_x = to_y = 0;
    to_ret = daemon_canvas_on_drag_area(canvas, sprite_x, sprite_y,
					&to_x, &to_y);

    from_ret = daemon_canvas_on_drag_area(canvas, sprite_from_x, sprite_from_y,
					  &from_x, &from_y);

    p = daemon_sprite_get_koma(sprite);

    if (from_ret == D_CANVAS_AREA_NONE ||
	to_ret == D_CANVAS_AREA_NONE ||
	((p & 0xF) == 0x8 &&
	 (to_ret == D_CANVAS_AREA_LEFT_KOMADAI ||
	  to_ret == D_CANVAS_AREA_RIGHT_KOMADAI))) {
      /* 駒台、盤の外にドラッグしようとした */

      from_x = daemon_sprite_get_from_x(sprite);
      from_y = daemon_sprite_get_from_y(sprite);
      daemon_sprite_set_x(sprite, from_x);
      daemon_sprite_set_y(sprite, from_y);

      canvas_redraw(canvas);
    } 
    else if (to_x == from_x && to_y == from_y && 
	     from_ret == D_CANVAS_AREA_BOARD) {
      daemon_dboard_set_board(&(canvas->board), from_x, from_y, p);

      canvas_redraw(canvas);
    } 
    else if (type == KOMA_SPRITE) {
      if (from_ret == D_CANVAS_AREA_BOARD) {
	daemon_dboard_set_board(&(canvas->board), from_x, from_y, 0);
      } else if (from_ret == D_CANVAS_AREA_LEFT_KOMADAI) {
	daemon_dboard_dec_piece(&(canvas->board),
				daemon_canvas_get_front(canvas) ^ 1,
				p & 7);
      } else if (from_ret == D_CANVAS_AREA_RIGHT_KOMADAI) {
	daemon_dboard_dec_piece(&(canvas->board),
				daemon_canvas_get_front(canvas),
				p & 7);
      } else {
	p &= 0xF;
	p = (p != 8) ? p & 7 : p;
	daemon_dboard_dec_pbox(&(canvas->board), p);
      }
      if (to_ret == D_CANVAS_AREA_BOARD) {
	p2 = daemon_dboard_get_board(&(canvas->board), to_x, to_y);
	if ((p2 & 0xF) == 8) {
	  p2 &= 0xF;
	  p2 = (p2 != 8) ? p2 & 7 : p2;
	  daemon_dboard_add_pbox(&(canvas->board), p2);
	} else if (p2 != 0) {
	  if (daemon_canvas_get_front(canvas) == D_SENTE) {
	    daemon_dboard_add_piece(&(canvas->board),
				    (p < 0x10) ? D_SENTE : D_GOTE,
				    p2 & 7);
	  } else {
	    daemon_dboard_add_piece(&(canvas->board),
				    (p < 0x10) ? D_GOTE : D_SENTE,
				    p2 & 7);
	  }
	}
	daemon_dboard_set_board(&(canvas->board), to_x, to_y, p);
      } else if (to_ret == D_CANVAS_AREA_LEFT_KOMADAI) {
	daemon_dboard_add_piece(&(canvas->board),
				daemon_canvas_get_front(canvas) ^ 1,
				p & 7);
      } else if (to_ret == D_CANVAS_AREA_RIGHT_KOMADAI) {
	daemon_dboard_add_piece(&(canvas->board),
				daemon_canvas_get_front(canvas),
				p & 7);
      } else {
	p &= 0xF;
	p = (p != 8) ? p & 7 : p;
	daemon_dboard_add_pbox(&(canvas->board), p);
      }

      canvas_redraw(canvas);
    }
}


static void button_release_on_game_mode(Canvas* canvas, GdkEventButton* event)
{
  gint to_x, to_y;
  gint from_x, from_y;
  gint sprite_x, sprite_y;
  gint sprite_from_x, sprite_from_y;
  gint no;
  gint to_ret, from_ret;
  gint p;
  Sprite *sprite;
  SPRITE_TYPE type;

  no            = daemon_canvas_get_dragno(canvas);
  sprite        = daemon_canvas_get_sprite(canvas, no);
  sprite_x      = daemon_sprite_get_x(sprite);
  sprite_y      = daemon_sprite_get_y(sprite);
  sprite_from_x = daemon_sprite_get_from_x(sprite);
  sprite_from_y = daemon_sprite_get_from_y(sprite);
  type          = daemon_sprite_get_type(sprite);

  to_x = to_y = 0;
  to_ret = daemon_canvas_on_drag_area(canvas, sprite_x, sprite_y,
					&to_x, &to_y);

  from_ret = daemon_canvas_on_drag_area(canvas, sprite_from_x, sprite_from_y,
					  &from_x, &from_y);

  p = daemon_sprite_get_koma(sprite);

  if (from_ret == D_CANVAS_AREA_NONE ||
      to_ret == D_CANVAS_AREA_NONE ||
      to_ret != D_CANVAS_AREA_BOARD) {
    /* 駒台、盤の外にドラッグしようとした */

    from_x = daemon_sprite_get_from_x(sprite);
    from_y = daemon_sprite_get_from_y(sprite);
    daemon_sprite_set_x(sprite, from_x);
    daemon_sprite_set_y(sprite, from_y);

    canvas_redraw(canvas);
    return;
  }

  assert( type == KOMA_SPRITE );
  
  TE te;
  gboolean just_te;

  if (from_ret == D_CANVAS_AREA_LEFT_KOMADAI) {
	te.fm = 0;
	te.to = (to_y << 4) + to_x;
	te.nari = 0;
	te.uti = p & 7;
  } else if (from_ret == D_CANVAS_AREA_RIGHT_KOMADAI) {
	te.fm = 0;
	te.to = (to_y << 4) + to_x;
	te.nari = 0;
	te.uti = p & 7;
  } else {
	te.fm = (from_y << 4) + from_x;
	te.to = (to_y << 4) + to_x;
	te.nari = 0;
	te.uti = 0;
  }

  if (daemon_canvas_get_front(canvas) == D_GOTE) {
    gint x, y;
    if (te.fm) {
	  x = te.fm & 0xF;
	  y = te.fm >> 4;
	  te.fm = ((10 - y) << 4) + (10 - x);
	}
	x = te.to & 0xF;
	y = te.to >> 4;
	te.to = ((10 - y) << 4) + (10 - x);
  }

  // 合法な手？
  just_te = FALSE;
  if ( te_is_put(&te) )
    just_te = is_valid_move(&g_board, &te);
  else {
    just_te = is_valid_move(&g_board, &te);
    
    TE te2 = te;
    te2.nari = true;
    bool just_nari = is_valid_move(&g_board, &te2);
    
    if (just_nari) {
      if (just_te) {
        if ( daemon_canvas_yes_no_dialog(canvas, _("Promote ?")) == 
             GTK_RESPONSE_YES )
          te.nari = true;
      }
      else {
	just_te = true;
        te.nari = true;
      }
    }
  }

  if (just_te) {
    // 千日手？
    bool forbidden = false; // 連続王手
    if (is_move_sennichite(&g_board, &te, &forbidden)) {
      if (forbidden)
	just_te = false; // 連続王手ではない千日手は指してよい
    }
  }

  if (just_te) {
    if (canvas->mode == D_CANVAS_MODE_GAME) {
      canvas->gui_te = te;
      daemon_canvas_invalidate(canvas);
    }
    else if (canvas->mode == D_CANVAS_MODE_BOOK) {
      if ( canvas->history->focus_line.size() <= canvas->history->cur_pos ) {
        // 棋譜を伸ばす
      }
      else if ( teCmpTE(&canvas->history->focus_line[canvas->history->cur_pos].first,
                        &te) ) {
        // なぞらない。分岐
        int r = daemon_canvas_yes_no_dialog(canvas, _("Do you add alternative?"));
        if (r == GTK_RESPONSE_YES) {
          // TODO:
        }
      }
      else {
        // 棋譜をなぞる
      }
    }
    else {
      assert(0);
    }
  } 
  else {
    from_x = daemon_sprite_get_from_x(sprite);
    from_y = daemon_sprite_get_from_y(sprite);
    daemon_sprite_set_x(sprite, from_x);
    daemon_sprite_set_y(sprite, from_y);

    canvas_redraw(canvas);
  }
}


/** マウスボタンを放した -> 駒をドロップ */
void daemon_canvas_button_release(Canvas* canvas, GdkEventButton* event) 
{
  g_assert(canvas != NULL);
  g_assert(event != NULL);

  if (event->type != GDK_BUTTON_RELEASE || event->button != 1) 
    return;

  if (daemon_canvas_isdrag(canvas) == FALSE)
    return;

  switch (daemon_canvas_get_mode(canvas)) {
  case D_CANVAS_MODE_EDIT:
    button_release_on_edit_mode(canvas, event);
    break;
  case D_CANVAS_MODE_GAME:
  case D_CANVAS_MODE_BOOK:
    button_release_on_game_mode(canvas, event);
    break;
  default:
    assert(0); 
  }

  daemon_canvas_set_drag(canvas, FALSE);
  g_object_unref(canvas->tmp_pixmap);
  canvas->tmp_pixmap = NULL;
}


/** 盤の大きさを変更する */
void daemon_canvas_change_size(Canvas *canvas, D_CANVAS_SIZE size) 
{
  g_assert(canvas);
  g_assert(size == D_CANVAS_SIZE_SMALL ||
	   size == D_CANVAS_SIZE_MIDDLE ||
	   size == D_CANVAS_SIZE_BIG);

  daemon_canvas_set_size(canvas, size);

  gtk_widget_set_size_request(canvas->drawingarea, 
			      canvas->width, canvas->height);

  daemon_canvas_change_size_sprite(canvas, size);

  // 再描画
  canvas_redraw(canvas);

  gtk_widget_show(canvas->drawingarea);
}


bool bookwindow_item_is_actived(Canvas* canvas) 
{
  GtkWidget* bookwindow_item = lookup_widget(canvas->window, "bookwindow");
  assert(bookwindow_item);
  return gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(bookwindow_item));
}


/** メニュー/棋譜ウィンドウのチェックをON/OFF */
void history_window_menuitem_set_active(Canvas* canvas, bool a)
{
  GtkWidget* bookwindow_item = lookup_widget(canvas->window, "bookwindow");
  assert(bookwindow_item);
  GTK_CHECK_MENU_ITEM(bookwindow_item)->active = a;
}


/** モード変更 */
void daemon_canvas_change_mode(Canvas *canvas, D_CANVAS_MODE mode) 
{
  D_CANVAS_MODE before_mode;

  before_mode = daemon_canvas_get_mode(canvas);

  if (mode == D_CANVAS_MODE_EDIT) {
    daemon_dboard_set_for_edit(&(canvas->board));
    if (bookwindow_item_is_actived(canvas)) {
      canvas->history->hide_window();
      history_window_menuitem_set_active(canvas, false);
    }

    // 棋譜をクリア
    if (canvas->record)
      daemon_record_free(canvas->record);
    canvas->record = daemon_record_new();
    canvas->history->clear();
    alt_moves_dialog_hide();
  }

  daemon_canvas_set_mode(canvas, mode);

  /* 背景の画像を編集モード用にする */
  daemon_canvas_create_back_pixmap(canvas);

  // 再描画
  canvas_redraw(canvas);

  /* ボタン、メニューの使用可／使用不可を設定する */
  daemon_canvas_set_sensitive(canvas);
  
  if (mode == D_CANVAS_MODE_BOOK) {
    // daemon_canvas_set_kif_sensitive(canvas);
    if (!bookwindow_item_is_actived(canvas))
      canvas->history->show_window();
  }

  daemon_canvas_update_statusbar(canvas);
}


/** 画面の大きさ変更に合わせてスプライトの大きさを変更する */
void daemon_canvas_change_size_sprite(Canvas *canvas, D_CANVAS_SIZE size) {
  Sprite *sprite;
  GdkPixmap *koma;
  GdkBitmap *mask;
  gint width, height;
  gint i;

  koma   = NULL;
  mask   = NULL;
  width  = 0;
  height = 0;

  if (size == D_CANVAS_SIZE_SMALL) {
    koma = canvas->koma[0];
    mask = canvas->mask[0];
    width = 40;
    height = 40;
  } else if (size == D_CANVAS_SIZE_MIDDLE) {
    koma = canvas->koma[1];
    mask = canvas->mask[1];
    width = 50;
    height = 50;
  } else if (size == D_CANVAS_SIZE_BIG) {
    koma = canvas->koma[2];
    mask = canvas->mask[2];
    width = 64;
    height = 64;
  } else {
    /* 仕様上ありえない分岐 */
    g_assert(1);
  }

  for (i=0; i<SPRITEMAX; i++) {
    sprite = daemon_canvas_get_sprite(canvas, i);

    if (daemon_sprite_get_type(sprite) == ETC_SPRITE) {
      continue;
    }

    daemon_sprite_set_pixmap( sprite, koma, mask, 0, 0, width, height );
  }
}


static void daemon_canvas_draw_time_up(Canvas* canvas, int teban)
{
  if (daemon_canvas_get_mode(canvas) != D_CANVAS_MODE_GAME)
    return;

  int x, y, width, height, step;
  char buf[100];
  PangoLayout* layout;
 
  if ((teban == 0 && daemon_canvas_get_front(canvas) == D_SENTE) ||
      (teban == 1 && daemon_canvas_get_front(canvas) == D_GOTE)) {
    /* 画面右手 */
    x      = daemon_canvas_rate_xy(canvas, 516 - 1);
    y      = daemon_canvas_rate_xy(canvas, 158 + 10 + 4);
    width  = daemon_canvas_rate_xy(canvas, 129);
    height = daemon_canvas_rate_xy(canvas,  38 - 20);
    step   = daemon_canvas_rate_xy(canvas,  15);
  }
  else {
    /* 画面左手 */
    x      = daemon_canvas_rate_xy(canvas,   6 - 1);
    y      = daemon_canvas_rate_xy(canvas, 158 + 10 + 4);
    width  = daemon_canvas_rate_xy(canvas, 129);
    height = daemon_canvas_rate_xy(canvas,  38 - 20);
    step   = daemon_canvas_rate_xy(canvas,  15);
  }

  GdkPixmap* pixmap = daemon_canvas_get_pixmap(canvas);
  GdkGC* gc = canvas->gc;
  GdkPixmap* back = daemon_canvas_get_back(canvas);

  set_color(gc, 0xFFFF, 0xFFFF, 0xFFFF);

  GdkRectangle rect;
  rect.x = x; rect.y = y; rect.width = width; rect.height = height;

  // gdk_gc_set_clip_rectangle(gc, &rect);
  // gdk_gc_set_clip_origin(gc, 0, 0);
  gdk_draw_drawable(pixmap, gc, back, x, y, x, y, width, height);
  // gdk_draw_rectangle(pixmap, gc, FALSE, x, y, width, height); // DEBUG

  time2string(buf, g_board.total_time[teban] + 
	      (teban == canvas->board.next ? canvas->elapsed : 0) );
  buf[8] = '/';
  time2string(buf + 9, g_board.limit_time[teban]);

  layout = gtk_widget_create_pango_layout(canvas->window, buf);
  gdk_draw_layout(pixmap, gc, x, y, layout);
  g_object_unref(layout);

  daemon_canvas_invalidate_rect(canvas, &rect);
}


static gboolean on_timeout( gpointer data )
{
  g_canvas->elapsed++;
  daemon_canvas_draw_time_up( g_canvas, g_canvas->board.next );
  return TRUE;
}


/** 棋譜ファイルを読み込む */
void daemon_kiffile_load_ok_button_pressed(const char* filename)
{
  // const gchar* buf;
  Record *record;
  D_LOADSTAT stat;

  record = daemon_record_new();
  if (!record) {
    printf("new record error.\n"); 
    abort();
  }

  daemon_record_load(record, filename);
  stat = daemon_record_get_loadstat(record);
  if (stat != D_LOAD_SUCCESS) {
    // ファイル読み込み失敗
#ifndef NDEBUG
    KyokumenNode* node = record->mi;
    while (node) {
      if (node->moves.size() > 0) {
	KyokumenNode::Moves::const_iterator it;
	for (it = node->moves.begin(); it != node->moves.end(); it++)
	  daemon_dte_output(&it->te, stdout);
	printf("\n");
	node = node->moves.begin()->node;
      }
      else
	break;
    }
#endif // NDEBUG
    daemon_messagebox(g_canvas, _("A file did not open."), GTK_MESSAGE_ERROR);
    daemon_record_free(record);
    return;
  }

  // ファイル読み込み成功
  printf("%s: load success\n", __func__); // DEBUG

  daemon_record_free(g_canvas->record);
  g_canvas->record = record;

  daemon_dboard_copy(&(record->first_board), &(g_canvas->board));

  // 注目する手筋を更新
  g_canvas->history->sync( *record, 0, record->mi->moves.begin()->te );
  alt_moves_dialog_update();

  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_BOOK);
}


/** 棋譜ファイルを保存する */
void daemon_kiffile_save_ok_button_pressed(const char* filename, 
                                           enum KifuFileType filetype)
{
  int ret;

  if (g_canvas->record == NULL) {
    g_canvas->record = daemon_record_new();
    daemon_dboard_copy(&(g_canvas->board), &(g_canvas->record->first_board));
  }

  ret = -1;
  switch (filetype)
  {
  case CSA_FORMAT:
    ret = daemon_record_output_csa(g_canvas->record, filename); 
    break;
  case KIF_FORMAT:
    ret = daemon_record_output_kif(g_canvas->record, filename); 
    break;
  default:
    assert(0);
  }

  if (ret != 0) {
    /* ファイル書き込み失敗 */
    daemon_messagebox(g_canvas, _("The writing of a file went wrong."),
		      GTK_MESSAGE_ERROR);
    return;
  }
}


/**
 * マウスカーソルがcanvasから出たときに呼ばれる。
 * ドラッグ中ならばスプライトを戻す。
 */
void daemon_canvas_leave_notify(Canvas *canvas,
				GtkWidget *widget,
				GdkEventCrossing *event,
				gpointer user_data) {
  gint no;
  Sprite *sprite;
  gint x, y;
  GdkPoint diff;
  GdkRectangle rect;

  if (daemon_canvas_isdrag(canvas) == FALSE) {
    return;
  }

  daemon_canvas_set_drag(canvas, FALSE);
  no     = daemon_canvas_get_dragno(canvas);
  sprite = daemon_canvas_get_sprite(canvas, no);
  diff   = daemon_canvas_get_diff(canvas);
  x      = daemon_sprite_get_from_x(sprite);
  y      = daemon_sprite_get_from_y(sprite);

  rect.x      = daemon_sprite_get_x(sprite);
  rect.y      = daemon_sprite_get_y(sprite);
  rect.width  = daemon_sprite_get_width(sprite);
  rect.height = daemon_sprite_get_height(sprite);

  daemon_sprite_set_x(sprite, x);
  daemon_sprite_set_y(sprite, y);

  /* 再描画 */
  daemon_canvas_draw_back_update_from_tmp_pixmap(canvas, &rect);
  daemon_canvas_draw_sprite(canvas, no);
  daemon_canvas_invalidate(canvas);
}


/** 主ウィンドウをすべて再描画 */
void canvas_redraw(Canvas* canvas)
{
  /* スプライト再配置 */
  daemon_canvas_dispose_sprite(canvas);

  /* 画面再描画 */
  daemon_canvas_draw_back(canvas);
  daemon_canvas_draw_sprite_all(canvas);
  daemon_canvas_draw_time(canvas);
  daemon_canvas_invalidate(canvas);
}


/**
 * メニュー項目のうち進む/戻る.
 * 棋譜モードで、進む/戻るボタンを押したときに呼び出される
 */
void daemon_canvas_set_kif_sensitive(Canvas* canvas) 
{
  GtkWidget *widget;
  int i;
  bool back_enable, forward_enable;
  
  g_assert(canvas);

  if ( canvas->mode == D_CANVAS_MODE_BOOK ) {
    back_enable = canvas->history->cur_pos != 0;
    if (canvas->history->focus_line.size() > 0 && 
	canvas->history->focus_line.back().first.special != DTe::NORMAL_MOVE) {
      forward_enable = canvas->history->cur_pos < 
	               canvas->history->focus_line.size() - 1;
    }
    else {
    forward_enable = canvas->history->cur_pos < 
                     canvas->history->focus_line.size();
    }
  }
  else {
    back_enable = false;
    forward_enable = false;
  }

  static const char* back_menuitems[] = {
    "first", "back", "button_first", "button_back", NULL};
  static const char* forward_menuitems[] = {
    "next", "last", "button_next", "button_last", NULL};
  
  for (i = 0; back_menuitems[i]; i++) {
    widget = lookup_widget(canvas->window, back_menuitems[i]);
    gtk_widget_set_sensitive(widget, back_enable );
  }

  for (i = 0; forward_menuitems[i]; i++) {
    widget = lookup_widget(canvas->window, forward_menuitems[i]);
    gtk_widget_set_sensitive(widget, forward_enable );
  }
}


/**
 * ゲームモードでのメニュー項目の有効・無効
 */
void daemon_canvas_set_game_sensitive(Canvas* canvas)
{
  GtkWidget* widget;

  // 投了できるか
  bool can_resign = canvas->mode == D_CANVAS_MODE_GAME &&
                    canvas->playertype[canvas->board.next] == SI_GUI_HUMAN;
  
  // 待ったできるか
  bool can_retract = can_resign && canvas->history->focus_line.size() != 0;

  widget = lookup_widget(canvas->window, "button_stop");
  gtk_widget_set_sensitive(widget, canvas->mode == D_CANVAS_MODE_GAME);
  widget = lookup_widget(canvas->window, "stop");
  gtk_widget_set_sensitive(widget, canvas->mode == D_CANVAS_MODE_GAME);

  widget = lookup_widget(canvas->window, "button_give_up");
  gtk_widget_set_sensitive(widget, can_resign);
  widget = lookup_widget(canvas->window, "give_up");
  gtk_widget_set_sensitive(widget, can_resign);

  // 待った
  widget = lookup_widget(canvas->window, "retract");
  gtk_widget_set_sensitive(widget, can_retract);
}


/** メニュー項目の有効・無効. */
void daemon_canvas_set_sensitive(Canvas *canvas)
{
  GtkWidget *widget;
  int i;

  // ゲーム中、詰め探索中は無効
  static const char* disable_on_game[] = {
    "fileopen", "filesave", "button_new_game", "playgame", "check_mate",
    "connect_server", NULL
  };

  for (i = 0; disable_on_game[i]; i++) {
    widget = lookup_widget( canvas->window, disable_on_game[i] );
    gtk_widget_set_sensitive( widget,
                              daemon_canvas_get_mode(canvas) != D_CANVAS_MODE_GAME );
  }

  // 編集開始は棋譜モードからのみ
  widget = lookup_widget(canvas->window, "startedit");
  gtk_widget_set_sensitive( widget,
                            canvas->mode != D_CANVAS_MODE_GAME &&
                            canvas->mode != D_CANVAS_MODE_EDIT );

  // 編集モードでのみ有効
  static const char* enable_only_edit[] = {
    "set_hirate", "set_mate", "all_koma_to_pbox", "rl_reversal", "order_reversal",
    "edit_flip", NULL
  };

  for (i = 0; enable_only_edit[i]; i++) {
    widget = lookup_widget( canvas->window, enable_only_edit[i] );
    gtk_widget_set_sensitive( widget, canvas->mode == D_CANVAS_MODE_EDIT );
  }

  // 進む/戻る
  daemon_canvas_set_kif_sensitive( canvas );

  // 投了など
  daemon_canvas_set_game_sensitive( canvas );
}


int daemon_canvas_mate_start_pressed(int is_sente)
{
  Canvas* canvas;

  canvas = g_canvas;

  daemon_dboard_output(&canvas->board, stdout); // DEBUG
  daemon_dboard_copy_board(&(canvas->board), &g_board);
  printBOARD(&g_board); // DEBUG

  boSetTsumeShogiPiece(&g_board);
  boPlayInit(&g_board);

  printBOARD(&g_board); // DEBUG
  daemon_dboard_copy_from_board(&g_board, &(canvas->board));
  daemon_dboard_output(&canvas->board, stdout); // DEBUG

  // 再描画
  canvas_redraw(canvas);

  if (is_sente) {
    g_board.next = SENTE;
    canvas->board.next = D_SENTE;
  } 
  else {
    g_board.next = GOTE;
    canvas->board.next = D_GOTE;
  }

  if (boCheckJustMate(&g_board) == 0) {
    daemon_messagebox(canvas, _("No good board."), GTK_MESSAGE_ERROR);
    return 0;
  }

  return 1;
}


/** 詰め将棋のmain loop */
void daemon_canvas_mate_loop()
{
  Canvas* canvas;
  GtkWidget* dialog;
  TREE root;
  int ret;
  MOVEINFO* mi = NULL;
  int i;

  canvas = g_canvas;

  dialog = gtk_message_dialog_new(GTK_WINDOW(canvas->window),
				  GTK_DIALOG_DESTROY_WITH_PARENT,
				  GTK_MESSAGE_INFO,
				  GTK_BUTTONS_CANCEL,
				  _("Now thinking..."));
  g_signal_connect_swapped(dialog, "response", 
			   G_CALLBACK(daemon_canvas_mate_think_cancel), NULL);
  gtk_widget_show(dialog);

  gmSetGameStatus(&g_game, SI_NORMAL);

#ifdef USE_HASH  
  /* ハッシュテーブル初期化 */
  newHASH();
#endif /* USE_HASH */

  initTREE(&root);

  ret = 0;

  for (i = 0; i < 8; i++ ) {
    MATE_DEEP_MAX = i;
    if ( mate(&g_board, &root, 0) ) {
      ret = 1;
      /* 詰んだ */
      mi = newMOVEINFO();
      trSetMoveinfo(root.child, mi);
      break;
    }
  }

#ifdef USE_HASH  
  /* ハッシュテーブル解放 */
  freeHASH();
#endif /* USE_HASH */

  gtk_widget_destroy(dialog);

  if (ret) {
    DTe te;

    daemon_messagebox(canvas, _("Check mate."), GTK_MESSAGE_INFO);

    if (canvas->record != NULL)
      daemon_record_free(canvas->record);
    canvas->record = daemon_record_new();
    canvas->history->clear();

    daemon_dboard_copy(&(canvas->board), &(canvas->record->first_board));
    
    DBoard* tmp_board = daemon_dboard_new();
    daemon_dboard_copy(&canvas->record->first_board, tmp_board);
    for (i = 0; i < mi->count; i++) {
      te.special = DTe::NORMAL_MOVE;
      te.fm   = mi->te[i].fm;
      te.to   = mi->te[i].to;
      te.nari = mi->te[i].nari;
      te.uti  = mi->te[i].uti;
      daemon_dmoveinfo_add(canvas->record->mi, &te, 0);
      canvas->history->append_move(*tmp_board, te, 0);
      daemon_dboard_move(tmp_board, &te);
    }
    freeMOVEINFO(mi);
    daemon_dboard_free(tmp_board);
    // canvas->count = 0;
    // canvas->board.count = 0;
    daemon_canvas_change_mode(canvas, D_CANVAS_MODE_BOOK);
  } 
  else {
    daemon_messagebox(canvas, _("No check mate."), GTK_MESSAGE_INFO);
  }
}


/** 詰め将棋でのキャンセルボタン */
void daemon_canvas_mate_think_cancel()
{
  gmSetGameStatus(&g_game, SI_CHUDAN);
}


PLAYERTYPE get_player_type(int combo_index)
{
  PLAYERTYPE playertype;

  switch (combo_index) {
  case 0:
    playertype = SI_GUI_HUMAN;
    break;
  case 1:
    playertype = SI_COMPUTER_2;
    break;
  case 2:
    playertype = SI_COMPUTER_3;
    break;
  case 3:
    playertype = SI_COMPUTER_4;
    break;
  case 4:
    playertype = SI_CHILD_PROCESS;
    break;
  default:
    g_error("internal error: unknown player type = '%d'", combo_index);
    assert(0);
  }

  return playertype;
}


/** bonanzaを起動する
    @return 成功した時は非0, 失敗した時は0 */
int spawn_async_with_pipe(ChildProcess* x)
{
  // TODO:設定で変えれるように。
  static const char* exefile = "/opt/src/shogi/bonanza-4.0.4/src/bonanza/bonanza"; 
  static const char* dir = "/opt/src/shogi/bonanza-4.0.4/winbin";
  
  int pipe_fd1[2]; // 子 -> 親
  int pipe_fd2[2]; // 親 -> 子

  if (pipe(pipe_fd1) < 0 || pipe(pipe_fd2) < 0) {
    perror("pipe()");
    return 0;
  }

  pid_t pid = fork();
  if (pid < 0) {
    perror("fork()");
    return 0;
  }

  if (pid == 0) {
    // 子プロセス
    dup2(pipe_fd2[0], 0); // 標準入力 (0)
    close(pipe_fd2[1]);   // write側は閉じる
    dup2(pipe_fd1[1], 1); // 標準出力 (1)
    close(pipe_fd1[0]);   // read側は閉じる

    chdir(dir);
    execl(exefile, "bonanza", NULL);

    // ここに来るときはエラー
    perror("exec");
    exit(1); // プロセスを終了
  }
  else {
    // 親プロセス
    close(pipe_fd1[1]);
    close(pipe_fd2[0]);

    x->pid = pid;
    x->write_fd = pipe_fd2[1];
    x->read_fd = pipe_fd1[0];
  }

  return 1;
}


/** メニュー -> ゲーム -> OK */
int daemon_canvas_new_game_ok(GtkWidget* dialog_game) 
{
  PLAYERTYPE playertype[2];
  TEAI teai;
  // const gchar* player[2];
  // const gchar* t;
  const gchar* playername[2];
  const gchar* tmp;
  int i;

  GtkWidget *menu_sente;
  GtkWidget *menu_gote;
  GtkWidget *menu_teai;
  GtkWidget* entry_playername;

  menu_sente = lookup_widget(dialog_game, "optionmenu_sente");
  menu_gote  = lookup_widget(dialog_game, "optionmenu_gote");
  menu_teai  = lookup_widget(dialog_game, "optionmenu_teai");

  playertype[0] = get_player_type(
                     gtk_combo_box_get_active(GTK_COMBO_BOX(menu_sente)));
  playertype[1] = get_player_type(
                     gtk_combo_box_get_active(GTK_COMBO_BOX(menu_gote)));

  int t = gtk_combo_box_get_active(GTK_COMBO_BOX(menu_teai));
  switch (t) {
  case 0:
    teai = HIRATE;
    daemon_dboard_set_hirate(&(g_canvas->board));
    break;
  case 1:
    teai = TEAI_ETC;
    daemon_dboard_copy_board(&(g_canvas->board), &g_board);
    if (boCheckJustGame(&g_board) == 0) {
      daemon_messagebox(g_canvas, _("No good board."), GTK_MESSAGE_ERROR);
      return 0;
    }
    break;
  default:
    g_error("unknown teai = '%d'", t);
    assert(0);
  }

  for (i = 0; i < 2; i++) {
    entry_playername = lookup_widget(dialog_game, 
				     i == 0 ? "entry_sente" : "entry_gote");
    tmp = gtk_entry_get_text(GTK_ENTRY(entry_playername));
    if (tmp[0] == '\0') {
      if (playertype[i] == SI_GUI_HUMAN) {
	uid_t uid;
	struct passwd *curr_passwd;
	uid = getuid();
	curr_passwd = getpwuid(uid);
	playername[i] = curr_passwd->pw_name;
      }
      else 
	playername[i] = "COM";
    } 
    else 
      playername[i] = tmp;
  }

  // bonanzaの起動
  for (i = 0; i < 2; i++) {
    g_canvas->child_process[i].clear();
    if (playertype[i] == SI_CHILD_PROCESS) {
      if (!spawn_async_with_pipe(&g_canvas->child_process[i])) {
	daemon_messagebox(g_canvas, _("failed: run bonanza"), 
			  GTK_MESSAGE_ERROR);
	if (i == 1)
	  g_canvas->child_process[0].kill();
	return 0;
      }
    }
  }

  daemon_game_setup(playertype[0], playername[0],
		    playertype[1], playername[1]);

  return 1;
}


/** recordのクリアなどの準備 */
void daemon_game_setup(PLAYERTYPE playertype1, const char* playername1,
		       PLAYERTYPE playertype2, const char* playername2)
{
  if (g_canvas->record)
    daemon_record_free(g_canvas->record);
  g_canvas->record = daemon_record_new();

  daemon_record_set_player(g_canvas->record, D_SENTE, playername1);
  daemon_record_set_player(g_canvas->record, D_GOTE, playername2);

  daemon_dboard_copy(&g_canvas->board, &g_canvas->record->first_board);

  g_canvas->playertype[0] = playertype1;
  g_canvas->playertype[1] = playertype2;

  g_canvas->history->clear();
  alt_moves_dialog_hide();

  // g_canvas->record->matchstat = D_UNKNOWN;
  daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_GAME);
}



/*************************************************************/
/* draw */
/*************************************************************/

void daemon_canvas_draw_back(Canvas *canvas) {
  GdkRectangle rect;
  
  rect.x = 0;
  rect.y = 0;
  rect.width  = canvas->width;
  rect.height = canvas->height;

  daemon_canvas_draw_back_update(canvas, &rect);
}

void daemon_canvas_draw_back_update(Canvas *canvas, GdkRectangle* rect) {
  GdkGC* gc;
  GdkPixmap* pixmap;
  GdkPixmap* back;
  
  gc     = canvas->gc;
  pixmap = daemon_canvas_get_pixmap(canvas);
  back   = daemon_canvas_get_back(canvas);
  
  set_color(gc, 0, 0, 0);
  gdk_gc_set_clip_rectangle(gc, rect);
  gdk_draw_drawable(pixmap, gc, back, rect->x, rect->y, rect->x, rect->y,
		  rect->width, rect->height);
}

void daemon_canvas_draw_back_update_from_tmp_pixmap(Canvas *canvas, GdkRectangle *rect) {
  GdkGC* gc;
  GdkPixmap* pixmap;
  GdkPixmap* tmp_pixmap;
  GdkPixmap* back;
  
  gc         = canvas->gc;
  pixmap     = daemon_canvas_get_pixmap(canvas);
  tmp_pixmap = canvas->tmp_pixmap;
  back       = daemon_canvas_get_back(canvas);
  
  gdk_gc_set_clip_rectangle(gc, rect);
  gdk_draw_drawable(pixmap, gc, tmp_pixmap, rect->x, rect->y, rect->x, rect->y,
		  rect->width, rect->height);
}


static void daemon_canvas_draw_sprite_all(Canvas *canvas)
{
  Sprite* sprite;
  gint i;

  g_assert(canvas != NULL);

  for (i=0; i<SPRITEMAX; i++) {
    sprite = daemon_canvas_get_sprite(canvas, i);

    if (sprite == NULL) {
      continue;
    }
    if (daemon_sprite_isvisible(sprite) == FALSE) {
      continue;
    }

    daemon_canvas_draw_sprite(canvas, i);
  }

  // 最後の手をハイライト
  // TODO: 場所はここでいい？
  if ( canvas->history && canvas->history->focus_line.size() > 0 &&
       canvas->history->cur_pos > 0 ) {
    DTe te = canvas->history->focus_line[canvas->history->cur_pos - 1].first;
    int x = te.to & 0xf;
    int y = (te.to >> 4);

    GdkRectangle rect;
    if (daemon_canvas_get_front(canvas) == D_SENTE) {
      rect.x = daemon_canvas_rate_xy(canvas, 
				     140 + (9 - x) * D_SPRITE_KOMA_WIDTH);
      rect.y = daemon_canvas_rate_xy(canvas, 
				     10 + (y - 1) * D_SPRITE_KOMA_HEIGHT);
    }
    else {
      rect.x = daemon_canvas_rate_xy(canvas, 
				     140 + (x - 1) * D_SPRITE_KOMA_WIDTH);
      rect.y = daemon_canvas_rate_xy(canvas, 
				     10 + (10 - y - 1) * D_SPRITE_KOMA_HEIGHT);
    }
    rect.width = daemon_canvas_rate_xy(canvas, D_SPRITE_KOMA_WIDTH);
    rect.height = daemon_canvas_rate_xy(canvas, D_SPRITE_KOMA_HEIGHT);

    gdk_draw_rectangle( daemon_canvas_get_pixmap(g_canvas),
                        g_canvas->drawingarea->style->white_gc,
                        FALSE,
                        rect.x, rect.y, rect.width, rect.height );

    daemon_canvas_invalidate_rect(g_canvas, &rect);
  }
}


static void daemon_canvas_draw_sprite_update_all(Canvas *canvas, GdkRectangle* rect)
{
  GdkRectangle s_rect;
  Sprite* sprite;
  gint    i;

  g_assert(canvas != NULL);

  for (i=0; i<SPRITEMAX; i++) {
    sprite = daemon_canvas_get_sprite(canvas, i);

    if (sprite == NULL) {
      continue;
    }
    if (daemon_sprite_isvisible(sprite) == FALSE) {
      continue;
    }
    s_rect.x      = daemon_sprite_get_x(sprite);
    s_rect.y      = daemon_sprite_get_y(sprite);
    s_rect.width  = daemon_sprite_get_width(sprite);
    s_rect.height = daemon_sprite_get_height(sprite);
    if (is_on_rectangle(rect, &s_rect)) {
      daemon_canvas_draw_sprite(canvas, i);
    }
  } 
}


static void daemon_canvas_draw_sprite(Canvas *canvas, gint no)
{
  // GdkGC*     gc;
  // GdkPixmap* pixmap;
  // GdkPixmap* sprite_pixmap;
  // GdkBitmap* mask;
  Sprite*    sprite;
  // gint       width, height;
  // gint       x, y;
  // gint       src_x, src_y;

  g_assert(canvas != NULL);
  g_assert(0 <= no && no < SPRITEMAX);

  sprite = daemon_canvas_get_sprite(canvas, no);
  assert(sprite);

  daemon_sprite_draw(sprite, daemon_canvas_get_pixmap(canvas), canvas->gc);
}


/** 消費時間を描く */
void daemon_canvas_draw_time(Canvas* canvas) 
{
  GdkGC*     gc;
  GdkPixmap* pixmap;
  GdkPixmap* back;
  // GdkFont *font;
  GdkRectangle rect;
  gint x, y, width, height, step;
  gchar buf[BUFSIZ];
  PangoLayout* layout;
  int teban;

  g_assert(canvas != NULL); 
  
  gc          = canvas->gc;
  pixmap      = daemon_canvas_get_pixmap(canvas);
  back        = daemon_canvas_get_back(canvas);
  rect.x      = 0;
  rect.y      = 0;
  rect.width  = canvas->width;
  rect.height = canvas->height;
  x = y = width = height = step = 0;

  for (teban = 0; teban < 2; teban++) {
    if ((teban == 0 && daemon_canvas_get_front(canvas) == D_SENTE) ||
	(teban == 1 && daemon_canvas_get_front(canvas) == D_GOTE)) {
      /* 画面右手 */
      x      = daemon_canvas_rate_xy(canvas, 516);
      y      = daemon_canvas_rate_xy(canvas, 158);
      width  = daemon_canvas_rate_xy(canvas, 129);
      height = daemon_canvas_rate_xy(canvas,  38);
      step   = daemon_canvas_rate_xy(canvas,  15);
    }
    else {
      /* 画面左手 */
      x      = daemon_canvas_rate_xy(canvas,   6);
      y      = daemon_canvas_rate_xy(canvas, 158);
      width  = daemon_canvas_rate_xy(canvas, 129);
      height = daemon_canvas_rate_xy(canvas,  38);
      step   = daemon_canvas_rate_xy(canvas,  15);
    }

    // font = gtk_style_get_font(canvas->window->style);
    set_color(gc, 0xFFFF, 0xFFFF, 0XFFFF);

    gdk_gc_set_clip_rectangle(gc, &rect);
    gdk_gc_set_clip_origin(gc, 0, 0);
    gdk_draw_drawable(pixmap, gc, back, x, y, x, y, width, height);

    daemon_canvas_draw_time_up(canvas, teban);

    if (canvas->record != NULL) {
      sprintf(buf, "%-20s", canvas->record->player[teban]);
      buf[16] = '\0';
      // gdk_draw_string(pixmap, font, gc, x, y + step * 2 + 3, buf);
      layout = gtk_widget_create_pango_layout(canvas->window, buf);
      gdk_draw_layout(pixmap, gc, x, y + step * 2 + 3, layout);
      g_object_unref(layout);
    }
  }

  /* 手番、前の手を表示 */
  x      = daemon_canvas_rate_xy(canvas, 516);
  y      = daemon_canvas_rate_xy(canvas, 131 - 14);
  width  = daemon_canvas_rate_xy(canvas, 129);
  height = daemon_canvas_rate_xy(canvas,  38);
  step   = daemon_canvas_rate_xy(canvas,  16);

  // font = gtk_style_get_font(canvas->window->style);
  set_color(gc, 0xFFFF, 0xFFFF, 0XFFFF);
  
  gdk_gc_set_clip_rectangle(gc, &rect);
  gdk_gc_set_clip_origin(gc, 0, 0);
  gdk_draw_drawable(pixmap, gc, back, x, y, x, y, width, height);

  // 手番を表示
  gint count;
  sprintf(buf, "%s", canvas->board.next == D_SENTE ? 
          _("next SENTE (BLACK)") : _("next GOTE (WHITE)"));
  y += step;
  // gdk_draw_string(pixmap, font, gc, x, y, buf);
  layout = gtk_widget_create_pango_layout(canvas->window, buf);
  gdk_draw_layout(pixmap, gc, x, y, layout);
  g_object_unref(layout);

  if (daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_GAME) {
    y += step;
    count = canvas->history->focus_line.size();
    if (count > 0) {
      // 最後の手を表示
      DTe te = canvas->history->focus_line[count - 1].first;
      create_te_string2(&(canvas->board), count, &te, buf);
      layout = gtk_widget_create_pango_layout(canvas->window, buf);
      gdk_draw_layout(pixmap, gc, x, y, layout);
      g_object_unref(layout);
    }
  }

  daemon_canvas_invalidate_rect(canvas, &rect);
}


/** ステータスバーを更新 */
void daemon_canvas_update_statusbar(Canvas *canvas) 
{
  GtkStatusbar* statusbar;
  static guint context_id = -1;
  const char* buf;

  if (daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_BOOK) {
    buf = _("BOOK MODE");
  } else if (daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_GAME) {
    buf = _("GAME MODE");
  } else if (daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_EDIT) {
    buf = _("EDIT MODE");
  } 
  else {
    assert(0);
  }

  statusbar = (GtkStatusbar*) lookup_widget(GTK_WIDGET(canvas->window), 
					    "statusbar");
  if (context_id != -1) {
    gtk_statusbar_pop(statusbar, context_id);
  }
  context_id = gtk_statusbar_get_context_id(statusbar, "status");
  gtk_statusbar_push(statusbar, context_id, buf);
}


#if 0
/** ステータスバーを更新 */
void daemon_canvas_update_statusbar_te(Canvas* canvas, const DTe* te) 
{
  GtkStatusbar* statusbar;
  static guint context_id = -1;
  char buf[BUFSIZ];
  char buf2[BUFSIZ];
  gint count;

  if (daemon_canvas_get_mode(canvas) != D_CANVAS_MODE_GAME)
    return;

  count = canvas->history->focus_line.size();
  create_te_string(&canvas->board, count, te, buf);

  sprintf(buf2,
	  "%s %s --- %s", _("GAME MODE"),
	  buf, 
          canvas->board.next == D_GOTE ? _("SENTE (BLACK)") : _("GOTE (WHITE)"));

  statusbar = (GtkStatusbar*) lookup_widget(GTK_WIDGET(canvas->window), 
					    "statusbar");
  if (context_id != -1) {
    gtk_statusbar_pop(statusbar, context_id);
  }
  context_id = gtk_statusbar_get_context_id(statusbar, "status");
  gtk_statusbar_push(statusbar, context_id, buf2);
}
#endif // 0


/*************************************************************/
/* etc */
/*************************************************************/

/**
 * canvas->mode の値を見て back の内容を書き換える。
 *
 */
void daemon_canvas_create_back_pixmap(Canvas *canvas) {
  GdkRectangle rect;

  /* 駒台の座標 */
  static const gint x1 = 1, y1 = 210, x2 = 129, y2 = 378;

  g_assert(canvas != NULL);

  if (canvas->back[0] != NULL)
    g_object_unref(canvas->back[0]);
  if (canvas->back[1] != NULL)
    g_object_unref(canvas->back[1]);
  if (canvas->back[2] != NULL)
    g_object_unref(canvas->back[2]);

  canvas->back[0] = load_pixmap_from_imlib_image_scaled(canvas->window,
							canvas->back_src,
							D_CANCAS_SIZE_SMALL_X,
							D_CANCAS_SIZE_SMALL_Y);
  canvas->back[1] = load_pixmap_from_imlib_image_scaled(canvas->window,
							canvas->back_src,
							D_CANCAS_SIZE_MIDDLE_X,
							D_CANCAS_SIZE_MIDDLE_Y);
  canvas->back[2] = load_pixmap_from_imlib_image_scaled(canvas->window,
							canvas->back_src,
							D_CANCAS_SIZE_BIG_X,
							D_CANCAS_SIZE_BIG_Y);

  if (canvas->mode == D_CANVAS_MODE_EDIT) {
    return;
  }

  rect.x      = 0;
  rect.y      = 0;
  rect.width  = canvas->width;
  rect.height = canvas->height;

  gdk_gc_set_clip_rectangle(canvas->gc, &rect);
  gdk_gc_set_clip_origin(canvas->gc, 0, 0);

  /* gc に黒をセット */
  set_color(canvas->gc, 0, 0, 0);

  gdk_draw_rectangle(canvas->back[0], canvas->gc, TRUE, x1, y1, x2, y2);
  gdk_draw_rectangle(canvas->back[1],canvas->gc, TRUE,
		     x1 * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X,
		     y1 * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X,
		     x2 * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X + 1,
		     y2 * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X);
  gdk_draw_rectangle(canvas->back[2], canvas->gc, TRUE,
		     x1 * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X,
		     y1 * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X,
		     x2 * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X + 1,
		     y2 * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X);
}


/**
 * マウスカーソルがスプライトの上にあるか
 *
 * @param canvas 対象のDragTestCanvas
 * @param event 対象のGdkEventButton
 *
 * @return マウスカーソルが乗っているスプライトの番号。なければ-1
 */
gint daemon_canvas_on_sprite(Canvas* canvas, GdkEventButton  *event) {
  Sprite* sprite;
  gint x, y, width, height;
  gint i;

  for (i=0; i<SPRITEMAX; i++) {
    sprite = daemon_canvas_get_sprite(canvas, i);

    if (sprite == NULL) {
      continue;
    }

    if (daemon_sprite_ismove(sprite) == FALSE) {
      continue;
    }

    if (daemon_sprite_isvisible(sprite) == FALSE) {
      continue;
    }

    x      = daemon_sprite_get_x(sprite);
    y      = daemon_sprite_get_y(sprite);
    width  = daemon_sprite_get_width(sprite);
    height = daemon_sprite_get_height(sprite);

    if (x <= event->x && event->x <= x + width &&
	y <= event->y && event->y <= y + height) {
      /* スプライト上にマウスカーソルがある */
      return i;
    }
  }

  return -1;
}

/**
 * スプライトが盤上にあれば TREU を返す。それ以外は FALSE を返す。
 */
gboolean daemon_canvas_sprite_on_board(Canvas *canvas, Sprite *sprite) {
  GdkRectangle board;
  GdkPoint point;
  
  point.x = daemon_sprite_get_x(sprite);
  point.y = daemon_sprite_get_y(sprite);
  point.x = daemon_canvas_rate_xy(canvas, point.x);
  point.y = daemon_canvas_rate_xy(canvas, point.y);

  board.x = 140;
  board.y = 11;
  board.width  = 360;
  board.height = 360;

  return daemon_on_rectangle(&board, &point);
}

/**
 * ダブルクリックチェック。
 * @param canvas 対象のDragTestCanvas
 * @return ダブルクリックならば TRUEを返す
 */
gboolean daemon_canvas_is_doubleclick(Canvas* canvas) {
  struct timeval  tv;
  struct timezone tz;

  gettimeofday(&tv, &tz);

  if (check_timeval(&(canvas->tv), &tv) == TRUE) {
    canvas->tv.tv_sec  = 0;
    canvas->tv.tv_usec = 0;
    return TRUE;
  }

  gettimeofday(&(canvas->tv), &tz);
  canvas->tv.tv_sec  = tv.tv_sec;
  canvas->tv.tv_usec = tv.tv_usec;

  return FALSE;
}

/** ドラッグ中に使う作業用の pixmap を作成する */
void daemon_canvas_create_tmp_pixmap(Canvas* canvas) {
  Sprite *sprite;
  GdkRectangle rect;
  GdkPixmap *pixmap;
  GdkGC *gc;
  gint no;

  g_assert(canvas != NULL);
  g_assert(daemon_canvas_isdrag(canvas) == 1);

  canvas->tmp_pixmap = gdk_pixmap_new(canvas->window->window, 
				      canvas->width,
				      canvas->height,
				      -1);
  if (canvas->tmp_pixmap == NULL) {
    daemon_abort("In daemon_canvas_create_tmp_pixmap(). tmp_pixmap is NULL.\n");
  }

  pixmap = daemon_canvas_get_pixmap(canvas);
  gc     = daemon_canvas_get_gc(canvas);
  
  no = daemon_canvas_get_dragno(canvas);
  sprite = daemon_canvas_get_sprite(canvas, no);
  rect.x      = daemon_sprite_get_x(sprite);
  rect.y      = daemon_sprite_get_y(sprite);
  rect.width  = daemon_sprite_get_width(sprite);
  rect.height = daemon_sprite_get_height(sprite);

  daemon_sprite_set_visible(sprite, FALSE);
  /* 背景を描く */
  daemon_canvas_draw_back_update(canvas, &rect);
  /* 駒を描く */
  daemon_canvas_draw_sprite_update_all(canvas, &rect);
  daemon_sprite_set_visible(sprite, TRUE);

  rect.x = 0;
  rect.y = 0;
  rect.width  = canvas->width;
  rect.height = canvas->height;
  gdk_gc_set_clip_rectangle(gc, &rect);
  gdk_draw_drawable(canvas->tmp_pixmap, gc, pixmap, 0, 0, 0, 0,
		  canvas->width, canvas->height);

  daemon_canvas_draw_sprite(canvas, no);
}


/** 拡大表示にあわせて、実際の座標を返す */
gint daemon_canvas_rate_xy(const Canvas* canvas, gint value) 
{
  switch (canvas->size) {
  case D_CANVAS_SIZE_SMALL:
    break; // empty
  case D_CANVAS_SIZE_MIDDLE:
    value = value * D_CANCAS_SIZE_MIDDLE_X / D_CANCAS_SIZE_SMALL_X;
    break;
  case D_CANVAS_SIZE_BIG:
    value = value * D_CANCAS_SIZE_BIG_X / D_CANCAS_SIZE_SMALL_X;
    break;
  default:
    // 仕様上ありえない分岐
    g_assert(0);
  }

  return value;
}


gint daemon_canvas_rerate_xy(Canvas* canvas, gint value) {
  if (canvas->size == D_CANVAS_SIZE_SMALL) {
  } else if (canvas->size == D_CANVAS_SIZE_MIDDLE) {
    value = value * D_CANCAS_SIZE_SMALL_X / D_CANCAS_SIZE_MIDDLE_X;
  } else if (canvas->size == D_CANVAS_SIZE_BIG) {
    value = value * D_CANCAS_SIZE_SMALL_X / D_CANCAS_SIZE_BIG_X;
  } else {
    /* 仕様上ありえない分岐 */
    g_assert(0);
  }

  return value;
}


/** 子プロセスからの入力を受け取る.
  \todo impl.
 */
INPUTSTATUS daemon_input_next_child_process_impl(BOARD* bo, TE* te)
{
  assert(0);
}


/**
 * 対局のメインルーチン.
 * この関数を呼び出す前に daemon_game_setup() で準備すること。
 */
void daemon_canvas_game(Canvas* canvas) 
{
  BOARD *board;
  GAME *game;
  int minmax_return;
  // char send_buf[BUFSIZ];
  TE te;
  // char te_buf[100];
  // INPUTSTATUS (*si_input_next[2])(BOARD *bo, TE *te);
  int elapsed_time = -1;

  g_assert(canvas != NULL);
  g_assert(daemon_canvas_get_mode(canvas) == D_CANVAS_MODE_GAME);

  board = &g_board;
  game = &g_game;

  int i;
  for (i = 0; i < 2; i++) {
    switch (canvas->playertype[i]) {
    case SI_GUI_HUMAN:
      gmSetInputFunc(game, i, canvas->playertype[i], 
		     daemon_input_next_gui_human_impl);
      break;
    case SI_COMPUTER_2:
    case SI_COMPUTER_3:
    case SI_COMPUTER_4:
      gmSetInputFunc(game, i, canvas->playertype[i], NULL);
      break;
    case SI_NETWORK_1: // CSA server
      gmSetInputFunc(game, i, canvas->playertype[i],
		     daemon_input_next_network_impl);
      break;
    case SI_CHILD_PROCESS: // bonanza
      gmSetInputFunc(game, i, canvas->playertype[i],
		     daemon_input_next_child_process_impl);
      break;
    default:
      assert(0);
    }
  }

  boInit(board);
  daemon_dboard_copy_board(&(canvas->board), board);
  boPlayInit(board);

  gmSetGameStatus(game, SI_NORMAL);

#ifdef USE_HASH  
  /* ハッシュテーブル初期化 */
  newHASH();
#endif /* USE_HASH */

  // タイムアウトをセット
  canvas->timeout_id = g_timeout_add_seconds( 1, on_timeout, NULL );

  // main loop
  while ( 1 ) {
    DTe dte;

    daemon_canvas_set_game_sensitive(canvas);
    processing_pending();

    canvas->elapsed = 0;
    elapsed_time = -1;
    THINK_DEEP_MAX = gmGetComputerLevel(game, board->next);
    /* 現在の時間を記録する */
    boSetNowTime(board);
    minmax_return = (*game->si_input_next[board->next])(board, &te);
    /* 消費時間を記録する */
    boSetUsedTime(board);

    if (canvas->playertype[board->next == 0] == SI_NETWORK_1) {
      if (minmax_return == SI_NORMAL) {
	csa_send_move(&canvas->board, board->next == 0, &te);
	csa_wait_for_result(&elapsed_time, &minmax_return);
      }
      else if (minmax_return == SI_TORYO) {
	csa_send_toryo();
	csa_wait_for_result(&elapsed_time, &minmax_return);
      }
    }

    // 指し手を記録する
    if (minmax_return == SI_TORYO) {
      dte.special = DTe::RESIGN;
      dte.fm = 0; dte.to = 0; dte.nari = 0; dte.uti = 0; dte.tori = 0;
    }
    else if (minmax_return == SI_NORMAL) {
      assert( te.to >= 0x11 && te.to <= 0x99 );

      dte.special = DTe::NORMAL_MOVE;
      dte.fm   = te.fm;
      dte.to   = te.to;
      dte.nari = te.nari;
      dte.uti  = te.uti;
      dte.tori = canvas->board.board[dte.to];

      play_move( board, &te );
      daemon_dboard_move(&(canvas->board), &dte);
    }
    else 
      break;

    if (elapsed_time < 0) // サーバを介さない場合
      elapsed_time = canvas->elapsed;
    
    daemon_dmoveinfo_add(canvas->record->mi, &dte, elapsed_time);

    // 棋譜を追加
    canvas->history->append_move(canvas->board, dte, elapsed_time);
    if (dte.special == DTe::NORMAL_MOVE)
      canvas->history->select_nth(board->mi.count);

    if (minmax_return != SI_NORMAL)
      break;

    // 再描画
    canvas_redraw(canvas);
  }

  g_source_remove(canvas->timeout_id);
  canvas->timeout_id = 0;

  /* ステータスの判定処理 */
  if (minmax_return == SI_TORYO) {
    if (board->next == SENTE) {
      daemon_messagebox(canvas, _("Gote(white) win."), GTK_MESSAGE_INFO);
    } else {
      daemon_messagebox(canvas, _("Sente(black) win."), GTK_MESSAGE_INFO);
    }
  } else if ( minmax_return == SI_WIN ) {
    if (board->next == SENTE) {
      daemon_messagebox(canvas, _("Sente(black) win."), GTK_MESSAGE_INFO);
    } else {
      daemon_messagebox(canvas, _("Gote(white) win."), GTK_MESSAGE_INFO);
    }
  } else if ( minmax_return == SI_CHUDAN ) {
    daemon_messagebox(canvas, _("Stoped."), GTK_MESSAGE_INFO);
  } else if ( minmax_return == SI_CHUDAN_RECV ) {
    daemon_messagebox(canvas, _("Stoped."), GTK_MESSAGE_INFO);
  } else if ( minmax_return == SI_NGDATA ) {
    daemon_messagebox(canvas, _("No good data."), GTK_MESSAGE_ERROR);
  } else {
      /* 仕様上ありえない分岐 */
    g_assert(0);
  }

#ifdef USE_HASH  
  /* ハッシュテーブル解放 */
  freeHASH();
#endif /* USE_HASH */

  // daemon_dboard_copy(&(canvas->record->first_board), &(canvas->board));
  /// canvas->count = 0;
  daemon_canvas_set_mode(canvas, D_CANVAS_MODE_BOOK);
  // on_go_last_activate(NULL, NULL);
  daemon_canvas_change_mode(canvas, D_CANVAS_MODE_BOOK);
}


/**
 * 人間のGUIからの一手を te に格納する。
 * @param bo 対象のBOARD
 * @param te TE
 * @return GAMESTAT
 */
INPUTSTATUS daemon_input_next_gui_human_impl(BOARD* bo, TE* te) 
{
  TE tmp_te;
  TE *gui_te;

  gui_te = &(g_canvas->gui_te);

  teSetTE(&tmp_te, 0 , 0, 0, 0);
  teSetTE(gui_te, 0 , 0, 0, 0);
  
  while ( teCmpTE(&tmp_te, gui_te) == 0 ) {
    if ( gmGetGameStatus(&g_game) != 0 ) {
      /* 中断または、投了が入った。 */
      return gmGetGameStatus(&g_game);
    }
    processing_pending();
    usleep( 50 );
  }
  
  *te = *gui_te;

  return SI_NORMAL;
}


/** 駒箱内の駒の種類の数を数える */
gint count_pbox(DBoard *board) {
  gint count;
  gint i;

  count = 0;
  for (i=1; i <= 8; i++) {
    if (0 < daemon_dboard_get_pbox(board, i)) {
      count++;
    }
  }

  return count;
}


/**
 * 棋譜ウィンドウ/ステータスバーに表示する手の文字列を生成する。
 * @param board 局面
 * @param no    プレイヤー
 * @param te    指し手. NULLでもよい
 * @param buf   出力先バッファ
 */
void create_te_string(const DBoard* board, int no, const DTe* te, char* buf) 
{
  const char* x[] = {
    "",     _("1_"), _("2_"), _("3_"), _("4_"),
    _("5_"),_("6_"), _("7_"), _("8_"), _("9_"),
  };
  const char* y[] = {
    "",     _("1 "), _("2 "), _("3 "), _("4 "),
    _("5 "),_("6 "), _("7 "), _("8 "), _("9 "),
  };
  const char* piece[] = {
    "",           _("FU"), _("KYO"),     _("KEI"),
    _("GIN"),     _("KIN"),_("KAKU"),    _("HISYA"),
    _("OH"),      _("TO"), _("NARIKYO"), _("NARIKEI"),
    _("NARIGIN"), "",      _("UMA"),     _("RYU"),
  };
  const char* nari = _(" NARI");
  const char* uti = _(" UTI");
  int p;

  if (!te) {
    sprintf(buf, "%s", _("game start") );
    return;
  }

  if (te->special != DTe::NORMAL_MOVE) {
    switch (te->special) {
    case DTe::RESIGN:
      sprintf(buf, "%s", _("RESIGN"));
      return;
    default:
      printf("internal error: special = %d\n", te->special);
      assert(0);
    }
  }

  // 通常の指し手
  if ( !te_is_put(te) ) {
    p = daemon_dboard_get_board(board, te->fm & 0xF, te->fm >> 4);
    p &= 0xF;
    sprintf(buf, "%s%s%s%s%s(%d,%d)",
	    (no % 2) ? "▲" : "▽",
	    x[te->to & 0xF],
	    y[te->to >> 4],
	    piece[p],
	    te->nari ? nari : "",
	    te->fm & 0xF,
	    te->fm >> 4
	    );
  } 
  else {
    sprintf(buf, "%s%s%s%s%s",
	    (no % 2) ? "▲" : "▽",
	    x[te->to & 0xF],
	    y[te->to >> 4],
	    piece[te->uti],
	    uti);
  }
}


void create_te_string2(const DBoard* board, int no, const DTe* te, char* buf) 
{
  const char* x[10] = {
    "",     _("1_"), _("2_"), _("3_"), _("4_"),
    _("5_"),_("6_"), _("7_"), _("8_"), _("9_"),
  };
  const char* y[10] = {
    "",     _("1 "), _("2 "), _("3 "), _("4 "),
    _("5 "),_("6 "), _("7 "), _("8 "), _("9 "),
  };
  const char* piece[16] = {
    "",           _("FU"), _("KYO"),     _("KEI"),
    _("GIN"),     _("KIN"),_("KAKU"),    _("HISYA"),
    _("OH"),      _("TO"), _("NARIKYO"), _("NARIKEI"),
    _("NARIGIN"), "",      _("UMA"),     _("RYU"),
  };
  const char* nari = _(" NARI");
  const char* uti = _(" UTI");
  const char* game_start = _("game start");
  const char* teme = _("teme");
  int p;
  DBoard *tmp_board;

  if (no == 0) {
    sprintf(buf, "%d %s %s",
	    no,
	    teme,
	    game_start);
  } 
  else if ( !te_is_put(te) ) {
    tmp_board = daemon_dboard_new();
    daemon_dboard_copy(board, tmp_board);
    daemon_dboard_back(tmp_board, te);

    p = daemon_dboard_get_board(tmp_board, te->fm & 0xF, te->fm >> 4);
    p &= 0xF;
    sprintf(buf, "%d %s %s%s%s%s(%d,%d)",
	    no,
	    teme,
	    x[te->to & 0xF],
	    y[te->to >> 4],
	    piece[p],
	    te->nari ? nari : "",
	    te->fm & 0xF,
	    te->fm >> 4
	    );
  } else {
    sprintf(buf, "%d %s %s%s%s%s",
	    no,
	    teme, 
	    x[te->to & 0xF],
	    y[te->to >> 4],
	    piece[te->uti],
	    uti
	    );
  }
}

