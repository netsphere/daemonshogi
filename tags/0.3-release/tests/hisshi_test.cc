// -*- encoding:utf-8 -*-

#include <si/si.h>
#include "tests.h"

using namespace std;
using namespace CppUnit;


/** 必至のテスト */
class HisshiTest: public TestCase
{
  CPPUNIT_TEST_SUITE( HisshiTest );
  CPPUNIT_TEST( test_1 );
  CPPUNIT_TEST_SUITE_END();

public:
  virtual void setUp() {
    newHASH();
  }

  virtual void tearDown() {
    freeHASH();
  }

  /** やさしい必至 */
  void test_1()
  {
    BOARD* bo = newBOARDwithInit();

    // http://morinobu27.blog.drecom.jp/archive/55
    // No.82
    bo_set_by_text( bo,
                    "P1 *  *  *  *  *  *  * -KE-KY\n"
                    "P2 *  *  *  *  *  * -FU+GI-OU\n"
                    "P3 *  *  *  *  *  *  * +FU * \n"
                    "P4 *  *  *  *  *  *  *  *  * \n"
                    "P5 *  *  *  *  *  *  * -KI * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00KI00KI\n"
                    "+\n" );

    boSetTsumeShogiPiece(bo);
    boSetToBOARD(bo);

    TE te_ret;
    CPPUNIT_ASSERT( think_hisshi(bo, 1, 5, &te_ret) );
    CPPUNIT_ASSERT_EQUAL( te_make_put(0x31, KIN), te_ret);
  }
};
CPPUNIT_TEST_SUITE_REGISTRATION( HisshiTest );

int main() {
  TextTestRunner runner;
  TestFactoryRegistry& registry = TestFactoryRegistry::getRegistry();
  runner.addTest( registry.makeTest() );
  return !runner.run("", false);
}
