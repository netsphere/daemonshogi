
// 詰め将棋のテスト

#include <si/si.h>
#include <si/ui.h>
#include <si/hashval.h>
#include <si/hashseed_inhand.h>
#include "tests.h"
#include <tcutil.h> // TCMAP
#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>

using namespace std;
using namespace CppUnit;


/** 詰将棋のテスト */
class MateTest: public TestCase 
{
  CPPUNIT_TEST_SUITE( MateTest );
  CPPUNIT_TEST( test_3 );
  CPPUNIT_TEST( test_4 );
  CPPUNIT_TEST( test_5 );
  CPPUNIT_TEST_SUITE_END();

public:
  virtual void setUp() 
  {
    newHASH();
  }

  virtual void tearDown() 
  {
    freeHASH();
  }

  /** 詰め将棋 */
  void test_3()
  {
    // http://www.geocities.co.jp/Playtown/6157/3-5te/sannte.html
    BOARD* bo = newBOARDwithInit();
    bo_set_by_text( bo,
                    "P1 *  *  *  *  *  *  * -OU-KY\n"
                    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  *  *  * +TO * -FU\n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00GI\n"
                    "+\n" );

    boSetTsumeShogiPiece(bo);
    boSetToBOARD(bo);

    // 手の生成
    TE line[] = {
      te_make_put(0x22, GIN),
      te_make_move(0x12, 0x21, false),
      te_make_move(0x22, 0x11, true),
    };
    int i;
    for (i = 0; i < 3; i++) 
      boMove_mate(bo, line[i]);
    MOVEINFO mi;
    uke(bo, &mi);
    CPPUNIT_ASSERT( mi.count != 0 );

    for (i = 0; i < 3; i++)
      boBack_mate(bo);

    // hashが衝突しないか
    uint64_t h[100];
    h[0]= bo->hash_all;
    printf("hash = %" PRIx64 "\n", h[0]);

    TE line2[] = {
      te_make_put(0x22, GIN),
      te_make_move(0x12, 0x21, false),
      te_make_move(0x33, 0x32, false),
    };
    for (i = 0; i < 3; i++) {
      boMove_mate( bo, line2[i] );
      h[i + 1] = bo->hash_all;
      printf("hash = %" PRIx64 "\n", h[i + 1]);
      for (int j = 0; j <= i; j++) {
        CPPUNIT_ASSERT( h[i + 1] != h[j] );
      }
    }
    CPPUNIT_ASSERT( h[1] == ~(h[0] ^ hashval[GIN][0x22] ^ hashseed_inhand[0][GIN][1]) );
    CPPUNIT_ASSERT( h[2] == ((~h[1]) ^ hashval[OH+0x10][0x12] ^ hashval[OH+0x10][0x21]) );
    CPPUNIT_ASSERT( h[3] == ~(h[2] ^ hashval[TOKIN][0x33] ^ hashval[TOKIN][0x32]) );
    for (i = 0; i < 3; i++) {
      boBack_mate(bo);
      CPPUNIT_ASSERT( bo->hash_all == h[2 - i] );
    }

    // df-pnのテスト
    TE te_ret;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, dfpn_mate(bo, 5, &te_ret) );
    TE te = te_make_put(0x23, GIN);
    // CPPUNIT_ASSERT( !teCmpTE(&te, &te_ret) );
    CPPUNIT_ASSERT_EQUAL( te, te_ret );

    // 置換表の使われ方
    CPPUNIT_ASSERT( hs_entry_count(0) != 0 );
    CPPUNIT_ASSERT_EQUAL( 0, hs_entry_count(1) );
    CPPUNIT_ASSERT_EQUAL( 0, hs_entry_count(2) );
  }

  /** 詰め将棋: 5手詰め */
  void test_4()
  {
    // http://www.geocities.co.jp/Playtown/6157/3-5te/gotedume.html
    BOARD* bo = newBOARDwithInit();
    bo_set_by_text( bo,
                    "P1 *  *  *  *  *  *  * -KE * \n"
                    "P2 *  *  *  *  *  *  *  * -OU\n"
		    "P3 *  *  *  *  *  * +UM-FU * \n"
		    "P4 *  *  *  *  *  *  *  * -FU\n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00GI00KE\n"
                    "+\n" );

    boSetTsumeShogiPiece(bo);
    boSetToBOARD(bo);

    TREE root;
    initTREE(&root);

    MATE_DEEP_MAX = 1; // 0=1手、1=3手, ...
    CPPUNIT_ASSERT_EQUAL( NOT_CHECK_MATE, mate(bo, &root, 0) );
    CPPUNIT_ASSERT( root.child == NULL );

    MATE_DEEP_MAX = 3;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, mate(bo, &root, 0) );
    CPPUNIT_ASSERT( root.child != NULL );
    TE te = te_make_put(0x42, KEI);
    CPPUNIT_ASSERT( !teCmpTE(&root.child->te, &te) );
  }

  /** 相玉、長手数 */
  void test_5()
  {
    // http://members.at.infoseek.co.jp/donotmind/dabun36.html
    BOARD* bo = newBOARDwithInit();
    bo_set_by_text( bo,
                    "P1 *  *  *  *  *  *  *  * -OU\n"
                    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  *  *  * +OU *  * \n"
		    "P4 *  *  *  *  *  * +KE+KY-FU\n"
		    "P5 *  *  *  *  *  * -RY * -UM\n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00KA00KE00KE00KE\n"
                    "+\n" );

    boSetTsumeShogiPiece(bo);
    boSetToBOARD(bo);

    TREE root;
    initTREE(&root);

    MATE_DEEP_MAX = 7; // 15手詰
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, mate(bo, &root, 0) );
    CPPUNIT_ASSERT( root.child != NULL );
    TE te = te_make_put(0x32, KEI);
    CPPUNIT_ASSERT( !teCmpTE(&root.child->te, &te) );

    printf("%s:%d: ", __func__, __LINE__);
#if 0
    BOARD* tmp_bo = boCopy(bo);
    for (TREE* p = root.child; p; p = p->child) {
      char buf[100];
      te_str(tmp_bo, &p->te, buf);
      printf("%s ", buf);
      boMove_mate(tmp_bo, p->te );
    }
    printf("\n");
#endif
    printTREE(root.child, 0);
    freeTREE(root.child);

    // http://www.ne.jp/asahi/tetsu/toybox/kato/tchu.htm
    // No.16
    bo_set_by_text( bo,
                    "P1 *  *  *  *  *  *  *  *  * \n"
                    "P2 *  *  *  *  *  * +KY+KY * \n"
		    "P3 *  *  *  *  * +GI+GI+KE+KI\n"
		    "P4 *  *  *  *  *  * +GI+KE+KI\n"
		    "P5 *  *  *  *  *  * +KI+KE * \n"
		    "P6 *  *  *  *  *  * +KY *  * \n"
		    "P7 *  *  *  *  *  *  *  * +KA\n"
		    "P8 *  *  *  *  * +KY+HI+HI+KA\n"
		    "P9 *  *  *  * +GI-OU * +KE+KI\n"
                    "+\n" );

    boSetTsumeShogiPiece(bo);
    boSetToBOARD(bo);

#if 0
    MOVEINFO mi;
    TE line[] = {
      te_make_move(0x83, 0x93, false),
      te_make_move(0x94, 0x93, false),
      te_make_move(0x82, 0x83, false),
      te_make_move(0x93, 0x83, false)
    };
    for (int i = 0; i < 4; i++) {
      if ((i % 2) == 0)
        ohte(bo, &mi);
      else
        uke(bo, &mi);
      CPPUNIT_ASSERT( mi_includes(&mi, line[i]) );
      boMove_mate(bo, line[i] );
    }
#endif

    // MATE_DEEP_MAX = 19; // 39手詰め
    TE te_ret;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, dfpn_mate(bo, 41, &te_ret) );
    te = te_make_move(0x83, 0x93, false);
    CPPUNIT_ASSERT( !teCmpTE(&te_ret, &te) );
  }

};
CPPUNIT_TEST_SUITE_REGISTRATION( MateTest );


int main()
{
  TextTestRunner runner;
  TestFactoryRegistry& registry = TestFactoryRegistry::getRegistry();
  runner.addTest( registry.makeTest() );
  return !runner.run("", false);
}
