
// 置換表のテスト

#include <si/si.h>
#include "tests.h"
#include <tcutil.h> // TCMAP

using namespace std;
using namespace CppUnit;


/** 置換表のテスト */
class TranspositionTest: public TestCase 
{
  CPPUNIT_TEST_SUITE( TranspositionTest );
  CPPUNIT_TEST( test_1 );
  CPPUNIT_TEST( test_2 );
  CPPUNIT_TEST_SUITE_END();

  struct D { int32_t x; };

  bool ary_equals(TCMAP* m , int n, const int32_t* v) 
  {
    int i;

    tcmapiterinit(m);
    for (i = 0; i < n; i++) {
      int siz;
      const int32_t* key = 
                    static_cast<const int32_t*>( tcmapiternext(m, &siz) );
      if ( v[i] != 
	   static_cast<const D*>(tcmapget(m, key, sizeof(int32_t), &siz))->x )
	return false;
    }
    return true;
  }

  void ary_print(TCMAP* m)
  {
    const D* r;
    const int32_t* key;
    int siz;

    tcmapiterinit(m);
    while ( (key = static_cast<const int32_t*>(tcmapiternext(m, &siz))) 
	    != NULL ) {
      r = static_cast<const D*>(tcmapget(m, key, sizeof(int32_t), &siz));
      printf("%d ", r->x);
    }
    printf("\n");
  }

public:
  virtual void setUp() 
  {
    newHASH();
  }

  virtual void tearDown() 
  {
    freeHASH();
  }

  /** TCMAPのテスト */
  void test_1()
  {
    int32_t key;

    TCMAP* m = tcmapnew();

    CPPUNIT_ASSERT_EQUAL( (size_t) 4, sizeof(D) ); // 8??
    static const D d[] = { {1}, {-1}, {5}, {3}, {0} };

    // 末尾に追加される
    int i;
    for (i = 0; i < 5; i++)
      tcmapput(m, &d[i].x, sizeof(d[i].x), &d[i], sizeof(D));

    static const int32_t dx[] = {1, -1, 5, 3, 0};
    CPPUNIT_ASSERT( ary_equals(m, 5, dx) );

    // 上書きのとき、末尾に移されるか？  -> されない
    static const D nv = {100};
    key = 5;
    // tcmapout(m, &key, sizeof(key));
    tcmapput(m, &key, sizeof(key), &nv, sizeof(D));
    printf("%s: %d\n", __func__, __LINE__);
    ary_print(m);

    static const int32_t nl[] = {1, -1, 100, 3, 0};
    CPPUNIT_ASSERT( ary_equals(m, 5, nl) );

    // 末尾に移動
    key = 3;
    tcmapmove( m, &key, sizeof(key), false );
    static const int32_t l2[] = {1, -1, 100, 0, 3};
    CPPUNIT_ASSERT( ary_equals(m, 5, l2) );

    // 先頭を削除
    tcmapcutfront(m, 3);
    CPPUNIT_ASSERT_EQUAL( (uint64_t) 2, tcmaprnum(m) );
    static const int32_t l3[] = {0, 3};
    CPPUNIT_ASSERT( ary_equals(m, 2, l3) );

    tcmapdel(m);
  }

  /** 置換表 */
  void test_2()
  {
    BOARD* bo = newBOARDwithInit();
    bo_set_by_text( bo,
                    "P1 *  *  *  * -OU *  *  *  * \n"
		    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  * +KI *  *  *  * \n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00KI\n"
                    "+\n" );

    TE te = te_make_put(0x25, KIN);
    hsPutBOARD(0, bo, 0, VAL_INF, 0, &te);

    int phi = -1;
    int delta = -1;
    TE te_ret;
    hsGetBOARD(0, bo, &phi, &delta, &te_ret, 0);
    CPPUNIT_ASSERT_EQUAL(0, phi);
    CPPUNIT_ASSERT_EQUAL(VAL_INF, delta);
    CPPUNIT_ASSERT( !teCmpTE(&te, &te_ret) );

    // 優越する局面
    BOARD* bo2 = newBOARDwithInit();
    bo_set_by_text( bo2,
                    "P1 *  *  *  * -OU *  *  *  * \n"
                    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  * +KI *  *  *  * \n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00KI00GI\n"
                    "+\n" );

    CPPUNIT_ASSERT( bo->key == bo2->key );
    CPPUNIT_ASSERT( boCmp(bo, bo2) );

    phi = -1; delta = -1;
    hsGetBOARD(0, bo2, &phi, &delta, NULL, 0);
    CPPUNIT_ASSERT_EQUAL(0, phi);
    CPPUNIT_ASSERT_EQUAL(VAL_INF, delta);

    // 優越しない局面
    bo_set_by_text( bo2,
                    "P1 *  *  *  * -OU *  *  *  * \n"
                    "P2 *  *  *  *  *  *  *  *  * \n"
		    "P3 *  *  *  * +KI *  *  *  * \n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
		    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00GI\n"
                    "+\n" );

    CPPUNIT_ASSERT( bo->key == bo2->key );
    CPPUNIT_ASSERT( !hsGetBOARD(0, bo2, &phi, &delta, NULL, 0) );

    freeBOARD( bo );
    freeBOARD( bo2 );
  }
};
CPPUNIT_TEST_SUITE_REGISTRATION( TranspositionTest );
