
// 通常探索のテスト

#include <si/si.h>
#include "tests.h"

using namespace std;
using namespace CppUnit;

class SearchTest: public TestCase
{
  CPPUNIT_TEST_SUITE( SearchTest );
  CPPUNIT_TEST( test_1 );
  CPPUNIT_TEST_SUITE_END();

public:
  virtual void setUp()
  {
    newHASH();
  }

  virtual void tearDown()
  {
    freeHASH();
  }

  /** 通常探索 */
  void test_1()
  {
    BOARD* bo = newBOARDwithInit();
    boSetHirate(bo);

    extern INPUTSTATUS bfs_minmax( BOARD* bo, int depth, TE* best_move );

    TE te;
    bfs_minmax(bo, 15, &te);
    printf("best = %s\n", te_str(bo, &te).c_str() );
  }
};
CPPUNIT_TEST_SUITE_REGISTRATION( SearchTest );

int main()
{
  TextTestRunner runner;
  TestFactoryRegistry& registry = TestFactoryRegistry::getRegistry();
  runner.addTest( registry.makeTest() );
  return !runner.run("", false);
}
