
# specの書き方は
# http://docs.fedoraproject.org/developers-guide/ch-rpm-building.html

%define name    daemonshogi
%define version 0.3
%define release 1.fc11


################################################
# introduction

Name:        %name
Summary:     Daemonshogi is a GTK+ based, simple shogi (Japanese chess) program.
Summary(ja): デーモン将棋はGTK+を使ったシンプルな将棋プログラムです。
Version:     %version
Release:     %release
License:     GPLv3+
Group:       Amusements/Games
URL:         http://daemonshogi.sourceforge.jp/
Requires:    gtk2 >= 2.14.0
Requires:    tokyocabinet

Source:        %{name}-%{version}.tar.gz
BuildRequires: gtk2-devel >= 2.14.0
BuildRequires: tokyocabinet-devel
BuildRequires: cppunit-devel
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root

%description
Daemonshogi is a GTK+ based, simple shogi (Japanese chess) program.

%description -l ja
デーモン将棋はGTK+を使ったシンプルな将棋プログラムです。


################################################
%prep
%setup -q


################################################
%build
%configure
make


#%ifarch alpha
#   CFLAGS="$RPM_OPT_FLAGS" LDFLAGS="-s" ./configure --host=alpha-redhat-linux\
#	--prefix=%{prefix} 
#%else
#   CFLAGS="$RPM_OPT_FLAGS" LDFLAGS="-s" ./configure \
#	--prefix=%{prefix} 
#%endif


################################################
%install
rm -rf %{buildroot}
%makeinstall


################################################
%clean
rm -rf %{buildroot}


################################################
%files
%defattr(-, root, root)
%{_bindir}/*
%{_datadir}/*
%doc README README.ja COPYING ChangeLog NEWS AUTHORS INSTALL


################################################
%changelog
* Mon Dec 12 2005 Masahiko Tokita <BQB04357@nifty.ne.jp>
- version 0.1.14
- Fixed for Redhat 9 / Fedora Core 2 / Fedore Core 4

* Fri Dec  9 2005 Masahiko Tokita <BQB04357@nifty.ne.jp>
- Fixed for Vine linux 3.2

* Thu Sep 13 2002 Masahiko Tokita <BQB04357@nifty.ne.jp>
- Cleaned up a bit

* Thu Sep 12 2002 Masahiko Tokita <BQB04357@nifty.ne.jp>
- First try at an RPM
