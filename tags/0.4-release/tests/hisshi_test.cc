/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <si/si.h>
#include <si/ui.h>
#include "tests.h"
#include <dirent.h>

using namespace std;
using namespace CppUnit;


BOARD* new_board_from_file(const string& filename)
{
  BOARD* bo = newBOARDwithInit();
  if ( !bo_load_file(bo, filename.c_str()) ) {
    freeBOARD(bo);
    return NULL;
  }

  return bo;
}

static const string dirname = "../../tests/hisshi"; // TODO: きちんと取得


/** 必至のテスト */
class HisshiTest: public TestCase
{
  CPPUNIT_TEST_SUITE( HisshiTest );
  CPPUNIT_TEST( test_1 );
#if 0
  // CPPUNIT_TEST( test_2 );
  // CPPUNIT_TEST( test_3 );
#endif
  CPPUNIT_TEST( test_4a );
  CPPUNIT_TEST( test_4 );
  CPPUNIT_TEST_SUITE_END();

public:
  virtual void setUp() {
    newHASH();
  }

  virtual void tearDown() {
    freeHASH();
  }

  /** やさしい必至 */
  void test_1()
  {
    BOARD* bo = newBOARDwithInit();

    // http://morinobu27.blog.drecom.jp/archive/55
    // No.82
    bo_set_by_text( bo,
                    "P1 *  *  *  *  *  *  * -KE-KY\n"
                    "P2 *  *  *  *  *  * -FU+GI-OU\n"
                    "P3 *  *  *  *  *  *  * +FU * \n"
                    "P4 *  *  *  *  *  *  *  *  * \n"
                    "P5 *  *  *  *  *  *  * -KI * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7 *  *  *  *  *  *  *  *  * \n"
                    "P8 *  *  *  *  *  *  *  *  * \n"
		    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00KI00KI\n"
                    "+\n" );

    boSetTsumeShogiPiece(bo);

    TE te_ret;
    int node_limit = 5000;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, 
			  bfs_hisshi(bo, 1, 5, &te_ret, &node_limit) );
    // CPPUNIT_ASSERT_EQUAL( te_make_put(0x31, KIN), te_ret);

    MOVEINFO2 seq;
    seq.count = 0;
    hs_get_best_sequence(1, bo, &seq);
    mi2_print_sequence(&seq, *bo);
  }

#if 0
  /** 2手スキ */
  void test_2() 
  {
    // http://www.hakusa.net/shogi/note/item_57.html
    BOARD* bo = newBOARDwithInit();
    bo_set_by_text( bo,
                    "P1+UM-KE-GI *  *  *  * -KE-KY\n"
                    "P2 *  *  *  *  *  * -HI *  * \n"
                    "P3-FU-FU * -OU-FU *  *  *  * \n"
                    "P4 *  * -FU * -KI *  *  * -FU\n"
                    "P5+FU * +FU *  *  *  *  *  * \n"
		    "P6 *  *  *  *  * -FU * +FU * \n"
		    "P7 * -GI *  * -GI * +FU * +FU\n"
		    "P8 *  *  *  *  * +HI+KI * +KY\n"
		    "P9+KY+KE * -TO *  *  * +KE+OU\n"
                    "P+00KI00GI00KY00FU00FU\n"
                    "P-00KA00KI00FU00FU00FU00FU\n"
                    "+\n" );
    // boSetTsumeShogiPiece(bo);
    boSetToBOARD(bo);

    TE te_ret;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, bfs_hisshi(bo, 2, 5, &te_ret) );
    hs_print_best_sequence(2, bo);
  }
#endif


  /** 難しい必至問題 */
  void test_3()
  {
    BOARD* bo = newBOARDwithInit();

    // http://ameblo.jp/professionalhearts/entry-10000931734.html
    bo_set_by_text( bo,
                    "P1 *  *  *  *  * -KE * -KE-KY\n"
                    "P2 *  *  *  *  *  *  * -FU-FU\n"
                    "P3 *  *  *  * -FU * -GI * -OU\n"
                    "P4 *  *  *  *  * +KE *  *  * \n"
                    "P5 *  *  *  *  *  *  * +GI+FU\n"
                    "P6 *  *  *  *  * -RY *  *  * \n"
                    "P7 *  *  *  *  *  *  *  *  * \n"
                    "P8 *  *  *  *  *  *  *  *  * \n"
                    "P9 *  *  *  *  *  *  *  *  * \n"
                    "P+00HI\n"
                    "+\n" );
    // 24HI 16RY 14FU 14FU 14HI
    boSetTsumeShogiPiece(bo);

    BOARD* tmp_bo;
    TE te_ret;
    int node_limit;

    // 無駄受け
    tmp_bo = bo_dup(bo);
    boMove_mate(tmp_bo, te_make_put(0x42, HISHA) );
    boMove_mate(tmp_bo, te_make_put(0x13, FU) );
    node_limit = 5000;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, 
			  dfpn_mate(tmp_bo, 31, &te_ret, &node_limit) );
    freeBOARD(tmp_bo);
#if 0
    // 実は24飛では詰まない？
    tmp_bo = bo_dup(bo);
    boMove_mate(tmp_bo, te_make_put(0x42, HISYA) );
    boMove_mate(tmp_bo, te_make_move(0x22, 0x32, false) );
    printBOARD(tmp_bo);

    newHASH();
    node_limit = 20000;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, 
			  dfpn_mate(tmp_bo, 51, &te_ret, &node_limit) );
    freeBOARD(tmp_bo);
#endif // 0

    // 必至？
    node_limit = 20000;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, 
                          bfs_hisshi(bo, 1, 31, &te_ret, &node_limit) );

    MOVEINFO2 seq;
    seq.count = 0;
    hs_get_best_sequence(1, bo, &seq);
    mi2_print_sequence(&seq, *bo);
  }


  /** 打ち歩詰めチェックが不味い？ */
  void test_4a()
  {
    BOARD* bo = new_board_from_file( dirname + "/" + "f.csa" );
    boSetTsumeShogiPiece(bo);

    printBOARD( bo );

    boMove_mate( bo, te_make_put(0x36, KAKU) );
    bo_pass(bo);
    
    printBOARD( bo );

    CPPUNIT_ASSERT_EQUAL( 1, bo->long_attack[0][0x45].count );
    CPPUNIT_ASSERT( bo->long_attack[0][0x45].from[0] == 0x36 );

    MOVEINFO mi;
    ohte(bo, &mi);
    TE te = te_make_move(0x36, 0x45, true);
    CPPUNIT_ASSERT( mi_includes(&mi, te) );

#if 0
    boMove_mate(bo, te );
    boMove_mate(bo, te_make_move(0x44, 0x45, false) );
#endif // 0
    
    TE te_ret;
    int node_limit = 2000;
    CPPUNIT_ASSERT_EQUAL( CHECK_MATE, dfpn_mate(bo, 31, &te_ret, &node_limit) );

    MOVEINFO2 seq;
    seq.count = 0;
    hs_get_best_sequence(0, bo, &seq);
    mi2_print_sequence(&seq, *bo); 
  }


  /** いっぱい試す */
  void test_4()
  {
    static const char* filename[] = {
      "38.csa",
      "41.csa",
      "42.csa",
      "43.csa",
      "f.csa",
      "d20050315.csa",

      // 3手目の18HI が詰めろ探索から漏れる.
      // "d20050803.csa",
      NULL
    };

    for (int i = 0; filename[i]; i++) {
      printf("file: %s\n", filename[i]);

      BOARD* bo = new_board_from_file( dirname + "/" + filename[i] );
      boSetTsumeShogiPiece(bo);

      printBOARD(bo);
      newHASH();

      TE te_ret;
      int node_limit = 5000;
      CPPUNIT_ASSERT_EQUAL( CHECK_MATE,
                            bfs_hisshi(bo, 1, 15, &te_ret, &node_limit) );

      freeBOARD(bo);
    }
  }
};
CPPUNIT_TEST_SUITE_REGISTRATION( HisshiTest );

int main() {
  TextTestRunner runner;
  TestFactoryRegistry& registry = TestFactoryRegistry::getRegistry();
  runner.addTest( registry.makeTest() );
  return !runner.run("", false);
}


/*
  必至の問題

  http://www.geocities.jp/problem_extra/hint.html
 */
