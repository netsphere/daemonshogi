
/** daemonshogi 単体テスト */

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TextTestRunner.h>

inline std::ostream& operator << (std::ostream& ost, std::pair<int, int> x) {
  ost << '(' << x.first << ',' << x.second << ')';
  return ost;
}

