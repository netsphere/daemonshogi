
// 通常探索のテスト

#include <si/si.h>
#include "tests.h"

using namespace std;
using namespace CppUnit;

class SearchTest: public TestCase
{
  CPPUNIT_TEST_SUITE( SearchTest );
  CPPUNIT_TEST( test_1 );
  CPPUNIT_TEST_SUITE_END();

public:
  virtual void setUp()
  {
    newHASH();
  }

  virtual void tearDown()
  {
    freeHASH();
  }

  /** 通常探索 */
  void test_1()
  {
    BOARD* bo = newBOARDwithInit();
    boSetHirate(bo);

    TE te;
    bfs_minmax(bo, 11, &te);
    printf("best = %s\n", te_str(&te, *bo).c_str() );

    MOVEINFO2 seq;
    seq.count = 0;
    hs_get_best_sequence(-1, bo, &seq);
    mi2_print_sequence(&seq, *bo);
  }
};
CPPUNIT_TEST_SUITE_REGISTRATION( SearchTest );

int main()
{
  TextTestRunner runner;
  TestFactoryRegistry& registry = TestFactoryRegistry::getRegistry();
  runner.addTest( registry.makeTest() );
  return !runner.run("", false);
}
