/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <memory>
#include <stdio.h>
#include <si/si.h>
#include "tests.h"

using namespace std;
using namespace CppUnit;


/** 局面クラスのテスト */
class BoardTest: public TestCase
{
  CPPUNIT_TEST_SUITE( BoardTest );
  CPPUNIT_TEST(test_1);
  CPPUNIT_TEST(test_1a);
  CPPUNIT_TEST(test_2);
  CPPUNIT_TEST(test_3);
  CPPUNIT_TEST(test_4);
  CPPUNIT_TEST(test_4a);
#ifdef ENABLE_SPEED_TEST
  CPPUNIT_TEST(test_5);
#endif // ENABLE_SPEED_TEST
  CPPUNIT_TEST_SUITE_END();

  BOARD* bo;

public:
  virtual void setUp() 
  {
    bo = newBOARDwithInit();
    boSetHirate(bo);
  }

  virtual void tearDown() 
  {
    freeBOARD(bo);
  }

  /** 局面を生成 */
  void test_1()
  {
    printf("BoardTest::%s: begin\n", __func__);

    // 手番
    CPPUNIT_ASSERT_EQUAL( 0, bo->next );

    // 持ち駒
    CPPUNIT_ASSERT_EQUAL( (int16_t) 0, bo->inhand[0][HISHA] );

    CPPUNIT_ASSERT( bo->board[0x95] == OH );

    CPPUNIT_ASSERT( bo->board[0x15] == OH + 0x10);
    CPPUNIT_ASSERT_EQUAL( (Xy) 0x15, bo->king_xy[1] ); // 後手玉

    // 利き情報 /////////////////////

    // 先手飛車
    CPPUNIT_ASSERT_EQUAL( 9, bo->piece_kiki_count[0x82] );

    CPPUNIT_ASSERT( bo->short_attack[0][0x77].count == 1 ); // 桂
    CPPUNIT_ASSERT( bo->long_attack[0][0x77].count == 1 ); // 角
  }

  /** テキストから作る */
  void test_1a()
  {
    BOARD* bo2 = newBOARDwithInit();
    bo_set_by_text( bo2, 
		    "P1-KY-KE-GI-KI-OU-KI-GI-KE-KY\n"
		    "P2 * -HI *  *  *  *  * -KA * \n"
		    "P3-FU-FU-FU-FU-FU-FU-FU-FU-FU\n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7+FU+FU+FU+FU+FU+FU+FU+FU+FU\n"
		    "P8 * +KA *  *  *  *  * +HI * \n"
		    "P9+KY+KE+GI+KI+OU+KI+GI+KE+KY\n"
		    "+\n" );

    CPPUNIT_ASSERT_EQUAL( 0, boCmp(bo, bo2) );

    CPPUNIT_ASSERT_EQUAL( bo->key, bo2->key );
    CPPUNIT_ASSERT_EQUAL( bo->hash_all, bo2->hash_all );

    CPPUNIT_ASSERT_EQUAL(1, bo->long_attack[0][0x83].count);
    CPPUNIT_ASSERT_EQUAL(2, bo->long_attack[0][0x81].count);
    CPPUNIT_ASSERT_EQUAL(1, bo->long_attack[0][0x72].count);
    CPPUNIT_ASSERT_EQUAL(1, bo->long_attack[0][0x92].count);
  }

  /** 局面を進める: 利きも更新 */
  void test_2()
  {
    printf("BoardTest::%s: begin\n", __func__);

    boMove(bo, te_make_move(0x77, 0x67, false) );
    CPPUNIT_ASSERT( bo->next == 1 );

    CPPUNIT_ASSERT( bo_attack_count(bo, 0, 0x67) == 0 );
    CPPUNIT_ASSERT( bo_attack_count(bo, 0, 0x57) == 1 ); // 歩の利き
    CPPUNIT_ASSERT( bo_attack_count(bo, 0, 0x33) == 1 );
    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x33) == 2 );
    CPPUNIT_ASSERT_EQUAL( 8, bo->piece_kiki_count[0x88] );
  }

  /** 大駒を動かす */
  void test_3()
  {
    printf("BoardTest::%s: begin\n", __func__);

    boMove(bo, te_make_move(0x77, 0x67, false) );

    // 動かす前の状態
    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x38) == 1 );
    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x29) == 2 );

    //飛車先の歩
    boMove(bo, te_make_move(0x38, 0x48, false) );

    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x38) == 1 );
    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x48) == 1 );
    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x58) == 1 ); // 歩による利き

    boMove(bo, te_make_move(0x72, 0x62, false) );

    // 飛車を動かす
    CPPUNIT_ASSERT_EQUAL( 10, bo->piece_kiki_count[0x28] );
    boMove( bo, te_make_move(0x28, 0x27, false) );
    CPPUNIT_ASSERT_EQUAL( 9, bo->piece_kiki_count[0x27] );

    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x29) == 2 );
    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x28) == 2 );    
    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x27) == 2 );    
    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x26) == 4 );

    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x38) == 0 );
    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x48) == 0 );    
    CPPUNIT_ASSERT( bo_attack_count(bo, 1, 0x58) == 1 );
  }

  /** ハッシュ値 */
  void test_4()
  {
    printf("BoardTest::%s: begin\n", __func__);
    
    // 手番だけ変える
    bo_set_by_text( bo, 
		    "P1-KY-KE-GI-KI-OU-KI-GI-KE-KY\n"
		    "P2 * -HI *  *  *  *  * -KA * \n"
		    "P3-FU * -FU-FU-FU-FU-FU-FU-FU\n"
		    "P4 *  *  *  *  *  *  *  *  * \n"
		    "P5 * -FU *  *  *  *  * +FU * \n"
		    "P6 *  *  *  *  *  *  *  *  * \n"
		    "P7+FU+FU+FU+FU+FU+FU+FU * +FU\n"
		    "P8 * +KA *  *  *  *  * +HI * \n"
		    "P9+KY+KE+GI+KI+OU+KI+GI+KE+KY\n"
		    "+\n" );
    uint64_t s, t;
    s = bo->key; t = bo->hash_all;
    CPPUNIT_ASSERT( s == t );

    boMove( bo, te_make_move(0x82, 0x85, false) );
    boMove( bo, te_make_move(0x28, 0x25, false) );
    boMove( bo, te_make_move(0x85, 0x83, false) );
    boMove( bo, te_make_move(0x25, 0x28, false) );
    boMove( bo, te_make_move(0x83, 0x82, false) );

    CPPUNIT_ASSERT( ~s == bo->key );
    CPPUNIT_ASSERT( ~t == bo->hash_all );
  }

  /** 局面を進めて戻す */
  void test_4a()
  {
    bo_set_by_text( bo, 
		    "P1-KY-KE-GI-KI-OU-KI-GI-KE-KY\n"
		    "P2 * -HI *  *  *  *  * -KA * \n"
		    "P3-FU-FU-FU-FU-FU-FU * -FU-FU\n"
		    "P4 *  *  *  *  *  * -FU *  * \n"
		    "P5 *  *  *  *  *  *  *  *  * \n"
		    "P6 *  * +FU *  *  *  *  *  * \n"
		    "P7+FU+FU * +FU+FU+FU+FU+FU+FU\n"
		    "P8 * +KA *  *  *  *  * +HI * \n"
		    "P9+KY+KE+GI+KI+OU+KI+GI+KE+KY\n"
		    "+\n" );

    uint64_t s, t;
    s = bo->key; t = bo->hash_all;
    CPPUNIT_ASSERT( s == t );

    boMove(bo, te_make_move(0x88, 0x22, true) );

    CPPUNIT_ASSERT( s != bo->key );
    CPPUNIT_ASSERT( t != bo->hash_all );
    CPPUNIT_ASSERT( bo->key != bo->hash_all );

    boBack(bo );

    CPPUNIT_ASSERT( s == bo->key );
    CPPUNIT_ASSERT( t == bo->hash_all );
    CPPUNIT_ASSERT( bo->key == bo->hash_all );
  }

#ifdef ENABLE_SPEED_TEST
  void sub5(int depth, BOARD* bo)
  {
    if (depth <= 0)
      return;

    MOVEINFO mi;
    make_moves(bo, &mi);
    for (int j = 0; j < mi.count; j++) {
#if 1 // 2009.9.24現在 こちらが速い
      boMove_mate(bo, mi.te[j]);
      sub5(depth - 1, bo);
      boBack_mate(bo);
#else
      BOARD* bo2 = bo_dup(bo);
      boMove_mate(bo2, mi.te[j]);
      sub5(depth - 1, bo2);
      freeBOARD(bo2);
#endif
    }
  }

  /** 局面を戻すのとコピーするのはどちらが速いか */
  void test_5() 
  {
    sub5(5, bo);
  }
#endif // ENABLE_SPEED_TEST
};
CPPUNIT_TEST_SUITE_REGISTRATION( BoardTest );


int main()
{
  TextTestRunner runner;
  TestFactoryRegistry& registry = TestFactoryRegistry::getRegistry();
  runner.addTest( registry.makeTest() );
  return !runner.run("", false);
}
