

#include <memory>
#include <stdio.h>
#include <si/si.h>
#include <si/si-think.h>
#include "tests.h"
#include <vector>

using namespace std;
using namespace CppUnit;


void print_eval(const BOARD* bo)
{
  int x, y;
  for (y = 1; y <= 9; y++) {
    for (x = 9; x >= 1; x--) {
      PieceKind p = bo->board[(y << 4) + x];
      if ( p != 0 ) {
        if (getTeban(p) == SENTE)
          printf(" %4d ", add_grade(bo, (y << 4) + x, SENTE));
        else
          printf("-%4d ", add_grade(bo, (y << 4) + x, GOTE));
      }
      else
        printf("      ");
    }
    printf("\n");
  }
}


/** 静的評価のテスト */
class EvalTest: public TestCase
{
  CPPUNIT_TEST_SUITE( EvalTest );
  CPPUNIT_TEST(test_5);
  CPPUNIT_TEST(test_6);
  CPPUNIT_TEST_SUITE_END();

  BOARD* bo;

public:
  virtual void setUp() 
  {
    bo = newBOARDwithInit();
    boSetHirate(bo);
    boPlayInit(bo);
  }

  virtual void tearDown() 
  {
    freeBOARD(bo);
  }

  /** 局面を戻したときの得点 */
  void test_5()
  {
    typedef pair<int, int> Score;
    vector<Score> s;

    TE moves[] = {
      te_make_move(0x77, 0x67, false),
      te_make_move(0x33, 0x43, false),
      te_make_move(0x88, 0x22, true), // 角取り、成り
      te_make_move(0x15, 0x25, false), // 玉が動く
      te_make_move(0x96, 0x87, false)
    };

    for (int i = 0; i < 5; i++) {
      s.push_back( Score(bo->point[0], bo->point[1]) );
      cout << __func__ << ":" << s.back() << "\n";

      print_eval(bo);
      boMove( bo, moves[i] );
      print_eval(bo);
      CPPUNIT_ASSERT_EQUAL( Score(grade(bo, SENTE), grade(bo, GOTE)),
                            Score(bo->point[0], bo->point[1]) );
    }

    while (s.size() > 0) {
      boBack( bo );
      cout << pair<int, int>(bo->point[0], bo->point[1]) << "\n";
      CPPUNIT_ASSERT_EQUAL( s.back(), Score(bo->point[0], bo->point[1]) );
      s.pop_back();
    }
  }

  /** 同一局面に戻ったとき、評価値も同じ？ */
  void test_6()
  {
    pair<int, int> s = pair<int, int>(bo->point[0], bo->point[1]);
    uint64_t h = bo->hash_all;

    TE moves[] = {
      te_make_move(0x82, 0x85, false), // -30,0
      te_make_move(0x16, 0x26, false), // 0,-9
      te_make_move(0x95, 0x86, false), // 0,0
      te_make_move(0x15, 0x25, false), // 0,0
      te_make_move(0x85, 0x82, false), // +30,0
      te_make_move(0x26, 0x16, false), // 0,-38
      te_make_move(0x86, 0x95, false), // 0,0
      te_make_move(0x25, 0x15, false)  // 0,0
    };

    cout << pair<int, int>(bo->point[0], bo->point[1]) << "\n";
    for (int i = 0; i < 8; i++) {
      boMove( bo, moves[i] );
      cout << pair<int, int>(bo->point[0], bo->point[1]) << "\n";
    }

    CPPUNIT_ASSERT( bo->hash_all == h );
    CPPUNIT_ASSERT_EQUAL(s, (pair<int, int>(bo->point[0], bo->point[1])) );
  }

};
CPPUNIT_TEST_SUITE_REGISTRATION( EvalTest );

