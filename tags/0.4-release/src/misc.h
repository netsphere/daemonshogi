/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/misc.h,v $
 * $Id: misc.h,v 1.1.1.1 2005/12/09 09:03:06 tokita Exp $
 */

#ifndef _MISC_H_
#define _MISC_H_

#include <gtk/gtk.h>
#include <string>

/** ダブルクリックと判定するまでの時間(ミリ秒) */
#define DOUBLECLICKTIMEOUT 400

// void       daemon_abort         (const char* s);
GdkPixmap* load_pixmap_from_data(GtkWidget* window, char **data);
GdkPixmap* load_pixmap_from_data_scaled(GtkWidget* window, const char** data,
					gint w,
					gint h);
GdkPixmap* load_pixmap_from_imlib_image_scaled(GtkWidget* window,
					       GdkPixbuf *im,
					       gint w,
					       gint h);

void set_color(GdkGC* gc, guint16 r, guint16 g, guint16 b);

gboolean check_timeval(struct timeval *tv, struct timeval *now);
gboolean is_on_rectangle(GdkRectangle* a, GdkRectangle* b);
void pending_loop(void);
gboolean daemon_on_rectangle(GdkRectangle *rect, GdkPoint *point);

std::string get_datetime_string(const time_t& t);

#endif /* _MISC_H_ */



