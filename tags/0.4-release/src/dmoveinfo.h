/* -*- mode:c++ -*-
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _DMOVEINFO_H_
#define _DMOVEINFO_H_

#include "dte.h"
#include <string>
#include <vector>


extern const char* ANNOT_STR[];

enum Annotation {
  ANNOT_NONE = 0,
  ANNOT_POOR = 1,
  ANNOT_QUESTIONABLE = 2,
};

/** 指し手. グラフの辺 */
struct MoveEdge 
{
  /** 指し手 */
  DTe te;

  /** 消費時間. 単位 = 秒 */
  int time;

  std::string te_comment;

  /** poor など */
  Annotation annotation;

  /**
   * 指した後の局面.
   * 手が投了などのときはNULL
   */
  struct KyokumenNode* node;

  MoveEdge(): time(0), annotation(ANNOT_NONE), node(NULL) {}

  ~MoveEdge() { 
    // node は解放しない 
  }
};


/**
 * 局面ノード
 * 木構造で、合流は考えない。
 *  \todo tagTREE との統合
 */
struct KyokumenNode 
{
  typedef std::vector<MoveEdge> Moves;

  /** 親ノード。ルート局面ではNULL */
  KyokumenNode* const prev;

  /** 親ノードに戻るための手。ルート局面ではNULL */
  DTe* move_to_back;

  /** 候補手. すべての手を格納するわけではない。 */
  Moves moves;

  KyokumenNode(KyokumenNode* p, const DTe* m): prev(p) { 
    move_to_back = m ? new DTe(*m) : NULL; 
  }

  ~KyokumenNode() { 
    Moves::iterator it;
    for (it = moves.begin(); it != moves.end(); it++)
      delete it->node;
    moves.clear();
    delete move_to_back;
  }

  /**
   * 指し手（と進めたノード）を追加する
   * @return   追加したedgeへのイテレータ
   */
  Moves::iterator add_child(const DTe& te, int sec, 
			    const std::string& te_comment) 
  {
    MoveEdge p;
    p.te = te;
    p.te_comment = te_comment;
    p.time = sec;
    p.node = new KyokumenNode(this, &te);
    moves.push_back(p);
    return moves.end() - 1;
  }

  /** 手によって進む先のノードを削除する */
  void remove_child(const DTe& te) {
    Moves::iterator it;
    for (it = moves.begin(); it != moves.end(); it++) {
      if (it->te == te) {
	delete it->node;
	moves.erase(it);
	return;
      }
    }
  }

  Moves::iterator find_edge(const DTe& te) {
    Moves::iterator i;
    for (i = moves.begin(); i != moves.end(); i++) {
      if (i->te == te)
	return i;
    }
    return i;
  }

  /**
   * 1手進んだノードを返す.
   * @return 見つからなかったときは NULL
   */
  KyokumenNode* lookup_child(const DTe& te) const {
    Moves::const_iterator i;
    for (i = moves.begin(); i != moves.end(); i++) {
      if (i->te == te)
	return i->node;
    }
    return NULL;
  }

private:
  KyokumenNode(const KyokumenNode& );              // not impl.
  KyokumenNode& operator = (const KyokumenNode& ); // not impl.
};


typedef KyokumenNode DMoveInfo; // for compatibility

/*********************************************************/
/* function prototypes */
/*********************************************************/

KyokumenNode* daemon_dmoveinfo_new   (KyokumenNode* p, const DTe* m);
// void       daemon_dmoveinfo_init  (DMoveInfo* mi);
void       daemon_dmoveinfo_free  (KyokumenNode* mi);
void       daemon_dmoveinfo_add   (DMoveInfo* mi,
				   const DTe* te, int sec);

void daemon_dmoveinfo_pop(DMoveInfo* mi);

void       daemon_dmoveinfo_remove(DMoveInfo* mi,
				   int index);
void       daemon_dmoveinfo_output(DMoveInfo* mi,
				   FILE* out);


#endif /* _DMOVEINFO_H_ */
