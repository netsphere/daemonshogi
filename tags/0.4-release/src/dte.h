/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _DTE_H_
#define _DTE_H_

#include <stdint.h> // int16_t
#include <string>
#include "si/si.h"


/**
 * 指し手.
 */
struct DTe: public TE
{
  enum Special {
    NORMAL_MOVE = 0,
    RESIGN = 2,  // 投了
    KACHI = 6,  // (入玉での) 勝利宣言
    CHUDAN = 4, // 中断

    // 以下はCSA棋譜ファイルのみ
    // ...
  };
  
  /** 取った駒. undoに必要. */
  PieceKind tori;

  /** 投了など */
  Special special;

  bool operator == (const DTe& x) const {
    if (special != NORMAL_MOVE || x.special != NORMAL_MOVE)
      return special == x.special;
    else {
      return to == x.to && fm == x.fm && nari == x.nari && uti == x.uti &&
             tori == x.tori;
    }
  }
};


/*********************************************************/
/* function prototypes */
/*********************************************************/

DTe* daemon_dte_new   (void);
void daemon_dte_free  (DTe* te);
void daemon_dte_output(const DTe* te, FILE* out);

struct DBoard;
std::string te_to_str(const DTe* te, const DBoard* bo);

#endif /* _DTE_H_ */
