/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"
#include "ui.h"

void trSearch(TREE *tree, MOVEINFO *mi, MOVEINFO *t_mi, int *max, int count);

/**
 * 新しくメモリを確保してTREEのポインタを返す。
 * @return TREEのポインタ
 */
TREE *newTREE(void) {
  TREE *tree;

  tree = (TREE *) malloc(sizeof(TREE));
  if (tree == NULL) {
    si_abort("No enough memory. In tree.c#newTREE()");
  }

  initTREE(tree);

  return tree;
}

/**
 * tree を初期化する。
 * @param tree 対象のTREE
 */
void initTREE(TREE* tree)
{
  tree->te.fm   = 0;
  tree->te.to   = 0;
  tree->te.nari = 0;
  tree->te.uti  = 0;
  tree->parent  = NULL;
  tree->brother = NULL;
  tree->child   = NULL;
}


/**
 * tree のメモリを解放する。子と<b>自分の</b>兄弟も解放する。
 * @param tree 対象のTREE
 */
void freeTREE(TREE* tree)
{
#ifdef DEBUG
  assert ( tree != NULL );
#endif /* DEBUG */

  if ( tree->child != NULL )
    freeTREE( tree->child );

  if ( tree->brother != NULL )
    freeTREE( tree->brother );

  free(tree);
}

void trSearch(TREE *tree, MOVEINFO *mi, MOVEINFO *t_mi, int *max, int count) {
  /* miAdd(t_mi, &(tree->te)); */
  t_mi->te[count - 1] = tree->te;

  if (tree->child != NULL) {
    trSearch(tree->child, mi, t_mi, max, count + 1);
    /* t_mi->count--; */
  } else {
    if (*max <= count) {
      *max = count;
      t_mi->count = count;
      miCopy(mi, t_mi);
    }
  }
  if (tree->brother != NULL) {
    /* t_mi->count--; */
    trSearch(tree->brother, mi, t_mi, max, count);
  }
}

/**
 * tree の内容を MOVEINFO に格納する。
 *
 */	       
void trSetMoveinfo(TREE *tree, MOVEINFO *mi) {
  MOVEINFO t_mi;
  int max, count;

  assert(tree != NULL);
  assert(mi != NULL);

  max = 0;
  count = 1;
  t_mi.count = 0;

  trSearch(tree, mi, &t_mi, &max, count);
}


/** 詰将棋の結果の木をすべて (再帰的に) 表示 */
void printTREE(const TREE* tree, int n) 
{
  int i;
  
  if ( tree == NULL )
    return;
  
  for (i=0; i<n; i++) {
    putchar(' ');
  }

  /* printf("%X : ", (int)tree); */
  putchar(n % 2 ? 'v' : '^');
  printTE(&(tree->te));
  /* putBOARD(bo); */

  if ( tree->child != NULL )
    printTREE(tree->child, n + 1);

  if ( tree->brother != NULL )
    printTREE(tree->brother, n);
}


/**
 * parent に子の盤面のポインタ child を加える
 * @param parent 親のポインタ
 * @param child 子のポインタ
 */
void addTREEChild(TREE *parent, TREE *child) {
#ifdef DEBUG
  assert( parent != NULL );
  assert( child != NULL );
#endif /* DEBUG */

  if ( parent->child == NULL ) {
    parent->child = child;
    parent->child->parent = parent;
    parent->child->child = NULL;
    parent->child->brother = NULL;
  } else {
    TREE *tmp;
    tmp = parent->child;
    parent->child = child;
    parent->child->parent = parent;
    parent->child->child = NULL;
    parent->child->brother = tmp;
  }
}


/**
 * tree の兄弟のツリーをたどって target の一つ上の兄弟を探してそれを
 * 返す。
 * @param tree 検索対象のツリーへのポインタ
 * @param target 兄弟のツリーへのポインタ
 */
TREE *findPointerMe(TREE *tree, TREE *target) {
  TREE *tmp;
#ifdef DEBUG
  assert ( tree != NULL );
  assert ( target != NULL );
#endif /* DEBUG */

  tmp = tree;
  
  while ( 1 ) {
    if ( tmp->brother == target )
      break;
    tmp = tmp->brother;
#ifdef DEBUG
    assert ( tmp != NULL );
#endif /* DEBUG */
  }
  
  return tmp;
}


/**
 * 不詰が確定した手をツリーから削除する。
 * @param tree ツリー
 */
void removeTREEChild(TREE *tree) {
#ifdef DEBUG
  assert ( tree != NULL );
  assert ( tree->parent != NULL );
#endif /* DEBUG */

  if ( tree->child != NULL )
    freeTREE( tree->child );
  
  if ( tree->brother != NULL ) {
    if ( tree->parent->child == tree ) {
      tree->parent->child = tree->brother;
    } else {
      TREE *tmp;
      tmp = findPointerMe( tree->parent->child, tree );
      tmp->brother = tree->brother;
    }
  }
  
  tree->parent->child = NULL;

  free(tree);
}
