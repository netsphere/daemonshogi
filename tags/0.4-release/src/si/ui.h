/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _UI_H_
#define _UI_H_

#include <stdio.h>
#include "si.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* charcter base UI */

void fputBOARD(FILE* out, int mode, const BOARD* bo);
void fputNoBOARD(FILE *out, BOARD *bo);
void fputToBOARD(FILE *out, BOARD *bo);

void printNoBOARD(BOARD * bo);
void printMOVEINFO(MOVEINFO * mi);
void printMOVEINFO2(MOVEINFO2 * mi);

void printTE(const TE* te);

void fprintTE(FILE *out, TE * te);
void putPBOX(PBOX *pbox);
void fputPBOX(FILE *out, PBOX *pbox);

void printTREE(const TREE* bo, int n);

/* for GUI support */

void set_pending_function(void (*f)(void));
void processing_pending(void);


/* file I/O */
void CSA_output_auto(BOARD *bo);
void CSA_output(BOARD *bo, char *filename);


/* input functions */
INPUTSTATUS si_input_next_human(BOARD *bo, TE *te);
INPUTSTATUS si_input_next_computer(BOARD *bo, TE *te);
INPUTSTATUS si_input_next_serialport(BOARD *bo, TE *te);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _UI_H_ */

