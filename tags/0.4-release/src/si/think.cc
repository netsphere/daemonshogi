/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <assert.h>
#include "si.h"
#include "si-think.h"
#include "ui.h"
#include <algorithm>
#include <string.h>
#include "taboo_list.h"

using namespace std;


extern int THINK_DEEP_MAX;


/** 合法手のみ残す */
static void correct_moves(const BOARD* bo, MOVEINFO* mi)
{
  MOVEINFO legal_moves;
  int i;

  make_moves( bo, &legal_moves );
  for (i = 0; i < mi->count; ) {
    if ( !mi_includes(&legal_moves, mi->te[i]) )
      miRemoveTE2( mi, i );
    else
      i++;
  }
}


/**
 * 侯補手を探す。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param depth 読みの深さ
 */
void think(BOARD *bo, MOVEINFO *mi, int depth) 
{
  int king_xy, king_xy2;
  int num_of_ohte;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
  assert( 0 <= depth );
#endif /* DEBUG */
  
  mi->count = 0;

  /* 自分の王の位置 */
  king_xy = bo->king_xy[bo->next];
  /* 相手の王の位置 */
  king_xy2 = bo->king_xy[ 1 - bo->next ];
  
  /* 王手されているか調べる */
  num_of_ohte = bo_attack_count(bo, (bo->next == 0), king_xy);
  assert( 0 <= num_of_ohte && num_of_ohte <= 2 );
  
  if ( num_of_ohte ) {
    /* 王手されていたら受けを探す */
    uke( bo, mi );
    return;
  }
#if 0
  if ( depth == 0 ) {
    if ( think_is_tsumero(bo, 5) ) {
      /* 詰めろになっていた */
      /* 詰めろ逃れ専用ルーチンを呼ぶ */
      think_tsumero_uke(bo, mi);
      if ( 0 < mi->count ) {
	return;
      }
      
      /* 受ける手がない場合は王手を探す */
      ohte(bo, mi);
      
      if ( 0 < mi->count ) {
	return;
      }
      
      /* 王手もない場合は合法手をセットして終了する */
      search_053(bo, mi);
      search_068(bo, mi);
      /* ダブった手があったら削除する */
      miRemoveDuplicationTE( mi );

      // 合法手かどうか
      correct_moves( bo, mi );
      
      return;
    }
  }
#endif // 0

#if 0
  for ( i=0; i<1; i++ ) {
    MATE_DEEP_MAX = i;
    if ( mate(bo, &root, 0) ) {
      /* 詰んだ */
      printTREE(root.child, 0);
      break;
    }
  }
    
  if ( root.child != NULL ) {
    /* check mate */
    miAdd( mi, root.child->te );
    freeTREE( root.child );
    return;
  }
#endif /* NOREAD */

  search_011(bo, mi);
  search_013(bo, mi);
  search_014(bo, mi);
  search_012(bo, mi);
  if ( mi->count < 2 ) {
    search_024a(bo, mi);
  }
  if ( depth == 0 ) {
    search_068(bo, mi);
  }
  if ( depth <= THINK_DEEP_MAX - 3 ) {
    search_022(bo, mi);
    search_027(bo, mi);
    search_024a(bo, mi);
    search_031a(bo, mi);
    search_031b(bo, mi);
  }
  if ( depth <= 3 ) {
    search_046(bo, mi);
    search_048(bo, mi);
    search_044(bo, mi);
    search_041(bo, mi);
    search_047(bo, mi);
  }
  if ( depth <= 1 ) {
    search_053(bo, mi);
  }

  /* ダブった手があったら削除する */
  miRemoveDuplicationTE( mi );

  // 合法手かどうか
  correct_moves( bo, mi );

  return;
}


/** 
 * side による target の周りへの利きの合計.
 * bo->next と side は同じとは限らない
 */
int think_count_attack24(const BOARD* bo, int side, Xy target )
{
  assert( target >= 0x11 && target <= 0x99);

  int r = 0;

  for (int i = 0; i < 24; i++) {
    Xy const xy = target + arr_round_to24[i];
    if (xy < 0x11 || xy > 0x99)
      continue;
    if (bo->board[xy] == WALL)
      continue;
    r += bo_attack_count(bo, side, xy);
  }

  return r;
}


/**
 * side による target の周りへの利きの合計
 * bo->next と side は同じとは限らない
 */
int think_count_attack8( const BOARD* bo, int side, Xy target )
{
  assert( target >= 0x11 && target <= 0x99);

  int r = 0;

  for (int i = 0; i < 8; i++) {
    Xy const xy = target + arr_round_to[i];
    if (bo->board[xy] == WALL)
      continue;
    r += bo_attack_count(bo, side, xy);
  }

  return r;
}


/**
 * sideの自玉が行ける範囲を数え上げる.
 * とりあえず、2手で行ける範囲
 */
int think_count_king_movables(const BOARD* bo, int side)
{
  Xy king_xy = bo->king_xy[ side ];
  assert( king_xy != 0 );

  int r[69];
  memset( r, 0, 69 * sizeof(int) );

  Xy xy;
  for (int i = 0; i < 8; i++) {
    xy = king_xy + arr_round_to[i];
    if (bo->board[xy] == WALL)
      continue;
    if (bo->board[xy] && getTeban(bo->board[xy]) == side )
      continue;
    if (bo_attack_count(bo, 1 - side, xy))
      continue;

    for (int j = 0; j < 8; j++) {
      Xy xy2 = xy + arr_round_to[j];
      if (xy2 < 0x11 || xy > 0x99)
        continue;
      if (bo->board[xy2] == WALL)
        continue;
      if (bo->board[xy2] && getTeban(bo->board[xy2]) == side )
        continue;
      if (bo_attack_count(bo, 1 - side, xy2))
        continue;

      r[xy2 - king_xy + 34] = 1;
    }
  }

  r[34] = 0;
  return count(r, r + 69, 1);
}


/**
 * 詰めろを受ける手を検索する。相手玉に対する詰みはない
 * @param bo 対象のBOARD
 * @param mi_ret 手を記録するMOVEINFO
 */
void think_tsumero_uke(BOARD* bo, MOVEINFO* mi_ret)
{
  MOVEINFO mi1;
  int i;
  int my_side = bo->next;
  Xy king_xy = bo->king_xy[bo->next];

#if 0
  mi1.count = 0;

  /* 直前に動いた相手の駒を取る手 */
  search_011(bo, &mi1);

  /* 相手の王への王手 */
  // search_013(bo, &mi1);         ... これが不味い. 王手は受けにならない

  /* 王が動く手 */
  search_049(bo, &mi1);
  /* 自分の王周辺に動く手 */
  search_pressure(bo, &mi1, king_xy, bo->next);
  /* 直前に指した駒への歩打ち */ 
  search_047(bo, &mi1);
  /* 自分の王周辺に駒を打ち込む手 */
  search_031b(bo, &mi1);

  /* ダブった手があったら削除する */
  miRemoveDuplicationTE( &mi1 );
  // 合法手だけ残す
  correct_moves( bo, &mi1 );
#endif

  make_moves(bo, &mi1);

  mi_ret->count = 0;
  if ( mi1.count == 0 )
    return;

  for (i = 0; i < mi1.count; i++) {
    // assert( mi1.te[i].hint == 0 || mi1.te[i].hint == TE_STUPID );
    if ( (mi1.te[i].hint & TE_STUPID) != 0 )
      continue;

    int ma = think_count_attack24( bo, my_side, bo->king_xy[my_side] );
    int oa = think_count_attack24( bo, 1 - my_side, bo->king_xy[my_side] );
    int mc = think_count_king_movables(bo, my_side);

    boMove_mate( bo, mi1.te[i] );

    if ( think_count_attack24(bo, my_side, bo->king_xy[my_side]) <= ma &&
         think_count_attack24(bo, 1 - my_side, bo->king_xy[my_side]) >= oa &&
         think_count_king_movables(bo, my_side) <= mc ) {
      boBack_mate( bo );
      continue;
    }

    TE te_ret;
    int node_limit = GAME_MATE_NODE_LIMIT;
    if ( dfpn_mate(bo, 31, &te_ret, &node_limit) != CHECK_MATE ) {
      // 詰みを防いだ。侯補手に加える
      miAdd( mi_ret, mi1.te[i] );
    }
    boBack_mate( bo );
  }
}


/**
 * nodeの手番から見た評価値.
 * phi,delta = (+VAL_INF, -VAL_INF) で勝ち
 */
void think_eval(const BOARD* bo, PhiDelta* score) 
{
  int x, y;

  score->phi = bo->point[bo->next];
  score->delta = bo->point[bo->next == 0];

  for (y = 1; y <= 9; y++) {
    for (x = 1; x <= 9; x++) {
      PieceKind const p = bo->board[(y << 4) + x];

      if (!p)
        continue;

      // 大駒の可動性を加える
      if ( (p & 0xf) == KYO || (p & 0x7) == HISHA || (p & 0x7) == KAKU ) {
        if (getTeban(p) == bo->next) {
          score->phi += bo->piece_kiki_count[ (y << 4) + x ] * 
	    NUM_OF_MOVE_POINT;
	}
        else {
          score->delta += bo->piece_kiki_count[ (y << 4) + x ] * 
	    NUM_OF_MOVE_POINT;
	}
      }

      // 相手駒への当たり
      if ( getTeban(p) != bo->next ) {
        if ( bo_attack_count(bo, bo->next, (y << 4) + x) )
          score->phi += arr_point[p & 0x7] / 3;   // TODO: NPCへ移動
      }
      else {
        if ( bo_attack_count(bo, 1 - bo->next, (y << 4) + x) )
          score->delta += arr_point[p & 0x7] / 3; 
      }
    }
  }

  // 玉の周りの安全度 (危険度)
  score->phi += think_count_attack8(bo, bo->next, bo->king_xy[1 - bo->next]) *
    OH_SAFE_POINT;
  score->delta += think_count_attack8(bo, 1 - bo->next, bo->king_xy[bo->next]) *
    OH_SAFE_POINT;
}
