/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include "si.h"
#include "si-think.h"
#include "ui.h"
#include <algorithm>

#ifndef NDEBUG
#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>
#endif // !NDEBUG

using namespace std;


/* -------------------------------------------------------- */
/* BOARDの初期化関係関数 */
/* -------------------------------------------------------- */


/**
 * 新しくメモリを確保してBOARDのポインタを返す。
 * @return BOARDのポインタ
 */
static BOARD* newBOARD()
{
  BOARD* b;

  b = (BOARD*) malloc(sizeof(BOARD));
  if (b == NULL) {
    si_abort("No enough memory. In board.c#newBOARD()");
  }

  return b;
}


/**
 * 新しくメモリを確保してBOARDのポインタを返す。初期化付き。
 * @return BOARDのポインタ
 */
BOARD* newBOARDwithInit()
{
  BOARD* b = newBOARD();
  boInit(b);
  
  return b;
}


/**
 * bo のメモリを解放する。
 * @param bo 対象のBOARD
 */
void freeBOARD(BOARD* bo) 
{
  assert( bo != NULL );
  free(bo);
}


/** 
 * 盤面を初期化する。
 * bo 対象のBOARD
 */
void boInit(BOARD* bo) 
{
  int i, j;
  
  assert( bo != NULL );
  
  for (i = -16; i < 16 * 12; i++) {
    // 盤の外に WALL (0x20) で壁を作る
    bo->board[i] = WALL;
  }

  for (i = 0; i < 2; i++)
    bo->king_xy[i] = 0;
  
  for (i = 1; i <= 9; i++) {
    for (j = 1; j <= 9; j++) {
      Xy const sq = (i << 4) + j;
      
      // 中は 0 で初期化
      bo->board[sq] = 0;
      bo->piece_kiki_count[sq] = 0;

      // 利きをクリア
#ifdef USE_PIN
      bo->pinned[sq] = 0;
#endif
      bo->short_attack[0][sq].count = 0;
      bo->short_attack[1][sq].count = 0;
      bo->long_attack[0][sq].count = 0;
      bo->long_attack[1][sq].count = 0;
    }
  }
  
  bo->point[0] = 0;
  bo->point[1] = 0;

  // 駒はすべて駒箱に.
  pbInit(&(bo->pb));
  
  bo->mi.count = 0;
  for (i = 0; i < MOVEINFO2MAX; i++) {
    bo->tori[ i ] = 0;
    bo->used_time[i] = 0;
  }

  bo->total_time[SENTE] = 0;
  bo->total_time[GOTE] = 0;
  bo->limit_time[SENTE] = DEFAULT_LIMIT_TIME;
  bo->limit_time[GOTE] = DEFAULT_LIMIT_TIME;
  bo->before_think_time = 0;

  bo->next = 0;
}


/**
 * 対局用に利き情報や消費時間を初期化する。
 * この関数を呼び出す前に、盤上や持ち駒を設定しておく。
 * @param bo 対象のBOARD
 */
void boPlayInit(BOARD* bo) 
{
  boSetToBOARD(bo);
  
  /* 消費時間に 0 をセットする */
  bo->total_time[0] = 0;
  bo->total_time[1] = 0;
  
  /* 持ち時間に1500秒(25分)をセットする */
  bo->limit_time[0] = 1500;
  bo->limit_time[1] = 1500;

  bo->mi.count = 0;
}


/**
 * bo に平手の配置をセットする。
 * @param bo 対象のBOARD
 */
void boSetHirate(BOARD* bo) 
{
  /* 平手の配置パターン */
  static const PieceKind hirate[] = {
    0x12, 0x13, 0x14, 0x15, 0x18, 0x15, 0x14, 0x13, 0x12,
    0x00, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x17, 0x00,
    0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00,
    0x02, 0x03, 0x04, 0x05, 0x08, 0x05, 0x04, 0x03, 0x02,
  };
  int i, j;
  
  assert( bo != NULL );
  
  for (i = 0; i < 9; i++) {
    for (j = 0; j < 9; j++)
      /* 平手の配置を盤面にセットする */
      boSetPiece( bo, (i + 1) * 16 + (j + 1), hirate[i * 9 + j] );
  }
  for (i = 0; i <= 7; i++) {
    /* 持ち駒を初期化 */
    bo->inhand[0][i] = 0;
    bo->inhand[1][i] = 0;
  }
  bo->next = 0;

  // 利きを更新
  boSetToBOARD(bo);
}

/* -------------------------------------------------------- */
/* BOARDの初期化関係関数 ここまで*/
/* -------------------------------------------------------- */



/**
 * 盤面データ board に基づいて駒コードcode_xyをセットする。
 * @param bo 対象のBOARD
 */
static void update_code_xy(BOARD* bo) 
{
  int x, y;
  
  assert( bo != NULL );
  
  for (y = 1; y <= 9; y++) {
    for (x = 1; x <= 9; x++) {
      PieceKind pie;
      if ((pie = bo->board[(y << 4) + x]) != 0) {
        pbPop(&bo->pb, pie);

        if (pie == OH)
          bo->king_xy[0] = (y << 4) + x;
        else if (pie == OH + 0x10)
          bo->king_xy[1] = (y << 4) + x;;
      } 
    }
  }
}


extern const int arr_xy[];


/**
 * board に基づいて盤面すべての利き情報を設定する。
 * (盤面, 持ち駒, 手番) を更新した後で呼び出すこと。
 * @param bo BOARD
 */
void boSetToBOARD(BOARD* bo) 
{
  int x, y, i;
  int fm;

  assert( bo != NULL );

  pbInit( &bo->pb );
  update_code_xy(bo);

  for (i = 1; i <= 81; i++) {
    bo->short_attack[0][ arr_xy[i] ].count = 0;
    bo->short_attack[1][ arr_xy[i] ].count = 0;
    bo->long_attack[ 0 ][ arr_xy[i] ].count = 0;
    bo->long_attack[ 1 ][ arr_xy[i] ].count = 0;

    bo->piece_kiki_count[ arr_xy[i] ] = 0;
#ifdef USE_PIN
    bo->pinned[ arr_xy[i] ] = 0;
#endif
  }

  // 利きを更新
  for (y = 1; y <= 9; y++) {
    for (x = 1; x <= 9; x++) {
      fm = (y << 4) + x;
      if ( boGetPiece(bo, fm) == 0 )
        continue;
      bo_movetoKiki(bo, fm); // 利きを更新
    }
  }

#ifdef USE_HASH
  bo->key = hsGetHashKey(bo);

  int j, k;
  uint64_t inhand_hash = 0;
  for (i = 0; i < 2; i++) {
    for (j = 1; j <= 7; j++) {
      // 個数分だけ編み込む. 0のときは bo->key = bo->hash_all
      for (k = 0; k < bo->inhand[i][j]; k++)
        inhand_hash ^= hashseed_inhand[i][j][k + 1];
    }
  }

  if (bo->next) // 手番 = 後手
    bo->hash_all = bo->key ^ ~ inhand_hash;
  else
    bo->hash_all = bo->key ^ inhand_hash;

  bo->hash_history[0] = bo->hash_all;
#endif /* USE_HASH */

  // 得点を設定
  if ( bo->king_xy[0] && bo->king_xy[1] ) {
    bo->point[0] = grade(bo, 0);
    bo->point[1] = grade(bo, 1);
  }
}


/**
 * BOARDのコピーを作成する。
 */
BOARD* bo_dup(const BOARD* src) 
{
  BOARD* dest;

  assert( src != NULL );
  
  dest = newBOARD();
  
  /* 構造体をコピーするのに memcpy() を使っているがちょっとあやしいかも。 */
  memcpy(dest, src, sizeof(BOARD));
  
  return dest;
}


/**
 * 持ち駒を増やす。
 * @param bo BOARD
 * @param p 持駒にする駒種類. 成り/不成り、プレイヤー付き
 */
static void inc_inhand(BOARD* bo, PieceKind p )
{
  assert( bo != NULL );
  assert( 1<=p && p<=0x1F );
  assert( p != 8 && p != 0x18 );

  int count = ++bo->inhand[bo->next][p & 0x07];
  pbPush( &bo->pb, p );

#ifdef USE_HASH  
  if (bo->next) 
    bo->hash_all ^= ~ hashseed_inhand[ bo->next][p & 7][ count ];
  else
    bo->hash_all ^= hashseed_inhand[ bo->next][p & 7][ count ];    
#endif
}


/**
 * 持ち駒を減らす.
 */
static void dec_inhand(BOARD* bo, PieceKind p)
{
#ifdef USE_HASH
  if (bo->next)
    bo->hash_all ^= ~ hashseed_inhand[ bo->next][p & 7][ bo->inhand[bo->next][p & 7]];
  else
    bo->hash_all ^= hashseed_inhand[ bo->next][p & 7][ bo->inhand[bo->next][p & 7]];
#endif

  bo->inhand[bo->next][p & 7]--;
  pbPop( &bo->pb, p );
}


/** 駒を置き、周りからの利きを更新しない */
static void put_piece(BOARD* bo, int xy, PieceKind pie )
{
  assert(xy >= 0x11 && xy <= 0x99);
  assert( pie >= 1 && pie <= 0x1f );

  bo->board[xy] = pie;

  if (pie == OH)
    bo->king_xy[0] = xy;
  else if (pie == OH + 0x10)
    bo->king_xy[1] = xy;

  bo_movetoKiki(bo, xy); // xyからの利きを付ける

#ifdef USE_HASH
  /* ハッシュ値に te->to の位置の駒の値を足す */
  bo_add_hash_value(bo, xy);
#endif /* USE_HASH */
}


/** 駒を置き、利きを更新する */
static void put_piece_with_kiki( BOARD* bo, Xy xy, PieceKind pie )
{
  assert(xy >= 0x11 && xy <= 0x99);

  int i;
  int side;

  put_piece( bo, xy, pie );

  for (side = 0; side < 2; side++) {
    for (i = 0; i < bo->long_attack[side][xy].count; i++) {
      Xy vw = xy;
      Xy const attack_from = bo->long_attack[side][xy].from[i];
      XyDiff const direc = norm_direc(attack_from, xy);
      do {
        vw += direc;
        if (bo->board[vw] == WALL)
          break;

        ary_erase( bo->long_attack[side][vw].from,
               bo->long_attack[side][vw].from + bo->long_attack[side][vw].count,
               attack_from );
        bo->long_attack[side][vw].count--;
        bo->piece_kiki_count[attack_from]--;

        if (bo->board[vw]) {
#ifdef USE_PIN
          ... ;
#endif
          break;
        }
      } while (true);
    }
  }
}


/** 駒を取り除き、周りからの利きを更新しない */
static void remove_piece(BOARD* bo, Xy xy)
{
#ifdef USE_HASH
  /* ハッシュ値に te->fm の位置の駒の値を引いておく */
  bo_sub_hash_value(bo, xy);
#endif /* USE_HASH */

  boPopToBoard(bo, xy); // xyからの利きを除く

  if (bo->board[xy] == OH)
    bo->king_xy[0]  = 0;
  else if (bo->board[xy] == OH + 0x10)
    bo->king_xy[1] = 0;

  bo->board[xy] = 0;
}


/** 駒を取り除き、利きを更新する */
static void remove_piece_with_kiki(BOARD* bo, Xy xy)
{
  assert(bo->board[xy]);
  assert(xy >= 0x11 && xy <= 0x99);

  int i;
  int side;

  remove_piece( bo, xy );

  for (side = 0; side < 2; side++) {
    for (i = 0; i < bo->long_attack[side][xy].count; i++) {
      Xy vw = xy;
      Xy const attack_from = bo->long_attack[side][xy].from[i];
      XyDiff const direc = norm_direc(attack_from, xy);
      do {
        vw += direc;
        if (bo->board[vw] == WALL)
          break;

        bo->long_attack[side][vw].from[ bo->long_attack[side][vw].count++ ] = attack_from;
        bo->piece_kiki_count[attack_from]++;

        if (bo->board[vw]) {
#ifdef USE_PIN
          ... ;
#endif
          break;
        }
      } while (true);
    }
  }
}


#ifdef NDEBUG
  #define assert_move(expr)  (static_cast<void>(0))
#else
static void assert_move(const BOARD* bo, const TE& te)
{
  assert( 1 <= (te.to & 0x0f) && (te.to & 0x0f) <=9 );
  assert( 1 <= (te.to >> 4) && (te.to >> 4) <=9 );
  if ( !te_is_put(&te) ) {
    assert( 1 <= (te.fm & 0x0f) && (te.fm & 0x0f) <= 9 );
    assert( 1 <= (te.fm >> 4) && (te.fm >> 4) <=9 );

    assert( bo->board[ te.fm ] != 0 );

    // 移動先に自分の駒があってはいけない
    if ( bo->board[ te.to ] ) {
      if ( bo->board[ te.fm ] & 0x10 )
	assert( (bo->board[ te.to ] & 0x10) == 0 );
      else
	assert( (bo->board[ te.to ] & 0x10) == 0x10 );
    }

    /* 成れないのに成りにしていないか調べる */
    if ( te.nari ) {
      assert( (bo->board[ te.fm ] & 0x0F) <= 7 );
    }
  }
  else {
    assert( te.uti >= 1 && te.uti <= 7 );
    if ( bo->inhand[ bo->next ][ te.uti ] <= 0 ) {
      printBOARD(bo);
      printf("bad move = %s\n", te_str(&te, *bo).c_str());
      si_abort("cannot put as inhand <= 0");
    }

    assert(te.fm == 0);
    assert( bo->board[ te.to ] == 0 );

    assert(te.nari == 0);
  }
}
#endif // !NDEBUG


/** 
 * 手番を入れ替える. 手数などは変更しない。
 * パスしたいときはこの関数ではなく bo_pass() を呼び出すこと
 * @param bo 対象のBOARD
 */
static void boToggleNext(BOARD* bo)
{
  assert( bo != NULL );
  
  bo->next = (bo->next == 0);
  bo->key = ~ bo->key;
  bo->hash_all = ~ bo->hash_all;
}


#ifdef USE_PIN
/**
 * 玉が動いたときに、相手方からのpinを更新する
 * @param bo 局面
 * @param side pin攻撃を更新する側
 */
static void bo_update_pin(BOARD* bo, int king_side, Xy from, Xy to)
{
  int i;
  
  // pinを外す
  for (i = 0; i < 8; i++) {
    Xy xy = from;
    do {
      xy += arr_round_to[i];
      if (bo->board[xy] == WALL)
        break;
      if (bo->board[xy]) {
        if ( getTeban(bo->board[xy]) == king_side )
          bo->pinned[xy] = 0;
        break;
      }
    } while (true);
  }

  // 新しくpinを付ける
  for (i = 0; i < 8; i++) {
    Xy xy = to;
    do {
      xy += arr_round_to[i];
      if (bo->board[xy] == WALL)
        break;
      if (bo->board[xy]) {
        if ( getTeban(bo->board[xy]) == king_side ) {
          // 先まで延ばす
          for (int j = 0; j < bo->long_attack[1 - king_side][xy].count; j++) {
            Xy attack_from = bo->long_attack[1 - king_side][xy].from[j];
            if ( norm_direc(attack_from, xy) == -arr_round_to[i] )
              bo->pinned[xy] = -arr_round_to[i];
          }
        }
        break;
      }
    } while (true);
  }
}
#endif // USE_PIN


#ifdef NDEBUG
  #define assert_post_move(expr) (static_cast<void>(0))
#else
static void assert_post_move(BOARD* bo, const TE& te)
{
  if ( bo_is_checked(bo, 1 - bo->next) ) {
    printf("internal error: still checked.\n");

    boBack_mate(bo); // 不正な手は戻す
    printBOARD(bo);
    printf("hash = %" PRIx64 ", %" PRIx64 "\n", bo->key, bo->hash_all);
    printf("illegal move = %s\n", te_str(&te, *bo).c_str() );

    BOARD* bo_tmp = bo_dup(bo);
    while (bo_tmp->mi.count > 0)
      boBack_mate(bo_tmp);

    printBOARD(bo_tmp);
    printf("seq = ");
    mi2_print_sequence( &bo->mi, *bo_tmp );
    freeBOARD(bo_tmp);

    abort();
  }
}
#endif // !NDEBUG


/*
 * BOARD上の駒を動かす。
 * @param bo BOARD
 * @param te 移動情報
 */
int boMove( BOARD* bo, const TE& te )
{
  int c;
  PieceKind p = 0;
  int flg_tori;

  // printf("\n%s: in: point = (%d, %d)\n", __func__, bo->point[0], bo->point[1]);

  assert_move(bo, te);
  
  flg_tori = 0;
  int next = bo->next;

  // 手順を記録
  mi2Add( &bo->mi, te ); 

  // 王の安全度の評価を盤面評価に足す (戻す)
  // bo->point[SENTE] += add_oh_safe_grade(bo, SENTE);
  // bo->point[GOTE] += add_oh_safe_grade(bo, GOTE);
  
  if ( !te_is_put(&te) ) {
    /* 駒を移動する */

    p = bo->board[ te.fm ];

    // 移動元.
    // printf("from:%d ", -add_grade(bo, te.fm, next));
    bo->point[next] -= add_grade(bo, te.fm, next );
    remove_piece_with_kiki(bo, te.fm);

    c = bo->board[ te.to ];
    if ( c != 0 ) {
      /* 移動先に駒があった */
      
      // 持ち駒に加える
      inc_inhand(bo, c );
      // printf("inhand:%d ", +score_inhand(bo, c & 7, next));
      bo->point[next] += score_inhand( bo, c & 7, next );

      // 盤上から駒を取り除く
      // printf("remove_to(op):%d ", -add_grade(bo, te.to, (next == 0)));
      bo->point[1 - next] -= add_grade(bo, te.to, (next == 0));
      remove_piece(bo, te.to);

      /* 取られる駒を覚えておく */
      bo->tori[ bo->mi.count - 1 ] = c;
      flg_tori = 1;
    } 
    else {
      bo->tori[ bo->mi.count - 1 ] = 0;
    }

    // 移動先
    if ( te.nari )
      p += 8;

    if (flg_tori)
      put_piece(bo, te.to, p );
    else
      put_piece_with_kiki(bo, te.to, p );
    // printf("to:%d ", add_grade(bo, te.to, next));
    bo->point[next] += add_grade(bo, te.to, next );
  } 
  else {
    // 駒打ちの場合
    
    // 持ち駒を減らす
    // printf("dec_inhand:%d ", -score_inhand(bo, te.uti, next));
    bo->point[next] -= score_inhand( bo, te.uti, next );
    dec_inhand(bo, te.uti);

    // 駒を置く
    put_piece_with_kiki(bo, te.to, te.uti + (next << 4) );
    // printf("put:%d ", add_grade(bo, te.to, next));
    bo->point[next] += add_grade(bo, te.to, next);
    
    bo->tori[ bo->mi.count - 1 ] = 0;
  }

  boToggleNext(bo);
  bo->hash_history[bo->mi.count] = bo->hash_all;

  if ((p & 0xf) == OH) {
    bo->point[0] = grade(bo, SENTE);
    bo->point[1] = grade(bo, GOTE);
#ifdef USE_PIN
    bo_update_pin(bo, 1 - bo->next, te.fm, te.to );
#endif
  }
  else {
    /* 王の安全度の評価を盤面評価から引く */
    // sub_oh_safe_grade(bo, SENTE);
    // sub_oh_safe_grade(bo, GOTE);
  }

  if ( bo_is_checked(bo, bo->next) )
    bo->mi.te[bo->mi.count - 1].hint |= TE_CHECK;

  assert_post_move(bo, te);

  // printf("\n%s: out: point = (%d, %d)\n", __func__, bo->point[0], bo->point[1]);

  return flg_tori;
}


#ifdef NDEBUG
  #define assert_back(expr) (static_cast<void>(0))
#else
static void assert_back(const BOARD* bo)
{
  assert( bo != NULL );
  assert( bo->mi.count > 0 );
}
#endif // !NDEBUG


/**
 * 一手戻す。
 * @param bo 対象のBOARD
 */
void boBack( BOARD* bo ) 
{
  PieceKind p = 0;
  PieceKind cap;

  // printf("\n%s: in: point = (%d, %d)\n", __func__, bo->point[0], bo->point[1]);
  
  assert_back(bo);

  boToggleNext(bo);
  int next = bo->next;

  const TE& te = bo->mi.te[ --bo->mi.count ];
  cap = bo->tori[ bo->mi.count ];

  if (!te.to) // pass手
    return;

  /* 王の安全度の評価を盤面評価に足す */
  // bo->point[SENTE] += add_oh_safe_grade(bo, SENTE);
  // bo->point[GOTE] += add_oh_safe_grade(bo, GOTE);
  
  if ( !te_is_put(&te) ) {
    p = bo->board[ te.to ];

    /* 盤面評価点数から te.to の位置の駒の点数を引く */
    // printf("to:%d ", -add_grade(bo, te.to, next));
    bo->point[next] -= add_grade(bo, te.to, next );

    if (cap) {
      // 駒を取っていた場合
      remove_piece(bo, te.to);

      // 持ち駒から減らす
      // printf("inhand:%d ", -score_inhand(bo, cap & 7, next));
      bo->point[next] -= score_inhand(bo, cap & 7, next);
      dec_inhand(bo, cap);

      // 駒を戻す
      put_piece(bo, te.to, cap);
      // printf("restore(op):%d ", add_grade(bo, te.to, (next == 0)));
      bo->point[1 - next] += add_grade(bo, te.to, (next == 0));
    }
    else
      remove_piece_with_kiki(bo, te.to);

    // 移動元
    if ( te.nari )
      p &= 0x17;
    put_piece_with_kiki(bo, te.fm, p );
    // printf("from:%d ", add_grade(bo, te.fm, next));
    bo->point[next] += add_grade(bo, te.fm, next);
  } 
  else {
    /* 駒打ちだった場合 */

    // 取り除く
    // printf("remove:%d ", -add_grade(bo, te.to, next));
    bo->point[next] -= add_grade(bo, te.to, next);
    remove_piece_with_kiki(bo, te.to);

    // 持ち駒に戻す
    inc_inhand(bo, te.uti );
    // printf("to_inhand:%d ", +score_inhand(bo, te.uti, next));
    bo->point[next] += score_inhand(bo, te.uti, next);
  }
  
  if ( (p & 0xf) == OH ) {
    bo->point[0] = grade(bo, SENTE);
    bo->point[1] = grade(bo, GOTE);
#ifdef USE_PIN
    bo_update_pin(bo, bo->next, te.to, te.fm );
#endif
  }
  else {
    /* 王の安全度の評価を盤面評価から引く */
    // sub_oh_safe_grade(bo, SENTE);
    // sub_oh_safe_grade(bo, GOTE);
  }

  // printf("\n%s: out: point = (%d, %d)\n", __func__, bo->point[0], bo->point[1]);
}


/*
 * BOARD上の駒を動かす。
 * @param bo BOARD
 * @param te 移動情報
 * @param next 手番
 */
void boMove_mate(BOARD* bo, const TE& te)
{
  int c;
  PieceKind p = 0;
  int flg_tori;

  assert_move(bo, te);
  
  flg_tori = 0;

  mi2Add( &bo->mi, te );
  
  if ( !te_is_put(&te) ) {
    /* 駒を移動する */

    p = bo->board[ te.fm ];

    // 移動元
    remove_piece_with_kiki(bo, te.fm);

    c = bo->board[ te.to ];
    if ( c != 0 ) {
      /* 移動先に駒があった */
      
      // 持ち駒に加える
      inc_inhand(bo, c );

      remove_piece(bo, te.to);

      /* 取られる駒を覚えておく */
      bo->tori[ bo->mi.count - 1 ] = c;
      flg_tori = 1;
    } 
    else  {
      bo->tori[ bo->mi.count - 1 ] = 0;
      flg_tori = 0;
    }

    // 移動先    
    if (te.nari)
      p += 8;

    if (flg_tori)
      put_piece(bo, te.to, p );
    else
      put_piece_with_kiki(bo, te.to, p );
  } 
  else {
    /* 駒打ちの場合 */
    dec_inhand(bo, te.uti);
    put_piece_with_kiki(bo, te.to, te.uti + (bo->next << 4) );

    bo->tori[ bo->mi.count - 1 ] = 0;
  }

  boToggleNext(bo);
  bo->hash_history[bo->mi.count] = bo->hash_all;

#ifdef USE_PIN
  if ( (p & 0xf) == OH )
    bo_update_pin(bo, 1 - bo->next, te.fm, te.to );
#endif
  
  if ( bo_is_checked(bo, bo->next) )
    bo->mi.te[bo->mi.count - 1].hint |= TE_CHECK;

#ifdef USE_PIN
#if 1
  // 打ち歩詰めは実際に動かして確認する
  // TODO: 動作確認できたら削除すること
  assert_post_move(bo, te);
#endif
#endif // USE_PIN
}


/**
 * 一手戻す.
 * @param bo 対象のBOARD
 */
void boBack_mate( BOARD* bo )
{
  assert_back(bo);

  const TE& te = bo->mi.te[ --bo->mi.count ];
  PieceKind cap = bo->tori[ bo->mi.count ];
  PieceKind p = 0;
  
  boToggleNext(bo);

  if (!te.to) // pass手
    return;

  if ( !te_is_put(&te) ) {
    p = bo->board[ te.to ];

    if ( cap ) {
      /* 駒を取っていた場合 */

      remove_piece(bo, te.to);

      dec_inhand(bo, cap);
      put_piece(bo, te.to, cap );
    }
    else
      remove_piece_with_kiki(bo, te.to);

    // 移動元
    if ( te.nari ) 
      p &= 0x17;
    put_piece_with_kiki(bo, te.fm, p );
  } 
  else {
    /* 駒打ちだった場合 */
#ifndef NDEBUG
    assert( 1 <= te.uti && te.uti <= 7 );
    assert( bo->board[ te.to ] != 0 );
    assert( bo->board[ te.to ] != WALL );
#endif /* DEBUG */

    remove_piece_with_kiki(bo, te.to);
    inc_inhand(bo, te.uti );
  }

#ifdef USE_PIN
  if ( (p & 0xf) == OH )
    bo_update_pin(bo, bo->next, te.to, te.fm );
#endif
}


/**
 * bo1 と bo2 を比較する。
 *
 * @param bo1 対象のBOARD
 * @param bo2 対象のBOARD
 * @return 等しければ0を、違いがあれば0以外を返す
 */
int boCmp(const BOARD* bo1, const BOARD* bo2) 
{
#ifdef USE_HASH
  if ( bo1->hash_all != bo2->hash_all )
    return 1;
#endif

  if ( memcmp( bo1->board, bo2->board, sizeof( bo1->board ) ) )
    return 1;

  if ( memcmp( bo1->inhand[0], bo2->inhand[0], sizeof( bo1->inhand[0] ) ) )
    return 1;

  if ( memcmp( bo1->inhand[1], bo2->inhand[1], sizeof( bo1->inhand[1] ) ) )
    return 1;

  return 0;
}


static const int piece_max[] = {
  0, 18, 4, 4, 4, 4, 2, 2, 2,
};


/**
 * 使っていない駒を詰将棋用に受け側の持駒としてセットする。
 * @param bo 対象のBOARD
 */
void boSetTsumeShogiPiece(BOARD* bo)
{
  int i;

  boSetToBOARD(bo);

  for ( i = 1; i <= 7; i++ ) {
    bo->inhand[ (bo->next == 0) ][ i ] =
      piece_max[ i ] - bo->pb.count[ i ] - bo->inhand[ bo->next ][ i ];
  }

  boSetToBOARD(bo);
}


/**
 * ルール上正しい１手かチェックする。
 * @param bo 対象のBOARD
 * @param te 調べる一手
 * @return 正しければ true を返す。正しくなければ false を返す。
 */
bool is_valid_move(const BOARD* bo, const TE* te)
{
  MOVEINFO mi;

  make_moves(bo, &mi);
  return mi_includes(&mi, *te);
}


/** パスする */
void bo_pass( BOARD* bo )
{
  TE te;
  te_clear(&te);

  mi2Add( &bo->mi, te );
  bo->tori[bo->mi.count - 1] = 0;

  boToggleNext(bo);
}


/**
 * te の手が成りが可能か調べる。
 * @param bo 対象のBOARD
 * @param te 対象のTE
 * @return 成れないときは 0, どちらも可能 1, 成るしかない 2
 */
int can_move_promote(const BOARD* bo, const TE* te) 
{
  PieceKind p;
  int next;
  int fm ,to;

  assert(bo != NULL);
  assert(te != NULL);

  if ( te_is_put(te) )
    return 0;

  assert( te->fm >= 0x11 && te->fm <= 0x99 );
  assert( bo->board[te->fm] != 0 );

  p = boGetPiece(bo, te->fm);
  next = getTeban(p);

  p &= 0x0F;

  /* 王と金は成れない。戻る */
  if (p == OH || p == KIN)
    return 0;

  /* もう成っている駒はなれない。戻る */
  if ((p & 0x08) == 0x08)
    return 0;

  fm = (te->fm >> 4);
  to = (te->to >> 4);

  if ( next == SENTE ) {
    if ( (p == FU || p == KYO) && to <= 1 )
      return 2;
    else if ( p == KEI && to <= 2 )
      return 2;
    else if (fm <= 3 || to <= 3)
      return 1;
  }
  else {
    if ( (p == FU || p == KYO) && 9 <= to )
      return 2;
    else if ( p == KEI && 8 <= to )
      return 2;
    else if (7 <= fm || 7 <= to)
      return 1;
  }

  return 0;
}


/**
 * 千日手 (同一局面4回) になる手か？
 * @param bo 現在の局面
 * @param te これから指そうとする手
 * @param forbidden 連続王手の千日手のときtrueに出力する
 *
 * @return 千日手のときtrue
 */
bool is_move_sennichite(const BOARD* bo, const TE* te, bool* forbidden)
{
  BOARD* tmp_bo = bo_dup(bo);
  boMove_mate(tmp_bo, *te);
  uint64_t hash = tmp_bo->hash_all;
  if ( count(tmp_bo->hash_history,
             tmp_bo->hash_history + tmp_bo->mi.count + 1, hash) >= 4 ) {
    // 連続王手の千日手かどうか

    // http://www.shogi.or.jp/faq/
    // 全部の手が王手のとき以外は、通常の千日手扱い

    freeBOARD(tmp_bo);
    return true;
  }

  freeBOARD(tmp_bo);
  return false;
}


/**
 * 現在の時刻を記録する。
 * (思考時間の測定用)
 * @param bo 対象のBOARD
 */
void boSetNowTime(BOARD *bo) {
  bo->before_think_time = time(NULL);
}


/**
 * 消費した時間を記録する。
 * @param bo 対象のBOARD
 */ 
void boSetUsedTime(BOARD *bo) {
  time_t used_time;
  
  used_time = time(NULL) - bo->before_think_time;
#ifdef DEBUG
  assert( 0 <= used_time );
#endif /* DEBUG */
  if ( used_time <= 0 ) {
    used_time = 1;
  }
  
  bo->total_time[bo->next] += used_time;
  
  bo->used_time[bo->mi.count] = used_time;
}


static int bo_is_correct_mate(const BOARD* bo, Xy* oh_xy)
{
  int piece[9]; // 駒種ごとの駒の数
  Xy xy;
  PieceKind p;
  int i;
  int x, y, flg;

  assert(bo != NULL);

  oh_xy[0] = 0;
  oh_xy[1] = 0;
  for (i = 0; i < 9; i++) {
    piece[i] = 0;
  }

  for (y = 1; y <= 9; y++) {
    for (x = 1; x <= 9; x++) {
      xy = (y << 4) + x;

      p = boGetPiece(bo, xy);
      if (p == 0)
	continue;

      // 動かせない駒はないか
      if ( (p == FU && y <= 1) ||
	   (p == KYO && y <= 1) ||
	   (p == KEI && y <= 2) ) {
	printf("cannot move\n");
	return 0;
      }
      if ( (p == FU + 0x10 && y >= 9) ||
	   (p == KYO + 0x10 && y >= 9) ||
	   (p == KEI + 0x10 && y >= 8) ) {
	printf("cannot move\n");
	return 0;
      }

      // 玉は高々1枚ずつ
      if (p == OH) {
	if (oh_xy[0]) {
	  printf("king only 1\n");
	  return 0;
	}
	oh_xy[0] = xy;
	piece[8]++;
      }
      else if (p == OH + 0x10) {
	if (oh_xy[1]) {
	  printf("king only 1\n");
	  return 0;
	}
	oh_xy[1] = xy;
	piece[8]++;
      }
      else
	piece[p & 7]++;
    }
  }

  // 二歩
  for (x=1; x<=9; x++) {
    flg = 0;
    for (y=1; y<=9; y++) {
      p = boGetPiece(bo, (y << 4) + x);
      if (p == FU) {
	if (flg == 1) {
	  printf("nifu\n");
	  return 0;
	}
	flg = 1;
      }
    }
  }

  for (x=1; x<=9; x++) {
    flg = 0;
    for (y=1; y<=9; y++) {
      p = boGetPiece(bo, (y << 4) + x);
      if (p == FU + 0x10) {
	if (flg == 1) {
	  printf("nifu\n");
	  return 0;
	}
	flg = 1;
      }
    }
  }

  /* 持ち駒の数を調べる */
  for (i=1; i<=7; i++) {
    piece[i] += bo->inhand[0][i];
    piece[i] += bo->inhand[1][i];
  }

  /* 駒全部の数を数える */
  for (i = 1; i <= 8; i++) {
    if (piece_max[i] < piece[i]) {
      printf("pieces too many\n");
      return 0;
    }
  }
/*
  // 受け玉
  if (bo->next == SENTE) {
    if (oh_xy[1] == 0) {
      printf("uke-king nothing\n");
      return 0;
    }
  } 
  else {
    if (oh_xy[0] == 0) {
      printf("uke-king nothing\n");
      return 0;
    }
  }
*/  
  if (bo->next == SENTE) {
    // 後手に王手が掛かっていてはいけない
    if ( bo_is_checked(bo, GOTE) ) {
      printf("king can capture\n");
      return 0;
    }
  }
  else {
    if ( bo_is_checked(bo, SENTE) ) {
      printf("king can capture\n");
      return 0;
    }
  }

  return 1;
}


/**
 * 対局用に正当な盤面か調べる。
 * @param bo 対象のBOARD
 * @return 1 ならば正当。0 ならば不正。
 */
int boCheckJustGame(const BOARD* bo)
{
  Xy oh_xy[2];
  if ( !bo_is_correct_mate(bo, oh_xy) )
    return 0;

  if (oh_xy[0] == 0)
    return 0;

  if (oh_xy[1] == 0)
    return 0;

  return 1;
}


/**
 * 詰将棋用に正当な盤面か調べる。
 * @param bo 対象のBOARD
 * @return 1 ならば正当。0 ならば不正。
 */
int boCheckJustMate(const BOARD* bo)
{
  Xy oh_xy[2];
  return bo_is_correct_mate(bo, oh_xy);
}


extern const char* CSA_PIECE_NAME[];

/** 盤面をコンソールに出力 */
void printBOARD(const BOARD* brd)
{
  int x, y;

  assert( brd != NULL );

  for (y = 1; y <= 9; y++) {
    printf("P%d", y);
    for (x = 9; x >= 1; x--) {
      PieceKind p = boGetPiece(brd, (y << 4) + x);
      if (!p)
        printf(" * ");
      else
        printf("%c%s", (getTeban(p) == 0 ? '+' : '-'), CSA_PIECE_NAME[p & 0xf]);
    }
    printf("\n");
  }

  // 持ち駒
  int i, side;
  for (side = 0; side < 2; side++) {
    if ( *max_element(brd->inhand[side] + 1, brd->inhand[side] + 8) > 0) {
      // 読みやすいように
      printf("'%c ", side == 0 ? '+' : '-');
      for (x = 1; x <= 7; x++) {
        if ( brd->inhand[side][x] )
          printf("%s%d ", CSA_PIECE_NAME[x], brd->inhand[side][x]);
      }
      printf("\n");

      // CSA形式
      printf("P%c", side == 0 ? '+' : '-');
      for (x = 1; x <= 7; x++) {
        for (i = 0; i < brd->inhand[ side ][x]; i++)
          printf("00%s", CSA_PIECE_NAME[x]);
      }
      printf("\n");
    }
  }
  printf( "%c\n", brd->next == 0 ? '+' : '-' );
  printf( "'key = %" PRIx64 ", all = %" PRIx64 "\n", brd->key, brd->hash_all );
}

