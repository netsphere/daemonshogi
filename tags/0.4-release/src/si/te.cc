/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"
#include "ui.h"
#include <string>
using namespace std;


/**
 * csa shogi の座標を si の座標に変換するテーブル。
 */ 
static const int csa2si_table[] = {
  0,
  0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
  0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
  0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
  0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49,
  0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59,
  0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69,
  0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79,
  0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
  0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99,
};

/**
 * si の座標を csa shogi の座標に変換するテーブル。
 */ 
static const int si2csa_table[] = {
   0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,
   0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 0, 0, 0, 0, 0, 0,
   0, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0, 0, 0, 0, 0, 0,
   0, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 0, 0, 0, 0, 0,
   0, 28, 29, 30, 31, 32, 33, 34, 35, 36, 0, 0, 0, 0, 0, 0,
   0, 37, 38, 39, 40, 41, 42, 43, 44, 45, 0, 0, 0, 0, 0, 0,
   0, 46, 47, 48, 49, 50, 51, 52, 53, 54, 0, 0, 0, 0, 0, 0,
   0, 55, 56, 57, 58, 59, 60, 61, 62, 63, 0, 0, 0, 0, 0, 0,
   0, 64, 65, 66, 67, 68, 69, 70, 71, 72, 0, 0, 0, 0, 0, 0,
   0, 73, 74, 75, 76, 77, 78, 79, 80, 81, 0, 0, 0, 0, 0, 0,
};


/**
 * 初期化付きでTEを生成する。
 * @param from 移動元の座標
 * @param to 移動先のX座標
 * @param promote 成りフラグ。0 なら成らず、1 なら成り。
 *
 * @return TEインスタンス
 */
TE te_make_move(Xy from, Xy to, bool promote)
{
  TE te;

  assert( from >= 0x11 && from <= 0x99 );
  assert( to >= 0x11 && to <= 0x99 );

  te.fm = from;
  te.to = to;
  te.nari = promote;
  te.uti = 0;
  te.hint = 0;

  return te;
}


/** 打つ手を生成 */
TE te_make_put(Xy to, PieceKind pie)
{
  TE te;

  assert( to >= 0x11 && to <= 0x99 );
  assert( pie >= 1 && pie <= 7 );

  te.fm = 0;
  te.to = to;
  te.nari = 0;
  te.uti = pie;
  te.hint = 0;

  return te;
}


const char* CSA_PIECE_NAME[] = {
  "", 
  "FU", "KY", "KE", "GI", "KI", "KA", "HI", "OU", 
  "TO", "NY", "NK", "NG", "",   "UM", "RY", "",
};


/**
 * デバッグ用の表現。
 * @param bo 動かす前の局面
 * @param te 指し手
 */
string te_str( const TE* te, const BOARD& bo )
{
  assert(te);

  char buf[100];

  if ( !te->to )
    return "%TORYO";
  else if ( te_is_put(te) ) {
    sprintf(buf, "%d%d%s*", 
	    te->to & 0xf, te->to >> 4, CSA_PIECE_NAME[te->uti]);
  }
  else {
    sprintf(buf, "%d%d%s%s(%d%d)",
	    te->to & 0xf, te->to >> 4, 
	    CSA_PIECE_NAME[bo.board[te->fm] & 0xf],
	    (te->nari ? "+" : ""),
	    te->fm & 0xf, te->fm >> 4);
  }

  return string(buf);
}


/**
 * 手をセットする。
 * @param te 手を格納する te
 * @param fm 移動元
 * @param to 移動先
 * @param nari 成り
 * @param uti 打ちの場合駒種類
 */
void teSetTE(TE* te, Xy fm, Xy to, bool nari, PieceKind uti)
{
#ifdef DEBUG
  assert(te != NULL);
  assert(0 <= fm && fm <= 10);
  assert(0 <= to && to <= 10);
  assert(nari == 0 || nari == 1);
  assert(0 <= uti && uti <= 7);
#endif /* DEBUG */

  te->fm = fm;
  te->to = to;
  te->nari = nari;
  te->uti = uti;
}


/**
 * te の移動元を CSA shogi の座標に変換して返す。
 * @param te 
 * @return CSAshogi の移動元の座標を返す。
 */ 
int teCSAimoto(TE *te)
{
  if ( te_is_put(te) ) {
    /* 駒打ちの場合 */
    return te->uti + 100;
  }
  return si2csa_table[te->fm];
}

/**
 * te の移動先を CSA shogi の座標に変換して返す。
 * @param te 
 * @return CSAshogi の移動先の座標を返す。
 */
int teCSAisaki(TE *te) {
  return si2csa_table[te->to];
}

/**
 * te が成りの場合は 1 を返す。
 * それ以外は 0 を返す。
 * すでに成っている駒の移動は 0 を返す。
 * @param te 
 * @return 成りならば 0 を返す。
 */
int teCSAinaru(TE *te) {
  return te->nari;
}

/**
 * imoto, isaki の内容を解析して te をセットする。
 * @param te 対象のTE
 * @param imoto CSAshogi の移動元
 * @param isaki CSAshogi の移動先
 */
void teCSASet(TE *te, int imoto, int isaki) {
  if (101 <= imoto) {
    /* 駒打ちの場合 */
    te->uti = imoto - 100;
    te->to = csa2si_table[isaki];
    te->fm = 0;
    te->nari = 0;
  } else {
    te->uti = 0;
    te->fm = csa2si_table[imoto];
    if (100 < isaki) {
      /* 成りの場合 */
      te->nari = 1;
      isaki -= 100;
    } else {
      te->nari = 0;
    }
    te->to = csa2si_table[isaki];
  }
}


void printTE(const TE* te) 
{
  assert( te != NULL );
  assert( 0 <= te->uti && te->uti <= 7 );
  
  if ( te_is_put(te) ) {
    printf("%d%d%s*\n",
	   (te->to)&0x0f, (te->to)>>4, CSA_PIECE_NAME[te->uti]);
  } else {
    printf("(%d,%d)->(%d,%d)%s\n",
	   (te->fm)&0x0f, (te->fm)>>4,
	   (te->to)&0x0f, (te->to)>>4,
	   te->nari ? "nari" : "");
  }
}


std::ostream& operator << (std::ostream& ost, const TE& te)
{
  char buf[100];

  if ( te_is_put(&te) ) {
    sprintf(buf, "%d%d%s*",
            te.to &0x0f, te.to >> 4, CSA_PIECE_NAME[te.uti]);
  }
  else {
    sprintf(buf, "%d%d(%d%d)%s", 
            te.to & 0x0f, te.to >> 4,
            te.fm & 0x0f, te.fm >> 4,
            te.nari ? "+" : "");
  }

  ost << buf;
  return ost;
}
