/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"

/**
 * 新しくメモリを確保してPBOXのポインタを返す。
 * @return PBOXへのポインタ
 */
PBOX *newPBOX(void) {
  PBOX *b;

  b = (PBOX *) malloc(sizeof(PBOX));
  if (b == NULL) {
    si_abort("No enough memory. In pbox.c#newPBOX()");
  }
  pbInit(b);

  return b;
}

/**
 * pb のメモリを解放する。
 * @param pb 対象のPBOX
 */
void freePBOX(PBOX *pb) {
#ifdef DEBUG
  assert( pb != NULL );
#endif /* DEBUG */
  
  free( pb );
}


/**
 * pb の内容を初期化する。使われていない駒としてPBOXへセットする。
 * @param pb 対象のPBOX
 */
void pbInit(PBOX* pb) 
{
  int i;
  
  assert( pb != NULL );
  
  for (i = 0; i <= 8; i++)
    pb->count[i] = 0; // 全部駒箱にある
}


/**
 * 駒箱に駒を戻す。
 * @param pb 対象のPBOX
 * @param p 駒種
 */
void pbPush(PBOX* pb, PieceKind p )
{
  assert( pb != NULL );
  assert( p >= 1 && p <= 0x1f );

  if ( p == OH || p == OH + 0x10 ) {
    assert( pb->count[8] > 0 );
    pb->count[8]--;
    return;
  }

  assert( pb->count[p & 7] > 0 );
  pb->count[p & 7]--;

#ifdef DEBUG  
  /* 戻しすぎじゃないかチェック */
  if (23 <= n) {                   /* fu */
    assert(0 <= pb->count[1]-1);
  } else if (19 <= n) {            /* kyo */
    assert(0 <= pb->count[2]-1);
  } else if (15 <= n) {            /* kei */
    assert(0 <= pb->count[3]-1);
  } else if (11 <= n) {            /* gin */
    assert(0 <= pb->count[4]-1);
  } else if (7 <= n) {             /* kin */
    assert(0 <= pb->count[5]-1);
  } else if (5 <= n) {             /* kaku */
    assert(0 <= pb->count[6]-1);
  } else if (3 <= n) {             /* hisya */
    assert(0 <= pb->count[7]-1);
  } else if ( n == 1 || n == 2 ) { /* oh */
    assert(0 <= pb->count[8]-1);
  }
#endif /* DEBUG */
}


/**
 * 駒箱から駒を取り出す。
 * @param pb 対象の駒コード
 * @param p 駒種類
 */
void pbPop(PBOX* pb, PieceKind p)
{
  assert( pb != NULL );
  assert( p >= 1 && p <= 0x1f );

  /* oh */
  if (p == 0x08) {
    assert( pb->count[8] < 2 );
    pb->count[ 8 ]++;
    return; //  pb->number[ 8 ][ 0 ];
  }
  else if (p == 0x18) {
    assert( pb->count[8] < 2 );
    pb->count[ 8 ]++;
    return; //  pb->number[ 8 ][ 1 ];
  }

  pb->count[p & 7]++;
}


/* 駒が多すぎないか調べる
 * OK : 0を返す
 * NG : 1を返す
 */
int pbCheck(PBOX * pb) {
  if ( 18 < pb->count[1] ) {
    return 1;
  } else if ( 4 < pb->count[2] ) {
    return 1;
  } else if ( 4 < pb->count[3] ) {
    return 1;
  } else if ( 4 < pb->count[4] ) {
    return 1;
  } else if ( 4 < pb->count[5] ) {
    return 1;
  } else if ( 2 < pb->count[6] ) {
    return 1;
  } else if ( 2 < pb->count[7] ) {
    return 1;
  } else if ( 2 < pb->count[8] ) {
    return 1;
  }
  
  return 0;
}
