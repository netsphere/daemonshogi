/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"
#include "si-think.h"
#include "ui.h"
#include "taboo_list.h"

#define DEBUGLEVEL 0
#define DEBUG_TESUKI_LEVEL 0

#if DEBUGLEVEL >= 1
#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>
#endif
using namespace std;


/** 
 * タブーリストと置換表の両方から探す
 * @return 見つかったときtrue 
 */
static bool mate_lookup_hash( const TabooList<PhiDelta>* taboo_list, 
                              int tag, const BOARD* bo, int const te_count,
			      TE* best_move,
                              int32_t* phi, int32_t* delta )
{
  PhiDelta ent;
  if ( taboo_list->lookup(bo, &ent) ) {
    *phi = ent.phi; *delta = ent.delta;
    te_clear(best_move);

#if DEBUGLEVEL >= 4
    printf("taboo hit! k=%" PRIx64 "\n", bo->hash_all);

    BOARD* tmp_bo = bo_dup(bo);
    while (tmp_bo->mi.count > 0) {
      boBack_mate(tmp_bo);
      if (tmp_bo->hash_all == bo->hash_all)
	break;
    }
    printBOARD(tmp_bo);
    printf("seq = ");
    while (tmp_bo->mi.count < bo->mi.count) {
      printf( "%s ", te_str(&tmp_bo->mi.te[tmp_bo->mi.count], *tmp_bo).c_str() );
      boMove_mate( tmp_bo, tmp_bo->mi.te[tmp_bo->mi.count] );
    }
    printf("\n");
    freeBOARD(tmp_bo);
#endif

    return true;
  }

  if ( hsGetBOARD(tag, bo, phi, delta, best_move, te_count) )
    return true;

  *phi = 1;  // TODO: ありそうな局面はよい (低い) 値
  *delta = 1;
  return false;
}


static int32_t sum_step(int32_t x, int32_t y) __attribute__((const));

static int32_t sum_step(int32_t x, int32_t y)
{
  if (x >= VAL_INF || y >= VAL_INF)
    return VAL_INF;
  else if (x >= VAL_MAY_MATCH || y >= VAL_MAY_MATCH)
    return VAL_MAY_MATCH;
  else {
    return min(x + y, VAL_MAY_MATCH - 1);
  }
}


struct EndingSearchParam {
  int offense_side;
  int te_count;
  int start_depth;
  int start_tesuki;
};


static int tt_tag(const EndingSearchParam& param)
{
  return param.start_tesuki * 2 + param.offense_side;
}


extern int think_count_attack24(const BOARD* bo, int side, Xy king);
extern int think_count_king_movables(const BOARD* bo, int side);


/** 攻め方の無駄打ちを置換表に登録 */
static void set_offense_muda_uchi( const EndingSearchParam& param,
                                   BOARD* node, Xy to )
{
  assert( node->next == param.offense_side );

  MOVEINFO mi;
  mi.count = 0;
  mi_append_put_moves(node, to, &mi);
  for (int i = 0; i < mi.count; i++) {
    boMove_mate(node, mi.te[i]);
    hsPutBOARD( tt_tag(param), node, 0, +VAL_INF, param.te_count, NULL );
    boBack_mate(node);
  }
}


/**
 * 王手/詰めろのときだけ手を指す
 * heuristics で手を選別
 * @return 攻め方の手として正しくないときfalse
 */
static bool offense_move( const TabooList<PhiDelta>* taboo_list, 
			  const EndingSearchParam& param, 
			  BOARD* node, 
			  TE& te,
			  int32_t* child_phi, int32_t* child_delta )
{
  assert(node->next == param.offense_side);

  if ( (te.hint & TE_STUPID) != 0 )
    return false;

  bool attacked;
  int ac;

  if (param.start_tesuki == 1) {
    attacked = bo_is_checked(node, param.offense_side);
    ac = think_count_attack24(node, param.offense_side,
                              node->king_xy[1 - param.offense_side]);
  }

  boMove_mate( node, te );
  
  TE tt_move;
  bool r = mate_lookup_hash(taboo_list, tt_tag(param), node, param.te_count,
			    &tt_move,
                            child_phi, child_delta);
  if (r) {
    if (te == tt_move)
      te.hint = tt_move.hint;
    return true;
  }

  // 常にstart_tesuki == tesuki
  if ( !param.start_tesuki ) {
#ifndef NDEBUG
    if ( !bo_is_checked(node, 1 - param.offense_side) ) {
      printBOARD(node);
      boBack_mate(node);
      si_abort("check error: move = %s\n", te_str(&te, *node).c_str() );
    }
#endif // !NDEBUG

    MOVEINFO mi_uke;
    *child_delta = uke(node, &mi_uke );

    if (*child_delta == 0) {
      *child_phi = +VAL_INF;
      return true;
    }

    // 攻め手が打ちで、受け手に取り返す手があって、攻め手が続かない
    // TODO: 攻め方の持ち駒が大量にある場合は続くことがある
    if ( te_is_put(&te) &&
         bo_attack_count(node, 1 - param.offense_side, te.to) ) {
      // 取ってみる
      for (int i = 0; i < mi_uke.count; i++) {
        if ( mi_uke.te[i].to != te.to )
          continue;
        boMove_mate(node, mi_uke.te[i]);
        if ( think_count_attack24(node, param.offense_side,
                              node->king_xy[1 - param.offense_side]) == 0 ) {
          // この手はダメ
          boBack_mate(node); // 取る手
          boBack_mate(node); // 攻め方の打つ手

          // ほかもダメ
          set_offense_muda_uchi( param, node, te.to );

          return false;
        }
        boBack_mate(node);
      }
    }

    return true;
  }
  else if ( param.start_tesuki == 1 ) {
    // 王手はいつでもOK
    if ( bo_is_checked(node, 1 - param.offense_side) )
      return true;

    // 見込みのありそうな手か::
    bool promise = false;

    // - 逆王手の受け
    if (attacked)
      promise = true;

    // - 取る手
    if (node->tori[node->mi.count - 1])
      promise = true;

    // - 利きを入れる手
    else if (think_count_attack24(node, param.offense_side,
                                  node->king_xy[1 - param.offense_side]) > ac)
      promise = true;

    // ... ほかにもあれば
    
    if (!promise) {
      hsPutBOARD(tt_tag(param), node, 0, +VAL_INF, param.te_count, NULL);
      boBack_mate(node);
      return false;
    }

    // 実際に詰めろが掛かるか
    bo_pass(node); // 受け側

    TE te_ret;
    int node_limit = GAME_MATE_NODE_LIMIT;
    if ( dfpn_mate(node, 31, &te_ret, &node_limit) == CHECK_MATE ) {
      boBack_mate(node); // パスだけ戻す
      return true;
    }
    else {
      boBack_mate(node);

      assert( node->next != param.offense_side );
      hsPutBOARD( tt_tag(param), node, 0, +VAL_INF, param.te_count, NULL );

      boBack_mate(node);
      return false;
    }
  }
  else { // tesuki > 1
    if (attacked)
      return true;
    assert(0);
  }
}


struct MatePutWin {
  const EndingSearchParam& param;
  MatePutWin(const EndingSearchParam& param_): param(param_) {}
  void operator () (const BOARD* bo, const TE* move) const {
    hsPutBOARD( tt_tag(param), bo, 0, +VAL_INF, param.te_count, move);
  }
};

void TsumeroNogare::before_move(const BOARD* node, int offense_side_) 
{
    offense_side = offense_side_;

    // 自玉の周囲への攻め方の数
    ac = think_count_attack24(node, offense_side,
                              node->king_xy[1 - offense_side]);
    // 自玉の周囲への受け方の数
    mc = think_count_attack24( node, 1 - offense_side,
                               node->king_xy[1 - offense_side] );

    // 自玉の広さ
    mkm = think_count_king_movables( node, 1 - offense_side );
}


/** 見込みのありそうな手か */
bool TsumeroNogare::promise(const BOARD* node) const 
{
    bool r = false;

    // - 取る手
    if ( node->tori[node->mi.count - 1] )
      r = true;

    // - 攻め方の利きを払う手
    else if ( think_count_attack24(node, offense_side,
                                 node->king_xy[1 - offense_side]) < ac )
      r = true;

    // - 受けを足す手
    else if ( think_count_attack24(node, 1 - offense_side,
                                   node->king_xy[1 - offense_side]) > mc )
      r = true;

    // - 玉を広くする手
    else if ( think_count_king_movables(node, 1 - offense_side) > mkm )
      r = true;

    // - 玉が動く手
    else if ( (node->board[node->mi.te[node->mi.count - 1].to] & 0xf) == OH )
      r = true;

    return r;
}


static bool defense_move( const TabooList<PhiDelta>* taboo_list,
			  const EndingSearchParam& param,
			  BOARD* node,
                          MOVEINFO* const mi, int index,
			  int32_t* child_phi, int32_t* child_delta )
{
  assert( node->next == 1 - param.offense_side );

  if ( (mi->te[index].hint & TE_STUPID) != 0 )
    return false;

  bool is_checked = bo_is_checked(node, node->next);
  TsumeroNogare tsumero_nogare;

  if ( !is_checked )
    tsumero_nogare.before_move(node, param.offense_side);

  boMove_mate(node, mi->te[index]); // 受け側の手

  TE tt_move;
  bool r = mate_lookup_hash(taboo_list, tt_tag(param), node, param.te_count,
			    &tt_move,
			    child_phi, child_delta);
  if (r) {
    if (mi->te[index] == tt_move)
      mi->te[index].hint = tt_move.hint;
    return true;
  }

#ifndef NDEBUG
  if ( param.start_tesuki == 0 )
    assert( is_checked );
#endif // !NDEBUG

  if ( is_checked ) {
    MOVEINFO mi_dmy;

    ohte(node, &mi_dmy);
    *child_delta = mi_dmy.count;
    if (*child_delta == 0)
      *child_phi = +VAL_INF;

    return true;
  }
  else {
    // 攻め側の指し手が手スキ (詰めろ) だった

    // 見込みのなさそうなのは全て刈る
    if ( !tsumero_nogare.promise(node) ) {
      hsPutBOARD(tt_tag(param), node, 0, +VAL_INF, param.te_count, NULL);
      boBack_mate(node);
      return false;
    }

    // 本当に受けになっているか
    TE te_ret;
    int node_limit = GAME_MATE_NODE_LIMIT;
    int mr = bfs_hisshi(node, param.start_tesuki - 1, 31, &te_ret, 
			&node_limit);
    if ( mr == CHECK_MATE ) {
      // 受けになっていない. 負け
      *child_phi = 0; *child_delta = +VAL_INF;
      hsPutBOARD( tt_tag(param), node, 0, +VAL_INF, param.te_count, NULL );

      if ( te_is_put(&mi->te[index]) ) {
	// ほかの打つ手も受けになっていない？
	mate_set_muda_uchi(param.offense_side, param.start_tesuki,
			   MatePutWin(param), node, mi, index + 1);
      }
    }
  }

  return true;
}


/**
 * 手を展開し、最善手を選ぶ.
 * 子ノードのうち最も pn が小さいものへの指し手が最善手.
 * node が攻め方の場合は、子ノードのdelta 最小を選ぶ
 * 勝ち = (0, +VAL_INF), 負け = (+VAL_INF, 0)
 * 一通り展開するので、move ordering は効果なし
 */
static void expand_and_best_move( const TabooList<PhiDelta>* taboo_list,
                                  const EndingSearchParam& param,
                                  BOARD* node,
                                  int rest_depth, int rest_node,
                                  PhiDelta* best_child,
                                  int32_t* child_phi_sum,
                                  int32_t* second_child_delta,
                                  TE* best_move )
{
  // 手を展開する
  MOVEINFO mi;

  if (param.start_tesuki == 0) { // 必ずstart_tesuki == tesuki
    if (node->next == param.offense_side)
      ohte(node, &mi);
    else
      uke(node, &mi);
  }
  else {
    make_moves(node, &mi);
  }

  best_child->delta = +VAL_INF;
  *child_phi_sum = 0;

  if (!mi.count) {
    te_clear(best_move);
    return;
  }

  if ( node->next != param.offense_side && 
       (rest_depth <= 0 || rest_node <= 0) ) {
    // 逃げ切った
#if DEBUGLEVEL >= 3
    if (param.start_tesuki >= DEBUG_TESUKI_LEVEL)
      printf("%d: unreach win.\n", rest_depth);
#endif
    best_child->delta = 0;
    *child_phi_sum = +VAL_MAY_MATCH;
    *best_move = mi.te[0];
    return;
  }

  *second_child_delta = +VAL_INF;
  const TE* may_best = mi.te; // 手がある限り何か選ぶ

#if DEBUGLEVEL >= 3
  if (param.start_tesuki >= DEBUG_TESUKI_LEVEL)
    printf("%d: ", rest_depth);
#endif

  for (int i = 0; i < mi.count; i++) {
    int32_t child_phi, child_delta;

    if (node->next == param.offense_side) {
      if ( !offense_move(taboo_list, param, node, mi.te[i], 
			 &child_phi, &child_delta) ) {
	continue;
      }
    }
    else {
      if ( !defense_move(taboo_list, param, node, &mi, i,
			 &child_phi, &child_delta) ) {
	continue;
      }
    }

    if (child_delta < best_child->delta) {
      // 最善手を更新
      *second_child_delta = best_child->delta;
      best_child->phi = child_phi;
      best_child->delta = child_delta;
      may_best = &mi.te[i];
    }
    else if (child_delta < *second_child_delta) {
      // 2位を更新
      *second_child_delta = child_delta;
    }

    *child_phi_sum = sum_step(*child_phi_sum, child_phi);

    boBack_mate(node);
#if DEBUGLEVEL >= 3
    if (param.start_tesuki >= DEBUG_TESUKI_LEVEL) {
      printf("%s=(%d,%d), ", te_str(&mi.te[i], *node).c_str(), 
	     child_phi, child_delta);
    }
#endif

    if (child_phi >= +VAL_MAY_MATCH)
      break; // nodeの手番側の勝ち確定
  }

  *best_move = *may_best;

#if DEBUGLEVEL >= 3
  if (param.start_tesuki >= DEBUG_TESUKI_LEVEL)
    printf( "best = %s\n", te_str(may_best, *node).c_str() );
#endif
}


/**
 * 無駄合いを負けとして置換表に登録
 */
static void set_muda_ai(int tag, const BOARD* node, int te_count)
{
  assert( node->next != (tag % 2) );

  BOARD* tmp_bo = bo_dup(node);

  boBack_mate( tmp_bo ); // 取る手を戻す
  boBack_mate( tmp_bo ); // 打つ手を戻す

  // すべての打つ手は負け
  MOVEINFO pm;
  pm.count = 0;
  mi_append_put_moves( tmp_bo, node->mi.te[node->mi.count - 2].to, &pm );
  for (int i = 0; i < pm.count; i++) {
    boMove_mate( tmp_bo, pm.te[i] );
    assert( tmp_bo->next == (tag % 2) );
    hsPutBOARD( tag, tmp_bo, 0, +VAL_INF, te_count, 
                &node->mi.te[node->mi.count - 1] );
    boBack_mate( tmp_bo );
  }
  freeBOARD( tmp_bo );
}


#ifdef USE_GUI
extern GAME g_game;
#endif // USE_GUI


/**
 * 勝ちを探す.
 * tesuki = 0 のとき詰みを、tesuki = 1 のとき必至 (1手スキ) を、以下同様
 * 1手スキ... 攻め方は詰めろまたは王手のみ.
 */
static void bfs_hisshi_search( TabooList<PhiDelta>* taboo_list,
                               const EndingSearchParam& param,
                               BOARD* node,
                               int tesuki,
                               const PhiDelta& threshold,
                               int depth,
                               int* node_limit )
{
  assert( node->mi.count == param.te_count + (param.start_depth - depth) );

#if DEBUGLEVEL >= 3
  if (param.start_tesuki >= DEBUG_TESUKI_LEVEL) {
    printf("%s: enter: depth=%d, tesuki=%d, (%d, %d), %d\n",
	   __func__, depth, tesuki, threshold.phi, threshold.delta, 
	   *node_limit);
  }
#endif

  // 最初に置換表を確認するのは不味い -> 必ず一度はノードを展開する

  TE best_move, prev_move;
  te_clear(&best_move);
  te_clear(&prev_move);

#ifdef USE_GUI
  if ( !(*node_limit % 700) ) {
    processing_pending();
  }
#endif // USE_GUI

  if (node->next == param.offense_side && param.start_tesuki > 0) {
    // 下位の置換表を利用する
    if ( tesuki < param.start_tesuki ) {
      assert( tesuki == param.start_tesuki - 1 );

      TE te_ret;
      int r = bfs_hisshi(node, tesuki, 31, &te_ret, node_limit);
      if (r == CHECK_MATE)
        hsPutBOARD( tt_tag(param), node, 0, +VAL_INF, param.te_count, &te_ret );
      else {
        hsPutBOARD( tt_tag(param), node,
                    (r == NOT_CHECK_MATE ? +VAL_INF : +VAL_MAY_MATCH), 0,
                    param.te_count, &te_ret );
      }

      return;
    }
  }
  // 受け方は、すべてが詰む（受けなし）か確認するため、手当て不要.

  // サイクル回避
  taboo_list->add( node, threshold );

  PhiDelta best_child;
  int32_t child_phi_sum;

  while (true) {
    int32_t second_child_delta;

    te_clear(&best_move);

    // ノードを展開して最善手を得る
    expand_and_best_move( taboo_list, param, node,
                          depth, *node_limit,
                          &best_child,
                          &child_phi_sum, &second_child_delta,
                          &best_move );
    // しきい値を超えた？
    if ( best_child.delta >= threshold.phi || child_phi_sum >= threshold.delta )
      break;

    assert( best_move.to ); // 候補手が一つでもあれば必ず設定される

#ifdef USE_GUI
    if ( gmGetGameStatus(&g_game) == SI_CHUDAN )
      break;
#endif // USE_GUI

    if (best_move == prev_move) {
#if 0
      printf( "%s: warning: search loop??? depth=%d, threshold=(%d,%d), best_child_delta=%d, child_phi_sum=%d\n",
              __func__, depth, threshold.phi, threshold.delta, best_child.delta,
              child_phi_sum ); // DEBUG
#endif // 0
    }

#if DEBUGLEVEL >= 3
    if (param.start_tesuki >= DEBUG_TESUKI_LEVEL) {
      printf( "move %s (%d, %d)\n",
              te_str(&best_move, *node).c_str(), best_child.delta, 
              child_phi_sum );
    }
#endif

    // 一番見込みのありそうな手で探索範囲を広げる
    PhiDelta threshold_child;
    int n = 0;

    // このしきい値を超える -> delta cutが起こる
    threshold_child.phi = threshold.delta - (child_phi_sum - best_child.phi);

    // このしきい値を超える -> 2位の方が有望
    threshold_child.delta = min( threshold.phi, second_child_delta + 1 );

    boMove_mate(node, best_move);

    // 双玉問題で、受け手が詰めろ逃れの詰めろ
    if ( node->next == param.offense_side && 
	 node->king_xy[param.offense_side] &&
	 param.start_tesuki >= 1 ) {
      bo_pass(node); // 攻め方

      TE te_ret;
      int node_limit = GAME_MATE_NODE_LIMIT;
      if ( dfpn_mate(node, 31, &te_ret, &node_limit) == CHECK_MATE ) 
	n = 1;

      boBack_mate(node);
    }

    bfs_hisshi_search( taboo_list, param, node, tesuki - n, 
                       threshold_child, depth - 1, node_limit );
    boBack_mate(node);

    prev_move = best_move;
  }

  if (tesuki == 0 && node->next != param.offense_side) {
    // 受け方で、無駄合いか？
    const MOVEINFO2* mi = &node->mi;
    if ( mi->count >= 2 && te_is_put(&mi->te[mi->count - 2]) &&
         mi->te[mi->count - 1].to == mi->te[mi->count - 2].to ) {
      // たぶん無駄合い
      BOARD* tmp_bo = bo_dup(node);

      PieceKind t = node->tori[mi->count - 1] & 0x7;
      tmp_bo->inhand[ node->next ][ t ]++;
      tmp_bo->inhand[ param.offense_side ][ t ]--;
      te_clear( &tmp_bo->mi.te[mi->count - 2] );
      tmp_bo->tori[mi->count - 1] = 0;
      boSetToBOARD(tmp_bo);

#if DEBUGLEVEL >= 4
      printf("MUDA-AI???\n");
      printBOARD(tmp_bo);
#endif

      // 探索をやり直す
      bfs_hisshi_search( taboo_list, param, tmp_bo, tesuki,
                         make_phi_delta(+VAL_MAY_MATCH, +VAL_MAY_MATCH),
                         depth, node_limit );

      // 詰んでたら、何で合いしても詰む -> 局面を逆算して置換表に登録
      int32_t p = 0; int32_t d = 0;
      hsGetBOARD( tt_tag(param), tmp_bo, &p, &d, NULL, param.te_count );
      if ( p >= +VAL_INF && d == 0 )
        set_muda_ai( tt_tag(param), node, param.te_count );

      freeBOARD(tmp_bo);
    }
  }

#if DEBUGLEVEL >= 1
  if (param.start_tesuki >= DEBUG_TESUKI_LEVEL) {
    printf( "%d: put %" PRIx64 "-> (%d,%d), best = %s\n",
            depth, node->key, best_child.delta, child_phi_sum,
            te_str(&best_move, node).c_str() );
  }
#endif

  // ノードの証明数を更新
  hsPutBOARD( tt_tag(param), node, best_child.delta, child_phi_sum,
              param.te_count, &best_move );
  (*node_limit)--;
  taboo_list->remove(node);
}


/**
 * 必至を探す. 最良優先探索版
 * @return 必至が見つかった場合 true
 */
MATE_STATE bfs_hisshi( BOARD* bo, int tesuki, int depth, TE* best_move,
		       int* node_limit )
{
  assert( tesuki >= 0 && tesuki <= 2 );
  assert( best_move );
  assert( node_limit );

  TabooList<PhiDelta> taboo_list;
  EndingSearchParam param;
  int32_t phi, delta;
  int r;

  param.offense_side = bo->next;
  param.te_count = bo->mi.count;
  param.start_depth = depth;
  param.start_tesuki = tesuki;

  // 内部ノードで詰みを探すときなど、すでに結果が分かっている場合がある
  phi = 0; delta = 0;
  r = hsGetBOARD( tt_tag(param), bo, &phi, &delta, best_move, 0 );
  if (r) {
    // 結論が出ている場合のみ
    if ( phi == 0 && delta >= VAL_INF )
      return CHECK_MATE;
    else if ( phi >= VAL_INF && delta == 0 )
      return NOT_CHECK_MATE;
  }

  PhiDelta threshold = make_phi_delta( +VAL_MAY_MATCH, +VAL_MAY_MATCH );

  bfs_hisshi_search( &taboo_list, param, bo, tesuki, threshold,
                     depth, node_limit );

  if ( gmGetGameStatus(&g_game) == SI_CHUDAN )
    return UNKNOWN_CHECK_MATE;

  phi = 0; delta = 0;
  r = hsGetBOARD( tt_tag(param), bo, &phi, &delta, best_move, 0 );
  if (!r) {
    si_abort("%s: internal error: not found result.\n", __func__);
  }

  if ( phi == 0 && delta >= VAL_MAY_MATCH )
    return CHECK_MATE;
  else {
    if ( phi >= +VAL_INF && delta == 0 )
      return NOT_CHECK_MATE;
    else
      return UNKNOWN_CHECK_MATE;
  }
}


/**
 * 詰め将棋を解く
 * 呼び出し側では、戻り値を CHECK_MATE と比較すること
 * @return 詰んだ CHECK_MATE, 詰まなかった NOT_CHECK_MATE, 不明 UNKNOWN_CHECK_MATE
 */
MATE_STATE dfpn_mate( BOARD* bo, int depth, TE* best_move, int* node_limit )
{
  return bfs_hisshi( bo, 0, depth, best_move, node_limit );
}

