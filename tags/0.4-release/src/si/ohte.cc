/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <assert.h>
#include <algorithm>
#include "si.h"
#include "ui.h"
using namespace std;


static const int can_ohte_to[8] = {
  -16, -17, -1, 15, 16, 17, 1, -15,
};

/** 
 * 歩、香車の成り、桂馬、銀、金、角の成り、飛車の成り、
 * と、成り香、成り桂、成り銀、馬の縦横、竜の斜めでの王手を検索する。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param king_xy 受け方の玉の位置
 */
void normalOhte(const BOARD* bo, MOVEINFO* mi, Xy king_xy) 
{
  static const int can_ohte[8][32][2] = {
      {
	/* -16 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 1, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* -17 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymmy, fu, kyo, kei, */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymny, to, narikyo, narikei */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* -1 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* +15 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymmy, fu, kyo, kei, */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymny, to, narikyo, narikei */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* +16 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 1, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* +17 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymmy, fu, kyo, kei, */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymny, to, narikyo, narikei */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* +1 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* -15 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymmy, fu, kyo, kei, */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymny, to, narikyo, narikei */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
  };
  TE te;
  PieceKind p;
  int i, j;

  te.uti = 0;
  for (i = 0; i < 8; i++) {
    te.to = king_xy + can_ohte_to[ i ];

    if ( bo->board[te.to] == WALL )
      continue;
    
    if ( bo->board[te.to] != 0 && getTeban(bo->board[te.to]) == bo->next )
      continue;

    // pivotまで短いの
    for (j = 0; j < bo->short_attack[bo->next][te.to].count; j++) {
      te.fm = bo->short_attack[bo->next][te.to].from[j];
      p = bo->board[te.fm];

      if ( can_ohte[i][p][0] && can_ohte[i][p][1] )
        miAddWithNari( mi, *bo, te );
      else {
        if ( can_ohte[i][p][0] || can_ohte[i][p][1] ) {
          te.nari = can_ohte[i][p][0] ? 0 : 1;
          mi_add_with_pin_check( mi, *bo, te );
        }
      }
    }

    // pivotまでは長いの
    for (j = 0; j < bo->long_attack[bo->next][te.to].count; j++) {
      te.fm = bo->long_attack[bo->next][te.to].from[j];
      p = bo->board[te.fm];

      if ( can_ohte[i][p][0] && can_ohte[i][p][1] )
        miAddWithNari( mi, *bo, te );
      else {
        if ( can_ohte[i][p][0] || can_ohte[i][p][1] ) {
          te.nari = can_ohte[i][p][0] ? 0 : 1;
          mi_add_with_pin_check( mi, *bo, te );
        }
      }
    }
  }
}


/**
 * 桂の王手を探す。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param xy 相手玉 (受け方) の位置 
 */
static void keiOhte(const BOARD* bo, MOVEINFO* mi, Xy const xy) 
{
  TE te;
  int i, j;

  assert( bo != NULL );
  assert( mi != NULL );

  te.nari = 0;
  te.uti = 0;

  PieceKind pie = KEI + (bo->next << 4);

  for (i = 0; i < normal_to_count[pie]; i++) {
    te.to = xy - normal_to[pie][i];

    if (bo->board[te.to] == WALL)
      continue;

    if (bo->board[te.to] && getTeban(bo->board[te.to]) == bo->next)
      continue;

    for (j = 0; j < normal_to_count[pie]; j++) {
      te.fm = te.to - normal_to[pie][j];
      if ( bo->board[te.fm] == pie )
        mi_add_with_pin_check( mi, *bo, te );
    }
  }
}


/**
 * 長い攻撃による王手を探す。
 * @param bo 検索するBOARD
 * @param mi 王手を格納するMOVEINFO
 * @param king_xy 受け側の玉の位置
 */
static void remoteOhte(const BOARD* bo, MOVEINFO* mi, Xy const king_xy) 
{
  TE te;
  int j;
  int direc;

  assert( bo != NULL );
  assert( mi != NULL );

  int const next = bo->next;

  te.nari = 0;
  te.uti = 0;

  for (direc = 0; direc < 8; direc++) {
    Xy pivot;
    for (pivot = king_xy + arr_round_to[direc]; 1; 
         pivot += arr_round_to[direc]) {
      if ( bo->board[pivot] == WALL )
        break;
      if ( bo->board[pivot] && getTeban(bo->board[pivot]) == next )
        break;

      // pivot から玉をlong attackできるような駒があるか
      te.to = pivot;

      // pivotまでは短いの
      for (j = 0; j < bo->short_attack[next][pivot].count; j++) {
        te.fm = bo->short_attack[next][pivot].from[j];
        PieceKind const pie = bo->board[ te.fm ];
        PieceKind p = (pie & 0xf);
        if (p != UMA && p != RYU)
          continue;

        if ( find(remote_to[pie], remote_to[pie] + remote_to_count[pie],
                  -arr_round_to[direc]) 
             != remote_to[pie] + remote_to_count[pie] ) {
          miAddWithNari( mi, *bo, te );
        }
      }

      // pivotまでも長いの
      for (j = 0; j < bo->long_attack[next][pivot].count; j++) {
        te.fm = bo->long_attack[next][pivot].from[j];

        PieceKind const pie = bo->board[ te.fm ];
#ifndef NDEBUG
        PieceKind p = (pie & 0xf);
        assert(p == KYO || p == KAKU || p == HISHA || p == UMA || p == RYU);
#endif // !NDEBUG	

        if ( find(remote_to[pie], remote_to[pie] + remote_to_count[pie],
                  -arr_round_to[direc])
             != remote_to[pie] + remote_to_count[pie] ) {
          if ( (pie & 0xf) == KYO )
            mi_add_with_pin_check( mi, *bo, te );
          else
            miAddWithNari( mi, *bo, te );
        }
      }

      if ( bo->board[pivot] )
        break;
    }
  }
}


/**
 * 歩、桂、銀、金の打ち込んで王手を探す。
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 * @param king_xy 受け方の玉の位置
 */
static void utiOhte(const BOARD* bo, MOVEINFO* mi, Xy const king_xy) 
{
  for (PieceKind pie = 1; pie <= 7; pie++) {
    if ( !bo->inhand[bo->next][pie] )
      continue;

    for (int j = 0; j < normal_to_count[pie]; j++) {
      Xy to = king_xy - normal_to[ pie + (bo->next << 4) ][j];

      if (bo->board[to]) // 盤外か駒がある
        continue;

      append_a_put_move(bo, mi, pie, to);
    }
  }
}


/**
 * 遠くに効く駒(飛車、角、香)を打ち込んでの王手を探す。
 * @param bo 対象のBOARD
 * @param mi 結果を納めるMOVEINFO
 * @param king_xy 受け方の玉の位置
 */
static void UtiOhte2(const BOARD* bo, MOVEINFO* mi, Xy const king_xy) 
{
  int i;
  TE te;

  assert( bo != NULL );
  assert( mi != NULL );

  te.fm = 0;
  te.nari = 0;
  te.hint = 0;

  /* kyo */
  if ( bo->next ) {
    /* gote */
    if (bo->inhand[1][KYO]) {
      for (i = king_xy - 0x10; bo->board[i] == 0; i -= 0x10) {
	te.to = i;
	te.uti = KYO;
        miAdd(mi, te);
      }
    }
  } else {
    /* sente */
    if (bo->inhand[0][KYO]) {
      for (i = king_xy + 0x10; bo->board[i] == 0; i += 0x10) {
	te.to = i;
	te.uti = KYO;
        miAdd(mi, te);
      }
    }
  }

  /* hisya */
  if (bo->inhand[bo->next][HISHA]) {
    for (i = king_xy + 1; bo->board[i] == 0; i++) {
      te.to = i;
      te.uti = HISHA;
      miAdd(mi, te);
    }
    for (i = king_xy - 1; bo->board[i] == 0; i--) {
      te.to = i;
      te.uti = HISHA;
      miAdd(mi, te);
    }
    for (i = king_xy + 0x10; bo->board[i] == 0; i += 0x10) {
      te.to = i;
      te.uti = HISHA;
      miAdd(mi, te);
    }
    for (i = king_xy - 0x10; bo->board[i] == 0; i -= 0x10) {
      te.to = i;
      te.uti = HISHA;
      miAdd(mi, te);
    }
  }

  /* kaku */
  if (bo->inhand[bo->next][KAKU]) {
    for (i = king_xy + 0x11; bo->board[i] == 0; i+=0x11) {
      te.to = i;
      te.uti = KAKU;
      miAdd(mi, te);
    }
    for (i = king_xy + 0x0F; bo->board[i] == 0; i+=0x0F) {
      te.to = i;
      te.uti = KAKU;
      miAdd(mi, te);
    }
    for (i = king_xy - 0x0F; bo->board[i] == 0; i -= 0x0F) {
      te.to = i;
      te.uti = KAKU;
      miAdd(mi, te);
    }
    for (i = king_xy - 0x11; bo->board[i] == 0; i -= 0x11) {
      te.to = i;
      te.uti = KAKU;
      miAdd(mi, te);
    }
  }
}


/**
 * 空き王手を探す。
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 * @param king_xy 受け方の玉の座標
 */
static void akiOhte(const BOARD* bo, MOVEINFO* mi, Xy const king_xy) 
{
  int d, p;
  int i;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

#ifdef DEBUG
  assert( 1 <= (king_xy & 0x0F) && (king_xy & 0x0F) <= 9 );
  assert( 1 <= (king_xy >> 4) && (king_xy >> 4) <= 9 );
#endif /* DEBUG */

  for (d = 0; d < 8; d++) {
    Xy fxy = king_xy;
    XyDiff vw = can_ohte_to[d];
    while (true) {
      fxy += vw;

      p = bo->board[ fxy ];
      if ( p == 0 )
        continue;

      if ( p == WALL )
        break;

      if ( getTeban(p) == bo->next ) {
        // 攻め方の駒に自分側のlong attackが当たっていて、かつ方角が同じ
        for (i = 0; i < bo->long_attack[bo->next][fxy].count; i++) {
          if (norm_direc(bo->long_attack[bo->next][fxy].from[i], fxy) == -vw) {
            // 開き王手できる

            MOVEINFO mi1;
            int j;
            mi1.count = 0;
            mi_append_moveto(bo, fxy, &mi1);
            for (j = 0; j < mi1.count; j++) {
              int te_direc = norm_direc(fxy, mi1.te[j].to);
              if ( te_direc != vw && te_direc != -vw )
                miAdd(mi, mi1.te[j]); // hintもコピーされる
            }
          }
        }
      }
      break;
    }
  }
}


/**
 * 王手を検索して mi に格納する。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 */
void ohte(const BOARD* bo, MOVEINFO* mi) 
{
#if 0 // これはだいぶ遅い
  MOVEINFO all_mi;
  make_moves(bo, &all_mi);

  mi->count = 0;
  BOARD* tmp_bo = bo_dup(bo);
  for (int i = 0; i < all_mi.count; i++) {
    boMove_mate( tmp_bo, all_mi.te[i] );
    if ( bo_is_checked(tmp_bo, 1 - bo->next) )
      miAdd( mi, all_mi.te[i] );
    boBack_mate( tmp_bo );
  }
  freeBOARD(tmp_bo);
  return;
#endif // 0

  Xy king_xy;

  mi->count = 0;

  // 自玉が王手されている
  if ( bo_is_checked(bo, bo->next) ) {
    MOVEINFO uke_mi;
    BOARD* tmp_bo = bo_dup(bo);

    uke( bo, &uke_mi );
    int i;
    for (i = 0; i < uke_mi.count; i++) {
      boMove_mate( tmp_bo, uke_mi.te[i] );
      if ( bo_is_checked(tmp_bo, 1 - bo->next) )
        miAdd( mi, uke_mi.te[i] ); // hintもコピーされる
      boBack_mate( tmp_bo );
    }

    freeBOARD(tmp_bo);
    return;
  }

  if (bo->next) {
    /* gote */
    king_xy = bo->king_xy[0];
  } else {
    /* sente */
    king_xy = bo->king_xy[1];
  }

  // まず打ちから
  UtiOhte2(bo, mi, king_xy);
  utiOhte(bo, mi, king_xy);

  remoteOhte(bo, mi, king_xy);
  keiOhte(bo, mi, king_xy);
  normalOhte(bo, mi, king_xy);
  akiOhte(bo, mi, king_xy);

  miRemoveDuplicationTE(mi); // 移動の手がダブる
}
