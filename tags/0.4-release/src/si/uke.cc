/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdlib.h>
#include <assert.h>
#include "si.h"
#include "ui.h"


static int ukeAigoma(const BOARD* bo, MOVEINFO* mi, Xy king_xy, Xy rxy,
                     int next);


/**
 * xy に動ける next 手番の駒を探して MOVEINFO に te を追加する。
 * ただし王は除く。受け生成用。
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 * @param xy 場所
 * @param next 次の手番
 */
static void moveto2(const BOARD* bo, MOVEINFO* mi, Xy xy, int next)
{
  assert( !bo->board[xy] || getTeban(bo->board[xy]) != next );

  TE te;
  int i, p;

  te.nari = 0;
  te.uti = 0;
  te.to = xy;

  // 短いの
  for (i = 0; i < bo->short_attack[ next ][ xy ].count; i++) {
    te.fm = bo->short_attack[ next ][ xy ].from[i];

    p = bo->board[ te.fm ];
    if ( p == OH || p == (OH + 0x10) )
      continue;

    miAddWithNari( mi, *bo, te );
  }

  // 長いの
  for (i = 0; i < bo->long_attack[ next ][ xy ].count; i++) {
    te.fm = bo->long_attack[ next ][ xy ].from[i];

    miAddWithNari( mi, *bo, te );
  }
}


/**
 * 王手された状態で受けを探す。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @return heuristicsを考慮した手の数
 */
int uke( const BOARD* bo, MOVEINFO* mi )
{
#ifdef USE_PIN // DEBUG
  static int test_count = 0;
  if (bo->hash_all == 0xc7d4d4ba7b49000bULL ) {
    ++test_count;

    if (test_count == 18) {
      printBOARD(bo);
      for (int y = 1; y <= 9; y++) {
	for (int x = 1; x <= 9; x++) {
	  printf("%2d ", bo->pinned[(y << 4) + x]);
	}
	printf("\n");
      }

      BOARD* bo_tmp = bo_dup(bo);
      while (bo_tmp->mi.count > 0)
	boBack_mate(bo_tmp);
      mi2_print_sequence( &bo->mi, *bo_tmp );
      freeBOARD(bo_tmp);
    }
  }
#endif

  int num_of_ohte;
  int const next = bo->next;

  mi->count = 0;
  int ret = 0;

  // 自玉
  Xy const king_xy = bo->king_xy[bo->next];
  
  /*
   * 王手している駒の数を数える。
   * ２つならば王が動くしかない。
   * １つならば、
   * ・王が動く
   * ・王手している駒を取る
   * ・合い駒する
   * の３種類の受けがある。
   */ 
  num_of_ohte = bo_attack_count(bo, next == 0, king_xy);
  assert( num_of_ohte == 1 || num_of_ohte == 2 );

  if ( num_of_ohte == 1 ) {
    Xy xy;
    if (bo->short_attack[ next == 0 ][ king_xy ].count)
      xy = bo->short_attack[ next == 0 ][ king_xy ].from[0];
    else
      xy = bo->long_attack[ next == 0 ][ king_xy ].from[0];

    /* 王手している駒を取って受ける */
    moveto2(bo, mi, xy, next);

    /* 王が動いて受ける */
    mi_append_king_moves(bo, mi, king_xy, next);
    ret = mi->count;

    /* 合い駒して受ける */
    ret += ukeAigoma(bo, mi, king_xy, xy, next);
  }
  else {
    /* 両王手の場合 */
    /* 王が動いて受ける */
    mi_append_king_moves(bo, mi, king_xy, next);
    ret = mi->count;
  }

#ifdef USE_PIN // DEBUG
  if (bo->hash_all == 0xc7d4d4ba7b49000bULL && test_count == 20) {
    printf("test_count = %d\n", test_count);
    printBOARD(bo);
    mi_print(mi, *bo);
    abort();
  }
#endif

  return ret;
}


/**
 * 合い駒して王手を受ける。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param king_xy 王の場所
 * @param rxy 王手している駒の場所
 * @param next 受け側の手番
 */
static int ukeAigoma(const BOARD* bo, MOVEINFO* mi, Xy king_xy, Xy rxy, 
                     int next) 
{
  int v, w;
  int i;
  PieceKind p;

  p = bo->board[ rxy ] & 0xF;
  if (bo->long_attack[1 - next][king_xy].count == 0)
    return 0;

  v = (rxy & 0x0F) - (king_xy & 0x0F);
  w = (rxy >> 4) - (king_xy >> 4);

  if ( abs(v) <= 1 && abs(w) <= 1) {
    /* 合い駒できないので戻る */
    return 0;
  }
  
  if ( 0 < v )
    v = 1;
  else if ( v < 0 )
    v = -1;
  
  if ( 0 < w )
    w = 1;
  else if ( w < 0 )
    w = -1;

  int const count_before = mi->count;

  // 空白のところに駒を置いていく
  for (i = 1; 1; i++) {
    Xy xy = king_xy + ((w * i) << 4) + (v * i);
    p = bo->board[ xy ];

#ifdef DEBUG
    assert( p != WALL );
#endif /* DEBUG */

    if ( p )
      break;

    // 移動して合い駒. 当然、玉を動かす手は除く。
    moveto2(bo, mi, xy, next);

    // 打つ
    mi_append_put_moves( bo, xy, mi );
  }

  return mi->count > count_before ? 1 : 0;
}

