/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <assert.h>
#include "si.h"
#include "si-think.h"
#include "ui.h"
#include "taboo_list.h"
#include <algorithm>

#define DEBUGLEVEL 0

#if DEBUGLEVEL >= 3
#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>
#endif
using namespace std;


#ifdef USE_GUI
extern GAME g_game;
#endif


/**
 * タブーリストと置換表の両方から探す
 * @return 見つかったときtrue
 */
static bool lookup_hash( const TabooList<PhiDelta>* taboo_list, 
			 const BOARD* bo,
                         int const te_count, 
			 TE* best_move,
			 PhiDelta* score )
{
  if ( taboo_list->lookup(bo, score) ) {
    te_clear(best_move);
    return true;
  }

  // ここが詰め将棋と共通化できない
  if ( hsGetBOARDMinMax(bo, score, best_move, te_count) )
    return true;

  // 仮評価からスタート
  think_eval(bo, score);
  return false;
}


struct SearchParam
{
  int te_count;
  int start_depth;
};


/** 置換表に勝ちを登録 */
struct PutWin {
  int te_count;
  PutWin(int te_count_): te_count(te_count_) {}
  void operator () (const BOARD* bo, const TE* move) const {
    hsPutBOARDMinMax(bo, +VAL_INF, -VAL_INF, te_count, move);
  }
};


static bool do_move( const TabooList<PhiDelta>* taboo_list,
		     const SearchParam& param,
                     int rest_depth,
                     BOARD* node,
                     MOVEINFO* const mi, int index,
		     PhiDelta* child_score )
{
  if ( (mi->te[index].hint & TE_STUPID) != 0 )
    return false;

  bool is_checked = bo_is_checked(node, node->next);
  TsumeroNogare tsumero_nogare;

  int atkcnt = think_count_attack24(node, node->next, 
				    node->king_xy[1 - node->next]);
  if ( !is_checked )
    tsumero_nogare.before_move(node, 1 - node->next);

  boMove( node, mi->te[index] );

  TE tt_move;
  bool r = lookup_hash( taboo_list, node, param.te_count, &tt_move, 
			child_score );
  if (r) {
    if ( mi->te[index] == tt_move)
      mi->te[index].hint = tt_move.hint;
    return true;
  }

  if (is_checked) {
    // 王手の受け
    return true; 
  }
  else if ( bo_is_checked(node, node->next) ) {
    // 王手
    return true;
  }

  if ( (node->mi.te[node->mi.count - 2].hint & TE_TSUMERO) != 0) {
    // 詰めろを掛けられていたとき
    // TODO: 自分からの詰み探索は別の場所で
    if ( !tsumero_nogare.promise(node) ) {
      hsPutBOARDMinMax(node, +VAL_INF, -VAL_INF, param.te_count, NULL);
      boBack(node);
      return false;
    }

    // 受けになっているか
    TE mate_te;
    int mate_node_limit = GAME_MATE_NODE_LIMIT;
    if ( dfpn_mate(node, 31, &mate_te, &mate_node_limit) == CHECK_MATE ) {
      // 受けになっていない. 負け
      hsPutBOARDMinMax(node, +VAL_INF, -VAL_INF, param.te_count, NULL);

      if ( te_is_put(&mi->te[index]) ) {
        // ほかの打つ手はどうか
        mate_set_muda_uchi( node->next, 1, PutWin(param.te_count),
                            node, mi, index + 1 );
      }
      boBack(node);
      return false;
    }
  }

  // こちらから詰めろ (または詰めろ逃れの詰めろ) が掛かるか
  if ( param.start_depth == rest_depth ) {
    bool promise = false;

    // - 取る手
    if ( node->tori[node->mi.count - 1] )
      promise = true;

    // - 利きを入れる手
    else if ( think_count_attack24(node, 1 - node->next, 
				 node->king_xy[node->next]) > atkcnt )
      promise = true;

    if ( promise ) {
      // 詰めろ？
      bo_pass(node);
      int mate_node_limit = GAME_MATE_NODE_LIMIT;
      TE mate_te;
      if ( dfpn_mate(node, 31, &mate_te, &mate_node_limit) == CHECK_MATE ) {
        // 詰めろを掛けたときは仮評価を足して、優先的に読むように
        child_score->delta += 1000; // TODO: NPC へ移動
        mi->te[index].hint |= TE_TSUMERO;
      }
      boBack_mate(node); // パスを戻す
    }
  }

  return true;
}


/**
 * 手を展開し、最善手を選ぶ
 * 子ノードのうち delta 最小を選ぶ
 * 勝ち = (+VAL_INF, -VAL_INF), 負け = (-VAL_INF, +VAL_INF)
 * 一通り展開するので、move ordering は効果なし
 */
static void expand_and_best_move( const TabooList<PhiDelta>* taboo_list,
				  const SearchParam& param,
                                  BOARD* node,
                                  int rest_depth, int rest_node,
                                  PhiDelta* best_child,
				  int32_t* child_phi_min,
                                  int32_t* second_child_delta,
                                  TE* best_move )
{
  // 手を展開する
  MOVEINFO mi;
  make_moves(node, &mi);

  best_child->delta = -VAL_INF; // worst value
  *child_phi_min = +VAL_INF;

  if (!mi.count) {
    // 負け
    te_clear(best_move);
    return;
  }

  *second_child_delta = -VAL_INF;
  const TE* may_best = mi.te; // 手がある限り何か選ぶ

#if DEBUGLEVEL >= 3
  printf("%d: ", rest_depth);
#endif

  for (int i = 0; i < mi.count; i++) {
    PhiDelta child_score;  // child nodeにとってよい局面 -> phi大, delta小

    if ( !do_move(taboo_list, param, rest_depth, node, &mi, i, &child_score) )
      continue;

    if (child_score.delta > best_child->delta) {
      // bestを更新
      *second_child_delta = best_child->delta;
      *best_child = child_score;
      may_best = mi.te + i;
    }
    else if (child_score.delta > *second_child_delta) {
      // 2位のみ更新
      *second_child_delta = child_score.delta;
    }

    // TODO: これでいいかどうか
    *child_phi_min = min(*child_phi_min, child_score.phi);

    boBack(node);

#if DEBUGLEVEL >= 3
    printf("%s=(%d,%d), ", te_str(mi.te + i, *node).c_str(),
           child_score.phi, child_score.delta);
#endif

    if (child_score.phi <= -VAL_MAY_MATCH)
      break;  // nodeの手番側の勝ち確定
  }

#if DEBUGLEVEL >= 3
  printf("best = %s\n", te_str(may_best, *node).c_str());
#endif
  *best_move = *may_best;
}


/**
 * 探索する
 * 必ず一度はノードを展開する
 */
static void bfs_search( TabooList<PhiDelta>* taboo_list, 
			const SearchParam& param,
			BOARD* node,
                        const PhiDelta& threshold,
                        int depth, int* node_limit )
{
#ifndef NDEBUG
  uint64_t const node_hash = node->hash_all;
#endif // !NDEBUG

#if DEBUGLEVEL >= 3
  printf("%d: enter: threshold=(%d, %d), %d\n",
         depth, threshold.phi, threshold.delta, *node_limit);
#endif

  TE best_move, prev_move;
  PhiDelta prev_threshold = make_phi_delta(0, 0);
  te_clear(&best_move);
  te_clear(&prev_move);

#ifdef USE_GUI
  if ( !(*node_limit % 700) ) {
    /* GUIのペンディングされたイベントを処理する */
    processing_pending();
  }
#endif /* USE_GUI */

  if ( param.start_depth == depth ||
       (node->mi.count >= 1 && 
        (node->mi.te[node->mi.count - 1].hint & TE_TSUMERO) != 0) ) {
    // ルートノードか、詰めろを掛けられていた. 勝ちがあるか
    int mate_node_limit = GAME_MATE_NODE_LIMIT;
    if ( dfpn_mate(node, 31, &best_move, &mate_node_limit) == CHECK_MATE ) {
      hsPutBOARDMinMax( node, +VAL_INF, -VAL_INF, param.te_count, &best_move );
      (*node_limit)--;
      return;
    }
  }

  if ( node->mi.count >= 2 && 
       (node->mi.te[node->mi.count - 2].hint & TE_TSUMERO) != 0 ) {
    // 詰めろを掛けた次の手で、詰め将棋を読んでみる
    int mate_node_limit = GAME_MATE_NODE_LIMIT;
    if ( dfpn_mate(node, 31, &best_move, &mate_node_limit) == CHECK_MATE ) {
      hsPutBOARDMinMax( node, +VAL_INF, -VAL_INF, param.te_count, &best_move );
      (*node_limit)--;
      return;
    }
  }

  // サイクル回避
  taboo_list->add( node, threshold );

  PhiDelta best_child;
  int32_t child_phi_min;

  while (true) {
    int32_t second_child_delta;

    te_clear(&best_move);

    // ノードを展開して最善手を得る
    expand_and_best_move( taboo_list, param, node,
                          depth, *node_limit,
                          &best_child,
			  &child_phi_min,
                          &second_child_delta,
                          &best_move );

    // 閾値を超えた？
    if ( best_child.delta <= threshold.phi || child_phi_min <= threshold.delta )
      break;

    // 候補手が一つでもあれば必ず設定
    assert(best_move.to);

#ifdef USE_GUI
    /* 中断や投了が入ってないか調べる */
    if ( gmGetGameStatus(&g_game) == SI_CHUDAN )
      break;
#endif // USE_GUI

    // 探索ノードを使い果たした
    if ( depth <= 1 || *node_limit < 0 )
      break;

    PhiDelta threshold_child;

    // このしきい値を超える (下回る) -> delta cutが起こる
    threshold_child.phi = threshold.delta;

    // このしきい値を超える (下回る) -> 2位の方が有望
    threshold_child.delta = max( threshold.phi, second_child_delta - 1);

    // 先端 ?
    if ( prev_move == best_move && prev_threshold == threshold_child ) {
#if 0
      printf( "%d: loop!! threshold=(%d, %d), score=(%d,%d)\n",
	      depth, threshold.phi, threshold.delta, 
	      best_child.delta, child_phi_min ); // DEBUG
#endif // 0
      break;
    }

    boMove(node, best_move);

    bfs_search( taboo_list, param, node, threshold_child,
                depth - 1, node_limit );

    boBack(node);

    prev_move = best_move;
    prev_threshold = threshold_child;
  }

  assert( node_hash == node->hash_all );
  hsPutBOARDMinMax( node, best_child.delta, child_phi_min, 
		    param.te_count, &best_move );

#if DEBUGLEVEL >= 3
    printf("%d: put %" PRIx64 "-> (%d,%d)\n",
           depth, node->key, best_child.delta, child_phi_min );
#endif

  (*node_limit)--;
  taboo_list->remove(node);
}


/**
 * 通常探索をおこなう
 * @param bo 現在の局面
 * @param depth 探索する最大の深さ
 * @param best_move 最善手を返す
 *
 * @return SI_NORMAL 通常の手
 *         SI_TORYO  負け
 */
INPUTSTATUS bfs_minmax( BOARD* bo, int depth, TE* best_move )
{
  assert(best_move);

  int r;
  PhiDelta score;

  // 結論が出ているか？
  r = hsGetBOARDMinMax( bo, &score, best_move, 0 );
  if (r) {
    // 結論が出ている場合のみ利用
    if ( score.phi >= +VAL_INF && score.delta <= -VAL_INF ) {
      assert(best_move->to >= 0x11 && best_move->to <= 0x99);
      return SI_NORMAL;
    }
    else if ( score.phi <= -VAL_INF && score.delta >= +VAL_INF )
      return SI_TORYO;
  }

  TabooList<PhiDelta> taboo_list;
  SearchParam param;
  param.te_count = bo->mi.count;
  param.start_depth = depth;
  int node_limit = 5000; // magic
  bfs_search( &taboo_list, param, bo, 
	      make_phi_delta(-VAL_MAY_MATCH, -VAL_MAY_MATCH),
              depth, &node_limit );

  if ( gmGetGameStatus(&g_game) == SI_CHUDAN )
    return SI_CHUDAN_RECV;

  r = hsGetBOARDMinMax( bo, &score, best_move, 0 );
  if (!r) {
    si_abort("internal error: not found result\n");
  }

  if ( score.phi <= -VAL_MAY_MATCH && score.delta >= +VAL_MAY_MATCH )
    return SI_TORYO;

  assert(best_move->to >= 0x11 && best_move->to <= 0x99);
  return SI_NORMAL;
}


/**
 * 定跡 (opening book) 手があるか調べてあったら te に格納する。
 *
 * @param bo 対象のBOARD
 * @param te 見つかった手 (出力)
 * @return 1 ならば定跡手があった。 0 ならばなし。
 */
int jouseki(const BOARD* bo, TE* te)
{
  if ( bo->next == SENTE ) {
    if ( bo->mi.count == 0 ) {
      te->fm = 0x96;
      te->to = 0x87;
      te->nari = 0;
      te->uti = 0;
      return 1;
    } else if ( bo->mi.count == 2 ) {
      te->fm = 0x93;
      te->to = 0x84;
      te->nari = 0;
      te->uti = 0;
      return 1;
    }
  } else {
    if ( bo->mi.count == 1 ) {
      te->fm = 0x14;
      te->to = 0x23;
      te->nari = 0;
      te->uti = 0;
      return 1;
    } else if ( bo->mi.count == 3 ) {
      te->fm = 0x17;
      te->to = 0x26;
      te->nari = 0;
      te->uti = 0;
      return 1;
    }
  }
   
  return 0;
}

