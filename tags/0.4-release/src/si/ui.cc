/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "si.h"
#include "si-think.h"
#include "ui.h"


static void (*pending_function)(void) = NULL;
void putCmdHelp(void);


void putBOARD(const BOARD* brd)
{
  extern int flgDisplayMode;

#ifdef DEBUG
  assert( brd != NULL );
#endif /* DEBUG */
  
  if ( flgDisplayMode )
    fputBOARD(stdout, 0, brd);
  else 
    fputBOARD(stdout, 1, brd);
}

void fputBOARD(FILE* out, int mode, const BOARD* bo)
{
  int i, j;
  int c;

#ifdef DEBUG
  assert( out != NULL && bo != NULL );
  assert( mode == 0 || mode == 1 );
#endif /* DEBUG */

  fprintf(out, "/Board\n");
  for (i = 1; i <= 9; i++) {
    for (j = 9; 0<j; j--) {
      c = boGetPiece( bo, (i << 4) + j );
      if (c) {
	if (mode)
	  fprintf(out, "%c%X ", (c & 0x10) ? 'v' : '^', c & 0x0F);
	else
	  fprintf(out, "%02X ", c);
      } else {
	if (mode)
	  fprintf(out, " . ");
	else
	  fprintf(out, "00 ");
      }
    }
    fprintf(out, "\n");
  }
  fprintf(out, "/Piece\n");
  fprintf(out, "%d %d %d %d %d %d %02d\n",
	 bo->inhand[0][7],
	 bo->inhand[0][6],
	 bo->inhand[0][5],
	 bo->inhand[0][4],
	 bo->inhand[0][3],
	 bo->inhand[0][2],
	 bo->inhand[0][1]);
  fprintf(out, "%d %d %d %d %d %d %02d\n",
	 bo->inhand[1][7],
	 bo->inhand[1][6],
	 bo->inhand[1][5],
	 bo->inhand[1][4],
	 bo->inhand[1][3],
	 bo->inhand[1][2],
	 bo->inhand[1][1]);
  fprintf(out, "/Next move\n");
  if (mode)
    fprintf(out, "%s\n", bo->next ? "gote" : "sente");
  else
    fprintf(out, "%d\n", bo->next);
  
#ifdef DEBUG
  printf("key : %08LX\n", bo->key);
#endif /* DEBUG */
  
  return;
}


#if 0
void printMOVEINFO(MOVEINFO * mi) {
  int i;

#ifdef DEBUG
  assert( mi != NULL );
#endif /* DEBUG */

  printf( "MOVEINFO\n" );
  for (i = 0; i < mi->count; i++) {
    printTE(&(mi->te[i]));
  }
  printf("count : %d\n", mi->count);
}
#endif // 0


void printMOVEINFO2(MOVEINFO2 * mi) {
  int i;

#ifdef DEBUG
  assert( mi != NULL );
#endif /* DEBUG */

  printf( "MOVEINFO\n" );
  for (i = 0; i < mi->count; i++) {
    printTE(&(mi->te[i]));
  }
  printf("count : %d\n", mi->count);
}


void fprintTE(FILE *out, TE * te) {
#ifdef DEBUG
  assert( te != NULL );
  assert( 0 <= te->uti && te->uti <= 7 );
#endif /* DEBUG */
  
  if ( te_is_put(te) ) {
    fprintf(out, "(%d,%d) %d uti\n",
	   (te->to)&0x0f, (te->to)>>4, te->uti);
  } else {
    fprintf(out, "(%d,%d)->(%d,%d)%s\n",
	   (te->fm)&0x0f, (te->fm)>>4,
	   (te->to)&0x0f, (te->to)>>4,
	   te->nari ? "nari" : "");
  }
}


#ifdef USE_PIECE_NO
void putPBOX(PBOX *pb) {
  fputPBOX(stdout, pb);
}


void fputPBOX(FILE *out, PBOX *pb) {
  static const char* name[] = {
    "dummy", "fu", "kyo", "kei", "gin", "kin", "kaku", "hisya", "oh",
  };
  int i, j;
  
#ifdef DEBUG
  assert( out != NULL );
  assert( pb != NULL );
#endif /* DEBUG */

  for ( i=1; i<9; i++ ) {
    fprintf( out, "%-5s : %2d   ", name[i], pb->count[i] );
    for ( j=0; j<pb->count[i]; j++ ) {
      fprintf( out, "%2d, ", pb->number[i][j] );
    }
    fprintf( out, "\n" );
  }
}
#endif // 0


/**
 * ペンディングされたイベントを処理する関数へのポインタをセットする。
 * @param f ペンディングされたイベントを処理する関数へのポインタ
 */
void set_pending_function(void (*f)(void)) {
  pending_function = f;
}

/**
 * GUI のペンディングされたイベントを処理する。
 */
void processing_pending() {
  if ( pending_function == NULL )
    return;
  
  (*pending_function)();
}

/* -------------------------------------------------------- */
/* 手の入力関数 */
/* -------------------------------------------------------- */

/**
 * 画面から文字ベースで次の一手を入力し te に格納する。
 * @param bo 対象のBOARD
 * @param tte TE
 * @return GAMESTAT
 */
INPUTSTATUS si_input_next_human(BOARD *bo, TE *tte) {
  char buf[BUFSIZ];
  TE te;

  while ( 1 ) {
    printf("$ ");
    (void)fgets(buf, BUFSIZ - 1, stdin);
    if ( buf[0] == '\n' ) {
    } else if ( strncmp( buf, "back", 4 ) == 0 ) {
      if ( 0 < bo->mi.count ) {
        boBack( bo );
        boBack( bo );
	putBOARD( bo );
      }
    } else if ( strncmp( buf, "help", 4 ) == 0 ) {
      putCmdHelp();
    } else if ( strncmp( buf, "disp", 4 ) == 0 ) {
      extern int flgDisplayMode;
      flgDisplayMode = (1 - flgDisplayMode);
    } else if ( strncmp( buf, "toryo", 4 ) == 0 ) {
      return SI_TORYO;
    } else if ( strncmp( buf, "make", 4 ) == 0 ) {
      return SI_TORYO;
    } else if ( strncmp( buf, "quit", 4 ) == 0 ) {
      return SI_CHUDAN;
    } else if ( strncmp( buf, "board", 5 ) == 0 ) {
      putBOARD(bo);
    } else if ( isdigit(buf[0]) && isdigit(buf[1]) &&
	        isdigit(buf[2]) && isdigit(buf[3]) ) {
      te.fm = ((buf[1]-'0') << 4) + buf[0]-'0';
      te.to = ((buf[3]-'0') << 4) + buf[2]-'0';
      te.nari = ( buf[4] == 'n' );
      te.uti = 0;
      if ( is_valid_move( bo, &te ) ) {
	*tte = te;
	break;
      } else {
	printf("No good move\n");
      }
    } else if ( isdigit(buf[0]) && isdigit(buf[1]) &&
	        buf[2] == 'u' && isdigit(buf[3]) ) {
      te.to = ((buf[1]-'0') << 4) + buf[0]-'0';
      te.fm = 0;
      te.nari = 0;
      te.uti = buf[3] - '0';
      if ( is_valid_move( bo, &te ) ) {
	*tte = te;
	break;
      } else {
	printf("No good move\n");
      }
    } else {
      printf("Unknown command : %s", buf);
    }
  }

  return SI_NORMAL;
}


extern GAME g_game;

/**
 * プログラムの一手を te に格納する。
 * @param bo 対象のBOARD
 * @param best_move 指し手 (出力)
 * @return GAMESTAT
 */
INPUTSTATUS si_input_next_computer( BOARD* bo, TE* best_move )
{
  INPUTSTATUS ret;
#ifndef NDEBUG
  int side = bo->next;
  uint64_t hash = bo->hash_all;
#endif

  if ( jouseki(bo, best_move) ) {
    // 定跡があった
    return SI_NORMAL;
  }

  // 詰めろを掛けられているか
  if ( bo->mi.count >= 1 && !bo_is_checked(bo, bo->next) ) {
    bo_pass(bo);
    int node_limit = GAME_MATE_NODE_LIMIT;
    if ( dfpn_mate(bo, 31, best_move, &node_limit) == CHECK_MATE ) {
      // 詰めろ
      bo->mi.te[bo->mi.count - 2].hint |= TE_TSUMERO;
    }
    boBack_mate(bo);
  }

  if ( gmGetGameStatus(&g_game) == SI_CHUDAN )
    return SI_CHUDAN_RECV;

  // 通常探索
  ret = bfs_minmax(bo, 23, best_move);

  assert(side == bo->next);
  assert(hash == bo->hash_all);
  return ret;
}


void putCmdHelp(void)
{
  printf("- command\n");
  printf("\n");
  printf("6978 move (6,9) to (7,8)\n");
  printf("34u1 koma uti (6,9) fu\n");
  printf("\n");
  printf("         fu    1 , kyo  2\n");
  printf("         kei   3 , gin  4\n");
  printf("         kin   5 , kaku 5\n");
  printf("         hisya 7\n");
  printf("\n");
  printf("toryo or make : Lost.\n");
  printf("quit   : Exit this program.\n");
  printf("disp   : Change to display mode.\n");
  printf("board  : Display board.\n");
  printf("\n");
}
