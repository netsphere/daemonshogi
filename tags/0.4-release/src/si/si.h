/* -*- mode:c++ -*-
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _SI_H_
#define _SI_H_

#include <time.h>
#include <stdint.h>  // uint64_t
#include <stdbool.h> // bool
#include <assert.h>
#include <ostream>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef VERSION
#define VERSION "0.0.2"
#endif /* VERSION */

#ifndef REVISION
#define REVISION "unknown"
#endif /* VERSION */


//////////////////////////////////////////////////////////////////
// 開発のためのフラグ. ライブラリ利用者は変更不可

/** 検索中にハッシュを使う場合はこのフラグを定義する */
#define USE_HASH 1
// #undef USE_HASH

/** pin情報を使うか */
// #define USE_PIN 1
#undef USE_PIN

/** シリアルポートを使って対局する場合はこのフラグを定義する */
// #undef USE_SERIAL 


//////////////////////////////////////////////////////////////////
// ライブラリ利用者が選択する

/** 検索中にGUIのシグナルを処理する場合はこのフラグを定義する */
#define USE_GUI 1


/** デフォルトの制限時間 */
#define DEFAULT_LIMIT_TIME 1500


/** 手番インデックス */
typedef enum {
  SENTE = 0,
  GOTE  = 1
} TEBAN;


/** 駒種. 引き算はない */
typedef uint16_t PieceKind;

/** 駒種の定義 */
typedef enum {
  RYU     = 0x0F,
  UMA     = 0x0E,
  // skip
  NARIGIN = 0x0C,
  NARIKEI = 0x0B,
  NARIKYO = 0x0A,
  TOKIN   = 0x09,
  OH      = 0x08,
  HISHA   = 0x07,
  KAKU    = 0x06,
  KIN     = 0x05,
  GIN     = 0x04,
  KEI     = 0x03,
  KYO     = 0x02,
  FU      = 0x01
} KOMATYPE;

#define WALL 0x20


/** mate() が返すステータス */
enum MATE_STATE {
  /** 不詰み */
  NOT_CHECK_MATE = 0,

  /** 詰みあり */
  CHECK_MATE = 1,

  /** 不明(ハッシュからデータを取り出すときだけ使う) */
  UNKNOWN_CHECK_MATE = -1,
};


/** 手合 */
enum TEAI {
  /** 平手 */
  HIRATE = 0,

  /** 2枚落ち */
  NIMAIOTI = 6,

  /** 4枚落ち */
  YONMAIOTI = 7,

  /** その他 */
  TEAI_ETC = 10,
};


/** si_input_next_* が返すゲームのステータス */
enum INPUTSTATUS {
  /** 通常の手 */
  SI_NORMAL = 0,
  /** 手番のプレイヤーが勝ち(相手が反則手を指した) */
  SI_WIN = 1, 
  /** 手番のプレイヤーが投了 */
  SI_TORYO = 2,
  /** 手番のプレイヤーが待った */
  SI_MATTA = 3,
  /** 手番のプレイヤーがゲームを中断 */
  SI_CHUDAN = 4,
  /** 手番のプレイヤーが中断を受信 */
  SI_CHUDAN_RECV = 5,
  /** 手番のプレイヤーが入玉で勝ちを宣言 */
  SI_KACHI = 6,
  /** 手番のプレイヤーが入玉で引き分けを宣言 */
  SI_HIKIWAKE = 7,
  /** 送信されてきた手が不正な手だった */
  SI_NGDATA = 8,
  /** 手番のプレイヤーが時間切れ負け */
  SI_TIMEOUT = 9
};


typedef enum {
  /** 人間による入力 */
  SI_HUMAN = 0,
  /** プログラムレベル１による入力 */
  SI_COMPUTER_1 = 1,
  /** プログラムレベル２による入力 */
  SI_COMPUTER_2 = 2,
  /** プログラムレベル３による入力 */
  SI_COMPUTER_3 = 3,
  /** プログラムレベル４による入力 */
  SI_COMPUTER_4 = 4,
  /** プログラムレベル５による入力 */
  SI_COMPUTER_5 = 5,
#ifdef USE_SERIAL
  /** シリアルポートからの入力１ */
  SI_SERIALPORT_1 = 6,
  /** シリアルポートからの入力２ */
  SI_SERIALPORT_2 = 7,
#endif
  /** ネットワークからの入力１ */
  SI_NETWORK_1 = 8,
  /** 子プロセスとパイプで通信 */
  SI_CHILD_PROCESS = 9,
  /** GUIを使った人間による入力 */
  SI_GUI_HUMAN = 10
} PLAYERTYPE;


/** 盤上に出ている駒以外の駒を格納する構造体 */
typedef struct
{
  /** 駒の数を格納する */
  int count[9];
} PBOX;


/** ヒント情報 */
enum {
  TE_STUPID = 1,
  TE_CHECK = 2,
  TE_TSUMERO = 4,
};


typedef int16_t Xy;
typedef int16_t XyDiff;


/**
 * 指し手を表す構造体.
 *
 * \todo 取った駒 (captured) を記録するようにする. undoに必要. プレイヤー、成りは付けたまま.
 * \todo 移動する駒も記録する. utiの意味を変更し、打つ手はfmで判定するようにする。
 */
struct TE
{
  /** 移動元の場所 */
  Xy fm;

  /** 移動先の場所 */
  Xy to;

  /** 成り (promote) ならば true, それ以外は false をセットする */
  bool nari;

  /** 駒打ち (put) ならば打ち込む駒種、それ以外は 0 をセットする */
  PieceKind uti;

  /** ヒント情報. TE_STUPID */
  uint16_t hint;
};


/** 可能な合法手の最大は593 */
#define MOVEINFOMAX 593

/** 指し手 (候補手) を格納する構造体 */
typedef struct {
  /** 指し手 */
  TE te[MOVEINFOMAX + 1];
  /** 指し手の数 */
  int count;
} MOVEINFO;


#define MOVEINFO2MAX 2000

/** 指し手 (手順) を格納する構造体. */
typedef struct {
  /** 指し手 */
  TE te[MOVEINFO2MAX];

  /** 指し手の数 */
  int count;
} MOVEINFO2;


/** 移動先情報の最大引数 */
#define KIKI_FROM_MAX 10

typedef struct {
  Xy from[ KIKI_FROM_MAX ]; // short=10, long=5
  int count;
} KikiFrom;


/**
 * 局面の状態と棋譜.
 * 局面 = 盤面 + 持ち駒 + 手番.
 */
struct BOARD
{
  /** 局面の情報。*/
  //@{

  /** 1行追加の余白 */
  PieceKind ban_padding__[16];

  /** 
   * 盤面の駒種. 
      インデックスは0x11..0x99まで。1一 -> 0x11, 9九 -> 0x99. 
      2六 -> 0x62 (yxの順).
      駒種は下から1-3ビットが素の駒種、ビット4が成り、ビット5が
      プレイヤ (1=後手) */
  PieceKind board[16 * 12];

  /**
   * 持駒. [0]が先手、[1]が後手の持ち駒。1行128ビット
   * プレイヤー、成りは戻して格納。
   */
  int16_t inhand[2][8];

  /** 手番 */
  int next;

  /** 手順を記録しておく. 千日手検出 */
  MOVEINFO2 mi;

  /** 過去の局面のハッシュ値. [0] が初期局面の. \todo 非常に効率が悪い */
  uint64_t hash_history[ MOVEINFO2MAX + 1 ];

  /**
   * 取った駒があったら覚えておく.
   */
  PieceKind tori[MOVEINFO2MAX];

  //@}

  /** 利きなど、局面情報から生成できるもの */
  //@{

  /** 玉の位置 */
  Xy king_xy[2];

  /** 盤上以外の駒の駒コード入れ. 盤上と合わせて40枚 */
  PBOX pb;

  /**
   * ハッシュキー. 
   * 盤上の駒のみで、持ち駒は反映させない
   */
  uint64_t key;

  /** 持ち駒も含めたハッシュ値 */
  uint64_t hash_all;

  /** 
   * 駒ごとの利きの数. 自駒への利きもカウントする 
   * \todo think_eval() のなかでpinと同時にカウントする. 
   */
  int piece_kiki_count[16 * 10];

  /** それぞれの場所に対して利きを付けている駒の場所 */
  KikiFrom short_attack[2][16 * 10];
  KikiFrom long_attack[2][16 * 10];

#ifdef USE_PIN
  /**
   * pinされていればその方向。(上からなら+16など。)
   * たかだか一つの駒にしかpinされない。
   */
  XyDiff pinned[16 * 10];
#endif

  //@}

  //////////////////////////////////////////////////////////////
  // 思考用

  /** 
   * 局面の評価点数. boMove() で更新できるものだけ. 
   * この変数を直接使わずに、think_eval() で評価を得ること 
   */
  int32_t point[2];

  /** 消費時間の合計 */
  time_t total_time[2];
  /** 制限時間 */
  time_t limit_time[2];
  /** 思考開始前の時刻 */
  time_t before_think_time;
  /** １手づつの消費時間 */
  time_t used_time[MOVEINFO2MAX];
};


/** 指し手情報をツリー状に格納するための構造体 */
struct tagTREE {
  /** 直前の一手 */
  TE te;
  /** 親へのポインタ */
  struct tagTREE *parent;

  /** 兄弟へのポインタ */
  struct tagTREE* brother;

  /** 先頭の子へのポインタ. 子はchildとそのbrother. */
  struct tagTREE* child;

  /** 盤面の点数 */
  int point;
};

typedef struct tagTREE TREE;


/** 置換表のエントリ. 持ち駒も区別する. PODでなければならない */
struct EntVariant 
{
  /** 持駒 (packed). ビットの数で表すわけではないので、
      計算できるのは一致のみ. */
  uint32_t packed_inhand[2];

  /** 得点 */
  int32_t phi, delta;

  /** 記録したときの手数 (深さではない) */
  int32_t te_count;

  /** 最善手 */
  uint32_t best_move;
};
#define HASH_ENT_SIZE 24 // x86-64: 8の倍数


/** 盤面を記録する置換表のエントリ. PODでなければならない */
typedef struct 
{
  /** 持ち駒を含まないハッシュ値.
   \todo 削除する？ */
  uint64_t key;

  /** 得点. 複数記録する */
  struct EntVariant ent[1];
} HASH;
#define HASH_HEADER_SIZE 8 // ent[]は除く


/** ゲームの進行に必要な情報を集めた構造体 */
typedef struct 
{
  /** 手合 */
  TEAI teai;

  /** player type */
  PLAYERTYPE player_number[2];

  /** 入力を受ける関数へのポインタ */
  INPUTSTATUS (*si_input_next[2])(BOARD *bo, TE *te);

  /** コンピュータの思考レベル */
  int computer_level[2];
#ifdef USE_SERIAL
  /** シリアルポートを使い通信対局するのための構造体 */
  COMACCESS ca[2];
#endif /* USE_SERIAL */
  /** 1 の場合、時間切れは負けとする */
  int flg_run_out_to_lost;

  /**
   * 中断等が入ったときに入力待ちループから抜けるためのフラグ。
   * 0 ならば通常通り。
   * SI_CHUDAN ならば中断が入った。SI_TORYO ならば投了。
   * 中断または投了が入った場合、入力待ちループからすぐ抜ける。
   */
  INPUTSTATUS game_status;
  
  /**
   * 中断フラグへのポインタ
   */
  int *chudan;
} GAME;


/* ------------------- function prototype ------------------- */


////////////////////////////////////////////////////////////////////////
// BOARD: 局面

BOARD* newBOARDwithInit();

void freeBOARD(BOARD* bo);
void boInit(BOARD * bo);
void boPlayInit(BOARD *bo);

void boSetHirate(BOARD* bo);

bool bo_set_by_text( BOARD* bo, const char* s);

bool bo_load_file( BOARD* bo, const char* filename );


/** 駒種をセットする */
static inline void boSetPiece(BOARD* bo, Xy xy, PieceKind p)
{
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( 1 <= (xy & 0xF) && (xy & 0xF) <= 9 );

  bo->board[xy] = p;
}


void boSetTsumeShogiPiece(BOARD* bo);

void boSetToBOARD(BOARD* bo);

/** 局面を複製する */
BOARD* bo_dup(const BOARD* bo);


/**
 * xyにある駒の駒種を得る.
 * @param bo 対象のBOARD
 * @param xy 座標. 1手分だけはみ出してもよい
 */
static inline PieceKind boGetPiece(const BOARD* bo, Xy xy)
{
  assert(-1 <= (xy >> 4) && (xy >> 4) <= 11);
  assert(0 <= (xy & 0xF) && (xy & 0xF) <= 10);

  return bo->board[xy];
}


/** sideによる攻撃 (利き) を数える */
static inline int bo_attack_count(const BOARD* bo, int side, Xy xy)
{
  assert(xy >= 0x11 && xy <= 0x99);
  assert(side == 0 || side == 1);
  return bo->short_attack[side][xy].count + bo->long_attack[side][xy].count;
}


/** side側の玉に王手が掛かっているか */
static inline bool bo_is_checked(const BOARD* bo, int side)
{
  assert( side == 0 || side == 1 );
  return bo->king_xy[side] &&
         bo_attack_count(bo, 1 - side, bo->king_xy[side]);
}


/** 局面を手 te で進める. 局面 bo を更新する */
int boMove(BOARD* bo, const TE& te);

void boBack(BOARD* bo);

void boMove_mate(BOARD* bo, const TE& te);
void boBack_mate(BOARD* bo);

/** パスする */
void bo_pass( BOARD* bo );

/** 手が合法手か */
bool is_valid_move(const BOARD* bo, const TE* te);

bool is_move_sennichite(const BOARD* bo, const TE* te, bool* forbidden);

int can_move_promote(const BOARD* bo, const TE* te);

void boSetNowTime(BOARD *bo);
void boSetUsedTime(BOARD *bo);

/** 本将棋として局面が妥当かどうか */
int boCheckJustGame(const BOARD* bo);

int boCheckJustMate(const BOARD* bo);

/** record.cc から参照される */
void bo_update_board_by_csa_line(BOARD* bo, const char* s, int* error);

int boCmp(const BOARD* bo1, const BOARD* bo2);

XyDiff norm_direc(Xy xy1, Xy xy2) __attribute__((const));

/** xyにある駒による長い利きを付ける */
void bo_add_long_attack(BOARD* bo, Xy xy);

/** xyにある駒による長い利きを取り除く */
void bo_remove_long_attack(BOARD* bo, Xy xy, PieceKind pie);

/** xyにある駒の利き情報を付ける */
void bo_movetoKiki(BOARD* bo, Xy xy);

/** xyにある駒の利き情報を取り除く */
void boPopToBoard(BOARD* bo, Xy xy);

void printBOARD(const BOARD* bo);


////////////////////////////////////////////////////////////////////////
// MOVEINFO: 候補手

extern const XyDiff arr_round_to[];
extern const XyDiff arr_round_to24[];

MOVEINFO* newMOVEINFO();

void freeMOVEINFO(MOVEINFO* mi);


/** mi に指し手を追加する */
static inline void miAdd( MOVEINFO* mi, const TE& te )
{
  assert( mi->count < MOVEINFOMAX );
  mi->te[mi->count++] = te;
}


/** miに手を追加する。成らずと成りの両方。 */
void miAddWithNari( MOVEINFO* mi, const BOARD& board, const TE& te );

void mi_add_with_pin_check( MOVEINFO* mi, const BOARD& bo, const TE& te );

void miRemoveTE2(MOVEINFO *mi, int num );
void miRemoveDuplicationTE(MOVEINFO *mi);

/** コピーする */
void miCopy(MOVEINFO* dest, const MOVEINFO* src);

void mi_print( const MOVEINFO* mi, const BOARD& bo );
bool mi_includes( const MOVEINFO* mi, const TE& te );

void mi_append_king_moves(const BOARD* bo, MOVEINFO* mi, Xy king_xy, int next);


////////////////////////////////////////////////////////////////////////
// misc

void si_abort(const char* fmt, ...) __attribute__((noreturn));

void ary_erase(int16_t* begin, int16_t* end, int16_t value);


////////////////////////////////////////////////////////////////////////
// MOVEINFO2: 手順

MOVEINFO2* newMOVEINFO2();

void freeMOVEINFO2(MOVEINFO2* mi);

void mi2Copy(MOVEINFO2* dest, const MOVEINFO2* src);


/**
 * MOVEINFO2 に TE を追加する。
 * @param mi 対象のmi
 * @param te 追加するte
 */
static inline void mi2Add(MOVEINFO2* mi, const TE& te) 
{
  assert(mi != NULL);
  assert( mi->count < MOVEINFO2MAX );

  mi->te[mi->count++] = te;
}


void mi2_print_sequence( const MOVEINFO2* mi, const BOARD& bo );


////////////////////////////////////////////////////////////////////////
// TE: 指し手

/** 手を生成 */
TE te_make_move(Xy from, Xy to, bool promote);

TE te_make_put(Xy to, PieceKind pie);


/** 手をクリアする */
static inline void te_clear(TE* te)
{
  te->fm = 0; te->to = 0; te->nari = 0; te->uti = 0; te->hint = 0;
}


/** 置換表に登録するために、指し手をpackする */
static inline uint32_t te_pack(const TE* te) 
{
  return (te->fm << 24) + (te->to << 16) + (te->nari ? 0x8000 : 0) + 
    (te->uti << 10) + (te->hint & 0x3ff);
}


/** TE に戻す */
static inline TE te_unpack(uint32_t packed_te)
{
  TE te;
  te.fm = (packed_te >> 24) & 0xff; // 8bit
  te.to = (packed_te >> 16) & 0xff; // 8bit
  te.nari = (packed_te >> 15) & 1; // 1bit
  te.uti = (packed_te >> 10) & 0x1f; // 5bit. TODO: プレイヤビットも格納
  te.hint = packed_te & 0x3ff; // 10bit
  return te;
}


/** 打つ手かどうか */
static inline bool te_is_put(const TE* te)
{
  return !te->fm && te->to;
}


/**
 * 手を比較する
 * @param te1 比較する手１
 * @param te2 比較する手２
 * @return 同じならば 0 、違いがあれば非 0 を返す
 */
static inline int teCmpTE( const TE* te1, const TE* te2 ) 
{
  assert( te1 != NULL );
  assert( te2 != NULL );

  if ( te1->fm == te2->fm &&
       te1->to == te2->to &&
       te1->nari == te2->nari &&
       te1->uti == te2->uti ) {
    return 0;
  }
  
  return 1;
}


static inline bool operator == (const TE& x, const TE& y) {
  return !teCmpTE(&x, &y);
}

std::ostream& operator << (std::ostream& ost, const TE& te);


/** デバッグ用に手を文字列化 */
std::string te_str( const TE* te, const BOARD& bo );

void teSetTE(TE* te, Xy fm, Xy to, bool nari, PieceKind uti);

int teCSAimoto(TE *te);
int teCSAisaki(TE *te);
int teCSAinaru(TE *te);
void teCSASet(TE *te, int imoto, int isaki);


/* moveto.cc */

extern const int remote_to_count[];
extern const XyDiff remote_to[][8];
extern const XyDiff normal_to[][8];
extern const int normal_to_count[];

/** 合法手をすべて生成する */
void make_moves(const BOARD* bo, MOVEINFO* mi);

/** xyにある駒での指し手をmiに追加 */
void mi_append_moveto(const BOARD* bo, Xy xy, MOVEINFO* mi);

void moveto2_add(const BOARD* bo, MOVEINFO* mi, Xy xy, int next);

/** 駒 pie を xy に打つ手を生成 */
void append_a_put_move( const BOARD* bo, MOVEINFO* mi, PieceKind pie, Xy xy );

void mi_append_put_moves( const BOARD* bo, Xy xy, MOVEINFO* mi );


/* pbox */
PBOX *newPBOX(void);
void freePBOX(PBOX *pb);
void pbInit(PBOX * pb);

void pbPush(PBOX* pb, PieceKind p );

void pbPop(PBOX* pb, PieceKind p);

int pbCheck(PBOX * pb);


/* ohte.cc */

void ohte(const BOARD* bo, MOVEINFO* mi);

void normalOhte(const BOARD *bo, MOVEINFO *mi, Xy king_xy);
// void remoteOhte(const BOARD* bo, MOVEINFO *mi, Xy king_xy);
// void keiOhte(const BOARD* bo, MOVEINFO* mi, Xy king_xy);
// void utiOhte(const BOARD *bo, MOVEINFO *mi, Xy king_xy);
// void UtiOhte2(const BOARD* bo, MOVEINFO* mi, Xy king_xy);
// void akiOhte(const BOARD* bo, MOVEINFO* mi, Xy king_xy);



/* uke.c */

/** 王手を受ける手を生成する */
int uke(const BOARD* bo, MOVEINFO* mi );


/* mate.cc */

MATE_STATE dfpn_mate( BOARD* bo, int depth, TE* best_move, int* node_limit );

MATE_STATE bfs_hisshi( BOARD* bo, int tesuki, int depth, TE* best_move, 
		       int* node_limit );

  
/* tree */

void initTREE(TREE *tree);
void freeTREE(TREE *tree);
void trSetMoveinfo(TREE *tree, MOVEINFO *mi);


/* hash.cc */

extern const uint64_t hashseed_inhand[2][8][19];
extern const uint64_t hashval[32][16 * 10];
struct PhiDelta;

/** 置換表を初期化する */
void newHASH();

void freeHASH();

uint64_t hsGetHashKey(const BOARD* bo);

/** 局面を置換表に登録する。詰将棋用 */
void hsPutBOARD( int tag, const BOARD* bo, int32_t phi, int32_t delta,
                 int deep, const TE* best_move );

int hsGetBOARD( int tag, const BOARD* bo, int32_t* phi, int32_t* delta, TE* te,
                int req_deep );

void hsPutBOARDMinMax( const BOARD* bo, int32_t phi, int32_t delta, 
		       int deep, const TE* best_move );

int hsGetBOARDMinMax( const BOARD* bo, PhiDelta* score, TE* te, int te_count );

void bo_add_hash_value(BOARD* bo, Xy xy);
void bo_sub_hash_value(BOARD* bo, Xy xy);

int hs_entry_count(int tag);

bool hs_minimize_proof_pieces(int offense_side, BOARD* bo);
void hs_get_best_sequence(int level, const BOARD* bo, MOVEINFO2* mi);


/* play */
void play(BOARD *bo, GAME *gm);

void play_move(BOARD* bo, const TE* te);

void getTE2SerialString(char *buf, BOARD *bo, TE *te);
void play_lost(BOARD *bo, GAME *gm);
void play_toryo(BOARD *bo, GAME *gm);
void play_win(BOARD *bo, GAME *gm);
void play_chudan(BOARD *bo, GAME *gm);
void play_chudan_recv(BOARD *bo, GAME *gm);
void play_ngdata(BOARD *bo, GAME *gm);
void putHelp(void);

/* think */
void think(BOARD *bo, MOVEINFO *mi, int depth);

bool think_is_tsumero(BOARD* bo, int teban);
void think_tsumero_uke(BOARD* bo, MOVEINFO* mi_ret);

bool think_hisshi(BOARD* bo, int tenuki, int depth, TE* te_ret);

void think_eval(const BOARD* bo, PhiDelta* score);

int think_count_attack24( const BOARD* bo, int side, Xy target );
int think_count_attack8( const BOARD* bo, int side, Xy target );


/* minmax */

INPUTSTATUS bfs_minmax(BOARD* bo, int depth, TE* best_move);
int jouseki(const BOARD* bo, TE* te);


/* GAME */
GAME *newGAME(void);
void initGAME(GAME *game);
void freeGAME(GAME *game);

typedef INPUTSTATUS (*InputCallbackFunc)(BOARD* bo, TE* te);
InputCallbackFunc gmGetInputNextPlayer(GAME* gm, int next);

char *gmGetDeviceName(GAME *gm, int next);
int gmGetComputerLevel(GAME *gm, int next);

/** プレイヤー種別とコールバック関数を設定 */
void gmSetInputFunc(GAME* gm, int next, PLAYERTYPE p, InputCallbackFunc fn);

void gmSetDeviceName(GAME *gm, int next, char *s);

void gmSetGameStatus(GAME* gm, INPUTSTATUS status);


/**
 * ゲームのステータスを返す。
 * @param gm 対象のGAME
 * @return ゲームのステータス (SI_CHUDAN など)
 */
static inline INPUTSTATUS gmGetGameStatus(const GAME* gm) 
{
  assert(gm->game_status == SI_NORMAL ||
         gm->game_status == SI_CHUDAN ||
         gm->game_status == SI_TORYO);

  return gm->game_status;
}


void gmSetChudan(GAME *gm, int *chudan);
int *gmGetChudan(GAME *gm);
int gmGetChudanFlg(GAME *gm);

/* misc */

void time2string(char *buf, time_t t);


static int getTeban(PieceKind p) __attribute__((const));

/**
 * 駒種類の手番を返す.
 * @param p 駒種類
 * @return 0ならば先手、1ならば後手。
 */
static inline int getTeban(PieceKind p)
{
  assert( p != 0 );
  assert( p != WALL );

  return (p & 0x10) == 0x10;
}




int notSentePiece(int p);
int notGotePiece(int p);

/** 歩がすでにないかどうか */
int checkFuSente(const BOARD* bo, int x);
int checkFuGote(const BOARD* bo, int x);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _SI_H_ */
