/* -*- mode:c++ -*-
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __SI_THINK__
#define __SI_THINK__ 1

#include "si.h"
#include <stdio.h>


/** 評価関数のパラメータ */
struct NPC
{
  /** 盤上の自分の駒 */
  int32_t piece_values[16];

  /** 自分の持ち駒 */
  int32_t inhand_pieces[8];

  /** 大駒の利き. 香, 飛, 角, 竜, 馬 */
  int32_t movables[5];

  /** 相手玉への利き */
  int32_t attack_king[24];
};


/** 
 * 駒の評価点の基礎値.
 * \todo NPCへ移行
 */
enum {
  /* 歩が上に行く程可算する点数 */
  FU_POSITION_POINT = 10,

  /* 自分の王への距離の評価点の率。減らす程、激しく反応する */
  MY_OH_DISTANCE_POINT = 200,

  /* 相手の王への距離の評価点の率。減らす程点数が高くなる */
  ONES_OH_DISTANCE_POINT = 100,

  /** 
   * 王の周りの安全度の点数。
   * 相手の利きが3ある場合は 3 × OH_SAFE_POINT を盤面評価から差し引く。
   */
  OH_SAFE_POINT = 100,

  /** 駒の動きの評価. (駒の動ける数 * NUM_OF_MOVE_POINT) を加算する。 */
  NUM_OF_MOVE_POINT = 4,

  /* 王と飛車が近いときに減点するポイント */
  OH_HISYA_POINT = 10,
};

/** 勝ちの値. 評価ルーチンでの値域の外側でなければならない */
#define VAL_INF 300000

/** 逃げ切った. 詰め将棋でだけ使う */
#define VAL_MAY_MATCH (VAL_INF - 10)


/** 通常探索内部での詰め探索のノード数 */
#define GAME_MATE_NODE_LIMIT 1500


/* grade */
extern const int arr_xy[];
extern const int arr_point[]; // TODO: 思考ルーチンへ
extern const int arr_piece[]; // TODO: 思考ルーチンへ
extern const int arr_point_xy[17][9];
extern const int arr_point_xy2[17][9];
extern const int king_score_ary[];

int grade(const BOARD* bo, int next);
int add_grade(const BOARD* bo, Xy xy, int next );

int score_inhand( const BOARD* bo, PieceKind piece, int next );

int oh_safe_grade(const BOARD* bo, Xy king_xy, int next);

int add_oh_safe_grade(const BOARD* bo, int next);

void sub_oh_safe_grade(BOARD *bo, int next);
void add_moving_grade(BOARD *bo, int xy, int next);
void sub_moving_grade(BOARD *bo, int xy, int next);

/* search */
void search_011(BOARD *bo, MOVEINFO *mi);
void search_012(BOARD *bo, MOVEINFO *mi);
void search_013(BOARD *bo, MOVEINFO *mi);
void search_014(BOARD *bo, MOVEINFO *mi);
void search_022(BOARD *bo, MOVEINFO *mi);
void search_023(BOARD *bo, MOVEINFO *mi);
void search_024a(BOARD *bo, MOVEINFO *mi);
void search_027(BOARD *bo, MOVEINFO *mi);
void search_031a(BOARD *bo, MOVEINFO *mi);
void search_031b(BOARD *bo, MOVEINFO *mi);
void search_041(BOARD *bo, MOVEINFO *mi);
void search_044(BOARD *bo, MOVEINFO *mi);
void search_046(BOARD *bo, MOVEINFO *mi);
void search_047(BOARD *bo, MOVEINFO *mi);
void search_048(BOARD *bo, MOVEINFO *mi);
void search_049(const BOARD* bo, MOVEINFO* mi);
void search_053(BOARD *bo, MOVEINFO *mi);
void search_068(BOARD *bo, MOVEINFO *mi);
void search_pressure(const BOARD* bo, MOVEINFO* mi, int to_xy, int next);
void search_pressure_uti(BOARD *bo, MOVEINFO *mi, int to_xy, int next);

struct TsumeroNogare
{
  int ac, mc, mkm;
  int offense_side;

  void before_move(const BOARD* node, int offense_side_);

  bool promise(const BOARD* node) const;
};


/** 受け側の無駄打ちを負けとして置換表に登録 */
template <typename PutWinOp>
void mate_set_muda_uchi(int offense_side, int tesuki, 
                        const PutWinOp& put_win, BOARD* node, 
                        const MOVEINFO* cand_moves, int start_index)
{
  assert( offense_side == node->next );

#if DEBUGLEVEL >= 1
  printf("%s: enter.\n", __func__); // DEBUG
  printBOARD(node); // DEBUG
#endif // DEBUGLEVEL
  
  int i, j, k;

  // 元の手順を得る
  MOVEINFO2 seq;
  hs_get_best_sequence(tesuki - 1, node, &seq);

#if DEBUGLEVEL >= 1
  mi2_print_sequence(node, &seq);
#endif // DEBUGLEVEL

  boBack_mate(node); // 受け側の打つ手を戻す
  // printf("bad move: %s\n", te_str(&node->mi.te[node->mi.count], *node).c_str());

  BOARD* bo2 = bo_dup(node);
#if DEBUGLEVEL >= 1
  BOARD* debug_bo = bo_dup(node); // DEBUG
#endif // DEBUGLEVEL

  // node は元に戻しておく
  boMove_mate(node, node->mi.te[node->mi.count]);

  // 詰め手順に影響がなければ無駄打ち
  for (i = start_index; i < cand_moves->count; i++) {
    if ( !te_is_put(&cand_moves->te[i]) )
      continue;

    boMove_mate(bo2, cand_moves->te[i]); // 打つ手を試す
    Xy pie_xy = cand_moves->te[i].to;
/*
    int32_t phi, delta;
    if ( hsGetBOARD(tt_tag(param), bo2, &phi, &delta, NULL, param.te_count) ) {
      printf("dupli!!\n");
      // boBack_mate(bo2);
      break;
    }
*/
    bool may_not_lose = false;
    for (j = 0; j < seq.count; j++) {
      MOVEINFO mi;
      make_moves(bo2, &mi);

      if ( (j % 2) == 1 ) {
        // 受け方.
        
        // 元の手順では投了だが、試した手では続けられることがある.
        if (!seq.te[j].to) {
          if ( mi_includes(&mi, te_make_move(pie_xy, seq.te[j - 1].to, 
					     false)) ||
               mi_includes(&mi, te_make_move(pie_xy, seq.te[j - 1].to, 
					     true)) ) {
            may_not_lose = true;
            printf("rsn 1 ");
            break;
          }
          break;
        }

        // 元の手順の受けが指せない
        if ( !mi_includes(&mi, seq.te[j]) ) {
          // 同じところだが違う駒を打った
          // 違うところに打った
          bool can_move = mi_includes(&mi, te_make_move(pie_xy, seq.te[j].to, 
							false));
          bool can_move_with_p = mi_includes(&mi, te_make_move(pie_xy, 
							 seq.te[j].to, true));
          if ( !can_move && ! can_move_with_p ) {
            // 試す駒でも受けられない -> 負け
            break;
          }

          if ( (can_move && !can_move_with_p) || 
	       (!can_move && can_move_with_p) ||
               seq.te[j + 1].to == seq.te[j].to ) {
            // 手を継いで進める
            boMove_mate(bo2, te_make_move(pie_xy, seq.te[j].to, 
					  can_move_with_p));
            pie_xy = seq.te[j - 1].to;
            continue;
          }
          
          // 不明
          may_not_lose = true;
          printf("rsn 2 ");
          break;
        }

        // 新たに取る手が発生
        if ( seq.te[j].to != seq.te[j - 1].to && 
             (mi_includes(&mi, te_make_move(pie_xy, seq.te[j - 1].to, false)) ||
              mi_includes(&mi, te_make_move(pie_xy, seq.te[j - 1].to, true))) ) {
          // TODO: 攻め方の大駒の移動経路にある受け方の利きのある場所への移動
          // TODO: 元の手順で取れない場合のみ
          may_not_lose = true;
          // printf("rsn 3 ");
          break;
        }

        if (seq.te[j].fm == pie_xy)
          pie_xy = seq.te[j].to;
      }
      else {
        // 攻め方
        if ( !mi_includes(&mi, seq.te[j]) ) {
          may_not_lose = true;
          // printf("rsn 4 ");
          break; // 影響あり
        }

        if (seq.te[j].to == pie_xy) {
          // 打ち駒を取る -> 以降に影響なし
          break;
        }
      }

      boMove_mate(bo2, seq.te[j]);
    }

    // 巻き戻す
    for (k = 0; k < j; k++)
      boBack_mate(bo2);

    if (!may_not_lose) {
      assert( offense_side == bo2->next );
      put_win(bo2, &seq.te[0]);
      // hsPutBOARD( tt_tag(param), bo2, 0, +VAL_INF, param.te_count, &seq.te[0] );
    }
    else {
#if DEBUGLEVEL >= 1
      printf("not cut: %s\n", te_str(debug_bo, &cand_moves->te[i]).c_str() );
#endif 
    }

    boBack_mate(bo2);
  }

  freeBOARD(bo2);
#if DEBUGLEVEL >= 1
  freeBOARD(debug_bo); // DEBUG
#endif
}


#endif // __SI_THINK__
