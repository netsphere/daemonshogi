/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <list>
#include "dte.h"
#include "dmoveinfo.h"
#include "dboard.h"
#include "record.h"
#include "filereader.h"
#include "filewriter.h"
#include "misc.h"
using namespace std;



/** Record を初期化 */
static void daemon_record_init(Record* record) 
{
  assert(record != NULL);

  record->teai = D_HIRATE;
  daemon_dboard_init(&(record->first_board));
  record->start_time = 0;

  record->player_name[0] = "";
  record->player_name[1] = "";
  record->set_error("not open");
}


Record::Record() 
{
  mi = new KyokumenNode(NULL, NULL);
  daemon_record_init(this);
}


/**
 * コンストラクタ.
 */
Record* daemon_record_new() 
{
  Record* record = new Record();
  if (record == NULL) {
    printf("No enough memory in daemon_record_new().");
    abort();
  }

  return record;
}


/** デストラクタ */
void daemon_record_free(Record* record) 
{
  if (record)
    delete record;
}


static const char* csa_koma_str[] = {
  "",   "FU", "KY", "KE", "GI", "KI", "KA", "HI",
  "OU", "TO", "NY", "NK", "NG", "",   "UM", "RY",
};

static void csa_output_seq_sub(DBoard* bo, const KyokumenNode* node,
			      int te_count,
			      FileWriter& writer)
{
  char buf[80];
  int i;
  // int te_count = 0;

  for (i = 0; i < node->moves.size(); i++) {
    if ( i != 0 ) {
      sprintf(buf, "'手数目:%d\n", te_count + 1);
      writer << buf;
    }

    const MoveEdge mv = node->moves[i];
    DTe te = mv.te;

    if (te.special) {
      // 投了など
      switch (te.special) {
      case DTe::RESIGN:
	sprintf(buf, "%s\n", "%TORYO");
	break;
      case DTe::KACHI:
	sprintf(buf, "%s\n", "%KACHI");
	break;
      case DTe::CHUDAN:
	sprintf(buf, "%s\n", "%CHUDAN");
	break;
      default:
	assert(0);
      }
    }
    else if ( te_is_put(&te) ) {
      /* 駒打ち */
      sprintf(buf, "%c%d%d%d%d%s\n",
	      (bo->next == D_SENTE) ? '+' : '-',
	      0, 0,
	      te.to & 0xF, te.to >> 4,
	      csa_koma_str[te.uti]);
    } 
    else {
      /* 駒移動 */
      PieceKind p = daemon_dboard_get_board(bo, te.fm & 0xF, te.fm >> 4);
      p += te.nari ? 0x08 : 0;
      sprintf(buf, "%c%d%d%d%d%s\n",
	      (bo->next == D_SENTE) ? '+' : '-',
	      te.fm & 0xF, te.fm >> 4,
	      te.to & 0xF, te.to >> 4,
	      csa_koma_str[p & 0xF]);
    }
    writer << buf;
    sprintf(buf, "T%d\n", mv.time);
    writer << buf;
    
    if ( !te.special ) {
      daemon_dboard_move(bo, &te);
      csa_output_seq_sub(bo, node->lookup_child(te), te_count + 1, writer);
      daemon_dboard_back(bo, &te);
    }
  }

#if 0
  switch (record->matchstat) {
  case D_RESIGN:
    daemon_filewriter_put(writer, "%TORYO");
    break;
  case D_MATTA:
    daemon_filewriter_put(writer, "%MATTA");
    break;
  case D_SENNICHITE:
    daemon_filewriter_put(writer, "%SENNICHITE");
    break;
  case D_JISHOGI:
    daemon_filewriter_put(writer, "%JISHOGI");
    break;
  case D_TSUMI:
    daemon_filewriter_put(writer, "%TSUMI");
    break;
  case D_FUZUMI:
    daemon_filewriter_put(writer, "%FUZUMI");
    break;
  case D_ERROR:
    daemon_filewriter_put(writer, "%ERROR");
    break;
  default:
    assert(0); // unknown
  }
#endif // 0
}


/**
 * 棋譜 (手順木) を出力する.
 */
static void daemon_record_output_csa_te(const Record* record, 
					FileWriter* writer) 
{
  DBoard* bo = daemon_dboard_new();
  daemon_dboard_copy(&(record->first_board), bo);

  daemon_filewriter_put(writer, "'  指し手\n");
  csa_output_seq_sub(bo, record->mi, 0, *writer);

  daemon_dboard_free(bo);
}


/** CSA形式でファイル filename に出力する。 */
int daemon_record_output_csa(Record* record, FileWriter* writer )
{
  return daemon_record_output_csa_code(record, writer, "CP932");
}


/** 持ち駒出力 */
static void daemon_record_output_csa_piece(const Record* record, 
					   FileWriter* writer) 
{
  const DBoard* board;
  int p, j, i;
  char buf[BUFSIZ];

  board = &(record->first_board);

  daemon_filewriter_put(writer, "'  先手持駒\n");
  for (j=7; 1 <= j; j--) {
    p = daemon_dboard_get_piece(board, D_SENTE, j);
    for (i=0; i<p; i++) {
      sprintf(buf, "P+00%s\n", csa_koma_str[j]);
      daemon_filewriter_put(writer, buf);
    }    
  }

  daemon_filewriter_put(writer, "'  後手持駒\n");
  for (j=7; 1 <= j; j--) {
    p = daemon_dboard_get_piece(board, D_GOTE, j);
    for (i=0; i<p; i++) {
      sprintf(buf, "P-00%s\n", csa_koma_str[j]);
      daemon_filewriter_put(writer, buf);
    }
  }
}


static void daemon_record_output_csa_board(const Record* record, 
					   FileWriter* writer) 
{
  const DBoard* bo;
  int p;
  int x, y;
  char buf[BUFSIZ];

  bo = &(record->first_board);

  for (y=1; y <= 9; y++) {
    sprintf(buf, "P%d", y);
    daemon_filewriter_put(writer, buf);
    for (x=9; 1 <= x; x--) {
      p = daemon_dboard_get_board(bo, x, y);
      if (p) {
	sprintf(buf, "%c%s", (p & 0x10) ? '-' : '+', csa_koma_str[p & 0x0F]);
	daemon_filewriter_put(writer, buf);
      } else {
	daemon_filewriter_put(writer, " * ");
      }
    }
    daemon_filewriter_put(writer, "\n");
  }
}


/** CSA形式でファイル filename に出力する。 */
static int daemon_record_output_csa_p(const Record* record, FileWriter& writer) 
{
  assert(record != NULL);

  writer << "V2.2\n";

  daemon_filewriter_put(&writer, "'  対局者名\n");
  writer << "N+" << 
    (record->player_name[0] != "" ? record->player_name[0] : "") << "\n";

  writer << "N-" <<
    (record->player_name[1] != "" ? record->player_name[1] : "") << "\n";

  daemon_filewriter_put(&writer, "'  盤面\n");
  daemon_record_output_csa_board(record, &writer);
  daemon_record_output_csa_piece(record, &writer);
  daemon_filewriter_put(&writer, "'  手番\n");
  daemon_filewriter_put(&writer, record->first_board.next ? "-\n" : "+\n");
  daemon_record_output_csa_te(record, &writer);

  return 0;
}


/**
 * CSA形式でファイル filename に出力する。
 * @param record 棋譜
 * @param filename ファイル名
 * @param code 文字コード
 */
int daemon_record_output_csa_code(Record* record, 
				  FileWriter* writer,
				  const char* code) 
{
  assert(record != NULL);
  assert( writer != NULL);
  assert(code);

  int r;
  
  // writer = daemon_filewriter_new();
  daemon_filewriter_set_outcode(writer, code);
/*
  r = daemon_filewriter_open(writer, filename);
  if (r == -1) 
    return -1;
*/
  if (string(code) == "CP932")
    daemon_filewriter_put(writer, "' -*- coding:shift_jis -*-\n");

  r = daemon_record_output_csa_p(record, *writer);
  // daemon_filewriter_close(writer);

  return r;
}


void daemon_record_create_time_string(char *buf, time_t t, time_t total) {
  time_t min, sec;
  time_t total_hour, total_min, total_sec;

  min = t / 60;
  sec = t % 60;
  total_hour = total / 3600;
  total = total % 3600;
  total_min  = total / 60;
  total_sec  = total % 60;

  sprintf(buf, "(%2d:%02d/%02d:%02d:%02d)",
	  (int)min,
	  (int)sec,
	  (int)total_hour,
	  (int)total_min,
	  (int)total_sec);
}


static const char* knum_data[] = {
  "",  "一", "二", "三", "四", "五", "六", "七", "八", "九",
};

static const char* anum_data[] = {
  "",  "１", "２", "３", "４", "５", "６", "７", "８", "９", 
};

static const char* kif_koma_data[] = {
  "",   "歩", "香", "桂", "銀", "金", "角", "飛",
  "玉", "と", "杏", "圭", "全", "",   "馬", "龍",
};


/** 持ち駒を出力 */
static void daemon_record_output_kif_piece(const DBoard* board, 
				    FileWriter* writer, 
				    D_TEBAN next) 
{
  int count;
  int i;

  if (next == D_SENTE) {
    daemon_filewriter_put(writer, "先手の持駒：");
  } else {
    daemon_filewriter_put(writer, "後手の持駒：");
  }

  for (i=1; i<=7; i++) {
    if (daemon_dboard_get_piece(board, next, i) != 0) 
      break;
  }
  if (i == 8) {
    /* 持ち駒なし */
    daemon_filewriter_put(writer, "なし");
  }

  for (i=7; 1 <= i; i--) {
    count = daemon_dboard_get_piece(board, next, i);
    if (count == 0) {
      continue;
    }
    daemon_filewriter_put(writer, kif_koma_data[i]);
    if (count == 1) {
      daemon_filewriter_put(writer, "　");
      continue;
    }
    if (10 <= count) {
      daemon_filewriter_put(writer, "十");
      count -= 10;
      if (count == 0) {
	daemon_filewriter_put(writer, "　");
	continue;
      }
    }
    daemon_filewriter_put(writer, knum_data[count]);
    daemon_filewriter_put(writer, "　");
  }

  daemon_filewriter_put(writer, "\n");
}


static const char* long_koma_data[] = {
  "",   "歩", "香",   "桂",   "銀",   "金", "角", "飛",
  "玉", "と", "成香", "成桂", "成銀", "",   "馬", "龍",
};


struct ElapsedTime {
  time_t time[2];
};


/** boostの代替 */
template <typename Container>
void str_split( Container* container, const string& str, char delim )
{
  int pos = 0;
  int i;
  for (i = 0; i < str.size(); i++) {
    if ( str[i] == delim ) {
      container->push_back( string(str, pos, i - pos) );
      pos = i + 1;
    }
  }
  container->push_back( string(str, pos, i - pos) );
}


static int kif_output_seq_sub(DBoard* board, 
			      int te_count, 
			      ElapsedTime total_time,
			      Xy last_xy,
			      const KyokumenNode* node,
			      FileWriter* writer)
{
  int i;
  char buf[BUFSIZ];
  int back_count = 0;
  char time_buf[BUFSIZ];

  if ( !node->moves.size() ) {
    daemon_record_create_time_string(time_buf, 0, 
				     total_time.time[board->next]);
    sprintf(buf, "%4d 中断         %s\n", te_count + 1, time_buf);
    daemon_filewriter_put(writer, buf);
    sprintf(buf, "まで%d手で中断\n", te_count);
    daemon_filewriter_put(writer, buf);
  }

  for (i = 0; i < node->moves.size(); i++) {
    if (i && back_count > 0) {
      sprintf(buf, "\n変化：%d手\n", te_count + 1 );
      daemon_filewriter_put(writer, buf);
    }

    const MoveEdge& mv = node->moves[i];
    DTe te = mv.te; 

    if (te.special) {
      switch (te.special) {
      case DTe::RESIGN:
        sprintf(buf, "%4d 投了        ", te_count + 1);
	break;
      case DTe::KACHI:
	assert(0); // 不明
	break;
//    case DTe::CHUDAN:
//      sprintf(buf, "%4d 中断        ", te_count + 1);
//	break;
      default:
	assert(0);
      }
    }
    else if ( te_is_put(&te) ) {
      sprintf(buf, "%4d %s%s%s打    ",
	      te_count + 1,
	      anum_data[te.to & 0xF],
	      knum_data[te.to >> 4],
	      long_koma_data[te.uti]);
      last_xy = te.to;
    } 
    else {
      PieceKind p = daemon_dboard_get_board(board, te.fm & 0xF, te.fm >> 4);
      p &= 0xF;
      if (last_xy == te.to) {
	sprintf(buf, "%4d 同　%s%s(%d%d)%s",
		te_count + 1,
		long_koma_data[p],
		te.nari ? "成" : "",
		te.fm & 0xF,
		te.fm >> 4,
		te.nari ? "" : "  " );
      } 
      else {
	sprintf(buf, "%4d %s%s%s%s(%d%d)%s",
		te_count + 1,
		anum_data[te.to & 0xF],
		knum_data[te.to >> 4],
		long_koma_data[p],
		te.nari ? "成" : "",
		te.fm & 0xF, 
		te.fm >> 4,
		te.nari ? "" : "  " );
      }
      last_xy = te.to;
    }
    total_time.time[board->next] += mv.time;
    daemon_record_create_time_string(time_buf, mv.time, 
				     total_time.time[board->next]);
    sprintf( buf + strlen(buf), " %s%s\n", time_buf,
	     node->moves.size() > i + 1 ? "+" : "" );
    daemon_filewriter_put(writer, buf);

    if ( mv.te_comment != "" || mv.annotation ) {
      vector<string> c;

      if ( mv.annotation == ANNOT_POOR )
	*writer << "*[?]";
      else if ( mv.annotation == ANNOT_QUESTIONABLE )
	*writer << "*[?!]";
      else 
	*writer << "*";

      str_split(&c, mv.te_comment, '\n');
      for (int i = 0; i < c.size(); i++) {
	if (i)
	  *writer << "*";
	*writer << c[i] << "\n";
      }
    }

    if ( te.special ) {
      sprintf( buf, "まで%d手で%sの勝ち\n", 
	       te_count, (board->next == D_SENTE ?  "後手" : "先手") );
      daemon_filewriter_put(writer, buf);
    }
    else {
      // 子ノードを先に出力する
      KyokumenNode* child = node->lookup_child(te);
      daemon_dboard_move(board, &te);
      back_count = kif_output_seq_sub(board, te_count + 1, 
				      total_time,
				      last_xy,
				      child, writer);
      daemon_dboard_back(board, &te);
    }
  }

  return back_count + 1;
}


static void output_kif_board(const DBoard* board, FileWriter* writer)
{
  int x, y;
  int p;

  daemon_record_output_kif_piece(board, writer, D_GOTE);
  daemon_filewriter_put(writer, "  ９ ８ ７ ６ ５ ４ ３ ２ １\n");
  daemon_filewriter_put(writer, "+---------------------------+\n");
  for (y=1; y<=9; y++) {
    daemon_filewriter_put(writer, "|");
    for (x=9; 1 <= x; x--) {
      p = daemon_dboard_get_board(board, x, y);
      if (p == 0) {
	daemon_filewriter_put(writer, " ・");
      } else {
	if (p < 0x10) {
	  daemon_filewriter_put(writer, " ");
	} else {
	  daemon_filewriter_put(writer, "v");
	}
	daemon_filewriter_put(writer, kif_koma_data[p & 0xF]);
      }
    }
    daemon_filewriter_put(writer, "|");
    daemon_filewriter_put(writer, knum_data[y]);
    daemon_filewriter_put(writer, "\n");
  }
  daemon_filewriter_put(writer, "+---------------------------+\n");
  daemon_record_output_kif_piece(board, writer, D_SENTE);
  if (board->next == D_SENTE) {
    daemon_filewriter_put(writer, "先手番\n");
  } else {
    daemon_filewriter_put(writer, "後手番\n");
  }
}


/**
 * @return 成功 0
 */
static int daemon_record_output_kif_p(Record* record, FileWriter& writer) 
{
  DBoard* board;

  board = daemon_dboard_new();
  daemon_dboard_copy(&(record->first_board), board);

  daemon_filewriter_put(&writer, "# ---- daemonshogi kif file ----\n");
  writer << "# ファイル名：" << writer.filename << "\n";
  if ( record->start_time != 0 )
    writer << "開始日時：" << get_datetime_string(record->start_time) << "\n";
  if ( record->title != "" )
    writer << "表題：" << record->title << "\n";
  if ( record->source != "" )
    writer << "掲載：" << record->source << "\n";

  // 初期局面
  DBoard* tmp_bo = daemon_dboard_new();
  daemon_dboard_set_hirate(tmp_bo);
  if (*tmp_bo == *board)
    daemon_filewriter_put(&writer, "手合割：平手\n");
  else
    output_kif_board(board, &writer);
  daemon_dboard_free(tmp_bo);

  writer << "先手：" << 
    (record->player_name[0] != "" ? record->player_name[0] : "") << "\n";

  writer << "後手：" <<
    (record->player_name[1] != "" ? record->player_name[1] : "") << "\n";

  daemon_filewriter_put(&writer, "手数----指手---------消費時間--\n");

  ElapsedTime total_time;
  total_time.time[0] = 0;
  total_time.time[1] = 0;
  Xy last_xy = 0;

  kif_output_seq_sub(board, 0, total_time, last_xy, record->mi, &writer);
  daemon_dboard_free(board);

  return 0;
}


/** 
 * 柿木 KIF形式でファイル filename に出力する。
 * @return 成功 0
 */
int daemon_record_output_kif(Record* record, FileWriter* writer )
{
  return daemon_record_output_kif_code(record, writer, "CP932");
}


/**
 * 柿木 KIF形式でファイル filename に出力する。
 * @param record 棋譜データ
 * @param filename ファイル名
 * @param code 文字コード
 */
int daemon_record_output_kif_code(Record* record, 
				  FileWriter* writer,
				  const char* code) 
{
  assert(record != NULL);
  assert( writer != NULL);
  assert(code);

  daemon_filewriter_set_outcode(writer, code);
/*
  r = daemon_filewriter_open(writer, filename);
  if (r == -1)
    return -1;
*/
  return daemon_record_output_kif_p(record, *writer);
  // daemon_filewriter_close(writer);
}


/** ファイル filename のフォーマットを調査して返す。 */
static D_RECFORMAT daemon_record_research_format(const char* filename) 
{
  char *s;
  FileReader *reader;
  D_RECFORMAT format;
  int i;
  
  assert(filename != NULL);

  reader = daemon_filereader_new();
  if (reader == NULL) {
    return D_FORMAT_ERROR;
  }
  if (daemon_filereader_open(reader, filename) != 0) {
    return D_FORMAT_ERROR;
  }
  if (reader == NULL) {
    printf("No enough memory in daemon_record_reserach_format().\n");
    abort();
  }

  format = D_FORMAT_ERROR;

  for (i=0; i<256 && (s=daemon_filereader_next(reader)) != NULL; i++) {
    if (strncmp(s, "N+", 2) == 0 || strncmp(s, "P1", 2) == 0) {
      format = D_FORMAT_CSA;
      break;
    } else if (strncmp(s, "開始日時：", 10) == 0) {
      format = D_FORMAT_KIF;
      break;
    } else if (strncmp(s, "終了日時：", 10) == 0) {
      format = D_FORMAT_KIF;
      break;
    } else if (strncmp(s, "対局日：", 8) == 0) {
      format = D_FORMAT_KIF;
      break;
    }
  }

  daemon_filereader_close(reader);
  daemon_filereader_free(reader);

  return format;
}


/**
 * 棋譜ファイル filename を読み込んで Record を生成する。
 * ファイル形式は自動で認識する。
 */
void daemon_record_load(Record* record, const char* filename) 
{
  D_RECFORMAT recformat;

  assert(record != NULL);
  assert(filename != NULL);

  recformat = daemon_record_research_format(filename);
  switch (recformat)
  {
  case D_FORMAT_CSA:
    daemon_record_load_csa(record, filename);
    break;
  case D_FORMAT_KIF:
    daemon_record_load_kif(record, filename);
    break;
  default:
    record->set_error("unknown format");
    break;
  }
}


/**
 * 初期盤面を読み込む. 次のようなフォーマット
<pre>
P1-KY-KE-GI-KI-OU-KI-GI-KE-KY
P2 * -HI *  *  *  *  * -KA * 
P3-FU-FU-FU-FU-FU-FU-FU-FU-FU
P4 *  *  *  *  *  *  *  *  * 
P5 *  *  *  *  *  *  *  *  * 
P6 *  *  *  *  *  *  *  *  * 
P7+FU+FU+FU+FU+FU+FU+FU+FU+FU
P8 * +KA *  *  *  *  * +HI * 
P9+KY+KE+GI+KI+OU+KI+GI+KE+KY
P+00GI00GI
P-00FU00FU00FU00FU00KA       # 00 はセパレータ。個数ではない。
+                            # 手番
</pre>
 */
static void daemon_record_load_csa_board(Record* record, FileReader* reader) 
{
  const char* s;

  BOARD* bo = newBOARDwithInit();

  while ((s = daemon_filereader_skip_comment(reader, '\'')) != NULL) {
    int error = 0;
    bo_update_board_by_csa_line(bo, s, &error);

    switch (error) {
    case 1:
      record->set_error("inhand separator error");
      goto ex;
    case 2:
      record->set_error("inhand pieces too much");
      goto ex;
    case 3:
      record->set_error("piece side error");
      goto ex;
    case 4:
      record->set_error("piece kind error");
      goto ex;
    case 5:
      record->set_error("unknown line type");
      goto ex;
    default:
      break;
    }

    if (*s == '+' || *s == '-')
      break;
  }

ex:
  daemon_dboard_copy_from_board(bo, &record->first_board);
  freeBOARD(bo);
}


/** 
 * 経過時間 (opt.) を読み込む
 * 古い形式では次の行、新しい形式 (V2?) では同じ行
 */
static int csa_parse_time(FileReader* reader, const char* s)
{
  int tim = 0;
  if (*s == ',') {
    s++;
    sscanf(s, "T%d", &tim);
  }
  else { 
    s = daemon_filereader_skip_comment(reader, '#');
    if (s == NULL) { // ファイル末尾
/*
      daemon_dboard_free(board);
      record->loadstat = D_LOAD_SUCCESS;
*/
      return 0;
    }
    if (*s != 'T')
      daemon_filereader_push(reader, s);
    else
      sscanf(s, "T%d", &tim);
  }

  return tim;
}


/** 棋譜を読み込む */
static void daemon_record_load_csa_te(Record* record, FileReader* reader) 
{
  int p, p2;
  const char* s;

  DBoard *board;
  static const char* tesume = "'手数目:";

  printf("%s: start.\n", __func__); // DEBUG

  board = daemon_dboard_new();
  daemon_dboard_copy(&(record->first_board), board);

  daemon_dboard_output(board, stdout); // DEBUG

  KyokumenNode* current = record->mi;
  int pos = 0;

  while ((s = daemon_filereader_skip_comment(reader, '#')) != NULL) {
    DTe te;
    int tim = 0;

    // printf("read: '%s'\n", s); // DEBUG
    switch (*s) {
    case '+':
    case '-':
      /* 指す手 */
      te.fm = ((s[2] - 0x30) << 4) + s[1] - 0x30;
      te.to = ((s[4] - 0x30) << 4) + s[3] - 0x30;
      s += 5;
      for (p=1; p<=0xF; p++) {
	if (strncmp(s, csa_koma_str[p], 2) == 0)
	  break;
      }
      if (0xF < p) {
	/* 仕様上ありえない */
	record->set_error("piece kind error");
	daemon_dboard_free(board);
	return;
      }
      if (te.fm == 0) { // 駒打ちの場合
	if (te.to < 0x11 || 0x99 < te.to) {
	  /* 仕様上ありえない */
	  record->set_error("put sq error");
	  daemon_dboard_free(board);
	  return;
	}
	te.uti = p;
	te.nari = 0;
	te.tori = 0;
	te.special = DTe::NORMAL_MOVE;
      } 
      else { // 駒移動
	te.uti = 0;
	te.special = DTe::NORMAL_MOVE;

	if (te.fm < 0x11 || 0x99 < te.fm ||
	    te.to < 0x11 || 0x99 < te.to) {
	  /* 仕様上ありえない */
	  record->set_error("move sq error");
	  daemon_dboard_free(board);
	  return;
	}
	if (p == 0x05 || p == 0x8 || p == 0x15 || p == 0x18) {
	  /* 金、王の場合は成りはない */
	  te.nari = 0;
	} 
	else {
	  /* 金、王以外で移動元が不成り、移動先が成りなら、成りとする */
	  p2 = daemon_dboard_get_board(board, te.fm & 0xF, te.fm >> 4);
	  if (0x9 <= p && (p2 & 0xF) < 0x9) {
	    te.nari = 1;
	  } else {
	    te.nari = 0;
	  }
	}
	te.tori = board->board[te.to];
      }

      s += 2;
      tim = csa_parse_time(reader, s);

      // 手を追加
      current = current->add_child(te, tim, "")->node;
      pos++;

      daemon_dboard_move(board, &te);
      break;
    case '%':
      te.fm = 0; te.to = 0; te.uti = 0; te.nari = 0; te.tori = 0;

      if (strncmp(s, "%TORYO", 6) == 0) {
	te.special = DTe::RESIGN;
	s += 6;
	tim = csa_parse_time(reader, s);
      }
      else if (strncmp(s, "%KACHI", 6) == 0) 
	te.special = DTe::KACHI;
      else if (strncmp(s, "%CHUDAN", 7) == 0)
	te.special = DTe::CHUDAN;
      else {
	assert(0); // TODO:不正な手、千日手の場合は？
#if 0
      else if (strncmp(s, "%MATTA", 6) == 0) {
	record->matchstat = D_MATTA;
      } 
      else if (strncmp(s, "%SENNICHITE", 11) == 0) {
	record->matchstat = D_SENNICHITE;
      } 
      else if (strncmp(s, "%JISHOGI", 8) == 0) {
	record->matchstat = D_JISHOGI;
      } 
      else if (strncmp(s, "%TSUMI", 6) == 0) {
	record->matchstat = D_TSUMI;
      } 
      else if (strncmp(s, "%FUZUMI", 7) == 0) {
	record->matchstat = D_FUZUMI;
      } else if (strncmp(s, "%ERROR", 6) == 0) {
	record->matchstat = D_ERROR;
      } 
#endif
      }
      current = current->add_child(te, tim, "")->node;
      pos++;
      break;
    case '\'':
      // コメント or 分岐
      if (!string(s).find(tesume)) {
	int to_back = -1;
	sscanf(s + strlen(tesume), "%d", &to_back);
	
	// 巻き戻す 
	while (pos >= to_back) {
	  daemon_dboard_back(board, current->move_to_back);
	  current = current->prev;
	  pos--;
	}
      }
      break;
    case '\0':
      // 空行
      break;
    default:
      /* ありえない分岐 */
      assert(0);
    }
  }

  daemon_dboard_free(board);
  record->loadstat = D_LOAD_SUCCESS;
}


/** 
 * CSA形式のファイルを読み込んで Record を生成して返す。
 */
static void daemon_record_load_csa_p(Record* record, FileReader* reader) 
{
  char* s;

  assert(record != NULL);
  assert(reader != NULL);

  // 初期局面
  while ((s = daemon_filereader_skip_comment(reader, '\'')) != NULL) {
    if (strncmp(s, "V2", 2) == 0) {
      // バージョン番号
    }
    else if (strncmp(s, "N+", 2) == 0) {
      /* 先手対局者名 */
      record->player_name[0] = s + 2;
    }
    else if (strncmp(s, "N-", 2) == 0) {
      /* 後手対局者名 */
      record->player_name[1] = s + 2;
    }
    else if (strncmp(s, "$EVENT:", 7) == 0) {
      // ゲーム名
    }
    else if (strncmp(s, "$START_TIME:", 12) == 0) {
      // 開始実時間
    }
    // else if (strncmp(s, "PI", 2) == 0) {
    //   /* 初期配置 */
    // }
    else if (strncmp(s, "P1", 2) == 0) {
      /* 初期配置  盤面*/
      daemon_filereader_push(reader, s);
      daemon_record_load_csa_board(record, reader);
      break;
    }
#if 0
    else if (strncmp(s, "+", 1) == 0) {
      /* 初期配置  手番先手*/
      record->first_board.next = D_SENTE;
      break;
    } 
    else if (strncmp(s, "-", 1) == 0) {
      /* 初期配置  手番後手*/
      record->first_board.next = D_GOTE;
      break;
    }
#endif // 0
    else {
      assert(0); // DEBUG
    }
  }

  // 棋譜
  daemon_record_load_csa_te(record, reader);

#ifndef NDEBUG
  KyokumenNode* p = record->mi;
  while (p && p->moves.size() > 0) {
    daemon_dte_output(&p->moves.begin()->te, stdout);
    p = p->moves.begin()->node;
  }
#endif // NDEBUG
}


/** CSA形式のファイルを読み込む */
void daemon_record_load_csa(Record* record, const char* filename) 
{
  daemon_record_load_csa_code(record, filename, "CP932");
}


/**
 * CSA形式のファイルを読み込む. 文字コードを指定できる。
 * @param record 棋譜
 * @param filename ファイル名
 * @param encoding 文字コード
 */
void daemon_record_load_csa_code(Record* record, 
                                 const char* filename, 
                                 const char* encoding) 
{
  FileReader* reader;

  assert(record != NULL);
  assert(filename != NULL);
  assert(encoding != NULL);

  reader = daemon_filereader_new();
  daemon_filereader_set_incode(reader, encoding);
  daemon_filereader_open(reader, filename);

  if (daemon_filereader_get_stat(reader) == D_FILEREADER_STAT_ERROR) {
    /* 読み込みに失敗した */
    daemon_filereader_free(reader);
    record->set_error("failed open file");
    return;
  }

  daemon_record_load_csa_p(record, reader);

  daemon_filereader_close(reader);
  daemon_filereader_free(reader);
}


/** 持ち駒の解析 */
static void daemon_record_load_kif_piece(Record* record, 
					 const char* s, 
					 D_TEBAN next) 
{
  int p, i, num;

  // 1が空白
  static const char* num_data[] = {
    "",  "一", "二", "三", "四", "五", "六", "七", "八", "九",
  };

  // s += 12;

  if (strstr(s, "なし") == s)
    return; // 持ち駒なし

  while (*s != '\0' && *s != 0xd && *s != 0xa) {
    if (strstr(s, "　") == s || *s == ' ') {
      s++;
      continue;
    }

    for (p=1; p<=7; p++) {
      if (strncmp(kif_koma_data[p], s, strlen(kif_koma_data[p])) == 0)
	break;
    }
    if (7 < p) {
      /* 仕様上ありえない */
      record->set_error("mochigoma piece kind error");
      return;
    }
    s += strlen(kif_koma_data[p]);

    // 枚数
    if (*s == '\0' || *s == '\r' || *s == '\n') 
      num = 1;
    else if (strncmp("　", s, strlen("　")) == 0) {
      s += strlen("　");
      num = 1;
    } 
    else {
      num = 0;
      if (strncmp(s, "十", strlen("十")) == 0) {
	// 10～18枚
	s += strlen("十");
	num = 10;
      }

      for (i=1; i<=9; i++) {
	if (strncmp(num_data[i], s, strlen(num_data[i])) == 0) {
	  s += strlen(num_data[i]);
	  break;
	}
      }
      if (9 < i) {
	/* 仕様上ありえない */
	record->set_error("mochigoma too much");
	return;
      }
      num += i;
    }
    daemon_dboard_set_piece(&(record->first_board), next, p, num);
  }
}


/** KIF形式のファイルの盤面データを読み込む */
static void daemon_record_load_kif_board(Record* record, FileReader* reader) 
{
  char *s;
  int x, y, p;
  int next;

  for (y=1; (s=daemon_filereader_skip_comment(reader, '#')) != NULL && y<=9; 
       y++) {
    s++;
    for (x=9; 1 <= x; x--) {
      next = (*s == 'v') ? D_GOTE : D_SENTE;
      s++;
      if (strncmp("・", s, strlen("・")) == 0) {
	s += strlen("・");
	continue;
      }
      for (p=1; p<=0xF; p++) {
	if (strncmp(kif_koma_data[p], s, strlen(kif_koma_data[p])) == 0)
	  break;
      }
      if (0xF < p) {
	/* 仕様上ありえない */
	record->set_error("piece kind error");
	return;
      }
      if (next == D_GOTE)
	p += 0x10;
      daemon_dboard_set_board(&(record->first_board), x, y, p);
      s += strlen(kif_koma_data[p]);
    }
  }  
}


/** KIF形式のファイルのヘッダー部分を読み込んで Record を生成して返す。 */
static void daemon_record_load_kif_header(Record* record, FileReader* reader) 
{
  char* s;
  bool board_read = false;
  string name, value;
  const char* p;

  while ((s = daemon_filereader_skip_comment(reader, '#')) != NULL) {
    if ((p = strstr(s, "：")) != NULL) {
      name = string(s, p - s);
      value = string(p + strlen("："));
    }
    else
      name = "";

    if (name == "対局日" || name == "開始日時") {
      // TODO:
    } 
    else if ( name == "表題" )
      record->title = value;
    else if ( name == "掲載" )
      record->source = value;
    else if (name == "手合割") {
      if (value == "平手")
	record->teai = D_HIRATE;
      else
	record->teai = HANDICAP_ETC;
    } 
    else if (name == "後手の持駒" || name == "下手の持駒") {
      daemon_record_load_kif_piece(record, 
				   s + name.length() + strlen("："), D_GOTE);
    }
    else if (strstr(s, "+---------------------------+") == s) {
      daemon_record_load_kif_board(record, reader);
      board_read = true;
    }
    else if (name == "先手の持駒" || name == "上手の持駒") {
      daemon_record_load_kif_piece(record, 
				   s + name.length() + strlen("："), D_SENTE);
    }
    else if (strstr(s, "先手番") == s)
      record->first_board.next = D_SENTE;
    else if (strstr(s, "後手番") == s)
      record->first_board.next = D_GOTE;
    else if (name == "先手") {
      char* p = s + strlen("先手：");
      daemon_filereader_return2zero(p);
      record->player_name[0] = p;
    } 
    else if (name == "後手") {
      char* p = s + strlen("後手：");
      daemon_filereader_return2zero(p);
      record->player_name[1] = p;
    } 
    else if (strstr(s, "手数----指手---------消費時間--") == s)
      break;
  }  

  if (!board_read)
    daemon_dboard_set_hirate(&record->first_board);
}


/** 棋譜を読み込む */
static void daemon_record_load_kif_te(Record* record, FileReader* reader,
				      KyokumenNode* current, 
				      const DBoard* first_board,
			      int pos, KyokumenNode::Moves::iterator last ) 
{
  const char* s;
  const char* ss;
  int p, x, y;
  int next;
  time_t t;
  // KyokumenNode::Moves::iterator last;

  p = x = y = 0;
  next = record->first_board.next;

  list<int> alt_nodes;

  DBoard* bo = daemon_dboard_new();
  daemon_dboard_copy(first_board, bo);

  while ((s = daemon_filereader_next(reader)) != NULL) {
    DTe te;
    te.fm = 0; te.to = 0; te.nari = 0; te.uti = 0; te.tori = 0; te.hint = 0;
    te.special = DTe::NORMAL_MOVE;

    if (strstr(s, "まで") == s) {
      printf("%d: line end.\n", __LINE__); // DEBUG

      // 変化がある？
      while (alt_nodes.size() > 0) {
	s = daemon_filereader_next(reader);
	s = daemon_filereader_next(reader);
	if (strstr(s, "変化：") != s) {
	  record->set_error("alt start");
	  return;
	}
	int n = alt_nodes.back();
	alt_nodes.pop_back();
	while (pos > n) {
	  daemon_dboard_back( bo, current->move_to_back );
	  pos--;
	  current = current->prev;
	  last = current->prev->find_edge( *current->move_to_back );
	  assert( last != current->prev->moves.end() );
	  printf("last = %s\n", te_to_str(&last->te, bo).c_str() );
	}
	// 再帰する
	daemon_record_load_kif_te(record, reader, current, bo, pos, last);
      }
      break;
    }
    else if (*s == '*') {
      // コメント. 複数行のことがある
      if (last->te_comment == "") {
	if ( string(s + 1).find("[?]") == 0 ) {
	  last->annotation = ANNOT_POOR;
	  last->te_comment = s + 4;
	}
	else if ( string(s + 1).find("[?!]") == 0 ) {
	  last->annotation = ANNOT_QUESTIONABLE;
	  last->te_comment = s + 5;
	}
	else
	  last->te_comment = s + 1;
      }
      else
	last->te_comment = last->te_comment + "\n" + (s + 1);
      continue;
    }
    else if (*s == '&') {
      // 取り込まない
      continue;
    }

    ss = s;
    s += 5;
    // printf("%d: '%s'\n", __LINE__, s); // DEBUG
    if (strstr(s, "詰み") == s) {
      assert(0); // TODO:手じゃない
      break;
    } 

    if (strstr(s, "投了") == s) {
      te.special = DTe::RESIGN;
      s += strlen("投了");
    } 
    else if (strstr(s, "中断") == s) {
      te.special = DTe::CHUDAN;
      s += strlen("中断");
    }
    else {
      // 通常の指し手
      if (strstr(s, "同　") == s) {
	printf("last = %s\n", te_to_str(&last->te, bo).c_str() ); // DEBUG
	x = last->te.to & 0xf;
	y = (last->te.to >> 4);
	s += strlen("同　");
      } 
      else {
	for (x = 1; x <= 9; x++) {
	  if (strncmp(anum_data[x], s, strlen(anum_data[x])) == 0)
	    break;
	}
	if (9 < x) {
	  /* 仕様上ありえない */
	  record->set_error("to-sq x error");
	  return;
	}
	s += strlen(anum_data[x]);
	for (y = 1; y <= 9; y++) {
	  if (strncmp(knum_data[y], s, strlen(knum_data[y])) == 0) 
	    break;
	}
	if (9 < y) {
	  /* 仕様上ありえない */
	  record->set_error("to-sq y error");
	  return;
	}
	s += strlen(knum_data[y]);
      }

      for (p = 1; p <= 0xF; p++) {
	if (*long_koma_data[p] != '\0' && 
	    strncmp(long_koma_data[p], s, strlen(long_koma_data[p])) == 0)
	  break;
      }
      if (0xF < p) {
	/* 仕様上ありえない */
	record->set_error("piece kind error");
	return;
      }
      s += strlen(long_koma_data[p]);
      if (strstr(s, "打") == s) {
	te.uti = p;
	s += strlen("打");
      } 
      else if (strstr(s, "成") == s) {
	te.nari = true;
	s += strlen("成");
      } 

      s++;
      if (!te.uti)
	te.fm = ((*(s + 1) - 0x30) << 4) + (*s - 0x30);
      te.to = (y << 4) + x;
      s += 3;

      printf("from = %x, to = %x, s = %s\n", te.fm, te.to, s); // DEBUG
    }

    int min, sec;
    char alt = '\0';
    sscanf(s, " (%d:%d/%*d:%*d:%*d)%c", &min, &sec, &alt);
    t = min * 60 + sec;
    if (alt == '+')
      alt_nodes.push_back(pos);

    if ( !te.special || te.special == DTe::RESIGN ) {
      te.tori = bo->board[te.to];
      last = current->add_child(te, t, "");
      current = last->node;
      daemon_dboard_move(bo, &te);
      pos++;
    }
    
    next ^= 1;
  }

  daemon_dboard_free(bo);

  record->loadstat = D_LOAD_SUCCESS;
}


/** KIF形式のファイルを読み込んで Record を生成して返す。 */
void daemon_record_load_kif(Record* record, const char* filename) 
{
  daemon_record_load_kif_code(record, filename, "CP932");
}


/** KIF形式のファイルを読み込んで Record を生成して返す。 */
void daemon_record_load_kif_code(Record* record, 
				 const char* filename, 
				 const char* encoding) 
{
  FileReader* reader;

  assert(record != NULL);
  assert(filename != NULL);
  assert(encoding != NULL);

  reader = daemon_filereader_new();
  daemon_filereader_set_incode(reader, encoding);
  daemon_filereader_open(reader, filename);

  if (daemon_filereader_get_stat(reader) == D_FILEREADER_STAT_ERROR) {
    /* 読み込みに失敗した */
    daemon_filereader_free(reader);
    record->set_error("failed open file");
    return;
  }

  daemon_record_load_kif_header(record, reader);
  daemon_record_load_kif_te(record, reader, record->mi, 
			    &record->first_board, 1, record->mi->moves.end() );

  daemon_filereader_close(reader);
  daemon_filereader_free(reader);
}


D_LOADSTAT daemon_record_get_loadstat(const Record* record) 
{
  assert(record != NULL);
  return record->loadstat;
}


string daemon_record_get_player(const Record* record,
			      D_TEBAN next) {
  return record->player_name[next];
}


void daemon_record_set_player(Record* record,
			      D_TEBAN next,
			      const char* s) {
  assert(record != NULL);
  assert(next == D_SENTE || next == D_GOTE);
  assert(s != NULL);

  record->player_name[next] = s;
}

