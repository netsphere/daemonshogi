/* -*- encoding:utf-8 -*-
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * DTe class
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/dte.c,v $
 * $Id: dte.c,v 1.1.1.1 2005/12/09 09:03:04 tokita Exp $
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "dte.h"
#include "dboard.h"
using namespace std;


DTe* daemon_dte_new(void) {
  DTe* te;

  te = (DTe*) malloc(sizeof(DTe));
  if (te == NULL) {
    printf("No enough memory in daemon_dte_new().");
    abort();
  }

  return te;
}

void daemon_dte_free(DTe* te) {
  assert(te != NULL);
  free(te);
}


/** デバッグ用の表現 */
void daemon_dte_output(const DTe* te, FILE* out) 
{
  assert(te != NULL);
  assert(out != NULL);

  if ( te_is_put(te) ) {
    fprintf(out, "(%d,%d) %d uti\n", te->to & 0xF, te->to >> 4, te->uti);
  } else {
    fprintf(out, "(%d,%d)->(%d,%d)%s\n",
	    te->fm & 0xF, te->fm >> 4,
	    te->to & 0xF, te->to >> 4,
	    te->nari ? " nari" : "");
  }
}


extern const char* CSA_PIECE_NAME[];

/** 局面を利用した文字列化. 主にデバッグ用 */
string te_to_str(const DTe* te, const DBoard* bo)
{
  assert(te);
  assert(bo);

  char buf[100];

  if ( te->special ) {
    switch (te->special) {
    case DTe::RESIGN:
      sprintf(buf, "%s", "%TORYO");
      break;
    default:
      assert(0);
    }
  }
  else if ( te_is_put(te) ) {
    sprintf(buf, "%d%d%s*",
	    te->to & 0xf, te->to >> 4, CSA_PIECE_NAME[te->uti]);
  }
  else {
    sprintf(buf, "%d%d%s%s(%d%d)",
	    te->to & 0xf, te->to >> 4,
	    CSA_PIECE_NAME[bo->board[te->fm] & 0xf],
	    (te->nari ? "+" : ""),
	    te->fm & 0xf, te->fm >> 4);
  }

  return buf;
}
