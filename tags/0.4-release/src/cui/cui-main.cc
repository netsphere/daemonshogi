/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2009

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#define G_DISABLE_DEPRECATED 1
#define _XOPEN_SOURCE 700 // SUSv4
#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <si/si.h>
#include <si/ui.h>
#include <glib.h>

#ifndef LINT
static const char copyright[] =
  "SI SHOGI " VERSION "-" REVISION " copyright(c)2000-2002 Masahiko Tokita";
#endif /* LINT */


static void usage()
{
  fprintf( stderr, "SI SHOGI %s-%s\n", VERSION, REVISION );
  fprintf( stderr, "usage : si [-b BOARD file]\n" );
  fprintf( stderr, "        -n        Change display mode.\n" );
  fprintf( stderr, "        -h     Print help. This output.\n" );
}


extern GAME g_game;

static gboolean on_stdin_read( GIOChannel* source,
			       GIOCondition condition,
			       gpointer data )
{
}


static gint io_watch = 0;

void add_watch_stdin()
{
  GIOChannel* channel = g_io_channel_unix_new( STDIN_FILENO );
  io_watch = g_io_add_watch( channel,
			     GIOCondition(G_IO_IN | G_IO_HUP | G_IO_ERR),
			     on_stdin_read,
			     NULL );
  g_io_channel_unref( channel );
}


int main(int argc, char* argv[])
{
  srand( time(NULL) );
  newHASH();

  int c;

  BOARD* bo = newBOARDwithInit();
  boInit( bo );
  boSetHirate( bo );

  extern int flgDisplayMode;
  
  while ( (c = getopt( argc, argv, "b:hn")) != -1 ) {
    switch ( c ) {
    case 'b':
      if ( !bo_load_file(bo, optarg) ) {
	fprintf( stderr, "File not found or illegal board.\n" );
	exit( EXIT_FAILURE );
      }	
      break;
    case 'n':
      flgDisplayMode = 1;
      break;
    case 'h':
    default:
      usage();
      exit(EXIT_FAILURE);
      break;
    }
  }
  
  initGAME(&g_game);

  gmSetInputFunc(&g_game, SENTE, SI_HUMAN, si_input_next_human );
  gmSetInputFunc(&g_game, GOTE, SI_COMPUTER_3, si_input_next_computer );

  /* ゲーム開始 */
  play( bo, &g_game);

  GMainLoop* loop = g_main_loop_new(NULL, FALSE);
  g_main_loop_run(loop);

  g_main_loop_unref(loop);

  freeHASH();

  return 0;
}
