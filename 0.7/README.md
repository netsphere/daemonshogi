
# Daemonshogi

Daemonshogi is a GTK+ based, simple shogi (Japanese chess) program.

Daemonshogi is free software: you can redistribute it and/or modify 
it under the terms of the GNU General Public License version 3 (GPLv3),
or (at your option) any later version.

The official web site is:
  http://www.nslabs.jp/daemonshogi/

Copyright (C) 2002-2005,2009 Masahiko Tokita
Copyright (C) 2008-2010 Hisashi Horikawa
All rights reserved.


## Installation

Please see the file 'INSTALL'
