/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "si.h"
#include <string>
using namespace std;


const char* CSA_PIECE_NAME[] = {
  "", 
  "FU", "KY", "KE", "GI", "KI", "KA", "HI", "OU", 
  "TO", "NY", "NK", "NG", "",   "UM", "RY", "",
};


/**
 * デバッグ用の表現。
 * @param bo 動かす前の局面
 * @param te 指し手
 */
string te_str( const TE& te, const BOARD& bo )
{
  char buf[100];

  if ( !te_to(te) )
    return "%TORYO";
  else if ( te_is_put(te) ) {
    sprintf(buf, "%d%d%s*", 
	    te_to(te) & 0xf, te_to(te) >> 4, CSA_PIECE_NAME[te_pie(te)] );
  }
  else {
    sprintf(buf, "%d%d%s%s(%d%d)",
	    te_to(te) & 0xf, te_to(te) >> 4, 
	    CSA_PIECE_NAME[bo.board[te_from(te)] & 0xf],
	    (te_promote(te) ? "+" : ""),
	    te_from(te) & 0xf, te_from(te) >> 4 );
  }

  string r(buf);

  if ( (te_hint(te) & TE_STUPID) != 0 )
    r += "#STPD";

  if ( (te_hint(te) & TE_TSUMERO) != 0 )
    r += "#TMRO";
  else if ( (te_hint(te) & TE_2TESUKI) != 0 )
    r += "#2SUK";

  return r;
}


/** board を利用しないで表示 */
void te_print(const TE& te, std::ostream& ost)
{
  char buf[100];

  if ( te_is_put(te) ) {
    sprintf(buf, "%d%d%s*",
            te_to(te) &0x0f, te_to(te) >> 4, CSA_PIECE_NAME[te_pie(te)]);
  }
  else {
    sprintf(buf, "%d%d(%d%d)%s", 
            te_to(te) & 0x0f, te_to(te) >> 4,
            te_from(te) & 0x0f, te_from(te) >> 4,
            te_promote(te) ? "+" : "");
  }

  ost << buf;
}


/**
 * te の手が成りが可能か調べる。
 * @param bo 対象のBOARD
 * @param te 対象のTE
 * @return 成れないときは 0, どちらも可能 1, 成るしかない 2
 */
int can_move_promote(const BOARD* bo, const TE& te) 
{
  PieceKind p;
  int next;
  int fml ,tol;

  assert(bo != NULL);

  if ( te_is_put(te) )
    return 0;

  assert( te_from(te) >= 0x11 && te_from(te) <= 0x99 );
  assert( bo->board[te_from(te)] != 0 );

  p = boGetPiece(bo, te_from(te) );
  next = getTeban(p);

  p &= 0x0F;

  /* 王と金は成れない。戻る */
  if (p == OH || p == KIN)
    return 0;

  /* もう成っている駒はなれない。戻る */
  if ((p & 0x08) == 0x08)
    return 0;

  fml = (te_from(te) >> 4);
  tol = (te_to(te) >> 4);

  if ( next == SENTE ) {
    if ( (p == FU || p == KYO) && tol <= 1 )
      return 2;
    else if ( p == KEI && tol <= 2 )
      return 2;
    else if (fml <= 3 || tol <= 3)
      return 1;
  }
  else {
    if ( (p == FU || p == KYO) && 9 <= tol )
      return 2;
    else if ( p == KEI && 8 <= tol )
      return 2;
    else if (7 <= fml || 7 <= tol)
      return 1;
  }

  return 0;
}


/**
 * 千日手 (同一局面4回) になる手か？
 * @param bo 現在の局面
 * @param te これから指そうとする手
 * @param forbidden 連続王手の千日手のときtrueに出力する
 *
 * @return 千日手のときtrue
 */
bool te_is_sennichite(const BOARD* bo, const TE& te, bool* forbidden)
{
  BOARD* tmp_bo = bo_dup(bo);
  boMove_mate(tmp_bo, te);

  if ( bo_repetition_count(tmp_bo) >= 3 ) {
    // 連続王手の千日手かどうか

    // http://www.shogi.or.jp/faq/
    // 全部の手が王手のとき以外は、通常の千日手扱い

    printBOARD(bo);
    printf("move = %s = sennichite\n", te_str(te, *bo).c_str() ); // DEBUG

    freeBOARD(tmp_bo);
    return true;
  }

  freeBOARD(tmp_bo);
  return false;
}


/**
 * 指し手が打ち歩詰めになるか
 * @param bo 歩を打つ前の局面
 * @param te 歩を打つ手
 * @return 打ち歩詰めのときtrue
 */
bool te_is_uchifuzume(const BOARD* bo, const TE& te)
{
  int defense;
  Xy defense_king;
  int i;

  if (bo->next) {
    // 後手が攻め
    defense = 0;
    defense_king = te_to(te) + 16;
  }
  else {
    // 先手が攻め
    defense = 1;
    defense_king = te_to(te) - 16;
  }

  // 歩に攻め方の利きが付いてなければ玉で取れる
  if ( bo_attack_count(bo, 1 - defense, te_to(te)) == 0 )
    return false;

  // 受け玉の周りに攻撃側の利きのないところがあればok
  for (i = 0; i < 8; i++) {
    Xy sq = defense_king + normal_to[ OH + (defense << 4) ][i];
    if (bo->board[sq] == WALL)
      continue;

    if ( (!bo->board[sq] || getTeban(bo->board[sq]) != defense) && 
         bo_attack_count(bo, 1 - defense, sq) == 0 )
      return false;
  }

  // 受け側の駒で取れるか (短いの)
  for (i = 0; i < bo->short_attack[defense][te_to(te)].count; i++) {
    Xy defense_from = bo->short_attack[defense][te_to(te)].from[i];
    if ( (bo->board[defense_from] & 0xf) == OH )
      continue;
#ifdef USE_PIN
    // pinされているときは決して取れない
    if (!bo->pinned[defense_from])
      return false;
#else
    // これは厳密ではない。
    if (bo->long_attack[1 - defense][defense_from].count == 0)
      return false;
#endif // USE_PIN
  }

  // 受け側の駒で取れるか (長いの)
  for (i = 0; i < bo->long_attack[defense][te_to(te)].count; i++) {
    Xy defense_from = bo->long_attack[defense][te_to(te)].from[i];

#ifdef USE_PIN
    // pinされているときは決して取れない
    if (!bo->pinned[defense_from])
      return false;
#else
    // これは厳密ではない。
    if (bo->long_attack[1 - defense][defense_from].count == 0)
      return false;
#endif // USE_PIN
  }

#ifdef USE_PIN
  // 歩で遮られることはない -> 受けなし
  if ( !bo->long_attack[1 - defense][te_to(te) - 1].count &&
       !bo->long_attack[1 - defense][te_to(te) + 1].count )
    return true;
#endif

  // 歩を打つことで攻め方の長い攻撃が遮られることがある
  // 実際に動かしてみる
  BOARD* tmp_bo = bo_dup(bo);
  boMove_mate( tmp_bo, te );

  MOVEINFO tmp_mi;
  make_moves(tmp_bo, &tmp_mi);
  bool ret = tmp_mi.count == 0;  // 詰み

  freeBOARD(tmp_bo);

  return ret;
}


/**
 * ルール上正しい指し手かチェックする。
 * canvas.cc から呼び出される. この関数は重い
 *
 * @param bo 対象のBOARD
 * @param te 調べる一手
 * @return 正しければ true を返す。正しくなければ false を返す。
 */
bool te_is_valid( const BOARD* bo, const TE& te)
{
  MOVEINFO mi;

  make_moves(bo, &mi);
  if ( !mi_includes(&mi, te) )
    return false;
#if 0
  if ( te_is_put(te) && te_pie(te) == FU ) {
    if ( (bo->next == SENTE && te_to(te) == bo->king_xy[GOTE] + 16) ||
	 (bo->next == GOTE && te_to(te) == bo->king_xy[SENTE] - 16) ) {
      if ( te_is_uchifuzume(bo, te) )
	return false;
    }
  }
#endif // 0

  return true;
}
