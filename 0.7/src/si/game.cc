/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "si.h"


/**
 * 初期化付きでGAMEを生成する。
 * @return GAMEのポインタ
 */
GAME* newGAME() 
{
  GAME *game;

  game = (GAME *) malloc(sizeof(GAME));
  if (game == NULL) {
    si_abort("No enough memory. In moveinfo.c#newGAME()");
  }

  initGAME(game);

  return game;
}


/**
 * game を初期化する。
 * @param gm 対象のGAME
 */
void initGAME(GAME* gm) 
{
  assert(gm != NULL);

  // gm->teai = HIRATE;

  for (int i = 0; i < 2; i++) {
    gm->si_input_next[i] = NULL;
    gm->computer_level[i] = 5;

    gm->total_time[i] = 0;
    gm->total_elapsed_time[i] = 0;
  }
  gm->flg_run_out_to_lost = 1;
  gm->game_status = SI_NORMAL;
  // gm->chudan = NULL;
}


/**
 * game のメモリを解放する。
 * @param game 対象のGAME
 */
void freeGAME(GAME* game) 
{
  if (game)
    free( game );
}


/**
 * 次の手を入力する関数へのポインタを返す。
 * @param gm 対象のGAME
 * @param next 次の手番
 * @return 次の手を入力する関数へのポインタ
 */ 
InputCallbackFunc gmGetInputNextPlayer(const GAME* gm, int next)
{
  return gm->si_input_next[next];
}


#ifdef USE_SERIAL
/**
 * シリアルポートのデバイルファイルの名前を返す。
 * @param gm 対象のGAME
 * @param next 次の手番
 * @return 次の手を入力する関数へのポインタ
 */ 
char *gmGetDeviceName(GAME *gm, int next)
{
  return gm->ca[next].DEVICENAME;
}
#endif /* USE_SERIAL */


/**
 * コンピューターのレベルを返す。
 * @param gm 対象のGAME
 * @param next 次の手番
 * @return コンピューターのレベル
 */ 
int gmGetComputerLevel(GAME *gm, int next) {
  return gm->computer_level[next];
}

/**
 * 対戦を行うプレイヤーを設定する。
 * 
 * @param gm 対象GAME
 * @param next 設定する手番
 * @param fn コールバック関数
 */ 
void gmSetInputFunc(GAME* gm, int next, InputCallbackFunc fn) 
{
  assert(gm != NULL);
  assert(next == SENTE || next == GOTE); 
  assert( fn );

  gm->si_input_next[next] = fn;
}


#ifdef USE_SERIAL
/**
 * デバイス名をセットする。
 * @param gm 対象のGAME
 * @param next セットする手番 
 * @param s デバイス名
 */
void gmSetDeviceName(GAME *gm, int next, char *s)
{
  caSetDEVICENAME(&(gm->ca[next]), s);

}
#endif /* USE_SERIAL */


/**
 * ゲームのステータスをセットする。
 * @param gm 対象のGAME
 * @param status ゲームのステータス
 */
void gmSetGameStatus(GAME* gm, INPUTSTATUS status) 
{
  assert( gm != NULL);
  gm->game_status = status;
}


/**
 * 現在の時刻を記録する。
 * (思考時間の測定用)
 * @param bo 対象のBOARD
 */
void gm_set_think_start_time(GAME* gm) 
{
  gm->think_start_time = time(NULL);
}


/**
 * 消費した時間を記録する。
 * @param bo 対象のBOARD
 */ 
void gm_add_elapsed_time(GAME* gm, int side, int t)
{
  gm->total_elapsed_time[side] += t;
}


static void (*pending_function)( bool may_block ) = NULL;


/**
 * ペンディングされたイベントを処理する関数へのポインタをセットする。
 * @param f ペンディングされたイベントを処理する関数へのポインタ
 */
void si_set_pending_function( void (*f)(bool may_block) ) 
{
  pending_function = f;
}


/**
 * GUI のペンディングされたイベントを処理する。
 */
void processing_pending( bool may_block ) 
{
  if ( pending_function == NULL )
    return;
  
  (*pending_function)( may_block );
}
