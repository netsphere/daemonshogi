/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * wrapper.c
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/wrapper.c,v $
 * $Id: wrapper.c,v 1.1.1.1 2005/12/09 09:03:11 tokita Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include "si.h"
#include "ui.h"
#include "wrapper.h"

FILE *out;

/**
 * コンストラクタ。
 */ 
CSAShogiWrapper *si_wrap_new(void) {
  CSAShogiWrapper *wrapper;

  wrapper = (CSAShogiWrapper *)malloc(sizeof(CSAShogiWrapper));
  if (wrapper == NULL) {
    printf("No enough memory in si_wrap_new().\n");
    abort();
  }

  wrapper->log = fopen("log.txt", "w");
  if (wrapper->log == NULL) {
	  abort();
  }
  out = wrapper->log;

  return wrapper;
}

/** 
 * dispatch用の関数へのポインタをセットする
 * @param wrapper 
 * @param f 関数へのポインタ
 */
void si_wrap_set_dispatch_func(CSAShogiWrapper *wrapper, void (*f)(void)) {
	set_pending_function(f);
}

/**
 * 初期化する。
 * sikou() から呼ばれる。
 * @param wrapper
 * @param chudan 中断フラグへのポインタ
 */ 
void si_wrap_sikou_ini(CSAShogiWrapper *wrapper, int *chudan) {
  extern GAME g_game;
  extern BOARD g_board;
  extern int flgDisplayMode;
  extern int THINK_DEEP_MAX;

  wrapper->chudan = chudan;
  gmSetChudan(&g_game, chudan);

  boInit(&g_board);
  flgDisplayMode = 0;
  boSetHirate(&g_board);
#ifdef USE_HASH  
  /* ハッシュテーブル初期化 */
  newHASH();
#endif /* USE_HASH */
  initGAME(&g_game);

  THINK_DEEP_MAX = 4;
#ifdef USE_SERIAL
  gmSetInputFunc(&g_game, SENTE, SI_SERIALPORT_1);
#else
  gmSetInputFunc(&g_game, SENTE, SI_NETWORK_1, NULL);
#endif // USE_SERIAL
  gmSetInputFunc(&g_game, GOTE, SI_COMPUTER_3, NULL);

  // g_board.point[ 0 ] = grade(&g_board, 0);
  // g_board.point[ 1 ] = grade(&g_board, 1);

  boPlayInit(&g_board);
  fputBOARD(wrapper->log, 0, &g_board);
  fflush(wrapper->log);
}

/**
 * 思考ルーチン。sikou() から呼ばれる。
 * <pre>
 * return value は、以下の意味。
 *
 *        ０       −−−  投了
 *        ０より大 −−−  i_moto,i_saki,i_naruに指し手が入っている。
 *        −５     −−−  引き分け宣言を行う。（９８年ルール）
 *        −４     −−−  勝ち宣言を行う。    （９８年ルール）
 * </pre>
 * @param wrapper
 * @param tesu      現在の手数で０から数えます。
 *                  平手初期局面先手番のときは、０になってます。
 * @param kifu      それまでの棋譜
 * @param timer_sec 先手・後手のそれまでの消費時間が秒単位で入ります。
 *                  思考中カウントアップはされませんし、セットしてもいけませ
 *                  ん。
 * @param i_moto    移動元座標(1-81)をセットします。
 *                  駒を打つ場合は、101〜107の値をセットします。
 * @param i_saki    移動先座標(1-81)をセットします。
 * @param i_naru    成る場合は1をセットし、不成や打つ場合は0をセットします。
 *                  すでに成っている駒の移動は、0をセットします。
 * @return 
 */ 
int si_wrap_sikou(CSAShogiWrapper *wrapper,
		  int tesu,
		  unsigned char kifu[][2],
		  int* timer_sec,
		  int* i_moto,
		  int* i_saki,
		  int* i_naru) {
  extern BOARD g_board;
  extern TE best_te;
  TE te;
  int ret;
  
    g_board.point[ 0 ] = grade(&g_board, 0);
    g_board.point[ 1 ] = grade(&g_board, 1);

  fflush(wrapper->log);
  if (0 < tesu) {
    int saki = kifu[tesu - 1][0];
    int moto = kifu[tesu - 1][1];
	fprintf(wrapper->log, "tesu : %d\n", tesu);
	fprintf(wrapper->log, "saki : %d\n", saki);
	fprintf(wrapper->log, "moto : %d\n", moto);
    teCSASet(&te, moto, saki);
    boMove(&g_board, te );
    g_board.point[ 0 ] = grade(&g_board, 0);
    g_board.point[ 1 ] = grade(&g_board, 1);
	fprintTE(wrapper->log, &te);
  }

  fputBOARD(wrapper->log, 0,&g_board);

  ret = minmax(&g_board);
  fprintf(wrapper->log, "ret : %d\n", ret);
  boMove(&g_board, best_te );
  g_board.point[ 0 ] = grade(&g_board, 0);
  g_board.point[ 1 ] = grade(&g_board, 1);
  fprintTE(wrapper->log, &best_te);

  fputBOARD(wrapper->log, 0, &g_board);

  *i_moto = teCSAimoto(&best_te);
  *i_saki = teCSAisaki(&best_te);
  *i_naru = teCSAinaru(&best_te);

  fflush(wrapper->log);

  if (ret== SI_NORMAL) {
	return 1;
  } else if (ret == SI_TORYO) {
	return 0;
  }
		       
  return -5;
}

/**
 * 対局が終了したときに、呼ばれる。後処理を行う。
 * void sikou_end() から呼ばれる。
 * @param wrapper
 */
void si_wrap_sikou_end(CSAShogiWrapper *wrapper) {
  fflush(wrapper->log);
  fclose(wrapper->log);
#ifdef USE_HASH  
  /* ハッシュテーブル解放 */
  freeHASH();
#endif /* USE_HASH */
}

