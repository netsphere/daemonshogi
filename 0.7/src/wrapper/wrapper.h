/* SI SHOGI Library
   Copyright (C) 2002 Masahiko Tokita
   This file is part of the SI SHOGI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.w

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with The Daemonshogi; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */
/*
 * wrapper.h
 * $Source: /cvsroot/daemonshogi/daemonshogi/src/si/wrapper.h,v $
 * $Id: wrapper.h,v 1.1.1.1 2005/12/09 09:03:11 tokita Exp $
 */

#ifndef _WRAPPER_H_
#define _WRAPPER_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * CSA shogi <-> si shogi のラッパークラス
 */
typedef struct {
  /**
   * 中断フラグ
   * 初期値は 0 が入る。0 以外がセットされたら中断して処理を戻す。
   */
  int *chudan;
  /** ログの出力先 */
  FILE *log;
} CSAShogiWrapper;

CSAShogiWrapper* si_wrap_new               (void);
void             si_wrap_set_dispatch_func (CSAShogiWrapper *wrapper,
					    void (*f)(void));
void             si_wrap_sikou_ini         (CSAShogiWrapper *wrapper,
					    int *chudan);
int              si_wrap_sikou             (CSAShogiWrapper *wrapper, 
					    int tesu,
					    unsigned char kifu[][2],
					    int* timer_sec,
					    int* i_moto,
					    int* i_saki,
					    int* i_naru);
void             si_wrap_sikou_end         (CSAShogiWrapper *wrapper);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _WRAPPER_H_ */
