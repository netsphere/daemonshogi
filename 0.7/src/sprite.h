/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _SPRITE_H_
#define _SPRITE_H_

#include <gtk/gtk.h>

// TODO: 拡張性
#define SPRITEMAX 41



/** スプライトクラス.
 * \todo Z座標.
 * \todo 駒情報は追い出す
 */
struct Sprite
{
  /** スプライトの種類 */
  // SPRITE_TYPE type;

  /**
   * sprite の元画像. このスプライトでは画像を所有しない.
   * (src_x, src_y)-(width, height) を転送する。
   */
  cairo_surface_t* pixmap;

  /** 元画像の透過マスク */
  // GdkBitmap* mask;

  /** sprite の元画像でのX位置 */
  gint src_x;

  /** sprite の元画像でのY位置 */
  gint src_y;

  /** sprite の元画像での横のドット数 */
  int src_width;

  /** sprite の元画像での縦のドット数 */
  int src_height;
  
  /** sprite の表示先でのX位置 */	
  gint x;
  /** sprite の表示先でのY位置 */	
  gint y;

  /** 表示の際に大きさを拡大する。座標はスケールさせない。 */
  double scale_size_x, scale_size_y;

  /** ドラッグ前のX位置 */
  gint from_x;
  /** ドラッグ前のY位置 */
  gint from_y;

  /** 表示するか？のフラグ。TRUEならば表示する。 */
  gboolean visible;

  /** 移動できるかのフラグ。TRUEならば移動できる。 */
  gboolean movable;

  /** ユーザデータ */
  void* userdata;
};


/*********************************************************/
/* function prototypes */
/*********************************************************/

Sprite*    daemon_sprite_new( cairo_surface_t* pixmap,
                              // GdkBitmap* bitmap,
                              int src_x, int src_y,
                              gint width,
                              gint height);

void daemon_sprite_draw_all( Sprite* sprites[], cairo_surface_t* drawable );

void daemon_sprite_draw_overlap( Sprite* sprites[], const GdkRectangle* rect,
				 cairo_surface_t* drawable );

// SPRITE_TYPE daemon_sprite_get_type  (Sprite* sprite);

int daemon_sprite_get_width( const Sprite* sprite );
int daemon_sprite_get_height( const Sprite* sprite );

gint       daemon_sprite_get_x      (const Sprite* sprite);
gint       daemon_sprite_get_y      (const Sprite* sprite);

int daemon_sprite_get_src_x( const Sprite* sprite );
int daemon_sprite_get_src_y( const Sprite* sprite );

void daemon_sprite_set_src_xy( Sprite* sprite, int src_x, int src_y );

void daemon_sprite_set_pixmap( Sprite* sprite, cairo_surface_t* pixmap, 
			       // GdkBitmap* mask,
                               int src_x, int src_y, int width, int height );

cairo_surface_t* daemon_sprite_get_pixmap( Sprite* sprite );

gboolean daemon_sprite_isvisible( const Sprite* sprite );

// void       daemon_sprite_set_type   (Sprite* sprite,
// 				     SPRITE_TYPE type);

void       daemon_sprite_set_xy( Sprite* sprite, int x, int y );

gboolean   daemon_sprite_on         (Sprite* sprite,
				     int x,
				     int y);

//void       daemon_sprite_set_mask   (Sprite* sprite,
//				     GdkBitmap* mask);

// GdkBitmap* daemon_sprite_get_mask   (Sprite* sprite);

void       daemon_sprite_set_visible(Sprite* sprite,
				     gboolean bool_);

// void       daemon_sprite_set_koma   (Sprite* sprite,
// 				     gint koma);

void* daemon_sprite_get_userdata( Sprite* sprite );

gboolean daemon_sprite_ismovable( const Sprite* sprite );

void       daemon_sprite_set_movable( Sprite* sprite, gboolean move);

void daemon_sprite_set_drag_from( Sprite* sprite, int x, int y );

gint       daemon_sprite_get_from_x (Sprite* sprite);
gint       daemon_sprite_get_from_y (Sprite* sprite);

void daemon_sprite_draw( Sprite* sprite, cairo_surface_t* drawable );

int daemon_sprite_point_on_sprite( Sprite* sprites[], int pt_x, int pt_y );

void daemon_sprite_set_scale( Sprite* sprite, double xscale, double yscale );

#endif /* _SPRITE_H_ */
