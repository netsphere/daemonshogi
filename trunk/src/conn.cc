/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// TCP/IP library.

// リファレンス
// http://msdn.microsoft.com/en-us/library/ms740673(VS.85).aspx


// #define GTK_DISABLE_DEPRECATED 1
// #define GDK_DISABLE_DEPRECATED 1
// #define G_DISABLE_DEPRECATED 1

#include <config.h>

#ifdef _WINDOWS
  #define WIN32_LEAN_AND_MEAN
  #define _WIN32_WINNT  0x0501
  #include <winsock2.h>
  #include <ws2tcpip.h>
#else  // UNIX
  #include <sys/socket.h>
  #include <netdb.h>
  #include <unistd.h>
  #include <errno.h>
  #define closesocket close
  #define WSAGetLastError() errno
  #define WSACleanup() 
#endif

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <string>
#include <list>
using namespace std;

G_BEGIN_DECLS
#include "support.h"
#include "interface.h"
G_END_DECLS

#include "conn.h"
#include "canvas.h"


/** 行に分けた受信データ */
static RecvBuffer* recv_buffer = NULL;

static int wait_state = 0;
static gint io_watch = 0;
static int server_fd = -1;

// typedef list<string> NetTeList;
static NetQueue net_queue;

extern GtkWidget* connect_status_dialog;


/** サーバとの接続を切断。 */
void disconnect_server()
{
  gtk_widget_hide(connect_status_dialog);

  if (server_fd < 0)
    return;

  delete recv_buffer;
  recv_buffer = NULL;
  wait_state = 0;
  net_queue.queue.clear();

  g_source_remove(io_watch);
  io_watch = 0;

  conn_close( server_fd );
  server_fd = -1;
}


static const ConnectThreadArg* connect_arg;
NetGameInfo game_info;


/**
 * 持ち時間をパース.
 */
static bool parse_time()
{
  while ( recv_buffer->recv_list.size() > 0 ) {
    string line = recv_buffer->get_front();

    if (line.find("END Time") == 0) {
      wait_state = 2;
      return true;
    }
    
    string name, value;
    int pos;

    if ( (pos = line.find(":")) > 0 ) {
      name = line.substr(0, pos);
      value = line.substr(pos + 1);
    }
    else
      name = "";

    if (name == "Time_Unit") {
      if ( value != "1sec" ) {
	si_abort( "not support except 'one second.' value = %s\n", 
		  value.c_str() );
      }
      // TODO: impl.
    }
    else if (name == "Total_Time")
      game_info.total_time = atoi(value.c_str());
    else if (name == "Byoyomi") {
    }
    else if (name == "Least_Time_Per_Move") {
    }
  }

  return false; // 引き続き呼び出してもらう
}


/**
 * CSAサーバからゲーム提案を受信する.
 * http://www.computer-shogi.org/protocol/tcp_ip_server_112.html
 * @return キューを消化したときは true
 */
static bool state_2_read()
{
  assert(g_canvas);
  printf("%s\n", __func__); // DEBUG

  bool finished = false;

  while ( recv_buffer->recv_list.size() > 0 ) {
    string line = recv_buffer->get_front();

    string name, value;
    int pos;

    if ( (pos = line.find(":")) > 0 ) {
      name = line.substr(0, pos);
      value = line.substr(pos + 1);
    }
    else
      name = "";

    if (name == "Game_ID")
      game_info.game_id = value;
    else if (name == "Name+") 
      game_info.player_name[0] = value;
    else if (name == "Name-") 
      game_info.player_name[1] = value;
    else if (name == "Your_Turn") {
      game_info.my_turn = value[0];
      assert( game_info.my_turn == '+' || game_info.my_turn == '-' );
    }
    else if ( line.find("BEGIN Time") == 0) {
      wait_state = 21;
      return false;
    }
    else if ( line.find("BEGIN Position") == 0) {
      // TODO: impl.
    }
    else if ( line.find("END Game_Summary") == 0) {
      finished = true;
      break;
    }
  }

  if (!finished) {
    printf("data partial.\n"); // DEBUG
    return false; // まだ全部届いていない
  }

  printf("receive summary.\n"); // DEBUG
  return true;
}


/** ゲーム開始 or 対戦相手からreject */
static bool state_3_read()
{
  printf("%s: line = %s\n", __func__, 
	 recv_buffer->recv_list.front().c_str()); // DEBUG

  string line = recv_buffer->get_front(); 
  if (line.find("START:", 0) == string::npos) {
    // reject ?
    // network_log_append_message("reject game.\n");
    // network_game_start = -1;
    return false;
  }

  // network_game_start = 1;
  wait_state = 4;
  return true;
}


/** 
 * 相手の手または自分の手番で時間切れを受信
 * 先読みがあるので、ここでは読むだけ
 */
static bool state_4_read()
{
  printf("%s: front = '%s'\n", __func__, 
	 recv_buffer->recv_list.front().c_str()); // DEBUG

  string line = recv_buffer->get_front();
  net_queue.queue.push_back(line);
  return true;
}


/** 
 * ネットワーク経由の手を格納.
 * ブロックする 
 */
INPUTSTATUS daemon_input_next_network_impl( const BOARD* bo, 
					    DTe* te, int* elapsed_time )
{
  printf( "%s: net_queue.size() = %lu\n", __func__, net_queue.queue.size() ); // DEBUG

  // 手が設定されるまで待つ
  while (net_queue.queue.size() == 0) {
    // 時間切れ, 中断
    if ( gmGetGameStatus(&g_game) != 0 )
      return gmGetGameStatus(&g_game);

    processing_pending( true );
  }

  INPUTSTATUS r = conn_queue_front_move( &net_queue, bo, te, elapsed_time);
  if ( r == SI_CHUDAN_REMOTE || r == SI_TIMEOUT )
    gmSetGameStatus(&g_game, r );
  return r;
}


/** ログイン待ち */
static void state_1_read()
{
  string s = "LOGIN:" + connect_arg->username + " OK";
  if ( recv_buffer->get_front() == s) {
    // network_log_append_message(_("%s login ok.\n"), arg->username.c_str());
    // arg->status_queue.push_back(_("login ok."));
    wait_state = 2;
  }
  else {
    disconnect_server();
    daemon_messagebox(g_canvas, _("login failed."), GTK_MESSAGE_ERROR);

    // ダイアログ再表示
    g_signal_emit_by_name(
		  G_OBJECT(lookup_widget(g_canvas->window, "connect_server")),
		  "activate", NULL);
  }
}


/** 
 * ソケットのコールバック関数.
 * 行を切り出して、recv_list に追加する. 
 */
static gboolean on_socket_read( GIOChannel* source,
                                GIOCondition condition,
                                gpointer data )
{
  if ( !recv_buffer->read() ) {
    // network_log_append_message(_("server shutdown or an error occured.\n"));
    disconnect_server();
    return FALSE;
  }

  GtkWidget* dialog;

  while ( recv_buffer->recv_list.size() > 0) {
    switch (wait_state) {
    case 1:
      // ログイン待ち
      state_1_read();
      break;
    case 2:
      // ゲーム情報待ち
      if (!state_2_read()) {
	if (wait_state == 2)
	  return TRUE; // データが一部届いていない. 届くまで待つ
	break;
      }

      gtk_widget_hide(connect_status_dialog);

      if ( !connect_arg->accept_immediately ) {
	// 受けるか拒否するかだけ
	dialog = create_network_game_dialog();
	if ( gtk_dialog_run(GTK_DIALOG(dialog)) != GTK_RESPONSE_OK ) {
	  csa_send_logout(server_fd);
	  disconnect_server();
	  gtk_widget_hide(dialog);
	  return FALSE;
	}
	gtk_widget_hide(dialog);
      }

      daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_PRE_GAME);
      conn_send_str(server_fd, "AGREE\n");
      wait_state = 3;
      break;
    case 21:
      // ゲーム情報 -> 持ち時間
      if ( !parse_time() )
	return TRUE; // 一部届いていない
      break;
    case 3:
      // ゲーム開始待ち
      if (state_3_read()) {
	// ゲーム開始
	g_signal_emit_by_name(G_OBJECT(g_canvas->window), 
			      "notify::startgame", NULL);
	return FALSE; // watchをいったん終了
      }
      else {
        // 対戦相手からreject
        csa_send_logout(server_fd);
        disconnect_server(); 
        daemon_messagebox(g_canvas, _("reject from the opponent"), 
			  GTK_MESSAGE_INFO);
	daemon_canvas_change_mode(g_canvas, D_CANVAS_MODE_EDIT);
        return FALSE;
      }
      break;
    case 4:
      // 相手の手または自分の手番で時間切れ
      state_4_read();
      break;
    default:
      printf("wait_state = %d\n", wait_state); // DEBUG
      abort();
      break;
    }
  }
  return TRUE; // 引き続き呼び出してもらう
}


///////////////////////////////////////////////////////////////////
// CSAプロトコル


/** CSAサーバに接続してログインする. ワーカースレッドとして呼び出す */
void* csa_connect_thread_main(void* arg_)
{
  ConnectThreadArg* arg = static_cast<ConnectThreadArg*>(arg_);

  // network_log_append_message("connect to %s:%s\n",
  //                           arg->hostname.c_str(), arg->port.c_str());
  arg->status_queue.push_back(_("connect..."));

  if ((server_fd = connect_to_server(arg->hostname, arg->port)) < 0) {
    arg->result = -1;
    return NULL;
  }

  arg->result = +1;
  return NULL;
}


/** ソケットを待つコールバック関数を登録する */
void add_watch_socket()
{
  if (!recv_buffer)
    recv_buffer = new RecvBuffer(server_fd);

  GIOChannel* channel = g_io_channel_unix_new(server_fd);
  io_watch = g_io_add_watch( channel, 
                             GIOCondition( G_IO_IN | G_IO_HUP | G_IO_ERR ),
                             on_socket_read,
                             NULL );
  assert( io_watch > 0 );
  g_io_channel_unref( channel );

  // printf("io_watch = %d\n", io_watch); // DEBUG
}


void csa_login_and_wait_socket(const ConnectThreadArg* arg)
{
  connect_arg = arg;

  add_watch_socket();
  wait_state = 1;  // サーバからのログイン許可を待つ

  // 引き続き、ログインする。
  // network_log_append_message("LOGIN %s\n", arg->username.c_str());
  csa_send_login(server_fd, connect_arg->username, connect_arg->password);
}


/**
 * 手を送信した結果 (経過秒数など) を得る.
 * ブロックする。
 */
INPUTSTATUS csa_wait_for_result( const BOARD* bo, DTe* te, int* elapsed_time )
{
  printf("%s\n", __func__); // DEBUG

  while (net_queue.queue.size() == 0) {
    if ( gmGetGameStatus(&g_game) != 0 )
      return gmGetGameStatus(&g_game);

    processing_pending( true );
  }

  INPUTSTATUS r = conn_queue_front_move( &net_queue, bo, te, elapsed_time);
  if ( r == SI_CHUDAN_REMOTE || r == SI_TIMEOUT )
    gmSetGameStatus(&g_game, r );
  return r;
}


bool net_queue_is_pending()
{
  return net_queue.queue.size() > 0;
}


int net_get_server_fd()
{
  return server_fd;
}
