
/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <config.h>
#include "learn-common.h"
using namespace std;


/** 
 * 主ラインの勝者を調べる
 * @return 先手 0, 後手 0, 引き分け -1
 */
int get_mainline_winner( const Record* record )
{
  int winner = -1;
  int te_count;

  KyokumenNode* mi = record->mi;
  for ( te_count = 0; mi && mi->moves.size() > 0; te_count++ ) {
    if ( mi->moves[0].te.special ) {
      if (mi->moves[0].te.special == DTe::RESIGN ) {
	winner = (te_count + 1) % 2;
	printf("te_count = %d, winner = %d\n", te_count, winner); // DEBUG
      }
      break;
    }
    mi = mi->lookup_child( mi->moves[0].te );
  }

  return winner;
}

