
/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <dirent.h>
#include <string>
#include "record.h"


int get_mainline_winner( const Record* record );


/** ディレクトリ内のすべてのファイルを調べる */
template <typename U>
bool traverse_all_files( const char* dirname, U& do_a_game )
{
  DIR* d = ::opendir( dirname );
  if ( !d ) {
    printf("opendir() failed.\n");
    return false;
  }

  int i = 0;
  const struct dirent* e;
  while ( (e = ::readdir(d)) != NULL ) {
    if ( std::string(e->d_name) == "." || std::string(e->d_name) == ".." )
      continue;

    i++;
    printf("file: %d: %s\n", i, e->d_name); // DEBUG

    Record* record = daemon_record_load( 
			(std::string(dirname) + "/" + e->d_name).c_str() );
    if ( record->loadstat != D_LOAD_SUCCESS ) {
      printf( "load error: %s\n", record->strerror.c_str() );
      daemon_record_free(record);
      continue;
    }

    do_a_game(record);
    daemon_record_free(record);
  }

  ::closedir( d );
  return true;
}
