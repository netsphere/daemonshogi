/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <config.h>
#include <tchdb.h>
#include "record.h"
#include "learn-common.h"
#include <string>
using namespace std;


template <typename U>
class DoAGame {
  U updater;

public:
  DoAGame(const U& x): updater(x) { }

  /** 
   * 1ゲーム分、定跡ファイルをなめて定跡を更新
   */
  void operator () (const Record* record) 
  {
    updater.clear();
    updater.set_winner( get_mainline_winner(record) );
    update_book(&record->first_board, 0, record->mi, updater);
  }

private:
  /** 再帰する */
  static void update_book( const BOARD* first_board,
		    int te_cnt,
		    const KyokumenNode* node, U& updater )
  {
    BOARD* bo = bo_dup(first_board);

    for ( ; node && node->moves.size() > 0; te_cnt++ ) {
      KyokumenNode::Moves::size_type i;
      for ( i = 1; i < node->moves.size(); i++ ) {
	// 分岐
	const MoveEdge& mv = node->moves[i];

	if ( mv.te.special ) // 投了とか
	  continue;

	U u2 = updater; // 内部状態が変更されることがある
	if ( !u2(bo, te_cnt, mv.te) )
	  continue;

	boMove_mate( bo, node->moves[i].te.pack() );
	update_book( bo, te_cnt + 1, node->moves[i].node, u2 );
	boBack_mate( bo );
      }

      // 主ラインを進める
      if ( node->moves[0].te.special )
	break;

      if ( !updater(bo, te_cnt, node->moves[0].te) )
	break;

      boMove_mate( bo, node->moves[0].te.pack() );
      node = node->moves[0].node;
    }

    freeBOARD( bo );
  }
};


/**
 * Tier2: 手の良し悪しが検討されてないもの. 悪手を含むことがある
 */
struct Tier2 
{
  int winner;
  bool opening;

  Tier2() { clear(); }

  void clear() {
    winner = -1;
    opening = true;
  }

  void set_winner(int w) { winner = w; }

  /**
   * @param bo     現在の局面
   * @param te_cnt 手数. 初手 = 0
   * @param mv     登録すべき手
   *
   * @return 引き続き呼び出してもらうとき true
   */
  bool operator () ( const BOARD* bo, int te_cnt, const DTe& mv )
  {
    // 開戦後は勝者の手のみを登録する
    if ( !opening && winner != (te_cnt % 2) )
      return true;

    int count = 0;
    BookEnt* ent = book_get( bo->hash_all, &count );

    bool to_update = true;
    bool found = false;
    for (int i = 0; i < count; i++) {
      if ( !teCmpTE(ent[i].book_move, mv.pack()) ) {
	if (ent[i].weight >= 2)
	  to_update = false;
	else
	  ent[i].weight++;

	found = true;
	break;
      }
    }

    if ( !found ) {
      // 追加する
      count++;
      ent = static_cast<BookEnt*>(realloc(ent, sizeof(BookEnt) * count) );
      ent[count - 1].book_move = mv.pack();
      ent[count - 1].weight = 1;
    }

    if (to_update)
      book_put( bo->hash_all, ent, count );
    free(ent);

    // 開戦?
    if ( opening && mv.fm && 
	 bo->board[mv.to] && (bo->board[mv.to] & 0xf) != FU ) {
      printBOARD(bo); // DEBUG
      printf("last move = %s\n", mv.to_str(*bo).c_str() );
      opening = false;
    }

    return true;
  }
};


/**
 * Tier1: 悪手にはフラグが付いている, または定跡として編集されたもの
 */
struct Tier1 {
  bool operator () ( const BOARD* bo, BookEnt* ent, int count, 
		     const MoveEdge& mv ) 
  {
    BookEnt* new_ent = static_cast<BookEnt*>( 
			     malloc(sizeof(BookEnt) * (count + 1)) );
    int new_count = 0;

    bool to_update = false;
    for (int i = 0; i < count ; i++) {
      if ( !teCmpTE(ent[i].book_move, mv.te.pack() ) ) {
	if ( ent[i].weight > 10 ) {
	  // 登録済み
	  free(ent);
	  free(new_ent);
	  return true;
	}

	new_ent[new_count] = ent[i];
	new_ent[new_count++].weight = 11; // magic
	to_update = true;
      }
      else {
	// tier2の手は削除
	if ( ent[i].weight > 10 ) {
	}
      }
    }

    free(ent);
    return true;
  }
};


static const char* BOOKFILE = "/tmp/daemonshogi-book.bin";
int main() 
{
  remove(BOOKFILE);
  book_init( BOOKFILE, true );

  DoAGame<Tier2> do_a_game = DoAGame<Tier2>( Tier2() );
  traverse_all_files("/opt/src/shogi-book/tier2", do_a_game ); // TODO: 場所
  // update_all("/opt/src/shogi-book/tier1", Tier1() );

  book_free();

  return 0;
}

