/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <assert.h>
#include <algorithm>
#include "si.h"
using namespace std;


static const int can_ohte_to[8] = {
  -16, -17, -1, 15, 16, 17, 1, -15,
};

/** 
 * 歩、香車の成り、桂馬、銀、金、角の成り、飛車の成り、
 * と、成り香、成り桂、成り銀、馬の縦横、竜の斜めでの王手を検索する。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param king_xy 受け方の玉の位置
 */
static void normalOhte(const BOARD* bo, MOVEINFO* mi, Xy king_xy) 
{
  static const int can_ohte[8][32][2] = {
      {
	/* -16 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 1, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* -17 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymmy, fu, kyo, kei, */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymny, to, narikyo, narikei */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* -1 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* +15 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymmy, fu, kyo, kei, */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymny, to, narikyo, narikei */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* +16 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 1, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* +17 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymmy, fu, kyo, kei, */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymny, to, narikyo, narikei */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* +1 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 0 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
      {
	/* -15 の位置から王手 */
	  /* sente */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymmy, fu, kyo, kei, */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, /* dymny, to, narikyo, narikei */
	  { 0, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
	  /* gote */
	  { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, /* dymmy, fu, kyo, kei, */
	  { 1, 1 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, /* gin, kin, kaku, hisya, */
	  { 0, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, /* dymny, to, narikyo, narikei */
	  { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, /* narigin, dummy, uma, ryu, dummy */
      },
  };

  PieceKind p;
  int i, j;

  // te.uti = 0;
  for (i = 0; i < 8; i++) {
    Xy const to = king_xy + can_ohte_to[ i ];
    Xy fm;

    if ( bo->board[to] == WALL )
      continue;
    
    if ( bo->board[to] != 0 && getTeban(bo->board[to]) == bo->next )
      continue;

    // pivotまで短いの
    for (j = 0; j < bo->short_attack[bo->next][to].count; j++) {
      fm = bo->short_attack[bo->next][to].from[j];
      p = bo->board[fm];

      if ( can_ohte[i][p][0] && can_ohte[i][p][1] )
        miAddWithNari( mi, *bo, fm, to );
      else {
        if ( can_ohte[i][p][0] || can_ohte[i][p][1] ) {
          mi_add_with_pin_check( mi, *bo, 
                      te_make_move(fm, to, can_ohte[i][p][0] ? 0 : 1) );
        }
      }
    }

    // pivotまでは長いの
    for (j = 0; j < bo->long_attack[bo->next][to].count; j++) {
      fm = bo->long_attack[bo->next][to].from[j];
      p = bo->board[fm];

      if ( can_ohte[i][p][0] && can_ohte[i][p][1] )
        miAddWithNari( mi, *bo, fm, to );
      else {
        if ( can_ohte[i][p][0] || can_ohte[i][p][1] ) {
          mi_add_with_pin_check( mi, *bo, 
                       te_make_move(fm, to, can_ohte[i][p][0] ? 0 : 1) );
        }
      }
    }
  }
}


/**
 * 桂の王手を探す。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param xy 相手玉 (受け方) の位置 
 */
static void keiOhte(const BOARD* bo, MOVEINFO* mi, Xy const xy) 
{
  int i, j;

  assert( mi != NULL );

  PieceKind pie = KEI + (bo->next << 4);

  for (i = 0; i < normal_to_count[pie]; i++) {
    Xy const to = xy - normal_to[pie][i];

    if (bo->board[to] == WALL)
      continue;

    if (bo->board[to] && getTeban(bo->board[to]) == bo->next)
      continue;

    for (j = 0; j < normal_to_count[pie]; j++) {
      Xy fm = to - normal_to[pie][j];
      if ( bo->board[fm] == pie )
        mi_add_with_pin_check( mi, *bo, te_make_move(fm, to, false) );
    }
  }
}


/**
 * 長い攻撃による王手を探す。
 * @param bo 検索するBOARD
 * @param mi 王手を格納するMOVEINFO
 * @param king_xy 受け側の玉の位置
 */
static void remoteOhte(const BOARD* bo, MOVEINFO* mi, Xy const king_xy) 
{
  // TE te;
  int j;
  int direc;

  assert( bo != NULL );
  assert( mi != NULL );

  int const next = bo->next;

  // te.nari = 0;
  // te.uti = 0;

  for (direc = 0; direc < 8; direc++) {
    Xy pivot;
    for (pivot = king_xy + arr_round_to[direc]; 1; 
         pivot += arr_round_to[direc]) {
      if ( bo->board[pivot] == WALL )
        break;
      if ( bo->board[pivot] && getTeban(bo->board[pivot]) == next )
        break;

      // pivot から玉をlong attackできるような駒があるか
      // te.to = pivot;
      Xy fm;

      // pivotまでは短いの
      for (j = 0; j < bo->short_attack[next][pivot].count; j++) {
        fm = bo->short_attack[next][pivot].from[j];
        PieceKind const pie = bo->board[ fm ];
        PieceKind p = (pie & 0xf);
        if (p != UMA && p != RYU)
          continue;

        if ( find(remote_to[pie], remote_to[pie] + remote_to_count[pie],
                  -arr_round_to[direc]) 
             != remote_to[pie] + remote_to_count[pie] ) {
          miAddWithNari( mi, *bo, fm, pivot );
        }
      }

      // pivotまでも長いの
      for (j = 0; j < bo->long_attack[next][pivot].count; j++) {
        fm = bo->long_attack[next][pivot].from[j];

        PieceKind const pie = bo->board[ fm ];
#ifndef NDEBUG
        assert( is_long_piece(pie) );
#endif // !NDEBUG	

        if ( find(remote_to[pie], remote_to[pie] + remote_to_count[pie],
                  -arr_round_to[direc])
             != remote_to[pie] + remote_to_count[pie] ) {
          if ( (pie & 0xf) == KYO )
            mi_add_with_pin_check( mi, *bo, te_make_move(fm, pivot, false) );
          else
            miAddWithNari( mi, *bo, fm, pivot );
        }
      }

      if ( bo->board[pivot] )
        break;
    }
  }
}


/**
 * ある駒を打つ手を生成.
 * utiOhte() から呼び出される. 
 * 二歩, 打ち歩詰めになる手は生成しない
 *
 * @param bo 局面
 * @param mi 追加するMOVEINFO
 * @param pie 駒種
 * @param xy 座標. 0x11-0x99まで
 */
static void mi_append_a_put_move( const BOARD* bo, MOVEINFO* mi, 
                                  PieceKind pie, Xy xy )
{
  TE te = te_make_put(xy, pie);

  if (bo->next == 0) {
    // 先手
    switch (pie) {
    case FU:
      if (checkFuSente(bo, xy & 0xf) || xy <= 0x19)
        return;

      // 打ち歩詰め？
      if (bo->board[xy - 16] == (OH + 0x10)) {
        if (te_is_uchifuzume(bo, te))
          return;
      }
      break;
    case KYO:
      if (xy <= 0x19)
        return;
      break;
    case KEI:
      if (xy <= 0x29)
        return;
      break;
    default:
      break;
    }
  }
  else {
    // 後手
    switch (pie) {
    case FU:
      if (checkFuGote(bo, xy & 0xf) || xy >= 0x91)
        return;

      if (bo->board[xy + 16] == OH) {
        if (te_is_uchifuzume(bo, te))
          return;
      }
      break;
    case KYO:
      if (xy >= 0x91)
        return;
      break;
    case KEI:
      if (xy >= 0x81)
        return;
      break;
    default:
      break;
    }
  }

  miAdd(mi, te);
}


/**
 * ある場所に打つ手をすべて生成
 * 2009.11 ループの内部でこの関数を呼び出すと遅い. 
 */
void mi_append_put_moves(const BOARD* bo, Xy xy, MOVEINFO* mi)
{
  assert( xy >= 0x11 && xy <= 0x99 );
  assert( !bo->board[xy] );

  int i;
  for (i = 1; i <= 7; i++) {
    if ( !bo->inhand[bo->next][i] )
      continue;

    mi_append_a_put_move(bo, mi, i, xy);
  }
}


/**
 * 歩、桂、銀、金の打ち込んで王手を探す。
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 * @param king_xy 受け方の玉の位置
 */
static void utiOhte(const BOARD* bo, MOVEINFO* mi, Xy const king_xy) 
{
  for (PieceKind pie = 1; pie <= 7; pie++) {
    if ( !bo->inhand[bo->next][pie] )
      continue;

    for (int j = 0; j < normal_to_count[pie]; j++) {
      Xy to = king_xy - normal_to[ pie + (bo->next << 4) ][j];

      if (bo->board[to]) // 盤外か駒がある
        continue;

      mi_append_a_put_move(bo, mi, pie, to);
    }
  }
}


/**
 * 遠くに効く駒(飛車、角、香)を打ち込んでの王手を探す。
 * @param bo 対象のBOARD
 * @param mi 結果を納めるMOVEINFO
 * @param king_xy 受け方の玉の位置
 */
static void UtiOhte2(const BOARD* bo, MOVEINFO* mi, Xy const king_xy) 
{
  int i;
  // TE te;

  assert( bo != NULL );
  assert( mi != NULL );

  // te.fm = 0;
  // te.nari = 0;
  // te.hint = 0;

  /* kyo */
  if ( bo->next ) {
    /* gote */
    if (bo->inhand[1][KYO]) {
      for (i = king_xy - 0x10; bo->board[i] == 0; i -= 0x10) {
        miAdd(mi, te_make_put(i, KYO) );
      }
    }
  } else {
    /* sente */
    if (bo->inhand[0][KYO]) {
      for (i = king_xy + 0x10; bo->board[i] == 0; i += 0x10) {
        miAdd(mi, te_make_put(i, KYO) );
      }
    }
  }

  /* hisya */
  if (bo->inhand[bo->next][HISHA]) {
    for (i = king_xy + 1; bo->board[i] == 0; i++) {
      miAdd(mi, te_make_put(i, HISHA) );
    }
    for (i = king_xy - 1; bo->board[i] == 0; i--) {
      miAdd(mi, te_make_put(i, HISHA) );
    }
    for (i = king_xy + 0x10; bo->board[i] == 0; i += 0x10) {
      miAdd(mi, te_make_put(i, HISHA) );
    }
    for (i = king_xy - 0x10; bo->board[i] == 0; i -= 0x10) {
      miAdd(mi, te_make_put(i, HISHA) );
    }
  }

  /* kaku */
  if (bo->inhand[bo->next][KAKU]) {
    for (i = king_xy + 0x11; bo->board[i] == 0; i+=0x11) {
      miAdd(mi, te_make_put(i, KAKU) );
    }
    for (i = king_xy + 0x0F; bo->board[i] == 0; i+=0x0F) {
      miAdd(mi, te_make_put(i, KAKU) );
    }
    for (i = king_xy - 0x0F; bo->board[i] == 0; i -= 0x0F) {
      miAdd(mi, te_make_put(i, KAKU) );
    }
    for (i = king_xy - 0x11; bo->board[i] == 0; i -= 0x11) {
      miAdd(mi, te_make_put(i, KAKU) );
    }
  }
}


static bool can_attack_to( PieceKind const p, const BOARD& bo, 
                           Xy const from, Xy const oppo_king_xy )
{
  int i;

  if ( abs((from >> 4) - (oppo_king_xy >> 4)) <= 2 && 
       abs((from & 0xf) - (oppo_king_xy & 0xf)) <= 1 ) {
    for (i = 0; i < normal_to_count[p]; i++) {
      if ( from + normal_to[p][i] == oppo_king_xy )
	return true;
    }
  }

  XyDiff direc;
  if ( is_long_piece(p) && (direc = norm_direc( from, oppo_king_xy )) != 0 ) {
    for (i = 0; i < remote_to_count[p]; i++) {
      if ( remote_to[p][i] == direc ) {
	Xy xy = from;
	do {
	  xy += direc;
	  if ( xy == oppo_king_xy )
	    return true;
	} while ( !bo.board[xy] );
      }
    }
  }

  return false;
}


/**
 * 長い攻撃の指し手を生成する
 * @param mi 結果を格納するMOVEINFO
 * @param bo 対象のBOARD
 * @param xy 駒の座標
 * @param next 駒の持ち主
 */
static void movetoLong(MOVEINFO* mi, const BOARD* bo, Xy const xy, int next)
{
  assert( 1 <= (xy & 0xf) && (xy & 0xf) <= 9 );
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( bo->board[xy] != 0 );
  assert( getTeban(bo->board[xy]) == bo->next );

  int i;
  PieceKind piece = boGetPiece(bo, xy);
  // PieceKind p = piece & 0x0F;

  for (i = 0; i < remote_to_count[piece]; i++) {
#ifdef USE_PIN
    // こちらでもpinされていないか確認しておく
    if (bo->pinned[xy]) {
      if (bo->pinned[xy] != remote_to[piece][i] &&
           bo->pinned[xy] != -remote_to[piece][i])
        continue;
    }
#endif

    bool flag = false;
    Xy vw = xy;
    while (1) {
      vw += remote_to[piece][i];

      PieceKind const c = boGetPiece(bo, vw);
      if (c == WALL)
        break;

      if (c) {
        // 自分の駒があるとダメ
	if ( getTeban(c) == next )
	  break;
        flag = true;
      }

      // te.to = vw;
      miAddWithNari( mi, *bo, xy, vw );

      if (flag)
        break;
    }
  }
}


/**
 * xy にある駒の動ける場所 (指し手) を探してmiに追加する。
 * @param bo 対象のBOARD
 * @param xy 駒の場所
 * @param mi 結果を格納するMOVEINFO
 */
static void mi_append_moveto(const BOARD* bo, Xy xy, MOVEINFO* mi)
{
  assert( 1 <= (xy & 0x0f) && (xy & 0x0f) <= 9 );
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( bo->board[xy] != 0 );
  assert( getTeban(bo->board[xy]) == bo->next );

  PieceKind const piece = bo->board[ xy ];
#if 0
  if ( (piece & 0xf) == OH )
    mi_append_king_moves( bo, mi, xy, bo->next );
  else
    append_short_onboard_moves( bo, xy, piece, mi );

  if ( is_long_piece(piece) )
    movetoLong(mi, bo, xy, bo->next);
#else
  // 展開してみる   2009.12 多少だが効果あり
  switch (piece) {
  case FU:
    miAddWithNari( mi, *bo, xy, xy - 16 );
    break;
  case KYO: case KYO + 0x10:
  case KAKU: case KAKU + 0x10:
  case HISHA: case HISHA + 0x10:
    movetoLong( mi, bo, xy, bo->next );
    break;
  case KEI:
    miAddWithNari( mi, *bo, xy, xy - 33 );
    miAddWithNari( mi, *bo, xy, xy - 31 );
    break;
  case GIN:
    miAddWithNari( mi, *bo, xy, xy - 16 );
    miAddWithNari( mi, *bo, xy, xy - 17 );
    miAddWithNari( mi, *bo, xy, xy + 15 );
    miAddWithNari( mi, *bo, xy, xy + 17 );
    miAddWithNari( mi, *bo, xy, xy - 15 );
    break;
  case KIN:
  case TOKIN: case NARIKYO: case NARIKEI: case NARIGIN:
    miAddWithNari( mi, *bo, xy, xy - 16 );
    miAddWithNari( mi, *bo, xy, xy - 17 );
    miAddWithNari( mi, *bo, xy, xy - 1 );
    miAddWithNari( mi, *bo, xy, xy + 16 );
    miAddWithNari( mi, *bo, xy, xy + 1 );
    miAddWithNari( mi, *bo, xy, xy - 15 );
    break;
  case OH:
  case OH + 0x10:
    mi_append_king_moves( bo, mi, xy, bo->next );
    break;
  case UMA:
  case UMA + 0x10:
    miAddWithNari( mi, *bo, xy, xy - 16 );
    miAddWithNari( mi, *bo, xy, xy - 1 );
    miAddWithNari( mi, *bo, xy, xy + 16 );
    miAddWithNari( mi, *bo, xy, xy + 1 );
    movetoLong( mi, bo, xy, bo->next );
    break;
  case RYU:
  case RYU + 0x10:
    miAddWithNari( mi, *bo, xy, xy - 17 );
    miAddWithNari( mi, *bo, xy, xy + 15 );
    miAddWithNari( mi, *bo, xy, xy + 17 );
    miAddWithNari( mi, *bo, xy, xy - 15 );
    movetoLong( mi, bo, xy, bo->next );
    break;
  case FU + 0x10:
    miAddWithNari( mi, *bo, xy, xy + 16 );
    break;
  case KEI + 0x10:
    miAddWithNari( mi, *bo, xy, xy + 33 );
    miAddWithNari( mi, *bo, xy, xy + 31 );
    break;
  case GIN + 0x10:
    miAddWithNari( mi, *bo, xy, xy + 16 );
    miAddWithNari( mi, *bo, xy, xy + 17 );
    miAddWithNari( mi, *bo, xy, xy - 15 );
    miAddWithNari( mi, *bo, xy, xy - 17 );
    miAddWithNari( mi, *bo, xy, xy + 15 );
    break;
  case KIN + 0x10:
  case TOKIN + 0x10: case NARIKYO + 0x10: case NARIKEI + 0x10: case NARIGIN + 0x10:
    miAddWithNari( mi, *bo, xy, xy + 16 );
    miAddWithNari( mi, *bo, xy, xy + 17 );
    miAddWithNari( mi, *bo, xy, xy + 1 );
    miAddWithNari( mi, *bo, xy, xy - 16 );
    miAddWithNari( mi, *bo, xy, xy - 1 );
    miAddWithNari( mi, *bo, xy, xy + 15 );
    break;
  default:
    abort();
  }
#endif
}


/**
 * 空き王手を探す。
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 * @param king_xy 受け方の玉の座標
 */
static void akiOhte(const BOARD* bo, MOVEINFO* mi, Xy const king_xy) 
{
  int d;

#ifdef DEBUG
  assert( bo != NULL );
  assert( mi != NULL );
#endif /* DEBUG */

#ifdef DEBUG
  assert( 1 <= (king_xy & 0x0F) && (king_xy & 0x0F) <= 9 );
  assert( 1 <= (king_xy >> 4) && (king_xy >> 4) <= 9 );
#endif /* DEBUG */

  for (d = 0; d < 8; d++) {
    Xy fxy = king_xy;
    XyDiff const vw = can_ohte_to[d];
    while (true) {
      fxy += vw;

      PieceKind const p = bo->board[ fxy ];
      if ( p == 0 )
        continue;

      if ( p == WALL )
        break;

      if ( getTeban(p) == bo->next ) {
        // 攻め方の駒に自分側のlong attackが当たっていて、かつ方角が同じ
        for (int i = 0; i < bo->long_attack[bo->next][fxy].count; i++) {
          if (norm_direc(bo->long_attack[bo->next][fxy].from[i], fxy) == -vw) {
            // 開き王手できる

            MOVEINFO mi1;
            int j;
            mi1.count = 0;
            mi_append_moveto(bo, fxy, &mi1);
            for (j = 0; j < mi1.count; j++) {
              int te_direc = norm_direc(fxy, te_to(mi1.te[j]) );
              if ( te_direc != vw && te_direc != -vw ) {
#if 1
		// 動かす手自身で王手するのは除く
		if ( can_attack_to(te_promote(mi1.te[j]) ? (p | 7) : p,
			           *bo, te_to(mi1.te[j]), king_xy) ) {
		  continue;
		}
#endif // 0
                miAdd(mi, mi1.te[j]); // hintもコピーされる
	      }
            }
          }
        }
      }
      break;
    }
  }
}


/**
 * 王手を検索して mi に格納する。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 */
void ohte(const BOARD* bo, MOVEINFO* mi) 
{
#if 0 // これはだいぶ遅い
  MOVEINFO all_mi;
  make_moves(bo, &all_mi);

  mi->count = 0;
  BOARD* tmp_bo = bo_dup(bo);
  for (int i = 0; i < all_mi.count; i++) {
    boMove_mate( tmp_bo, all_mi.te[i] );
    if ( bo_is_checked(tmp_bo, 1 - bo->next) )
      miAdd( mi, all_mi.te[i] );
    boBack_mate( tmp_bo );
  }
  freeBOARD(tmp_bo);
  return;
#endif // 0

  Xy king_xy;

  mi->count = 0;

  // 自玉が王手されている
  if ( bo_is_checked(bo, bo->next) ) {
    MOVEINFO uke_mi;
    BOARD* tmp_bo = bo_dup(bo);

    uke( bo, &uke_mi );
    int i;
    for (i = 0; i < uke_mi.count; i++) {
      boMove_mate( tmp_bo, uke_mi.te[i] );
      if ( bo_is_checked(tmp_bo, 1 - bo->next) )
        miAdd( mi, uke_mi.te[i] ); // hintもコピーされる
      boBack_mate( tmp_bo );
    }

    freeBOARD(tmp_bo);
    return;
  }

  if (bo->next) {
    /* gote */
    king_xy = bo->king_xy[0];
  } else {
    /* sente */
    king_xy = bo->king_xy[1];
  }

  // まず打ちから
  UtiOhte2(bo, mi, king_xy);
  utiOhte(bo, mi, king_xy);

  remoteOhte(bo, mi, king_xy);
  keiOhte(bo, mi, king_xy);
  normalOhte(bo, mi, king_xy);
  akiOhte(bo, mi, king_xy);

  // 2009.12 これはだいぶ遅い
  // miRemoveDuplicationTE(mi); // 移動の手がダブる
}


/**
 * 局面で着手可能なすべての指し手を生成する
 * 打ち歩詰めの手は生成しない.
 * 千日手になる手は生成する
 *
 * @param bo 局面
 * @param mi 手一覧
 */
void make_moves(const BOARD* bo, MOVEINFO* mi)
{
  int i;
  PieceKind inhand_pie[8];

  // 王手を掛けられているか
  if ( bo_is_checked(bo, bo->next) ) {
    uke( bo, mi );
    return;
  }

  mi->count = 0;

  // あらかじめ持ち駒セットを作る. 2009.11 効果あり
  int p = 0;
  for (i = 1; i <= 7; i++) {
    if ( bo->inhand[bo->next][i] > 0 )
      inhand_pie[p++] = i;
  }
  inhand_pie[p] = 0;

  for (i = 1; i <= 81; i++) {
    if ( bo->board[arr_xy[i]] ) {
      // xyから移動する手
      if ( getTeban(bo->board[arr_xy[i]]) == bo->next ) 
	mi_append_moveto(bo, arr_xy[i], mi);
    }
    else {
      // 打つ手
      for (p = 0; inhand_pie[p]; p++)
	mi_append_a_put_move( bo, mi, inhand_pie[p], arr_xy[i] );
    }
  }
}
