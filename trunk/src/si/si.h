/* -*- mode:c++ -*-
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _SI_H_
#define _SI_H_

#include <time.h>
#include <stdint.h>  // uint64_t
#include <stdio.h>
#include <stdbool.h> // bool
#include <assert.h>
#include <ostream>
#include <list>
#include <string>
#include <tr1/unordered_map>
#include "dte.h"


/** 手番インデックス */
enum TEBAN {
  SENTE = 0,
  GOTE  = 1
};

extern const char* CSA_PIECE_NAME[];

/** mate() が返すステータス */
enum MATE_STATE {
  /** 不詰み */
  NOT_CHECK_MATE = 0,

  /** 詰みあり */
  CHECK_MATE = 1,

  /** 不明(ハッシュからデータを取り出すときだけ使う) */
  UNKNOWN_CHECK_MATE = -1,
};


/** 
 * si_input_next_* が返すゲームのステータス
 * サーバ, GUI が非同期で設定するもの.
 */
enum INPUTSTATUS 
{
  /** 通常の手, ローカルからの投了 */
  SI_NORMAL = 0,

  /** 手番のプレイヤーがゲームを中断 */
  SI_CHUDAN_LOCAL = 4,

  /** サーバーからの中断指示の受信 */
  SI_CHUDAN_REMOTE = 5,

  /** 手番のプレイヤーが時間切れ負け */
  SI_TIMEOUT = 9,

  /** 直ちにプロセスを終了させる */
  SI_TO_EXIT = 10,
};


/** canvas.cc で利用 */
enum PLAYERTYPE {
  /** 人間による入力 */
  SI_HUMAN = 0,
  /** プログラムレベル１による入力 */
  SI_COMPUTER_1 = 1,
  /** プログラムレベル２による入力 */
  SI_COMPUTER_2 = 2,
  /** プログラムレベル３による入力 */
  SI_COMPUTER_3 = 3,
  /** プログラムレベル４による入力 */
  SI_COMPUTER_4 = 4,
  /** プログラムレベル５による入力 */
  SI_COMPUTER_5 = 5,

  /** ネットワークからの入力１ */
  SI_NETWORK_1 = 8,
  /** 子プロセスとパイプで通信 */
  SI_CHILD_PROCESS = 9,
  /** GUIを使った人間による入力 */
  SI_GUI_HUMAN = 10
};


#define MOVEINFO2MAX 2000

/** 指し手 (手順) を格納する構造体. */
struct MOVEINFO2 
{
  /** 指し手 */
  TE te[MOVEINFO2MAX];

  /** 指し手の数 */
  int count;
};


/** 移動先情報の最大引数 */
#define KIKI_FROM_MAX 10

struct KikiFrom {
  Xy from[ KIKI_FROM_MAX ]; // short=10, long=5
  int count;
};


/**
 * 局面の状態と棋譜.
 * 局面 = 盤面 + 持ち駒 + 手番.
 */
struct BOARD
{
  /** 局面の情報。*/
  //@{

  /** 1行追加の余白 */
  PieceKind ban_padding__[16];

  /** 
   * 盤面の駒種. 
   * インデックスは0x11..0x99まで。1一 -> 0x11, 9九 -> 0x99. 
   * 2六 -> 0x62 (yxの順).
   * 駒種は下から1-3ビットが素の駒種、ビット4が成り、ビット5が
   * プレイヤー (1=後手) 
   */
  PieceKind board[16 * 12];

  /**
   * 持駒. [0]が先手、[1]が後手の持ち駒。1行128ビット
   * プレイヤー、成りは戻して格納。
   */
  int16_t inhand[2][8];

  /** 手番 */
  int next;

  /** 手順を記録しておく. */
  MOVEINFO2 mseq;

  /**
   * 取った駒があったら覚えておく. undo用
   */
  PieceKind tori[MOVEINFO2MAX];

  //@}

  /** 利きなど、局面情報から生成できるもの */
  //@{

  /** 玉の位置 */
  Xy king_xy[2];

  /** 盤上, 持ち駒以外の駒の枚数. 盤上と合わせて40枚 */
  int16_t pbox[9];

  /** 生歩があるか */
  bool fu_exists[2][10];

  /**
   * ハッシュキー. 
   * 盤上の駒のみで、持ち駒は反映させない
   */
  uint64_t key;

  /** 持ち駒も含めたハッシュ値 */
  uint64_t hash_all;

  /** 
   * 駒ごとの利きの数. 自駒への利きもカウントする 
   * \todo think_eval() のなかでpinと同時にカウントする. 
   */
  int piece_kiki_count[16 * 10];

  /** それぞれの場所に対して利きを付けている駒の場所 */
  KikiFrom short_attack[2][16 * 10];
  KikiFrom long_attack[2][16 * 10];

  /** 
   * 過去の局面の出現回数.
   * boMove(), boBack() では更新しない 
   */
  typedef std::tr1::unordered_map<uint64_t, int> ReappearingTimes;
  ReappearingTimes reappearing_times;

#ifdef USE_PIN
  /**
   * pinされていればその方向。(上からなら+16など。)
   * たかだか一つの駒にしかpinされない。
   */
  XyDiff pinned[16 * 10];
#endif

  //@}

  //////////////////////////////////////////////////////////////
  // 思考用
#if EVAL_VERSION == 1
  /** 
   * 局面の評価点数. boMove() で更新できるものだけ. 
   * この変数を直接使わずに、think_eval() で評価を得ること 
   */
  int32_t point[2];
#endif // EVAL_VERSION == 1
};


////////////////////////////////////////////////////////////////////////
// 定跡 book.cc

/** 定跡エントリ */
struct BookEnt {
  /** 定跡手 */
  uint32_t book_move;
  
  /** 重み */
  int32_t weight;
};


bool book_init( const char* filename, bool writable );

void book_free();

BookEnt* book_get( uint64_t hash_all, int* count );

void book_put( uint64_t hash_all, const BookEnt* ent, int count );

int jouseki(const BOARD* bo, TE* te);



////////////////////////////////////////////////////////////////////////
// GAME


typedef INPUTSTATUS (*InputCallbackFunc)( const BOARD* bo, 
					  DTe* te, int* elapsed_time );

/** 
 * ゲームの進行に必要な情報を集めた構造体
 * Recordに格納するものは除く
 */
struct GAME
{
  /** 入力を受ける関数へのポインタ */
  InputCallbackFunc si_input_next[2];

  /** コンピュータの思考レベル */
  int computer_level[2];

  /** 持ち時間. 単位は秒 */
  time_t total_time[2];

  /** 1 の場合、時間切れは負けとする */
  int flg_run_out_to_lost;

  /** 消費時間の合計. それぞれの指し手ごとの消費時間は Record に記録 */
  time_t total_elapsed_time[2];

  /** 思考開始前の時刻. */
  time_t think_start_time;

  /**
   * 中断等が入ったときに入力待ちループから抜けるためのフラグ。
   * 0 ならば通常通り。
   * SI_CHUDAN ならば中断が入った。SI_TORYO ならば投了。
   * 中断または投了が入った場合、入力待ちループからすぐ抜ける。
   */
  INPUTSTATUS game_status;
};


////////////////////////////////////////////////////////////////////////
// BOARD: 局面

BOARD* newBOARDwithInit();

void freeBOARD(BOARD* bo);

void bo_init( BOARD* bo );

void boPlayInit(BOARD *bo);

void bo_reset_to_hirate(BOARD* bo);

bool bo_set_by_text( BOARD* bo, const char* s);

bool bo_load_file( BOARD* bo, const char* filename );

std::string bo_to_fixed_str( const BOARD* bo );

bool bo_set_by_fixed_str(BOARD* bo, const std::string& str);


/** 駒種をセットする */
static inline void boSetPiece(BOARD* bo, Xy xy, PieceKind p)
{
  assert( 1 <= (xy >> 4) && (xy >> 4) <= 9 );
  assert( 1 <= (xy & 0xF) && (xy & 0xF) <= 9 );

  bo->board[xy] = p;
}


void boSetTsumeShogiPiece(BOARD* bo);

void boSetToBOARD(BOARD* bo);

/** 局面を複製する */
BOARD* bo_dup(const BOARD* bo);


/**
 * xyにある駒の駒種を得る.
 * @param bo 対象のBOARD
 * @param xy 座標. 1手分だけはみ出してもよい
 */
#ifndef NDEBUG
static inline PieceKind boGetPiece(const BOARD* bo, Xy xy)
{
  assert(-1 <= (xy >> 4) && (xy >> 4) <= 11);
  assert(0 <= (xy & 0xF) && (xy & 0xF) <= 10);

  return bo->board[xy];
}
#else
#define boGetPiece(bo, xy) ((bo)->board[xy])
#endif // !NDEBUG


/** sideによる攻撃 (利き) を数える */
static inline int bo_attack_count(const BOARD* bo, int side, Xy xy)
{
  assert(xy >= 0x11 && xy <= 0x99);
  assert(side == 0 || side == 1);
  return bo->short_attack[side][xy].count + bo->long_attack[side][xy].count;
}


/** side側の玉に王手が掛かっているか */
static inline bool bo_is_checked(const BOARD* bo, int side)
{
  assert( side == 0 || side == 1 );
  return bo->king_xy[side] &&
         bo_attack_count(bo, 1 - side, bo->king_xy[side]);
}


/** 局面を手 te で進める. 局面 bo を更新する */
int boMove(BOARD* bo, const TE& te);

void boBack(BOARD* bo);

void boMove_mate(BOARD* bo, const TE& te);
void boBack_mate(BOARD* bo);

/** パスする */
void bo_pass( BOARD* bo );


static inline int bo_repetition_count( const BOARD* bo )
{
  BOARD::ReappearingTimes::const_iterator rtit;
  rtit = bo->reappearing_times.find( bo->hash_all );
  if ( rtit == bo->reappearing_times.end() )
    return 0;
  return rtit->second;
}


static inline void bo_inc_repetition( BOARD* bo )
{
  BOARD::ReappearingTimes::iterator rtit;
  rtit = bo->reappearing_times.find( bo->hash_all );
  if ( rtit != bo->reappearing_times.end() )
    rtit->second++;
  else
    bo->reappearing_times.insert( std::pair<uint64_t, int>(bo->hash_all, 1) );
}


static inline void bo_dec_repetition( BOARD* bo )
{
  BOARD::ReappearingTimes::iterator rtit;
  rtit = bo->reappearing_times.find( bo->hash_all );
  if ( rtit->second == 1 )
    bo->reappearing_times.erase( rtit );
  else
    rtit->second--;
}


/** 本将棋として局面が妥当かどうか */
int boCheckJustGame(const BOARD* bo);

int boCheckJustMate(const BOARD* bo);

/** record.cc から参照される */
void bo_update_board_by_csa_line(BOARD* bo, const char* s, int* error);

int boCmp(const BOARD* bo1, const BOARD* bo2);

static inline bool operator == (const BOARD& x, const BOARD& y) {
  return !boCmp(&x, &y);
}

void printBOARD(const BOARD* bo, int front = 0);


////////////////////////////////////////////////////////////////////////
// MOVEINFO: 候補手  ohte.cc uke.cc moveinfo.cc

extern const Xy arr_xy[];

extern const XyDiff arr_round_to[];
extern const XyDiff arr_round_to24[];

extern const int remote_to_count[];
extern const XyDiff remote_to[][8];
extern const XyDiff normal_to[][8];
extern const int normal_to_count[];


/** 可能な合法手の最大は593 */
#define MOVEINFOMAX 593

/** 指し手 (候補手) を格納する構造体 */
struct MOVEINFO 
{
  /** 指し手 */
  TE te[MOVEINFOMAX + 1];
  /** 指し手の数 */
  int count;
};


MOVEINFO* newMOVEINFO();

void freeMOVEINFO(MOVEINFO* mi);


/** mi に指し手を追加する */
static inline void miAdd( MOVEINFO* mi, const TE& te )
{
  assert( mi->count < MOVEINFOMAX );
  mi->te[mi->count++] = te;
}


/** miに手を追加する。成らずと成りの両方。 */
void miAddWithNari( MOVEINFO* mi, const BOARD& board, Xy from, Xy to );

void mi_add_with_pin_check( MOVEINFO* mi, const BOARD& bo, const TE& te );

void miRemoveTE2(MOVEINFO *mi, int num );
void miRemoveDuplicationTE(MOVEINFO *mi);

/** コピーする */
void miCopy(MOVEINFO* dest, const MOVEINFO* src);

void mi_print( const MOVEINFO* mi, const BOARD& bo );
bool mi_includes( const MOVEINFO* mi, const TE& te );

void mi_append_king_moves(const BOARD* bo, MOVEINFO* mi, Xy king_xy, int next);

/** 合法手をすべて生成する */
void make_moves(const BOARD* bo, MOVEINFO* mi);

void mi_append_put_moves( const BOARD* bo, Xy xy, MOVEINFO* mi );

void ohte(const BOARD* bo, MOVEINFO* mi);

/** 王手を受ける手を生成する */
int uke(const BOARD* bo, MOVEINFO* mi );


////////////////////////////////////////////////////////////////////////
// misc

void si_abort(const char* fmt, ...) __attribute__((noreturn));

XyDiff norm_direc(Xy xy1, Xy xy2) __attribute__((const));


/** 受信バッファ */
struct RecvBuffer
{
  /** 行に分ける */
  typedef std::list<std::string> RecvList;

  int fd;

  /** 行に分ける前のデータ */
  std::string sock_buffer;

  /** 行に分けた受信データ */
  RecvList recv_list;

  RecvBuffer(int fd_): fd(fd_) { }

  void clear() {
    fd = -1;
    sock_buffer = "";
    recv_list.clear();
  }

  int read();

  /* キューから1行取り出す */
  std::string get_front() {
    std::string r = recv_list.front();
    recv_list.pop_front();
    return r;
  }
};


/** 文字列を分割する. boostの代替 */
template <typename Container>
void str_split( Container* container, const std::string& str, char delim )
{
  int pos = 0;
  std::string::size_type i;
  for (i = 0; i < str.size(); i++) {
    if ( str[i] == delim ) {
      container->push_back( std::string(str, pos, i - pos) );
      pos = i + 1;
    }
  }
  container->push_back( std::string(str, pos, i - pos) );
}

std::string str_format( const char* format, ... );

void si_start_alarm();
void si_end_alarm();


/** 子プロセス情報 */
struct ChildProcess 
{
  /** プロセスID */
  pid_t pid;

  /** 書き込み用パイプ */
  int write_fd;

  /** 読み込み用パイプ */
  int read_fd;

  /** 構造体を初期化 */
  void clear() {
    pid = -1; 
    write_fd = -1; read_fd = -1;
  }

  /** 子プロセスを終了させる.
    \todo impl.
   */
  void kill() {
    assert(0); // TODO: impl
  }
};


int spawn_async_with_pipe(const char* working_dir, char** argv, 
			  ChildProcess* x);


////////////////////////////////////////////////////////////////////////
// MOVEINFO2: 手順

MOVEINFO2* newMOVEINFO2();

void freeMOVEINFO2(MOVEINFO2* mi);

void mi2Copy(MOVEINFO2* dest, const MOVEINFO2* src);


/**
 * MOVEINFO2 に TE を追加する。
 * @param mi 対象のmi
 * @param te 追加するte
 */
static inline void mi2Add(MOVEINFO2* mi, const TE& te) 
{
  assert(mi != NULL);
  // printf("mi->count = %d\n", mi->count); // DEBUG
  assert( mi->count < MOVEINFO2MAX );

  mi->te[mi->count++] = te;
}


void mi2_print_sequence( const MOVEINFO2* mi, const BOARD& bo );


////////////////////////////////////////////////////////////////////////
// TE: 指し手


/**
 * TEを生成する。
 * @param from 移動元の座標
 * @param to 移動先のX座標
 * @param promote 成りフラグ。0 なら成らず、1 なら成り。
 *
 * @return TEインスタンス
 */
static inline TE te_make_move(Xy from, Xy to, bool promote) 
{
#ifdef USE_TE32
  return (((unsigned int) from) << 24) | (((unsigned int) to) << 16) | 
         (promote ? 0x8000 : 0);
#else
  TE r;
  r.fm = from; r.to = to; r.nari = promote; r.uti = 0; r.hint = 0;
  return r;
#endif // USE_TE32
}


/** 打つ手を生成 */
static inline TE te_make_put(Xy to, PieceKind pie) 
{
#ifdef USE_TE32
  return (((unsigned int) to) << 16) | (pie << 10);
#else
  TE r;
  r.fm = 0; r.to = to; r.nari = 0; r.uti = pie; r.hint = 0;
  return r;
#endif // USE_TE32
}


#ifdef USE_TE32
  #define TE_NUL  0
#else
extern TE TE_NUL;
#endif


/** 手をクリアする */
static inline void te_clear(TE* te)
{
#ifdef USE_TE32
  *te = 0;
#else
  te->fm = 0; te->to = 0; te->nari = 0; te->uti = 0; te->hint = 0;
#endif
}


#ifdef USE_TE32
  #define te_pack(te) ( te )
#else
/** 置換表に登録するために、指し手をpackする */
static inline uint32_t te_pack(const TE& te) 
{
  return (((unsigned int) te.fm) << 24) + (((unsigned int) te.to) << 16) + 
         (te.nari ? 0x8000 : 0) + 
    (te.uti << 10) + (te.hint & 0x3ff);
}
#endif


#ifdef USE_TE32
  #define te_unpack(packed_te)  ( packed_te )
#else
/** TE に戻す */
static inline TE te_unpack(uint32_t packed_te)
{
  TE te;
  te.fm = (packed_te >> 24) & 0xff; // 8bit
  te.to = (packed_te >> 16) & 0xff; // 8bit
  te.nari = (packed_te >> 15) & 1; // 1bit
  te.uti = (packed_te >> 10) & 0x1f; // 5bit. TODO: プレイヤビットも格納
  te.hint = packed_te & 0x3ff; // 10bit
  return te;
}
#endif


/** 打つ手かどうか */
static inline bool te_is_put(const TE& te)
{
  return !te_from(te) && te_to(te);
}

#ifdef USE_TE32
  #define te_add_hint(te, hint) ( *(te) |= (hint) )
#else
  #define te_add_hint(te, hint_) ( (te)->hint |= (hint_) )
#endif

#ifdef USE_TE32
static inline void te_set_hint(TE* te, uint16_t hint) {
  *te = (*te & ~ 0x3ff) | hint;
}

static inline void te_set_promote(TE* te, bool promote) {
  *te = promote ? (*te | 0x8000) : (*te & ~ 0x8000) ;
}
#else
static inline void te_set_hint(TE* te, uint16_t hint) {
  te->hint = hint;
}

static inline void te_set_promote(TE* te, bool promote) {
  te->nari = promote;
}
#endif


/**
 * 手を比較する. hint は無視する
 * @param te1 比較する手１
 * @param te2 比較する手２
 * @return 同じならば 0 、違いがあれば非 0 を返す
 */
static inline int teCmpTE( const TE& te1, const TE& te2 ) 
{
#ifdef USE_TE32
  return (te1 & 0xfffffc00) - (te2 & 0xfffffc00);
#else
  return te1.fm == te2.fm && te1.to == te2.to && te1.nari == te2.nari &&
    te1.uti == te2.uti;
#endif
}


void te_print(const TE& te, std::ostream& ost);


/** デバッグ用に手を文字列化 */
std::string te_str( const TE& te, const BOARD& bo );

void teSetTE(TE* te, Xy fm, Xy to, bool nari, PieceKind uti);


static inline bool is_long_piece( PieceKind p ) __attribute__((const));
/** 長い攻撃のできる駒か */
static inline bool is_long_piece( PieceKind p ) 
{
  switch (p & 0xf) {
  case KYO: case HISHA: case RYU: case KAKU: case UMA:
    return true;
  default:
    return false;
  }
}

/** 手が合法手か */
bool te_is_valid( const BOARD* bo, const TE& te);

bool te_is_sennichite(const BOARD* bo, const TE& te, bool* forbidden);

int can_move_promote(const BOARD* bo, const TE& te);

bool te_is_uchifuzume(const BOARD* bo, const TE& te);


////////////////////////////////////////////////////////////////////////
// ネットワーク接続 conn-base.cc

int connect_to_server(const std::string& hostname, const std::string& service);
void conn_close( int server_fd );

void conn_send_str(int server_fd, const char* line);
int conn_send_move(int server_fd, const BOARD* board, const DTe& te);

int csa_send_login( int server_fd, 
		    const std::string& username, const std::string& password);
void csa_send_logout( int server_fd );
int csa_send_chudan_request( int server_fd );


struct NetQueue {
  DTe pending_move;
  std::list<std::string> queue;
};

INPUTSTATUS conn_queue_front_move( NetQueue* net_queue,
				   const BOARD* bo,
				   DTe* te, int* elapsed_time );


////////////////////////////////////////////////////////////////////////
// 置換表 hash.cc


extern const uint64_t hashseed_inhand[2][8][19];
extern const uint64_t hashval[32][16 * 10];
struct PhiDelta;


/** 置換表のエントリ. 持ち駒も区別する. PODでなければならない */
struct EntVariant 
{
  /** 
   * 持駒 (packed). ビットの数で表すわけではないので、
   * 計算できるのは一致のみ. 
   */
  uint32_t packed_inhand[2];

  /** 得点 */
  int32_t phi, delta;

  /** 記録したときの深さ. 詰将棋で逃れのときだけ利用する */
  int16_t depth;

  // x86 (32bit) でもx86-64でもpaddingは不要

  /** このノードの手番側が詰めろを掛けられているか. 通常探索だけで利用 */
  uint16_t flags;

  /** 最善手 (参考) */
  uint32_t best_move;
};
// x86-64: 構造体は8バイト単位。
#define HASH_ENT_SIZE 24


/** 盤面を記録する置換表のエントリ. PODでなければならない */
struct HASH
{
#if 0
  /** 
   * 持ち駒を含まないハッシュ値.
   * \todo 削除する？ 
   */
  uint64_t key;
#endif // 0

  /** 得点. 複数記録する */
  struct EntVariant ent[1];
};
#define HASH_HEADER_SIZE 0 // ent[]は除く


/** 置換表を初期化する */
void newHASH();

void freeHASH();

uint64_t hsGetHashKey(const BOARD* bo);

/** 局面を置換表に登録する。詰将棋用 */
void hsPutBOARD( int tag, const BOARD* bo, int32_t phi, int32_t delta,
                 int deep, const TE& best_move );

int hsGetBOARD( int tag, const BOARD* bo, int32_t* phi, int32_t* delta, TE* te,
                int req_deep );

void hsPutBOARDMinMax( const BOARD* bo, int32_t phi, int32_t delta, 
		       uint16_t node_flags, const TE& best_move );

int hsGetBOARDMinMax( const BOARD* bo, PhiDelta* score, TE* te, 
                      uint16_t* node_flags );

void bo_add_hash_value(BOARD* bo, Xy xy);
void bo_sub_hash_value(BOARD* bo, Xy xy);

int hs_entry_count(int tag);

bool hs_minimize_proof_pieces(int offense_side, BOARD* bo);
void hs_get_best_sequence(int level, const BOARD* bo, MOVEINFO2* mi);


////////////////////////////////////////////////////////////////////////
// 詰将棋・必至探索  mate.cc


MATE_STATE dfpn_mate( BOARD* bo, int depth, TE* best_move, int* node_limit );

MATE_STATE bfs_hisshi( BOARD* bo, int tesuki, int depth, TE* best_move, 
		       int* node_limit );


////////////////////////////////////////////////////////////////////////
// 通常探索  minmax.cc


INPUTSTATUS bfs_minmax(BOARD* bo, int depth, PhiDelta* score, TE* best_move);


/* think */
void think(BOARD *bo, MOVEINFO *mi, int depth);

bool think_is_tsumero(BOARD* bo, int teban);
void think_tsumero_uke(BOARD* bo, MOVEINFO* mi_ret);

bool think_hisshi(BOARD* bo, int tenuki, int depth, TE* te_ret);

int32_t think_eval(const BOARD* bo);

// int think_count_attack24( const BOARD* bo, int side, Xy target );
// int think_count_attack8( const BOARD* bo, int side, Xy target );


/* GAME */
GAME *newGAME(void);
void initGAME(GAME *game);
void freeGAME(GAME *game);

bool think_init( const std::string& filename );

InputCallbackFunc gmGetInputNextPlayer(const GAME* gm, int next);

int gmGetComputerLevel(GAME *gm, int next);

/** プレイヤー種別とコールバック関数を設定 */
void gmSetInputFunc(GAME* gm, int next, InputCallbackFunc fn);

void gmSetGameStatus(GAME* gm, INPUTSTATUS status);

void gm_add_elapsed_time( GAME* gm, int side, int t );

void gm_set_think_start_time(GAME* gm);

extern GAME g_game;

/* for GUI support */
void si_set_pending_function( void (*f)(bool may_block) );

void processing_pending( bool may_block );


/**
 * ゲームのステータスを返す。
 * @param gm 対象のGAME
 * @return ゲームのステータス (SI_CHUDAN など)
 */
static inline INPUTSTATUS gmGetGameStatus(const GAME* gm) 
{
  return gm->game_status;
}


/* misc */

void time2string(char *buf, time_t t);


static inline int getTeban(PieceKind p) __attribute__((const));
/**
 * 駒種類の手番を返す.
 * @param p 駒種類
 * @return 0ならば先手、1ならば後手。
 */
static inline int getTeban(PieceKind p)
{
  assert( p != 0 );
  assert( p != WALL );

  return (p & 0x10) == 0x10;
}




int notSentePiece(int p);
int notGotePiece(int p);

/** 歩がすでにないかどうか */
static inline bool checkFuSente(const BOARD* bo, int x) {
  return bo->fu_exists[0][x];
}

static inline bool checkFuGote(const BOARD* bo, int x) {
  return bo->fu_exists[1][x];
}


#endif /* _SI_H_ */
