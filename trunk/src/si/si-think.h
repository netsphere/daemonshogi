/* -*- mode:c++ -*-
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __SI_THINK__
#define __SI_THINK__ 1

#include "si.h"
#include <stdio.h>
#include <math.h> // expf()
#include <set>  // multiset
#include <iostream>


////////////////////////////////////////////////////////////////////////
// PhiDelta

/** 詰め将棋, 通常探索での得点 */
struct PhiDelta {
  int32_t phi, delta;
};

static inline PhiDelta make_phi_delta(int32_t phi, int32_t delta)
{
  PhiDelta pd;
  pd.phi = phi; pd.delta = delta;
  return pd;
}


static inline bool operator == (const PhiDelta& x, const PhiDelta& y) 
{
  return x.phi == y.phi && x.delta == y.delta;
}

inline std::ostream& operator << (std::ostream& os, const PhiDelta& x) {
  os << "(" << x.phi << "," << x.delta << ")";
  return os;
}

/** 指し手と指した後の得点のペア */
struct MoveScore {
  TE te;
  PhiDelta score;
};


struct MoveScoreGreater {
  bool operator () (const MoveScore& x, const MoveScore& y) const {
    return x.score.delta > y.score.delta;
  }
};


/** (指し手, 得点) の順序づけたセット */
typedef std::multiset<MoveScore, MoveScoreGreater> RankSet;


////////////////////////////////////////////////////////////////////////

#if EVAL_VERSION == 1
/** 評価関数のパラメータ */
struct NPC
{
  /** 
   * 盤上の駒の基本点. これに relative_mod を乗じる
   * 0emp 1歩   香   桂   銀    金   角    7飛
   * 8玉  9と
   * 制約: 歩 = 100, (金 x 2) > 龍
   */
  int32_t base_piece_points[16];

  /**
   * 相手玉からの相対位置による乗数の加減.
   *   0   +1   +2   +3  +4  +5  +6  +7  +8 (X軸, 絶対値)
   * Y軸: 先手で、相手玉より上+8にあるとき
   * 同 +7
   *
   * 値は 100 が基本
   * 長い利きを持つ駒, 玉は対象外
   * TODO: 利き評価とどちらがよい？
   */
  int32_t offense_relative_mod[17][9];

  /** 
   * 自玉からの相対位置による乗数の加減
   * 値は 100が基本
   * 長い利きを持つ駒, 玉は対象外
   */
  int32_t defense_relative_mod[17][9];

  /**
   * 持ち駒の点数. 内部で 100を掛ける
   * 制約: each 持ち駒 > 基本点数
   * TODO: 歩切れの評価を悪くする
   */
  int32_t inhand_points[8];

  /**
   * 玉の位置による加算
   * 内部で 100 を掛ける. 
   * 上下は先手から見て
   */
  int32_t king_xy_points[16 * 10];

  /** 歩が上に行くほど加算する点数 */
  int32_t fu_position_bonus;

  /** 
   * 利きの数 (attack) に乗じる点数.
   * 一応、大駒以外も加算できるようにする 
   */
  int32_t attack_count_bonus[16];

  /** 
   * 当たりの加算. 利きが一つでも当たっているとき
   * 駒の基本点のどのぐらいを加算するか. 基本点に乗じる
   */
  int32_t atari_mod;

  /**
   * 相手玉の周りの利き制圧度 (攻め方の利きの数 - 受け方の利きの数)
   * 内部で 100 を乗じる
   * TODO: 駒の位置による評価とどちらがいい？
   */
  int32_t control_around_oppo_king[17][9];

  /** 
   * 玉の自由度 (低いと評価を減じる)
   * 玉が移動できるマス目の数. とりあえず2手で
   * 制圧度が0でも相手の利きがあれば移動できない
   */
  int32_t king_movables[25];
};
#endif // EVAL_VERSION == 1


/** 
 * 勝ちの値. 
 * (2^31 / 2 - 1). 評価ルーチンでの値域の外側でなければならない 
 */
#define VAL_INF 999999999

/** 探索深さ足らずで逃げ切った (本当は詰んでるかもしれない). 詰め将棋でだけ使う */
#define VAL_MAY_MATCH (VAL_INF - 10)

/** 末端-1ノードでの仮の値 */
#define VAL_LEAF_UNKNOWN (VAL_INF - 20)

/** 通常探索内部での詰め探索のノード数 */
#define GAME_MATE_NODE_LIMIT 1500


/* grade */

int grade(const BOARD* bo, int next);
int add_grade(const BOARD* bo, Xy xy, int next );

int score_inhand( const BOARD* bo, PieceKind piece, int next );

int oh_safe_grade(const BOARD* bo, Xy king_xy, int next);

int add_oh_safe_grade(const BOARD* bo, int next);

void sub_oh_safe_grade(BOARD *bo, int next);

int think_count_attack24(const BOARD* bo, int side, Xy king);
int think_count_king_movable2(const BOARD* bo, int side);


struct TsumeroNogare
{
  int ac, mc, mkm;
  int offense_side;

  void before_move(const BOARD* node, int offense_side_);

  bool promise(const BOARD* node) const;
};


/** 探索のためのパラメータ */
struct SearchParam
{
  int te_count;
  int start_depth;
};


extern void search_expand_trial_values( const SearchParam& param,
					BOARD* node,
					int rest_depth, int rest_node,
					RankSet* rank );


////////////////////////////////////////////////////////////////////////

#if EVAL_VERSION == 3

/** 要素の数 */
#define XVEC_SIZE 1381
#define XVEC_ROUND 1384 // roundup(xvec_size + 1). 8の倍数にする

/** 中間層 (隠れ層) の数 */
// #define HIDDEN_SIZE 799  // (hidden_size + 1) を 8の倍数にする


/** シグモイド関数. 単精度 */
static inline float sigmoid(float x, float gain) __attribute__((const));
static inline float sigmoid(float x, float gain)
{
  return 1.0 / (1.0 + expf(-gain * x));
}

void eval_set_xvec(const BOARD* bo, int16_t* xvec);

#endif // EVAL_VERSION == 2


////////////////////////////////////////////////////////////////////////

/** 受け側の無駄打ちを負けとして置換表に登録 */
template <typename PutWinOp>
void mate_set_muda_uchi(int offense_side, int tesuki, 
                        const PutWinOp& put_win, BOARD* node, 
                        const MOVEINFO* cand_moves, int start_index)
{
  assert( offense_side == node->next );

#if DEBUGLEVEL >= 1
  printf("%s: enter.\n", __func__); // DEBUG
  printBOARD(node); // DEBUG
#endif // DEBUGLEVEL
  
  int i, j, k;

  // 元の手順を得る
  MOVEINFO2 seq;
  hs_get_best_sequence(tesuki - 1, node, &seq);

#if DEBUGLEVEL >= 1
  mi2_print_sequence(node, &seq);
#endif // DEBUGLEVEL

  boBack_mate(node); // 受け側の打つ手を戻す
  // printf("bad move: %s\n", te_str(&node->mi.te[node->mi.count], *node).c_str());

  BOARD* bo2 = bo_dup(node);
#if DEBUGLEVEL >= 1
  BOARD* debug_bo = bo_dup(node); // DEBUG
#endif // DEBUGLEVEL

  // node は元に戻しておく
  boMove_mate( node, node->mseq.te[node->mseq.count] );

  // 詰め手順に影響がなければ無駄打ち
  for (i = start_index; i < cand_moves->count; i++) {
    if ( !te_is_put(cand_moves->te[i]) )
      continue;

    boMove_mate(bo2, cand_moves->te[i]); // 打つ手を試す
    Xy pie_xy = te_to(cand_moves->te[i]);
/*
    int32_t phi, delta;
    if ( hsGetBOARD(tt_tag(param), bo2, &phi, &delta, NULL, param.te_count) ) {
      printf("dupli!!\n");
      // boBack_mate(bo2);
      break;
    }
*/
    bool may_not_lose = false;
    for (j = 0; j < seq.count; j++) {
      MOVEINFO mi;
      make_moves(bo2, &mi);

      if ( (j % 2) == 1 ) {
        // 受け方.
        
        // 元の手順では投了だが、試した手では続けられることがある.
        if ( !te_to(seq.te[j]) ) {
          if ( mi_includes(&mi, te_make_move(pie_xy, te_to(seq.te[j - 1]), 
					     false)) ||
               mi_includes(&mi, te_make_move(pie_xy, te_to(seq.te[j - 1]), 
					     true)) ) {
            may_not_lose = true;
            printf("rsn 1 ");
            break;
          }
          break;
        }

        // 元の手順の受けが指せない
        if ( !mi_includes(&mi, seq.te[j]) ) {
          // 同じところだが違う駒を打った
          // 違うところに打った
          bool can_move = mi_includes(&mi, 
         	      te_make_move(pie_xy, te_to(seq.te[j]), false) );
          bool can_move_with_p = mi_includes(&mi, 
		       te_make_move(pie_xy, te_to(seq.te[j]), true) );
          if ( !can_move && ! can_move_with_p ) {
            // 試す駒でも受けられない -> 負け
            break;
          }

          if ( (can_move && !can_move_with_p) || 
	       (!can_move && can_move_with_p) ||
               te_to(seq.te[j + 1]) == te_to(seq.te[j]) ) {
            // 手を継いで進める
            boMove_mate(bo2, te_make_move(pie_xy, te_to(seq.te[j]), 
					  can_move_with_p));
            pie_xy = te_to(seq.te[j - 1]);
            continue;
          }
          
          // 不明
          may_not_lose = true;
          printf("rsn 2 ");
          break;
        }

        // 新たに取る手が発生
        if ( te_to(seq.te[j]) != te_to(seq.te[j - 1]) && 
             (mi_includes(&mi, te_make_move(pie_xy, te_to(seq.te[j - 1]), false)) ||
              mi_includes(&mi, te_make_move(pie_xy, te_to(seq.te[j - 1]), true))) ) {
          // TODO: 攻め方の大駒の移動経路にある受け方の利きのある場所への移動
          // TODO: 元の手順で取れない場合のみ
          may_not_lose = true;
          // printf("rsn 3 ");
          break;
        }

        if ( te_from(seq.te[j]) == pie_xy)
          pie_xy = te_to(seq.te[j]);
      }
      else {
        // 攻め方
        if ( !mi_includes(&mi, seq.te[j]) ) {
          may_not_lose = true;
          // printf("rsn 4 ");
          break; // 影響あり
        }

        if ( te_to(seq.te[j]) == pie_xy) {
          // 打ち駒を取る -> 以降に影響なし
          break;
        }
      }

      boMove_mate(bo2, seq.te[j]);
    }

    // 巻き戻す
    for (k = 0; k < j; k++)
      boBack_mate(bo2);

    if (!may_not_lose) {
      assert( offense_side == bo2->next );
      put_win(bo2, seq.te[0]);
      // hsPutBOARD( tt_tag(param), bo2, 0, +VAL_INF, param.te_count, &seq.te[0] );
    }
    else {
#if DEBUGLEVEL >= 1
      printf("not cut: %s\n", te_str(debug_bo, &cand_moves->te[i]).c_str() );
#endif 
    }

    boBack_mate(bo2);
  }

  freeBOARD(bo2);
#if DEBUGLEVEL >= 1
  freeBOARD(debug_bo); // DEBUG
#endif
}


#endif // __SI_THINK__
