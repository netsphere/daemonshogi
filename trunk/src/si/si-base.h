/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SI_BASE_H
#define SI_BASE_H 1

#include <stdint.h>

//////////////////////////////////////////////////////////////////
// 開発のためのフラグ. ライブラリ利用者は変更不可

/** 検索中にハッシュを使う場合はこのフラグを定義する */
#define USE_HASH 1

/** pin情報を使うか */
// #define USE_PIN 1
#undef USE_PIN

/** シリアルポートを使って対局する場合はこのフラグを定義する */
// #undef USE_SERIAL 

/** 評価関数のパラメータを交換可能にする */
#define USE_NPC 1
// #undef USE_NPC

#define USE_TE32 1
// #undef USE_TE32 // for debug. operator == を使っていないか

#define EVAL_VERSION 1


//////////////////////////////////////////////////////////////////
// ライブラリ利用者が選択する

/** 検索中にGUIのシグナルを処理する場合はこのフラグを定義する */
#define USE_GUI 1


/** デフォルトの制限時間 */
#define DEFAULT_LIMIT_TIME 1500


//////////////////////////////////////////////////////////////////

/** 駒種. 引き算はない */
// typedef uint16_t PieceKind;
typedef uint32_t PieceKind;


/** 駒種の定義 */
typedef enum {
  RYU     = 0x0F,
  UMA     = 0x0E,
  // skip
  NARIGIN = 0x0C,
  NARIKEI = 0x0B,
  NARIKYO = 0x0A,
  TOKIN   = 0x09,
  OH      = 0x08,
  HISHA   = 0x07,
  KAKU    = 0x06,
  KIN     = 0x05,
  GIN     = 0x04,
  KEI     = 0x03,
  KYO     = 0x02,
  FU      = 0x01
} KOMATYPE;

#define WALL 0x20


// typedef int16_t Xy;
// typedef int16_t XyDiff;
typedef int32_t Xy;
typedef int32_t XyDiff;


/** 指し手 */
#ifdef USE_TE32
typedef uint32_t TE;
#else
struct TE { int fm, to, nari, uti, hint; }; // for debug
#endif


/** 手のヒント情報 */
enum {
  TE_STUPID = 1,
  TE_CHECK = 2,
  TE_TSUMERO = 4,
  TE_2TESUKI = 8,
};

/** ノードのヒント情報 */
enum {
  NODE_TSUMERO = 4,
  NODE_2TESUKI = 8,
};


#ifdef USE_TE32
  #define te_from(te)    ( (Xy) ((te) >> 24) )
  #define te_to(te)      ( (Xy) (((te) >> 16) & 0xff) )
  #define te_promote(te) ( ((te) >> 15) & 1 )
  #define te_pie(te)     ( ((te) >> 10) & 0x1f )
  #define te_hint(te)    ( (te) & 0x3ff )
#else
  #define te_from(te)    ( (te).fm )
  #define te_to(te)      ( (te).to )
  #define te_promote(te) ( (te).nari )
  #define te_pie(te)     ( (te).uti )
  #define te_hint(te)    ( (te).hint )
#endif


#endif // SI_BASE_H
