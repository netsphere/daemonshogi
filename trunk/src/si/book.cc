/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "si.h"
#include <tchdb.h>


static TCHDB* hdb = NULL;


/** 
 * 定跡ファイルを開く
 * @return ファイルを開けなかったとき false
 */
bool book_init( const char* filename, bool writable )
{
  assert( (sizeof(BookEnt) % 8) == 0 );

  if ( hdb )
    return true;

  hdb = tchdbnew();
  bool r = tchdbopen( hdb, filename,
		      writable ? (HDBOWRITER | HDBOCREAT) : HDBOREADER );

  if ( !r ) {
    tchdbdel(hdb);
    hdb = NULL;
  }

  srandom( time(NULL) );
  return r;
}


/** 定跡ファイルを閉じる */
void book_free()
{
  if ( hdb ) {
    tchdbclose(hdb);
    tchdbdel(hdb);
    hdb = NULL;
  }
}


/** 
 * 定跡を探す 
 * @return 定跡手. 呼び出した側がfree() すること
 */
BookEnt* book_get( uint64_t hash_all, int* count )
{
  if (!hdb) {
    *count = 0;
    return NULL;
  }

  int size = 0;
  BookEnt* ent = static_cast<BookEnt*>(
	       tchdbget(hdb, &hash_all, sizeof(hash_all), &size) );
  if ( !ent ) {
    *count = 0;
    return NULL;
  }

  *count = size / sizeof(BookEnt);
  return ent;
}


void book_put( uint64_t hash_all, const BookEnt* ent, int count )
{
  assert( hdb );
  tchdbput( hdb, &hash_all, sizeof(hash_all), ent, sizeof(BookEnt) * count );
}


/**
 * 定跡 (opening book) 手があるか調べてあったら te に格納する。
 *
 * @param bo 対象のBOARD
 * @param te 見つかった手 (出力)
 * @return 1 ならば定跡手があった。 0 ならばなし。
 */
int jouseki(const BOARD* bo, TE* te)
{
  int count = 0;
  BookEnt* ent = book_get( bo->hash_all, &count );
  if ( !ent )
    return 0;

  // TODO: weight を考慮
  *te = ent[ random() % count ].book_move;
  free(ent);

  return 1;
}

