/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "si-base.h"

#include <stdio.h>
#include <assert.h>
#include "si.h"
#include "si-think.h"
#include <algorithm>
#include <string.h>
#include <emmintrin.h> // SSE2
using namespace std;


/** 
 * side による target の周りへの利きの合計.
 * bo->next と side は同じとは限らない
 */
int think_count_attack24(const BOARD* bo, int side, Xy target )
{
  assert( target >= 0x11 && target <= 0x99);

  int r = 0;

  for (int i = 0; i < 24; i++) {
    Xy const xy = target + arr_round_to24[i];
    if (xy < 0x11 || xy > 0x99)
      continue;
    if (bo->board[xy] == WALL)
      continue;
    r += bo_attack_count(bo, side, xy);
  }

  return r;
}


/**
 * sideの自玉が行ける範囲を数え上げる.
 * 2手で行ける範囲
 */
int think_count_king_movable2(const BOARD* bo, int side)
{
  Xy const king_xy = bo->king_xy[ side ];
  assert( king_xy != 0 );

  int r[69];
  memset( r, 0, 69 * sizeof(int) );

  Xy xy;
  for (int i = 0; i < 8; i++) {
    for (int ii = 0; ii < bo->long_attack[1 - side][king_xy].count; ii++) {
      // 引くのはダメ
      if (norm_direc(bo->long_attack[1 - side][king_xy].from[ii], king_xy)
	  == arr_round_to[i]) {
	goto next;
      }
    }

    xy = king_xy + arr_round_to[i];
    if (bo->board[xy] == WALL)
      continue;
    if (bo->board[xy] && getTeban(bo->board[xy]) == side )
      continue;
    if (bo_attack_count(bo, 1 - side, xy))
      continue;

    for (int j = 0; j < 8; j++) {
      Xy xy2 = xy + arr_round_to[j];
      if (xy2 < 0x11 || xy > 0x99)
        continue;
      if (bo->board[xy2] == WALL)
        continue;
      if (bo->board[xy2] && getTeban(bo->board[xy2]) == side )
        continue;
      if (bo_attack_count(bo, 1 - side, xy2))
        continue;

      r[xy2 - king_xy + 34] = 1;
    }
  next: ;
  }

  r[34] = 0;
  return count(r, r + 69, 1);
}

