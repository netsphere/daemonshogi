/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifdef _WINDOWS
  #define WIN32_LEAN_AND_MEAN
  #define _WIN32_WINNT  0x0501
  #include <winsock2.h>
  #include <ws2tcpip.h>
#else  // UNIX
  #include <sys/socket.h>
  #include <netdb.h>
  #include <unistd.h>
  #include <errno.h>
  #define closesocket close
  #define WSAGetLastError() errno
  #define WSACleanup() 
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "si.h"
#include "board-misc.h"
using namespace std;


/** 
 * WinSockの初期化 
 * @return 成功したとき1 
 */
static int init_socket()
{
#ifdef _WINDOWS
  WSADATA wsaData;
  int r = ::WSAStartup(MAKEWORD(2, 2), &wsaData);
  if (r != 0) {
    printf("WSAStartup() failed: %d\n", r);
    exit(1);
  }
#endif
  return 1;
}


/** システムワイド */
static int socket_inited = 0;


/** 
 * サーバに接続する。
 * @param hostname IPv4 or IPv6 ホスト名
 * @param service  ポート番号の文字列
 * @return fd. 失敗の場合は-1
 */
int connect_to_server(const string& hostname, const string& service)
{
  struct addrinfo hints;
  struct addrinfo* res = NULL;
  struct addrinfo* ai;

  if (!socket_inited) {
    socket_inited = init_socket();
    if (!socket_inited)
      return 0;
  }

  // ホスト情報を得る
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;   // IPv4/IPv6両対応
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = NI_NUMERICSERV; // AI_NUMERICSERV // serviceはポート番号に限る

  int r = getaddrinfo(hostname.c_str(), service.c_str(), &hints, &res);
  if (r != 0) {
    printf("getaddrinfo() failed: %s\n", gai_strerror(r));
    return 0;
  }

  // 接続する
  int sockfd = -1;
  for (ai = res; ai; ai = ai->ai_next) {
    sockfd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
    if (sockfd < 0) {
      printf("Error at socket(): %d\n", WSAGetLastError());
      break; // 致命的エラー
    }
    if (::connect(sockfd, ai->ai_addr, ai->ai_addrlen) < 0) {
      closesocket(sockfd);
      sockfd = -1;
      continue; // 別のアドレスを試す
    }
    // ok
    break;
  }
  freeaddrinfo(res);

  // server_fd = sockfd;
  return sockfd;
}


void conn_close( int server_fd )
{
  closesocket(server_fd);
}


/** 0終端の文字列を送信 */
void conn_send_str(int server_fd, const char* line)
{
  printf("send: %s", line); // DEBUG

  if (server_fd >= 0) {
    int ret = write(server_fd, line, strlen(line));
    if (ret < 0) {
      printf("%s: socket write error.\n", __func__);
      abort();
    }
  }
}


/** 
 * 指し手を送信.
 * daemon_record_output_csa_te() を参考に。 
 */
int conn_send_move( int server_fd, const BOARD* board, const DTe& te )
{
  if (te.special == DTe::RESIGN) {
    conn_send_str(server_fd, "%TORYO\n");
    return 1;
  }

  // 通常の指し手
  char buf[100];
  if ( !te.fm ) {
    // 打ち
    sprintf(buf, "%c%d%d%d%d%s\n",
	    !board->next ? '+' : '-',
	    0, 0, 
	    te.to & 0xf, te.to >> 4,
	    CSA_PIECE_NAME[te.uti]);
  }
  else {
    // 移動
    PieceKind p = daemon_dboard_get_board(board, te.fm & 0xf, te.fm >> 4);
    if ( te.nari ) p += 8;

    sprintf(buf, "%c%d%d%d%d%s\n",
	    !board->next ? '+' : '-',
	    te.fm & 0xf, te.fm >> 4,
	    te.to & 0xf, te.to >> 4,
	    CSA_PIECE_NAME[p & 0xf]);
  }
  conn_send_str(server_fd, buf);

  return 1;
}


/**
 * ログイン要求を送信する.
 * @return   1 成功 
 */
int csa_send_login( int server_fd, 
		    const string& username, const string& password)
{
  string buf = "LOGIN " + username + " " + password + "\n";
  conn_send_str(server_fd, buf.c_str());
  return 1;
}


/**
 * ログアウト要求を送信する
 */
void csa_send_logout( int server_fd )
{
  conn_send_str(server_fd, "LOGOUT\n");

// TODO:  "LOGOUT:completed" を待つ
}


int csa_send_chudan_request( int server_fd )
{
  printf("%s: in\n", __func__); // DEBUG
  conn_send_str(server_fd, "%CHUDAN\n");
  return 1;
}
