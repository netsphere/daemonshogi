/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// 静的評価関数の学習

#include <config.h>
#if EVAL_VERSION == 3

#include <dirent.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <xmmintrin.h> // SSE
#include "record.h"
#include "si-think.h"
using namespace std;


/** 重み (学習用) */
static __m128 weight[HIDDEN_SIZE][XVEC_ROUND / 4];
static float hidden_weight[HIDDEN_SIZE + 1] __attribute__((aligned(16)));
#define K 0.05

struct Learning 
{
  int16_t xvec[XVEC_ROUND];
  float hidden[HIDDEN_SIZE + 1] __attribute__((aligned(16)));
  float out_value;

  /** 先手から見た評価 */
  float eval_f(const BOARD* bo)
  {
    int i, j;

    eval_set_xvec(bo, xvec + 1);
    xvec[0] = 1;
    for ( i = 0; i < (4 - ((XVEC_SIZE + 1) % 4)) % 4; i++ )
      xvec[XVEC_SIZE + 1 + i] = 0;

    // 入力層 -> 中間層
    for (j = 0; j < HIDDEN_SIZE; j++) {
      __m128 in_ = _mm_setzero_ps();
      for ( i = 0; i < XVEC_SIZE + 1; i += 4 ) {
	// xvec[i] -> R0
	__m128 x_ = _mm_setr_ps(xvec[i], xvec[i + 1], xvec[i + 2], xvec[i + 3]);
	__m128 a = _mm_mul_ps( weight[j][i / 4], x_ );
	in_ = _mm_add_ps(in_, a); // 効率悪い
      }
      float in[4] __attribute__((aligned(16)));
      _mm_store_ps( in, in_ );
      hidden[j + 1] = sigmoid(in[0] + in[1] + in[2] + in[3], 1.0);
    }
    hidden[0] = 1.0;

    // 中間層 -> 出力層
    __m128 out_ = _mm_setzero_ps();
    for ( i = 0; i < HIDDEN_SIZE + 1; i += 4 ) {
      __m128 a = _mm_mul_ps( _mm_load_ps(hidden_weight + i),
			     _mm_load_ps(hidden + i) );
      out_ = _mm_add_ps(out_, a);
    }
    float out[4] __attribute__((aligned(16)));
    _mm_store_ps( out, out_ );

    out_value = sigmoid(out[0] + out[1] + out[2] + out[3], 1.0);
    return out_value;
  }

  /** ニューラルネットワークの学習 */
  void back_propagation( float target ) const
  {
    // 出力層 -> 中間層
    float const p = (target - out_value) * out_value * (1 - out_value) * K;
    float e[HIDDEN_SIZE + 1] __attribute__((aligned(16))); // 補正値
    float old_hidden_weight[HIDDEN_SIZE + 1];
    int i, j;

    memcpy( old_hidden_weight, hidden_weight, sizeof(hidden_weight) );

    __m128 p_ = _mm_set1_ps(p);
    for ( i = 0; i < HIDDEN_SIZE + 1; i += 4 ) {
      __m128 a = _mm_mul_ps( p_, _mm_load_ps(hidden + i) );
      _mm_store_ps( e + i, a );

      __m128 hw = _mm_add_ps( _mm_load_ps(hidden_weight + i), a );
      _mm_store_ps( hidden_weight + i, hw );
    }

    // 中間層 -> 入力層
    for ( i = 1; i < HIDDEN_SIZE + 1; i++) {
      // あらかじめ K で乗じた値
      __m128 ek_ = _mm_set1_ps(
	       e[i] * old_hidden_weight[i] * hidden[i] * (1 - hidden[i]) );
      for ( j = 0; j < XVEC_SIZE + 1; j += 4 ) {
	// xvec[j] -> R0
	__m128 x_ = _mm_setr_ps( 
			    xvec[j], xvec[j + 1], xvec[j + 2], xvec[j + 3] );
	__m128 a = _mm_mul_ps( x_, ek_ );
	weight[i - 1][j / 4] = _mm_add_ps( weight[i - 1][j / 4], a );
      }
    }
  }
};


#define MARGIN 0.001

/** 手を指した後は指す前よりいいはず */
static void move_weight( BOARD* bo, const TE& best_move )
{
  Learning l;

  // 手を指す前の評価値
  float base_value = l.eval_f(bo);

  // 最善手の評価値
  boMove_mate( bo, best_move );
  float best_value = l.eval_f(bo);
  if (bo->next) {
    // 先手が指した
    if ( best_value <= base_value && base_value + MARGIN <= 1.0 ) {
      l.back_propagation( base_value + MARGIN );
/*
      // DEBUG
      if ( l.eval_f(bo) <= best_value )
        abort();
*/
      best_value = base_value + MARGIN;
    }
  }
  else {
    if ( best_value >= base_value && base_value - MARGIN >= 0.0 ) {
      l.back_propagation( base_value - MARGIN );
/*
      // DEBUG
      if ( l.eval_f(bo) >= best_value )
        abort();
*/
      best_value = base_value - MARGIN;
    }
  }
  boBack_mate( bo );

  // 最善手以外
  MOVEINFO mi;
  make_moves(bo, &mi);

  for (int i = 0; i < mi.count; i++) {
    if ( !te_is_valid(bo, mi.te[i]) || !teCmpTE(mi.te[i], best_move) )
      continue;

    boMove_mate(bo, mi.te[i]);
    float v = l.eval_f(bo);
    
    if (bo->next) {
      // 先手が指した
      if ( v >= best_value && best_value - MARGIN >= 0.0 )
	l.back_propagation( best_value - MARGIN );
    }
    else {
      if ( v <= best_value && best_value + MARGIN <= 1.0 )
	l.back_propagation( best_value + MARGIN );
    }
    boBack_mate(bo);
  }
}


static void update_weight( const BOARD* first_board, int te_cnt, 
			   const KyokumenNode* node )
{
  BOARD bo = BOARD( *first_board );
  Learning l;

  if ( te_cnt == 0 ) {
    // 初期局面は0.5
    l.eval_f(&bo);
    l.back_propagation( 0.5 );
  }

  for ( ; node && node->moves.size() > 0; te_cnt++ ) {
    // 主ラインのみ
    const MoveEdge& mv = node->moves[0];
    if ( mv.te.special ) {
      if ( mv.te.special == DTe::RESIGN ) {
	// 負けの局面
	l.eval_f(&bo);
	l.back_propagation( bo.next == 0 ? 0.0 : 1.0 );
      }
      break;
    }

    // 差分で学習
    move_weight( &bo, mv.te.pack() );

    node = node->moves[0].node;
    if ( node && node->moves.size() > 0 &&
	 node->moves[0].te.special == DTe::RESIGN ) {
      // 勝ち. TODO: 一手差をどうする？
      l.eval_f(&bo);
      l.back_propagation( bo.next == 0 ? 1.0 : 0.0 );
    }

    boMove_mate( &bo, mv.te.pack() );
  }
}


#define weight_filename "/tmp/02ai.txt"

/** 保存. アトミックに更新する */
static void weight_save()
{
  FILE* fp = fopen(weight_filename ".new", "w");
  assert(fp);

  int i, j;

  for (j = 0; j < HIDDEN_SIZE; j++) {
    for ( i = 0; i < XVEC_SIZE + 1; i += 4 ) {
      float w[4];
      _mm_store_ps(w, weight[j][i / 4]);
      for ( int k = 0; k < 4; k++ ) {
	if (i + k >= XVEC_SIZE + 1)
	  break;
	fprintf(fp, "%f\n", w[k]);
      }
    }
  }
  for (i = 0; i < HIDDEN_SIZE + 1; i++) 
    fprintf(fp, "%f\n", hidden_weight[i]);
  fclose(fp);

  ::rename(weight_filename ".new", weight_filename);
}


/**
 * 学習器
 */
struct Updater {
  void operator () (const Record* record) {
    update_weight( &record->first_board, 0, record->mi );
    // 連続して保存
    weight_save();
  }
};


static bool weight_load_f()
{
  FILE* fp = fopen(weight_filename, "r");
  if (!fp)
    return false;

  int i, j;

  for (j = 0; j < HIDDEN_SIZE; j++) {
    for ( i = 0; i < XVEC_SIZE + 1; i += 4 ) {
      float w[4] __attribute__((aligned(16)));
      for (int k = 0; k < 4; k++) {
        if ( i + k >= XVEC_SIZE + 1)
	  break;
	fscanf(fp, "%f\n", &w[k] );
      }
      weight[j][i / 4] = _mm_load_ps(w);
    }
  }
  for (i = 0; i < HIDDEN_SIZE + 1; i++) 
    fscanf(fp, "%f\n", &hidden_weight[i] );

  fclose(fp);
  return true;
}


struct JCalc {
  void operator () (const Record* record) {
  }
};


int main()
{
  LearnOneGame learner = LearnOneGame();

  // J値を計算
  traverse_all_files("/opt/src/shogi-book/tier2", JCalc() );

  return 0;
}

int main()
{
  if ( !weight_load_f() ) {
    // 乱数で初期化
    printf("weight file load error.\n");

    int i, j;
    for (j = 0; j < HIDDEN_SIZE; j++) {
      for ( i = 0; i < XVEC_SIZE + 1; i += 4 ) {
	float w[4];
	for (int k = 0; k < 4; k++) {
	  if (i + k >= XVEC_SIZE + 1)
	    break;
	  w[k] = (((float) rand()) / ((float) RAND_MAX) - 0.5) / 100.0;
	}
	weight[j][i / 4] = _mm_load_ps(w);
      }
    }

    for (i = 0; i < HIDDEN_SIZE + 1; i++)
      hidden_weight[i] = (((float) rand()) / ((float) RAND_MAX) - 0.5) / 100.0;
  }

  update_all( "/opt/src/shogi-book/tier2" );

  return 0;
}


#else
int main() {
  return 1;
}
#endif // EVAL_VERSION == 2
