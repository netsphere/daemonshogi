/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdlib.h>
#include <assert.h>
#include "si.h"


/**
 * xy に動ける next 手番の駒を探して MOVEINFO に te を追加する。
 * ただし王は除く。受け生成用。
 * @param bo 対象のBOARD
 * @param mi MOVEINFO
 * @param xy 場所
 * @param next 次の手番
 */
static void moveto2(const BOARD* bo, MOVEINFO* mi, Xy xy, int next)
{
  assert( !bo->board[xy] || getTeban(bo->board[xy]) != next );

  // TE te;
  int i, p;
  Xy fm;

  // 短いの
  for (i = 0; i < bo->short_attack[ next ][ xy ].count; i++) {
    fm = bo->short_attack[ next ][ xy ].from[i];

    p = bo->board[ fm ];
    if ( p == OH || p == (OH + 0x10) )
      continue;

    miAddWithNari( mi, *bo, fm, xy );
  }

  // 長いの
  for (i = 0; i < bo->long_attack[ next ][ xy ].count; i++) {
    fm = bo->long_attack[ next ][ xy ].from[i];

    miAddWithNari( mi, *bo, fm, xy );
  }
}


/**
 * 合い駒して王手を受ける。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @param king_xy 王の場所
 * @param rxy 王手している駒の場所
 * @param next 受け側の手番
 */
static int ukeAigoma( const BOARD* bo, MOVEINFO* mi, Xy const king_xy, 
		      Xy const rxy, 
                      int next ) 
{
  if ( bo->long_attack[1 - next][king_xy].count == 0 )
    return 0;

  int const count_before = mi->count;

  XyDiff vw = norm_direc(king_xy, rxy);
  Xy xy = king_xy;

  // 空白のところに駒を置いていく
  while (true) {
    xy += vw;
    PieceKind p = bo->board[ xy ];

#ifdef DEBUG
    assert( p != WALL );
#endif /* DEBUG */

    if ( p )
      break;

    // 移動して合い駒. 当然、玉を動かす手は除く。
    moveto2(bo, mi, xy, next);

    // 打つ
    mi_append_put_moves( bo, xy, mi );
  }

  return (mi->count > count_before) ? 1 : 0;
}


/**
 * 王手された状態で受けを探す。
 * @param bo 対象のBOARD
 * @param mi 結果を格納するMOVEINFO
 * @return heuristicsを考慮した手の数
 */
int uke( const BOARD* bo, MOVEINFO* mi )
{
  int num_of_ohte;
  int const next = bo->next;

  mi->count = 0;
  int ret = 0;

  // 自玉
  Xy const king_xy = bo->king_xy[bo->next];
  
  /*
   * 王手している駒の数を数える。
   * ２つならば王が動くしかない。
   * １つならば、
   * ・王が動く
   * ・王手している駒を取る
   * ・合い駒する
   * の３種類の受けがある。
   */ 
  num_of_ohte = bo_attack_count(bo, next == 0, king_xy);
  assert( num_of_ohte == 1 || num_of_ohte == 2 );

  if ( num_of_ohte == 1 ) {
    Xy xy;
    if (bo->short_attack[ next == 0 ][ king_xy ].count)
      xy = bo->short_attack[ next == 0 ][ king_xy ].from[0];
    else
      xy = bo->long_attack[ next == 0 ][ king_xy ].from[0];

    /* 王手している駒を取って受ける */
    moveto2(bo, mi, xy, next);

    /* 王が動いて受ける */
    mi_append_king_moves(bo, mi, king_xy, next);
    ret = mi->count;

    /* 合い駒して受ける */
    ret += ukeAigoma(bo, mi, king_xy, xy, next);
  }
  else {
    /* 両王手の場合 */
    /* 王が動いて受ける */
    mi_append_king_moves(bo, mi, king_xy, next);
    ret = mi->count;
  }

  return ret;
}


