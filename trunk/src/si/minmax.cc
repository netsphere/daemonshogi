/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <assert.h>
#include <signal.h> // sig_atomic_t
#include "si.h"
#include "si-think.h"
#include <algorithm>

#ifdef NDEBUG
  // リリースモード
  #define DEBUGLEVEL 0
#else
  #define DEBUGLEVEL 3
#endif // NDEBUG

#if DEBUGLEVEL >= 3
  #define __STDC_FORMAT_MACROS 1
  #include <inttypes.h>
#endif

using namespace std;


#ifdef USE_GUI
extern GAME g_game;
extern volatile sig_atomic_t gui_pending_counter;
#endif


/**
 * 置換表から探す
 * @return 見つかったときtrue
 */
static bool lookup_hash( BOARD* bo,
                         int const te_count, int rest_depth,
			 TE* best_move,
			 PhiDelta* score, uint16_t* node_flags )
{
  // ここが詰め将棋と共通化できない
  if ( hsGetBOARDMinMax(bo, score, best_move, node_flags) )
    return true;

  score->phi = +VAL_LEAF_UNKNOWN;
  score->delta = -think_eval( bo );
  *node_flags = 0;
  return false;
}


#if 0
/** 置換表に勝ちを登録 */
struct PutWin {
  // int te_count;
  PutWin(int te_count_) /*: te_count(te_count_)*/ { }

  void operator () (const BOARD* bo, const TE& move) const {
    hsPutBOARDMinMax(bo, +VAL_INF, -VAL_INF, 0, move);
  }
};
#endif // 0


/**
 * 手を指した後の局面が詰めろかどうか
 * @param node          手を指した後の局面
 * @param atkcnt_before 手を指す前の受け方玉への利き
 * @param level         1で詰めろのみ調べる、2で2手すきか詰めろを調べる
 * @return 0 => 詰めろなし, 1 => 詰めろ, 2 => 2手すき
 */
static int is_board_tsumero(BOARD* node, int atkcnt_before, int level)
{
  // 詰めろでありそうか, 軽く調べる
  bool promise = false;
  // (1) 取る手
  if ( node->tori[node->mseq.count - 1] )
    promise = true;

  int atkcnt2 = think_count_attack24(node, 1 - node->next, 
				     node->king_xy[node->next] );
  // (2) 利きを入れる手
  if ( atkcnt2 > atkcnt_before )
    promise = true;

  if (!promise)
    return false;

  bo_pass( node );
  int mate_node_limit = GAME_MATE_NODE_LIMIT;
  TE mate_te;
  if ( dfpn_mate(node, 31, &mate_te, &mate_node_limit) == CHECK_MATE ) {
    boBack_mate( node ); // パスを戻す
    return 1;
  }

  if (level > 1 ) {
    // より深く調べる
    MOVEINFO mi2;
    make_moves(node, &mi2);
    for (int i2 = 0; i2 < mi2.count; i2++) {
      boMove( node, mi2.te[i2] );
      if ( is_board_tsumero(node, atkcnt2, level - 1) ) {
	// ここでも置換表に登録しておく
	assert(level == 2); // TODO: もっと深く
	// hsPutBOARDMinMax( node, +VAL_LEAF_UNKNOWN, 
	// 		  -think_eval(node) + 2000 * 100, // magic. TODO: NPC
	// 		  NODE_TSUMERO, TE_NUL );

	boBack(node); // 自分の手
	boBack_mate(node); // 相手のパス
	return level;
      }

      boBack( node );

#ifdef USE_GUI
      // 中断や時間切れが入ってないか
      if ( gmGetGameStatus(&g_game) != 0 )
	break;
#endif // USE_GUI
    }
  }

  boBack_mate( node ); // パスを戻す
  return 0;
}


/** 
 * 手を指して、指した後の（仮の）評価値を child_score に格納する.
 * 悪手のときは指さない
 * @return 悪手のとき, 指さずに false を返す
 */
static bool do_move( const SearchParam& param,
                     int rest_depth,
                     BOARD* node,
		     const TsumeroNogare& tsumero_nogare,
                     TE* te,
		     PhiDelta* child_score )
{
  if ( (te_hint(*te) & TE_STUPID) != 0 )
    return false;

  bool is_checked = bo_is_checked(node, node->next);
  int atkcnt = think_count_attack24(node, node->next, 
				    node->king_xy[1 - node->next]);

  boMove( node, *te );

  // 千日手？ とりあえず1回目でもループは避ける
  if ( bo_repetition_count(node) >= 1 ) {
    boBack( node );
    return false;
  }

  uint16_t node_flags = 0;
  bool r = lookup_hash( node, param.te_count, rest_depth,
                        NULL,
			child_score, &node_flags );
  // 例えば、指した後の局面が詰めろなら、その手が詰めろを掛ける手
  te_add_hint( te, node_flags );

  if (r)
    return true;

  if (is_checked) {
    // 王手の受け
    return true; 
  }
  else if ( bo_is_checked(node, node->next) ) {
    // 王手
    return true;
  }

  if ( node->mseq.count >= 2 && 
       (te_hint(node->mseq.te[node->mseq.count - 2]) & TE_TSUMERO) != 0) {
    // 詰めろを掛けられていたとき
    if ( !tsumero_nogare.promise(node) ) {
      // 判定が軽いときは置換表に登録するまでもない
      // hsPutBOARDMinMax(node, +VAL_INF, -VAL_INF, param.te_count, TE_NUL );
      boBack(node);
      // printf("not (p) tsumero-nogare: %s\n", te_str(*te, *node).c_str() ); // DEBUG
      return false;
    }

    // 受けになっているか
    TE mate_te;
    int mate_node_limit = GAME_MATE_NODE_LIMIT;
    if ( dfpn_mate(node, 31, &mate_te, &mate_node_limit) == CHECK_MATE ) {
      // 受けになっていない. 負け
      // hsPutBOARDMinMax(node, +VAL_INF, -VAL_INF, 0, mate_te );
      // TODO: 下のコメントを復活させる. この関数の引数はイテレータにする
/*
      if ( te_is_put(*te) ) {
        // ほかの打つ手はどうか
        mate_set_muda_uchi( node->next, 1, PutWin(param.te_count),
                            node, mi, index + 1 );
      }
*/
      boBack(node);
      // printf("not tsumero-nogare: %s\n", te_str(*te, *node).c_str() ); // DEBUG
      return false;
    }
  }

  // こちらから詰めろ (含む詰めろ逃れの詰めろ) が掛かるか
  // ルートノードのときと、自分の直前の手が詰めろのときのみ調べる.
  if ( rest_depth == param.start_depth ) {
    int t = is_board_tsumero(node, atkcnt, 2);
    if (t == 1) {
      child_score->delta += 2000 * 100; // magic. TODO: NPCへ移動
      te_add_hint( te, TE_TSUMERO );
      // hsPutBOARDMinMax( node, child_score->phi, child_score->delta, 
      // 			NODE_TSUMERO, TE_NUL );
      return true;
    }
    else if ( t == 2 ) {
      child_score->delta += 1900 * 100; // magic. TODO: NPCへ移動
      te_add_hint( te, TE_2TESUKI );
      // hsPutBOARDMinMax( node, child_score->phi, child_score->delta, 
      // 			NODE_2TESUKI, TE_NUL );
      return true;
    }
  }
  else if ( rest_depth >= param.start_depth - 2 && node->mseq.count >= 3 && 
	    (te_hint(node->mseq.te[node->mseq.count - 3]) & 
                                 	 (TE_TSUMERO | TE_2TESUKI)) != 0 ) {
    if ( is_board_tsumero(node, atkcnt, 1 ) ) {
      child_score->delta += 2000 * 100; // magic. TODO: NPCへ移動
      te_add_hint( te, TE_TSUMERO );
      // hsPutBOARDMinMax( node, child_score->phi, child_score->delta, 
      // 			NODE_TSUMERO, TE_NUL );
      return true;
    }
  }

  return true; // 普通の手
}


#if DEBUGLEVEL >= 3
static string score_str_val(int32_t v) 
{
  if (v == +VAL_INF)
    return "INF";
  else if (v == +VAL_LEAF_UNKNOWN)
    return "UNK";
  else if (v == -VAL_LEAF_UNKNOWN)
    return "-UNK";
  else if (v == -VAL_INF)
    return "-INF";
  else
    return str_format("%d", v);
}

static string score_str(const PhiDelta& s)
{
  return "(" + score_str_val(s.phi) + "," + score_str_val(s.delta) + ")";
}
#endif // !NDEBUG


/**
 * 手を展開し、仮の評価値を得る
 * 子ノードのうち delta 最大 (子ノードにとって悪い手) を選ぶ
 * 勝ち = (+VAL_INF, -VAL_INF), 負け = (-VAL_INF, +VAL_INF)
 * 一通り展開するので、move ordering は効果なし
 */
void search_expand_trial_values( const SearchParam& param,
                                  BOARD* node,
                                  int rest_depth, int rest_node,
				  RankSet* rank )
{
  MOVEINFO mi;
  make_moves(node, &mi);

  if (!mi.count) {
    // 負け
    return;
  }

#if DEBUGLEVEL >= 3
  printf("depth %d: ", rest_depth);
#endif

  // 手を指す前の状態. 相手から詰めろを掛けられていたときのチェック用
  TsumeroNogare tsumero_nogare;
  tsumero_nogare.before_move( node, 1 - node->next );

  for (int i = 0; i < mi.count; i++) {
    MoveScore move_score;   // child nodeにとってよい局面 -> phi大, delta小

    if ( !do_move(param, rest_depth, node, tsumero_nogare,
		  &mi.te[i], &move_score.score) ) {
      continue;
    }

    // ソートしながら整理
    move_score.te = mi.te[i];
    rank->insert(move_score);

    boBack(node);

#if DEBUGLEVEL >= 3
    printf( "%s=%s, ", 
       te_str(mi.te[i], *node).c_str(), score_str(move_score.score).c_str() );
#endif

    // nodeの手番側の勝ち確定
    if ( move_score.score.phi <= -VAL_INF || 
                               move_score.score.delta >= +VAL_INF ) {
      break;  
    }
  }
#if DEBUGLEVEL >= 3
  printf("\n");
//   printf("best = %s\n", te_str(*may_best, *node).c_str());
#endif
}


static int32_t new_delta(int32_t node_delta, int32_t child_phi) 
{
  if ( child_phi != +VAL_LEAF_UNKNOWN )
    return child_phi;
  else
    return node_delta;
}


/**
 * 探索し、現局面き評価値を更新する。
 * 必ず一度はノードを展開する
 */
static void bfs_search( const SearchParam& param,
			BOARD* node, 
                        const PhiDelta& threshold,
                        int depth, int* node_limit, 
			PhiDelta* node_score, TE* best_move )
{
#ifndef NDEBUG
  uint64_t const node_hash = node->hash_all;
#endif // !NDEBUG

#if DEBUGLEVEL >= 3
  printf("%d: enter: threshold=(%d, %d), %d\n",
         depth, threshold.phi, threshold.delta, *node_limit);
#endif

  TE prev_move;
  PhiDelta prev_threshold = make_phi_delta(0, 0);
  te_clear(best_move);
  te_clear(&prev_move);

#ifdef USE_GUI
  if ( gui_pending_counter ) {
    processing_pending( false );
    gui_pending_counter = 0;
  }
#endif /* USE_GUI */

  RankSet rank;
  RankSet::const_iterator it;

  // 次の場合に、詰みを読む。
  // (1) ルートノードか、頓死チェック
  // (2) 詰めろを掛けられていた. 
  // (3) 詰めろを掛けた次の手
  if ( param.start_depth - depth <= 1 ||
       (node->mseq.count >= 1 && 
        (te_hint(node->mseq.te[node->mseq.count - 1]) & TE_TSUMERO) != 0) ||
       (node->mseq.count >= 2 && 
	(te_hint(node->mseq.te[node->mseq.count - 2]) & TE_TSUMERO) != 0) ) {
    int mate_node_limit = GAME_MATE_NODE_LIMIT;
    if ( dfpn_mate(node, 31, best_move, &mate_node_limit) == CHECK_MATE ) {
      node_score->phi = +VAL_INF; node_score->delta = -VAL_INF;
      goto exit_search;
    }
  }

  // サイクル回避
  bo_inc_repetition(node);

  // 1回目はノードを展開して (仮の) 最善手を得る.
  search_expand_trial_values( param, node, /* mi,*/
			depth, *node_limit,
			&rank );
  assert( node_hash == node->hash_all );

  while (true) {
    // 手がない
    if ( rank.size() == 0 ) {
      te_clear(best_move);
      break;
    }

    // 現在の最善手
    it = rank.begin();
    *best_move = it->te;
#if DEBUGLEVEL >= 3
    printf("best = %s\n", te_str(it->te, *node).c_str());
#endif

#ifdef USE_GUI
    /* 中断や時間切れが入ってないか調べる */
    if ( gmGetGameStatus(&g_game) != 0 )
      break;
#endif // USE_GUI

    // 探索ノードを使い果たした
    if ( depth <= 1 || *node_limit < 0 )
      break;

    // 閾値を超えた？
    if ( it->score.delta <= threshold.phi || it->score.phi <= threshold.delta )
      break;

    // ノードを展開する
    PhiDelta threshold_child;

    // このしきい値を超える (下回る) -> このノードに戻ったときにdelta cutが起こる
    threshold_child.phi = threshold.delta;

    // このしきい値を超える (下回る) -> 2位の方が有望
    if (rank.size() == 1)
      threshold_child.delta = threshold.phi;
    else {
      RankSet::const_iterator it2 = it;
      ++it2;
      threshold_child.delta = max( threshold.phi, it2->score.delta - 1 );
    }

    // 先端 ?
    if ( !teCmpTE(prev_move, it->te) && prev_threshold == threshold_child ) {
#if 0
      printf( "%d: loop!! threshold=(%d, %d), score=(%d,%d)\n",
	      depth, threshold.phi, threshold.delta, 
	      best_child.delta, child_phi_min ); // DEBUG
#endif // 0
      break;
    }

    MoveScore new_move_score;
    new_move_score.score = it->score;
    TE te_dmy;

    // TODO: ここを並列化できる
    boMove(node, it->te );
    bfs_search( param, node, threshold_child, depth - 1, 
                node_limit, 
		&new_move_score.score, &te_dmy );
    boBack(node);

    prev_move = it->te;
    prev_threshold = threshold_child;

    // 読んだ手（とその得点）を差し替える
    new_move_score.te = it->te;
    rank.erase(it);
    rank.insert(new_move_score);
  }

  assert( node_hash == node->hash_all );

  if (rank.size() == 0) {
    node_score->phi = -VAL_INF; node_score->delta = +VAL_INF;
  }
  else {
    node_score->phi = it->score.delta;
    node_score->delta = new_delta(node_score->delta, it->score.phi);
  }

  // タブーリストから除く
  bo_dec_repetition(node);

exit_search:
  hsPutBOARDMinMax( node, node_score->phi, node_score->delta, 
      node->mseq.count >= 1 ? te_hint(node->mseq.te[node->mseq.count - 1]) : 0,
      *best_move );
#if DEBUGLEVEL >= 3
    printf("%d: put %" PRIx64 "-> %s %s\n",
	   depth, node->key, 
	   te_str(*best_move, *node).c_str(), score_str(*node_score).c_str() );
#endif
  (*node_limit)--;
}


/**
 * 通常探索をおこなう
 * @param bo 現在の局面
 * @param depth 探索する最大の深さ
 * @param best_move 最善手を返す. 手がない（投了）のときはクリアする
 *
 * @return SI_NORMAL
 */
INPUTSTATUS bfs_minmax( BOARD* bo, int depth, PhiDelta* score, TE* best_move )
{
  assert(score);
  assert(best_move);

  int r;
  // PhiDelta score;
  uint16_t node_flags = 0;

  // 結論が出ているか？
  r = hsGetBOARDMinMax( bo, score, best_move, &node_flags );
  te_add_hint( best_move, node_flags );
  if (r) {
    // 勝ち・負けの場合のみ利用
    if ( score->phi >= +VAL_INF || score->delta <= -VAL_INF ) {
      assert( te_to(*best_move) >= 0x11 && te_to(*best_move) <= 0x99);
      return SI_NORMAL;
    }
    else if ( score->phi <= -VAL_INF || score->delta >= +VAL_INF ) {
      te_clear(best_move);
      return SI_NORMAL;
    }
  }

  bool checked = bo_is_checked(bo, bo->next);
#ifndef NDEBUG
  if (checked && bo->mseq.count >= 1) {
    assert( (te_hint(bo->mseq.te[bo->mseq.count - 1]) & TE_CHECK) != 0 );
  }
#endif

  // 詰めろを掛けられているか？ ... 不明な場合のみ調べる
  if ( !checked && 
       !(node_flags & NODE_TSUMERO) && bo->mseq.count >= 1 &&
       !(te_hint(bo->mseq.te[bo->mseq.count - 1]) & TE_TSUMERO) ) {
    bo_pass(bo);
    int node_limit = GAME_MATE_NODE_LIMIT;
    TE te_ret;
    if ( dfpn_mate(bo, 31, &te_ret, &node_limit) == CHECK_MATE ) {
      // 詰めろ
      te_add_hint(&bo->mseq.te[bo->mseq.count - 2], TE_TSUMERO);
    }
    boBack_mate(bo);
  }
  // 中断, 時間切れ?
  if ( gmGetGameStatus(&g_game) != 0 )
    return gmGetGameStatus(&g_game);

  SearchParam param;
  param.te_count = bo->mseq.count;
  param.start_depth = depth;
  int node_limit = 25000; // magic
  *score = make_phi_delta(+VAL_LEAF_UNKNOWN, +VAL_LEAF_UNKNOWN);
  bfs_search( param, bo, 
	      make_phi_delta(-VAL_INF, -VAL_INF),
              depth, &node_limit, score, best_move );

  // 中断または時間切れ
  if ( gmGetGameStatus(&g_game) != 0 )
    return gmGetGameStatus(&g_game);

#if 0
  r = hsGetBOARDMinMax( bo, score, best_move, 0 );
  if (!r) {
    si_abort("internal error: not found result\n");
  }
#endif // 0

  if ( score->phi <= -VAL_INF || score->delta >= +VAL_INF ) {
    te_clear(best_move);
    return SI_NORMAL;
  }

  assert( te_to(*best_move) >= 0x11 && te_to(*best_move) <= 0x99);
  return SI_NORMAL;
}
