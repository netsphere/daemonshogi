/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// #define GTK_DISABLE_DEPRECATED 1
// #define GDK_DISABLE_DEPRECATED 1
// #define G_DISABLE_DEPRECATED 1

#include <config.h>
#include <gtk/gtk.h>
#include <assert.h>
G_BEGIN_DECLS
#include "interface.h"
#include "support.h"
G_END_DECLS
#include "si/record.h"
#include "history_window.h"
#include "canvas.h"
#include <algorithm>
using namespace std;


static GtkTreeView* get_history_list(GtkWidget* history_window)
{
  if (!history_window)
    return NULL;

  GtkWidget* list;
  list = lookup_widget(history_window, "booklist");
  g_assert(list);

  return GTK_TREE_VIEW(list);
}


/** 棋譜リストの列を整える */
static void add_columns(GtkTreeView* treeview)
{
  GtkCellRenderer* renderer;
  GtkTreeViewColumn* column;

  // column 1
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes("",
						    renderer,
						    "text", 0, // 列
						    NULL);
  gtk_tree_view_append_column(treeview, column);

  // column 2
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(_("Move"), 
						    renderer, 
						    "text", 1, 
						    NULL);
  gtk_tree_view_append_column(treeview, column);

  // column 3
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(_("Elapsed"), 
						    renderer, 
						    "text", 2, 
						    NULL);
  gtk_tree_view_append_column(treeview, column);
}


/** 棋譜ウィンドウを生成し、GUIコントロールを整える */
static GtkWidget* history_window_new()
{
  GtkWidget* window = create_bookwindow();
  g_assert(window);

  GtkTreeView* history_list = get_history_list(window);
  GtkListStore* model;

  // 手数, 手, 経過時間
  model = gtk_list_store_new(3, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
  gtk_tree_view_set_model(history_list, GTK_TREE_MODEL(model));

  add_columns(GTK_TREE_VIEW(history_list));

  return window;
}


static void clear_list(GtkWidget* window)
{
  if (!window)
    return;

  GtkTreeView* history_list = get_history_list(window);
  GtkListStore* model = GTK_LIST_STORE(gtk_tree_view_get_model(history_list));
  g_assert(model);

  gtk_list_store_clear(model);

  // 初期局面の行はいつでも表示
  const char* game_start = _("game start");

  GtkTreeIter iter;
  gtk_list_store_append(model, &iter);
  gtk_list_store_set(model, &iter, 
                     1, game_start,
                     -1);
}


/** 棋譜リストをクリア */
void History::clear()
{
  // printf("%s: called.\n", __func__); // DEBUG
  
  focus_line.clear();
  cur_pos = 0;

  clear_list(window);
}


const char* ANNOT_STR[] = {
    "",
    _("poor"),         // "?"  疑問手 = 悪い手. 奥ゆかしい
    _("questionable"), // "?!" 疑わしい手
    NULL
};


/** 変化手順ダイアログを更新 */
void alt_moves_dialog_update() 
{
  if (!g_canvas->move_property_dialog)
    return;
  if (!g_canvas->record)
    return;

  // グラフを辿って、局面ノードを得る
  KyokumenNode* node = g_canvas->record->mi;
  int te_count;
  for (te_count = 0; te_count < g_canvas->history->cur_pos; te_count++)
    node = node->lookup_child(g_canvas->history->focus_line[te_count].first);

  // 次の一手を表示
  // TODO: ラインで表示
  GtkTreeView* next_moves = GTK_TREE_VIEW(
	      lookup_widget(g_canvas->move_property_dialog, "next_moves"));
  assert(next_moves);
  GtkListStore* model = GTK_LIST_STORE(gtk_tree_view_get_model(next_moves));
  gtk_list_store_clear(model);

  GtkTreeIter iter;
  int nth = -1;
  for (int i = 0; i < node->moves.size(); i++) {

    gtk_list_store_append(model, &iter);
    char buf[100];
    create_te_string(&g_canvas->board, te_count + 1, &node->moves[i].te, buf);
    gtk_list_store_set(model, &iter,
		       0, buf,
                       1, ANNOT_STR[ node->moves[i].annotation ],
                       2, node->moves[i].te_comment.c_str(),
		       -1);
    if (node->moves[i].te == g_canvas->history->focus_line[te_count].first)
      nth = i;
  }

  // ハイライト
  // printf("%s: %d\n", __func__, nth); // DEBUG
  if (nth >= 0) {
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(model), &iter, NULL, nth);
    GtkTreeSelection* selection = gtk_tree_view_get_selection(next_moves);
    gtk_tree_selection_select_iter(selection, &iter);
  }
}


void alt_moves_dialog_hide()
{
  if (g_canvas->move_property_dialog) {
    alt_moves_dialog_update();

    gtk_widget_hide(g_canvas->move_property_dialog);
    GtkWidget* menuitem = lookup_widget(g_canvas->window, "move_property");
    assert(menuitem);
    gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM(menuitem), FALSE );
  }
}


void History::highlight_cur_pos()
{
  if (!window)
    return;

  GtkTreeView* history_list = get_history_list(window);
  GtkTreeModel* model = gtk_tree_view_get_model(history_list);

  GtkTreeIter iter;
  gtk_tree_model_iter_nth_child(model, &iter, NULL, cur_pos);

  GtkTreeSelection* selection = gtk_tree_view_get_selection(history_list);
  gtk_tree_selection_select_iter(selection, &iter);

  // 行をウィンドウ内に表示
  GtkTreePath* path = gtk_tree_path_new_from_indices(cur_pos, -1);
  gtk_tree_view_scroll_to_cell(history_list, path, NULL, 
			       FALSE, 0.0, 0.0);
  gtk_tree_path_free(path);
}


/** 棋譜リストの指定の行をハイライトする */
void History::select_nth(int nth)
{
  // printf("%s: nth = %d\n", __func__, nth); // DEBUG
  
  if (nth < 0) {
    printf("illegal nth??\n"); // DEBUG
    return;
  }

  cur_pos = nth;
  highlight_cur_pos();

  alt_moves_dialog_update();
}


/** 棋譜リストに1行追加 */
static void append_move_to_list( GtkWidget* window,
                                 int lineno,
                                 const DBoard& bo, 
                                 const DTe& te, 
                                 int elapsed_sec )
{
  if (!window)
    return;

  GtkTreeView* history_list = get_history_list(window);
  GtkListStore* model = GTK_LIST_STORE(gtk_tree_view_get_model(history_list));
  assert(model);

  char ln_buf[100];
  sprintf(ln_buf, "%d", lineno);

  char te_buf[100];
  create_te_string(&bo, lineno, &te, te_buf);

  char tbuf[100];
  sprintf(tbuf, "%d:%02d", elapsed_sec / 60, elapsed_sec % 60);

  GtkTreeIter iter;
  gtk_list_store_append(model, &iter);
  if ( te.special == DTe::NORMAL_MOVE || te.special == DTe::SENNICHITE ) {
    gtk_list_store_set(model, &iter, 
		       0, ln_buf,
		       1, te_buf, 
		       2, tbuf,
		       -1);
  }
  else {
    gtk_list_store_set(model, &iter,
		       1, te_buf,
		       -1);
  }
}


/**
 * 棋譜リストの末尾に指し手を追加.
 * @param bo   局面
 * @param te   指し手
 * @param elapsed_sec 消費時間
 */
void History::append_move(const DBoard& bo, const DTe& te, int elapsed_sec)
{
  focus_line.push_back( pair<DTe, int>(te, elapsed_sec) );
  append_move_to_list(window, focus_line.size(), bo, te, elapsed_sec);
}


/** 末尾を削除 */
void History::delete_last()
{
  focus_line.pop_back();
  cur_pos = min(cur_pos, (int) focus_line.size());

  if (!window)
    return;

  GtkTreeModel* model = NULL;
  GtkTreeIter iter;
  gboolean r;

  GtkTreeView* history_list = get_history_list(window);
  g_assert(history_list);

  model = gtk_tree_view_get_model(history_list);
  r = gtk_tree_model_iter_nth_child( model, &iter, NULL, focus_line.size() + 1);
  g_assert(r);

  gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
}


/**
 * 棋譜モードで、指定の手に移動
 * @param treeview 棋譜リスト
 * @param nth   0のとき初期局面
 */
void daemon_canvas_bookwindow_click(GtkTreeView* treeview,
				    int nth)
{
  assert(g_canvas);
  assert(nth >= 0);
  // printf("count = %d, nth = %d\n", g_canvas->history->cur_pos, nth); // DEBUG

  if (g_canvas->mode != D_CANVAS_MODE_BOOK)
    return;

  if (g_canvas->history->cur_pos < 0) { // DEBUG
    printf("%s: warning: cur_pos illegal??\n", __func__);
    return;
  }

  if (g_canvas->history->cur_pos == nth)
    return;

  if (g_canvas->history->cur_pos > nth) {
    // 遡る
    while (g_canvas->history->cur_pos > nth) {
      --g_canvas->history->cur_pos;
      boBack( &g_canvas->board );
    }
  }
  else {
    // 進める
    while (g_canvas->history->cur_pos < nth) {
      DTe te = g_canvas->history->focus_line[g_canvas->history->cur_pos].first;
      if ( te.special != DTe::NORMAL_MOVE && te.special != DTe::SENNICHITE )
	break;

      boMove_mate( &g_canvas->board, te.pack() );
      g_canvas->history->cur_pos++;
    }
  }

  daemon_canvas_set_kif_sensitive(g_canvas);
  alt_moves_dialog_update();

  // 再描画
  canvas_redraw(g_canvas);
}


/** 棋譜リストを表示し直す */
void History::refresh_list()
{
  if (!window)
    return;

  clear_list(window);

  int i;
  BOARD* board = bo_dup( &canvas->record->first_board );
  printBOARD( board ); // DEBUG

  for (i = 0; i < focus_line.size(); i++) {
    DTe te = focus_line[i].first;
    append_move_to_list(window, 
			i + 1,
			*board, 
			te,
			focus_line[i].second);

    // printf("%s: %d\n", te_to_str(&te, board).c_str(), 
    //                   focus_line[i].second); // DEBUG

    if ( te.special == DTe::NORMAL_MOVE )
      boMove_mate( board, te.pack() );
  }

  freeBOARD( board );
}


/** @param n n手目以降を削除する. 0 のときは (初期局面を除き) すべて削除 */
void History::delete_move( int n )
{
  focus_line.erase( focus_line.begin() + n, focus_line.end() );
  cur_pos = min( cur_pos, (int) focus_line.size() );

  refresh_list();
}


/** 
 * 棋譜リストに棋譜を表示
 * @param record ゲームデータ
 * @param n  n手を残す. 0のときは (初期局面を除き) すべて置き換え
 * @param te n手の後で選ぶ指し手
 */
void History::sync(const Record& record, int n, const DTe& te)
{
  int i;

  focus_line.erase( focus_line.begin() + n, focus_line.end() );
  cur_pos = min( cur_pos, (int) focus_line.size() );
  
  const KyokumenNode* p = record.mi;
  for (i = 0; i < focus_line.size(); i++) {
    DTe te;
    int elapsed;

    pair<DTe, int> x = focus_line[i];
    te = x.first;
    elapsed = x.second;

    p = p->lookup_child(te);
  }

  if (p->moves.size() > 0) {
    KyokumenNode::Moves::const_iterator it;
    for (it = p->moves.begin(); it != p->moves.end(); it++) {
      if (it->te == te) {
	focus_line.push_back( pair<DTe, int>(te, it->time) );
	p = p->lookup_child(te);
	break;
      }
    }
    assert( it != p->moves.end() );
  }

  while (p && p->moves.size() > 0) {
    KyokumenNode::Moves::const_iterator m = p->moves.begin();
    focus_line.push_back( pair<DTe, int>(m->te, m->time) );
    p = m->node;
  }

  refresh_list();
}


/** 棋譜ウィンドウを表示する */
void History::show_window()
{
  if (!window) {
    window = history_window_new();

    if (canvas->record != NULL) {
      // sync(*canvas->record, 0, canvas->record->mi->moves.begin()->te);
      refresh_list();
      select_nth(cur_pos);
    }
  }
  gtk_widget_show(window);
  history_window_menuitem_set_active(canvas, true);
}


void History::hide_window()
{
  if (!window)
    return;

  gtk_widget_hide(window);
}
