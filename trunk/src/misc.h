/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _MISC_H_
#define _MISC_H_

#include <gtk/gtk.h>
#include <string>

/** ダブルクリックと判定するまでの時間(ミリ秒) */
#define DOUBLECLICKTIMEOUT 400


cairo_surface_t* load_pixmap_from_imlib_image_scaled( cairo_format_t format,
             const GdkPixbuf* im, int width, int height );

// void set_color_rgb(cairo_t* cr, guint16 r, guint16 g, guint16 b);

gboolean check_timeval(const struct timeval* tv, const struct timeval* now);

gboolean is_on_rectangle( const GdkRectangle* a, const GdkRectangle* b);

// void pending_loop( bool may_block );

gboolean daemon_on_rectangle(GdkRectangle *rect, GdkPoint *point);

struct Canvas;
void daemon_messagebox( Canvas* canvas, const char* s,
				                    GtkMessageType mes_type );

#endif /* _MISC_H_ */



