/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DAE_CUI_MASTER_H
#define DAE_CUI_MASTER_H 1

#include <si/si.h>
#include <poll.h>  // poll()
#include <errno.h>
#include <string.h>
#include <list>
#include <string>


////////////////////////////////////////////////////////////////////////

struct GlobalSetting 
{
  enum Mode {
    GUI_ENGINE_MODE,
    HUMAN_CUI_MODE
  };
  Mode mode;

  int display_front;
};

typedef std::list<std::string> CliTeList;

extern GlobalSetting global;
extern RecvBuffer* recv_buffer;
extern BOARD* g_board;
// extern CliTeList cli_te_list;

void* worker_wait_for_user_input(void* arg);
void print_prompt();
void putHelp();
void shutdown_io_watch();


/** 
 * 入力があればコールバック関数に渡す.
 * 呼び出し側でどれかにPOLLHUPが立っている (切断) か確認すること
 * @return 0 = タイムアウト
 *         1 = コールバック関数を1回以上呼び出した, またはどれか切断された.
 */
template <typename Fn>
int dispatch_input( struct pollfd* poll_list, int num, int milliseconds, 
		    Fn& fn )
{
  int i;

 retry:
  int r = ::poll( poll_list, num, milliseconds );
  if (r < 0) {
    if (errno == EINTR)
      goto retry;
    else {
      si_abort("poll() failed: %s\n", strerror(errno));
    }
  }

  if (r == 0) // タイムアウト
    return 0;
  else {
    for (i = 0; i < num; i++) {
      if ( (poll_list[i].revents & POLLHUP) != 0 )
	continue;

      if ( (poll_list[i].revents & POLLIN) != 0 )
	fn(poll_list[i].fd, i);
    }
    return 1; // すぐ戻る
  }
}


#endif
