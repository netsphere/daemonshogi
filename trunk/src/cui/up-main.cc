/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// 単一プロセッサで計算. ponderもしない


#define _XOPEN_SOURCE 700 // SUSv4
#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string>
#include "si/si.h"
using namespace std;


static void usage()
{
  printf("\n");
  printf("USAGE:\n");
  printf("  $ daemonshogi-up -c pattern-file\n");
}


/** ゲーム開始を待つ */
static void wait_for_start()
{
  int buflen = 1000;
  char buf[buflen];

  while ( fgets(buf, buflen, stdin) ) {

  }
}


int main(int argc, char* argv[])
{
  string think_config_file;

  int ch;
  while ( (ch = getopt(argc, argv, "c:")) != -1 ) {
    switch (ch) {
    case 'c':
      think_config_file = optarg;
      break;
    default:
      usage();
      exit(1);
    }
  }

  if ( !think_init(think_config_file) ) {
    printf( "error: ai-config file '%s' not found.\n",
	    think_config_file.c_str() );
    usage();
    exit(1);
  }

  // 定跡も必須
  if ( !book_init(PACKAGE_DATA_DIR "/" PACKAGE "/daemonshogi-book.bin", 
		  false) ) {
    printf("error: book file '%s' not found.\n", 
	               PACKAGE_DATA_DIR "/" PACKAGE "/daemonshogi-book.bin");
    exit(1);
  }

  wait_for_start();

  return 0;
}
