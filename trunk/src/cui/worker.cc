/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// マスタノードのワーカスレッド

#define _XOPEN_SOURCE 700 // SUSv4
#define MPICH_SKIP_MPICXX 1
#include <config.h>

#include <stdio.h>
#include "mini-mpi.h"
#include "dae-cui.h"
#include "master.h"
using namespace std;


struct WorkerState 
{
  int state;
  bool queue_added;
  bool operator () (int fd, int idx_);

private:
  bool cli_state_2();
  void gui_state_12();
};


static CliTeList w_cli_te_list;

/** 
 * 指し手、投了、待ったを受け付ける 
 * 先読みがあるので、ここでは w_cli_te_list に格納するだけ.
 * 
 * 指し手::
 *     xyxypi     移動前の位置、移動後の位置、移動後の駒名
 *                ex) 3324NG ... 2四銀成
 *     00xypi     駒を打つ場合
 *     toryo      投了
 *     matta      待った
 *
 * @return 手を受け付けたとき true
 */
bool WorkerState::cli_state_2()
{
  string cmd = recv_buffer->get_front();

  if ( cmd == "quit" ) {
    gmSetGameStatus(&g_game, SI_CHUDAN_LOCAL);
    return true;
  }
  else if ( cmd == "help" ) {
    putHelp();
    print_prompt();
    return false;
  }
  else if ( cmd == "flip" ) {
    global.display_front = 1 - global.display_front;
    printBOARD( g_board, global.display_front );
    print_prompt();
    return false;
  }
  else if ( cmd == "matta" ) {
    // 先読みの可能性がある. ここでは戻さない
    // gmSetGameStatus(&g_game, SI_MATTA);     いつでもにするのは大変か
    w_cli_te_list.push_back( cmd ); // DTe(DTe::MATTA, TE_NUL, 0) );
    queue_added = true;
    return true;
  }
  else if ( cmd == "toryo" ) {
    w_cli_te_list.push_back( cmd ) ; // DTe(DTe::RESIGN, TE_NUL, 0) );
    queue_added = true;
    return true;
  }
  else if ( isdigit(cmd[0]) && isdigit(cmd[1]) &&
	    cmd[2] >= '1' && cmd[2] <= '9' && cmd[3] >= '1' && cmd[3] <= '9' ) {
    // 通常の指し手
    if ( (cmd[0] == '0' && cmd[1] != '0') || 
	 (cmd[0] != '0' && cmd[1] == '0') ) {
      printf("xy wrong.\n");
      print_prompt();
      return false;
    }
      
    // ここでは構文チェックだけ
    char koma_str[3];
    PieceKind p;
    sscanf(cmd.c_str() + 4, "%2s", koma_str );
    for ( p = 1; p < 0xf; p++ ) {
      if ( !strcmp(koma_str, CSA_PIECE_NAME[p]) )
	break;
    }
    if (p > 0xf) {
      printf("wrong piece name.\n");
      print_prompt();
      return false;
    }

    w_cli_te_list.push_back(cmd);
    queue_added = true;
    return true;
  }

  printf("unknown command.\n");
  print_prompt();
  return false;
}


/**
 * GUI部からの指し手情報.
 * 先読みがあるので、ここではリストに格納するだけ
 */
void WorkerState::gui_state_12()
{
  string cmd = recv_buffer->get_front();
  w_cli_te_list.push_back(cmd);
  queue_added = true;
}


bool WorkerState::operator () (int fd, int idx_)
{ 
  if ( !recv_buffer->read() ) {
    shutdown_io_watch();
    return false;
  }

  while ( recv_buffer->recv_list.size() > 0 ) {
    switch (state) {
    case 2:
      // ゲーム中
      if ( cli_state_2() )
	return false; // ユーザから手の入力があった. ループを抜ける
      break;

    case 12:
      // GUI部と通信 (ゲーム中)
      gui_state_12();
      break;

    default:
      abort();
    }
  }

  return true;
}


/** ワーカースレッド: ユーザ/GUI部からの入力を待つ */
void* worker_wait_for_user_input(void* arg)
{
  struct pollfd poll_list[1];
  poll_list[0].fd = STDIN_FILENO;
  poll_list[0].events = POLLIN | POLLPRI;

  WorkerState s;
  s.state = global.mode == GlobalSetting::HUMAN_CUI_MODE ? 2 : 12;

  while ( true ) {
    s.queue_added = false;
    dispatch_input( poll_list, 1, -1, s );

    if ( (poll_list[0].revents & POLLHUP) != 0 ) {
      // 切断
      int32_t i = -1; 
      mMPI_CHECK_RESULT( MPI_Send(&i, 1, MPI_INT32_T, MASTER_NODE, 
				  TAG_GUI_INPUT_CLOSE, MPI_COMM_WORLD) );
      break;
    }

    if ( s.queue_added ) {
      // 主スレッドにシグナル送信
      while (w_cli_te_list.size() > 0) {
	send_obj(MASTER_NODE, TAG_GUI_INPUT, w_cli_te_list.front());
	w_cli_te_list.pop_front();
      }
    }
  }

  return NULL;
}
