/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// マスタノード


#define _XOPEN_SOURCE 700 // SUSv4
#define MPICH_SKIP_MPICXX 1
#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <list>
#include <vector>
#include "mini-mpi.h"
#include <pthread.h>
#include <poll.h>
#include "dae-cui.h"
#include "master.h"
#include <si/si-think.h>
#include <si/record.h>
using namespace std;


/** ヘルプ */
void putHelp() 
{
  printf("# COMMAND\n"
	 "  quit             Exit this program.\n"
	 "  newgame (ai|human) (ai|human)\n"
	 "                   Start new game.\n"
	 "  flip             Flip board.\n"
	 "  load filename    Load CSA board.\n"
	 "  help             Print this message.\n"
         "\n"
         "# MOVE (in game mode)\n"
	 "  7776FU     move on board.\n"
         "             To promote, set piece name Nari-koma.\n"
         "  0055KA     put piece.\n"
	 "\n"
	 "  - piece name\n"
         "        FU (Fu)          TO (Tokin)\n"
	 "        KY (Kyosha)      NY (Nari-kyo)\n"
	 "        KE (Keima)       NK (Nari-kei)\n"
	 "        GI (Gin)         NG (Nari-gin)\n"
	 "        KI (Kin)\n"
	 "        KA (Kaku)        UM (Uma)\n"
	 "        HI (Hisha)       RY (Ryu)\n"
	 "        OU (Gyoku)\n"
	 "  toryo      resign\n"
	 "  matta      retract\n" );
}


/** 待っているノード番号 */
static list<int> wait_queue;

/** 探索中のノード番号 */
static set<int> active_queue;


/** 
 * コンピュータの手番
 * bfs_minmax() の変形バージョン
 */
static INPUTSTATUS do_think( int comm_size, 
			     const BOARD* bo_, int depth, DTe* best_move )
{
  BOARD bo = BOARD(*bo_);

  TE te_ret;
  PhiDelta score;

  if ( jouseki(&bo, &te_ret) ) {
    *best_move = DTe( DTe::NORMAL_MOVE, te_ret, bo.board[te_to(te_ret)] );
    return SI_NORMAL;
  }

  // 結論が出ているか
  int r = hsGetBOARDMinMax(&bo, &score, &te_ret, 0);
  if (r) {
    // 勝ち・負けの場合のみ利用
    if ( score.phi >= +VAL_INF || score.delta <= -VAL_INF ) {
      assert( te_to(te_ret) >= 0x11 && te_to(te_ret) <= 0x99);
      *best_move = DTe( DTe::NORMAL_MOVE, te_ret, bo.board[te_to(te_ret)] );
      return SI_NORMAL;
    }
    else if ( score.phi <= -VAL_INF || score.delta >= +VAL_INF ) {
      *best_move = DTe(DTe::RESIGN, TE_NUL, 0);
      return SI_NORMAL;
    }
  }

  // 分散探索
  Command cmd;
  vector<MPI_Request> reqs;
  RankSet rank;

  SearchParam param;
  param.te_count = bo.mseq.count;
  param.start_depth = depth;
  int node_limit = 25000; // magic

  // 仮評価を得る
  search_expand_trial_values( param, &bo, depth, node_limit, &rank );

  if ( !rank.size() ) {
    *best_move = DTe(DTe::RESIGN, TE_NUL, 0);
    return SI_NORMAL; // 負け
  }

  // 1ノードは必至探索だけ
  if (comm_size >= 3) {
    cmd.kind = Command::HISSHI;
    cmd.board = &bo;
    isend_obj( wait_queue.front(), TAG_CMD, cmd, &reqs );
    active_queue.insert( wait_queue.front() );
    wait_queue.pop_front();
  }

  // 上 (仮評価の良い方) から各ノードに配分する
  for ( int i = 1; i < (comm_size >= 3 ? comm_size - 1 : comm_size); i++ ) {
    RankSet::const_iterator it = rank.begin();

    cmd.kind = Command::NORMAL;
    cmd.board = &bo;
    cmd.te = it->te; // 計算ノードのほうで手を進める
    isend_obj( wait_queue.front(), TAG_CMD, cmd, &reqs );
    rank.erase(it);
    active_queue.insert( wait_queue.front() );
    wait_queue.pop_front();
  }
  mMPI_CHECK_RESULT( MPI_Waitall(reqs.size(), &reqs[0], MPI_STATUS_IGNORE) );

  // 計算を反映する. とりあえず1回だけ. TODO: 消費時間を見ながら調整
  RankSet result_set;
  while ( active_queue.size() > 0 ) {
    MPI_Status status;
    mMPI_CHECK_RESULT( 
         MPI_Probe( MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status) );

    if (status.MPI_TAG == TAG_GUI_INPUT) {
      string line;
      recv_obj(status.MPI_SOURCE, status.MPI_TAG, &line);
      cli_te_list.push_back(line);
      ...;
    }
    else if (status.MPI_TAG == TAG_GUI_INPUT_CLOSE) {
      int32_t value = 0;
      mMPI_CHECK_RESULT( MPI_Recv(&value, 1, MPI_INT32_T, status.MPI_SOURCE, 
			  status.MPI_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE) );
      assert( value < 0 );

      // GUIとのパイプ切断
      abort(); // TODO: 計算ノードを終了させる
      return SI_CHUDAN_REMOTE;
    }
    else {
      assert( status.MPI_TAG == TAG_RES );

      // 結果受信
      int const from_node = status.MPI_SOURCE;
      Response res;
      recv_obj( from_node, TAG_RES, &res );

      if (res.kind == Response::NORMAL_RES) {
	// 読んだ手とその得点を差し替える
	MoveScore move_score;
	move_score.te = res.te;
	move_score.score = res.score;
	result_set.insert(move_score);

	printf("%s->(%d,%d) ", te_str(res.te, bo).c_str(),
	     res.score.phi, res.score.delta );   // DEBUG
      }
      else if (res.kind == Response::HISSHI_RES) {
	abort(); // TODO:
      }
      active_queue.erase( from_node );
      wait_queue.push_back( from_node );
    }
  }

  *best_move = DTe( DTe::NORMAL_MOVE, result_set.begin()->te, 
		    bo.board[te_to(result_set.begin()->te)] );

  assert( wait_queue.size() == comm_size - 1 );
  return SI_NORMAL;
}


/** 
 * ノード終了(EXIT) か計算キャンセル(CANCEL) を全計算ノードに送る
 */
static void send_to_all_nodes(int comm_size, Tag tag )
{
  // MPI_Bcast() では MPI_Recv() で受け取れない
  vector<MPI_Request> reqs;
  for (int i = 1; i < comm_size; i++) {
    int32_t dmy = 0;
    MPI_Request req;
    mMPI_CHECK_RESULT(
	     MPI_Isend(&dmy, 1, MPI_INT32_T, i, tag, MPI_COMM_WORLD, &req) );
    reqs.push_back(req);
  }
  mMPI_CHECK_RESULT( MPI_Waitall(reqs.size(), &reqs[0], MPI_STATUS_IGNORE) );
}


/** 人間／GUI側のとき */
static void do_ponder( int comm_size, const BOARD* bo_ )
{
  printf("%s enter.\n", __func__); // DEBUG

  BOARD bo(*bo_);
  RankSet rank;

  SearchParam param;
  param.te_count = bo.mseq.count;
  param.start_depth = 11;  // magic
  int node_limit = 20000; // magic

  search_expand_trial_values( param, &bo, param.start_depth, node_limit, 
                              &rank );

  RankSet::const_iterator it;
  bool to_stop = false;
  for ( it = rank.begin(); it != rank.end() || active_queue.size() > 0; it++ ) {
    MPI_Status status;

    int flag = 0;
    if ( !wait_queue.size() ) {
      // 計算ノードが空くのを待つ
      mMPI_CHECK_RESULT(
	    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status) );
      flag = 1;
    }
    else {
      // GUIから入力があるか (計算ノードからの結果もあるかも)
      mMPI_CHECK_RESULT(
         	MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, 
				   &flag, &status) );
    }

    if ( flag ) {
      if (status.MPI_TAG == TAG_GUI_INPUT) {
	// GUIから入力
	string line;
	recv_obj( status.MPI_SOURCE, status.MPI_TAG, &line);
	cli_te_list.push_back(line);
	...;
      }
      else if (status.MPI_TAG == TAG_GUI_INPUT_CLOSE) {
	int32_t value = 0;
	mMPI_CHECK_RESULT( MPI_Recv(&value, 1, MPI_INT32_T, status.MPI_SOURCE,
		    status.MPI_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE) );
	assert( value < 0 );
	abort();  // TODO:
      }

	// 動いている計算を止める
	vector<MPI_Request> reqs;
	set<int>::const_iterator ii;
	for (ii = active_queue.begin(); ii != active_queue.end(); ii++) {
	  int32_t dmy = 0;
	  MPI_Request req;
	  mMPI_CHECK_RESULT(
	     MPI_Isend(&dmy, 1, MPI_INT32_T, *ii, TAG_CANCEL, 
                                                MPI_COMM_WORLD, &req) );
	  reqs.push_back(req);
	}
	mMPI_CHECK_RESULT( 
                     MPI_Waitall(reqs.size(), &reqs[0], MPI_STATUS_IGNORE) );
	to_stop = true;
      }
      else {
	assert( status.MPI_TAG == TAG_RES );

	Response res;
	recv_obj( status.MPI_SOURCE, TAG_RES, &res );
	assert( res.kind == Response::NORMAL_RES );

	// TODO: TTに保存
	abort();

	active_queue.erase( status.MPI_SOURCE );
	wait_queue.push_back( status.MPI_SOURCE );

	if ( to_stop && !active_queue.size() )
	  break;
      }
    }

    if ( !to_stop && wait_queue.size() > 0 && it != rank.end() ) {
      vector<MPI_Request> reqs;
      Command cmd;
      cmd.kind = Command::NORMAL;
      cmd.board = &bo;
      cmd.te = it->te;
      isend_obj( wait_queue.front(), TAG_CMD, cmd, &reqs );
      active_queue.insert( wait_queue.front() );
      wait_queue.pop_front();
      mMPI_CHECK_RESULT( 
                 MPI_Waitall(reqs.size(), &reqs[0], MPI_STATUS_IGNORE) );
    }
  } // for

  assert( wait_queue.size() == comm_size -1 );
}


/** GUIからのコマンド入力 */
static CliTeList cli_te_list;


/** csa_wait_for_result() とだいたい同じ */
static INPUTSTATUS gui_wait_for_result( const BOARD* bo, 
                                        DTe* te, int* elapsed_time )
{
  printf("%s\n", __func__); // DEBUG

  while (cli_te_list.size() == 0) {
    sleep(0);
  }

  return conn_queue_front_move(cli_te_list, bo, te, elapsed_time);
}


/** AI vs AIはできない */
static int local_ai_side = -1;

/**
 * マスタノード: 思考部は裏で勝手に読みつづける
 * daemon_game_main_loop() とだいたい同じ
 */
static void master_game_main_loop( int comm_size, BOARD* board )
{
  boPlayInit(board);
  gmSetGameStatus(&g_game, SI_NORMAL);
  Record record;

  newHASH();
  
  // MPIメッセージとGUI部からの通信を一つの関数で待てない
  // ユーザ/GUI部からの通信はワーカースレッドで待つ
  pthread_t thread_id = 0;
  if ( pthread_create(&thread_id, NULL, worker_wait_for_user_input, NULL) ) {
    printf("pthread_create() failed.\n");
    abort();
  }
  pthread_detach(thread_id);

  // 待ち行列
  wait_queue.clear();
  active_queue.clear();
  for (int i = 1; i < comm_size; i++)
    wait_queue.push_back(i);

  // main loop
  while ( true ) {
    gm_set_think_start_time(&g_game);

    printBOARD( board, global.display_front );

    DTe dte;
    int elapsed_time = 0;
    INPUTSTATUS minmax_return;
    if (board->next == local_ai_side) {
      printf("next = my side\n"); // DEBUG
      minmax_return = do_think( comm_size, board, 25, &dte); // magic

      // 指し手をサーバに送信
      switch (minmax_return) {
      case SI_NORMAL:
	conn_send_move(STDOUT_FILENO, board, dte);
	// 時間切れのことがある。千日手のことがある。
	minmax_return = gui_wait_for_result(board, &dte, &elapsed_time);
	break;
      case SI_TIMEOUT:
      case SI_CHUDAN_REMOTE:
	// 送信不要
	break;
      default:
	abort(); // ない
      }
    }
    else {
      // 人間/GUI側. 
      if (global.mode == GlobalSetting::HUMAN_CUI_MODE)
	print_prompt();

      // AIはバックグラウンドで考える
      do_ponder( comm_size, board );
      minmax_return = conn_queue_front_move(cli_te_list, board, 
                                            &dte, &elapsed_time );

      switch (minmax_return) {
      case SI_NORMAL:
	break;
      case SI_TIMEOUT:
	break;
      case SI_CHUDAN_REMOTE:
	break;
      default:
	abort();
      }

    }

    // 指し手を記録する
    if ( minmax_return != SI_CHUDAN_LOCAL &&
	 minmax_return != SI_CHUDAN_REMOTE ) {
      if (minmax_return == SI_TIMEOUT)
	dte.special = DTe::TIME_UP;
      daemon_dmoveinfo_add(record.mi, &dte, elapsed_time);
      gm_add_elapsed_time( &g_game, board->next, elapsed_time );
    }

    if ( minmax_return == SI_NORMAL &&
	 (dte.special == DTe::NORMAL_MOVE || dte.special == DTe::SENNICHITE) ) {
      boMove( board, dte.pack() );
      // 千日手チェック用
      bo_inc_repetition(board);
    }

    if ( !(minmax_return == SI_NORMAL && dte.special == DTe::NORMAL_MOVE) )
      break;
  }

  // 後片付け

  // TODO: ワーカースレッドの終了
  abort();
}


void print_prompt()
{
  assert( global.mode == GlobalSetting::HUMAN_CUI_MODE );

  printf("\n");
  printf("> ");
  fflush(stdout);
}


/** 入力バッファ */
RecvBuffer* recv_buffer = NULL;

/** 現在の局面. コマンドモードでloadすることもある */
BOARD* g_board = NULL;

/** 0でコマンドモード, 1でゲームモード */
static int game_mode = 0;


/**
 * コマンド入力を受け付ける.
 */
void cli_state_1()
{
  printf("%s: enter.\n", __func__); // DEBUG
  
  string cmd = recv_buffer->get_front();

  string name, value;
  string::size_type p;
  if ( (p = cmd.find(" ")) != string::npos ) {
    name = string(cmd, 0, p);
    value = string(cmd, p + 1);
  }
  else {
    name = cmd;
    value = "";
  }

  if ( name == "load" ) {
    if ( !bo_load_file(g_board, value.c_str()) )
      printf( "File not found or illegal board.\n" );
    else 
      printBOARD( g_board, global.display_front );
  }
  else if ( name == "quit") {
    game_mode = -1;
    return;
  }
  else if ( name == "newgame" ) {
    vector<string> pt;
    str_split(&pt, value, ' ');

    if ( (pt[0] != "human" && pt[0] != "ai") ||
	 (pt[1] != "human" && pt[1] != "ai") ) {
      printf("  invalid player type.\n");
      goto PROMPT;
    }

    if ( pt[0] == "ai" && pt[1] == "ai" ) {
      printf("  cannot play AI vs AI.\n");
      goto PROMPT;
    }

    // ゲーム準備
    initGAME(&g_game);
    local_ai_side = -1;
    for (int i = 0; i < 2; i++) {
      if (pt[i] == "ai")
	local_ai_side = i;
      // gmSetInputFunc( &g_game, i,
      // pt[i] == "human" ? si_input_next_human : si_input_next_computer );
    }

    // いったんループから抜ける
    game_mode = 1;
    return;
  }
  else if ( name == "flip" ) {
    global.display_front = 1 - global.display_front;
    printBOARD( g_board, global.display_front );
  }
  else if ( name == "help" )
    putHelp();
  else {
    printf("  invalid command.\n");
  }

PROMPT:
  print_prompt();
}


void shutdown_io_watch()
{
  recv_buffer->clear();
}


/** 
 * GUI部からのゲーム情報
 * とりあえず、どちらの側を持つか、だけ
 */
static void gui_state_11()
{
  string line = recv_buffer->get_front();

  string name, value;
  string::size_type p;
  if ( (p = line.find(":")) != string::npos ) {
    name = string(line, 0, p);
    value = string(line, p + 1);
  }
  else {
    name = line;
    value = "";
  }

  if ( name == "Your_Turn" ) {
    local_ai_side = value == "+" ? SENTE : GOTE;
  }
  else if (name == "END Game_Summary")
    game_mode = +1;
}


struct MasterState
{
  /** コールバック関数の切り替え */
  int state;

  bool operator () (int fd, int idx_);
};


bool MasterState::operator () (int fd, int idx_)
{
  printf("%s: enter.\n", __func__); // DEBUG

  if ( !recv_buffer->read() ) {
    shutdown_io_watch();
    game_mode = -1; // quit
    return false;
  }

  while ( recv_buffer->recv_list.size() > 0 ) {
    switch (state) {
    case 1:
      // コマンドモード時のコマンド入力
      cli_state_1();
      if ( game_mode != 0 )
	return false;
      break;

    case 11:
      // GUI部からのゲーム情報
      gui_state_11();
      if ( game_mode != 0 )
	return false;
      break;

    default:
      abort();
    }
  }

  return true; // 引き続き呼び出してもらう
}


void master_process( int comm_size )
{
  MasterState state;

  if ( !book_init(PACKAGE_DATA_DIR "/" PACKAGE "/daemonshogi-book.bin",
		  false) ) {
      printf("warning: book file '%s' not found.\n", 
	               PACKAGE_DATA_DIR "/" PACKAGE "/daemonshogi-book.bin");
      printf("         book disabled.\n");
  }

  if (global.mode == GlobalSetting::GUI_ENGINE_MODE)
    state.state = 11;
  else {
    printf( "# Welcome to Daemonshogi %s\n", VERSION );
    printf( "  Copyright (C) Masahiko Tokita    2002-2005,2009\n"
	      "  Copyright (C) Hisashi Horikawa   2008-2010\n"
	      "\n" );
    printf( "# To start game: newgame (ai|human) (ai|human)\n");
    printf( "  To help:       help\n" );

    print_prompt();
    state.state = 1;
  }

  global.display_front = 0;

  g_board = newBOARDwithInit();
  bo_reset_to_hirate( g_board );

  // ワーカースレッドでも共用
  recv_buffer = new RecvBuffer(STDIN_FILENO);

  // 標準入力からの入力待ち
  struct pollfd poll_list[1];
  poll_list[0].fd = STDIN_FILENO;
  poll_list[0].events = POLLIN | POLLPRI;

  while ( true ) {
    // コマンドモード. 普通にブロックしてもよさそうだが？
    game_mode = 0;
    do {
      dispatch_input( &poll_list[0], 1, -1, state );
      if ( (poll_list[0].revents & POLLHUP) != 0 )
	game_mode = -1;
    } while ( !game_mode );

    if (game_mode < 0)
      break;

    // ゲームモード (この中でブロックする)
    master_game_main_loop( comm_size, g_board );
  }

  delete recv_buffer;
  freeBOARD(g_board);

  printf("to exit...\n"); // DEBUG
  send_to_all_nodes(comm_size, TAG_EXIT);
  // 返信を待つ必要なし
}
