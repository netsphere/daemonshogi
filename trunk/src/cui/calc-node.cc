/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// 計算ノード

#define MPICH_SKIP_MPICXX 1
#include <config.h>

#include <assert.h>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <string>
#include <sstream>
#include "mini-mpi.h"
#include "dae-cui.h"
using namespace std;


/** 
 * マスタノードからキャンセル指示が届いていないか 
 * minmaxの内部からコールバックされる。
 */
static void test_cancel( bool may_block )
{
  if (may_block) {
    assert(0);
  }
  else {
    int flag = 0;
    MPI_Status status;
    mMPI_CHECK_RESULT(
        MPI_Iprobe( MASTER_NODE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status) );
    if (flag) {
      assert( status.MPI_TAG == TAG_CANCEL || status.MPI_TAG == TAG_EXIT );
      int32_t dmy;
      mMPI_CHECK_RESULT(
              MPI_Recv(&dmy, 1, MPI_INT32_T, MASTER_NODE, status.MPI_TAG, 
                                         MPI_COMM_WORLD, MPI_STATUS_IGNORE) );
      gmSetGameStatus( &g_game, 
               status.MPI_TAG == TAG_CANCEL ? SI_CHUDAN_LOCAL : SI_TO_EXIT );
    }
  }
}


void sub_process(int my_rank)
{
  printf("rank %d: %s start.\n", my_rank, __func__); // DEBUG

  si_set_pending_function( test_cancel );

  BOARD bo;

  while ( true ) {
    MPI_Status status;

    // ます、探索指示を受信する
    mMPI_CHECK_RESULT(
	      MPI_Probe(MASTER_NODE, MPI_ANY_TAG, MPI_COMM_WORLD, &status) );
    if (status.MPI_TAG == TAG_EXIT)
      break;
    else if (status.MPI_TAG == TAG_CANCEL) {
      // タイミングによって、こっちで受けることがある
      // Responseを返信ずみなので、cancelは単に捨ててよい
      abort(); // TODO: 1バイト読む
    }
    else {
      assert( status.MPI_TAG == TAG_CMD );

      Command cmd;
      cmd.board = &bo;
      recv_obj(MASTER_NODE, TAG_CMD, &cmd);

      TE mv;
      INPUTSTATUS status;
      Response res;

      // 探索
      if (cmd.kind == Command::HISSHI) {
	int node_limit = 10000; // magic

	si_start_alarm();
	MATE_STATE r = bfs_hisshi( &bo, 1, 21, &mv, &node_limit );
	si_end_alarm();

	res.kind = Response::HISSHI_RES;
	res.mate_state = r;
	res.te = mv;
      }
      else {
	assert( cmd.kind == Command::NORMAL );

	boMove(&bo, cmd.te);

	si_start_alarm();
	status = bfs_minmax( &bo, 31, &mv );
	si_end_alarm();

	if ( gmGetGameStatus(&g_game) == SI_TO_EXIT )
	  break;

	int r = hsGetBOARDMinMax(&bo, &res.score, NULL, 0);
	assert(r);
	res.kind = Response::NORMAL_RES;
	res.te = cmd.te;
      }

      // 返信
      send_obj( MASTER_NODE, TAG_RES, res );
    }
  } // while

  printf("rank %d: terminate.\n", my_rank); // DEBUG
}


/** 
 * 探索する。ブロックする = テスト用.
 * si_input_next_computer() とほぼ同じ
 */
static INPUTSTATUS do_think_with_block(const BOARD* bo_, DTe* best_move ) 
{
  TE te_ret;
#ifndef NDEBUG
  int side = bo_->next;
  uint64_t hash = bo_->hash_all;
#endif

  BOARD bo = BOARD(*bo_);

  if ( jouseki(&bo, &te_ret) ) {
    *best_move = DTe( DTe::NORMAL_MOVE, te_ret, bo.board[te_to(te_ret)] );
    return SI_NORMAL;
  }

  INPUTSTATUS ret = bfs_minmax(&bo, 25, &te_ret);

  if ( ret == SI_NORMAL ) {
    if ( te_to(te_ret) )
      *best_move = DTe( DTe::NORMAL_MOVE, te_ret, bo.board[te_to(te_ret)] );
    else
      *best_move = DTe( DTe::RESIGN, 0, 0 );
  }

  assert( side == bo.next );
  assert( hash == bo.hash_all );
  return ret;
}
