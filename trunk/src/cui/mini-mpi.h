/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DAE_MINI_MPI_H
#define DAE_MINI_MPI_H 1

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <sstream>
#include <string>
#include <vector>
#include <mpi.h>
#include <stdio.h>


/** 戻り値を確認 */
#define mMPI_CHECK_RESULT( func_call ) \
  {                                   \
    int result__ = func_call;         \
    if (result__ != MPI_SUCCESS)      \
      throw result__;                 \
  }


/** 送信する */
template <typename Typ>
static void send_obj( int dest_node, int tag, const Typ& obj )
{
  std::ostringstream oss;
  boost::archive::binary_oarchive oa(oss);

  oa << obj;

  int32_t len = oss.str().size();
  mMPI_CHECK_RESULT( MPI_Send(&len, 1, MPI_INT32_T, dest_node, tag, 
			      MPI_COMM_WORLD) );

  mMPI_CHECK_RESULT( MPI_Send((void*) oss.str().data(), len, MPI_BYTE, 
			      dest_node, tag + 1, MPI_COMM_WORLD) );
}


/** 受信する */
template <typename Typ>
static void recv_obj( int source_node, int tag, Typ* obj ) 
{
  int32_t len = 0;
  mMPI_CHECK_RESULT( MPI_Recv(&len, 1, MPI_INT32_T, source_node, tag,
			      MPI_COMM_WORLD, MPI_STATUS_IGNORE) );

  char* buf = static_cast<char*>(malloc(len + 1));
  mMPI_CHECK_RESULT( MPI_Recv(buf, len, MPI_BYTE, source_node, tag + 1,
			      MPI_COMM_WORLD, MPI_STATUS_IGNORE) );

  // TODO: もっとメモリ確保が少なくならんか
  std::istringstream iss( std::string(buf, len) );
  boost::archive::binary_iarchive ia(iss);

  ia >> *obj;

  free(buf);
}


/** 
 * オブジェクトを送信する (non-blocking).
 * 完了する前に戻る. 呼び出し側で MPI_Wait() すること
 * vector (だけ) は要素が並んで格納される
 */
template <typename Typ>
void isend_obj( int dest_node, int tag, const Typ& obj, 
		std::vector<MPI_Request>* reqs )
{
  std::ostringstream oss;
  boost::archive::binary_oarchive oa(oss);

  oa << obj;

  int32_t len = oss.str().size();
  printf("len = %d\n", len); // DEBUG
  MPI_Request req;
  mMPI_CHECK_RESULT( MPI_Isend(&len, 1, MPI_INT32_T, dest_node, tag, 
			       MPI_COMM_WORLD, &req) );
  reqs->push_back(req);
  mMPI_CHECK_RESULT( MPI_Isend((void*) oss.str().data(), len, MPI_BYTE, 
			       dest_node, tag + 1, MPI_COMM_WORLD, &req) );
  reqs->push_back(req);
}


#endif // !DAE_MINI_MPI_H
