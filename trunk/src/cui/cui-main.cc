/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// マルチプロセッサ版 (MPIを利用)

#define _XOPEN_SOURCE 700 // SUSv4
#define MPICH_SKIP_MPICXX 1
#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <vector>
#include "mini-mpi.h"
#include <string.h>
#include <poll.h> // poll()
#include <errno.h>
#include "dae-cui.h"
#include "master.h"
using namespace std;


GlobalSetting global;

extern void master_process( int comm_size );
extern void sub_process(int my_rank);


int main(int argc, char* argv[])
{
  if ( MPI_Init(&argc, &argv) != MPI_SUCCESS ) {
    printf("MPI_Init() failed.\n");
    exit(1);
  }

  int comm_size = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
  if (comm_size < 2) {
    // CPUノード数 + 1 を指定
    printf("comm_size must be '>= 2'\n");
    exit(1);
  }

  int my_rank = -1;
  if ( MPI_Comm_rank(MPI_COMM_WORLD, &my_rank) != MPI_SUCCESS ) {
    printf("MPI_Comm_rank() failed.\n");
    exit(1);
  }

  srand( time(NULL) );
  newHASH();
      
  if (my_rank != 0) {
    // スレーブ (通常探索, 詰み探索)
    sub_process(my_rank);
  }
  else {
    // マスタ (定跡のみ, あとはスレーブに投げる)
    if ( argc == 2 && !strcmp(argv[1], "gui") ) {
      global.mode = GlobalSetting::GUI_ENGINE_MODE;
    }
    else {
      // CUIで遊ぶ
      global.mode = GlobalSetting::HUMAN_CUI_MODE;
    }
    master_process( comm_size );
  }

  freeHASH();
  MPI_Finalize();
  return 0;
}
