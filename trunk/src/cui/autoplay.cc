/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// UP版を二つ起動して、ゲームさせる


#define _XOPEN_SOURCE 700 // SUSv4
#include <config.h>
#include <string>
#include <vector>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <poll.h>
#include "si/si.h"
#include "dae-cui.h"
using namespace std;


struct PlayerReader
{
  bool agreed[2];
  RecvBuffer recv_buffer;

  PlayerReader() { 
    agreed[0] = false; agreed[1] = false;
  }

  void operator () (int fd, int index) {
    char s[1000];
    int r = read(fd, s, sizeof(s) - 1);
    s[r] = '\0';
    strbuf[index] += s;

    // 行を切り出す
  }
};


static void run_engine(const string& p1, const string& p2)
{
  ChildProcess child[2];
  int i;

  for (i = 0; i < 2; i++) {
    char* args[] = {
      strdup("./daemonshogi-up"),
      strdup("-c"),
      strdup(i == 0 ? string(p1).c_str() : string(p2).c_str()),
      NULL
    };

    if ( !spawn_async_with_pipe(".", args, &child[i]) ) {
      printf("spawn error.\n");
      exit(1);
    }

    for (int j = 0; args[j]; j++)
      free(args[j]);
  }

  // ゲーム送信
  initGAME(&g_game);
  BOARD bo;
  bo_reset_to_hirate(&bo);

  string game_id = str_format("gid%d", time(NULL));
  
  for (i = 0; i < 2; i++) {
    string s = str_format(
"BEGIN Game_Summary\n"
"Protocol_Version:1.1\n"
"Protocol_Mode:Server\n"
"Format:Shogi 1.0\n"
"Declaration:Jishogi 1.1\n"
"Game_ID:%s\n"
"Name+:%s\n"
"Name-:%s\n"
"Your_Turn:%c\n"
"Rematch_On_Draw:NO\n"
"To_Move:+\n"
"BEGIN Time\n"
"Time_Unit:1sec\n"
"Total_Time:%d\n"
"Least_Time_Per_Move:1\n"
"END Time\n"
"BEGIN Position\n"
"P1-KY-KE-GI-KI-OU-KI-GI-KE-KY\n"
"P2 * -HI *  *  *  *  * -KA * \n"
"P3-FU-FU-FU-FU-FU-FU-FU-FU-FU\n"
"P4 *  *  *  *  *  *  *  *  * \n"
"P5 *  *  *  *  *  *  *  *  * \n"
"P6 *  *  *  *  *  *  *  *  * \n"
"P7+FU+FU+FU+FU+FU+FU+FU+FU+FU\n"
"P8 * +KA *  *  *  *  * +HI * \n"
"P9+KY+KE+GI+KI+OU+KI+GI+KE+KY\n"
"P+\n"
"P-\n"
"+\n"
"END Position\n"
"END Game_Summary\n",
	    game_id.c_str(),
	    p1.c_str(),
	    p2.c_str(),
	    i == 0 ? '+' : '-',
	    60 * 15 );
    ssize_t r = ::write( child[i].write_fd, s.c_str(), s.size() );
    assert( r == s.size() );
  }

  // AGREEを待つ
  PlayerReader fn;
  struct pollfd poll_list[2];
  for (i = 0; i < 2; i++) {
    poll_list[i].fd = child[i].read_fd;
    poll_list[i].events = POLLIN | POLLPRI;
  }

  while( !(fn.agreed[0] && fn.agreed[1]) )
    dispatch_input(poll_list, 2, -1, fn);

  // ゲーム開始
  s = str_format("START:%s\n", game_id.c_str());
  for (i = 0; i < 2; i++)
    ::write( child[i].write_fd, s.c_str(), c.size() );

  // main loop
  int teban = 0;
  while (true) {
    fn.recv_buffer.read_from_server( child[teban].read_fd );
  }
}


int main(int argc, char* argv[])
{
  vector<string> pattern_files;

  // ディレクトリ内のパターンファイル
  DIR* d = opendir( argv[1] );
  if ( !d ) {
    perror("opendir() fail");
    exit(1);
  }

  const struct dirent* e;
  while ( (e = readdir(d)) != NULL ) {
    if ( e->d_name[0] == '.' )
      continue;
    pattern_files.push_back(e->d_name);
  }
  closedir( d );

  // 総当たり
  for (vector<string>::size_type i = 0; i < pattern_files.size(); i++) {
    for (vector<string>::size_type j = 0; j < pattern_files.size(); j++) {
      if (i == j)
	continue;
      run_engine(pattern_files[i], pattern_files[j]);
    }
  }

  return 0;
}
