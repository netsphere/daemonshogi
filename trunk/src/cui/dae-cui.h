/*
  Daemonshogi -- a GTK+ based, simple shogi (Japanese chess) program.

  Copyright (C) Masahiko Tokita    2002-2005,2009 
  Copyright (C) Hisashi Horikawa   2008-2010

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DAE_CLI_H
#define DAE_CLI_H 1

#include <boost/serialization/version.hpp>
#include <boost/serialization/split_member.hpp>
#include <string>
#include <algorithm>
#include "si/si.h"
#include "si/si-think.h"


////////////////////////////////////////////////////////////////////////
// ノード間通信

enum Tag {
  // マスタ -> 計算ノード. 可変長
  TAG_CMD = 1, // 1..2

  // 計算ノード -> マスタ. 可変長
  TAG_RES = 3, // 3..4

  // GUI入力部 (ワーカースレッド) -> マスタ
  TAG_GUI_INPUT = 5, // 5..6

  TAG_GUI_INPUT_CLOSE = 7,

  /** 計算ノード終了 */
  TAG_EXIT = 8, 

  /** 計算中断 */
  TAG_CANCEL = 9,
};

#define MASTER_NODE 0


/** マスタノード -> 計算ノード */
class Command 
{
  // おまじない
  friend class boost::serialization::access;
  BOOST_SERIALIZATION_SPLIT_MEMBER()

  // 必ず private
  template <class Archive>
  void save(Archive& ar, unsigned int version) const 
  {
    assert(board);

    ar & kind;

      std::string s = bo_to_fixed_str(board);
      ar & s;

      // 直近の最大2手のみ送信
      int count = std::min(board->mseq.count, 2);
      ar & count;
      for (int i = board->mseq.count - count; i < board->mseq.count; i++)
	ar & board->mseq.te[i];

      if (kind == NORMAL)
	ar & te;
  }

  // 必ず private
  template <class Archive>
  void load(Archive& ar, unsigned int version) 
  {
    assert(board);

    ar & kind;

      std::string board_str;
      ar & board_str;
      bo_set_by_fixed_str(board, board_str);

      ar & board->mseq.count;
      for (int i = 0; i < board->mseq.count; i++)
	ar & board->mseq.te[i];

      if (kind == NORMAL)
	ar & te;
  }

public:
  enum Kind {
    NORMAL,  // 通常探索. 詰み探索を含む
    HISSHI,
  };

  // constにはしない
  Kind kind;
  BOARD* board;
  TE te;

  Command(): board(NULL) { }
};
BOOST_CLASS_VERSION(Command, 1)


/** 計算ノード -> マスタノード */
class Response 
{
  friend class boost::serialization::access;
  BOOST_SERIALIZATION_SPLIT_MEMBER()

  // 必ず private
  template <class Archive>
  void save(Archive& ar, unsigned int version) const {
    ar & kind;
    ar & te;
    
    if (kind == NORMAL_RES)
      ar & score.phi & score.delta;
    else if (kind == HISSHI_RES)
      ar & mate_state;
    else {
      abort();
    }
  }

  // 必ず private
  template <class Archive>
  void load(Archive& ar, unsigned int version) {
    ar & kind;
    ar & te;

    if (kind == NORMAL_RES)
      ar & score.phi & score.delta;
    else if (kind == HISSHI_RES)
      ar & mate_state;
    else {
      abort();
    }
  }

public:
  enum Kind {
    NORMAL_RES,
    HISSHI_RES
  };

  Kind kind;
  MATE_STATE mate_state;
  TE te;
  PhiDelta score;
};
BOOST_CLASS_VERSION(Response, 1)


////////////////////////////////////////////////////////////////////////


#endif // DAE_CLI_H
