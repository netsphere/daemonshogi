
mi = 1000.0
ma = -1000.0

File.open("02ai.txt") {|fp|
  fp.each {|line|
    v = line.to_f
    mi = [mi, v].min
    ma = [ma, v].max
  }
}

print "min = #{mi}, max = #{ma}\n"

